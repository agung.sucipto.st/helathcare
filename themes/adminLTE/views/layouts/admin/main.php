<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title><?php echo CHtml::encode($this->pageTitle); ?></title>
        <meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>
        <!-- bootstrap 3.0.2 -->
        <link href="<?php echo Yii::app()->theme->baseUrl; ?>/assets/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
        <!-- font Awesome -->
        <link href="<?php echo Yii::app()->theme->baseUrl; ?>/assets/font-awesome-4.7.0/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
		<link href="<?php echo Yii::app()->theme->baseUrl; ?>/assets/css/ionicons.min.css" rel="stylesheet" type="text/css" />
        <link href="<?php echo Yii::app()->theme->baseUrl; ?>/assets/simple-line-icons/simple-line-icons.min.css" rel="stylesheet" type="text/css"/>
        <!-- Theme style -->
		<link rel="shortcut icon" href="<?php echo Yii::app()->theme->baseUrl; ?>/assets/logo.png"/>
        <link href="<?php echo Yii::app()->theme->baseUrl; ?>/assets/css/AdminLTE.css" rel="stylesheet" type="text/css" />
		
		<script language="javascript" type="text/javascript" src="<?php echo Yii::app()->baseUrl; ?>/tinymce/tinymce.min.js"></script>
        <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
          <script src="<?php echo Yii::app()->theme->baseUrl; ?>/assets/https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
          <script src="<?php echo Yii::app()->theme->baseUrl; ?>/assets/https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
        <![endif]-->
		<style>
		.static{
			position:fixed;
			z-index:9;
			width:100%;
			left:0;
		}
		.head-container .navbar-nav>li>a{
			padding-top: 5px;
			padding-bottom: 5px;
		}
		.head-container{
			background:white;
			border-bottom:1px solid #ccc;
		}
		</style>
    </head>
    <body class="skin-blue fixed">
        <!-- header logo: style can be found in header.less -->
        <header class="header">
            <a href="<?php echo Yii::app()->createUrl('default');?>" class="logo">
                <!-- Add the class icon to your logo image or logo icon to add the margining -->
                Healthcare +
            </a>
            <!-- Header Navbar: style can be found in header.less -->
			<?php $this->beginContent('//layouts/admin/main_notification'); ?>
			<?php $this->endContent(); ?>
        </header>
		
		
        <div class="wrapper row-offcanvas row-offcanvas-left">
            <!-- Left side column. contains the logo and sidebar -->
            <aside class="left-side sidebar-offcanvas collapse-left">
                <!-- sidebar: style can be found in sidebar.less -->
                <section class="sidebar">
                    <div class="user-panel">
                        <div class="pull-left image">
                            <img src="<?php echo Yii::app()->theme->baseUrl; ?>/assets/img/avatar3.png" class="img-circle" alt="User Image" />
                        </div>
                        <div class="pull-left info">
                            <p>
							<?php $id=Yii::app()->user->id;
									$data=User::model()->findByPk($id);
									echo $data->username; 
							?>
							</p>

                            <a href="<?php echo Yii::app()->theme->baseUrl; ?>/assets/#"><i class="fa fa-circle text-success"></i> Online</a>
                        </div>
                    </div>
					
                    <!-- sidebar menu: : style can be found in sidebar.less -->
                    <?php $this->beginContent('//layouts/admin/main_menu'); ?>
					<?php $this->endContent(); ?>
                </section>
                <!-- /.sidebar -->
            </aside>

            <!-- Right side column. Contains the navbar and content of the page -->
            <aside class="right-side strech">
				<!--
				<div class="head-container">
					<?php // $this->beginContent('//layouts/admin/main_navigation'); ?>
					<?php //$this->endContent(); ?>
				</div>
				-->
				<?php
				if(!empty($this->breadcrumbs)){
				?>
                <!-- Content Header (Page header) -->
                <section class="content-header">
					<h1 class="title"><?php echo (isset($this->title['title'])?$this->title['title']:"");?>
						<small><?php echo (isset($this->title['deskripsi'])?$this->title['deskripsi']:"");?></small>
					</h1>
					
					<?php $this->beginContent('//layouts/admin/breadcrumbs'); ?>
					<?php $this->endContent(); ?>
                </section>
				<?php
				}
				?>

                <!-- Main content -->
                <section class="content">

					<?php echo $content; ?>
				
					<!-- END PAGE CONTENT-->

                </section><!-- /.content -->
            </aside><!-- /.right-side -->
        </div><!-- ./wrapper -->

        <!-- add new calendar event modal -->


        <!-- jQuery 2.0.2 -->
       <?php  Yii::app()->clientScript->registerCoreScript('jquery'); ?>
        <script src="<?php echo Yii::app()->theme->baseUrl; ?>/assets/js/jquery-migrate-1.2.1.min.js" type="text/javascript"></script>
        <!-- AdminLTE App -->
        <script src="<?php echo Yii::app()->theme->baseUrl; ?>/assets/js/AdminLTE/app.js" type="text/javascript"></script>
    </body>
	<script>
	$(".sidebar-menu").slimscroll({
        height: $(window).height(),
        alwaysVisible: false,
        size: "3px"
    });
	$(window).scroll(function(){
		var head = $(".head-container");
		var leftMenu = $(".left-side").hasClass("collapse-left");
		var widthMenu = $(".left-side").width();
		if($(this).scrollTop()>10){
			head.addClass("static");
			if(leftMenu==false){
				$('.static').css('left',widthMenu);
			}else{
				$('.static').css('left',0);
			}if(leftMenu==false){
				$('.static').css('left',widthMenu);
			}else{
				$('.static').css('left',0);
			}
		}else{
			head.removeClass("static");
		}
	});
	</script>
</html>