<!-- Modal 1 (Confirm)-->
	<div class="modal fade" id="modalKonfirmasi" aria-hidden="true" style="display: none;">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-hidden="true">x</button>
					<h4 class="modal-title">Konfirmasi</h4>
				</div>
				<div class="modal-body">
					Apakah anda yakin untuk menghapus data yang dipilih?
					<div class="preview-data"></div>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-white" data-dismiss="modal" id="modalKonfirmasiCancel">Batal</button>
					<button type="button" class="btn btn-info" id="modalKonfirmasiOk">Ya</button>
				</div>
			</div>
		</div>
	</div>
	
	<!-- Modal 6 (Long Modal)-->
	<div class="modal fade" id="modalForm">
		<div class="modal-dialog">
			<div class="modal-content">
				
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
					<h4 class="modal-title">Modal Content is Responsive</h4>
				</div>
				
				<div class="modal-body">
					
				</div>
				
				<div class="modal-footer">
					<button type="button" class="btn btn-white" data-dismiss="modal">Close</button>
					<button type="button" class="btn btn-info">Save changes</button>
				</div>
			</div>
		</div>
	</div>