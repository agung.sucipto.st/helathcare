<nav class="navbar navbar-static-top" role="navigation">
	<!-- Sidebar toggle button-->
	<a href="<?php echo Yii::app()->theme->baseUrl; ?>/assets/#" class="navbar-btn sidebar-toggle" data-toggle="offcanvas" role="button">
		<span class="sr-only">Toggle navigation</span>
		<span class="icon-bar"></span>
		<span class="icon-bar"></span>
		<span class="icon-bar"></span>
	</a>
	
	<div class="navbar-left">
		<?php $this->beginContent('//layouts/admin/main_navigation'); ?>
		<?php $this->endContent(); ?>
	</div>
	<div class="navbar-right">
		
		<ul class="nav navbar-nav">
			<?php $this->beginContent('//layouts/admin/_menu_search'); ?>
			<?php $this->endContent(); ?>
			
			<?php //$this->beginContent('//layouts/admin/_menu_notification'); ?>
			<?php //$this->endContent(); ?>
			
			<li class="dropdown user user-menu">
				<a href="#"" class="dropdown-toggle" data-toggle="dropdown">
					<i class="glyphicon glyphicon-user"></i>
					<span>
						<?php $id=Yii::app()->user->id;
								$data=User::model()->findByPk($id);
								echo $data->username; 
						?>
						<i class="caret"></i>
					</span>
				</a>
				<ul class="dropdown-menu">
					<!-- User image -->
					<li class="user-header bg-light-blue">
						<img src="<?php echo Yii::app()->theme->baseUrl; ?>/assets/img/avatar3.png" class="img-circle" alt="User Image">
						<p>
							<?=Ucwords($data->username)?>
							<small>Last Login : <?=$data->last_login?></small>
						</p>
					</li>
					<!-- Menu Body -->
					<li class="user-body">
						<div class="col-xs-6 text-center">
							<a href="#">My Profile</a>
						</div>
						<div class="col-xs-6 text-center">
							<a href="<?php echo Yii::app()->createUrl('default/logout'); ?>">Log Out</a>
						</div>
					</li>
				</ul>
			</li>
		</ul>
	</div>
</nav>

