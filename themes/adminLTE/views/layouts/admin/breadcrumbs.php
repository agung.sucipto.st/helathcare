<?php
if(!empty($this->breadcrumbs)){
?>
<ol class="breadcrumb">
	<li><a href="<?php echo Yii::app()->baseUrl;?>"><i class="fa fa-dashboard"></i> Home</a></li>
	<?php
	$breadcrumb=$this->breadcrumbs;
	foreach($breadcrumb as $key=>$row){
		if(is_array($row)){
			echo'<li><a href="'.Yii::app()->createUrl($row[0]).'">'.$key.'</a></li>';
		}else{
			echo'<li class="active">'.$row.'</li>';
		}
	}
	?>
</ol>
<?php
}
?>