<style>
@media screen and (max-width: 620px) and (min-width: 120px) {
    .search-top {
		display:none;
	}
}

@media screen and (max-width: 768px) and (min-width: 620px) {
    .search-top {
		border-radius: 100px;
		margin-top:-10px;
		height: 34px;
		padding: 6px 12px;
		font-size: 14px;
		line-height: 1.428571429;
		color: #555;
		vertical-align: middle;
		background-color: #fff;
		background-image: none;
		border: 1px solid #ccc;
		display:block;
	}
}

@media screen and (max-width: 1600px) and (min-width: 700px) {
    .search-top {
		border-radius: 20px;
		height: 34px;
		padding: 6px 12px;
		font-size: 14px;
		line-height: 1.428571429;
		color: #555;
		vertical-align: middle;
		background-color: #fff;
		background-image: none;
		border: 1px solid #ccc;
		display:block;
	}
}
</style>
<li class="search-menu">
	<form class="navbar-form navbar-left" id="search" role="search" action="<?=Yii::app()->createUrl('pasien/view');?>">
		<div class="form-group">
		  <input type="text" class="search-top" id="navbar-search-input" name="id" placeholder="Cari Rekam Medis ...">
		</div>
	 </form>
</li>