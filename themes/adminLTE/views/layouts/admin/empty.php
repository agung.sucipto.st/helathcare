<!DOCTYPE html>
<htmL>
<head>
	<meta charset="UTF-8">
	<title><?php echo CHtml::encode($this->pageTitle); ?></title>
	<meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>
	<!-- bootstrap 3.0.2 -->
	<link href="<?php echo Yii::app()->theme->baseUrl; ?>/assets/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
	<!-- font Awesome -->
	<link href="<?php echo Yii::app()->theme->baseUrl; ?>/assets/font-awesome-4.7.0/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
	<link href="<?php echo Yii::app()->theme->baseUrl; ?>/assets/css/ionicons.min.css" rel="stylesheet" type="text/css" />
	<link href="<?php echo Yii::app()->theme->baseUrl; ?>/assets/simple-line-icons/simple-line-icons.min.css" rel="stylesheet" type="text/css"/>
	<!-- Theme style -->
	<link rel="shortcut icon" href="<?php echo Yii::app()->theme->baseUrl; ?>/assets/logo.png"/>
	<link href="<?php echo Yii::app()->theme->baseUrl; ?>/assets/css/AdminLTE.css" rel="stylesheet" type="text/css" />
</head>
<body>
<?php echo $content; ?>
</body>
</html>
