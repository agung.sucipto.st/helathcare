<?php
$this->breadcrumbs=array(
	'Kelola Resep Pasien'=>array('resepPasien/index'),
	'Form Penjualan Resep',
);
$this->title=array(
	'title'=>'Form Penjualan Resep',
	'deskripsi'=>''
);?>

<div class="panel panel-default">
	<div class="panel-heading">
		<h3 class="panel-title pull-left">Form Penjualan Resep</h3>
		<a href="<?php echo Yii::app()->createUrl(Yii::app()->controller->id);?>" class="btn btn-primary btn-xs pull-right">
			<i class="fa fa-arrow-circle-left"></i>
			<span>Kembali</span>
		</a>
		<div style="clear:both"></div>
	</div>
	<div class="panel-body">
		<?php echo $this->renderPartial('buat_resep',array("model"=>$model,"registrasi"=>$registrasi,"resep"=>$resep,"transaksi"=>$transaksi));?>
	</div>
</div>