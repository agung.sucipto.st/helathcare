<?php
$this->breadcrumbs=array(
	'Resep Elektronik',
);
$this->title=array(
	'title'=>'Resep Elektronik',
	'deskripsi'=>'Mengelola Penjualan Menggunakan Resep Elektronik'
);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	$('.import-form').hide();
	return false;
});
$('.btn-cancel').click(function(){
	$('.import-form').toggle();
	return false;
});
$('.import-button').click(function(){
	$('.import-form').toggle();
	$('.search-form').hide();
	return false;
});
$('.search-form form').submit(function(){
	$.fn.yiiGridView.update('resep-pasien-grid', {
		data: $(this).serialize()
	});
	return false;
});

");
?>

<div class="box box-primary">
	<div class="box-body">
		<h4 class="box-title">Resep Elektronik</h4>
		<hr >
		<div class="search-form">
			<?php $this->renderPartial('_search',array('model'=>$model)); ?>
		</div><!-- search-form -->
		
		<style>
		.pagination li.selected a{
			background:rgb(235, 235, 236);
		}
		</style>
		
		
		<?php
		if(Yii::app()->user->hasFlash('success')){
			echo'
			<div class="alert alert-success">
				<button type="button" class="close" data-dismiss="alert">&times;</button>
				'.Yii::app()->user->getFlash('success').'
			</div>';
		}
		?>
							
		<div id="example-2_wrapper" class="dataTables_wrapper form-inline dt-bootstrap no-footer">
		
		<?php 
		// put this somewhere on top
		$pageSize=Yii::app()->user->getState('pageSize',Yii::app()->params['defaultPageSize']); ?>		
		<?php		$this->widget('booster.widgets.TbExtendedGridView', array(
			'id'=>'resep-pasien-grid',
			'type' => 'striped',
			'dataProvider' => $model->search(),
			'summaryText'=>'Menampilkan {start}-{end} dari {count} hasil.',
			'selectableRows' => 2,
			'responsiveTable' => true,
			'enablePagination' => true,
			'pager' => array(
				'htmlOptions'=>array(
					'class'=>'pagination'
				),
				'maxButtonCount' => 5,
				'cssFile' => true,
				'header' => false,
				'firstPageLabel' => '<<',
				'prevPageLabel' => '<',
				'nextPageLabel' => '>',
				'lastPageLabel' => '>>',
			),
			'columns'=>array(
					array(
						"header"=>"Waktu",
						"name"=>"waktu_resep",
						"value"=>'Lib::dateIndShortMonth($data->waktu_resep,false)'
					),
					'no_resep',
					array(
						"header"=>"No Registrasi",
						"name"=>"no_registrasi",
						"value"=>'$data->idRegistrasi->no_registrasi'
					),
					array(
						"header"=>"No RM",
						"name"=>"id_pasien",
						"value"=>'Lib::MRN($data->idRegistrasi->id_pasien)'
					),
					array(
						"header"=>'Nama Pasien',
						"type"=>'Raw',
						"name"=>"nama_lengkap",
						"value"=>function($data){
							$icon='male';
							if($data->idRegistrasi->idPasien->idPersonal->jenis_kelamin=='Perempuan'){
								$icon='<i class="fa fa-venus" style="color:red;font-weight:bold;"></i>';
							}else{
								$icon='<i class="fa fa-mars" style="color:blue;font-weight:bold;"></i>';
							}
							return $icon.$data->idRegistrasi->idPasien->idPersonal->nama_lengkap;					
						}
					),
					array(
						"header"=>"Dokter",
						"value"=>'$data->idDokter->idPersonal->nama_lengkap'
					),
					array(
						"header"=>"Jenis Registrasi",
						"value"=>'$data->idRegistrasi->jenis_registrasi'
					),
					array(
						"header"=>"Departemen",
						"value"=>function($data){
							$text='';
							if($data->idRegistrasi->jenis_registrasi=='Rawat Inap'){
								$bedLast = BedPasien::model()->find(array("condition"=>"id_registrasi='$data->id_registrasi'","order"=>"id_bed_pasien DESC","limit"=>1));
								$text.=$bedLast->idBed->idRuangRawatan->idKelas->nama_kelas.' '.$bedLast->idBed->idRuangRawatan->kode_ruangan." ".$bedLast->idBed->idRuangRawatan->nama_ruangan.' / '.$bedLast->idBed->no_bed;
							} else {
								$text.=$data->idRegistrasi->idDepartemen->nama_departemen;
							}
							return $text;
						}
					),
					array(
						"header"=>"Jenis Jaminan",
						"type"=>"raw",
						"value"=>function($data){
							$text='';
							if($data->idRegistrasi->jenis_jaminan=="UMUM"){
								$text.="Umum";
							}else{
								foreach($data->idRegistrasi->jaminanPasiens as $row){
									$text.=$row->idPenjamin->nama_penjamin.'<br/>';
								}
							}
							return $text;
						}
					),
					'status_resep',
					array(
						"header"=>"User Input",
						"type"=>"raw",
						"value"=>function($data){
							return $data->userCreate->idPegawai->idPersonal->nama_lengkap.'<br/>'.Lib::dateIndShortMonth($data->time_create,false);
						}
					),
					array(
						'class'=>'booster.widgets.TbButtonColumn',
						 'deleteConfirmation'=>'Anda yakin akan menhapus data?',
						'template'=>'{proses}',
						'buttons'=>array
						(
							'proses' => array
							(
								'label'=>'View',
								'icon'=>'search',
								'url'=>'Yii::app()->createUrl("resepPasien/proses",array("id"=>$data->id_resep_pasien))',
								'visible'=>'(Lib::accessBy(\'resepPasien\',\'proses\') AND $data->status_resep=="Belum Diproses")',
								'options'=>array(
									'class'=>'btn btn-default btn-xs',
								),
							),
						),
						'header'=>CHtml::dropDownList('pageSize',$pageSize,array(10=>10,20=>20,50=>50,100=>100,200=>200,500=>500,1000=>1000),array(
							'onchange'=>"$.fn.yiiGridView.update('resep-pasien-grid',{ data:{pageSize: $(this).val() }})",
						)),
					),
			),
		));
		?>
		</div>
	</div>
</div>