<div class="box box-primary">
	<div class="box-body">
		<div class="row">
			<div class="col-sm-1">
				<?php
				$img='';
				$icon='';
				if($model->idPersonal->jenis_kelamin=='Perempuan'){
					$img=Yii::app()->theme->baseUrl.'/assets/images/female.png';
					$icon='<i class="fa fa-venus" style="color:red;font-weight:bold;"></i>';
				}else{
					$img=Yii::app()->theme->baseUrl.'/assets/images/male.png';
					$icon='<i class="fa fa-mars" style="color:blue;font-weight:bold;"></i>';
				}	
				echo'<img src="'.$img.'" class="img-responsive"/>';
				?>
			</div>
			<div class="col-sm-7">
				<div class="row">
					<div class="col-sm-12" style="font-size:25px;font-weight:bold;">
					<?php
						echo Lib::MRN($model->id_pasien).' - '.$icon.strtoupper($model->idPersonal->nama_lengkap).", ".$model->panggilan;	
					?>
					</div>
				</div>
				<div class="row">
					<div class="col-sm-12">
					<?php
						echo $model->idPersonal->jenis_kelamin.' / '.Lib::dateInd($model->idPersonal->tanggal_lahir,false).", ".Lib::umurDetail($model->idPersonal->tanggal_lahir);	
					?>
					<br/>
					<span class="text-danger text-bold">Alergi</span> : <?=$model->idPersonal->alergi;?>
					</div>					
				</div>
				
			</div>
			<div class="col-sm-4">
				<div class="row">
					<div class="col-sm-4 text-danger text-bold">No Registrasi</div>
					<div class="col-sm-8">: <?=$registrasi->no_registrasi;?></div>
				</div>
				<div class="row">
					<div class="col-sm-4 text-danger text-bold">Waktu Registrasi</div>
					<div class="col-sm-8">: <?=Lib::dateInd($registrasi->waktu_registrasi,false);?></div>
				</div>
				<div class="row">
					<div class="col-sm-4 text-danger text-bold">Dokter</div>
					<div class="col-sm-8">: 
					<?php
					foreach($registrasi->dpjps as $row){
						echo $row->idDokter->idPersonal->nama_lengkap.'<br/>';
					}
					?>
					</div>
				</div>
				<div class="row">
					<div class="col-sm-4 text-danger text-bold">Layanan</div>
					<div class="col-sm-8">: <?=$registrasi->idDepartemen->nama_departemen;?></div>
				</div>
				<div class="row">
					<div class="col-sm-4 text-danger text-bold">Jenis Jaminan</div>
					<div class="col-sm-8">: 
					<?php
						if($registrasi->jenis_jaminan=="UMUM"){
							echo "Umum";
						}else{
							foreach($registrasi->jaminanPasiens as $row){
								echo $row->idPenjamin->nama_penjamin.'<br/>';
							}
						}
					?>
					</div>
				</div>
				<div class="row">
					<div class="col-sm-4 text-danger text-bold">Catatan</div>
					<div class="col-sm-8">: <?=$registrasi->catatan;?></div>
				</div>
			</div>
		</div>
	</div>
</div>

<div class="box box-primary">
	<div class="box-body">
		<?php
		if(Yii::app()->user->hasFlash('error')){
			echo'
			<div class="alert alert-danger">
				<button type="button" class="close" data-dismiss="alert">&times;</button>
				'.Yii::app()->user->getFlash('error').'
			</div>';
		}
		?>
		<?php $form=$this->beginWidget('booster.widgets.TbActiveForm',array(
				'id'=>'resep-pasien-form',
				'enableAjaxValidation'=>false,
			)); ?>
		<div class="row">
			<div class="col-sm-6">
				<?php echo $form->dropDownListGroup($transaksi,'gudang_asal', array('label'=>"Depo",'widgetOptions'=>array('data'=>CHtml::listData(UserGudang::model()->findAll(array("condition"=>"id_user='".Yii::app()->user->id."'")),'idGudang.id_gudang','idGudang.nama_gudang'), 'htmlOptions'=>array('class'=>'input-large','empty'=>'- Pilih Depo -','required'=>true)))); ?>
				<div class="row">
					<div class="col-sm-8">
						<label>Cari Nama Obat  / Kandungan Obat</label>
						<?php
						$this->widget('zii.widgets.jui.CJuiAutoComplete',array(
							'name'=>'resep',
							'options'=>array(
								'minLength'=>3,
								'showAnim'=>'fold',
								'select'=>"js:function(event, data) {
									var item=data.item.label;
									addRow(item);
								}"
							),
							'source'=>$this->createUrl("registrasi/getObat"),
							'htmlOptions'=>array(
								'class'=>"form-control",
								"id"=>"searchResep"
							),
						));
						?>
					</div>
					<div class="col-sm-4">
						<br />
						<button class="btn btn-primary" onClick="addHeader()" type="button" id="addHead">Tambah Racikan</button>
						<button class="btn btn-danger" onClick="closeHeader()" type="button" id="closeHead" style="display:none;">Tutup Racikan</button>
						
					</div>
				</div>
				<br/>
				<small>
					*Klik Tambah Header Untuk Membuat Racikan, Lalu Pilih Item yang akan menjadi komponen racikan.
				</small>
				<br/>
				<small>
					*Klik Tutup Header Racikan Untuk Menginputkan Item non racikan
				</small>
				<br/>
				<small>
					*Untuk Input Item Non Racikan, Pastikan Tulisan Tombol adalah 
					<b>"Tambah Header Racikan"</b>
				</small>
				<br/>
				<small>
					*Komponen Racikan Harus Lebih dari 1 Item
				</small>
			</div>
			<div class="col-sm-6">
			
				<?php $this->widget('booster.widgets.TbDetailView',array(
				'data'=>$resep,
				'attributes'=>array(
					array(
						"label"=>"Dokter",
						"value"=>$resep->idDokter->idPersonal->nama_lengkap
					),
					array(
						"label"=>"Racikan",
						"value"=>$resep->racikan
					),
					array(
						"label"=>"Status Resep",
						"value"=>$resep->status_resep
					),
				),
				)); ?>
			</div>
		</div>
		
		<table class="table table-stripped">
			<tr>
				<th>Nama Obat</th>
				<th>Stok</th>
				<th width="100px">Qty</th>
				<th>Satuan</th>
				<th>Harga / Biaya Racikan</th>
				<th>Signa</th>
				<th></th>
			</tr>
			<tbody id="data">
				<?php
				$data=array();
				$auto=array();
				foreach($resep->resepPasienLists as $row){
					$data[]='rowResep['.$row->id_item.']="'.$row->idItem->nama_item.'";';
					$auto[]='
					jQuery("#signa'.$row->id_item.'").autocomplete({"minLength":3,"showAnim":"fold","source":"'.Yii::app()->createUrl("registrasi/getSigna").'"});';
					echo'
					<tr id="row'.$row->id_item.'">
						<td>
							<input type="hidden" name="id_item[]" value="'.$row->id_item.'"/>'.$row->idItem->nama_item.'
						</td>	
						<td>'.ItemGudang::getStock($row->id_item,1).'</td>
						<td>
							<input type="number" name="qty'.$row->id_item.'" class="form-control" required="required" max="'.ItemGudang::getStock($row->id_item,1).'" min="1" value="'.$row->jumlah.'"/>
						</td>
						<td>'.Item::getParentUnit($row->id_item)->idSatuan->nama_satuan.'</td>
						<td>'.Item::getPrice($row->id_item,$registrasi->id_kelas,1).'</td>
						<td>
							<input type="text" name="signa'.$row->id_item.'" id="signa'.$row->id_item.'" class="form-control" required="required" value="'.$row->signa.'"/>
						</td>
						<td>
							<span class="btn btn-danger" onclick="deleteRow('.$row->id_item.')"><i class="fa fa-times"></i></span>
						</td>
					</tr>';
				}
				?>
			</tbody>
		</table>
		
		<hr>
		<div class="form-actions">
			<?php $this->widget('booster.widgets.TbButton', array(
					'buttonType'=>'submit',
					'context'=>'primary',
					'htmlOptions'=>array(
						//"disabled"=>true,
						"id"=>"submit",
						"onClick"=>"return confirm('Apakah Resep Sudah Benar?');"
					),
					'label'=>$model->isNewRecord ? 'Tambah Data Pasien' : 'Proses Resep',
				)); 
				?>
		</div>
		<?php $this->endWidget(); ?>
	</div>
</div>

<?php
Yii::app()->clientScript->registerScript('addResep', '
var gudang = "'.$transaksi->gudang_asal.'";
var rowResep=[];
var racikan = false;
var racikanId = "1";

'.implode("\n",$data).'
'.implode("\n",$auto).'

$("#ItemTransaksi_gudang_asal").change(function(){
	var id = $("#ItemTransaksi_gudang_asal").val();
	gudang = id;
	if(rowResep.length > 0) {
		rowResep.forEach((x, idx) => {
			deleteRowEvent(idx);
		});
		rowResep = [];
	}
});

function deleteRowEvent(id){
	if(id[0] === `R`){
		jQuery(`.row${id}`).each(function() {
			var val = $(this).attr(`val`);
			$(this).remove();
		});
		$(`#row${id}`).remove();
	} else {
		$(`#row${id}`).remove();
	}
}

function deleteRow(id){
	if(id[0] === `R`){
		jQuery(`.row${id}`).each(function() {
			var val = $(this).attr(`val`);
			$(this).remove();
			rowResep.splice(val, 1);
		});
		$(`#row${id}`).remove();
		rowResep.splice(id, 1);
	} else {
		$(`#row${id}`).remove();
		rowResep.splice(id, 1);
	}
	//cekSubmit();
}

function cekSubmit(){
	console.log(rowResep);
	console.log(rowResep.length);
}

function closeHeader(){
	racikan = false;
	racikanId++;
	$("#closeHead").hide();
	$("#addHead").show();
}

function addHeader(){
	racikan = true;
	$("#addHead").hide();
	$("#closeHead").show();
	rowResep["R"+racikanId]="Racikan";
	var inputItem="<input type=\"hidden\" name=\"id_item[R"+racikanId+"]\" value=\"R"+racikanId+"\" class=\"form-control\"/><input type=\"text\" name=\"nameR"+racikanId+"\" value=\"Racikan Ke "+racikanId+"\" class=\"form-control\"/>";
	var stokItem=0;
	var qty="<input type=\"number\" name=\"qtyR"+racikanId+"\" value=\"\" class=\"form-control\" required=\"required\" min=\"1\"/>";
	var satuan="Racikan";
	var harga="<input type=\"number\" name=\"biayaR"+racikanId+"\" value=\"5000\" class=\"form-control\" required=\"required\" min=\"1\" placeholder=\"Biaya Racikan\"/>";
	var signa="<input type=\"text\" name=\"signaR"+racikanId+"\" id=\"signaR"+racikanId+"\" value=\"\" class=\"form-control\" required=\"required\"//>";
	var remove="<span class=\"btn btn-danger\" onclick=\"deleteRow(\'R"+racikanId+"\')\"><i class=\"fa fa-times\"></i></span>";
	$("#data").append("<tr id=\"rowR"+racikanId+"\"><td>"+inputItem+"</td><td>"+stokItem+"</td><td>"+qty+"</td><td>"+satuan+"</td><td>"+harga+"</td><td>"+signa+"</td><td>"+remove+"</td></tr>");
					
	jQuery("#signaR"+racikanId).autocomplete({"minLength":3,"showAnim":"fold","source":"'.Yii::app()->createUrl("registrasi/getSigna").'"});
}

function addRow(item){
	if(gudang !== ""){
	$.ajax({
		url: "'.Yii::app()->createAbsoluteUrl('registrasi/getObat').'",
		cache: false,
		type: "POST",
		data:"item="+item,
		success: function(msg){
			var data=$.parseJSON(msg);
			if(rowResep[data.id_item] == undefined){
				cekSubmit();
				if(!racikan){
					rowResep[data.id_item]=data.nama_item;
					$("#searchResep").val("");
					var inputItem="<input type=\"hidden\" name=\"id_item[]\" value=\""+data.id_item+"\"/>"+data.nama_item;
					var stokItem=data.stok;
					var qty="<input type=\"number\" name=\"qty"+data.id_item+"\" value=\"\" class=\"form-control\" required=\"required\" max=\""+stokItem+"\" min=\"1\"/>";
					var satuan=data.satuan;
					var harga=data.harga;
					var signa="<input type=\"text\" name=\"signa"+data.id_item+"\" id=\"signa"+data.id_item+"\" value=\"\" class=\"form-control\" required=\"required\"//>";
					var remove="<span class=\"btn btn-danger\" onclick=\"deleteRow("+data.id_item+")\"><i class=\"fa fa-times\"></i></span>";
					$("#data").append("<tr id=\"row"+data.id_item+"\"><td>"+inputItem+"</td><td>"+stokItem+"</td><td>"+qty+"</td><td>"+satuan+"</td><td>"+harga+"</td><td>"+signa+"</td><td>"+remove+"</td></tr>");
					jQuery("#signa"+data.id_item).autocomplete({"minLength":3,"showAnim":"fold","source":"'.Yii::app()->createUrl("registrasi/getSigna").'"});
				} else {
					rowResep[data.id_item]=data.nama_item;
					$("#searchResep").val("");
					var inputItem="&nbsp;&nbsp;<i class=\"fa fa-chevron-right\"></i> "+data.nama_item;
					var stokItem=data.stok;
					var qty="<input type=\"number\" name=\"id_item[R"+racikanId+"]["+data.id_item+"][qty]\"  value=\"\" class=\"form-control\" required=\"required\" max=\""+stokItem+"\" min=\"0.1\" step=\"0.1\"/>";
					var satuan=data.satuan;
					var harga=data.harga;
					var remove="<span class=\"btn btn-danger\" onclick=\"deleteRow("+data.id_item+")\"><i class=\"fa fa-times\"></i></span>";
					$("#data").append("<tr id=\"row"+data.id_item+"\" val=\""+data.id_item+"\" class=\"rowR"+racikanId+"\"><td>"+inputItem+"</td><td>"+stokItem+"</td><td>"+qty+"</td><td>"+satuan+"</td><td>"+harga+"</td><td></td><td>"+remove+"</td></tr>");
					
					jQuery("#signa"+data.id_item).autocomplete({"minLength":3,"showAnim":"fold","source":"'.Yii::app()->createUrl("registrasi/getSigna").'"});
				}
			}else{
				$("#searchResep").val("");
			}
		}
	});
} else {
		$("#searchResep").val("");
		alert("Gudang Asal Harus Dipilih !");
	}
}
', CClientScript::POS_END);
?>