<link rel="stylesheet" href="<?php echo Yii::app()->theme->baseUrl; ?>/assets/css/bootstrap-select.css">
<script src="<?php echo Yii::app()->theme->baseUrl; ?>/assets/js/bootstrap-select.js"></script>

<?php $form=$this->beginWidget('booster.widgets.TbActiveForm',array(
	'id'=>'item-transaction-form',
	'enableAjaxValidation'=>true,
	'type'=>"horizontal",
)); 
?>

<p class="help-block">Isian dengan tanda <span class="required">*</span> wajib diisi.</p>
<?php echo $form->errorSummary($model); ?>
<div class="row">
	<div class="col-sm-6">
		<?php echo $form->dateTimePickerGroup($model,'waktu_transaksi',array('widgetOptions'=>array('options'=>array('format'=>'yyyy-mm-dd hh:ii','viewFormat'=>'yyyy-mm-dd hh:ii')),'prepend'=>'<i class="glyphicon glyphicon-calendar"></i>')) ;?>
	</div>
	<div class="col-sm-6">
		<?php echo $form->dropDownListGroup($model,'id_supplier', array('append'=>'<input type="button" value="Tambah Data" onClick="openSupplier()" >','widgetOptions'=>array('data'=>CHtml::listData(Supplier::model()->findAll(),'id_supplier','nama_supplier'), 'htmlOptions'=>array('class'=>'input-large selectpicker show-tick','data-live-search'=>"true",'empty'=>'- Pilih Supplier -')))); ?>
	</div>
</div>

<div class="row">
	<div class="col-sm-6">
		<?php echo $form->dropDownListGroup($model,'gudang_tujuan', array('widgetOptions'=>array('data'=>CHtml::listData(Gudang::model()->findAll(array("condition"=>"	status_penerimaan='1'")),'id_gudang','nama_gudang'), 'htmlOptions'=>array('class'=>'input-large','empty'=>'- Choose -')))); ?>
	</div>
	<div class="col-sm-6">
		<?php echo $form->textAreaGroup($model,'catatan_transaksi', array('widgetOptions'=>array('htmlOptions'=>array('rows'=>1, 'cols'=>50, 'class'=>'span8')))); ?>
	</div>
</div>
<div class="row">
	<div class="col-sm-6">
		<label>Cari Nama Item / Kode Item</label>
		<?php
		$this->widget('zii.widgets.jui.CJuiAutoComplete',array(
			'name'=>'resep',
			'options'=>array(
				'minLength'=>3,
				'showAnim'=>'fold',
				'select'=>"js:function(event, data) {
					var item=data.item.label;
					addRow(item);
				}"
			),
			'source'=>$this->createUrl("item/getItem"),
			'htmlOptions'=>array(
				'class'=>"form-control",
				"id"=>"searchItem"
			),
		));
		?>
	</div>
</div>

<hr>
<table class="table table-striped">
	<tr>
		<th>Kode Item</th>
		<th>Nama Item</th>
		<th>Stok</th>
		<th>Exp Date</th>
		<th width="100px">Jumlah Terima</th>
		<th>Satuan</th>
		<th>Harga Terima (Rp)</th>
		<th>Harga Dasar (Rp)</th>
		<th>Discount (Rp)</th>
		<th>Sub Total (Rp)</th>
		<th></th>
	</tr>
	<tbody id="data">
		
	</tbody>
</table>
<hr>
<div class="row">
	<div class="col-sm-6">
		
	</div>
	<div class="col-sm-6">
		<?php echo $form->textAreaGroup($model,'nominal_transaksi', array('widgetOptions'=>array('htmlOptions'=>array('rows'=>1, 'cols'=>50, 'class'=>'span8',"disabled"=>true)))); ?>
	</div>
</div>

<div class="form-actions">
	<?php $this->widget('booster.widgets.TbButton', array(
			'buttonType'=>'submit',
			'context'=>'primary',
			'label'=>$model->isNewRecord ? 'Tambah Penerimaan' : 'Simpan',
			'htmlOptions'=>array(
				//"disabled"=>true,
				"id"=>"submit",
				"onClick"=>"return confirm('Apakah Penerimaan Barang Sudah Benar?');"
			),
		)); 
		echo CHtml::button('Batal', array(
            'class' => 'btn btn-primary',
            'onclick' => "history.go(-1)",
                )
        );
		?>

		</div>

<?php $this->endWidget(); ?>

<?php
Yii::app()->clientScript->registerScript('addResep', '

var rowItem=[];
var Item=[];
function deleteRow(id){
	$("#row"+id).remove();
	rowItem.splice(id, 1);
	Item.splice(id, 1);
}

function recount(){
	var total = 0;
	jQuery.each(Item,function(){
		var id = parseInt(this);
		var qty = $("#qty"+id).val();
		var harga = $("#harga"+id).val();
		var discount = $("#discount"+id).val();
		var subtotal = parseInt(qty)*(parseInt(harga)-parseInt(discount));
		total+=subtotal;
		$("#subtotal"+id).val(subtotal);
	});
	
	var grand = parseInt(total);
	$("#ItemTransaksi_nominal_transaksi").val(grand);	
}


function addRow(item){
	var gudang = $("#ItemTransaksi_gudang_tujuan").val();
	if(gudang != ""){
	$.ajax({
		url: "'.Yii::app()->createAbsoluteUrl('item/getItem').'",
		cache: false,
		type: "POST",
		data:"item="+item+"&gudang="+gudang,
		success: function(msg){
			var data=$.parseJSON(msg);
			if(rowItem[data.id_item] == undefined){
				console.log(data);
				rowItem[data.id_item]=data.nama_item;
				Item.push(data.id_item);
				$("#searchItem").val("");
				var inputItem="<input type=\"hidden\" name=\"item[]\" value=\""+data.id_item+"|"+data.id_item_satuan+"|"+data.parent+"|"+data.nilaiKeSatuanKecil+"\"/>"+data.nama_item;
				var stokItem=data.stok;
				var exp="<input type=\"date\" name=\"exp"+data.id_item+"\" value=\"\" class=\"form-control\" required=\"required\"/>";
				var qty="<input type=\"number\" name=\"qty"+data.id_item+"\" id=\"qty"+data.id_item+"\" value=\"\" class=\"form-control\" required=\"required\" min=\"1\" onchange=\"recount();\"/>";
				var satuan=data.satuan;
				var harga="<input type=\"text\" name=\"harga"+data.id_item+"\" id=\"harga"+data.id_item+"\" value=\""+data.hargaPerSatuanBesar+"\" class=\"form-control\" required=\"required\" onchange=\"recount();\"/>";
				var hargaDasar="<input type=\"text\" name=\"dasar"+data.id_item+"\" id=\"dasar"+data.item_id+"\" value=\""+data.hargaPerSatuanBesar+"\" class=\"form-control\" required=\"required\" readonly/>";
				var discount="<input type=\"number\" name=\"discount"+data.id_item+"\" id=\"discount"+data.id_item+"\" value=\"0\" class=\"form-control\" required=\"required\" min=\"0\" onchange=\"recount();\"/>";
				var subtotal="<input type=\"number\" name=\"subtotal"+data.id_item+"\" id=\"subtotal"+data.id_item+"\" value=\"\" class=\"form-control\" required=\"required\" readonly/>";
				var remove="<span class=\"btn btn-danger\" onclick=\"deleteRow("+data.id_item+")\"><i class=\"fa fa-times\"></i></span>";
				$("#data").append("<tr id=\"row"+data.id_item+"\"><td>"+data.kode_item+"</td><td>"+inputItem+"</td><td>"+stokItem+"</td><td>"+exp+"</td><td>"+qty+"</td><td>"+satuan+"</td><td>"+harga+"</td><td>"+hargaDasar+"</td><td>"+discount+"</td><td>"+subtotal+"</td><td>"+remove+"</td></tr>");
				
			}else{
				$("#searchItem").val("");
			}
		}
	});
	}else{
		$("#searchItem").val("");
		alert("Silahkan Pilih Gudang Penerimaan Barang !");
	}
}

$("#ItemTransaction_warehouse_destination_id").change(function(){
	jQuery.each(Item,function(){
		var id = parseInt(this);
		deleteRow(id);
	});
});

function openSupplier(){
	window.open("'.Yii::app()->createUrl(Yii::app()->controller->module->id.'/supplier/addDirect').'","","width=800,height=600");
}

function reloadData(id){
	getVal(id,"id_supplier","#ItemTransaksi_id_supplier","supplier/getData");
}

function getVal(value,param,id,url){	
	$.ajax({
		url: "'.Yii::app()->createAbsoluteUrl(Yii::app()->controller->module->id).'/"+url,
		cache: false,
		type: "POST",
		data:"ItemTransaksi["+param+"]="+value,
		success: function(msg){
			$(id).html("");
			$(id).html(msg);
			$(id).selectpicker("refresh");
		}
	});
}
', CClientScript::POS_END);
?>
