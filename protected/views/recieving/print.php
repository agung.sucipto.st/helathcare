<div class="container">
<center>
	<h2>Bukti Penerimaan Barang</h2>
</center>
<div class="row">
			<div class="col-sm-6">
			<?php $this->widget('booster.widgets.TbDetailView',array(
			'data'=>$model,
			'attributes'=>array(
				'no_transaksi',
				'waktu_transaksi',
				array(
					"label"=>"Supplier",
					"value"=>$model->idSupplier->nama_supplier
				)
			),
			)); ?>
			</div>
			<div class="col-sm-6">
			<?php $this->widget('booster.widgets.TbDetailView',array(
			'data'=>$model,
			'attributes'=>array(
				array(
					"label"=>"Destination",
					"value"=>$model->gudangTujuan->nama_gudang
				),
				'catatan_transaksi',
				array(
					"label"=>"User",
					"value"=>$model->userCreate->username
				)
			),
			)); ?>
			</div>
		</div>
		<hr>
		<table class="table table-bordered">
			<tr>
				<th class="text-center">Kode Item</th>
				<th class="text-center">Nama Item</th>
				<th class="text-center">Exp Date</th>
				<th class="text-center">Jumlah Terima</th>
				<th class="text-center">Satuan</th>
				<th class="text-center">Harga Terima (Rp)</th>
				<th class="text-center">Discount (Rp)</th>
				<th class="text-center">Sub Total (Rp)</th>
			</tr>
			<tbody id="data">
				<?php
				$list=DaftarItemTransaksi::model()->findAll(array("condition"=>"id_item_transaksi='$model->id_item_transaksi'"));
				$subtotal=0;
				foreach($list as $row){
					$subtotal+=$row->jumlah_satuan_besar*$row->harga_transaksi;
					echo'
					<tr>
						<td>'.$row->idItem->kode_item.'</td>
						<td>'.$row->idItem->nama_item.'</td>
						<td>'.$row->tanggal_expired.'</td>
						<td>'.$row->jumlah_satuan_besar.'</td>
						<td>'.$row->idItemSatuanBesar->idSatuan->alias_satuan.' ('.(($row->idItemSatuanBesar->parent_id_item_satuan!='')?$row->idItemSatuanBesar->nilai_satuan_konversi.' '.$row->idItemSatuanBesar->parentIdItemSatuan->idSatuan->alias_satuan:$row->idItemSatuanBesar->nilai_satuan_konversi.' '.$row->idItemSatuanBesar->idSatuan->alias_satuan).')</td>
						<td class="text-right">'.number_format(($row->harga_transaksi+$row->discount),0).'</td>
						<td class="text-right">'.number_format(($row->discount*$row->jumlah_satuan_besar)).'</td>
						<td class="text-right">'.number_format(($row->jumlah_satuan_besar*$row->harga_transaksi),0).'</td>
					</tr>';
				}
				?>
				<tr>
					<th colspan="7" class="text-right">Total</th>
					<th class="text-right"><?=number_format($subtotal,0);?></th>
				</tr>
				<tr>
					<th colspan="7" class="text-right">Grand Total</th>
					<th class="text-right"><?=number_format(($subtotal),0);?></th>
				</tr>
			</tbody>
		</table>
<br/>
<div class="row">
	<div class="col-xs-4 text-center">
	Supplier
	<br/>
	<br/>
	<br/>
	<br/>
	<br/>
	(______________)
	</div>
	<div class="col-xs-4"></div>
	<div class="col-xs-4 text-center">
	Penerima
	<br/>
	<br/>
	<br/>
	<br/>
	<br/>
	(<?=$model->userCreate->username;?>)</div>
</div>
</div>
<?php
Yii::app()->clientScript->registerScript('validatex', '
window.print();
', CClientScript::POS_END);
?>