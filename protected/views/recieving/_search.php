<?php $form=$this->beginWidget('booster.widgets.TbActiveForm',array(
	'action'=>Yii::app()->createUrl($this->route),
	'method'=>'get',
	'type'=>'horizontal',
)); ?>

<div class="row">
	<div class="col-sm-6">
		<?php echo $form->dateRangeGroup(
			$model,
			'waktu_transaksi',
			array(
				'label'=>'Periode Penerimaan',
				'wrapperHtmlOptions' => array(
					'class' => '',
				),
				'prepend' => '<i class="glyphicon glyphicon-calendar"></i>'
			)
		); ?>
	</div>
	<div class="col-sm-6">
		<?php echo $form->textFieldGroup($model,'no_transaksi',array('widgetOptions'=>array('htmlOptions'=>array('class'=>'span5','maxlength'=>45)))); ?>
	</div>
</div>
<div class="row">
	<div class="col-sm-6">
		<?php echo $form->dropDownListGroup($model,'gudang_tujuan', array('widgetOptions'=>array('data'=>CHtml::listData(Gudang::model()->findAll(array("condition"=>"	status_penerimaan='1'")),'id_gudang','nama_gudang'), 'htmlOptions'=>array('class'=>'input-large','empty'=>'- Choose -')))); ?>
	</div>
	<div class="col-sm-6">
		<?php echo $form->dropDownListGroup($model,'id_supplier', array('widgetOptions'=>array('data'=>CHtml::listData(Supplier::model()->findAll(),'id_supplier','nama_supplier'), 'htmlOptions'=>array('class'=>'input-large','empty'=>'- Choose -')))); ?>
	</div>
</div>

		
	<div class="form-actions">
		<?php $this->widget('booster.widgets.TbButton', array(
			'buttonType' => 'submit',
			'context'=>'primary',
			'label'=>'Search',
		)); ?>
	</div>

<?php $this->endWidget(); ?>
