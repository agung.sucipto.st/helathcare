<?php
$this->breadcrumbs=array(
	'Kelola Penerimaan Barang'=>array('recieving/index'),
	'Detail Penerimaan Barang',
);

$this->title=array(
	'title'=>'Detail Penerimaan Barang',
	'deskripsi'=>'Untuk Melihat Detail Penerimaan Barang'
);
?>

<div class="panel panel-default">
	<div class="panel-heading">
		<h3 class="panel-title pull-left">Detail</h3>
		<a href="<?php echo Yii::app()->createUrl(Yii::app()->controller->id);?>" class="btn btn-primary btn-xs pull-right">
			<i class="fa fa-arrow-circle-left"></i>
			<span>Kembali</span>
		</a>
		<button onclick="cetak(<?=$model->id_item_transaksi;?>);" class="btn btn-primary btn-xs pull-right">
			<i class="fa fa-print"></i>
			<span>Print Penerimaan</span>
		</button>
		<a href="<?php echo Yii::app()->createUrl(Yii::app()->controller->id.'/create');?>" class="btn btn-primary btn-xs pull-right">
			<i class="fa fa-copy"></i>
			<span>Tambah Data</span>
		</a>
		<div style="clear:both"></div>
	</div>
	<div class="panel-body">		
		<div class="row">
			<div class="col-sm-6">
			<?php $this->widget('booster.widgets.TbDetailView',array(
			'data'=>$model,
			'attributes'=>array(
				'no_transaksi',
				'waktu_transaksi',
				array(
					"label"=>"Supplier",
					"value"=>$model->idSupplier->nama_supplier
				)
			),
			)); ?>
			</div>
			<div class="col-sm-6">
			<?php $this->widget('booster.widgets.TbDetailView',array(
			'data'=>$model,
			'attributes'=>array(
				array(
					"label"=>"Destination",
					"value"=>$model->gudangTujuan->nama_gudang
				),
				'catatan_transaksi',
				array(
					"label"=>"User",
					"value"=>$model->userCreate->username
				)
			),
			)); ?>
			</div>
		</div>
		<hr>
		<table class="table table-bordered">
			<tr>
				<th class="text-center">Kode Item</th>
				<th class="text-center">Nama Item</th>
				<th class="text-center">Exp Date</th>
				<th class="text-center">Jumlah Terima</th>
				<th class="text-center">Satuan</th>
				<th class="text-center">Harga Terima (Rp)</th>
				<th class="text-center">Discount (Rp)</th>
				<th class="text-center">Sub Total (Rp)</th>
			</tr>
			<tbody id="data">
				<?php
				$list=DaftarItemTransaksi::model()->findAll(array("condition"=>"id_item_transaksi='$model->id_item_transaksi'"));
				$subtotal=0;
				foreach($list as $row){
					$subtotal+=$row->jumlah_satuan_besar*$row->harga_transaksi;
					echo'
					<tr>
						<td>'.$row->idItem->kode_item.'</td>
						<td>'.$row->idItem->nama_item.'</td>
						<td>'.$row->tanggal_expired.'</td>
						<td>'.$row->jumlah_satuan_besar.'</td>
						<td>'.$row->idItemSatuanBesar->idSatuan->alias_satuan.' ('.(($row->idItemSatuanBesar->parent_id_item_satuan!='')?$row->idItemSatuanBesar->nilai_satuan_konversi.' '.$row->idItemSatuanBesar->parentIdItemSatuan->idSatuan->alias_satuan:$row->idItemSatuanBesar->nilai_satuan_konversi.' '.$row->idItemSatuanBesar->idSatuan->alias_satuan).')</td>
						<td class="text-right">'.number_format(($row->harga_transaksi+$row->discount),0).'</td>
						<td class="text-right">'.number_format(($row->discount*$row->jumlah_satuan_besar)).'</td>
						<td class="text-right">'.number_format(($row->jumlah_satuan_besar*$row->harga_transaksi),0).'</td>
					</tr>';
				}
				?>
				<tr>
					<th colspan="7" class="text-right">Total</th>
					<th class="text-right"><?=number_format($subtotal,0);?></th>
				</tr>
				<tr>
					<th colspan="7" class="text-right">Grand Total</th>
					<th class="text-right"><?=number_format(($subtotal),0);?></th>
				</tr>
			</tbody>
		</table>
	</div>
</div>

<?php
Yii::app()->clientScript->registerScript('validatex', '
function cetak(id){
	var left = (screen.width/2)-(900/2);
	var top = (screen.height/2)-(500/2);
	window.open("'.Yii::app()->createUrl('recieving/print').'/id/"+id,"","width=900,height=500,top="+top+",left="+left);
}
', CClientScript::POS_END);
?>