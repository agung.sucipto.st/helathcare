<?php
$this->breadcrumbs=array(
	'Kelola Penerimaan Barang',
);
$this->title=array(
	'title'=>'Kelola Penerimaan Barang',
	'deskripsi'=>'Untuk Mengelola Penerimaan Barang'
);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	$('.import-form').hide();
	return false;
});
$('.btn-cancel').click(function(){
	$('.import-form').toggle();
	return false;
});
$('.import-button').click(function(){
	$('.import-form').toggle();
	$('.search-form').hide();
	return false;
});
$('.search-form form').submit(function(){
	$.fn.yiiGridView.update('item-transaction-grid', {
		data: $(this).serialize()
	});
	return false;
});

");
?>

<div class="panel panel-default">
	<div class="panel-heading">
		<h3 class="panel-title pull-left">Data</h3>
		<a href="<?php echo Yii::app()->createUrl(Yii::app()->controller->id.'/create');?>" class="btn btn-primary btn-xs pull-right">
			<i class="fa fa-copy"></i>
			<span>Tambah Data</span>
		</a>
		<a href="#" class="btn btn-primary btn-xs pull-right search-button">
			<i class="fa fa-search"></i>
			<span>Search</span>
		</a>
		<div style="clear:both"></div>
	</div>
	<div class="panel-body">
		<div class="search-form">
			<?php $this->renderPartial('_search',array(
			'model'=>$model,
		)); ?>
		</div><!-- search-form -->
		
		<style>
		.pagination li.selected a{
			background:rgb(235, 235, 236);
		}
		</style>
		
		
		<?php
		if(Yii::app()->user->hasFlash('success')){
			echo'
			<div class="alert alert-success">
				<button type="button" class="close" data-dismiss="alert">&times;</button>
				'.Yii::app()->user->getFlash('success').'
			</div>';
		}
		?>
							
		<div id="example-2_wrapper" class="dataTables_wrapper form-inline dt-bootstrap no-footer">
		
		<?php 
		// put this somewhere on top
		$pageSize=Yii::app()->user->getState('pageSize',Yii::app()->params['defaultPageSize']); ?>		
		<?php		
		$this->widget('booster.widgets.TbExtendedGridView', array(
			'id'=>'item-transaction-grid',
			'type' => 'striped',
			'dataProvider' => $model->search(),
			//'filter'=>$model,
			'summaryText'=>'Menampilkan {start}-{end} dari {count} hasil.',
			'responsiveTable' => true,
			'enablePagination' => true,
			'pager' => array(
				'htmlOptions'=>array(
					'class'=>'pagination'
				),
				'maxButtonCount' => 5,
				'cssFile' => true,
				'header' => false,
				'firstPageLabel' => '<<',
				'prevPageLabel' => '<',
				'nextPageLabel' => '>',
				'lastPageLabel' => '>>',
			),
			'columns'=>array(
					array(
						"header"=>$model->getAttributeLabel('waktu_transaksi'),
						"value"=>'Lib::dateIndShortMonth($data->waktu_transaksi)',
						"name"=>'waktu_transaksi',
					),
					array(
						"header"=>$model->getAttributeLabel('gudang_tujuan'),
						"value"=>'$data->gudangTujuan->nama_gudang',
						"name"=>'gudang_tujuan',
					),
					array(
						"header"=>"Supplier",
						"value"=>'$data->idSupplier->nama_supplier',
						"name"=>'id_supplier',
					),
					array(
						"header"=>$model->getAttributeLabel('no_transaksi'),
						"value"=>function($data){
							if(Lib::accessBy("recieving","print")==true){
								return '<a href="#" onclick="cetak('.$data->id_item_transaksi.');">'.$data->no_transaksi.'</a>';
							}else{
								return $data->no_transaksi;
							}
						},
						"name"=>'no_transaksi',
						"type"=>'raw',
					),
					array(
						"header"=>"Item",
						"type"=>"raw",
						"value"=>function($data){
							$res='<ul>';
							foreach($data->daftarItemTransaksis as $row){
								$res.='<li>'.$row->idItem->nama_item.', '.$row->jumlah_transaksi.' '.$row->idItemSatuanBesar->idSatuan->alias_satuan.'</li>';
							}
							$res.='</ul>';
							return $res;
						}
					),
					array(
						"header"=>"Nominal",
						"type"=>"raw",
						"value"=>'number_format($data->nominal_transaksi,0)',
						"name"=>'nominal_transaksi',
					),
					array(
						"header"=>"Penerima",
						"type"=>"raw",
						"value"=>'$data->userCreate->username."<br/>".Lib::dateIndShortMonth($data->waktu_transaksi,false)',
						"name"=>'user_create',
					),
					array(
						'class'=>'booster.widgets.TbButtonColumn',
						 'deleteConfirmation'=>'Anda yakin akan menhapus data?',
						'template'=>'{view}{delete}',
						'buttons'=>array
						(
							'view' => array
							(
								'label'=>'View',
								'icon'=>'search',
								'options'=>array(
									'class'=>'btn btn-default btn-xs',
								),
							),
							'delete' => array
							(
								'label'=>'Delete',
								'icon'=>'trash',
								'visible'=>'$data->xRecieve()',
								'options'=>array(
									'class'=>'btn btn-default btn-xs delete',
								),
							)
						),
						'header'=>CHtml::dropDownList('pageSize',$pageSize,array(10=>10,20=>20,50=>50,100=>100,200=>200,500=>500,1000=>1000),array(
							'onchange'=>"$.fn.yiiGridView.update('item-transaction-grid',{ data:{pageSize: $(this).val() }})",
						)),
					),
			),
		));
		?>
		</div>
	</div>
</div>

<?php
Yii::app()->clientScript->registerScript('validatex', '
function cetak(id){
	var left = (screen.width/2)-(900/2);
	var top = (screen.height/2)-(500/2);
	window.open("'.Yii::app()->createUrl('recieving/print').'/id/"+id,"","width=900,height=500,top="+top+",left="+left);
}
', CClientScript::POS_END);
?>