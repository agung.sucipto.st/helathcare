<?php
$this->breadcrumbs=array(
	'Kelola Terima Retur'=>array('recieveRetur/index'),
	'Detail Terima Retur',
);

$this->title=array(
	'title'=>'Detail Terima Retur',
	'deskripsi'=>'Untuk Melihat Detail Terima Retur'
);
?>

<div class="panel panel-default">
	<div class="panel-heading">
		<h3 class="panel-title pull-left">Detail</h3>
		<a href="<?php echo Yii::app()->createUrl(Yii::app()->controller->id);?>" class="btn btn-primary btn-xs pull-right">
			<i class="fa fa-arrow-circle-left"></i>
			<span>Kembali</span>
		</a>
		<?php
		if(Lib::accessBy("returSupplier","print")==true){
		?>
		<button onclick="cetak(<?=$model->item_transaction_id;?>);" class="btn btn-primary btn-xs pull-right">
			<i class="fa fa-print"></i>
			<span>Print Terima Retur</span>
		</button>
		<?php
		}
		?>
		<a href="<?php echo Yii::app()->createUrl(Yii::app()->controller->id.'/list');?>" class="btn btn-primary btn-xs pull-right">
			<i class="fa fa-copy"></i>
			<span>Tambah Data</span>
		</a>
		<div style="clear:both"></div>
	</div>
	<div class="panel-body">		
		<div class="row">
			<div class="col-sm-6">
			<?php $this->widget('booster.widgets.TbDetailView',array(
			'data'=>$model,
			'attributes'=>array(
				'item_transaction_code',
				'item_transaction_time',
				array(
					"label"=>"Kode Retur",
					"value"=>$model->linkTransaction->item_transaction_code
				),
				array(
					"label"=>"Supplier",
					"value"=>$model->supplier->supplier_name
				)
			),
			)); ?>
			</div>
			<div class="col-sm-6">
			<?php $this->widget('booster.widgets.TbDetailView',array(
			'data'=>$model,
			'attributes'=>array(
				array(
					"label"=>"Gudang Tujuan",
					"value"=>$model->warehouseDestination->warehouse_name
				),
				'item_transaction_note',
				array(
					"label"=>"User",
					"value"=>$model->userCreate->username
				)
			),
			)); ?>
			</div>
		</div>
		<hr>
		<table class="table table-bordered">
			<tr>
				<th class="text-center">Kode Item</th>
				<th class="text-center">Nama Item</th>
				<th class="text-center">Tanggal Expire</th>
				<th class="text-center">Jumlah Terima Retur</th>
				<th class="text-center">Satuan</th>
			</tr>
			<tbody id="data">
				<?php
				$list=ItemTransactionList::model()->findAll(array("condition"=>"item_transaction_id='$model->item_transaction_id'"));
				$subtotal=0;
				foreach($list as $row){
					$subtotal+=$row->transaction_amount*$row->transaction_price_per_unit;
					echo'
					<tr>
						<td>'.$row->item->code.'</td>
						<td>'.$row->item->item_name.'</td>
						<td>'.$row->expire_date.'</td>
						<td>'.$row->big_amount.'</td>
						<td>'.$row->itemBigUnit->unit->unit_alias.' ('.(($row->itemBigUnit->parent_item_unit_id!='')?$row->itemBigUnit->unit_amount.' '.$row->itemBigUnit->parentItemUnit->unit->unit_alias:$row->itemBigUnit->unit_amount.' '.$row->itemBigUnit->unit->unit_alias).')</td>
						
					</tr>';
				}
				?>
			</tbody>
		</table>
	</div>
</div>

<?php
Yii::app()->clientScript->registerScript('validatex', '
function cetak(id){
	var left = (screen.width/2)-(900/2);
	var top = (screen.height/2)-(500/2);
	window.open("'.Yii::app()->createUrl('recieveRetur/print').'/id/"+id,"","width=900,height=500,top="+top+",left="+left);
}
', CClientScript::POS_END);
?>