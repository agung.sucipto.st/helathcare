<?php
$this->breadcrumbs=array(
	'Kelola Daftar Tagihan'=>array('daftarTagihan/index'),
	'Detail Daftar Tagihan',
);

$this->title=array(
	'title'=>'Detail Daftar Tagihan',
	'deskripsi'=>'Untuk Melihat Detail Daftar Tagihan'
);
?>

<div class="panel panel-default">
	<div class="panel-heading">
		<h3 class="panel-title pull-left">Detail</h3>
		<a href="<?php echo Yii::app()->createUrl(Yii::app()->controller->id);?>" class="btn btn-primary btn-xs pull-right" style="display:<?php echo $visibleEdit;?>">
			<i class="fa fa-arrow-circle-left"></i>
			<span>Kembali</span>
		</a>
		<a href="<?php echo Yii::app()->createUrl(Yii::app()->controller->id.'/create');?>" class="btn btn-primary btn-xs pull-right" style="display:<?php echo ((Lib::accessBy('DaftarTagihan','create')==true)?"block":"");?>">
			<i class="fa fa-copy"></i>
			<span>Tambah Data</span>
		</a>
		<a href="<?php echo Yii::app()->createUrl(Yii::app()->controller->id.'/update/id/'.$_GET['id']);?>" class="btn btn-primary btn-xs pull-right" style="display:<?php echo ((Lib::accessBy('DaftarTagihan','update')==true)?"block":"");?>">
			<i class="fa fa-edit"></i>
			<span>Edit Data</span>
		</a>
		<div style="clear:both"></div>
	</div>
	<div class="panel-body">		
		<div id="example-2_wrapper" class="dataTables_wrapper form-inline dt-bootstrap no-footer">
			<?php $this->widget('booster.widgets.TbDetailView',array(
			'data'=>$model,
			'attributes'=>array(
							'id_daftar_tagihan',
				'id_tagihan',
				'id_registrasi',
				'id_penjamin',
				'id_jenis_komponen_tagihan',
				'id_tindakan_pasien',
				'id_peralatan_pasien',
				'id_item_pasien',
				'id_visite_dokter_pasien',
				'id_radiology_pasien_list',
				'id_laboratorium_pasien_list',
				'id_bed_pasien',
				'id_ruangan_pasien',
				'id_administrasi_pasien',
				'deskripsi_komponen',
				'waktu_daftar_tagihan',
				'harga',
				'jumlah',
				'jenis_discount',
				'discount',
				'jumlah_bayar',
				'jumlah_jaminan',
				'tarif',
				'tarif_orig',
				'gabungkan',
				'id_registrasi_gabung',
				'konfirmasi',
				'user_create',
				'user_update',
				'user_final',
				'user_confirm',
				'time_create',
				'time_update',
				'time_final',
				'time_confirm',
			),
			)); ?>
		</div>
	</div>
</div>
