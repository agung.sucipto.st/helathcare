<?php
$this->breadcrumbs=array(
	'Kelola Daftar Tagihan',
);
$this->title=array(
	'title'=>'Kelola Daftar Tagihan',
	'deskripsi'=>'Untuk Mengelola Daftar Tagihan'
);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	$('.import-form').hide();
	return false;
});
$('.btn-cancel').click(function(){
	$('.import-form').toggle();
	return false;
});
$('.import-button').click(function(){
	$('.import-form').toggle();
	$('.search-form').hide();
	return false;
});
$('.search-form form').submit(function(){
	$.fn.yiiGridView.update('daftar-tagihan-grid', {
		data: $(this).serialize()
	});
	return false;
});

");
?>

<div class="panel panel-default">
	<div class="panel-heading">
		<h3 class="panel-title pull-left">Data</h3>
		<a href="<?php echo Yii::app()->createUrl(Yii::app()->controller->id.'/create');?>" class="btn btn-primary btn-xs pull-right" style="display:<?php echo ((Lib::accessBy('DaftarTagihan','create')==true)?"block":"");?>">
			<i class="fa fa-copy"></i>
			<span>Tambah Data</span>
		</a>
		<a href="#" class="btn btn-primary btn-xs pull-right search-button">
			<i class="fa fa-search"></i>
			<span>Search</span>
		</a>
		<div style="clear:both"></div>
	</div>
	<div class="panel-body">
		<div class="search-form" style="display:none">
			<?php $this->renderPartial('_search',array(
			'model'=>$model,
		)); ?>
		</div><!-- search-form -->
		
		<style>
		.pagination li.selected a{
			background:rgb(235, 235, 236);
		}
		</style>
		
		
		<?php
		if(Yii::app()->user->hasFlash('success')){
			echo'
			<div class="alert alert-success">
				<button type="button" class="close" data-dismiss="alert">&times;</button>
				'.Yii::app()->user->getFlash('success').'
			</div>';
		}
		?>
							
		<div id="example-2_wrapper" class="dataTables_wrapper form-inline dt-bootstrap no-footer">
		
		<?php 
		// put this somewhere on top
		$pageSize=Yii::app()->user->getState('pageSize',Yii::app()->params['defaultPageSize']); ?>		
		<?php		$this->widget('booster.widgets.TbExtendedGridView', array(
			'id'=>'daftar-tagihan-grid',
			'type' => 'striped',
			'dataProvider' => $model->search(),
			'filter'=>$model,
			'summaryText'=>'Menampilkan {start}-{end} dari {count} hasil.',
			'selectableRows' => 2,
			'responsiveTable' => true,
			'enablePagination' => true,
			'pager' => array(
				'htmlOptions'=>array(
					'class'=>'pagination'
				),
				'maxButtonCount' => 5,
				'cssFile' => true,
				'header' => false,
				'firstPageLabel' => '<<',
				'prevPageLabel' => '<',
				'nextPageLabel' => '>',
				'lastPageLabel' => '>>',
			),
			'columns'=>array(
										'id_daftar_tagihan',
					'id_tagihan',
					'id_registrasi',
					'id_penjamin',
					'id_jenis_komponen_tagihan',
					'id_tindakan_pasien',
		/*
					'id_peralatan_pasien',
					'id_item_pasien',
					'id_visite_dokter_pasien',
					'id_radiology_pasien_list',
					'id_laboratorium_pasien_list',
					'id_bed_pasien',
					'id_ruangan_pasien',
					'id_administrasi_pasien',
					'deskripsi_komponen',
					'waktu_daftar_tagihan',
					'harga',
					'jumlah',
					'jenis_discount',
					'discount',
					'jumlah_bayar',
					'jumlah_jaminan',
					'tarif',
					'tarif_orig',
					'gabungkan',
					'id_registrasi_gabung',
					'konfirmasi',
					'user_create',
					'user_update',
					'user_final',
					'user_confirm',
					'time_create',
					'time_update',
					'time_final',
					'time_confirm',
		*/
					array(
						'class'=>'booster.widgets.TbButtonColumn',
						 'deleteConfirmation'=>'Anda yakin akan menhapus data?',
						'template'=>'{view}{update}{delete}',
						'buttons'=>array
						(
							'view' => array
							(
								'label'=>'View',
								'icon'=>'search',
								'visible'=>'$data->getAllowView()',
								'options'=>array(
									'class'=>'btn btn-default btn-xs',
								),
							),
							'update' => array
							(
								'label'=>'Update',
								'icon'=>'pencil',
								'visible'=>'$data->getAllowUpdate()',
								'options'=>array(
									'class'=>'btn btn-default btn-xs',
								),
							),
							'delete' => array
							(
								'label'=>'Delete',
								'icon'=>'trash',
								'visible'=>'$data->getAllowDelete($data->id_daftar_tagihan)',
								'options'=>array(
									'class'=>'btn btn-default btn-xs delete',
								),
							)
						),
						'header'=>CHtml::dropDownList('pageSize',$pageSize,array(10=>10,20=>20,50=>50,100=>100,200=>200,500=>500,1000=>1000),array(
							'onchange'=>"$.fn.yiiGridView.update('daftar-tagihan-grid',{ data:{pageSize: $(this).val() }})",
						)),
					),
			),
		));
		?>
		</div>
	</div>
</div>