<?php
$this->breadcrumbs=array(
	'Kelola Transaction'=>array('index'),
	'Detail Transaction',
);

$this->title=array(
	'title'=>'Detail Transaction',
	'deskripsi'=>'Untuk Melihat Detail Transaction'
);
?>

<div class="panel panel-default">
	<div class="panel-heading">
		<h3 class="panel-title pull-left">Detail</h3>
		<a href="<?php echo Yii::app()->createUrl(Yii::app()->controller->id);?>" class="btn btn-primary btn-xs pull-right">
			<i class="fa fa-arrow-circle-left"></i>
			<span>Kembali</span>
		</a>
		<a href="<?php echo Yii::app()->createUrl(Yii::app()->controller->id.'/create');?>" class="btn btn-primary btn-xs pull-right">
			<i class="fa fa-copy"></i>
			<span>Tambah Data</span>
		</a>
		<a href="<?php echo Yii::app()->createUrl(Yii::app()->controller->id.'/update/id/'.$_GET['id']);?>" class="btn btn-primary btn-xs pull-right">
			<i class="fa fa-edit"></i>
			<span>Edit Data</span>
		</a>
		<div style="clear:both"></div>
	</div>
	<div class="panel-body">		
		<div id="example-2_wrapper" class="dataTables_wrapper form-inline dt-bootstrap no-footer">
			<?php $this->widget('booster.widgets.TbDetailView',array(
			'data'=>$model,
			'attributes'=>array(
							'transaction_id',
				'type_id',
				'transaction_time',
				'supplier_id',
				'member_id',
				'transaction_code',
				'transaction_ref_number',
				'item_transaction_id',
				'transaction_note',
				'transaction_total',
				'transaction_status',
				'transaction_group',
				'link_transaction',
				'pay_type',
				'pay_destinantion',
				'payed',
				'user_create',
				'user_update',
				'time_create',
				'time_update',
			),
			)); ?>
		</div>
	</div>
</div>
