<div class="view">
		<b><?php echo CHtml::encode($data->getAttributeLabel('transaction_id')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->transaction_id),array('view','id'=>$data->transaction_id)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('type_id')); ?>:</b>
	<?php echo CHtml::encode($data->type_id); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('transaction_time')); ?>:</b>
	<?php echo CHtml::encode($data->transaction_time); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('supplier_id')); ?>:</b>
	<?php echo CHtml::encode($data->supplier_id); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('member_id')); ?>:</b>
	<?php echo CHtml::encode($data->member_id); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('transaction_code')); ?>:</b>
	<?php echo CHtml::encode($data->transaction_code); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('transaction_ref_number')); ?>:</b>
	<?php echo CHtml::encode($data->transaction_ref_number); ?>
	<br />

	<?php /*
	<b><?php echo CHtml::encode($data->getAttributeLabel('item_transaction_id')); ?>:</b>
	<?php echo CHtml::encode($data->item_transaction_id); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('transaction_note')); ?>:</b>
	<?php echo CHtml::encode($data->transaction_note); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('transaction_total')); ?>:</b>
	<?php echo CHtml::encode($data->transaction_total); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('transaction_status')); ?>:</b>
	<?php echo CHtml::encode($data->transaction_status); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('transaction_group')); ?>:</b>
	<?php echo CHtml::encode($data->transaction_group); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('link_transaction')); ?>:</b>
	<?php echo CHtml::encode($data->link_transaction); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('pay_type')); ?>:</b>
	<?php echo CHtml::encode($data->pay_type); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('pay_destinantion')); ?>:</b>
	<?php echo CHtml::encode($data->pay_destinantion); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('payed')); ?>:</b>
	<?php echo CHtml::encode($data->payed); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('user_create')); ?>:</b>
	<?php echo CHtml::encode($data->user_create); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('user_update')); ?>:</b>
	<?php echo CHtml::encode($data->user_update); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('time_create')); ?>:</b>
	<?php echo CHtml::encode($data->time_create); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('time_update')); ?>:</b>
	<?php echo CHtml::encode($data->time_update); ?>
	<br />

	*/ ?>
</div>