<?php
$this->breadcrumbs=array(
	'Kelola Personal'=>array('personal/index'),
	'Detail Personal',
);

$this->title=array(
	'title'=>'Detail Personal',
	'deskripsi'=>'Untuk Melihat Detail Personal'
);
$visible='';

$akses = UserAccess::model()->find(array("with"=>array("access"),'condition'=>"id_user='".Yii::app()->user->id."' and access.access_controller='Personal' and access.access_action='create'"));
if(!empty($akses)){
	$visible='block';
} else {
	$visible='none';
}

$visibleEdit='';

$akses = UserAccess::model()->find(array("with"=>array("access"),'condition'=>"id_user='".Yii::app()->user->id."' and access.access_controller='Personal' and access.access_action='update'"));
if(!empty($akses)){
	$visibleEdit='block';
} else {
	$visibleEdit='none';
}
?>

<div class="panel panel-default">
	<div class="panel-heading">
		<h3 class="panel-title pull-left">Detail</h3>
		<a href="<?php echo Yii::app()->createUrl(Yii::app()->controller->id);?>" class="btn btn-primary btn-xs pull-right" style="display:<?php echo $visibleEdit;?>">
			<i class="fa fa-arrow-circle-left"></i>
			<span>Kembali</span>
		</a>
		<a href="<?php echo Yii::app()->createUrl(Yii::app()->controller->id.'/create');?>" class="btn btn-primary btn-xs pull-right" style="display:<?php echo $visible;?>">
			<i class="fa fa-copy"></i>
			<span>Tambah Data</span>
		</a>
		<a href="<?php echo Yii::app()->createUrl(Yii::app()->controller->id.'/update/id/'.$_GET['id']);?>" class="btn btn-primary btn-xs pull-right">
			<i class="fa fa-edit"></i>
			<span>Edit Data</span>
		</a>
		<div style="clear:both"></div>
	</div>
	<div class="panel-body">		
		<div id="example-2_wrapper" class="dataTables_wrapper form-inline dt-bootstrap no-footer">
			<?php $this->widget('booster.widgets.TbDetailView',array(
			'data'=>$model,
			'attributes'=>array(
							'id_personal',
				'nama_lengkap',
				'jenis_kelamin',
				'tempat_lahir',
				'tanggal_lahir',
				'status_perkawinan',
				'id_agama',
				'id_pendidikan',
				'id_pekerjaan',
				'alamat_domisili',
				'alamat_sementara',
				'id_negara',
				'id_provinsi',
				'id_kabupaten',
				'id_kecamatan',
			),
			)); ?>
		</div>
	</div>
</div>
