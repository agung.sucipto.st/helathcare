<div class="box box-primary">
	<div class="box-body">
		<h4 class="box-title">Pendaftaran Tidak Dapat Dibatalkan !</h4>
		<span class="text-danger">Sudah Terjadi Transaksi Penjualan Resep / Pembayaran Tagihan !</span>
		<hr>
		
		<?php 
		echo CHtml::button('Batal', array(
			'class' => 'btn btn-primary',
			'onclick' => "window.self.close();",
				)
		);
		?>
	</div>
</div>