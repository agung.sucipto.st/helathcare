<div class="box box-primary">
	<div class="box-body">
		<div class="row">
			<div class="col-sm-1">
				<?php
				$img='';
				$icon='';
				if($model->idPersonal->jenis_kelamin=='Perempuan'){
					$img=Yii::app()->theme->baseUrl.'/assets/images/female.png';
					$icon='<i class="fa fa-venus" style="color:red;font-weight:bold;"></i>';
				}else{
					$img=Yii::app()->theme->baseUrl.'/assets/images/male.png';
					$icon='<i class="fa fa-mars" style="color:blue;font-weight:bold;"></i>';
				}	
				echo'<img src="'.$img.'" class="img-responsive"/>';
				?>
			</div>
			<div class="col-sm-7">
				<div class="row">
					<div class="col-sm-12" style="font-size:25px;font-weight:bold;">
					<?php
						echo Lib::MRN($model->id_pasien).' - '.$icon.strtoupper($model->idPersonal->nama_lengkap).", ".$model->panggilan;	
					?>
					</div>
				</div>
				<div class="row">
					<div class="col-sm-12">
					<?php
						echo $model->idPersonal->jenis_kelamin.' / '.Lib::dateInd($model->idPersonal->tanggal_lahir,false).", ".Lib::umurDetail($model->idPersonal->tanggal_lahir);	
					?>
					<br/>
					<span class="text-danger text-bold">Alergi</span> : <?=$model->idPersonal->alergi;?>
					</div>
					<div class="col-sm-12">
						<br/>
						<a href="#" class="btn btn-success btn-xs" onCLick="pastVisit()">Past Visit</a>
						<a href="#" class="btn btn-warning btn-xs" onClick="medicalHistory()">Riwayat Medis Pasien</a>
					</div>
					
				</div>
				
			</div>
			<div class="col-sm-4">
				<div class="row">
					<div class="col-sm-4 text-danger text-bold">No Registrasi</div>
					<div class="col-sm-8">: <?=$registrasi->no_registrasi;?></div>
				</div>
				<div class="row">
					<div class="col-sm-4 text-danger text-bold">Waktu Registrasi</div>
					<div class="col-sm-8">: <?=Lib::dateInd($registrasi->waktu_registrasi,false);?></div>
				</div>
				<div class="row">
					<div class="col-sm-4 text-danger text-bold">Dokter</div>
					<div class="col-sm-8">: 
					<?php
					foreach($registrasi->dpjps as $row){
						echo $row->idDokter->idPersonal->nama_lengkap.'<br/>';
					}
					?>
					</div>
				</div>
				<div class="row">
					<div class="col-sm-4 text-danger text-bold">Layanan</div>
					<div class="col-sm-8">: <?=$registrasi->idDepartemen->nama_departemen;?></div>
				</div>
				<div class="row">
					<div class="col-sm-4 text-danger text-bold">Jenis Jaminan</div>
					<div class="col-sm-8">: 
					<?php
						if($registrasi->jenis_jaminan=="UMUM"){
							echo "Umum";
						}else{
							foreach($registrasi->jaminanPasiens as $row){
								echo $row->idPenjamin->nama_penjamin.'<br/>';
							}
						}
					?>
					</div>
				</div>
				<div class="row">
					<div class="col-sm-4 text-danger text-bold">Catatan</div>
					<div class="col-sm-8">: <?=$registrasi->catatan;?></div>
				</div>
			</div>
		</div>
	</div>
</div>
