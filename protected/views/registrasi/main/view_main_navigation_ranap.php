<ul class="nav nav-pills nav-stacked">
	<li class="header">Navigasi</li>
	<li>
		<a href="<?php echo Yii::app()->createUrl('pasien/view',array("id"=>$model->id_pasien));?>" style="display:<?php echo ((Lib::accessBy('pasien','view')==true)?"block":"none");?>"><i class="fa fa-user"></i> Data Identitas</a>
	</li>
	<li <?=(Yii::app()->controller->action->id=="info")?'class="active"':''?>>
		<a href="<?php echo Yii::app()->createUrl('registrasi/info',array("id"=>$model->id_pasien,"idReg"=>$registrasi->id_registrasi));?>" style="display:<?php echo ((Lib::accessBy('registrasi','info')==true)?"block":"none");?>"><i class="fa fa-info"></i> Registrasi</a>
	</li>
	<li <?=(Yii::app()->controller->action->id=="dataDasar")?'class="active"':''?>>
		<a href="<?php echo Yii::app()->createUrl('registrasi/dataDasar',array("id"=>$model->id_pasien,"idReg"=>$registrasi->id_registrasi));?>" style="display:<?php echo ((Lib::accessBy('registrasi','dataDasar')==true)?"block":"none");?>"><i class="fa fa-medkit"></i> Data Dasar</a>
	</li>
	<li <?=(Yii::app()->controller->action->id=="soap")?'class="active"':''?>>
		<a href="<?php echo Yii::app()->createUrl('registrasi/soap',array("id"=>$model->id_pasien,"idReg"=>$registrasi->id_registrasi));?>" style="display:<?php echo ((Lib::accessBy('registrasi','soap')==true)?"block":"none");?>"><i class="fa fa-user-md"></i> SOAP</a>
	</li>
	<li <?=(Yii::app()->controller->action->id=="icd")?'class="active"':''?>>
		<a href="<?php echo Yii::app()->createUrl('registrasi/icd',array("id"=>$model->id_pasien,"idReg"=>$registrasi->id_registrasi));?>" style="display:<?php echo ((Lib::accessBy('registrasi','icd')==true)?"block":"none");?>"><i class="fa fa-list"></i> ICD</a>
	</li>
	<li <?=(Yii::app()->controller->action->id=="resep")?'class="active"':''?>>
		<a href="<?php echo Yii::app()->createUrl('registrasi/resep',array("id"=>$model->id_pasien,"idReg"=>$registrasi->id_registrasi));?>" style="display:<?php echo ((Lib::accessBy('registrasi','resep')==true)?"block":"none");?>"><i class="fa fa-pencil"></i> Resep</a>
	</li>
	<li <?=(Yii::app()->controller->action->id=="obatAlkes")?'class="active"':''?>>
		<a href="<?php echo Yii::app()->createUrl('registrasi/obatAlkes',array("id"=>$model->id_pasien,"idReg"=>$registrasi->id_registrasi));?>" style="display:<?php echo ((Lib::accessBy('registrasi','obatAlkes')==true)?"block":"none");?>"><i class="fa fa-medkit"></i> Obat / Alkes</a>
	</li>
	<li <?=(Yii::app()->controller->action->id=="tindakan")?'class="active"':''?>>
		<a href="<?php echo Yii::app()->createUrl('registrasi/tindakan',array("id"=>$model->id_pasien,"idReg"=>$registrasi->id_registrasi));?>" style="display:<?php echo ((Lib::accessBy('registrasi','tindakan')==true)?"block":"none");?>"><i class="fa fa-heartbeat"></i> Tindakan</a>
	</li>
	<li <?=(Yii::app()->controller->action->id=="visite")?'class="active"':''?>>
		<a href="<?php echo Yii::app()->createUrl('registrasi/visite',array("id"=>$model->id_pasien,"idReg"=>$registrasi->id_registrasi));?>" style="display:<?php echo ((Lib::accessBy('registrasi','visite')==true)?"block":"none");?>"><i class="fa fa-user-md"></i> Visite</a>
	</li>
	<li <?=(Yii::app()->controller->action->id=="alkes")?'class="active"':''?>>
		<a href="<?php echo Yii::app()->createUrl('registrasi/alkes',array("id"=>$model->id_pasien,"idReg"=>$registrasi->id_registrasi));?>" style="display:<?php echo ((Lib::accessBy('registrasi','alkes')==true)?"block":"none");?>"><i class="fa fa-stethoscope"></i> Alat Medis</a>
	</li>
	<li <?=(Yii::app()->controller->action->id=="berkas")?'class="active"':''?>>
		<a href="<?php echo Yii::app()->createUrl('registrasi/berkas',array("id"=>$model->id_pasien,"idReg"=>$registrasi->id_registrasi));?>" style="display:<?php echo ((Lib::accessBy('registrasi','berkas')==true)?"block":"none");?>"><i class="fa fa-file"></i> Berkas</a>
	</li>
</ul>