<link rel="stylesheet" href="<?php echo Yii::app()->theme->baseUrl; ?>/assets/css/bootstrap-select.css">
<script src="<?php echo Yii::app()->theme->baseUrl; ?>/assets/js/bootstrap-select.js"></script>
<?php
if($event=="close"){
?>
<script language="javascript">
	window.opener.refresTindakan();
	window.self.close();
</script>
<?php
}
?>
<?php 
if($registrasi->jenis_registrasi=="IGD"){
	echo $this->renderPartial('main/view_main_header', array(
		'model'=>$model,
		'registrasi'=>$registrasi
	));
}elseif($registrasi->jenis_registrasi=="Rawat Jalan"){
	echo $this->renderPartial('main/view_main_header', array(
		'model'=>$model,
		'registrasi'=>$registrasi
	));
}elseif($registrasi->jenis_registrasi=="Rawat Inap"){
	echo $this->renderPartial('main/view_main_header_ranap', array(
		'model'=>$model,
		'registrasi'=>$registrasi
	));
}else{
	echo $this->renderPartial('main/view_main_header', array(
		'model'=>$model,
		'registrasi'=>$registrasi
	));
}
?>
<select id="dataDokter" style="display:none;">
	<option value="">Pilih Dokter</option>
	<?php
	$medis=Pegawai::model()->findAll(array("with"=>array("idPersonal"),"condition"=>"jenis_pegawai='Medis'"));
	foreach($medis as $row){
		echo'<option value="'.$row->id_pegawai.'">'.$row->idPersonal->nama_lengkap.'</option>';
	}
	?>
</select>
<div class="box box-primary">
	<div class="box-body">
		<?php
		$form=$this->beginWidget('booster.widgets.TbActiveForm',array(
			'id'=>'tindakan-pasien-form',
			'enableAjaxValidation'=>true,
			//'htmlOptions'=>array('novalidate'=>true)
		));
		?>
		<?php
		if(Yii::app()->user->hasFlash('error')){
			echo'
			<div class="alert alert-danger">
				<button type="button" class="close" data-dismiss="alert">&times;</button>
				'.Yii::app()->user->getFlash('error').'
			</div>';
		}
		?>
		<?php echo $form->dateTimePickerGroup($tindakan,'waktu_tindakan',array('widgetOptions'=>array('options'=>array('format'=>'yyyy-mm-dd hh:ii:ss','viewFormat'=>'yyyy-mm-dd hh:ii:ss'),'htmlOptions'=>array('value'=>date('Y-m-d H:i:s'))))) ;?>
		<?php echo $form->dropDownListGroup($tindakan,'jenis_input', array('widgetOptions'=>array('data'=>array("tarif" => "Input Tarif", "input" => "Buat Tindakan & Tarif"), 'htmlOptions'=>array('class'=>'input-large')))); ?>
		
			<?php
				if($tindakan->jenis_input == 'tarif') {
					echo $this->renderPartial('tindakan/_input_tarif', array('form' => $form, 'tindakan' => $tindakan));
				}
			?>
			<?php
				if($tindakan->jenis_input == 'input') {
					echo $this->renderPartial('tindakan/_create_tindakan', array('form' => $form, 'tindakan' => $tindakan));
				}
			?>
		<div class="form-actions">
			<?php $this->widget('booster.widgets.TbButton', array(
					'buttonType'=>'submit',
					'context'=>'primary',
					'htmlOptions'=>array(
						"id"=>"submit"
					),
					'label'=>$tindakan->isNewRecord ? 'Tambah Tindakan' : 'Simpan',
				)); 
				?>
		</div>
		<?php $this->endWidget(); ?>
	</div>
</div>

<?php
Yii::app()->clientScript->registerScript('addResep', '
var tarifAll=0;
var qty=1;
$("#tindakan-pasien-form").submit(function(e){
	if(confirm("Apakah Data yang diinpu sudah benar?")) {
        return true;
    }
    return false;
});

$("#comboDokterManual").html($("#dataDokter").html());
$("#comboDokterManual").selectpicker("refresh");
function reCount(){
	var harga = parseInt(tarifAll) * parseInt(qty);
	$("#TindakanPasien_tarif_tindakan").val(harga);
}

$("#TindakanPasien_jenis_input").change(function(){
	var tipe= $("#TindakanPasien_jenis_input").val();
	if(tipe == "tarif") {
		window.location.assign("'.Yii::app()->createUrl('registrasi/tambahTindakanManual',array("id"=>$model->id_pasien,"idReg"=>$registrasi->id_registrasi)).'");
	} else {
		window.location.assign("'.Yii::app()->createUrl('registrasi/tambahTindakanManual',array("id"=>$model->id_pasien,"idReg"=>$registrasi->id_registrasi,'input'=>'input')).'");
	}
});

$("#TindakanPasien_jumlah_tindakan").change(function(){
	qty = $("#TindakanPasien_jumlah_tindakan").val();
	reCount();
});

$("#TindakanPasien_id_kelompok_tindakan").change(function(){
	var id = $("#TindakanPasien_id_kelompok_tindakan").val();
	getVal(id,"id_kelompok_tindakan","#TindakanPasien_id_tindakan","tindakan/getTindakan");
});

function getVal(value,param,id,url){	
	$.ajax({
		url: "'.Yii::app()->createAbsoluteUrl('/').'/"+url,
		cache: false,
		type: "POST",
		data:"TindakanPasien["+param+"]="+value,
		success: function(msg){
			$(id).html("");
			$(id).html(msg);
			$(id).selectpicker("refresh");
		}
	});
}

function getDetailTindakan(id,kelas){
	$.ajax({
		url: "'.Yii::app()->createAbsoluteUrl('tindakan/getDetailTindakan').'",
		cache: false,
		type: "POST",
		data:"id="+id+"&kelas="+kelas+"&idreg="+'.$registrasi->id_registrasi.',
		success: function(msg){
			var data=$.parseJSON(msg);
			tarifAll=data.tarif.tarif_all;
			$("#peranMedis").html("");
			$("#id_tarif_tindakan").val(data.tarif.id);
			reCount();
			var dataOptions = $("#dataDokter").html();
			for(i=0;i<data.medis.length;i++){
				if(data.medis[i].mandatory=="1"){
					$("#peranMedis").append("<div class=\"row\"><div class=\"col-sm-8\"><label>"+data.medis[i].nama_peran+"</label><select class=\"form-control\" id=\"comboDokter"+data.medis[i].id_tarif+"\" data-live-search=\"true\" name=\"PeranMedis["+data.medis[i].id_tarif+"][medis]\" required=\"required\">"+dataOptions+"</select><br/><br/></div><div class=\"col-sm-4\"><label>Jasa Medis "+data.medis[i].nama_peran+"</label><input type=\"number\" class=\"form-control\" id=\"jasMed\" name=\"PeranMedis["+data.medis[i].id_tarif+"][jasa_medis]\" value=\""+data.medis[i].tarif+"\" /></div></div>");
					$("#comboDokter"+data.medis[i].id_tarif).selectpicker("refresh");
				}else{
					$("#peranMedis").append("<div class=\"row\"><div class=\"col-sm-8\"><label>"+data.medis[i].nama_peran+"</label><select class=\"form-control\" id=\"comboDokter"+data.medis[i].id_tarif+"\" data-live-search=\"true\" name=\"PeranMedis["+data.medis[i].id_tarif+"][medis]\">"+dataOptions+"</select><br/><br/></div><div class=\"col-sm-4\"><label>Jasa Medis "+data.medis[i].nama_peran+"</label><input type=\"number\" class=\"form-control\" id=\"jasMed\" name=\"PeranMedis["+data.medis[i].id_tarif+"][jasa_medis]\" value=\""+data.medis[i].tarif+"\" /></div></div>");
					$("#comboDokter"+data.medis[i].id_tarif).selectpicker("refresh");
				}
				
			}
		}
	});
}
', CClientScript::POS_END);
?>