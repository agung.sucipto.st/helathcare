<div class="row">
	<div class="col-sm-4">
		<?php echo $form->dropDownListGroup($tindakan,'id_kelompok_tindakan', array('widgetOptions'=>array('data'=>CHtml::listData(KelompokTindakan::model()->findAll(),'id_kelompok_tindakan','nama_kelompok_tindakan'), 'htmlOptions'=>array('class'=>'input-large','empty'=>'Pilih Kelompok Tindakan','required'=>true)))); ?>
	</div>
	<div class="col-sm-4">
		<?php echo $form->dropDownListGroup($tindakan,'id_tindakan', array('widgetOptions'=>array('data'=>CHtml::listData(Tindakan::model()->findAll(array("condition"=>"status='Aktif'")),'id_tindakan','nama_tindakan'), 'htmlOptions'=>array('class'=>'input-large selectpicker show-tick','data-live-search'=>"true",'empty'=>'Pilih Tindakan')))); ?>
	</div>
	<div class="col-sm-4">
		<?php echo $form->dropDownListGroup($tindakan,'id_kelas', array('widgetOptions'=>array('data'=>CHtml::listData(Kelas::model()->findAll(),'id_kelas','nama_kelas'), 'htmlOptions'=>array('class'=>'input-large','empty'=>'Pilih Kelas')))); ?>
	</div>
</div>
<div class="row">
	<div class="col-sm-12">
		<?php echo $form->numberFieldGroup($tindakan,'jumlah_tindakan',array('widgetOptions'=>array('htmlOptions'=>array('class'=>'span5','maxlength'=>20,"min"=>1,"value"=>1)))); ?>
	</div>
</div>



<div id="peranMedis">
	
</div>

<div class="row">
	<div class="col-sm-6">
		<?php echo $form->textAreaGroup($tindakan,'catatan', array('widgetOptions'=>array('htmlOptions'=>array('rows'=>2, 'cols'=>50, 'class'=>'span8')))); ?>
	</div>
	<div class="col-sm-6">
		<?php echo $form->numberFieldGroup($tindakan,'tarif_tindakan',array('widgetOptions'=>array('htmlOptions'=>array('class'=>'span5', "style" => "font-size:20pt; height:50px; color: red;")))); ?>
	</div>
</div>
<input type="hidden" name="TindakanPasien[id_tarif_tindakan]" id="id_tarif_tindakan"/>


<?php
Yii::app()->clientScript->registerScript('addResep2', '
$("#TindakanPasien_id_tindakan").change(function(){
	var id = $("#TindakanPasien_id_tindakan").val();
	var kelas = $("#TindakanPasien_id_kelas").val();
	if(id != "" && kelas != ""){
		getDetailTindakan(id,kelas);
	}
});

', CClientScript::POS_END);
?>