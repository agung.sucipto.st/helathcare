<div class="row">
	<div class="col-sm-4">
		<?php echo $form->dropDownListGroup($tindakan,'id_kelompok_tindakan', array('widgetOptions'=>array('data'=>CHtml::listData(KelompokTindakan::model()->findAll(),'id_kelompok_tindakan','nama_kelompok_tindakan'), 'htmlOptions'=>array('class'=>'input-large','empty'=>'Pilih Kelompok Tindakan','required'=>true)))); ?>
	</div>
	<div class="col-sm-4">
		<?php echo $form->textFieldGroup($tindakan,'id_tindakan',array('widgetOptions'=>array('htmlOptions'=>array('class'=>'span5', "style" => "font-size:20pt; height:50px; color: red;")))); ?>
	</div>
	<div class="col-sm-4">
		<?php echo $form->dropDownListGroup($tindakan,'id_kelas', array('widgetOptions'=>array('data'=>CHtml::listData(Kelas::model()->findAll(),'id_kelas','nama_kelas'), 'htmlOptions'=>array('class'=>'input-large','empty'=>'Pilih Kelas')))); ?>
	</div>
</div>
<div class="row">
	<div class="col-sm-12">
		<?php echo $form->numberFieldGroup($tindakan,'jumlah_tindakan',array('widgetOptions'=>array('htmlOptions'=>array('class'=>'span5','maxlength'=>20,"min"=>1,"value"=>1)))); ?>
	</div>
</div>

<div id="peranMedis">
	<?php
		$medis=PeranMedis::model()->find(array("condition"=>"nama_peran_medis='Dokter'"));
	?>
	<div class="row"><div class="col-sm-8"><label><?=$medis->nama_peran_medis;?></label><select class="form-control" id="comboDokterManual" data-live-search="true" name="PeranMedis[medis]" required="required"></select><br/><br/></div><div class="col-sm-4"><label>Jasa Medis <?=$medis->nama_peran_medis;?></label><input type="number" class="form-control" id="jasMed" name="PeranMedis[jasa_medis]" /><input type="hidden" name="PeranMedis[id]" value="<?=$medis->id_peran_medis;?>"/></div></div>
</div>

<div class="row">
	<div class="col-sm-6">
		<?php echo $form->textAreaGroup($tindakan,'catatan', array('widgetOptions'=>array('htmlOptions'=>array('rows'=>2, 'cols'=>50, 'class'=>'span8')))); ?>
	</div>
	<div class="col-sm-6">
		<?php echo $form->numberFieldGroup($tindakan,'tarif_tindakan',array('widgetOptions'=>array('htmlOptions'=>array('class'=>'span5', "style" => "font-size:20pt; height:50px; color: red;")))); ?>
	</div>
</div>