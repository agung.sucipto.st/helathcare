<?php
if($event=="close"){
?>
<script language="javascript">
	window.opener.refreshResep();
	window.self.close();
</script>
<?php
}
?>

<?php 
if($registrasi->jenis_registrasi=="IGD"){
	echo $this->renderPartial('main/view_main_header', array(
		'model'=>$model,
		'registrasi'=>$registrasi
	));
}elseif($registrasi->jenis_registrasi=="Rawat Jalan"){
	echo $this->renderPartial('main/view_main_header', array(
		'model'=>$model,
		'registrasi'=>$registrasi
	));
}elseif($registrasi->jenis_registrasi=="Rawat Inap"){
	echo $this->renderPartial('main/view_main_header_ranap', array(
		'model'=>$model,
		'registrasi'=>$registrasi
	));
}else{
	echo $this->renderPartial('main/view_main_header', array(
		'model'=>$model,
		'registrasi'=>$registrasi
	));
}
?>

<div class="box box-primary">
	<div class="box-body">
		<?php $form=$this->beginWidget('booster.widgets.TbActiveForm',array(
				'id'=>'resep-pasien-form',
				'enableAjaxValidation'=>false,
			)); ?>
		<div class="row">
			<div class="col-sm-6">
				<label>Cari Nama Obat  / Kandungan Obat</label>
				<?php
				$this->widget('zii.widgets.jui.CJuiAutoComplete',array(
					'name'=>'resep',
					'options'=>array(
						'minLength'=>3,
						'showAnim'=>'fold',
						'select'=>"js:function(event, data) {
							var item=data.item.label;
							addRow(item);
						}"
					),
					'source'=>$this->createUrl("registrasi/getObat"),
					'htmlOptions'=>array(
						'class'=>"form-control",
						"id"=>"searchResep"
					),
				));
				?>
			</div>
			
			<div class="col-sm-6">
				<?php echo $form->dropDownListGroup($resep,'id_dokter', array('widgetOptions'=>array('data'=>CHtml::listData(Pegawai::model()->findAll(array("with"=>array("idPersonal"),"condition"=>"fungsional='Dokter'")),'id_pegawai','idPersonal.nama_lengkap'), 'htmlOptions'=>array('class'=>'input-large selectpicker show-tick','data-live-search'=>"true",'empty'=>'Pilih Dokter',)))); ?>
				<?php echo $form->textAreaGroup($resep,'racikan', array('widgetOptions'=>array('htmlOptions'=>array('rows'=>3, 'cols'=>50, 'class'=>'span8')))); ?>
			</div>
		</div>
		
		<table class="table table-stripped">
			<tr>
				<th>Nama Obat</th>
				<th>Stok</th>
				<th width="100px">Qty</th>
				<th>Satuan</th>
				<th>Harga</th>
				<th>Signa</th>
				<th></th>
			</tr>
			<tbody id="data">
			
			</tbody>
		</table>
		
		<hr>
		<div class="form-actions">
			<?php $this->widget('booster.widgets.TbButton', array(
					'buttonType'=>'submit',
					'context'=>'primary',
					'htmlOptions'=>array(
						//"disabled"=>true,
						"id"=>"submit"
					),
					'label'=>$model->isNewRecord ? 'Tambah Data Pasien' : 'Simpan',
				)); 
				?>
		</div>
		<?php $this->endWidget(); ?>
	</div>
</div>

<?php

Yii::app()->clientScript->registerScript('addResep', '
var rowResep=[];
cekSubmit();
function deleteRow(id){
	$("#row"+id).remove();
	rowResep.splice(id, 1);
	cekSubmit();
}

function cekSubmit(){
	console.log(rowResep);
	console.log(rowResep.length);
}

function addRow(item){
	$.ajax({
		url: "'.Yii::app()->createAbsoluteUrl('registrasi/getObat').'",
		cache: false,
		type: "POST",
		data:"item="+item,
		success: function(msg){
			var data=$.parseJSON(msg);
			if(rowResep[data.id_item] == undefined){
				cekSubmit();
				rowResep[data.id_item]=data.nama_item;
				$("#searchResep").val("");
				var inputItem="<input type=\"hidden\" name=\"id_item[]\" value=\""+data.id_item+"\"/>"+data.nama_item;
				var stokItem=data.stok;
				var qty="<input type=\"number\" name=\"qty"+data.id_item+"\" value=\"\" class=\"form-control\" required=\"required\" max=\""+stokItem+"\"/>";
				var satuan=data.satuan;
				var harga=data.harga;
				var signa="<input type=\"text\" name=\"signa"+data.id_item+"\" id=\"signa"+data.id_item+"\" value=\"\" class=\"form-control\" required=\"required\"//>";
				var remove="<span class=\"btn btn-danger\" onclick=\"deleteRow("+data.id_item+")\"><i class=\"fa fa-times\"></i></span>";
				$("#data").append("<tr id=\"row"+data.id_item+"\"><td>"+inputItem+"</td><td>"+stokItem+"</td><td>"+qty+"</td><td>"+satuan+"</td><td>"+harga+"</td><td>"+signa+"</td><td>"+remove+"</td></tr>");
				
				jQuery("#signa"+data.id_item).autocomplete({"minLength":3,"showAnim":"fold","source":"'.Yii::app()->createUrl("registrasi/getSigna").'"});
			}else{
				$("#searchResep").val("");
			}
		}
	});
}
', CClientScript::POS_END);
?>