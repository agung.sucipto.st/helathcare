<?php
if($event=="close"){
?>
<script language="javascript">
	window.opener.refresh();
	window.self.close();
</script>
<?php
}
?>

<link rel="stylesheet" href="<?php echo Yii::app()->theme->baseUrl; ?>/assets/css/bootstrap-select.css">
<script src="<?php echo Yii::app()->theme->baseUrl; ?>/assets/js/bootstrap-select.js"></script>

<div class="box box-primary">
	<div class="box-body">
		<h4 class="box-title">Pendaftaran Akan Dibatalkan</h4>
		<span class="text-danger">Semua Data Registrasi Akan Di Hapus, Pastikan Telah Berkoordinasi Dengan Seluruh Unit !</span>
		<hr>
		<?php
		if(Yii::app()->user->hasFlash('error')){
			echo'
			<br/>
			<div class="alert alert-danger">
				<button type="button" class="close" data-dismiss="alert">&times;</button>
				<i class="fa fa-warning"></i>'.Yii::app()->user->getFlash('error').'
			</div>';
		}
		?>
		<?php $form=$this->beginWidget('booster.widgets.TbActiveForm',array(
			'id'=>'registrasi-form',
			'enableAjaxValidation'=>false,
		)); ?>

		<?php echo $form->dropDownListGroup($registrasi,'alasan_pembatalan', array('widgetOptions'=>array('data'=>CHtml::listdata(AlasanOtorisasi::model()->findAll(array("condition"=>"id_otorisasi='1'")),"id_alasan_otorisasi","alasan"), 'htmlOptions'=>array('class'=>'input-large','empty'=>'Pilih Alasan Pembatalan','required'=>true)))); ?>
		<label>
		Password ( <?=$model->username;?> )
		</label>
		<?php echo $form->passwordField($model,'password', array("label"=>"Password",'class'=>'form-control', 'id'=>'pwd', 'size'=>'100', 'placeholder'=>'Password', 'required'=>'required')); ?>
		<br/>
		<div class="form-actions">
		<?php $this->widget('booster.widgets.TbButton', array(
			'buttonType'=>'submit',
			'context'=>'primary',
			'label'=>$model->isNewRecord ? 'Daftar' : 'Batalkan Pendaftaran',
		)); 
		echo CHtml::button('Batal', array(
			'class' => 'btn btn-primary',
			'onclick' => "window.self.close();",
				)
		);
		?>
		</div>

		<?php $this->endWidget(); ?>
	</div>
</div>


<?php
Yii::app()->clientScript->registerScript('validate', '
	$("#Registrasi_status_keluar").change(function(){
		var val = $("#Registrasi_status_keluar").val();
		if(val == "Dirujuk"){
			$("#Registrasi_id_faskes_tujuan_rujuk").attr("required","required");
			$("#Rujukan").show();
		} else if(val == "Meninggal"){
			$("#Registrasi_id_faskes_tujuan_rujuk").removeAttr("required");
			$("#Rujukan").hide();
			$("#Registrasi_waktu_meninggal").attr("required","required");
			$("#Registrasi_lama_meninggal").attr("required","required");
			$("#Registrasi_cara_meninggal").attr("required","required");
			$("#Meninggal").show();
		} else {
			$("#Registrasi_id_faskes_tujuan_rujuk").removeAttr("required");
			$("#Registrasi_waktu_meninggal").removeAttr("required");
			$("#Registrasi_lama_meninggal").removeAttr("required");
			$("#Registrasi_cara_meninggal").removeAttr("required");
			$("#Rujukan").hide();
			$("#Meninggal").hide();
		}
	});
', CClientScript::POS_END);
?>