<!-- start card.html -->
<!--
Works on IE 6.0.2
paper size : Card
margin top,bottom,left,right : 0
Orientation : Landscape 
-->
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<title>Kartu</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<style type="text/css">
body {margin: 0; padding: 0;}
#cardbox {width: 300px; height: 4.8cm; margin-left: -45px; margin-top: 55px; padding: 5px; border: 1px solid #fff; -webkit-transform: rotate(90deg); -moz-transform: rotate(90deg); transform: rotate(90deg); }
#cardbox .blankspot {height: 2.8cm;}
#cardbox .photo {float: left; height: 2.4cm; text-align: center; width: 85px; padding: 5px; font: .6em bold Arial, Helvetica, sans-serif; color: #666; text-transform: capitalize; display: none; }
#cardbox .photo img {width: 1.7cm; height: 2.2cm; padding: 2px; border: 1px solid #ccc; page }
#cardbox .infobarcode {text-align: left; margin-top: 1px;}
#cardbox .infocard {text-transform: capitalize; text-align: left; margin-left: 0.2cm; font: .8em bold Arial, Helvetica, sans-serif;}
#cardbox .rm {text-transform: capitalize; text-align: left; margin-left: 0.2cm; margin-top: -0.2cm; font: 10pt bold Arial, Helvetica, sans-serif;}
.satu { width: 213px; }
.dua { width: 170px; }
.tiga { width: 143px; }
.empat { width: 120px; }
.lima { width: 108px; }

		
		 @font-face { 
		font-family: IDAutomationHC39M;  
		src: url(<?php echo Yii::app()->theme->baseUrl;?>/assets/fonts/IDAutomationHC39M.woff);
		}
		.barcode {
		  font-family: 'IDAutomationHC39M'; 
		  width:auto;
		  font-size:7pt;
		}
	</style>
</head>

<body onLoad="window.print()">

<div id="cardbox">
	<div class="blankspot">&nbsp;</div>
	<div class="photo"></div>
	<div class="infocard" style="margin-top:-10px; margin-bottom: 0cm; margin-left:20px; word-wrap: break-word;width: 235;"><strong><?=$model->idPasien->idPersonal->nama_lengkap.' '.$model->idPasien->panggilan;?></strong></div>
	<div style="bottom:15px;position:absolute; margin-top:-13px;">
		<div class="rm" style="margin-left:20px;"><strong><?=Lib::MRN($model->id_pasien);?></strong></div>
		<div class="infobarcode" style="margin-top:6px;margin-left:23px; margin-bottom: 2px;">
			<span class="barcode">(<?=Lib::MRN($model->id_pasien);?>)</span>
		</div>
	</div>
	<!--div class="infocard">12-08-2017&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Female</div-->
	</div>
</div>

</body>
</html>
<script type="text/javascript">
        if ('9462'.length == 1){
                document.getElementById("barcodenya").className = 'satu';
        } else if ('9462'.length == 2) {
                document.getElementById("barcodenya").className = 'dua';
        } else if ('9462'.length == 3) {
                document.getElementById("barcodenya").className = 'tiga';
        } else if ('9462'.length == 4) {
                document.getElementById("barcodenya").className = 'empat';
        } else {
                document.getElementById("barcodenya").className = 'lima';
        }
</script>
<!-- end card.html -->
