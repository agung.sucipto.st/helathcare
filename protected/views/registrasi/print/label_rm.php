<html>
	<head>
	<style>
		body{
			font-family:Arial;
			
		}
		.page{
			width:55mm;
			height:auto;
			margin-left:-4px;
		}
		.label{
			width:auto;
			height:18mm;
			padding-top:0.9mm;
			padding-left:2px;
			margin-bottom:0mm;
		}
		.content{
			width:100%;
			font-size:7pt;
		}
		.label:first-child{
			margin-top:-7px;
		}
		.label:last-child{
			margin-bottom:-10px;
		}
		th{
			text-align:left;
		}
		table{
			font-size:10px;
			font-weight:bold;
		}
		.patient{
			width:100%;
			font-weight:bold;
			font-size:9pt;
			overflow: hidden;
    white-space: nowrap;
    text-overflow: ellipsis;
		}
		.info{
			<?php
				if($model->jenis_jaminan == 'UMUM') {
					echo "font-size:6pt;";
				}else{
					if(strlen($model->jaminanPasiens[0]->idPenjamin->nama_penjamin) > 40) {
						echo "font-size:4pt;";
					} else {
						echo "font-size:6pt;";
					}
				}
			?>
			font-weight:bold;
		}
		
		@media print {
		  #visible{
			  display:none;
		  }
		  #myP{
			  display:none;
		  }
		}
	</style>
	<head>
	<body>
		<input id="visible" onchange="generateLabel(this.value);" type="number" value="7"/>
		
		<input id="visible" type="button" value="print" onclick="window.print();"/>
		<div class="page" id="page">
		
		</div>
	
	<script type="text/javascript">
	var nama= "<?php echo $model->idPasien->idPersonal->nama_lengkap;?> <?php echo $model->idPasien->panggilan;?>";
	var rmori= "<?php echo Lib::MRN($model->id_pasien);?>";
	var rm= "<?php echo Lib::MRN($model->id_pasien);?>";
	var ttl= "<?php echo Lib::dateInd($model->idPasien->idPersonal->tanggal_lahir, false);?>";
	var dept= "<?php echo $model->idDepartemen->nama_departemen;?>";
	var oridept= "<?php echo $model->idDepartemen->nama_departemen;?>";
	var regis= "<?php echo Lib::dateInd($model->waktu_registrasi, false);?>";
	var penjamin= "<?php
		if($model->jenis_jaminan == 'UMUM') {
			echo 'UMUM';
		}else{
			$model->jaminanPasiens[0]->idPenjamin->nama_penjamin;
		}
	?>";
	var n= 7;
	var titipan='';
	var kamar='';
	
	function generateValue(val){
		titipan=val;
		dept=oridept;
		dept=dept+" / Kelas Asal :<b>"+titipan+"</b>";
		generateLabel(n);
	}
	
	function generateValueKamar(val){
		kamar=val;
		rm=rmori;
		rm=rm+" / Kamar Ibu :<b>"+kamar+"</b>";
		generateLabel(n);
	}
	function generateLabel(n){
		var n = n;
		document.getElementById("page").innerHTML = "";
		for (i = 0; i < n; i++) { 
			var data = '<div class="label"><div class="content"><div class="wristband_container3"><span class="patient" style="text-align: left;">'+nama+'</span><br/><span style="text-align: left;">'+rm+'</span><br/><span style="text-align: left;">'+ttl+'</span><br/><span>'+dept+'</span><br/><span class="tanggal">'+regis.trim()+'</span><br/><span class="info">'+penjamin+'</span></div></div></div>';
			document.getElementById("page").innerHTML+=data;
		}	
	}
	generateLabel(n);
	window.print();
	</script>
	
	
	<script>
	function myFunction() {
		if(document.getElementById("myP").style.display == "block"){
		 document.getElementById("myP").style.display = "none";
		 document.getElementById("myP").value = "";
		 titipan='';
		 dept=oridept;
		 generateLabel(n);
		}else{
		 document.getElementById("myP").style.display = "block";
		}  
	}
	
	function myFunctions() {
		if(document.getElementById("kamarIbu").style.display == "block"){
		 document.getElementById("kamarIbu").style.display = "none";
		 document.getElementById("kamarIbu").value = "";
		 kamar='';
		 rm=rmori;
		 generateLabel(n);
		}else{
		 document.getElementById("kamarIbu").style.display = "block";
		}  
	}
	</script>
	</body>
</html>