<table width="100%">
	<tr>
		<td width="50%">
			<div class="col-sm-7">
				<div class="row">
					<div class="col-sm-12" style="font-size:25px;font-weight:bold;">
					<?php
						echo Lib::MRN($model->id_pasien).' - '.strtoupper($model->idPasien->idPersonal->nama_lengkap).", ".$model->idPasien->panggilan;	
					?>
					</div>
				</div>
				<div class="row">
					<div class="col-sm-12">
					<?php
						echo $model->idPasien->idPersonal->jenis_kelamin.' / '.Lib::dateInd($model->idPasien->idPersonal->tanggal_lahir,false).", ".Lib::umurDetail($model->idPasien->idPersonal->tanggal_lahir);	
					?>
					<br/>
					<span class="text-danger text-bold">Alergi</span> : <?=$model->idPasien->idPersonal->alergi;?>
				</div>
				
			</div>
		</div>
		<td width="50%">
			No Reg : <?=$model->no_registrasi;?><br/>
			Waktu Registrasi : <?=Lib::dateInd($model->waktu_registrasi,false);?><br />
			Dokter  : <?php
					foreach($model->dpjps as $row){
						echo $row->idDokter->idPersonal->nama_lengkap.', ';
					}
					?> <br />
			Layanan : <?=$model->idDepartemen->nama_departemen;?><br/>
			Jenis Jaminan : <?php
						if($model->jenis_jaminan=="UMUM"){
							echo "Umum";
						}else{
							foreach($model->jaminanPasiens as $row){
								echo $row->idPenjamin->nama_penjamin.', ';
							}
						}
					?>
		</td>
	</tr>	
	<tr>
		<td colspan="2">
			<hr>
			Catatan Dokter :
			<br/>
			<br/>
			<br/>
			<br/>
			<br/>
			<br/>
			<br/>
			<br/>
			<br/>
			<br/>
			<br/>
			<br/>
			<br/>
			<br/>
			<br/>
			<hr>
		</td>
	</tr>
	<tr>
		<td></td>
		<td>
			Dokter Peanggungjawab Pasien,
			<br/>
			<br/>
			<br/>
			<br/>
			<b><u>
			<?=$model->dpjps[0]->idDokter->idPersonal->nama_lengkap;?>
			</u></b>
		</td>
	</tr>
</table>