<link rel="stylesheet" href="<?php echo Yii::app()->theme->baseUrl; ?>/assets/css/bootstrap-select.css">
<script src="<?php echo Yii::app()->theme->baseUrl; ?>/assets/js/bootstrap-select.js"></script>
<?php
if($event=="close"){
?>
<script language="javascript">
	window.opener.refresTindakan();
	window.self.close();
</script>
<?php
}
?>

<?php 
if($registrasi->jenis_registrasi=="IGD"){
	echo $this->renderPartial('main/view_main_header', array(
		'model'=>$model,
		'registrasi'=>$registrasi
	));
}elseif($registrasi->jenis_registrasi=="Rawat Jalan"){
	echo $this->renderPartial('main/view_main_header', array(
		'model'=>$model,
		'registrasi'=>$registrasi
	));
}elseif($registrasi->jenis_registrasi=="Rawat Inap"){
	echo $this->renderPartial('main/view_main_header_ranap', array(
		'model'=>$model,
		'registrasi'=>$registrasi
	));
}else{
	echo $this->renderPartial('main/view_main_header', array(
		'model'=>$model,
		'registrasi'=>$registrasi
	));
}
?>
<?php
$medis=Pegawai::model()->findAll(array("with"=>array("idPersonal"),"condition"=>"jenis_pegawai='Medis' AND fungsional='Dokter'"));
$dokter= array();
foreach($medis as $row){
	$dokter[$row->id_pegawai]=$row->idPersonal->nama_lengkap;
}
?>
<div class="box box-primary">
	<div class="box-body">
		<?php
			if(Yii::app()->user->hasFlash('failed')){
				echo'
				<br/>
				<div class="alert alert-danger">
					<button type="button" class="close" data-dismiss="alert">&times;</button>
					<i class="fa fa-warning"></i>'.Yii::app()->user->getFlash('failed').'
				</div>';
			}
			?>	
		<?php $form=$this->beginWidget('booster.widgets.TbActiveForm',array(
				'id'=>'visite-dokter-pasien-form',
				'enableAjaxValidation'=>false,
			)); ?>
		<?php echo $form->dateTimePickerGroup($visite,'waktu_visite',array('widgetOptions'=>array('options'=>array('format'=>'yyyy-mm-dd hh:ii:ss','viewFormat'=>'yyyy-mm-dd hh:ii:ss'),'htmlOptions'=>array('value'=>date('Y-m-d H:i:s'))))) ;?>
		<div class="row">
			<div class="col-sm-6">
				<?php echo $form->dropDownListGroup($visite,'id_dokter', array('widgetOptions'=>array('data'=>$dokter, 'htmlOptions'=>array('class'=>'input-large selectpicker show-tick','data-live-search'=>"true",'empty'=>'Pilih Dokter')))); ?>
			</div>
			<div class="col-sm-6">
				<?php echo $form->dropDownListGroup($visite,'id_kelas', array('widgetOptions'=>array('data'=>CHtml::listData(Kelas::model()->findAll(array("condition"=>"jenis_kelas='IP'")),'id_kelas','nama_kelas'), 'htmlOptions'=>array('class'=>'input-large selectpicker show-tick','data-live-search'=>"true",'empty'=>'Pilih Kelas')))); ?>
			</div>
		</div>
		<div class="row">
			<div class="col-sm-12">
				<?php echo $form->numberFieldGroup($visite,'jumlah',array('widgetOptions'=>array('htmlOptions'=>array('class'=>'span5','maxlength'=>20,"min"=>1,"value"=>1)))); ?>
			</div>
		</div>
		<div class="form-actions">
			<?php $this->widget('booster.widgets.TbButton', array(
					'buttonType'=>'submit',
					'context'=>'primary',
					'htmlOptions'=>array(
						"id"=>"submit"
					),
					'label'=>$visite->isNewRecord ? 'Tambah Visite' : 'Simpan',
				)); 
				?>
		</div>
		<?php $this->endWidget(); ?>
	</div>
</div>