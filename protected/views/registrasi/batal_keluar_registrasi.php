<?php
if($event=="close"){
?>
<script language="javascript">
	window.opener.refreshRegister();
	window.self.close();
</script>
<?php
}
?>

<link rel="stylesheet" href="<?php echo Yii::app()->theme->baseUrl; ?>/assets/css/bootstrap-select.css">
<script src="<?php echo Yii::app()->theme->baseUrl; ?>/assets/js/bootstrap-select.js"></script>

<div class="box box-primary">
	<div class="box-body">
		<h4 class="box-title">Status Kunjungan Akan Diaktifkan</h4>
		<hr>
		<?php
		if(Yii::app()->user->hasFlash('error')){
			echo'
			<br/>
			<div class="alert alert-danger">
				<button type="button" class="close" data-dismiss="alert">&times;</button>
				<i class="fa fa-warning"></i>'.Yii::app()->user->getFlash('error').'
			</div>';
		}
		?>
		<?php $form=$this->beginWidget('booster.widgets.TbActiveForm',array(
			'id'=>'registrasi-form',
			'enableAjaxValidation'=>true,
		)); ?>
		<label>
		Password ( <?=$model->username;?> )
		</label>
		<?php echo $form->passwordField($model,'password', array("label"=>"Password",'class'=>'form-control', 'id'=>'pwd', 'size'=>'100', 'placeholder'=>'Password', 'required'=>'required')); ?>
		<br/>
		
		<?php $this->widget('booster.widgets.TbButton', array(
			'buttonType'=>'submit',
			'context'=>'primary',
			'label'=>$model->isNewRecord ? 'Daftar' : 'Batal Keluar',
		)); 
		echo CHtml::button('Batal', array(
			'class' => 'btn btn-primary',
			'onclick' => "window.self.close();",
				)
		);
		?>
		</div>

		<?php $this->endWidget(); ?>
	</div>
</div>