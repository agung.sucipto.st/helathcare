<link rel="stylesheet" href="<?php echo Yii::app()->theme->baseUrl; ?>/assets/css/bootstrap-select.css">
<script src="<?php echo Yii::app()->theme->baseUrl; ?>/assets/js/bootstrap-select.js"></script>

<style>
	h4{
		font-weight:bold;
	}
	hr{
		border-color:green;
	}
</style>
<hr>
<?php $form=$this->beginWidget('booster.widgets.TbActiveForm',array(
	'id'=>'registrasi-form',
	'enableAjaxValidation'=>true,
)); ?>

<?php echo $form->errorSummary($model); ?>
	<?php echo $form->dateTimePickerGroup($model,'waktu_registrasi',array('widgetOptions'=>array('options'=>array('format'=>'yyyy-mm-dd hh:ii:ss','viewFormat'=>'yyyy-mm-dd hh:ii:ss'),'htmlOptions'=>array('value'=>date('Y-m-d H:i:s'))))) ;?>
	
	<?php echo $form->dropDownListGroup($model,'jenis_jaminan', array('widgetOptions'=>array('data'=>array("UMUM"=>"UMUM","JAMINAN"=>"JAMINAN ASURANSI",), 'htmlOptions'=>array('class'=>'input-large')))); ?>
	<?php
	if(!$model->isNewRecord){
		if($model->id_penjamin!=""){
			$display='block';
		}else{
			$display='none';
		}
	}else{
		if($model->id_penjamin!=""){
			$display='block';
		}else{
			$display='none';
		}
	}
	?>
	<div class="jaminan" style="display:<?php echo $display;?>;">
		<?php echo $form->dropDownListGroup($model,'id_penjamin', array('widgetOptions'=>array('data'=>CHtml::listData(Penjamin::model()->findAll(),'id_penjamin','nama_penjamin'), 'htmlOptions'=>array('class'=>'input-large','empty'=>'Pilih Penjamin')))); ?>
		<div class="row">
			<div class="col-sm-4">
				<?php echo $form->textFieldGroup($model,'no_kartu',array('widgetOptions'=>array('htmlOptions'=>array('class'=>'span5','maxlength'=>30)))); ?>
			</div>
			<div class="col-sm-4">
				<?php echo $form->textFieldGroup($model,'nama_pemegang_kartu',array('widgetOptions'=>array('htmlOptions'=>array('class'=>'span5','maxlength'=>30)))); ?>
			</div>
			<div class="col-sm-4">
				<?php echo $form->dropDownListGroup($model,'id_jenis_hubungan', array('widgetOptions'=>array('data'=>CHtml::listData(JenisHubungan::model()->findAll(),'id_jenis_hubungan','jenis_hubungan'), 'htmlOptions'=>array('class'=>'input-large','empty'=>'Pilih Hubungan')))); ?>
			</div>
		</div>
		<div class="row">
			<div class="col-sm-4">
				<?php echo $form->dropDownListGroup($model,'fasilitas_jaminan', array('widgetOptions'=>array('data'=>array("Kelas 1"=>"Kelas 1","Kelas 2"=>"Kelas 2","Kelas 3"=>"Kelas 3","VIP"=>"VIP","VVIP"=>"VVIP"), 'htmlOptions'=>array('class'=>'input-large','empty'=>'Pilih Fasilitas')))); ?>
			</div>
			<div class="col-sm-4">
				<?php echo $form->textFieldGroup($model,'no_eligibilitas',array('widgetOptions'=>array('htmlOptions'=>array('class'=>'span5','maxlength'=>30)))); ?>
			</div>
			<div class="col-sm-4">
				<?php echo $form->datePickerGroup($model,'tgl_eligibilitas',array('widgetOptions'=>array('options'=>array('format'=>'yyyy-mm-dd','viewFormat'=>'yyyy-mm-dd'),'htmlOptions'=>array('class'=>'span5')),)); ?>
			</div>
		</div>
	</div>
	<?php echo $form->dropDownListGroup($model,'id_dokter', array('widgetOptions'=>array('data'=>CHtml::listData(DepartemenPegawai::model()->findAll(array("with"=>array("idPegawai","idPegawai.idPersonal"),"condition"=>"idPegawai.status_aktif='Aktif' and id_departemen='1'")),'id_pegawai','idPegawai.idPersonal.nama_lengkap'), 'htmlOptions'=>array('class'=>'input-large selectpicker show-tick','data-live-search'=>"true",'empty'=>"Pilih Dokter")))); ?>
	
	<?php echo $form->dropDownListGroup($model,'rujukan', array('widgetOptions'=>array('data'=>array("Inisiatif Sendiri"=>"Inisiatif Sendiri","Luar"=>"Luar","Dalam"=>"Dalam",), 'htmlOptions'=>array('class'=>'input-large')))); ?>
	<?php
	if($model->isNewRecord){
		if($model->rujukan=="Inisiatif Sendiri"){
			$rujukanLuar='none';
			$rujukanDalam='none';
		}elseif($model->rujukan=="Luar"){
			$rujukanLuar='block';
			$rujukanDalam='none';
		}elseif($model->rujukan=="Luar"){
			$rujukanLuar='none';
			$rujukanDalam='block';
		}else{
			$rujukanLuar='none';
			$rujukanDalam='none';
		}
	}
	?>
	<div class="rujukanLuar" style="display:<?=$rujukanLuar;?>">
		<?php echo $form->dropDownListGroup($model,'id_faskes', array('widgetOptions'=>array('data'=>Faskes::getList(), 'htmlOptions'=>array('class'=>'input-large selectpicker show-tick','data-live-search'=>"true",'empty'=>"Pilih Faskes")))); ?>
	</div>
	<div class="rujukanDalam" style="display:<?=$rujukanDalam;?>">
		<?php echo $form->dropDownListGroup($model,'id_pegawai_perujuk', array('widgetOptions'=>array('data'=>CHtml::listData(Pegawai::model()->findAll(array("with"=>array("idPersonal"),"condition"=>"status_aktif='Aktif' and fungsional='Dokter'")),'id_pegawai','idPersonal.nama_lengkap'), 'htmlOptions'=>array('class'=>'input-large selectpicker show-tick','data-live-search'=>"true")))); ?>
	</div>

	<div class="row">
		<div class="col-sm-4">
			<?php echo $form->dropDownListGroup($model,'jenis_kasus', array('widgetOptions'=>array('data'=>array("Kecelakaan"=>"Kecelakaan","Kecelakaan Kerja"=>"Kecelakaan Kerja","Perkelahian"=>"Perkelahian","Keracunan"=>"Keracunan","Sakit"=>"Sakit"), 'htmlOptions'=>array('class'=>'input-large','empty'=>"Jenis Kasus")))); ?>
		</div>
		<div class="col-sm-4">
			<?php echo $form->dropDownListGroup($model,'tipe_emergency', array('widgetOptions'=>array('data'=>array("Darurat"=>"Darurat","Gawat Darurat"=>"Gawat Darurat","Gawat Tidak Darurat"=>"Gawat Tidak Darurat","Darurat Tidak Gawat"=>"Darurat Tidak Gawat","Tidak Darurat"=>"Tidak Darurat","Tidak Gawat Tidak Darurat"=>"Tidak Gawat Tidak Darurat"), 'htmlOptions'=>array('class'=>'input-large',"empty"=>'Tipe Emergency')))); ?>
		</div>
		<div class="col-sm-4">
			<?php echo $form->dropDownListGroup($model,'line_emergency', array('widgetOptions'=>array('data'=>array("Bedah"=>"Bedah","Non Bedah"=>"Non Bedah","Kebidanan"=>"Kebidanan","Anak"=>"Anak","Psikiatri"=>"Psikiatri"), 'htmlOptions'=>array('class'=>'input-large',"empty"=>'Line Emergency')))); ?>
		</div>
	</div>

	<?php echo $form->textAreaGroup($model,'catatan', array('widgetOptions'=>array('htmlOptions'=>array('rows'=>6, 'cols'=>50, 'class'=>'span8')))); ?>

<div class="form-actions">
<?php $this->widget('booster.widgets.TbButton', array(
	'buttonType'=>'submit',
	'context'=>'primary',
	'label'=>$model->isNewRecord ? 'Daftar' : 'Simpan',
)); 
echo CHtml::button('Batal', array(
	'class' => 'btn btn-primary',
	'onclick' => "history.go(-1)",
		)
);
?>
</div>

<?php $this->endWidget(); ?>

<?php
if($model->isNewRecord){
	if($model->id_penjamin!=""){
		$required='$("#Registrasi_no_kartu").attr("required","true");
		$("#Registrasi_nama_pemegang_kartu").attr("required","true");
		$("#Registrasi_id_jenis_hubungan").attr("required","true");
		$("#Registrasi_fasilitas_jaminan").attr("required","true");';
	}else{
		$required='$("#Registrasi_no_kartu").attr("required","false");
		$("#Registrasi_nama_pemegang_kartu").attr("required","false");
		$("#Registrasi_id_jenis_hubungan").attr("required","false");
		$("#Registrasi_fasilitas_jaminan").attr("required","false");';
	}
}
Yii::app()->clientScript->registerScript('validate', '
$("#Registrasi_jenis_jaminan").change(function(){
	var id = $("#Registrasi_jenis_jaminan").val();
	if(id == "UMUM"){
		$(".jaminan").hide();
		$("#Registrasi_id_penjamin").removeAttr("required");
		$("#Registrasi_id_penjamin").val("");
		$("#Registrasi_no_kartu").val("");
		$("#Registrasi_nama_pemegang_kartu").val("");
		$("#Registrasi_id_jenis_hubungan").val("");
		$("#Registrasi_fasilitas_jaminan").val("");
		$("#Registrasi_no_eligibilitas").val("");
		$("#Registrasi_tgl_eligibilitas").val("");
	}else{
		$(".jaminan").show();
		$("#Registrasi_id_penjamin").attr("required","true");
	}
});

$("#Registrasi_rujukan").change(function(){
	var id = $("#Registrasi_rujukan").val();
	if(id == "Inisiatif Sendiri"){
		$(".rujukanLuar").hide();
		$(".rujukanDalam").hide();
		$("#Registrasi_id_pegawai_perujuk").removeAttr("required");
		$("#Registrasi_id_faskes").removeAttr("required");
		$("#Registrasi_id_pegawai_perujuk").val("");
		$("#Registrasi_id_faskes").val("");
	}else if(id == "Luar"){
		$(".rujukanLuar").show();
		$(".rujukanDalam").hide();
		$("#Registrasi_id_pegawai_perujuk").removeAttr("required");
		$("#Registrasi_id_faskes").attr("required","true");
		$("#Registrasi_id_pegawai_perujuk").val("");
	}else{
		$(".rujukanLuar").hide();
		$(".rujukanDalam").show();
		$("#Registrasi_id_pegawai_perujuk").attr("required","true");
		$("#Registrasi_id_faskes").removeAttr("required");
		$("#Registrasi_id_faskes").val("");
	}
});

$("#Registrasi_id_penjamin").change(function(){
	var idPenjamin = $("#Registrasi_id_penjamin").val();
	if(idPenjamin != ""){
		$.ajax({
			url: "'.Yii::app()->createAbsoluteUrl('registrasi/getPenjaminPasien/id/'.$model->id_pasien).'",
			cache: false,
			type: "POST",
			data:"idPenjamin="+idPenjamin,
			success: function(msg){
				data = $.parseJSON(msg);
				console.log(data);
				$("#Registrasi_no_kartu").val(data.no_kartu);
				$("#Registrasi_nama_pemegang_kartu").val(data.nama_pemegang_kartu);
				$("#Registrasi_id_jenis_hubungan").val(data.id_jenis_hubungan);
				$("#Registrasi_fasilitas_jaminan").val(data.fasilitas_jaminan);
				$("#Registrasi_no_kartu").attr("required","true");
				$("#Registrasi_nama_pemegang_kartu").attr("required","true");
				$("#Registrasi_id_jenis_hubungan").attr("required","true");
				$("#Registrasi_fasilitas_jaminan").attr("required","true");
			}
		});
	}else{
		$("#Registrasi_jenis_jaminan").val("UMUM");
		$(".jaminan").hide();
		$("#Registrasi_id_penjamin").removeAttr("required");
		$("#Registrasi_no_kartu").val("");
		$("#Registrasi_nama_pemegang_kartu").val("");
		$("#Registrasi_id_jenis_hubungan").val("");
		$("#Registrasi_fasilitas_jaminan").val("");
		$("#Registrasi_no_eligibilitas").val("");
		$("#Registrasi_tgl_eligibilitas").val("");
	
		'.$required.'
	}
});

', CClientScript::POS_END);
?>