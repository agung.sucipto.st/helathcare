<style>
	h4{
		font-weight:bold;
	}
	hr{
		margin-top:5px;
	}
</style>

<div class="row">
	<div class="col-sm-2">
		<?php echo $this->renderPartial('main/view_main_navigation',array('model'=>$model,'registrasi'=>$registrasi)); ?>
	</div>
	<div class="col-sm-10">
		<h4>Penggunaan Obat & Alkes Ruangan</h4>
		<div class="box box-primary">
			<div class="box-body">
				<?php
				if(Yii::app()->user->hasFlash('success')){
					echo'
					<br/>
					<div class="alert alert-success">
						<button type="button" class="close" data-dismiss="alert">&times;</button>
						<i class="fa fa-warning"></i>'.Yii::app()->user->getFlash('success').'
					</div>';
				}
				?>					
				<?php		
				$this->widget('booster.widgets.TbExtendedGridView', array(
					'id'=>'item-transaksi-grid',
					'type' => 'striped',
					'dataProvider' => $trx->searchPasien(),
					'summaryText'=>false,
					'selectableRows' => 2,
					'responsiveTable' => true,
					'enablePagination' => true,
					'pager' => array(
						'htmlOptions'=>array(
							'class'=>'pagination'
						),
						'maxButtonCount' => 5,
						'cssFile' => true,
						'header' => false,
						'firstPageLabel' => '<<',
						'prevPageLabel' => '<',
						'nextPageLabel' => '>',
						'lastPageLabel' => '>>',
					),
					'columns'=>array(
							array(
								"header"=>"Waktu Penggunaan",
								"value"=>'Lib::dateInd($data->waktu_transaksi,false)'
							),
							array(
								"header"=>"Item",
								"type"=>"raw",
								"value"=>function($data){
									$return="<b>$data->no_transaksi</b><ul>";
									foreach($data->daftarItemTransaksis as $row){
										$return.="<li>R/ ".$row->idItem->nama_item." : ".$row->jumlah_transaksi." ".Item::getParentUnit($row->id_item)->idSatuan->nama_satuan."</li>";
									}
									$return.="</ul>";
									return $return;
								}
							),
							array(
								"header"=>"Harga",
								"value"=>'number_format($data->nominal_transaksi)'
							),
							array(
								"header"=>"User Input",
								"type"=>"raw",
								"value"=>function($data){
									return $data->userCreate->idPegawai->idPersonal->nama_lengkap.'<br/>'.Lib::dateInd($data->time_create,false);
								}
							),
							array(
								'class'=>'booster.widgets.TbButtonColumn',
								 'deleteConfirmation'=>'Anda yakin akan menhapus data?',
								'template'=>'{delete}',
								'buttons'=>array
								(
									'delete' => array
									(
										'label'=>'Delete',
										'icon'=>'trash',
										'url'=>'Yii::app()->createUrl("itemTransaksi/hapusRU",array("id"=>$data->id_item_transaksi))',
										'visible'=>'(Lib::accessBy(\'itemTransaksi\',\'hapusRU\') AND Registrasi::getStatus($data->id_registrasi)=="Aktif")',
										'options'=>array(
											'class'=>'btn btn-default btn-xs delete',
										),
									)
								),
								'header'=>'',
							),
					),
				));
				?>
			</div>
		</div>
		<?php
		if($registrasi->status_registrasi=="Aktif"){
		?>
		<button onClick="openItemTrx()" class="btn btn-primary pull-right" style="display:<?php echo ((Lib::accessBy('registrasi','tambahObatAlkes')==true)?"block":"none");?>">
			<i class="fa fa-plus"></i> Tambah Penggunaan Obat & Alkes
		</button>
		<?php
		}
		?>
	</div>
</div>

<?php
Yii::app()->clientScript->registerScript('obatAlkes', '

function openItemTrx(){
	window.open("'.Yii::app()->createUrl('registrasi/tambahObatAlkes',array("id"=>$model->id_pasien,"idReg"=>$registrasi->id_registrasi)).'","","width=1200,height=600");
}
function refreshParent(){
	$.fn.yiiGridView.update("item-transaksi-grid");
}
', CClientScript::POS_END);
?>