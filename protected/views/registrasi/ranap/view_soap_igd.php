<link rel="stylesheet" href="<?php echo Yii::app()->theme->baseUrl; ?>/assets/css/bootstrap-select.css">
<script src="<?php echo Yii::app()->theme->baseUrl; ?>/assets/js/bootstrap-select.js"></script>

<style>
	h4{
		font-weight:bold;
	}
	hr{
		margin-top:5px;
	}
</style>

<div class="row">
	<div class="col-sm-2">
		<?php echo $this->renderPartial('main/view_main_navigation_ranap',array('model'=>$model,'registrasi'=>$registrasi)); ?>
	</div>
	<div class="col-sm-10">
		<h4>SOAP</h4>
		
		<?php
		if($registrasi->status_registrasi=="Aktif"){
			echo $this->renderPartial('rajal/_aktif_soap_igd',array('soap'=>$soap,'registrasi'=>$registrasi)); 
		}else{
			echo $this->renderPartial('rajal/_tutup_soap_igd',array('registrasi'=>$registrasi)); 
		}
		?>
	</div>
</div>