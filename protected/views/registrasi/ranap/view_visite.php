<style>
	h4{
		font-weight:bold;
	}
	hr{
		margin-top:5px;
	}
</style>

<div class="row">
	<div class="col-sm-2">
		<?php echo $this->renderPartial('main/view_main_navigation_ranap',array('model'=>$model,'registrasi'=>$registrasi)); ?>
	</div>
	<div class="col-sm-10">
		<h4>Visite</h4>
		<div class="box box-primary">
			<div class="box-body">
				<?php
				if(Yii::app()->user->hasFlash('success')){
					echo'
					<br/>
					<div class="alert alert-success">
						<button type="button" class="close" data-dismiss="alert">&times;</button>
						<i class="fa fa-warning"></i>'.Yii::app()->user->getFlash('success').'
					</div>';
				}
				?>					
				<?php		
				$this->widget('booster.widgets.TbExtendedGridView', array(
					'id'=>'visite-dokter-pasien-grid',
					'type' => 'striped',
					'dataProvider' => $visite->search(),
					'summaryText'=>false,
					'selectableRows' => 2,
					'responsiveTable' => true,
					'enablePagination' => true,
					'pager' => array(
						'htmlOptions'=>array(
							'class'=>'pagination'
						),
						'maxButtonCount' => 5,
						'cssFile' => true,
						'header' => false,
						'firstPageLabel' => '<<',
						'prevPageLabel' => '<',
						'nextPageLabel' => '>',
						'lastPageLabel' => '>>',
					),
					'columns'=>array(
							array(
								"header"=>"Waktu Visite",
								"value"=>'Lib::dateInd($data->waktu_visite,false)'
							),
							array(
								"header"=>"Tenaga Medis",
								"type"=>"raw",
								"value"=>'$data->idDokter->idPersonal->nama_lengkap'
							),
							array(
								"header"=>"Jumlah",
								"value"=>'$data->jumlah'
							),
							array(
								"header"=>"Tarif",
								"value"=>function($data){
									return number_format($data->tarif,0);
								}
							),
							array(
								"header"=>"Kelas",
								"value"=>'$data->idKelas->nama_kelas'
							),
							array(
								"header"=>"User Input",
								"type"=>"raw",
								"value"=>function($data){
									return $data->userCreate->idPegawai->idPersonal->nama_lengkap.'<br/>'.Lib::dateInd($data->time_create,false);
								}
							),
							array(
								'class'=>'booster.widgets.TbButtonColumn',
								 'deleteConfirmation'=>'Anda yakin akan menhapus data?',
								'template'=>'{delete}',
								'buttons'=>array
								(
									'delete' => array
									(
										'label'=>'Delete',
										'icon'=>'trash',
										'url'=>'Yii::app()->createUrl("visiteDokterPasien/delete",array("id"=>$data->id_visite_dokter_pasien))',
										'visible'=>'(Lib::accessBy("visiteDokterPasien","delete") AND Registrasi::getStatus($data->id_registrasi)=="Aktif")',
										'options'=>array(
											'class'=>'btn btn-default btn-xs delete',
										),
									)
								),
								'header'=>'',
							),
					),
				));
				?>
			</div>
		</div>
		<?php
		if($registrasi->status_registrasi=="Aktif"){
		?>
		<button onClick="openVisite()" class="btn btn-primary pull-right" style="display:<?php echo ((Lib::accessBy('registrasi','tambahTindakan')==true)?"block":"none");?>">
			<i class="fa fa-plus"></i> Tambah Visite
		</button>
		<?php
		}
		?>
	</div>
</div>

<?php
Yii::app()->clientScript->registerScript('visite', '

function openVisite(){
	window.open("'.Yii::app()->createUrl('registrasi/tambahVisite',array("id"=>$model->id_pasien,"idReg"=>$registrasi->id_registrasi)).'","","width=1200,height=600");
}
function refresTindakan(){
	$.fn.yiiGridView.update("visite-dokter-pasien-grid");
}
', CClientScript::POS_END);
?>