<?php 
if($registrasi->jenis_registrasi=="IGD"){
	echo $this->renderPartial('main/view_main_header', array(
		'model'=>$model,
		'registrasi'=>$registrasi
	));
}elseif($registrasi->jenis_registrasi=="Rawat Jalan"){
	echo $this->renderPartial('main/view_main_header', array(
		'model'=>$model,
		'registrasi'=>$registrasi
	));
}elseif($registrasi->jenis_registrasi=="Rawat Inap"){
	echo $this->renderPartial('main/view_main_header_ranap', array(
		'model'=>$model,
		'registrasi'=>$registrasi
	));
}else{
	echo $this->renderPartial('main/view_main_header', array(
		'model'=>$model,
		'registrasi'=>$registrasi
	));
}
?>
<div class="row">
	<div class="col-sm-10">
	<div class="box box-solid">
		<div class="box-body">
			<?php 
			$action=Yii::app()->controller->action->id;
			if($registrasi->jenis_registrasi=="IGD"){
				if($action=="info"){
					echo $this->renderPartial('rajal/view_info_registrasi_igd', array(
						'model'=>$model,'registrasi'=>$registrasi
					)); 
				}elseif($action=="dataDasar"){
					echo $this->renderPartial('rajal/view_data_dasar', array(
						'model'=>$model,'registrasi'=>$registrasi,'vitalSign'=>$vitalSign
					)); 
				}elseif($action=="soap"){
					echo $this->renderPartial('rajal/view_soap_igd', array(
						'model'=>$model,'registrasi'=>$registrasi,'soap'=>$soap
					)); 
				}elseif($action=="icd"){
					echo $this->renderPartial('rajal/view_icd', array(
						'model'=>$model,'registrasi'=>$registrasi,'icd'=>$icd
					)); 
				}elseif($action=="resep"){
					echo $this->renderPartial('rajal/view_resep', array(
						'model'=>$model,'registrasi'=>$registrasi,'resep'=>$resep
					)); 
				}elseif($action=="tindakan"){
					echo $this->renderPartial('rajal/view_tindakan', array(
						'model'=>$model,'registrasi'=>$registrasi,'tindakan'=>$tindakan
					)); 
				}elseif($action=="berkas"){
					echo $this->renderPartial('rajal/view_berkas', array(
						'model'=>$model,'registrasi'=>$registrasi,'berkas'=>$berkas
					)); 
				}elseif($action=="alkes"){
					echo $this->renderPartial('rajal/view_alkes', array(
						'model'=>$model,'registrasi'=>$registrasi,'alkes'=>$alkes
					)); 
				}elseif($action=="obatAlkes"){
					echo $this->renderPartial('rajal/view_obat_alkes', array(
						'model'=>$model,'registrasi'=>$registrasi,'trx'=>$trx
					)); 
				}
			}elseif($registrasi->jenis_registrasi=="Rawat Jalan"){
				if($action=="info"){
					echo $this->renderPartial('rajal/view_info_registrasi_igd', array(
						'model'=>$model,'registrasi'=>$registrasi
					)); 
				}elseif($action=="dataDasar"){
					echo $this->renderPartial('rajal/view_data_dasar', array(
						'model'=>$model,'registrasi'=>$registrasi,'vitalSign'=>$vitalSign
					)); 
				}elseif($action=="soap"){
					echo $this->renderPartial('rajal/view_soap_igd', array(
						'model'=>$model,'registrasi'=>$registrasi,'soap'=>$soap
					)); 
				}elseif($action=="icd"){
					echo $this->renderPartial('rajal/view_icd', array(
						'model'=>$model,'registrasi'=>$registrasi,'icd'=>$icd
					)); 
				}elseif($action=="resep"){
					echo $this->renderPartial('rajal/view_resep', array(
						'model'=>$model,'registrasi'=>$registrasi,'resep'=>$resep
					)); 
				}elseif($action=="tindakan"){
					echo $this->renderPartial('rajal/view_tindakan', array(
						'model'=>$model,'registrasi'=>$registrasi,'tindakan'=>$tindakan
					)); 
				}elseif($action=="berkas"){
					echo $this->renderPartial('rajal/view_berkas', array(
						'model'=>$model,'registrasi'=>$registrasi,'berkas'=>$berkas
					)); 
				}elseif($action=="alkes"){
					echo $this->renderPartial('rajal/view_alkes', array(
						'model'=>$model,'registrasi'=>$registrasi,'alkes'=>$alkes
					)); 
				}elseif($action=="obatAlkes"){
					echo $this->renderPartial('rajal/view_obat_alkes', array(
						'model'=>$model,'registrasi'=>$registrasi,'trx'=>$trx
					)); 
				}
			}elseif($registrasi->jenis_registrasi=="Rawat Inap"){
				if($action=="info"){
					echo $this->renderPartial('ranap/view_info_registrasi_igd', array(
						'model'=>$model,'registrasi'=>$registrasi
					)); 
				}elseif($action=="dataDasar"){
					echo $this->renderPartial('ranap/view_data_dasar', array(
						'model'=>$model,'registrasi'=>$registrasi,'vitalSign'=>$vitalSign
					)); 
				}elseif($action=="soap"){
					echo $this->renderPartial('ranap/view_soap_igd', array(
						'model'=>$model,'registrasi'=>$registrasi,'soap'=>$soap
					)); 
				}elseif($action=="icd"){
					echo $this->renderPartial('ranap/view_icd', array(
						'model'=>$model,'registrasi'=>$registrasi,'icd'=>$icd
					)); 
				}elseif($action=="resep"){
					echo $this->renderPartial('ranap/view_resep', array(
						'model'=>$model,'registrasi'=>$registrasi,'resep'=>$resep
					)); 
				}elseif($action=="tindakan"){
					echo $this->renderPartial('ranap/view_tindakan', array(
						'model'=>$model,'registrasi'=>$registrasi,'tindakan'=>$tindakan
					)); 
				}elseif($action=="visite"){
					echo $this->renderPartial('ranap/view_visite', array(
						'model'=>$model,'registrasi'=>$registrasi,'visite'=>$visite
					)); 
				}elseif($action=="berkas"){
					echo $this->renderPartial('ranap/view_berkas', array(
						'model'=>$model,'registrasi'=>$registrasi,'berkas'=>$berkas
					)); 
				}elseif($action=="alkes"){
					echo $this->renderPartial('ranap/view_alkes', array(
						'model'=>$model,'registrasi'=>$registrasi,'alkes'=>$alkes
					)); 
				}elseif($action=="obatAlkes"){
					echo $this->renderPartial('rajal/view_obat_alkes', array(
						'model'=>$model,'registrasi'=>$registrasi,'trx'=>$trx
					)); 
				}
			}
			?>
		</div>
	</div>
	</div>
	<div class="col-sm-2">		
		<div class="box box-warning">
			<div class="box-body">
				<ul class="nav nav-pills nav-stacked">
					<li class="header">Info Registrasi</li>
					<?php
					if($registrasi->status_registrasi=="Aktif"){
					?>
					<li><a href="#" onClick="openBatal()" style="display:<?php echo ((Lib::accessBy('registrasi','batalRegistrasi')==true)?"block":"none");?>"><i class="fa fa-ban"></i> Batal Registrasi</a></li>
					<li><a href="#" onClick="openKeluar()" style="display:<?php echo ((Lib::accessBy('registrasi','keluar')==true)?"block":"none");?>"><i class="fa fa-sign-out"></i> Pasien Pulang</a></li>
					<?php
					}
					?>
					
					<?php
					if($registrasi->status_registrasi=="Tutup Kunjungan"){
					?>
					<li><a href="#" onClick="openBatalKeluar()" style="display:<?php echo ((Lib::accessBy('registrasi','batalKeluar')==true)?"block":"none");?>"><i class="fa fa-ban"></i> Batal Pulang / Keluar</a></li>
					<?php
					}
					?>
				</ul>
			</div>
		</div>
		
		<div class="box box-primary">
			<div class="box-body">
				<ul class="nav nav-pills nav-stacked">
					<li class="header">Pilih Layanan</li>
					<?php
					$cekRanap=Registrasi::model()->count(array("condition"=>"id_pasien='$model->id_pasien' and jenis_registrasi='Rawat Inap' and status_registrasi='Aktif'"));
					$cekIgd=Registrasi::model()->count(array("condition"=>"id_pasien='$model->id_pasien' and id_departemen='1' and status_registrasi='Aktif'"));
					if($cekRanap == 0) {
						if($cekIgd==0){
						?>
						<li><a href="<?php echo Yii::app()->createUrl('registrasi/igd',array("id"=>$model->id_pasien));?>" style="display:<?php echo ((Lib::accessBy('registrasi','igd')==true)?"block":"none");?>"><i class="fa fa-stethoscope"></i> Emergency</a></li>
						<?php
						}
						?>
					
						<li><a href="<?php echo Yii::app()->createUrl('registrasi/poliklinik',array("id"=>$model->id_pasien));?>" style="display:<?php echo ((Lib::accessBy('registrasi','poliklinik')==true)?"block":"none");?>"><i class="fa fa-stethoscope"></i> Poliklinik</a></li>
						<li><a href="<?php echo Yii::app()->createUrl('registrasi/rawatInap',array("id"=>$model->id_pasien));?>" style="display:<?php echo ((Lib::accessBy('registrasi','rawatInap')==true)?"block":"none");?>"><i class="fa fa-stethoscope"></i> Rawat Inap</a></li>
					<?php
					}
					?>
				</ul>
			</div>
		</div>
		
		<!--
		<div class="box box-danger">
			<div class="box-body">
				<ul class="nav nav-pills nav-stacked">
					<li class="header">Pilih Layanan Migrasi</li>
					<?php
					$cekIgd=Registrasi::model()->count(array("condition"=>"id_pasien='$model->id_pasien' and id_departemen='1' and status_registrasi='Aktif'"));
					if($cekIgd==0){
					?>
					<li><a href="<?php echo Yii::app()->createUrl('registrasi/igd',array("id"=>$model->id_pasien,"manual"=>true));?>" style="display:<?php echo ((Lib::accessBy('registrasi','igd')==true)?"block":"none");?>"><i class="fa fa-stethoscope"></i> Emergency</a></li>
					<?php
					}
					?>
					
					<li><a href="<?php echo Yii::app()->createUrl('registrasi/poliklinik',array("id"=>$model->id_pasien,"manual"=>true));?>" style="display:<?php echo ((Lib::accessBy('registrasi','poliklinik')==true)?"block":"none");?>"><i class="fa fa-stethoscope"></i> Poliklinik</a></li>
				</ul>
			</div>
		</div>
		-->
	</div>
</div>



<?php
Yii::app()->clientScript->registerScript('validate', '
function pastVisit(){
	var left = (screen.width/2)-(800/2);
	var top = (screen.height/2)-(400/2);
	window.open("'.Yii::app()->createUrl('pasien/pastVisit',array("id"=>$model->id_pasien)).'","","width=800,height=400,top="+top+",left="+left);
}

function medicalHistory(){
	var left = (screen.width/2)-(800/2);
	var top = (screen.height/2)-(400/2);
	window.open("'.Yii::app()->createUrl('pasien/medicalHistory',array("id"=>$model->id_pasien)).'","","width=800,height=400,top="+top+",left="+left);
}

function openBatalKeluar(){
	var left = (screen.width/2)-(600/2);
	var top = (screen.height/2)-(400/2);
	window.open("'.Yii::app()->createUrl('registrasi/batalKeluar',array("id"=>$registrasi->id_registrasi)).'","","width=600,height=400,top="+top+",left="+left);
}

function openBatal(){
	var left = (screen.width/2)-(600/2);
	var top = (screen.height/2)-(400/2);
	window.open("'.Yii::app()->createUrl('registrasi/batalRegistrasi',array("id"=>$registrasi->id_registrasi)).'","","width=600,height=400,top="+top+",left="+left);
}

function openKeluar(){
	var left = (screen.width/2)-(600/2);
	var top = (screen.height/2)-(600/2);
	window.open("'.Yii::app()->createUrl('registrasi/keluar',array("id"=>$model->id_pasien,"idReg"=>$registrasi->id_registrasi)).'","","width=600,height=600,top="+top+",left="+left);
}

function refresh(){
	window.location.href="'.Yii::app()->createUrl('pasien/view',array("id"=>$model->id_pasien)).'";
}

function refreshRegister(){
	window.location.href="'.Yii::app()->createUrl('registrasi/info',array("id"=>$model->id_pasien,"idReg"=>$registrasi->id_registrasi)).'";
}

function regUrl(id,idReg){
	window.location.href="'.Yii::app()->createUrl('registrasi/info').'/"+id+"?idReg="+idReg;
}
', CClientScript::POS_END);
?>