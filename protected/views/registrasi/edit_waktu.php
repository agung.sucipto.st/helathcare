<?php
if($event=="close"){
?>
<script language="javascript">
	window.opener.refreshRegister();
	window.self.close();
</script>
<?php
}
?>

<div class="box box-primary">
	<div class="box-body">
		<h4 class="box-title">Edit Waktu Registrasi</h4>
		<hr>
		<?php $form=$this->beginWidget('booster.widgets.TbActiveForm',array(
			'id'=>'registrasi-form',
			'enableAjaxValidation'=>true,
		)); ?>
		
		<?php echo $form->dateTimePickerGroup($model,'waktu_registrasi',array('widgetOptions'=>array('options'=>array('format'=>'yyyy-mm-dd hh:ii:ss','viewFormat'=>'yyyy-mm-dd hh:ii:ss')))) ;?>
		<br/>
		<div class="form-actions">
		<?php $this->widget('booster.widgets.TbButton', array(
			'buttonType'=>'submit',
			'context'=>'primary',
			'label'=>$model->isNewRecord ? 'Daftar' : 'Simpan',
		)); 
		echo CHtml::button('Batal', array(
			'class' => 'btn btn-primary',
			'onclick' => "window.self.close();",
				)
		);
		?>
		</div>

		<?php $this->endWidget(); ?>
	</div>
</div>