<style>
#vSignTable th{
	font-size:8pt;
	background-color:#ccc;
	text-align:Center;
	vertical-align:middle;
	padding:5px 10px 0px 10px;
}
</style>

<?php
$vHead = new TandaVitalPasien;
?>
<table class="table table-bordered table-striped" id="vSignTable">
	<thead>
		<tr>
			<th>Tanggal</th>
			<th><?=$vHead->getAttributeLabel('height');?><br/>Cm</th>
			<th><?=$vHead->getAttributeLabel('weight');?><br/>Kg</th>
			<th>BMI</th>
			<th><?=$vHead->getAttributeLabel('blood_pressure');?><br/>mmHg</th>
			<th><?=$vHead->getAttributeLabel('pulse_rate');?><br/>BPM</th>
			<th><?=$vHead->getAttributeLabel('respiration_rate');?><br/>Breaths/Minute</th>
			<th><?=$vHead->getAttributeLabel('temperature');?><br/><sup>o</sup>C</th>
			<th><?=$vHead->getAttributeLabel('head_circumference');?><br/>Cm</th>
		</tr>
	</thead>
	<tbody>
		<?php
		if($registrasi->tandaVitalPasiens){
			foreach($registrasi->tandaVitalPasiens as $row){
				echo'
					<tr>
						<td>'.Lib::dateInd($row->time_create, false).'</td>
						<td>'.$row->height.'</td>
						<td>'.$row->weight.'</td>
						<td>';
							$per=pow((($row->height)/100),2);
							@$bmi=number_format($row->weight/$per,2);
							$status='';
							if($bmi<18.5){
								$status="Underweight";
							}elseif($bmi>=18.5 and $bmi<24.9 ){
								$status="Healty";
							}elseif($bmi>=20 and $bmi<29.9 ){
								$status="Overweight";
							}elseif($bmi>=30 ){
								$status="Obese";
							}
							echo $bmi." ( $status )";
						echo'
						</td>
						<td>'.$row->blood_pressure.'</td>
						<td>'.$row->pulse_rate.'</td>
						<td>'.$row->respiration_rate.'</td>
						<td>'.$row->temperature.'</td>
						<td>'.$row->head_circumference.'</td>
					</tr>
				';
			}
		}else{
			echo'
			<tr>
				<td colspan="10">Tidak ada riwayat Tanda Vital Pasien</td>
			</tr>';
		}
		?>
	</tbody>
</table>