<style>
	h4{
		font-weight:bold;
	}
	hr{
		margin-top:5px;
	}
</style>

<div class="row">
	<div class="col-sm-2">
		<?php echo $this->renderPartial('main/view_main_navigation',array('model'=>$model,'registrasi'=>$registrasi)); ?>
	</div>
	<div class="col-sm-10">
		<h4>Tindakan</h4>
		<div class="box box-primary">
			<div class="box-body">
				<?php
				if(Yii::app()->user->hasFlash('success')){
					echo'
					<br/>
					<div class="alert alert-success">
						<button type="button" class="close" data-dismiss="alert">&times;</button>
						<i class="fa fa-warning"></i>'.Yii::app()->user->getFlash('success').'
					</div>';
				}
				?>					
				<?php		
				$this->widget('booster.widgets.TbExtendedGridView', array(
					'id'=>'tindakan-pasien-grid',
					'type' => 'striped',
					'dataProvider' => $tindakan->search(),
					'summaryText'=>false,
					'selectableRows' => 2,
					'responsiveTable' => true,
					'enablePagination' => true,
					'pager' => array(
						'htmlOptions'=>array(
							'class'=>'pagination'
						),
						'maxButtonCount' => 5,
						'cssFile' => true,
						'header' => false,
						'firstPageLabel' => '<<',
						'prevPageLabel' => '<',
						'nextPageLabel' => '>',
						'lastPageLabel' => '>>',
					),
					'columns'=>array(
							array(
								"header"=>"Waktu Tindakan",
								"value"=>'Lib::dateInd($data->waktu_tindakan,false)'
							),
							array(
								"header"=>"Nama Tindakan",
								"value"=>'$data->idTindakan->nama_tindakan'
							),
							array(
								"header"=>"Tenaga Medis",
								"type"=>"raw",
								"value"=>function($data){
									$back='';
									foreach($data->tindakanPasienMedises as $r){
										$back.=$r->idPegawai->idPersonal->nama_lengkap.'['.$r->idTindakanMedis->idPeranMedis->nama_peran_medis.']<br/>';
									}
									return $back;
								}
							),
							array(
								"header"=>"Jumlah",
								"value"=>'$data->jumlah_tindakan'
							),
							array(
								"header"=>"Tarif",
								"value"=>function($data){
									return number_format(($data->idDaftarTagihan->harga*$data->jumlah_tindakan),0);
								}
							),
							array(
								"header"=>"Kelas",
								"value"=>'$data->idKelas->nama_kelas'
							),
							array(
								"header"=>"User Input",
								"type"=>"raw",
								"value"=>function($data){
									return $data->userCreate->idPegawai->idPersonal->nama_lengkap.'<br/>'.Lib::dateInd($data->time_create,false);
								}
							),
							array(
								'class'=>'booster.widgets.TbButtonColumn',
								 'deleteConfirmation'=>'Anda yakin akan menhapus data?',
								'template'=>'{delete}',
								'buttons'=>array
								(
									'update' => array
									(
										'label'=>'Update',
										'icon'=>'pencil',
										'visible'=>'Lib::accessBy(\'registrasi\',\'ubahResep\')',
										'options'=>array(
											'class'=>'btn btn-default btn-xs',
										),
									),
									'delete' => array
									(
										'label'=>'Delete',
										'icon'=>'trash',
										'url'=>'Yii::app()->createUrl("tindakanPasien/delete",array("id"=>$data->id_tindakan_pasien))',
										'visible'=>'(Lib::accessBy("tindakanPasien","delete") AND Registrasi::getStatus($data->id_registrasi)=="Aktif")',
										'options'=>array(
											'class'=>'btn btn-default btn-xs delete',
										),
									)
								),
								'header'=>'',
							),
					),
				));
				?>
			</div>
		</div>
		<?php
		if($registrasi->status_registrasi=="Aktif"){
		?>
		<button onClick="openTindakanManual()" class="btn btn-danger pull-right" style="display:<?php echo ((Lib::accessBy('registrasi','tambahTindakan')==true)?"block":"none");?>">
			<i class="fa fa-plus"></i> Tambah Tindakan Manual
		</button>
		<button onClick="openTindakan()" class="btn btn-primary pull-right" style="display:<?php echo ((Lib::accessBy('registrasi','tambahTindakan')==true)?"block":"none");?>">
			<i class="fa fa-plus"></i> Tambah Tindakan
		</button>
		<?php
		}
		?>
	</div>
</div>

<?php
Yii::app()->clientScript->registerScript('tindakan', '

function openTindakanManual(){
	window.open("'.Yii::app()->createUrl('registrasi/tambahTindakanManual',array("id"=>$model->id_pasien,"idReg"=>$registrasi->id_registrasi)).'","","width=1200,height=600");
}
function openTindakan(){
	window.open("'.Yii::app()->createUrl('registrasi/tambahTindakan',array("id"=>$model->id_pasien,"idReg"=>$registrasi->id_registrasi)).'","","width=1200,height=600");
}
function refresTindakan(){
	$.fn.yiiGridView.update("tindakan-pasien-grid");
}
', CClientScript::POS_END);
?>