<style>
	h4{
		font-weight:bold;
	}
	hr{
		margin-top:5px;
	}
</style>

<div class="row">
	<div class="col-sm-2">
		<?php echo $this->renderPartial('main/view_main_navigation',array('model'=>$model,'registrasi'=>$registrasi)); ?>
	</div>
	<div class="col-sm-10">
		<h4>Data Dasar</h4>
		<div class="box box-primary">
			<div class="box-body">
				<?php 
				if($registrasi->status_registrasi=="Aktif"){
					echo $this->renderPartial('rajal/_form_data_dasar',array('vitalSign'=>$vitalSign,'registrasi'=>$registrasi)); 
				}else{
					echo $this->renderPartial('rajal/_view_data_dasar',array('registrasi'=>$registrasi)); 
				}
				?>
			</div>
		</div>
	</div>
</div>