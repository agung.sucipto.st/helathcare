<div class="box box-primary">
	<div class="box-body">
		<?php
		if(count($registrasi->soapPasiens)>0){
			foreach($registrasi->soapPasiens as $row){
				echo '
					<div class="box box-default">
						<div class="box-body">
							 <h4 class="pull-left">'.$row->idPegawai->idPersonal->nama_lengkap.' ('.$row->idPegawai->fungsional.')</h4>
							 <h6 class="pull-right">'.Lib::dateInd($row->time_create).' ('.$row->userCreate->idPegawai->idPersonal->nama_lengkap.')</h6>
							 <div style="clear:both;"></div>
							 <div class="row">
								<div class="col-sm-6">
									<b>Subjektif</b><br/>'.$row->subjektif.'
								</div>
								<div class="col-sm-6">
									<b>Objektif</b><br/>'.$row->objektif.'
								</div>
							 </div>
							 <div class="row">
								<div class="col-sm-6">
									<b>Assesment</b><br/>'.$row->assesment.'
								</div>
								<div class="col-sm-6">
									<b>Planning</b><br/>'.$row->planing.'
								</div>
							 </div>
							  <div class="row">
								<div class="col-sm-12">
									<b>Instruksi</b><br/>'.$row->instruksi.'
								</div>
							 </div>
							 <div style="clear:both;"></div>
						</div>
					</div>
				';
			}
		}else{
			echo'
			<div class="well">
				Data SOAP Tidak Tersedia !
			</div>';
		}
		?>
	</div>
</div>