<style>
	h4{
		font-weight:bold;
	}
	hr{
		margin-top:5px;
	}
</style>

<div class="row">
	<div class="col-sm-2">
		<?php echo $this->renderPartial('main/view_main_navigation',array('model'=>$model,'registrasi'=>$registrasi)); ?>
	</div>
	<div class="col-sm-10">
		<h4>ICD 10</h4>
		<?php
		if($registrasi->status_registrasi=="Aktif"){
		?>
		<div class="box box-primary">
			<div class="box-body">
			
			<?php 
			
			$form=$this->beginWidget('booster.widgets.TbActiveForm',array(
				'id'=>'icd10-pasien-form',
				'enableAjaxValidation'=>false,
			)); ?>
			<div class="row">
				<div class="col-sm-5">
					<label>Cari Nama Diagnosa / ICD 10</label>
					<?php
					$this->widget('zii.widgets.jui.CJuiAutoComplete',array(
						'name'=>'Icd10Pasien[id_icd10]',
						'options'=>array(
							'minLength'=>3,
							'showAnim'=>'fold',
						),
						'source'=>$this->createUrl("registrasi/getIcd10"),
						'htmlOptions'=>array(
							'class'=>"form-control",
							"id"=>"Icd10Pasien_id_icd10"
						),
					));
					?>
				</div>
				<div class="col-sm-2">
					<?php echo $form->dropDownListGroup($icd,'jenis_kasus', array('widgetOptions'=>array('data'=>array("Baru"=>"Baru","Lama"=>"Lama"), 'htmlOptions'=>array('class'=>'input-large selectpicker show-tick','data-live-search'=>"true",'empty'=>'Jenis Kasus')))); ?>
				</div>
				<div class="col-sm-3">
					<?php echo $form->dropDownListGroup($icd,'order_diagnosa', array('widgetOptions'=>array('data'=>array("Primer"=>"Primer","Sekunder"=>"Sekunder"), 'htmlOptions'=>array('class'=>'input-large selectpicker show-tick','data-live-search'=>"true",'empty'=>'Urutan Diagnosa')))); ?>
				</div>
				<div class="col-sm-2">
				<br/>
					<button type="submit" class="btn btn-primary">Simpan</button>
				</div>
			</div>
			<?php 
			$this->endWidget(); 
			?>
			</div>
		</div>
		<?php
		}
		?>	
		<div class="box box-primary">
			<div class="box-body">
				<?php
				if(Yii::app()->user->hasFlash('success')){
					echo'
					<br/>
					<div class="alert alert-success">
						<button type="button" class="close" data-dismiss="alert">&times;</button>
						<i class="fa fa-warning"></i>'.Yii::app()->user->getFlash('success').'
					</div>';
				}
				?>					
				<?php		
				$this->widget('booster.widgets.TbExtendedGridView', array(
					'id'=>'icd10-pasien-grid',
					'type' => 'striped',
					'dataProvider' => $icd->search(),
					'summaryText'=>false,
					'selectableRows' => 2,
					'responsiveTable' => true,
					'enablePagination' => true,
					'pager' => array(
						'htmlOptions'=>array(
							'class'=>'pagination'
						),
						'maxButtonCount' => 5,
						'cssFile' => true,
						'header' => false,
						'firstPageLabel' => '<<',
						'prevPageLabel' => '<',
						'nextPageLabel' => '>',
						'lastPageLabel' => '>>',
					),
					'columns'=>array(
							array(
								"header"=>"ICD",
								"value"=>'$data->idIcd10->kode_icd'
							),
							array(
								"header"=>"Diagnosa",
								"value"=>'$data->idIcd10->diagnosa'
							),
							array(
								"header"=>"Deskripsi",
								"value"=>'$data->idIcd10->deskripsi'
							),
							array(
								"header"=>"Jenis Kasus",
								"value"=>'$data->jenis_kasus'
							),
							array(
								"header"=>"Urutan Diagnosa",
								"value"=>'$data->order_diagnosa'
							),
							array(
								"header"=>"User Input",
								"type"=>"raw",
								"value"=>function($data){
									return $data->userCreate->idPegawai->idPersonal->nama_lengkap.'<br/>'.Lib::dateInd($data->time_create,false);
								}
							),
							array(
								'class'=>'booster.widgets.TbButtonColumn',
								 'deleteConfirmation'=>'Anda yakin akan menhapus data?',
								'template'=>'{delete}',
								'buttons'=>array
								(
									'delete' => array
									(
										'label'=>'Delete',
										'icon'=>'trash',
										'visible'=>'(Lib::accessBy("registrasi","hapusIcd") AND Registrasi::getStatus($data->id_registrasi)=="Aktif")',
										'url'=>'Yii::app()->createUrl("registrasi/hapusIcd/id/".$data->id_icd_pasien)',
										'options'=>array(
											'class'=>'btn btn-default btn-xs delete',
										),
									)
								),
								'header'=>'',
							),
					),
				));
				?>
			</div>
		</div>
	</div>
</div>

<?php
Yii::app()->clientScript->registerScript('icd', '

$("#icd10-pasien-form").submit(function(e) {

    var url = "'.Yii::app()->createUrl('registrasi/icd',array("id"=>$model->id_pasien,"idReg"=>$registrasi->id_registrasi)).'";

    $.ajax({
           type: "POST",
           url: url,
           data: $("#icd10-pasien-form").serialize(),
           success: function(data)
           {
               if(data=="Sukses"){
				  $("#Icd10Pasien_jenis_kasus").val("");
				  $("#Icd10Pasien_order_diagnosa").val("");
				  $("#Icd10Pasien_id_icd10").val("");
				  refreshIcd();
			   }else{
					$("#Icd10Pasien_jenis_kasus").val("");
					$("#Icd10Pasien_order_diagnosa").val("");
					$("#Icd10Pasien_id_icd10").val("");
				   alert(data);
			   }
           }
         });

    e.preventDefault();
});
function refreshIcd(){
	$.fn.yiiGridView.update("icd10-pasien-grid");
}
', CClientScript::POS_END);
?>