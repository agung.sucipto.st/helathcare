<style>
	h4{
		font-weight:bold;
	}
	hr{
		margin-top:5px;
	}
</style>

<div class="row">
	<div class="col-sm-2">
		<?php echo $this->renderPartial('main/view_main_navigation',array('model'=>$model,'registrasi'=>$registrasi)); ?>
	</div>
	<div class="col-sm-10">
		<h4>Berkas</h4>
		<?php
		if($registrasi->status_registrasi=="Aktif"){
		?>
		<div class="box box-primary">
			<div class="box-body">
			
			<?php 
			
			$form=$this->beginWidget('booster.widgets.TbActiveForm',array(
				'id'=>'berkas-pasien-form',
				'enableAjaxValidation'=>false,
	'htmlOptions'=>array('enctype'=>'multipart/form-data'),
			)); ?>
			<div class="row">
				<div class="col-sm-5">
					<?php echo $form->fileFieldGroup($berkas, 'nama_berkas',
						array(
							'wrapperHtmlOptions' => array(
								'class' => 'col-sm-5',
							),
							
						)
					); ?>
				</div>
				<div class="col-sm-5">
					<?php echo $form->dropDownListGroup($berkas,'id_jenis_berkas', array('widgetOptions'=>array('data'=>CHtml::listData(JenisBerkas::model()->findAll(),'id_jenis_berkas','nama_jenis_berkas'), 'htmlOptions'=>array('class'=>'input-large selectpicker show-tick','data-live-search'=>"true",'empty'=>'Jenis Berkas')))); ?>
				</div>
				<div class="col-sm-2">
				<br/>
					<button type="submit" class="btn btn-primary">Simpan</button>
				</div>
			</div>
			<?php 
			$this->endWidget(); 
			?>
			</div>
		</div>
		<?php
		}
		?>	
		<div class="box box-primary">
			<div class="box-body">
				<?php
				if(Yii::app()->user->hasFlash('success')){
					echo'
					<br/>
					<div class="alert alert-success">
						<button type="button" class="close" data-dismiss="alert">&times;</button>
						<i class="fa fa-warning"></i>'.Yii::app()->user->getFlash('success').'
					</div>';
				}
				?>					
				<?php		
				$this->widget('booster.widgets.TbExtendedGridView', array(
					'id'=>'berkas-pasien-grid',
					'type' => 'striped',
					'dataProvider' => $berkas->search(),
					'summaryText'=>false,
					'selectableRows' => 2,
					'responsiveTable' => true,
					'enablePagination' => true,
					'pager' => array(
						'htmlOptions'=>array(
							'class'=>'pagination'
						),
						'maxButtonCount' => 5,
						'cssFile' => true,
						'header' => false,
						'firstPageLabel' => '<<',
						'prevPageLabel' => '<',
						'nextPageLabel' => '>',
						'lastPageLabel' => '>>',
					),
					'columns'=>array(
							array(
								"header"=>"Jenis Berkas",
								"value"=>'$data->idJenisBerkas->nama_jenis_berkas'
							),
							array(
								"header"=>"Berkas",
								"type"=>"raw",
								"value"=>function($data){
									$image=Yii::app()->baseUrl.'/../Upload/'.Lib::MRN($data->idRegistrasi->id_pasien).'/'.$data->nama_berkas;
									return '<a rel="image_gallery" href="'.$image.'">'.$data->nama_berkas.'</a>';
								}
							),
							array(
								"header"=>"User Input",
								"type"=>"raw",
								"value"=>function($data){
									return $data->userCreate->idPegawai->idPersonal->nama_lengkap.'<br/>'.Lib::dateInd($data->time_create,false);
								}
							),
							array(
								'class'=>'booster.widgets.TbButtonColumn',
								 'deleteConfirmation'=>'Anda yakin akan menhapus data?',
								'template'=>'{delete}',
								'buttons'=>array
								(
									'delete' => array
									(
										'label'=>'Delete',
										'icon'=>'trash',
										//'visible'=>'(Lib::accessBy("registrasi","hapusBerkas") AND Registrasi::getStatus($data->id_registrasi)=="Aktif")',
										'url'=>'Yii::app()->createUrl("registrasi/hapusBerkas/id/".$data->id_berkas_pasien)',
										'options'=>array(
											'class'=>'btn btn-default btn-xs delete',
										),
									)
								),
								'header'=>'',
							),
					),
				));
				?>
			</div>
		</div>
	</div>
</div>

<?php
Yii::app()->clientScript->registerScript('icd', '

$("#icd10-pasien-form").submit(function(e) {

    var url = "'.Yii::app()->createUrl('registrasi/icd',array("id"=>$model->id_pasien,"idReg"=>$registrasi->id_registrasi)).'";

    $.ajax({
           type: "POST",
           url: url,
           data: $("#icd10-pasien-form").serialize(),
           success: function(data)
           {
               if(data=="Sukses"){
				  $("#Icd10Pasien_jenis_kasus").val("");
				  $("#Icd10Pasien_order_diagnosa").val("");
				  $("#Icd10Pasien_id_icd10").val("");
				  refreshIcd();
			   }else{
					$("#Icd10Pasien_jenis_kasus").val("");
					$("#Icd10Pasien_order_diagnosa").val("");
					$("#Icd10Pasien_id_icd10").val("");
				   alert(data);
			   }
           }
         });

    e.preventDefault();
});
function refreshIcd(){
	$.fn.yiiGridView.update("icd10-pasien-grid");
}
', CClientScript::POS_END);
?>