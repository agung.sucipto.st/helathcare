<style>
#vSignTable th{
	font-size:8pt;
	background-color:#ccc;
	text-align:Center;
	vertical-align:middle;
	padding:5px 10px 0px 10px;
}
</style>

<?php
$criteria = new CDbCriteria;
$criteria->condition="id_registrasi='$registrasi->id_registrasi'";
$vSign=TandaVitalPasien::model()->findAll($criteria);
$vHead = new TandaVitalPasien;
?>
<table class="table table-bordered table-striped" id="vSignTable">
	<thead>
		<tr>
			<th>Tanggal</th>
			<th><?=$vHead->getAttributeLabel('height');?><br/>Cm</th>
			<th><?=$vHead->getAttributeLabel('weight');?><br/>Kg</th>
			<th>BMI</th>
			<th><?=$vHead->getAttributeLabel('blood_pressure');?><br/>mmHg</th>
			<th><?=$vHead->getAttributeLabel('pulse_rate');?><br/>BPM</th>
			<th><?=$vHead->getAttributeLabel('respiration_rate');?><br/>Breaths/Minute</th>
			<th><?=$vHead->getAttributeLabel('temperature');?><br/><sup>o</sup>C</th>
			<th><?=$vHead->getAttributeLabel('head_circumference');?><br/>Cm</th>
			<th>Ubah / Hapus</th>
		</tr>
	</thead>
	<tbody>
		<?php
		if($vSign){
			foreach($vSign as $row){
				echo'
					<tr>
						<td>'.Lib::dateInd($row->time_create, false).'</td>
						<td>'.$row->height.'</td>
						<td>'.$row->weight.'</td>
						<td>';
							$per=pow((($row->height)/100),2);
							@$bmi=number_format($row->weight/$per,2);
							$status='';
							if($bmi<18.5){
								$status="Underweight";
							}elseif($bmi>=18.5 and $bmi<24.9 ){
								$status="Healty";
							}elseif($bmi>=20 and $bmi<29.9 ){
								$status="Overweight";
							}elseif($bmi>=30 ){
								$status="Obese";
							}
							echo $bmi." ( $status )";
						echo'
						</td>
						<td>'.$row->blood_pressure.'</td>
						<td>'.$row->pulse_rate.'</td>
						<td>'.$row->respiration_rate.'</td>
						<td>'.$row->temperature.'</td>
						<td>'.$row->head_circumference.'</td>
						<td>';
						echo'
							<a href="'.Yii::app()->createUrl('registrasi/dataDasar',array("id"=>$registrasi->id_pasien,"idReg"=>$registrasi->id_registrasi,'idVital'=>$row->id_tanda_vital,'hapus'=>'ya')).'" class="btn btn-xs btn-danger" onClick="return confirm(\'Apakah Data Akan Dihapus?\');"><i class="fa fa-times"></i></a>
						';
						echo'
							<a href="'.Yii::app()->createUrl('registrasi/dataDasar',array("id"=>$registrasi->id_pasien,"idReg"=>$registrasi->id_registrasi,'idVital'=>$row->id_tanda_vital)).'" class="btn btn-xs btn-success" onClick="return confirm(\'Apakah Data Akan Diedit?\');"><i class="fa fa-pencil"></i></a>
						';
						echo'</td>
					</tr>
				';
			}
		}else{
			echo'
			<tr>
				<td colspan="10">Tidak ada riwayat Tanda Vital Pasien</td>
			</tr>';
		}
		?>
	</tbody>
</table>
<hr />

<?php
			if(Yii::app()->user->hasFlash('success')){
				echo'
				<br/>
				<div class="alert alert-success">
					<button type="button" class="close" data-dismiss="alert">&times;</button>
					<i class="fa fa-warning"></i>'.Yii::app()->user->getFlash('success').'
				</div>';
			}
			?>
			<?php $form=$this->beginWidget('booster.widgets.TbActiveForm',array(
				'id'=>'tanda-vital-pasien-form',
				'enableAjaxValidation'=>false,
			)); ?>
			<div class="row">
				<div class="col-sm-6">
					<?php echo $form->numberFieldGroup($vitalSign,'height',array('widgetOptions'=>array('htmlOptions'=>array('class'=>'span5','maxlength'=>30,'step'=>'0.01','min'=>'1')),'append'=>'Cm')); ?>
				</div>
				<div class="col-sm-6">
					<?php echo $form->numberFieldGroup($vitalSign,'weight',array('widgetOptions'=>array('htmlOptions'=>array('class'=>'span5','maxlength'=>30,'step'=>'0.01','min'=>'1')),'append'=>'Kg')); ?>
				</div>
			</div>
			
			<div class="row">
				<div class="col-sm-6">
					<?php echo $form->textFieldGroup($vitalSign,'blood_pressure',array('widgetOptions'=>array('htmlOptions'=>array('class'=>'span5','maxlength'=>30)),'append'=>'mm/Hg')); ?>
				</div>
				<div class="col-sm-6">
					<?php echo $form->textFieldGroup($vitalSign,'pulse_rate',array('widgetOptions'=>array('htmlOptions'=>array('class'=>'span5','maxlength'=>30)),'append'=>'BPM')); ?>
				</div>
			</div>
			<div class="row">
				<div class="col-sm-6">
					<?php echo $form->textFieldGroup($vitalSign,'respiration_rate',array('widgetOptions'=>array('htmlOptions'=>array('class'=>'span5','maxlength'=>30)),'append'=>'Breaths/Minute')); ?>
				</div>
				<div class="col-sm-6">
					<?php echo $form->numberFieldGroup($vitalSign,'temperature',array('widgetOptions'=>array('htmlOptions'=>array('class'=>'span5','maxlength'=>30,'step'=>'0.01','min'=>'1')),'append'=>'<sup>o</sup>C')); ?>
				</div>
			</div>
			<div class="row">
				<div class="col-sm-6">
					<?php echo $form->numberFieldGroup($vitalSign,'head_circumference',array('widgetOptions'=>array('htmlOptions'=>array('class'=>'span5','maxlength'=>30,'step'=>'0.01')),'append'=>'Cm')); ?>
				</div>
			</div>
			<div class="form-actions">
				<?php $this->widget('booster.widgets.TbButton', array(
						'buttonType'=>'submit',
						'context'=>'primary',
						'label'=>$vitalSign->isNewRecord ? 'Tambah Data Pasien' : 'Simpan',
					)); 
					
				if(!$vitalSign->isNewRecord){
					echo'
					<a href="'.Yii::app()->createUrl('registrasi/dataDasar',array("id"=>$registrasi->id_pasien,"idReg"=>$registrasi->id_registrasi)).'" class="btn btn-danger">Batal</a>';
				}
				?>
			</div>
			<?php $this->endWidget(); ?>