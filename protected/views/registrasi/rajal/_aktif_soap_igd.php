<style>
#vSignTable th{
	font-size:8pt;
	background-color:#ccc;
	text-align:Center;
	vertical-align:middle;
	padding:5px 10px 0px 10px;
}
</style>

<div class="nav-tabs-custom">
	<ul class="nav nav-tabs">
		<li class="active"><a href="#tab_1" data-toggle="tab">Isi SOAP</a></li>
		<li class=""><a href="#tab_2" data-toggle="tab">Data SOAP</a></li>
	</ul>
	<div class="tab-content">
		<div class="tab-pane active" id="tab_1">
			<?php
			$criteria = new CDbCriteria;
			$criteria->with=array('idRegistrasi');
			$criteria->condition="idRegistrasi.id_pasien='$registrasi->id_pasien'";
			$criteria->order="idRegistrasi.waktu_registrasi DESC";
			$criteria->limit=4;
			$vitalSign=TandaVitalPasien::model()->findAll($criteria);
			$vHead = new TandaVitalPasien;
			?>
			<table class="table table-bordered table-striped" id="vSignTable">
				<thead>
					<tr>
						<th>Tanggal</th>
						<th><?=$vHead->getAttributeLabel('height');?><br/>Cm</th>
						<th><?=$vHead->getAttributeLabel('weight');?><br/>Kg</th>
						<th>BMI</th>
						<th><?=$vHead->getAttributeLabel('blood_pressure');?><br/>mmHg</th>
						<th><?=$vHead->getAttributeLabel('pulse_rate');?><br/>BPM</th>
						<th><?=$vHead->getAttributeLabel('respiration_rate');?><br/>Breaths/Minute</th>
						<th><?=$vHead->getAttributeLabel('temperature');?><br/><sup>o</sup>C</th>
						<th><?=$vHead->getAttributeLabel('head_circumference');?><br/>Cm</th>
					</tr>
				</thead>
				<tbody>
					<?php
					if($vitalSign){
						foreach($vitalSign as $row){
							echo'
								<tr>
									<td>'.Lib::dateInd($row->time_create, false).'</td>
									<td>'.$row->height.'</td>
									<td>'.$row->weight.'</td>
									<td>';
										$per=pow((($row->height)/100),2);
										@$bmi=number_format($row->weight/$per,2);
										$status='';
										if($bmi<18.5){
											$status="Underweight";
										}elseif($bmi>=18.5 and $bmi<24.9 ){
											$status="Healty";
										}elseif($bmi>=20 and $bmi<29.9 ){
											$status="Overweight";
										}elseif($bmi>=30 ){
											$status="Obese";
										}
										echo $bmi." ( $status )";
									echo'
									</td>
									<td>'.$row->blood_pressure.'</td>
									<td>'.$row->pulse_rate.'</td>
									<td>'.$row->respiration_rate.'</td>
									<td>'.$row->temperature.'</td>
									<td>'.$row->head_circumference.'</td>
								</tr>
							';
						}
					}else{
						echo'
						<tr>
							<td colspan="9">Tidak ada riwayat Tanda Vital Pasien</td>
						</tr>';
					}
					?>
				</tbody>
			</table>
			<hr />
			<?php $form=$this->beginWidget('booster.widgets.TbActiveForm',array(
				'id'=>'soap-pasien-form',
				'enableAjaxValidation'=>false,
			)); ?>
			<div class="row">
				<div class="col-sm-6">
					<?php echo $form->textAreaGroup($soap,'subjektif', array('widgetOptions'=>array('htmlOptions'=>array('rows'=>3, 'cols'=>50, 'class'=>'span8')))); ?>
				</div>
				<div class="col-sm-6">
					<?php echo $form->textAreaGroup($soap,'objektif', array('widgetOptions'=>array('htmlOptions'=>array('rows'=>3, 'cols'=>50, 'class'=>'span8')))); ?>
				</div>
			</div>
			<div class="row">
				<div class="col-sm-6">
					<?php echo $form->textAreaGroup($soap,'assesment', array('widgetOptions'=>array('htmlOptions'=>array('rows'=>3, 'cols'=>50, 'class'=>'span8')))); ?>
				</div>
				<div class="col-sm-6">
					<?php echo $form->textAreaGroup($soap,'planing', array('widgetOptions'=>array('htmlOptions'=>array('rows'=>3, 'cols'=>50, 'class'=>'span8')))); ?>
				</div>
			</div>
			<div class="row">
				<div class="col-sm-6">
					<?php echo $form->textAreaGroup($soap,'instruksi', array('widgetOptions'=>array('htmlOptions'=>array('rows'=>3, 'cols'=>50, 'class'=>'span8')))); ?>
				</div>
				<div class="col-sm-6">
					<?php echo $form->dropDownListGroup($soap,'id_pegawai', array('widgetOptions'=>array('data'=>CHtml::listData(Pegawai::model()->findAll(array("with"=>array("idPersonal"),"condition"=>"jenis_pegawai='Medis'")),'id_pegawai','idPersonal.nama_lengkap'), 'htmlOptions'=>array('class'=>'input-large selectpicker show-tick','data-live-search'=>"true",'empty'=>'Pilih Tenaga Medis',)))); ?>
				</div>
			</div>
			<div class="form-actions">
				<?php $this->widget('booster.widgets.TbButton', array(
						'buttonType'=>'submit',
						'context'=>'primary',
						'label'=>$model->isNewRecord ? 'Tambah Data Pasien' : 'Simpan',
					)); 
					?>
			</div>
			<?php $this->endWidget(); ?>
		</div><!-- /.tab-pane -->
		<div class="tab-pane" id="tab_2">
			<?php
			foreach($registrasi->soapPasiens as $row){
				echo '
					<div class="box box-success">
						<div class="box-body">
							 <h4 class="pull-left">'.$row->idPegawai->idPersonal->nama_lengkap.' ('.$row->idPegawai->fungsional.')</h4>
							 <h6 class="pull-right">'.Lib::dateInd($row->time_create).' ('.$row->userCreate->idPegawai->idPersonal->nama_lengkap.')</h6>
							 <div style="clear:both;"></div>
							 <form action="" method="POST" id="soapForm-'.$row->id_soap_pasien.'" style="display: none">
								<div class="row">
									<div class="col-sm-6">
										<div class="form-group">
											<label>Subjektif</label>
											<textarea name="EditSoap[subjektif]" class="form-control" rows="4">'.$row->subjektif.'</textarea>
										</div>
									</div>
									<div class="col-sm-6">
										<div class="form-group">
											<label>Objektif</label>
											<textarea name="EditSoap[objektif]" class="form-control" rows="4">'.$row->objektif.'</textarea>
										</div>
									</div>
								</div>
								<div class="row">
									<div class="col-sm-6">
										<div class="form-group">
											<label>Assesment</label>
											<textarea name="EditSoap[assesment]" class="form-control" rows="4">'.$row->assesment.'</textarea>
										</div>
									</div>
									<div class="col-sm-6">
										<div class="form-group">
											<label>Planning</label>
											<textarea name="EditSoap[planing]" class="form-control" rows="4">'.$row->planing.'</textarea>
										</div>
									</div>
								</div>
								<div class="row">
									<div class="col-sm-6">
										<div class="form-group">
											<label>Instruksi</label>
											<textarea name="EditSoap[instruksi]" class="form-control" rows="4">'.$row->instruksi.'</textarea>
											<input type="hidden" name="EditSoap[id_soap_pasien]" value="'.$row->id_soap_pasien.'" />
										</div>
									</div>
								</div>
								<div class="row">
									<div class="col-sm-12">
										<button type="submit" class="btn btn-primary">
											Simpan Perubahan
										</button>
									</div>
								</div>
							 </form>
							 <div id="dataSoap-'.$row->id_soap_pasien.'">
								 <div class="row">
									<div class="col-sm-6">
										<b>Subjektif</b><br/>'.$row->subjektif.'
									</div>
									<div class="col-sm-6">
										<b>Objektif</b><br/>'.$row->objektif.'
									</div>
								 </div>
								 <div class="row">
									<div class="col-sm-6">
										<b>Assesment</b><br/>'.$row->assesment.'
									</div>
									<div class="col-sm-6">
										<b>Planning</b><br/>'.$row->planing.'
									</div>
								 </div>
								  <div class="row">
									<div class="col-sm-12">
										<b>Instruksi</b><br/>'.$row->instruksi.'
									</div>
								 </div>
							</div>
							 <a href="'. Yii::app()->createUrl('soapPasien/delete',array("id"=>$registrasi->id_pasien,"idReg"=>$registrasi->id_registrasi,'idSoap'=>$row->id_soap_pasien)).'" class="pull-right btn btn-xs btn-danger" onClick="return confirm(\'Apakah Yakin Akan Menghapus SOAP?\');" style="display:'.((Lib::accessBy('soapPasien','delete')==true)?"block":"none").'">Hapus</a>
							 
							 <button class="pull-right btn btn-xs btn-warning" onClick="toggleForm('.$row->id_soap_pasien.')" id="editId-'.$row->id_soap_pasien.'">Edit</button>
							 <div style="clear:both;"></div>
						</div>
					</div>
				';
			}
			?>
		</div><!-- /.tab-pane -->
	</div><!-- /.tab-content -->
</div>


<?php
Yii::app()->clientScript->registerScript('edit-soap', '
	function toggleForm(id) {
		$("#soapForm-"+id).toggle();
		$("#dataSoap-"+id).toggle();
		var label = $("#editId-"+id).html();
		if( label == "Edit") {
			$("#editId-"+id).html("Batal Edit");
		} else {
			$("#editId-"+id).html("Edit")
		}
	}
', CClientScript::POS_END);
?>