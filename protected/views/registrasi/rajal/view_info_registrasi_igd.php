<style>
	h4{
		font-weight:bold;
	}
	hr{
		margin-top:5px;
	}
</style>

<div class="row">
	<div class="col-sm-2">
		<?php echo $this->renderPartial('main/view_main_navigation',array('model'=>$model,'registrasi'=>$registrasi)); ?>
	</div>
	<div class="col-sm-10">
		<h4>Informasi Registrasi</h4>
		<div class="box box-primary">
			<div class="box-body">
			<?php $this->widget('booster.widgets.TbDetailView',array(
				'data'=>$registrasi,
					'attributes'=>array(		
					array(
						"label"=>$registrasi->getAttributeLabel('no_registrasi'),
						"value"=>$registrasi->no_registrasi
					),
					array(
						"label"=>$registrasi->getAttributeLabel('waktu_registrasi'),
						"type"=>"raw",
						"value"=> function($data){
							$time = Lib::dateInd($data->waktu_registrasi);
							$time.='<br/><a href="#" onClick="editWaktu();">Edit Waktu Registrasi</a>';
							return $time;
						}
					),
					array(
						"label"=>$registrasi->getAttributeLabel('status_registrasi'),
						"value"=>$registrasi->status_registrasi
					),
					array(
						"label"=>$registrasi->getAttributeLabel('id_departemen'),
						"value"=>$registrasi->idDepartemen->nama_departemen
					),
					array(
						"label"=>"Rujukan",
						"type"=>"raw",
						"value"=>function($registrasi){
							$rujukan='';
							if($registrasi->rujukan=="Inisiatif Sendiri"){
								$rujukan.="Umum";
							}elseif($registrasi->rujukan=="Luar"){
								$rujukan.=$registrasi->rujukan." : ".$registrasi->idFaskes->nama_faskes;
							}elseif($registrasi->rujukan=="Dalam"){
								$rujukan.=$registrasi->rujukan." : ".$registrasi->idPegawaiPerujuk->idPersonal->nama_lengkap;
							}
							$rujukan.='<br/><a href="">Edit Rujukan</a>';
							return $rujukan;
						}
					),
					array(
						"label"=>"Jaminan",
						"type"=>"raw",
						"value"=>function($registrasi){
							$jaminan='';
							if($registrasi->jenis_jaminan=="UMUM"){
								$jaminan.="Umum";
							}else{
								foreach($registrasi->jaminanPasiens as $row){
									$jaminan.='<b>'.$row->idPenjamin->nama_penjamin.'</b><br/>';
									$jaminan.=$row->no_kartu.'<br/>';
									$jaminan.=$row->nama_pemegang_kartu.'<br/>';
									$jaminan.=$row->kelas_jaminan.'<br/>';
									$jaminan.=(empty($row->no_eligibilitas)?"":$row->tgl_eligibilitas.'/'.$row->no_eligibilitas.'<br/>');
								}
							}
							$jaminan.='<br/><a href="">Edit Penjamin</a>';
							return $jaminan;
						}
					),
					array(
						"label"=>"Dokter",
						"type"=>"raw",
						"value"=>function($registrasi){
							$dokter='';
							foreach($registrasi->dpjps as $row){
								$dokter.=$row->idDokter->idPersonal->nama_lengkap.'<br/>';
							}
							$dokter.='<a href="">Edit Dokter</a>';
							return $dokter;
						}
					),
					array(
						"label"=>$registrasi->getAttributeLabel('catatan'),
						"value"=>$registrasi->catatan
					),
				),
			)); ?>
			</div>
		</div>
		<div class="box box-primary">
			<div class="box-body">
				<a href="" class="btn btn-xs btn-primary" onCLick="labelRM()">Label RM</a>
				<a href="" class="btn btn-xs btn-primary" onCLick="kartuPasien()">Kartu Pasien</a>
				<a href="" class="btn btn-xs btn-primary" onCLick="slipDokter()">Slip Dokter</a>
			</div>
		</div>
		<div class="box box-primary">
			<div class="box-body">
				<?php $this->widget('booster.widgets.TbDetailView',array(
					'data'=>$registrasi,
						'attributes'=>array(
							array(
								"label"=>"Didaftarkan Oleh",
								"value"=>$registrasi->userCreate->idPegawai->idPersonal->nama_lengkap.' / '.Lib::dateInd($registrasi->time_create)
							),
							array(
								"label"=>"Diperbarui Oleh",
								"value"=>function($registrasi){
									if($registrasi->user_update!=''){
										return $registrasi->userUpdate->idPegawai->idPersonal->nama_lengkap.' / '.Lib::dateInd($registrasi->time_update);
									}
								}
							),
						),
				)); ?>
			</div>
		</div>
	</div>
</div>


<?php
Yii::app()->clientScript->registerScript('validatexxxx', '
function labelRM(){
	var left = (screen.width/2)-(300/2);
	var top = (screen.height/2)-(400/2);
	window.open("'.Yii::app()->createUrl('registrasi/cetakLabelRM',array("id"=>$registrasi->id_registrasi)).'","","width=300,height=400,top="+top+",left="+left);
}
function kartuPasien(){
	var left = (screen.width/2)-(500/2);
	var top = (screen.height/2)-(300/2);
	window.open("'.Yii::app()->createUrl('registrasi/kartuPasien',array("id"=>$registrasi->id_registrasi)).'","","width=500,height=300,top="+top+",left="+left);
}

function editWaktu(){
	var left = (screen.width/2)-(500/2);
	var top = (screen.height/2)-(500/2);
	window.open("'.Yii::app()->createUrl('registrasi/editWaktu',array("id"=>$registrasi->id_registrasi)).'","","width=500,height=500,top="+top+",left="+left);
}

function slipDokter(){
	var left = (screen.width/2)-(800/2);
	var top = (screen.height/2)-(400/2);
	window.open("'.Yii::app()->createUrl('registrasi/slipDokter',array("id"=>$registrasi->id_registrasi)).'","","width=800,height=400,top="+top+",left="+left);
}
', CClientScript::POS_END);
?>