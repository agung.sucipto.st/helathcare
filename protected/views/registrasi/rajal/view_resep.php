<style>
	h4{
		font-weight:bold;
	}
	hr{
		margin-top:5px;
	}
</style>

<div class="row">
	<div class="col-sm-2">
		<?php echo $this->renderPartial('main/view_main_navigation',array('model'=>$model,'registrasi'=>$registrasi)); ?>
	</div>
	<div class="col-sm-10">
		<h4>Resep Elektronik</h4>
		<div class="box box-primary">
			<div class="box-body">
				<?php
				if(Yii::app()->user->hasFlash('success')){
					echo'
					<br/>
					<div class="alert alert-success">
						<button type="button" class="close" data-dismiss="alert">&times;</button>
						<i class="fa fa-warning"></i>'.Yii::app()->user->getFlash('success').'
					</div>';
				}
				?>					
				<?php		
				$this->widget('booster.widgets.TbExtendedGridView', array(
					'id'=>'resep-pasien-grid',
					'type' => 'striped',
					'dataProvider' => $resep->search(),
					'summaryText'=>false,
					'selectableRows' => 2,
					'responsiveTable' => true,
					'enablePagination' => true,
					'pager' => array(
						'htmlOptions'=>array(
							'class'=>'pagination'
						),
						'maxButtonCount' => 5,
						'cssFile' => true,
						'header' => false,
						'firstPageLabel' => '<<',
						'prevPageLabel' => '<',
						'nextPageLabel' => '>',
						'lastPageLabel' => '>>',
					),
					'columns'=>array(
							array(
								"header"=>"Waktu Resep",
								"value"=>'Lib::dateInd($data->waktu_resep,false)'
							),
							array(
								"header"=>"Resep",
								"type"=>"raw",
								"value"=>function($data){
									$return="<b>$data->no_resep</b><ul>";
									foreach($data->resepPasienLists as $row){
										$return.="<li>R/ ".$row->idItem->nama_item." : ".$row->jumlah." ".Item::getParentUnit($row->id_item)->idSatuan->nama_satuan."</li>";
									}
									$return.="</ul>";
									return $return;
								}
							),
							array(
								"header"=>"Dokter",
								"value"=>function($data){
									return $data->idDokter->idPersonal->nama_lengkap;
								}
							),
							array(
								"header"=>"Status",
								"value"=>'$data->status_resep'
							),
							array(
								"header"=>"User Input",
								"type"=>"raw",
								"value"=>function($data){
									return $data->userCreate->idPegawai->idPersonal->nama_lengkap.'<br/>'.Lib::dateInd($data->time_create,false);
								}
							),
							array(
								'class'=>'booster.widgets.TbButtonColumn',
								 'deleteConfirmation'=>'Anda yakin akan menhapus data?',
								'template'=>'{delete}',
								'buttons'=>array
								(
									'update' => array
									(
										'label'=>'Update',
										'icon'=>'pencil',
										'visible'=>'Lib::accessBy(\'registrasi\',\'ubahResep\')',
										'options'=>array(
											'class'=>'btn btn-default btn-xs',
										),
									),
									'delete' => array
									(
										'label'=>'Delete',
										'icon'=>'trash',
										'url'=>'Yii::app()->createUrl("resepPasien/delete",array("id"=>$data->id_resep_pasien))',
										'visible'=>'(Lib::accessBy(\'resepPasien\',\'delete\') AND $data->status_resep=="Belum Diproses" AND Registrasi::getStatus($data->id_registrasi)=="Aktif")',
										'options'=>array(
											'class'=>'btn btn-default btn-xs delete',
										),
									)
								),
								'header'=>'',
							),
					),
				));
				?>
			</div>
		</div>
		<?php
		if($registrasi->status_registrasi=="Aktif"){
		?>
		<button onClick="openResep()" class="btn btn-primary pull-right" style="display:<?php echo ((Lib::accessBy('registrasi','buatResep')==true)?"block":"none");?>">
			<i class="fa fa-plus"></i> Buat Resep
		</button>
		<?php
		}
		?>
	</div>
</div>

<?php
Yii::app()->clientScript->registerScript('resep', '

function openResep(){
	window.open("'.Yii::app()->createUrl('registrasi/buatResep',array("id"=>$model->id_pasien,"idReg"=>$registrasi->id_registrasi)).'","","width=1200,height=600");
}
function refreshResep(){
	$.fn.yiiGridView.update("resep-pasien-grid");
}
', CClientScript::POS_END);
?>