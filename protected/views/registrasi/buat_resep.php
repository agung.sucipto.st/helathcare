<?php
if($event=="close"){
?>
<script language="javascript">
	window.opener.refreshResep();
	window.self.close();
</script>
<?php
}
?>

<div class="box box-primary">
	<div class="box-body">
		<div class="row">
			<div class="col-sm-1">
				<?php
				$img='';
				$icon='';
				if($model->idPersonal->jenis_kelamin=='Perempuan'){
					$img=Yii::app()->theme->baseUrl.'/assets/images/female.png';
					$icon='<i class="fa fa-venus" style="color:red;font-weight:bold;"></i>';
				}else{
					$img=Yii::app()->theme->baseUrl.'/assets/images/male.png';
					$icon='<i class="fa fa-mars" style="color:blue;font-weight:bold;"></i>';
				}	
				echo'<img src="'.$img.'" class="img-responsive"/>';
				?>
			</div>
			<div class="col-sm-7">
				<div class="row">
					<div class="col-sm-12" style="font-size:25px;font-weight:bold;">
					<?php
						echo Lib::MRN($model->id_pasien).' - '.$icon.strtoupper($model->idPersonal->nama_lengkap).", ".$model->panggilan;	
					?>
					</div>
				</div>
				<div class="row">
					<div class="col-sm-12">
					<?php
						echo $model->idPersonal->jenis_kelamin.' / '.Lib::dateInd($model->idPersonal->tanggal_lahir,false).", ".Lib::umurDetail($model->idPersonal->tanggal_lahir);	
					?>
					<br/>
					<span class="text-danger text-bold">Alergi</span> : <?=$model->idPersonal->alergi;?>
					</div>					
				</div>
				
			</div>
			<div class="col-sm-4">
				<div class="row">
					<div class="col-sm-4 text-danger text-bold">No Registrasi</div>
					<div class="col-sm-8">: <?=$registrasi->no_registrasi;?></div>
				</div>
				<div class="row">
					<div class="col-sm-4 text-danger text-bold">Waktu Registrasi</div>
					<div class="col-sm-8">: <?=Lib::dateInd($registrasi->waktu_registrasi,false);?></div>
				</div>
				<div class="row">
					<div class="col-sm-4 text-danger text-bold">Dokter</div>
					<div class="col-sm-8">: 
					<?php
					foreach($registrasi->dpjps as $row){
						echo $row->idDokter->idPersonal->nama_lengkap.'<br/>';
					}
					?>
					</div>
				</div>
				<div class="row">
					<div class="col-sm-4 text-danger text-bold">Layanan</div>
					<div class="col-sm-8">: <?=$registrasi->idDepartemen->nama_departemen;?></div>
				</div>
				<div class="row">
					<div class="col-sm-4 text-danger text-bold">Jenis Jaminan</div>
					<div class="col-sm-8">: 
					<?php
						if($registrasi->jenis_jaminan=="UMUM"){
							echo "Umum";
						}else{
							foreach($registrasi->jaminanPasiens as $row){
								echo $row->idPenjamin->nama_penjamin.'<br/>';
							}
						}
					?>
					</div>
				</div>
				<div class="row">
					<div class="col-sm-4 text-danger text-bold">Catatan</div>
					<div class="col-sm-8">: <?=$registrasi->catatan;?></div>
				</div>
			</div>
		</div>
	</div>
</div>

<div class="box box-primary">
	<div class="box-body">
		<?php $form=$this->beginWidget('booster.widgets.TbActiveForm',array(
				'id'=>'resep-pasien-form',
				'enableAjaxValidation'=>false,
			)); ?>
		<div class="row">
			<div class="col-sm-6">
				<label>Cari Nama Obat</label>
				<?php
				$this->widget('zii.widgets.jui.CJuiAutoComplete',array(
					'name'=>'resep',
					'options'=>array(
						'minLength'=>3,
						'showAnim'=>'fold',
						'select'=>"js:function(event, data) {
							var item=data.item.label;
							addRow(item);
						}"
					),
					'source'=>$this->createUrl("registrasi/getObat"),
					'htmlOptions'=>array(
						'class'=>"form-control",
						"id"=>"searchResep"
					),
				));
				?>
			</div>
			
			<div class="col-sm-6">
				<?php echo $form->dropDownListGroup($resep,'id_dokter', array('widgetOptions'=>array('data'=>CHtml::listData(Pegawai::model()->findAll(array("with"=>array("idPersonal"),"condition"=>"fungsional='Dokter'")),'id_pegawai','idPersonal.nama_lengkap'), 'htmlOptions'=>array('class'=>'input-large selectpicker show-tick','data-live-search'=>"true",'empty'=>'Pilih Dokter',)))); ?>
				<?php echo $form->textAreaGroup($resep,'racikan', array('widgetOptions'=>array('htmlOptions'=>array('rows'=>3, 'cols'=>50, 'class'=>'span8')))); ?>
			</div>
		</div>
		
		<table class="table table-stripped">
			<tr>
				<th>Nama Obat</th>
				<th>Stok</th>
				<th width="100px">Qty</th>
				<th>Satuan</th>
				<th>Harga</th>
				<th>Signa</th>
				<th></th>
			</tr>
			<tbody id="data">
			
			</tbody>
			<tr>
				<th colspan="5">Total Rp.</th>
				<th>-</th>
				<th></th>
			</tr>
		</table>
		
		<hr>
		<div class="form-actions">
			<?php $this->widget('booster.widgets.TbButton', array(
					'buttonType'=>'submit',
					'context'=>'primary',
					'htmlOptions'=>array(
						//"disabled"=>true,
						"id"=>"submit"
					),
					'label'=>$model->isNewRecord ? 'Tambah Data Pasien' : 'Simpan',
				)); 
				?>
		</div>
		<?php $this->endWidget(); ?>
	</div>
</div>

<?php
Yii::app()->clientScript->registerScript('addResep', '
var rowResep=[];
cekSubmit();
function deleteRow(id){
	$("#row"+id).remove();
	rowResep.splice(id, 1);
	cekSubmit();
}

function cekSubmit(){
	console.log(rowResep);
	console.log(rowResep.length);
}

function addRow(item){
	$.ajax({
		url: "'.Yii::app()->createAbsoluteUrl('registrasi/getObat').'",
		cache: false,
		type: "POST",
		data:"item="+item,
		success: function(msg){
			var data=$.parseJSON(msg);
			if(rowResep[data.id_item] == undefined){
				cekSubmit();
				rowResep[data.id_item]=data.nama_item;
				$("#searchResep").val("");
				var inputItem="<input type=\"hidden\" name=\"id_item[]\" value=\""+data.id_item+"\"/>"+data.nama_item;
				var stokItem=data.stok;
				var qty="<input type=\"number\" name=\"qty"+data.id_item+"\" value=\"\" class=\"form-control\" required=\"required\"/>";
				var satuan=data.satuan;
				var harga=data.harga;
				var signa="<input type=\"text\" name=\"signa"+data.id_item+"\" id=\"signa"+data.id_item+"\" value=\"\" class=\"form-control\"/>";
				var remove="<span class=\"btn btn-danger\" onclick=\"deleteRow("+data.id_item+")\"><i class=\"fa fa-times\"></i></span>";
				$("#data").append("<tr id=\"row"+data.id_item+"\"><td>"+inputItem+"</td><td>"+stokItem+"</td><td>"+qty+"</td><td>"+satuan+"</td><td>"+harga+"</td><td>"+signa+"</td><td>"+remove+"</td></tr>");
				
				jQuery("#signa"+data.id_item).autocomplete({"minLength":3,"showAnim":"fold","source":"'.Yii::app()->createUrl("registrasi/getSigna").'"});
			}else{
				$("#searchResep").val("");
			}
		}
	});
}
', CClientScript::POS_END);
?>