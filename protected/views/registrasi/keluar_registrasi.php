<?php
if($event=="close"){
?>
<script language="javascript">
	window.opener.refreshRegister();
	window.self.close();
</script>
<?php
}
?>

<link rel="stylesheet" href="<?php echo Yii::app()->theme->baseUrl; ?>/assets/css/bootstrap-select.css">
<script src="<?php echo Yii::app()->theme->baseUrl; ?>/assets/js/bootstrap-select.js"></script>

<div class="box box-primary">
	<div class="box-body">
		<h4 class="box-title">Pasien Akan Dipulangkan</h4>
		<hr>
		<?php
		if(Yii::app()->user->hasFlash('error')){
			echo'
			<br/>
			<div class="alert alert-danger">
				<button type="button" class="close" data-dismiss="alert">&times;</button>
				<i class="fa fa-warning"></i>'.Yii::app()->user->getFlash('error').'
			</div>';
		}
		?>
		<?php $form=$this->beginWidget('booster.widgets.TbActiveForm',array(
			'id'=>'registrasi-form',
			'enableAjaxValidation'=>true,
		)); ?>
		
		<?php echo $form->dropDownListGroup($registrasi,'status_medis', array('widgetOptions'=>array('data'=>array("Sembuh"=>"Sembuh","Membaik"=>"Membaik","Memburuk"=>"Memburuk","Meninggal"=>"Meninggal"), 'htmlOptions'=>array('class'=>'input-large','empty'=>'Pilih Status Medis')))); ?>
		<?php echo $form->dropDownListGroup($registrasi,'status_keluar', array('widgetOptions'=>array('data'=>array("Normal"=>"Normal","Rujuk Internal"=>"Rujuk Internal","Rujuk External"=>"Rujuk External","PAPS"=>"Pulang Atas Permintaan Sendiri","Rawat Inap"=>"Rawat Inap","Meninggal"=>"Meninggal"), 'htmlOptions'=>array('class'=>'input-large','empty'=>'Pilih Status Keluar')))); ?>
		<label>
		Password ( <?=$model->username;?> )
		</label>
		<?php echo $form->passwordField($model,'password', array("label"=>"Password",'class'=>'form-control', 'id'=>'pwd', 'size'=>'100', 'placeholder'=>'Password', 'required'=>'required')); ?>
		<br/>
		
		<div id="Rujukan" style="display:none;">
			<?php echo $form->dropDownListGroup($registrasi,'id_faskes_tujuan_rujuk', array('widgetOptions'=>array('data'=>Faskes::getList(), 'htmlOptions'=>array('class'=>'input-large selectpicker show-tick','data-live-search'=>"true",'empty'=>"Pilih Faskes")))); ?>
		</div>
		
		<div id="Meninggal" style="display:none;">
			<?php echo $form->dateTimePickerGroup($registrasi,'waktu_meninggal',array('widgetOptions'=>array('options'=>array('format'=>'yyyy-mm-dd hh:ii:ss','viewFormat'=>'yyyy-mm-dd hh:ii:ss'),'htmlOptions'=>array('value'=>date('Y-m-d H:i:s'))))) ;?>
			<?php echo $form->dropDownListGroup($registrasi,'cara_meninggal', array('widgetOptions'=>array('data'=>array("DOA"=>"DOA","DNR"=>"DNR","EXITUS"=>"EXITUS"), 'htmlOptions'=>array('class'=>'input-large','empty'=>'Cara Meninggal')))); ?>
			<?php echo $form->dropDownListGroup($registrasi,'lama_meninggal', array('widgetOptions'=>array('data'=>array("> 48 Jam"=>"> 48 Jam","< 48 Jam"=>"< 48 Jam"), 'htmlOptions'=>array('class'=>'input-large','empty'=>'Cara Meninggal')))); ?>
			<?php echo $form->textAreaGroup($registrasi,'keterangan_meninggal', array('widgetOptions'=>array('htmlOptions'=>array('rows'=>2, 'cols'=>50, 'class'=>'span8')))); ?>
		</div>
		<br/>
		<br/>
		<br/>
		<div class="form-actions">
		<?php $this->widget('booster.widgets.TbButton', array(
			'buttonType'=>'submit',
			'context'=>'primary',
			'label'=>$model->isNewRecord ? 'Daftar' : 'Simpan',
		)); 
		echo CHtml::button('Batal', array(
			'class' => 'btn btn-primary',
			'onclick' => "window.self.close();",
				)
		);
		?>
		</div>

		<?php $this->endWidget(); ?>
	</div>
</div>


<?php
Yii::app()->clientScript->registerScript('validate', '
	$("#Registrasi_status_keluar").change(function(){
		var val = $("#Registrasi_status_keluar").val();
		if(val == "Rujuk External"){
			$("#Registrasi_id_faskes_tujuan_rujuk").attr("required","required");
			$("#Rujukan").show();
		} else if(val == "Meninggal"){
			$("#Registrasi_id_faskes_tujuan_rujuk").removeAttr("required");
			$("#Rujukan").hide();
			$("#Registrasi_waktu_meninggal").attr("required","required");
			$("#Registrasi_lama_meninggal").attr("required","required");
			$("#Registrasi_cara_meninggal").attr("required","required");
			$("#Meninggal").show();
		} else {
			$("#Registrasi_id_faskes_tujuan_rujuk").removeAttr("required");
			$("#Registrasi_waktu_meninggal").removeAttr("required");
			$("#Registrasi_lama_meninggal").removeAttr("required");
			$("#Registrasi_cara_meninggal").removeAttr("required");
			$("#Rujukan").hide();
			$("#Meninggal").hide();
		}
	});
', CClientScript::POS_END);
?>