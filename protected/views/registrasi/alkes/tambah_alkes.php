<link rel="stylesheet" href="<?php echo Yii::app()->theme->baseUrl; ?>/assets/css/bootstrap-select.css">
<script src="<?php echo Yii::app()->theme->baseUrl; ?>/assets/js/bootstrap-select.js"></script>
<?php
if($event=="close"){
?>
<script language="javascript">
	window.opener.refresTindakan();
	window.self.close();
</script>
<?php
}
?>
<?php 
if($registrasi->jenis_registrasi=="IGD"){
	echo $this->renderPartial('main/view_main_header', array(
		'model'=>$model,
		'registrasi'=>$registrasi
	));
}elseif($registrasi->jenis_registrasi=="Rawat Jalan"){
	echo $this->renderPartial('main/view_main_header', array(
		'model'=>$model,
		'registrasi'=>$registrasi
	));
}elseif($registrasi->jenis_registrasi=="Rawat Inap"){
	echo $this->renderPartial('main/view_main_header_ranap', array(
		'model'=>$model,
		'registrasi'=>$registrasi
	));
}else{
	echo $this->renderPartial('main/view_main_header', array(
		'model'=>$model,
		'registrasi'=>$registrasi
	));
}
?>
<select id="dataDokter" style="display:none;">
	<option value="">Pilih Dokter</option>
	<?php
	$medis=Pegawai::model()->findAll(array("with"=>array("idPersonal"),"condition"=>"jenis_pegawai='Medis'"));
	foreach($medis as $row){
		echo'<option value="'.$row->id_pegawai.'">'.$row->idPersonal->nama_lengkap.'</option>';
	}
	?>
</select>
<div class="box box-primary">
	<div class="box-body">
		<?php $form=$this->beginWidget('booster.widgets.TbActiveForm',array(
				'id'=>'peralatan-pasien-form',
				'enableAjaxValidation'=>false,
			)); ?>
		<?php echo $form->dateTimePickerGroup($alkes,'waktu_penggunaan_alat',array('widgetOptions'=>array('options'=>array('format'=>'yyyy-mm-dd hh:ii:ss','viewFormat'=>'yyyy-mm-dd hh:ii:ss'),'htmlOptions'=>array('value'=>date('Y-m-d H:i:s'))))) ;?>
		<div class="row">
			<div class="col-sm-4">
				<?php echo $form->dropDownListGroup($alkes,'id_kelompok_peralatan', array('widgetOptions'=>array('data'=>CHtml::listData(KelompokPeralatan::model()->findAll(),'id_kelompok_peralatan','nama_kelompok_peralatan'), 'htmlOptions'=>array('class'=>'input-large selectpicker show-tick','data-live-search'=>"true",'empty'=>'Pilih Kelompok Peralatan')))); ?>
			</div>
			<div class="col-sm-4">
				<?php echo $form->dropDownListGroup($alkes,'id_peralatan', array('widgetOptions'=>array('data'=>CHtml::listData(Peralatan::model()->findAll(array("condition"=>"status='Aktif'")),'id_peralatan','nama_peralatan'), 'htmlOptions'=>array('class'=>'input-large selectpicker show-tick','data-live-search'=>"true",'empty'=>'Pilih Peralatan')))); ?>
			</div>
			<div class="col-sm-4">
				<?php echo $form->dropDownListGroup($alkes,'id_kelas', array('widgetOptions'=>array('data'=>CHtml::listData(Kelas::model()->findAll(),'id_kelas','nama_kelas'), 'htmlOptions'=>array('class'=>'input-large selectpicker show-tick','data-live-search'=>"true",'empty'=>'Pilih Kelas')))); ?>
			</div>
		</div>
		<div class="row">
			<div class="col-sm-12">
				<?php echo $form->numberFieldGroup($alkes,'jumlah_penggunaan',array('widgetOptions'=>array('htmlOptions'=>array('class'=>'span5','maxlength'=>20,"min"=>1,"value"=>1)))); ?>
			</div>
		</div>
		
		

		<div id="peranMedis">
			
		</div>
		
		<div class="row">
			<div class="col-sm-6">
				<?php echo $form->textAreaGroup($alkes,'catatan', array('widgetOptions'=>array('htmlOptions'=>array('rows'=>2, 'cols'=>50, 'class'=>'span8')))); ?>
			</div>
			<div class="col-sm-6">
				<div style="font-size:24pt;"><br/><b>Tarif : </b><span id="harga"></span></div>
			</div>
		</div>
		<input type="hidden" name="PeralatanPasien[id_tarif_peralatan]" id="id_tarif_peralatan"/>
		<div class="form-actions">
			<?php $this->widget('booster.widgets.TbButton', array(
					'buttonType'=>'submit',
					'context'=>'primary',
					'htmlOptions'=>array(
						"id"=>"submit"
					),
					'label'=>$alkes->isNewRecord ? 'Tambah Tindakan' : 'Simpan',
				)); 
				?>
		</div>
		<?php $this->endWidget(); ?>
	</div>
</div>

<?php
Yii::app()->clientScript->registerScript('alkes', '
var tarifAll=0;
var qty=1;

function reCount(){
	var harga = parseInt(tarifAll) * parseInt(qty);
	$("#harga").html(harga);
}

$("#PeralatanPasien_id_peralatan").change(function(){
	var id= $("#PeralatanPasien_id_peralatan").val();
	var kelas= $("#PeralatanPasien_id_kelas").val();
	if(id != "" && kelas != ""){
		getDetailPeralatan(id,kelas);
	}
});

$("#PeralatanPasien_jumlah_penggunaan").change(function(){
	qty = $("#PeralatanPasien_jumlah_penggunaan").val();
	reCount();
});

$("#PeralatanPasien_id_kelompok_peralatan").change(function(){
	var id = $("#PeralatanPasien_id_kelompok_peralatan").val();
	getVal(id,"id_kelompok_peralatan","#PeralatanPasien_id_peralatan","peralatan/getPeralatan");
});

function getVal(value,param,id,url){	
	$.ajax({
		url: "'.Yii::app()->createAbsoluteUrl('/').'/"+url,
		cache: false,
		type: "POST",
		data:"PeralatanPasien["+param+"]="+value,
		success: function(msg){
			$(id).html("");
			$(id).html(msg);
			$(id).selectpicker("refresh");
		}
	});
}

function getDetailPeralatan(id,kelas){
	$.ajax({
		url: "'.Yii::app()->createAbsoluteUrl('peralatan/getDetailPeralatan').'",
		cache: false,
		type: "POST",
		data:"id="+id+"&kelas="+kelas+"&idreg="+'.$registrasi->id_registrasi.',
		success: function(msg){
			var data=$.parseJSON(msg);
			console.log(data);
			tarifAll=data.tarif.tarif_all;
			$("#peranMedis").html("");
			$("#id_tarif_peralatan").val(data.tarif.id);
			reCount();
			var dataOptions = $("#dataDokter").html();
			for(i=0;i<data.medis.length;i++){
				if(data.medis[i].mandatory=="1"){
					$("#peranMedis").append("<div class=\"row\"><div class=\"col-sm-8\"><label>"+data.medis[i].nama_peran+"</label><select class=\"form-control\" id=\"comboDokter"+data.medis[i].id_tarif+"\" data-live-search=\"true\" name=\"PeranMedis["+data.medis[i].id_tarif+"][medis]\" required=\"required\">"+dataOptions+"</select><br/><br/></div><div class=\"col-sm-4\"><label>FOC "+data.medis[i].nama_peran+"</label><select class=\"form-control\" id=\"comboFOC\" name=\"PeranMedis["+data.medis[i].id_tarif+"][foc]\"><option>Tidak</option><option>Ya</option></select></div></div>");
					$("#comboDokter"+data.medis[i].id_tarif).selectpicker("refresh");
				}else{
					$("#peranMedis").append("<div class=\"row\"><div class=\"col-sm-8\"><label>"+data.medis[i].nama_peran+"</label><select class=\"form-control\" id=\"comboDokter"+data.medis[i].id_tarif+"\" data-live-search=\"true\" name=\"PeranMedis["+data.medis[i].id_tarif+"][medis]\">"+dataOptions+"</select><br/><br/></div><div class=\"col-sm-4\"><label>FOC "+data.medis[i].nama_peran+"</label><select class=\"form-control\" id=\"comboFOC\" name=\"PeranMedis["+data.medis[i].id_tarif+"][foc]\"><option>Tidak</option><option>Ya</option></select></div></div>");
					$("#comboDokter"+data.medis[i].id_tarif).selectpicker("refresh");
				}
				
			}
		}
	});
}
', CClientScript::POS_END);
?>