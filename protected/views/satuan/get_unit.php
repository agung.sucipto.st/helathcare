<?php

Yii::app()->clientScript->registerScript('search', "
$('.search-form form').submit(function(){
	$.fn.yiiGridView.update('satuan-grid', {
		data: $(this).serialize()
	});
	return false;
});

function send(id,name){
	window.opener.setUnit(id,name);
	window.self.close();
}
");
?>

<div class="panel panel-default">
	<div class="panel-heading">
		<h3 class="panel-title pull-left">Satuan</h3>
		<div style="clear:both;"></div>
	</div>
	<div class="panel-body">
		<div class="search-form">
			<?php $form=$this->beginWidget('booster.widgets.TbActiveForm',array(
				'action'=>Yii::app()->createUrl($this->route),
				'method'=>'get',
			)); ?>
				<div class="row">
					<div class="col-xs-5">
						<?php echo $form->textFieldGroup($model,'nama_satuan',array('widgetOptions'=>array('htmlOptions'=>array('class'=>'span5','maxlength'=>45)))); ?>
					</div>
					<div class="col-xs-5">
						<?php echo $form->textFieldGroup($model,'alias_satuan',array('widgetOptions'=>array('htmlOptions'=>array('class'=>'span5','maxlength'=>45)))); ?>
					</div>
					<div class="col-xs-2">
						<br/>
						<div class="form-actions">
							<?php $this->widget('booster.widgets.TbButton', array(
								'buttonType' => 'submit',
								'context'=>'primary',
								'label'=>'Cari',
							)); ?>
						</div>
					</div>
				</div>

			<?php $this->endWidget(); ?>

		</div><!-- search-form -->
		
		<style>
		.pagination li.selected a{
			background:rgb(235, 235, 236);
		}
		</style>
		
					
		<div id="example-2_wrapper" class="dataTables_wrapper form-inline dt-bootstrap no-footer">
		
		<?php 
		// put this somewhere on top
		$pageSize=Yii::app()->user->getState('pageSize',Yii::app()->params['defaultPageSize']); ?>		
		<?php		
		$this->widget('booster.widgets.TbExtendedGridView', array(
			'id'=>'satuan-grid',
			'type' => 'striped',
			'dataProvider' => $model->search(),
			
			'summaryText'=>'Menampilkan {start}-{end} dari {count} hasil.',
			'selectableRows' => 2,
			'responsiveTable' => false,
			'enablePagination' => true,
			'pager' => array(
				'htmlOptions'=>array(
					'class'=>'pagination'
				),
				'maxButtonCount' => 5,
				'cssFile' => true,
				'header' => false,
				'firstPageLabel' => '<<',
				'prevPageLabel' => '<',
				'nextPageLabel' => '>',
				'lastPageLabel' => '>>',
			),
			'columns'=>array(
					'nama_satuan',
					'alias_satuan',
					array(
						'value'=>function($data){
							return '<button class="btn btn-sm btn-success" onclick="send(\''.$data->id_satuan.'\',\''.$data->alias_satuan.'\')"><i class="fa fa-check"></i> Pilih</button>';
						},
						"type"=>"raw",
						'header'=>CHtml::dropDownList('pageSize',$pageSize,array(10=>10,20=>20,50=>50,100=>100,200=>200,500=>500,1000=>1000),array(
							'onchange'=>"$.fn.yiiGridView.update('unit-grid',{ data:{pageSize: $(this).val() }})",
						)),
					),
			),
		));
		?>
		</div>
	</div>
</div>