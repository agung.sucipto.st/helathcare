<script>
<?php
if($show==false){
	
	echo '
	window.opener.change();
	window.self.close();';
}
?>
</script>

<div class="panel panel-default">
	<div class="panel-heading">
		<h3 class="panel-title pull-left">Tambah Satuan</h3>
		<div style="clear:both"></div>
	</div>
	<div class="panel-body">
<?php $form=$this->beginWidget('booster.widgets.TbActiveForm',array(
	'id'=>'item-unit-form',
	'enableAjaxValidation'=>false,
)); ?>

<p class="help-block">Isian dengan tanda <span class="required">*</span> wajib diisi.</p>

<?php echo $form->errorSummary($model); ?>

	<?php echo $form->dropDownListGroup($model,'id_satuan', array('widgetOptions'=>array('data'=>CHtml::listData(Satuan::model()->findAll(),'id_satuan','alias_satuan'), 'htmlOptions'=>array('class'=>'input-large','empty'=>'- Choose -')))); ?>

	<?php echo $form->textFieldGroup($model,'nilai_satuan_konversi',array('widgetOptions'=>array('htmlOptions'=>array('class'=>'span5')),'append'=>$parent->idSatuan->alias_satuan)); ?>
	<?php echo $form->dropDownListGroup($model,'status', array('widgetOptions'=>array('data'=>array("Aktif"=>"Aktif","Tidak Aktif"=>"Tidak Aktif"), 'htmlOptions'=>array('class'=>'input-large','empty'=>'- Choose -')))); ?>

<div class="form-actions">
	<?php $this->widget('booster.widgets.TbButton', array(
			'buttonType'=>'submit',
			'context'=>'primary',
			'label'=>$model->isNewRecord ? 'Tambah' : 'Simpan',
		)); 
		echo CHtml::button('Batal', array(
            'class' => 'btn btn-primary',
            'onclick' => "history.go(-1)",
                )
        );
		?>

		</div>

<?php $this->endWidget(); ?>
</div>