<script type="text/javascript" src="<?php echo Yii::app()->theme->baseUrl; ?>/assets/source/jquery.fancybox.js"></script>
   
   <script type="text/javascript">
		$(document).ready(function() {
			$("a[rel=image_gallery]").fancybox({
				'transitionIn'		: 'none',
				'transitionOut'		: 'none',
				'titlePosition' 	: 'over',
				'titleFormat'		: function(title, currentArray, currentIndex, currentOpts) {
				return '<span id="fancybox-title-over">Image ' + (currentIndex + 1) + ' / ' + currentArray.length + (title.length ? ' &nbsp; ' + title : '') + '</span>';
				}
			});
		});
	</script>
	<link rel="stylesheet" href="<?php echo Yii::app()->theme->baseUrl; ?>/assets/source/jquery.fancybox.css" />
<div class="box box-primary">
	<div class="box-body">
		<h4 class="box-title">Riwayat Medis Pasien</h4>
		<hr>
		<div class="row">
			<div class="col-sm-2">
				<?php
				$img='';
				$icon='';
				if($pasien->idPersonal->jenis_kelamin=='Perempuan'){
					$img=Yii::app()->theme->baseUrl.'/assets/images/female.png';
					$icon='<i class="fa fa-venus" style="color:red;font-weight:bold;"></i>';
				}else{
					$img=Yii::app()->theme->baseUrl.'/assets/images/male.png';
					$icon='<i class="fa fa-mars" style="color:blue;font-weight:bold;"></i>';
				}	
				echo'<img src="'.$img.'" class="img-responsive"/>';
				?>
			</div>
			<div class="col-sm-10">
				<div class="row">
					<div class="col-sm-12" style="font-size:25px;font-weight:bold;">
					<?php
						echo Lib::MRN($pasien->id_pasien).' - '.$icon.strtoupper($pasien->idPersonal->nama_lengkap).", ".$pasien->panggilan;	
					?>
					</div>
				</div>
				<div class="row">
					<div class="col-sm-12">
					<?php
						echo $pasien->idPersonal->jenis_kelamin.' / '.Lib::dateInd($pasien->idPersonal->tanggal_lahir,false).", ".Lib::umurDetail($pasien->idPersonal->tanggal_lahir);	
					?>
					</div>
				</div>
				
			</div>
		</div>
		<hr>
		<div class="row">                        
			<div class="col-md-12">
				<?php
				if(count($model)>0){
				?>
				<!-- The time line -->
				<ul class="timeline">
					<?php
					foreach($model as $row){
						echo'
						<!-- timeline time label -->
						<li class="time-label">
							<span class="bg-red">
								'.Lib::dateIndShortMonth($row->waktu_registrasi,true,false).'
							</span>
						</li>
						<!-- /.timeline-label -->
						<!-- timeline item -->
						<li>
							<i class="fa fa-stethoscope bg-blue"></i>
							<div class="timeline-item">
								
								<h3 class="timeline-header"><a href="#">DPJP : </a> '.$row->dpjps[0]->idDokter->idPersonal->nama_lengkap.' <b>[ '.$row->idDepartemen->nama_departemen.' ]</b></h3>
								<div class="timeline-body">
									<table class="table">
										<tr>
											<th>Diagnosa</th>
											<td>';
											foreach($row->icd10Pasiens as $icd){
												echo $icd->idIcd10->kode_icd.' - '.$icd->idIcd10->diagnosa.'<br/>';
											}
											echo'</td>
										</tr>';
										
										echo'
										<tr>
											<th colspan="2">SOAP</th>
										</tr>
										';
										foreach($row->soapPasiens as $soap){
											echo'
											<tr>
												<th>
												'.Lib::dateIndShortMonth($soap->time_create).'<br/>
												<i>'.$soap->idPegawai->idPersonal->nama_lengkap.'</i>
												</th>
												<td>
													<table>
														<tr>
															<th>S</th>
															<td>'.$soap->subjektif.'</td>
														</tr>
														<tr>
															<th>O</th>
															<td>'.$soap->objektif.'</td>
														</tr>
														<tr>
															<th>A</th>
															<td>'.$soap->assesment.'</td>
														</tr>
														<tr>
															<th>P</th>
															<td>'.$soap->planing.'</td>
														</tr>
													</table>
												</td>
											</tr>
											';
										}
										echo'
										<tr>
											<th colspan="2">Resep</th>
										</tr>
										';
										foreach($row->resepPasiens as $resep){
											echo'
											<tr>
												<th>
												'.Lib::dateIndShortMonth($resep->waktu_resep).'<br/>
												<i>'.$resep->idDokter->idPersonal->nama_lengkap.'</i>
												</th>
												<td>
													';
													$return="<b>$resep->no_resep</b><ul>";
													foreach($resep->resepPasienLists as $rl){
														$return.="<li>R/".$rl->idItem->nama_item." ( ".$rl->jumlah." ".Item::getParentUnit($rl->id_item)->idSatuan->nama_satuan." ) ".$rl->signa."</li>";
													}
													$return.="</ul>";
													echo $return;
													echo'
												</td>
											</tr>
											';
										}
										
										echo'
										<tr>
											<th>Catatan</th>
											<td>'.$model->catatan.'</td>
										</tr>
										';
										echo'
										<tr>
											<th>Berkas</th>
											<td><ul>';
											foreach($row->berkasPasiens as $row){
												echo'
												<li>
													<a rel="image_gallery" href="'.Yii::app()->baseUrl.'/../Upload/'.Lib::MRN($row->idRegistrasi->id_pasien).'/'.$row->nama_berkas.'">'.$row->nama_berkas.'</a>
												</li>
												';
											}
											echo'</ul></td>
										</tr>
										';
										echo'
									</table>
								</div>
							</div>
						</li>
						<!-- END timeline item -->
						';
					}
					?>
					<li>
						<i class="fa fa-clock-o"></i>
					</li>
				</ul>
				<?php
				}else{
				?>
				<div class="well">
					Riwayat Medis Pasien Tidak tersedia
				</div>
				<?php
				}
				?>
				
			</div><!-- /.col -->
		</div>
		
		<center>
			<?php
			echo CHtml::button('Tutup Halaman', array(
				'class' => 'btn btn-primary',
				'onclick' => "window.self.close();",
					)
			);
			?>
		</center>
	</div>
</div>
