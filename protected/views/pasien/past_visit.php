<script language="javascript">
function goRegister(id,idReg){
	window.opener.regUrl(id,idReg);
	window.self.close();
}
</script>


<?php
$this->title['title']="Pendaftaran";
$this->title['deskripsi']="Mengelola Pendaftaran";
?>

<div class="box box-primary">
	<div class="box-body">
		<h4 class="box-title">Past Visit</h4>
		<hr>
		<?php 
		// put this somewhere on top
		$pageSize=Yii::app()->user->getState('pageSize',Yii::app()->params['defaultPageSize']); ?>		
		<?php		
		$this->widget('booster.widgets.TbExtendedGridView', array(
			'id'=>'registrasi-grid',
			'type' => 'striped',
			'dataProvider' => $model->searchPembayaran(),
			'summaryText'=>'Menampilkan {start}-{end} dari {count} hasil.',
			'selectableRows' => 2,
			'responsiveTable' => true,
			'enablePagination' => true,
			'pager' => array(
				'htmlOptions'=>array(
					'class'=>'pagination'
				),
				'maxButtonCount' => 5,
				'cssFile' => true,
				'header' => false,
				'firstPageLabel' => '<<',
				'prevPageLabel' => '<',
				'nextPageLabel' => '>',
				'lastPageLabel' => '>>',
			),
			'columns'=>array(
					array(
						"header"=>"Waktu Registrasi",
						"name"=>"waktu_registrasi",
						"value"=>'Lib::dateIndShortMonth($data->waktu_registrasi,false)'
					),
					array(
						"header"=>"No Registrasi",
						"name"=>"no_registrasi",
						"value"=>'$data->no_registrasi'
					),
					array(
						"header"=>"No RM",
						"name"=>"id_pasien",
						"value"=>'Lib::MRN($data->id_pasien)'
					),
					array(
						"header"=>'Nama Pasien',
						"type"=>'Raw',
						"name"=>"nama_lengkap",
						"value"=>function($data){
							$icon='male';
							if($data->idPasien->idPersonal->jenis_kelamin=='Perempuan'){
								$icon='<i class="fa fa-venus" style="color:red;font-weight:bold;"></i>';
							}else{
								$icon='<i class="fa fa-mars" style="color:blue;font-weight:bold;"></i>';
							}
							return $icon.$data->idPasien->idPersonal->nama_lengkap;					
						}
					),
					array(
						"header"=>"Layanan",
						"name"=>"id_departement",
						"value"=>'$data->idDepartemen->nama_departemen'
					),
					array(
						"header"=>"Jaminnan",
						"name"=>"id_penjamin",
						"value"=>function($data){
							if($data->jenis_jaminan=="UMUM"){
								echo "Umum";
							}else{
								foreach($data->jaminanPasiens as $row){
									echo $row->idPenjamin->nama_penjamin.'<br/>';
								}
							}
						}
					),
					array(
						"header"=>"Status Kunjungan",
						"name"=>"status_registrasi",
						"value"=>'$data->status_registrasi'
					),
					array(
						"header"=>"Status Tagihan",
						"name"=>"status_pembayaran",
						"value"=>'$data->status_pembayaran'
					),
					array(
						'header'=>CHtml::dropDownList('pageSize',$pageSize,array(10=>10,20=>20,50=>50,100=>100,200=>200,500=>500,1000=>1000),array(
							'onchange'=>"$.fn.yiiGridView.update('registrasi-grid',{ data:{pageSize: $(this).val() }})",
						)),
						'type'=>"raw",
						'value'=>function($data){
							return '<button onClick="goRegister('.$data->id_pasien.','.$data->id_registrasi.');" class="btn btn-danger">Lihat Kunjungan</button>';
						}
					),
			),
		));
		?>
		<br/>
		<br/>
		<center>
			<?php
			echo CHtml::button('Tutup Halaman', array(
				'class' => 'btn btn-primary',
				'onclick' => "window.self.close();",
					)
			);
			?>
		</center>
	</div>
</div>