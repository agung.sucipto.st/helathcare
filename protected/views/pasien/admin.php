<?php
$this->breadcrumbs=array(
	'Kelola Pasien',
);
$this->title=array(
	'title'=>'Kelola Pasien',
	'deskripsi'=>'Untuk Mengelola Pasien'
);

Yii::app()->clientScript->registerScript('search', "
	$('.search-form form').submit(function(){
		$.fn.yiiGridView.update('pasien-grid', {
			data: $(this).serialize()
		});
		return false;
	});
	$('.import-button').click(function(){
		$('.import-form').toggle();
		$('.search-form').hide();
		return false;
	});
");

?>

<div class="panel panel-default">
	<div class="panel-heading">
		<h3 class="panel-title pull-left">Data</h3>
		<a href="<?php echo Yii::app()->createUrl(Yii::app()->controller->id.'/create');?>" class="btn btn-primary btn-xs pull-right" style="display:<?php echo ((Lib::accessBy('pasien','create')==true)?"block":"none");?>">
			<i class="fa fa-copy"></i>
			<span>Daftar Baru Teridentifikasi</span>
		</a>
		<a href="<?php echo Yii::app()->createUrl(Yii::app()->controller->id.'/createNotIdentify');?>" class="btn btn-primary btn-xs pull-right" style="display:<?php echo ((Lib::accessBy('pasien','createNotIdentify')==true)?"block":"none");?>">
			<i class="fa fa-copy"></i>
			<span>Daftar Baru Tidak Teridentifikasi</span>
		</a>
		
		<a href="#" class="btn btn-primary btn-xs pull-right import-button">
			<i class="fa fa-upload"></i>
			<span>Import</span>
		</a>
		<div style="clear:both"></div>
	</div>
	<div class="panel-body">
		<div class="search-form">
			<?php $this->renderPartial('_search',array(
				'model'=>$model,
			)); ?>
		</div>
		<div class="import-form" style="display:none">
			<div class="xe-widget xe-counter-block" data-count=".num" data-from="0" data-to="99.9" data-suffix="%" data-duration="2">
			<div class="row">
				<div class="col-sm-7">
					<div class="xe-lower">
							
							<?php $form=$this->beginWidget('booster.widgets.TbActiveForm',array(
								'action'=>Yii::app()->createUrl(Yii::app()->controller->id.'/import'),
								'method'=>'POST',
								'htmlOptions'=>array('enctype'=>'multipart/form-data'),
							)); ?>
							<?php echo $form->fileFieldGroup($model, 'import',
									array(
										'wrapperHtmlOptions' => array(
											'class' => 'form-control',
										),
										'widgetOptions'=>array(
											'htmlOptions'=>array(
												'required'=>true,
												'accept'=>".xls"
											),
										),
										'label'=>''
										,
									)
								); ?>								<div class="text-center">
									<div class="col-sm-6">
										
									</div>
									<div class="col-sm-6">
									<button class="btn btn-success btn-icon btn-cancel">
											<i class="fa fa-times"></i>
											<span>Batal</span>
										</button>
										<button class="btn btn-success btn-icon">
											<i class="fa fa-file-o"></i>
											<span>Import</span>
										</button>
									</div>							
								</div>							
							
						<?php $this->endWidget(); ?>					
						</div>				
				</div>
				<div class="col-sm-5">
					<div class="xe-upper">
						<div class="xe-label">
							<!-- Excel Button --> 
							<a href="<?=Yii::app()->theme->baseUrl;?>/assets/template_pasien.xls" class="a-btn-2">
								<span class="a-btn-2-text">Download Template</span> 
							</a>
						</div>
					</div>		
				</div>
			</div>
			</div>
		</div><!-- search-form -->
		<style>
		.pagination li.selected a{
			background:rgb(235, 235, 236);
		}
		</style>
		
		<?php
		if(Yii::app()->user->hasFlash('success')){
			echo'
			<div class="alert alert-success">
				<button type="button" class="close" data-dismiss="alert">&times;</button>
				'.Yii::app()->user->getFlash('success').'
			</div>';
		}
		?>
							
		<div id="example-2_wrapper" class="dataTables_wrapper form-inline dt-bootstrap no-footer">
		
		<?php 
		// put this somewhere on top
		$pageSize=Yii::app()->user->getState('pageSize',Yii::app()->params['defaultPageSize']); ?>		
		<?php		
		$this->widget('booster.widgets.TbExtendedGridView', array(
			'id'=>'pasien-grid',
			'type' => 'striped',
			'dataProvider' => $model->search(),
			//'filter'=>$model,
			'summaryText'=>'Menampilkan {start}-{end} dari {count} hasil.',
			'selectableRows' => 2,
			'responsiveTable' => true,
			'enablePagination' => true,
			'pager' => array(
				'htmlOptions'=>array(
					'class'=>'pagination'
				),
				'maxButtonCount' => 5,
				'cssFile' => true,
				'header' => false,
				'firstPageLabel' => '<<',
				'prevPageLabel' => '<',
				'nextPageLabel' => '>',
				'lastPageLabel' => '>>',
			),
			'columns'=>array(
					array(
						"header"=>$model->getAttributeLabel('id_pasien'),
						"type"=>'raw',
						"name"=>'id_pasien',
						"value"=>function($data){
							if($data->pasiens[0]->id_pasien!=''){
								return Lib::MRN($data->pasiens[0]->id_pasien);
							}else{
								return '<a href="'.Yii::app()->createUrl(Yii::app()->controller->id.'/create',array("idPerson"=>$data->id_personal)).'" class="btn btn-xs btn-danger" style="display:'.((Lib::accessBy('pasien','create')==true)?"block":"none").'><i class="fa fa-plus"> Tambah Data Pasien</a>';
							}
						}
					),
					array(
						"header"=>'NIK/SIM/Passport',
						"name"=>'no_identitas',
						"value"=>function($data){
							return $data->identitasPersonals[0]->no_identitas;
						},
					),
					array(
						"header"=>$model->getAttributeLabel('nama_lengkap'),
						"name"=>'nama_lengkap',
						"type"=>'Raw',
						"value"=>function($data){
							$icon='male';
							if($data->jenis_kelamin=='Perempuan'){
								$icon='<i class="fa fa-venus" style="color:red;font-weight:bold;"></i>';
							}else{
								$icon='<i class="fa fa-mars" style="color:blue;font-weight:bold;"></i>';
							}
							if($data->pasiens[0]->id_pasien!=''){
								if(Lib::accessBy('pasien','view')==true){
									return '<a href="'.Yii::app()->createUrl('pasien/view',array("id"=>$data->pasiens[0]->id_pasien)).'" style="color:black;">'.$icon.$data->nama_lengkap.", ".$data->pasiens[0]->panggilan.'</a>';
								}else{
									return $icon.$data->nama_lengkap.", ".$data->pasiens[0]->panggilan;
								}	
							}else{
								return $icon.$data->nama_lengkap;
							}						
						}
					),
					array(
						"header"=>$model->getAttributeLabel('alamat_domisili'),
						"name"=>'alamat_domisili',
						"value"=>'$data->alamat_domisili'
					),
					array(
						"header"=>$model->getAttributeLabel('tempat_lahir'),
						"name"=>'tempat_lahir',
						"value"=>'$data->tempat_lahir'
					),
					array(
						"header"=>$model->getAttributeLabel('tanggal_lahir'),
						"name"=>'tanggal_lahir',
						"value"=>'Lib::dateInd($data->tanggal_lahir)'
					),
					array(
						"type"=>"raw",
						"header"=>"Hubungan Ke Pasien",
						"value"=>function($data){
							$hubungan=PersonalKeluarga::model()->findAll(array("condition"=>"id_personal_from='$data->id_personal' OR id_personal_to='$data->id_personal'"));
							$res="<ul>";
								foreach($hubungan as $row){
									if($row->id_personal_from==$data->id_personal){
										$res.="<li>".$row->idJenisHubunganToFrom->jenis_hubungan." - ".$row->idPersonalTo->nama_lengkap."</li>";
									}elseif($row->id_personal_to==$data->id_personal){
										$res.="<li>".$row->idJenisHubunganFromTo->jenis_hubungan." - ".$row->idPersonalFrom->nama_lengkap."</li>";
									}
								}
							$res.="</ul>";
							return $res;
						}
					),
					array(
						'class'=>'booster.widgets.TbButtonColumn',
						 'deleteConfirmation'=>'Anda yakin akan menhapus data?',
						'template'=>'{view}{delete}',
						'buttons'=>array
						(
							'view' => array
							(
								'label'=>'View',
								'icon'=>'search',
								'visible'=>'$data->getAllowView()',
								'options'=>array(
									'class'=>'btn btn-default btn-xs',
								),
							),
							'update' => array
							(
								'label'=>'Update',
								'icon'=>'pencil',
								'visible'=>'$data->getAllowUpdate()',
								'options'=>array(
									'class'=>'btn btn-default btn-xs',
								),
							),
							'delete' => array
							(
								'label'=>'Delete',
								'icon'=>'trash',
								'visible'=>'$data->getAllowDelete($data->id_pasien)',
								'options'=>array(
									'class'=>'btn btn-default btn-xs delete',
								),
							)
						),
						'header'=>CHtml::dropDownList('pageSize',$pageSize,array(10=>10,20=>20,50=>50,100=>100,200=>200,500=>500,1000=>1000),array(
							'onchange'=>"$.fn.yiiGridView.update('pasien-grid',{ data:{pageSize: $(this).val() }})",
						)),
					),
			),
		));
		?>
		</div>
	</div>
</div>