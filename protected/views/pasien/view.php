<div class="box box-primary">
	<div class="box-body">
		<div class="row">
			<div class="col-sm-1">
				<?php
				$img='';
				$icon='';
				if($model->idPersonal->jenis_kelamin=='Perempuan'){
					$img=Yii::app()->theme->baseUrl.'/assets/images/female.png';
					$icon='<i class="fa fa-venus" style="color:red;font-weight:bold;"></i>';
				}else{
					$img=Yii::app()->theme->baseUrl.'/assets/images/male.png';
					$icon='<i class="fa fa-mars" style="color:blue;font-weight:bold;"></i>';
				}	
				echo'<img src="'.$img.'" class="img-responsive"/>';
				?>
			</div>
			<div class="col-sm-6">
				<div class="row">
					<div class="col-sm-12" style="font-size:25px;font-weight:bold;">
					<?php
						echo Lib::MRN($model->id_pasien).' - '.$icon.strtoupper($model->idPersonal->nama_lengkap).", ".$model->panggilan;	
					?>
					</div>
				</div>
				<div class="row">
					<div class="col-sm-12">
					<?php
						echo $model->idPersonal->jenis_kelamin.' / '.Lib::dateInd($model->idPersonal->tanggal_lahir,false).", ".Lib::umurDetail($model->idPersonal->tanggal_lahir);	
					?>
					</div>
					<div class="col-sm-12">
						<a href="#" class="btn btn-success btn-xs" onCLick="pastVisit()">Past Visit</a>
						<a href="#" class="btn btn-warning btn-xs" onClick="medicalHistory()">Riwayat Medis Pasien</a>
					</div>
				</div>
				
			</div>
			<div class="col-sm-5">
				
			</div>
		</div>
	</div>
</div>
<div class="row">
	<div class="col-sm-10">
	<div class="box box-solid">
		<div class="box-body">
				<button onclick="history.go(-1)" class="btn btn-primary btn-xs pull-right">
				<i class="fa fa-arrow-left"></i>
					<span>Kembali</span>
				</button>
				<a href="<?php echo Yii::app()->createUrl(Yii::app()->controller->id.'/update/id/'.$_GET['id']);?>" class="btn btn-primary btn-xs pull-right" style="display:<?php echo ((Lib::accessBy('pasien','update')==true)?"block":"none");?>">
					<i class="fa fa-edit"></i>
					<span>Edit Data</span>
				</a>
			<div class="row">
				<div class="col-sm-2">
					<ul class="nav nav-pills nav-stacked">
						<li class="header">Navigasi</li>
						<li class="active"><a href="<?php echo Yii::app()->createUrl('pasien/view',array("id"=>$model->id_pasien));?>"><i class="fa fa-user-circle-o"></i> Data Identitas</a></li>
					</ul>
				</div>
				<div class="col-sm-10">
					<div class="nav-tabs-custom">
						<ul class="nav nav-tabs">
							<li class="active"><a href="#tab_1" data-toggle="tab">Data Identitas Pasien</a></li>
							<li class=""><a href="#tab_2" data-toggle="tab">Data Keluarga</a></li>
							<li class=""><a href="#tab_3" data-toggle="tab">Data Penjamin</a></li>
						</ul>
						<div class="tab-content">
							<div class="tab-pane active" id="tab_1">
								<div class="row">
									<div class="col-sm-6">
										<?php $this->widget('booster.widgets.TbDetailView',array(
											'data'=>$model,
												'attributes'=>array(		
												array(
													"label"=>$model->getAttributeLabel('nama_lengkap'),
													"value"=>strtoupper($model->idPersonal->nama_lengkap).", ".$model->panggilan
												),
												array(
													"label"=>$identitas->idJenisIdentitas->jenis_identitas,
													"value"=>$identitas->no_identitas
												),
												array(
													"label"=>$model->getAttributeLabel('jenis_kelamin'),
													"value"=>$model->idPersonal->jenis_kelamin
												),
												array(
													"label"=>"Tempat/Tgl Lahir",
													"value"=>$model->idPersonal->tempat_lahir.', '.Lib::dateInd($model->idPersonal->tanggal_lahir,false)
												),
												array(
													"label"=>$model->getAttributeLabel('status_perkawinan'),
													"value"=>$model->idPersonal->status_perkawinan
												),
												array(
													"label"=>$model->getAttributeLabel('id_agama'),
													"value"=>$model->idPersonal->idAgama->nama
												),
												array(
													"label"=>$model->getAttributeLabel('id_pendidikan'),
													"value"=>$model->idPersonal->idPendidikan->nama
												),
												array(
													"label"=>$model->getAttributeLabel('id_pekerjaan'),
													"value"=>$model->idPersonal->idPekerjaan->nama
												),
											),
										)); ?>
									</div>
									<div class="col-sm-6">
										<?php $this->widget('booster.widgets.TbDetailView',array(
										'data'=>$model,
										'attributes'=>array(	
											array(
												"label"=>$model->getAttributeLabel('alamat_domisili'),
												"value"=>$model->idPersonal->alamat_domisili
											),
											array(
												"label"=>$model->getAttributeLabel('alamat_sementara'),
												"value"=>$model->idPersonal->alamat_sementara
											),
											array(
												"label"=>$model->getAttributeLabel('id_negara'),
												"value"=>$model->idPersonal->idNegara->nama
											),
											array(
												"label"=>$model->getAttributeLabel('id_provinsi'),
												"value"=>$model->idPersonal->idProvinsi->nama
											),
											array(
												"label"=>$model->getAttributeLabel('id_kabupaten'),
												"value"=>$model->idPersonal->idKabupaten->nama
											),
											array(
												"label"=>$model->getAttributeLabel('id_kecamatan'),
												"value"=>$model->idPersonal->idKecamatan->nama
											),
											array(
												"label"=>$model->getAttributeLabel('kontak1'),
												"value"=>$kontak[0]->kontak
											),
											array(
												"label"=>$model->getAttributeLabel('kontak2'),
												"value"=>$kontak[1]->kontak
											),
											),
										)); ?>
									</div>
								</div>
							</div><!-- /.tab-pane -->
							<div class="tab-pane" id="tab_2">
								<?php $this->widget('booster.widgets.TbDetailView',array(
										'data'=>$model,
											'attributes'=>array(
												array(
														"label"=>$model->getAttributeLabel('nama_keluarga'),
														"value"=>$keluarga->nama_lengkap
												),
												array(
														"label"=>$model->getAttributeLabel('jenis_kelamin_keluarga'),
														"value"=>$keluarga->jenis_kelamin
												),
												array(
														"label"=>$model->getAttributeLabel('alamat_keluarga'),
														"value"=>$keluarga->alamat_domisili
												),
												array(
														"label"=>$model->getAttributeLabel('kontak3'),
														"value"=>$noHpKeluarga->kontak
												),
												array(
														"label"=>$model->getAttributeLabel('hubungan_ke_pasien'),
														"value"=>$relation->idJenisHubunganFromTo->jenis_hubungan
												),
											),
										)); ?>
							</div><!-- /.tab-pane -->
							<div class="tab-pane" id="tab_3">
								<?php $this->widget('booster.widgets.TbDetailView',array(
										'data'=>$model,
											'attributes'=>array(
												array(
														"label"=>$model->getAttributeLabel('id_penjamin'),
														"value"=>$jaminan->idPenjamin->nama_penjamin
												),
												array(
														"label"=>$model->getAttributeLabel('no_kartu'),
														"value"=>$jaminan->no_kartu
												),
												array(
														"label"=>$model->getAttributeLabel('nama_pemegang_kartu'),
														"value"=>$jaminan->nama_pemegang_kartu
												),
												array(
														"label"=>$model->getAttributeLabel('fasilitas_jaminan'),
														"value"=>$jaminan->fasilitas_jaminan
												),
											),
										)); ?>
							</div><!-- /.tab-pane -->
						</div><!-- /.tab-content -->
					</div>
					<div class="row">
					<div class="col-sm-6">
						<div class="box box-primary">
							<div class="box-body">
								<?php $this->widget('booster.widgets.TbDetailView',array(
									'data'=>$model,
										'attributes'=>array(
											array(
												"label"=>"Golongan Darah",
												"value"=>$model->idPersonal->golongan_darah.' '.$model->idPersonal->resus
											),
											array(
												"label"=>"Alergi",
												"value"=>$model->idPersonal->alergi
											),
										),
								)); ?>
							</div>
						</div>
					</div>
					<div class="col-sm-6">
						<div class="box box-primary">
							<div class="box-body">
								<?php $this->widget('booster.widgets.TbDetailView',array(
									'data'=>$model,
										'attributes'=>array(
											array(
												"label"=>$model->getAttributeLabel('user_create'),
												"value"=>$model->userCreate->username.' / '.Lib::dateInd($model->time_create)
											),
											array(
												"label"=>$model->getAttributeLabel('user_update'),
												"value"=>$model->userUpdate->username.' / '.Lib::dateInd($model->time_update)
											),
										),
								)); ?>
							</div>
						</div>
					</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	</div>
	<div class="col-sm-2">
	<?php
	$cekLayanan=Registrasi::model()->findAll(array("condition"=>"id_pasien='$model->id_pasien' and status_registrasi='Aktif'"));
	if(count($cekLayanan)>0){
	?>
		<div class="box box-danger">
			<div class="box-body">
				<ul class="nav nav-pills nav-stacked">
					<li class="header">Layanan Aktif</li>
				<?php
				foreach($cekLayanan as $row){
				?>
					<li><a href="<?php echo Yii::app()->createUrl('registrasi/info',array("id"=>$row->id_pasien,"idReg"=>$row->id_registrasi));?>" style="display:<?php echo ((Lib::accessBy('registrasi','info')==true)?"block":"none");?>"><i class="fa fa-stethoscope"></i> <?=$row->idDepartemen->nama_departemen;?></a></li>
				<?php
				}
				?>
			</ul>
			</div>
		</div>
	<?php
	}
	?>
		<div class="box box-primary">
			<div class="box-body">
				<ul class="nav nav-pills nav-stacked">
					<li class="header">Pilih Layanan</li>
					<?php
					$cekRanap=Registrasi::model()->count(array("condition"=>"id_pasien='$model->id_pasien' and jenis_registrasi='Rawat Inap' and status_registrasi='Aktif'"));
					$cekIgd=Registrasi::model()->count(array("condition"=>"id_pasien='$model->id_pasien' and id_departemen='1' and status_registrasi='Aktif'"));
					if($cekRanap == 0) {
						if($cekIgd==0){
						?>
						<li><a href="<?php echo Yii::app()->createUrl('registrasi/igd',array("id"=>$model->id_pasien));?>" style="display:<?php echo ((Lib::accessBy('registrasi','igd')==true)?"block":"none");?>"><i class="fa fa-stethoscope"></i> Emergency</a></li>
						<?php
						}
						?>
					
						<li><a href="<?php echo Yii::app()->createUrl('registrasi/poliklinik',array("id"=>$model->id_pasien));?>" style="display:<?php echo ((Lib::accessBy('registrasi','poliklinik')==true)?"block":"none");?>"><i class="fa fa-stethoscope"></i> Poliklinik</a></li>
						<li><a href="<?php echo Yii::app()->createUrl('registrasi/rawatInap',array("id"=>$model->id_pasien));?>" style="display:<?php echo ((Lib::accessBy('registrasi','rawatInap')==true)?"block":"none");?>"><i class="fa fa-stethoscope"></i> Rawat Inap</a></li>
					<?php
					}
					?>
				</ul>
			</div>
		</div>
		
		<!--
		<div class="box box-danger">
			<div class="box-body">
				<ul class="nav nav-pills nav-stacked">
					<li class="header">Pilih Layanan Migrasi</li>
					<?php
					$cekIgd=Registrasi::model()->count(array("condition"=>"id_pasien='$model->id_pasien' and id_departemen='1' and status_registrasi='Aktif'"));
					if($cekIgd==0){
					?>
					<li><a href="<?php echo Yii::app()->createUrl('registrasi/igd',array("id"=>$model->id_pasien,"manual"=>true));?>" style="display:<?php echo ((Lib::accessBy('registrasi','igd')==true)?"block":"none");?>"><i class="fa fa-stethoscope"></i> Emergency</a></li>
					<?php
					}
					?>
					
					<li><a href="<?php echo Yii::app()->createUrl('registrasi/poliklinik',array("id"=>$model->id_pasien,"manual"=>true));?>" style="display:<?php echo ((Lib::accessBy('registrasi','poliklinik')==true)?"block":"none");?>"><i class="fa fa-stethoscope"></i> Poliklinik</a></li>
				</ul>
			</div>
		</div>
		-->
	</div>
</div>

<?php
Yii::app()->clientScript->registerScript('validate', '
function pastVisit(){
	var left = (screen.width/2)-(800/2);
	var top = (screen.height/2)-(400/2);
	window.open("'.Yii::app()->createUrl('pasien/pastVisit',array("id"=>$model->id_pasien)).'","","width=800,height=400,top="+top+",left="+left);
}

function medicalHistory(){
	var left = (screen.width/2)-(800/2);
	var top = (screen.height/2)-(400/2);
	window.open("'.Yii::app()->createUrl('pasien/medicalHistory',array("id"=>$model->id_pasien)).'","","width=800,height=400,top="+top+",left="+left);
}


function regUrl(id,idReg){
	window.location.href="'.Yii::app()->createUrl('registrasi/info').'/"+id+"?idReg="+idReg;
}
', CClientScript::POS_END);
?>