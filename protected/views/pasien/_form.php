<link rel="stylesheet" href="<?php echo Yii::app()->theme->baseUrl; ?>/assets/css/bootstrap-select.css">
<script src="<?php echo Yii::app()->theme->baseUrl; ?>/assets/js/bootstrap-select.js"></script>
<style>
	h4{
		font-weight:bold;
	}
	hr{
		border-color:green;
	}
</style>


<?php $form=$this->beginWidget('booster.widgets.TbActiveForm',array(
	'id'=>'pasien-form',
	'enableAjaxValidation'=>true,
)); ?>

<p class="help-block">Isian dengan tanda <span class="required">*</span> wajib diisi.</p>

<?php echo $form->errorSummary($model); ?>
<div class="row">
	<div class="col-sm-6">
		<h4>Data Identitas Pasien</h4>
		<hr>
		<div class="row">
			<div class="col-sm-6">
				<?php echo $form->dropDownListGroup($model,'panggilan', array('widgetOptions'=>array('data'=>array("Tn"=>"Tn.","Ny"=>"Ny.","Nn"=>"Nn.","An"=>"An.","By"=>"By."), 'htmlOptions'=>array('class'=>'input-large','empty'=>'Pilih Title')))); ?>
			</div>
			<div class="col-sm-6">
				<?php echo $form->textFieldGroup($model,'nama_lengkap',array('widgetOptions'=>array('htmlOptions'=>array('class'=>'span5','maxlength'=>60)))); ?>
				<input type="hidden" value="<?=date('Y-m-d H:i:s');?>" name="Pasien[waktu_daftar]"/>
			</div>
		</div>
		
		<div class="row">
			<div class="col-sm-6">
				<?php echo $form->dropDownListGroup($model,'id_jenis_identitas', array('widgetOptions'=>array('data'=>CHtml::listData(JenisIdentitas::model()->findAll(),'id_jenis_identitas','jenis_identitas'), 'htmlOptions'=>array('class'=>'input-large','empty'=>'Pilih Identitas')))); ?>
			</div>
			<div class="col-sm-6">
				<?php echo $form->textFieldGroup($model,'no_identitas',array('widgetOptions'=>array('htmlOptions'=>array('class'=>'span5','maxlength'=>45)))); ?>
			</div>
		</div>
		
		<div class="row">
			<div class="col-sm-6">
				<?php echo $form->textFieldGroup($model,'tempat_lahir',array('widgetOptions'=>array('htmlOptions'=>array('class'=>'span5','maxlength'=>100)))); ?>
			</div>
			<div class="col-sm-6">
				<?php echo $form->datePickerGroup($model,'tanggal_lahir',array('widgetOptions'=>array('options'=>array('format'=>'yyyy-mm-dd','viewFormat'=>'yyyy-mm-dd'),'htmlOptions'=>array('class'=>'span5')),)); ?>
			</div>
		</div>

		<div class="row">
			<div class="col-sm-4">
				<?php echo $form->dropDownListGroup($model,'jenis_kelamin', array('widgetOptions'=>array('data'=>array("Laki-laki"=>"Laki-laki","Perempuan"=>"Perempuan",), 'htmlOptions'=>array('class'=>'input-large','empty'=>'Pilih Jenis Kelamin')))); ?>
			</div>
			<div class="col-sm-4">
				<?php echo $form->dropDownListGroup($model,'status_perkawinan', array('widgetOptions'=>array('data'=>array("Belum Menikah"=>"Belum Menikah","Menikah"=>"Menikah","Janda"=>"Janda","Duda"=>"Duda","Dibawah Umur"=>"Dibawah Umur"), 'htmlOptions'=>array('class'=>'input-large','empty'=>'Pilih Status')))); ?>
			</div>
			<div class="col-sm-4">
				<?php echo $form->dropDownListGroup($model,'id_agama', array('widgetOptions'=>array('data'=>CHtml::listData(Agama::model()->findAll(),'id_agama','nama'), 'htmlOptions'=>array('class'=>'input-large','empty'=>'Pilih Status')))); ?>
			</div>
		</div>
		
		
		<div class="row">
			<div class="col-sm-6">
				<?php echo $form->dropDownListGroup($model,'id_pendidikan', array('widgetOptions'=>array('data'=>CHtml::listData(Pendidikan::model()->findAll(),'id_pendidikan','nama'), 'htmlOptions'=>array('class'=>'input-large','empty'=>'Pilih Pendidikan')))); ?>
			</div>
			<div class="col-sm-6">
				<?php echo $form->dropDownListGroup($model,'id_pekerjaan', array('widgetOptions'=>array('data'=>CHtml::listData(Pekerjaan::model()->findAll(),'id_pekerjaan','nama'), 'htmlOptions'=>array('class'=>'input-large','empty'=>'Pilih Pekerjaan')))); ?>
			</div>
		</div>
		<div class="row">
			<div class="col-sm-6">
				<?php echo $form->textFieldGroup($model,'kontak1',array('widgetOptions'=>array('htmlOptions'=>array('class'=>'span5','maxlength'=>20)))); ?>
			</div>
			<div class="col-sm-6">
				<?php echo $form->textFieldGroup($model,'kontak2',array('widgetOptions'=>array('htmlOptions'=>array('class'=>'span5','maxlength'=>20)))); ?>
			</div>
		</div>
		<h4>Data Wilayah</h4>
		<hr>
		<div class="row">
			<div class="col-sm-6">
				<?php echo $form->textAreaGroup($model,'alamat_domisili', array('widgetOptions'=>array('htmlOptions'=>array('rows'=>2, 'cols'=>50, 'class'=>'span8')))); ?>
			</div>
			<div class="col-sm-6">
				<?php echo $form->textAreaGroup($model,'alamat_sementara', array('widgetOptions'=>array('htmlOptions'=>array('rows'=>2, 'cols'=>50, 'class'=>'span8')))); ?>
			</div>
		</div>
		<div class="row">
			<div class="col-sm-6">
				<?php echo $form->dropDownListGroup($model,'id_negara', array('widgetOptions'=>array('data'=>CHtml::listData(Negara::model()->findAll(),'id_negara','nama'), 'htmlOptions'=>array('class'=>'input-large','empty'=>'Pilih Negara')))); ?>
			</div>
			<div class="col-sm-6">	
				<?php echo $form->dropDownListGroup($model,'id_provinsi', array('widgetOptions'=>array('data'=>CHtml::listData(Provinsi::model()->findAll(),'id_provinsi','nama'), 'htmlOptions'=>array('class'=>'input-large selectpicker show-tick','data-live-search'=>"true",'empty'=>'Pilih Provinsi',)))); ?>
			</div>
		</div>
		
		<div class="row">
			<div class="col-sm-6">
				<?php echo $form->dropDownListGroup($model,'id_kabupaten', array('widgetOptions'=>array('data'=>$model->id_provinsi==''  ? array() : CHtml::listData(Kabupaten::model()->findAll(array("condition"=>"id_provinsi='$model->id_provinsi'")),'id_kabupaten','nama'), 'htmlOptions'=>array('class'=>'input-large selectpicker show-tick','data-live-search'=>"true",'empty'=>'Pilih Kabupaten',)))); ?>
			</div>
			<div class="col-sm-6">
				<?php echo $form->dropDownListGroup($model,'id_kecamatan', array('widgetOptions'=>array('data'=>$model->id_kabupaten==''   ? array() : CHtml::listData(Kecamatan::model()->findAll(array("condition"=>"id_kabupaten='$model->id_kabupaten'")),'id_kecamatan','nama'), 'htmlOptions'=>array('class'=>'input-large selectpicker show-tick','data-live-search'=>"true",'empty'=>'Pilih Kecamatan')))); ?>
			</div>
		</div>
	</div>
	<div class="col-sm-6">
		<?php
		if($model->isNewRecord){
		?>
		<h4>Data Keluarga</h4>
		<hr>
		<div class="row">
			<div class="col-sm-4">
				<?php echo $form->textFieldGroup($model,'nama_keluarga',array('widgetOptions'=>array('htmlOptions'=>array('class'=>'span5','maxlength'=>60)))); ?>
			</div>
			<div class="col-sm-4">
				<?php echo $form->dropDownListGroup($model,'jenis_kelamin_keluarga', array('widgetOptions'=>array('data'=>array("Laki-laki"=>"Laki-laki","Perempuan"=>"Perempuan",), 'htmlOptions'=>array('class'=>'input-large','empty'=>'Pilih Jenis Kelamin')))); ?>
			</div>
			<div class="col-sm-4">
				<?php echo $form->textFieldGroup($model,'kontak3',array('widgetOptions'=>array('htmlOptions'=>array('class'=>'span5','maxlength'=>20)))); ?>
			</div>
		</div>
		<?php echo $form->textAreaGroup($model,'alamat_keluarga', array('widgetOptions'=>array('htmlOptions'=>array('rows'=>2, 'cols'=>50, 'class'=>'span8')))); ?>
			
		<div class="row">
			<div class="col-sm-6">
				<?php echo $form->dropDownListGroup($model,'hubungan_ke_pasien', array('widgetOptions'=>array('data'=> CHtml::listData(JenisHubungan::model()->findAll(),'id_jenis_hubungan','jenis_hubungan'), 'htmlOptions'=>array('class'=>'input-large','empty'=>'Pilih Hubungan')))); ?>
			</div>
			<div class="col-sm-6">
				<?php echo $form->dropDownListGroup($model,'hubungan_pasien', array('widgetOptions'=>array('data'=>CHtml::listData(JenisHubungan::model()->findAll(),'id_jenis_hubungan','jenis_hubungan'), 'htmlOptions'=>array('class'=>'input-large','empty'=>'Pilih Hubungan')))); ?>
			</div>
		</div>
		<?php
		}
		?>
		
		<h4>Data Kesehatan</h4>
		<hr>
		<div class="row">
			<div class="col-sm-6">
				<?php echo $form->dropDownListGroup($model,'golongan_darah', array('widgetOptions'=>array('data'=>array("A"=>"A","AB"=>"AB","B"=>"B","O"=>"0"), 'htmlOptions'=>array('class'=>'input-large','empty'=>'Pilih Golongan Darah')))); ?>
			</div>
			<div class="col-sm-6">
				<?php echo $form->dropDownListGroup($model,'resus', array('widgetOptions'=>array('data'=>array("-"=>"-","+"=>"+"), 'htmlOptions'=>array('class'=>'input-large','empty'=>'Pilih Golongan Darah')))); ?>
			</div>
		</div>
		<?php echo $form->textAreaGroup($model,'alergi', array('widgetOptions'=>array('htmlOptions'=>array('rows'=>1, 'cols'=>50, 'class'=>'span8')))); ?>
		
		<h4>Data Penjamin</h4>
		<hr>
		
		<?php echo $form->dropDownListGroup($model,'id_penjamin', array('widgetOptions'=>array('data'=>CHtml::listData(Penjamin::model()->findAll(),'id_penjamin','nama_penjamin'), 'htmlOptions'=>array('class'=>'input-large','empty'=>'Pasien Umum')))); ?>
		<?php
		if(!$model->isNewRecord){
			if($model->id_penjamin!=""){
				$display='block';
			}else{
				$display='none';
			}
		}else{
			$display='none';
		}
		?>
		<div class="jaminan" style="display:<?php echo $display;?>;">
			<div class="row">
				<div class="col-sm-6">
					<?php echo $form->textFieldGroup($model,'no_kartu',array('widgetOptions'=>array('htmlOptions'=>array('class'=>'span5','maxlength'=>30)))); ?>
				</div>
				<div class="col-sm-6">
					<?php echo $form->textFieldGroup($model,'nama_pemegang_kartu',array('widgetOptions'=>array('htmlOptions'=>array('class'=>'span5','maxlength'=>30)))); ?>
				</div>
			</div>
			<div class="row">
				<div class="col-sm-6">
					<?php echo $form->dropDownListGroup($model,'id_jenis_hubungan', array('widgetOptions'=>array('data'=>CHtml::listData(JenisHubungan::model()->findAll(),'id_jenis_hubungan','jenis_hubungan'), 'htmlOptions'=>array('class'=>'input-large','empty'=>'Pilih Hubungan')))); ?>
				</div>
				<div class="col-sm-6">
					<?php echo $form->dropDownListGroup($model,'fasilitas_jaminan', array('widgetOptions'=>array('data'=>array("Kelas 1"=>"Kelas 1","Kelas 2"=>"Kelas 2","Kelas 3"=>"Kelas 3","VIP"=>"VIP","VVIP"=>"VVIP"), 'htmlOptions'=>array('class'=>'input-large','empty'=>'Pilih Fasilitas')))); ?>
				</div>
			</div>
		</div>

		
	</div>
</div>
<br/>
<br/>
<br/>
<div class="form-actions">
	<?php $this->widget('booster.widgets.TbButton', array(
			'buttonType'=>'submit',
			'context'=>'primary',
			'label'=>$model->isNewRecord ? 'Tambah Data Pasien' : 'Simpan',
		)); 
		echo CHtml::button('Batal', array(
            'class' => 'btn btn-primary',
            'onclick' => "history.go(-1)",
                )
        );
		?>

		</div>
<br/>
<br/>
<br/>
<?php $this->endWidget(); ?>



<?php
Yii::app()->clientScript->registerScript('validatex', '
$("#Pasien_id_provinsi").change(function(){
	var id = $("#Pasien_id_provinsi").val();
	getVal(id,"id_provinsi","#Pasien_id_kabupaten","kabupaten/getKabupaten");
});

$("#Pasien_id_kabupaten").change(function(){
	var id = $("#Pasien_id_kabupaten").val();
	getVal(id,"id_kabupaten","#Pasien_id_kecamatan","kecamatan/getKecamatan");
});

$("#Pasien_id_penjamin").change(function(){
	var id = $("#Pasien_id_penjamin").val();
	if(id == ""){
		$(".jaminan").hide();
	}else{
		$(".jaminan").show();
	}
});

function getVal(value,param,id,url){	
	$.ajax({
		url: "'.Yii::app()->createAbsoluteUrl('/').'/"+url,
		cache: false,
		type: "POST",
		data:"Pasien["+param+"]="+value,
		success: function(msg){
			$(id).html("");
			$(id).html(msg);
			$(id).selectpicker("refresh");
		}
	});
}
', CClientScript::POS_END);
?>
