<div class="box box-primary">
	<div class="box-body">
		<div class="row">
			<div class="col-sm-1">
				<?php
				$img='';
				$icon='';
				if($model->idPersonal->jenis_kelamin=='Perempuan'){
					$img=Yii::app()->theme->baseUrl.'/assets/images/female.png';
					$icon='<i class="fa fa-venus" style="color:red;font-weight:bold;"></i>';
				}else{
					$img=Yii::app()->theme->baseUrl.'/assets/images/male.png';
					$icon='<i class="fa fa-mars" style="color:blue;font-weight:bold;"></i>';
				}	
				echo'<img src="'.$img.'" class="img-responsive"/>';
				?>
			</div>
			<div class="col-sm-6">
				<div class="row">
					<div class="col-sm-12" style="font-size:25px;font-weight:bold;">
					<?php
						echo Lib::MRN($model->id_pasien).' - '.$icon.strtoupper($model->idPersonal->nama_lengkap).", ".$model->panggilan;	
					?>
					</div>
				</div>
				<div class="row">
					<div class="col-sm-12">
					<?php
						echo $model->idPersonal->jenis_kelamin.' / '.Lib::dateInd($model->idPersonal->tanggal_lahir,false).", ".Lib::umurDetail($model->idPersonal->tanggal_lahir);	
					?>
					</div>
					<div class="col-sm-12">
						<a href="" class="btn btn-success btn-xs">Past Visit</a>
						<a href="" class="btn btn-warning btn-xs">Medical History</a>
					</div>
				</div>
				
			</div>
			<div class="col-sm-5">
				
			</div>
		</div>
	</div>
</div>

<div class="box box-solid">
	<div class="box-body">
		<div class="row">
			<div class="col-sm-2">
				<ul class="nav nav-pills nav-stacked">
					<li class="header">Navigasi</li>
					<li class="active"><a href="<?php echo Yii::app()->createUrl('pasien/view',array("id"=>$model->id_pasien));?>"><i class="fa fa-user"></i> Data Identitas</a></li>
				</ul>
			</div>
			<div class="col-sm-10">
				<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>
			</div>
		</div>
	</div>
</div>