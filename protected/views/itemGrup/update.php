<?php
$this->breadcrumbs=array(
	'Kelola Item Grup'=>array('itemGrup/index'),
	'Perbarui Item Grup',
);

$this->title=array(
	'title'=>'Perbarui Item Grup',
	'deskripsi'=>'Untuk Memperbarui Item Gru'
);?>

<div class="panel panel-default">
	<div class="panel-heading">
		<h3 class="panel-title pull-left">Form Perbarui</h3>
		<a href="<?php echo Yii::app()->createUrl(Yii::app()->controller->id);?>" class="btn btn-primary btn-xs pull-right">
			<i class="fa fa-arrow-circle-left"></i>
			<span>Kembali</span>
		</a>
		<div style="clear:both"></div>
	</div>
	<div class="panel-body">		
		<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>	</div>
</div>