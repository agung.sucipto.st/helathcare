<?php $form=$this->beginWidget('booster.widgets.TbActiveForm',array(
	'id'=>'item-grup-form',
	'enableAjaxValidation'=>true,
)); ?>

<p class="help-block">Isian dengan tanda <span class="required">*</span> wajib diisi.</p>

<?php echo $form->errorSummary($model); ?>

	<?php echo $form->dropDownListGroup($model,'id_item_kategori', array('widgetOptions'=>array('data'=>CHtml::listData(ItemKategori::model()->findAll(),'id_item_kategori','nama_item_kategori'), 'htmlOptions'=>array('class'=>'input-large','empty'=>'- Choose -')))); ?>

	<?php echo $form->textFieldGroup($model,'nama_item_grup',array('widgetOptions'=>array('htmlOptions'=>array('class'=>'span5','maxlength'=>100)))); ?>

<div class="form-actions">
	<?php $this->widget('booster.widgets.TbButton', array(
			'buttonType'=>'submit',
			'context'=>'primary',
			'label'=>$model->isNewRecord ? 'Tambah' : 'Simpan',
		)); 
		echo CHtml::button('Batal', array(
            'class' => 'btn btn-primary',
            'onclick' => "history.go(-1)",
                )
        );
		?>

		</div>

<?php $this->endWidget(); ?>
