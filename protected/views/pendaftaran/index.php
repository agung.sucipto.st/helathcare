<?php
$this->title['title']="Pendaftaran";
$this->title['deskripsi']="Mengelola Pendaftaran";
?>

<div class="box box-primary">
	<div class="box-body">
		<h4 class="box-title">Kunjungan Pasien</h4>
		<hr>
		<?php

		Yii::app()->clientScript->registerScript('search', "
		$('.search-form form').submit(function(){
			$.fn.yiiGridView.update('registrasi-grid', {
				data: $(this).serialize()
			});
			return false;
		});

		");
		?>

		<div class="search-form">
			<?php $this->renderPartial('_search',array('model'=>$model)); ?>
		</div>
		
		<?php 
		// put this somewhere on top
		$pageSize=Yii::app()->user->getState('pageSize',Yii::app()->params['defaultPageSize']); ?>		
		<?php		
		$this->widget('booster.widgets.TbExtendedGridView', array(
			'id'=>'registrasi-grid',
			'type' => 'striped',
			'dataProvider' => $model->searchPendaftaran(),
			'summaryText'=>'Menampilkan {start}-{end} dari {count} hasil.',
			'selectableRows' => 2,
			'responsiveTable' => true,
			'enablePagination' => true,
			'pager' => array(
				'htmlOptions'=>array(
					'class'=>'pagination'
				),
				'maxButtonCount' => 5,
				'cssFile' => true,
				'header' => false,
				'firstPageLabel' => '<<',
				'prevPageLabel' => '<',
				'nextPageLabel' => '>',
				'lastPageLabel' => '>>',
			),
			'columns'=>array(
					array(
						"header"=>"Waktu Registrasi",
						"name"=>"waktu_registrasi",
						"value"=>'Lib::dateIndShortMonth($data->waktu_registrasi,false)'
					),
					array(
						"header"=>"No Registrasi",
						"name"=>"no_registrasi",
						"value"=>'$data->no_registrasi'
					),
					array(
						"header"=>"No RM",
						"name"=>"id_pasien",
						"value"=>'Lib::MRN($data->id_pasien)'
					),
					array(
						"header"=>'Nama Pasien',
						"type"=>'Raw',
						"name"=>"nama_lengkap",
						"value"=>function($data){
							$icon='male';
							if($data->idPasien->idPersonal->jenis_kelamin=='Perempuan'){
								$icon='<i class="fa fa-venus" style="color:red;font-weight:bold;"></i>';
							}else{
								$icon='<i class="fa fa-mars" style="color:blue;font-weight:bold;"></i>';
							}
							return $icon.$data->idPasien->idPersonal->nama_lengkap;					
						}
					),
					'jenis_registrasi',
					array(
						"header"=>"Departemen",
						"type"=>"raw",
						"value"=>function($data){
							$text='';
							if($data->jenis_registrasi=='Rawat Inap'){
								$bedLast = BedPasien::model()->find(array("condition"=>"id_registrasi='$data->id_registrasi'","order"=>"id_bed_pasien DESC","limit"=>1));
								if($bedLast){
									$text.=$bedLast->idBed->idRuangRawatan->idKelas->nama_kelas.'<br/>'.$bedLast->idBed->idRuangRawatan->kode_ruangan." ".$bedLast->idBed->idRuangRawatan->nama_ruangan.' / '.$bedLast->idBed->no_bed;
								} else {
									$text.='-';
								}
							} else {
								$text.=$data->idDepartemen->nama_departemen;
							}
							return $text;
						}
					),
					array(
						"header"=>"Jaminnan",
						"name"=>"id_penjamin",
						"value"=>function($data){
							if($data->jenis_jaminan=="UMUM"){
								echo "Umum";
							}else{
								foreach($data->jaminanPasiens as $row){
									echo $row->idPenjamin->nama_penjamin.'<br/>';
								}
							}
						}
					),
					array(
						"header"=>"Status Kunjungan",
						"name"=>"status_registrasi",
						"value"=>'$data->status_registrasi'
					),
					array(
						"header"=>"Status Tagihan",
						"name"=>"status_pembayaran",
						"value"=>'$data->status_pembayaran'
					),
					array(
						'header'=>CHtml::dropDownList('pageSize',$pageSize,array(10=>10,20=>20,50=>50,100=>100,200=>200,500=>500,1000=>1000),array(
							'onchange'=>"$.fn.yiiGridView.update('registrasi-grid',{ data:{pageSize: $(this).val() }})",
						)),
						'class'=>'booster.widgets.TbButtonColumn',
						 'deleteConfirmation'=>'Anda yakin akan menhapus data?',
						'template'=>'{view}',
						'buttons'=>array
						(
							'view' => array
							(
								'label'=>'View',
								'icon'=>'search',
								'visible'=>'$data->status_registrasi!="Batal"',
								'url'=>'Yii::app()->createUrl("registrasi/info",array("id"=>$data->id_pasien,"idReg"=>$data->id_registrasi))',
								'options'=>array(
									'class'=>'btn btn-default btn-xs',
								),
							),
						),
					),
			),
		));
		?>
	</div>
</div>