<?php $form=$this->beginWidget('booster.widgets.TbActiveForm',array(
	'action'=>Yii::app()->createUrl($this->route),
	'method'=>'get',
)); ?>

	<div class="row">
		<div class="col-sm-4">
			<?php echo $form->textFieldGroup($model,'id_pasien',array('widgetOptions'=>array('htmlOptions'=>array('class'=>'span5','maxlength'=>20)))); ?>
		</div>
		<div class="col-sm-4">
			<?php echo $form->textFieldGroup($model,'no_registrasi',array('widgetOptions'=>array('htmlOptions'=>array('class'=>'span5','maxlength'=>20)))); ?>
		</div>
		<div class="col-sm-4">
			<?php echo $form->textFieldGroup($model,'nama_lengkap',array('widgetOptions'=>array('htmlOptions'=>array('class'=>'span5','maxlength'=>20)))); ?>
		</div>
	</div>
		
	<div class="row">
		<div class="col-sm-4">
			<?php echo $form->dropDownListGroup($model,'jenis_registrasi', array('widgetOptions'=>array('data'=>array("IGD"=>"UGD","Rawat Jalan"=>"Rawat Jalan","Rawat Inap"=>"Rawat Inap"), 'htmlOptions'=>array('class'=>'input-large','empty'=>'- Pilih Jenis Registrasi -')))); ?>
		</div>
		<div class="col-sm-4">
			<?php echo $form->datePickerGroup($model,'waktu_registrasi',array('widgetOptions'=>array('options'=>array('format'=>'yyyy-mm-dd','viewFormat'=>'yyyy-mm-dd'),'htmlOptions'=>array('class'=>'span5')))); ?>
		</div>
		<div class="col-sm-4">
			<?php echo $form->dropDownListGroup($model,'status_registrasi', array('widgetOptions'=>array('data'=>array("Aktif"=>"Aktif","Tutup Kunjungan"=>"Tutup Kunjungan","Batal"=>"Batal"), 'htmlOptions'=>array('class'=>'input-large',"empty"=>"Semua Status Kunjungan")))); ?>
		</div>
	</div>
		
	<div class="form-actions">
		<?php $this->widget('booster.widgets.TbButton', array(
			'buttonType' => 'submit',
			'context'=>'primary',
			'label'=>'Cari',
		)); ?>
	</div>

<?php $this->endWidget(); ?>
