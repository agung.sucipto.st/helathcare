<div class="container">
<center>
	<h2>Bukti Retur Supplier</h2>
</center>
<div class="row">
	<div class="col-xs-6">
	<?php $this->widget('booster.widgets.TbDetailView',array(
	'data'=>$model,
	'attributes'=>array(
		'item_transaction_code',
		'item_transaction_time',
		array(
			"label"=>"Penerimaan",
			"value"=>$model->linkTransaction->item_transaction_code
		),
		array(
			"label"=>"Supplier",
			"value"=>$model->supplier->supplier_name
		)
	),
	)); ?>
	</div>
	<div class="col-xs-6">
	<?php $this->widget('booster.widgets.TbDetailView',array(
	'data'=>$model,
	'attributes'=>array(
		array(
			"label"=>"Origin",
			"value"=>$model->warehouseOrigin->warehouse_name
		),
		'item_transaction_note',
		array(
			"label"=>"User",
			"value"=>$model->userCreate->username
		)
	),
	)); ?>
	</div>
</div>
<hr>
<table class="table table-bordered">
	<tr>
		<th class="text-center">Kode Item</th>
		<th class="text-center">Nama Item</th>
		<th class="text-center">Faktur</th>
		<th class="text-center">Jumlah Retur</th>
		<th class="text-center">Satuan</th>
		<th class="text-center">@Harga</th>
		<th class="text-center">Sub Total</th>
	</tr>
	<tbody id="data">
		<?php
		$list=ItemTransactionList::model()->findAll(array("condition"=>"item_transaction_id='$model->item_transaction_id'"));
		$subtotal=0;
		foreach($list as $row){
			$subtotal+=$row->transaction_amount*$row->transaction_price_per_unit;
			echo'
			<tr>
				<td>'.$row->item->code.'</td>
				<td>'.$row->item->item_name.'</td>
				<td>'.$row->listId->itemTransaction->ref_number.'</td>
				<td>'.$row->big_amount.'</td>
				<td>'.$row->itemBigUnit->unit->unit_alias.' ('.(($row->itemBigUnit->parent_item_unit_id!='')?$row->itemBigUnit->unit_amount.' '.$row->itemBigUnit->parentItemUnit->unit->unit_alias:$row->itemBigUnit->unit_amount.' '.$row->itemBigUnit->unit->unit_alias).')</td>
				<td class="text-right">'.number_format($row->transaction_price_per_unit,0).'</td>
				<td class="text-right">'.number_format(($row->transaction_amount*$row->transaction_price_per_unit),0).'</td>
			</tr>';
		}
		?>
		<tr>
			<th colspan="6" class="text-right">Total</th>
			<th class="text-right"><?=number_format($subtotal,0);?></th>
		</tr>
	</tbody>
</table>
<br/>
<div class="row">
	<div class="col-xs-4 text-center">
	</div>
	<div class="col-xs-4"></div>
	<div class="col-xs-4 text-center">
	Petugas
	<br/>
	<br/>
	<br/>
	<br/>
	<br/>
	(<?=$model->userCreate->username;?>)</div>
</div>
</div>
<?php
Yii::app()->clientScript->registerScript('validatex', '
window.print();
', CClientScript::POS_END);
?>