<?php $form=$this->beginWidget('booster.widgets.TbActiveForm',array(
	'id'=>'item-transaction-form',
	'enableAjaxValidation'=>true,
	'type'=>"vertical",
)); 
?>

<p class="help-block">Isian dengan tanda <span class="required">*</span> wajib diisi.</p>
<?php echo $form->errorSummary($model); ?>
<div class="row">
	<div class="col-sm-3">
		<?php echo $form->dropDownListGroup($model,'supplier_id', array('widgetOptions'=>array('data'=>CHtml::listData(Supplier::model()->findAll(array("condition"=>"supplier_status='1'")),'supplier_id','supplier_name'), 'htmlOptions'=>array('class'=>'input-large','empty'=>'- Choose -')))); ?>
	</div>
	<div class="col-sm-3">
		<?php echo $form->dateTimePickerGroup($model,'item_transaction_time',array('widgetOptions'=>array('options'=>array('format'=>'yyyy-mm-dd hh:ii','viewFormat'=>'yyyy-mm-dd hh:ii')),'prepend'=>'<i class="glyphicon glyphicon-calendar"></i>')) ;?>
	</div>

	<div class="col-sm-3">
		<?php echo $form->dropDownListGroup($model,'warehouse_origin_id', array('widgetOptions'=>array('data'=>CHtml::listData(Warehouse::model()->findAll(array("condition"=>"	is_allow_receiveing='1'")),'warehouse_id','warehouse_name'), 'htmlOptions'=>array('class'=>'input-large','empty'=>'- Choose -')))); ?>
	</div>
	<div class="col-sm-3">
		<?php echo $form->textAreaGroup($model,'item_transaction_note', array('widgetOptions'=>array('htmlOptions'=>array('rows'=>1, 'cols'=>50, 'class'=>'span8')))); ?>
	</div>
</div>

<hr>
<table class="table">
	<tr>
		<th>Kode Item</th>
		<th>Nama Item</th>
		<th>Stok Asal</th>
		<th>Jumlah Terima</th>
		<th>Jumlah Sudah Retur</th>
		<th width="200px">Jumlah Retur</th>
		<th></th>
	</tr>
	<tbody id="data">
		<?php
			
			foreach($info->itemTransactionLists as $row){
				$stock=Item::getStock($row->item_id,$model->warehouse_origin_id);
				$unit=Item::getParentUnit($row->item_id);
				echo'
					<tr id="row'.$row->item_id.''.$row->item_big_unit_id.'">
						<td>'.$row->item->code.'</td>
						<td>'.$row->item->item_name.'</td>
						<td>'.$stock.' '.$unit->unit->unit_alias.'</td>
						<td>'.$row->big_amount.' '.$row->itemBigUnit->unit->unit_alias.' / '.($row->big_amount*$row->smal_amount).' '.(($row->itemBigUnit->parent_item_unit_id!='')?$row->itemBigUnit->parentItemUnit->unit->unit_alias:$row->itemBigUnit->unit->unit_alias).'</td>
						<td>'.$row->retur_amount.' '.$unit->unit->unit_alias.'</td>
						<td class="text-right">
						';
						if($stock>$row->transaction_amount AND $row->retur_amount<$row->transaction_amount){
						echo'
						<input type="hidden" name="item[]" value="'.$row->item_id.'|'.$unit->item_unit_id.'|'.$row->item_transaction_list_id.'|'.$row->item_big_unit_id.'"/>
						<div class="input-group"><input class="span5 form-control" placeholder="Retur" name="qty'.$row->item_id.''.$row->item_big_unit_id.'" type="number" min="1" max="'.($row->transaction_amount-$row->retur_amount).'" required onchange="cek(this,this.value,'.$row->transaction_amount.')"><span class="input-group-addon">'.$unit->unit->unit_alias.'</span></div>
						';
						}else{
							echo'Tidak Dapat Di Retur !';
						}
						
						echo'
						</td>
						<td>
						<span class="btn btn-danger" onclick="deleteRow('.$row->item_id.''.$row->item_big_unit_id.')"><i class="fa fa-times"></i></span>
						</td>
					</tr>';
			}
		?>
	</tbody>
</table>
<hr>

<div class="form-actions">
	<?php $this->widget('booster.widgets.TbButton', array(
			'buttonType'=>'submit',
			'context'=>'primary',
			'label'=>$model->isNewRecord ? 'Tambah Retur' : 'Simpan',
			'htmlOptions'=>array(
				//"disabled"=>true,
				"id"=>"submit",
				"onClick"=>"return confirm('Apakah Retur Supplier Sudah Benar?');"
			),
		)); 
		echo CHtml::button('Batal', array(
            'class' => 'btn btn-primary',
            'onclick' => "history.go(-1)",
                )
        );
		?>

		</div>

<?php $this->endWidget(); ?>

<?php
Yii::app()->clientScript->registerScript('addResep', '
function deleteRow(id){
	$("#row"+id).remove();
}

function cek(obj,val,max){
	if(val>max || val == 0){
		alert("Jumlah Retur Tidak Boleh Melebihi Penerimaan !");
		obj.value="";
	}
}
', CClientScript::POS_END);
?>
