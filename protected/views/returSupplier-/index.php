<?php
$this->breadcrumbs=array(
	'Kelola Retur Supplier',
);
$this->title=array(
	'title'=>'Kelola Retur Supplier',
	'deskripsi'=>'Untuk Mengelola Retur Supplier'
);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	$('.import-form').hide();
	return false;
});
$('.btn-cancel').click(function(){
	$('.import-form').toggle();
	return false;
});
$('.import-button').click(function(){
	$('.import-form').toggle();
	$('.search-form').hide();
	return false;
});
$('.search-form form').submit(function(){
	$.fn.yiiGridView.update('item-transaction-grid', {
		data: $(this).serialize()
	});
	return false;
});

");
?>

<div class="panel panel-default">
	<div class="panel-heading">
		<h3 class="panel-title pull-left">Data</h3>
		<?php
		if(Lib::accessBy("returSupplier","create")==true){
		?>
		<a href="<?php echo Yii::app()->createUrl(Yii::app()->controller->id.'/list');?>" class="btn btn-primary btn-xs pull-right">
			<i class="fa fa-copy"></i>
			<span>Tambah Data</span>
		</a>
		<?php
		}
		?>
		<a href="#" class="btn btn-primary btn-xs pull-right search-button">
			<i class="fa fa-search"></i>
			<span>Search</span>
		</a>
		<div style="clear:both"></div>
	</div>
	<div class="panel-body">
		<div class="search-form">
			<?php $this->renderPartial('_search',array(
			'model'=>$model,
		)); ?>
		</div><!-- search-form -->
		
		<style>
		.pagination li.selected a{
			background:rgb(235, 235, 236);
		}
		</style>
		
		
		<?php
		if(Yii::app()->user->hasFlash('success')){
			echo'
			<div class="alert alert-success">
				<button type="button" class="close" data-dismiss="alert">&times;</button>
				'.Yii::app()->user->getFlash('success').'
			</div>';
		}
		?>
							
		<div id="example-2_wrapper" class="dataTables_wrapper form-inline dt-bootstrap no-footer">
		
		<?php 
		// put this somewhere on top
		$pageSize=Yii::app()->user->getState('pageSize',Yii::app()->params['defaultPageSize']); ?>		
		<?php		
		$this->widget('booster.widgets.TbExtendedGridView', array(
			'id'=>'item-transaction-grid',
			'type' => 'striped',
			'dataProvider' => $model->search(),
			//'filter'=>$model,
			'summaryText'=>'Menampilkan {start}-{end} dari {count} hasil.',
			'selectableRows' => 2,
			'responsiveTable' => true,
			'enablePagination' => true,
			'pager' => array(
				'htmlOptions'=>array(
					'class'=>'pagination'
				),
				'maxButtonCount' => 5,
				'cssFile' => true,
				'header' => false,
				'firstPageLabel' => '<<',
				'prevPageLabel' => '<',
				'nextPageLabel' => '>',
				'lastPageLabel' => '>>',
			),
			'columns'=>array(
					array(
						"header"=>$model->getAttributeLabel('item_transaction_time'),
						"value"=>'Lib::dateIndShortMonth($data->item_transaction_time)',
						"name"=>'item_transaction_time',
					),
					array(
						"header"=>$model->getAttributeLabel('warehouse_origin_id'),
						"value"=>'$data->warehouseOrigin->warehouse_name',
						"name"=>'warehouse_origin_id',
					),
					array(
						"header"=>"Supplier",
						"value"=>'$data->supplier->supplier_name',
						"name"=>'supplier_id',
					),
					array(
						"header"=>$model->getAttributeLabel('item_transaction_code'),
						"value"=>function($data){
							if(Lib::accessBy("returSupplier","print")==true){
								return '<a href="#" onclick="cetak('.$data->item_transaction_id.');">'.$data->item_transaction_code.'</button>';
							}else{
								return $data->item_transaction_code;
							}
						},
						"name"=>'item_transaction_code',
						"type"=>'raw',
					),
					array(
						"header"=>$model->getAttributeLabel('link_transaction'),
						"value"=>function($data){
							if(Lib::accessBy("recieving","print")==true){
								return '<a href="#" onclick="cetakTerima('.$data->linkTransaction->item_transaction_id.');">'.$data->linkTransaction->item_transaction_code.'</button>';
							}else{
								return $data->linkTransaction->item_transaction_code;
							}
						},
						"name"=>'link_transaction',
						"type"=>"raw",
					),
					array(
						"header"=>"Item",
						"type"=>"raw",
						"value"=>function($data){
							$res='<ul>';
							$list=ItemTransactionList::model()->findAll(array("condition"=>"item_transaction_id='$data->item_transaction_id'"));
							foreach($list as $row){
								$res.='<li>'.$row->item->item_name.' '.$row->big_amount.' '.$row->itemBigUnit->unit->unit_alias.' ('.(($row->itemBigUnit->parent_item_unit_id!='')?$row->itemBigUnit->unit_amount.' '.$row->itemBigUnit->parentItemUnit->unit->unit_alias:$row->itemBigUnit->unit_amount.' '.$row->itemBigUnit->unit->unit_alias).')</li>';
							}
							$res.='</ul>';
							return $res;
						}
					),
					'transaction_total',
					array(
						"header"=>"User Retur",
						"type"=>"raw",
						"value"=>'$data->userCreate->username."<br/>".Lib::dateIndShortMonth($data->item_transaction_time,false)',
						"name"=>'supplier_id',
					),
					array(
						'class'=>'booster.widgets.TbButtonColumn',
						 'deleteConfirmation'=>'Anda yakin akan menhapus data?',
						'template'=>'{view}{delete}',
						'buttons'=>array
						(
							'view' => array
							(
								'label'=>'View',
								'icon'=>'search',
								'options'=>array(
									'class'=>'btn btn-default btn-xs',
								),
							),
							'delete' => array
							(
								'label'=>'Delete',
								'icon'=>'trash',
								'visible'=>'(Lib::accessBy("returSupplier","delete") AND $data->checkTransaction($data->item_transaction_id))',
								'options'=>array(
									'class'=>'btn btn-default btn-xs delete',
								),
							)
						),
						'header'=>CHtml::dropDownList('pageSize',$pageSize,array(10=>10,20=>20,50=>50,100=>100,200=>200,500=>500,1000=>1000),array(
							'onchange'=>"$.fn.yiiGridView.update('item-transaction-grid',{ data:{pageSize: $(this).val() }})",
						)),
					),
			),
		));
		?>
		</div>
	</div>
</div>

<?php
Yii::app()->clientScript->registerScript('validatex', '
function cetak(id){
	var left = (screen.width/2)-(900/2);
	var top = (screen.height/2)-(500/2);
	window.open("'.Yii::app()->createUrl('returSupplier/print').'/id/"+id,"","width=900,height=500,top="+top+",left="+left);
}

function cetakTerima(id){
	var left = (screen.width/2)-(900/2);
	var top = (screen.height/2)-(500/2);
	window.open("'.Yii::app()->createUrl('recieving/print').'/id/"+id,"","width=900,height=500,top="+top+",left="+left);
}
', CClientScript::POS_END);
?>