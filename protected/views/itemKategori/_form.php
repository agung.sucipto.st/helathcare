<?php $form=$this->beginWidget('booster.widgets.TbActiveForm',array(
	'id'=>'item-kategori-form',
	'enableAjaxValidation'=>true,
)); ?>

<p class="help-block">Isian dengan tanda <span class="required">*</span> wajib diisi.</p>

<?php echo $form->errorSummary($model); ?>

	<?php echo $form->textFieldGroup($model,'nama_item_kategori',array('widgetOptions'=>array('htmlOptions'=>array('class'=>'span5','maxlength'=>100)))); ?>

	<?php echo $form->dropDownListGroup($model,'jenis', array('widgetOptions'=>array('data'=>array("Logistik"=>"Logistik","Farmasi"=>"Farmasi",), 'htmlOptions'=>array('class'=>'input-large')))); ?>

	<?php echo $form->dropDownListGroup($model,'asset', array('widgetOptions'=>array('data'=>array("Ya"=>"Ya","Tidak"=>"Tidak",), 'htmlOptions'=>array('class'=>'input-large')))); ?>

	<?php echo $form->textFieldGroup($model,'id_coa_persediaan',array('widgetOptions'=>array('htmlOptions'=>array('class'=>'span5')))); ?>

	<?php echo $form->textFieldGroup($model,'id_coa_pendapatan_rajal',array('widgetOptions'=>array('htmlOptions'=>array('class'=>'span5')))); ?>

	<?php echo $form->textFieldGroup($model,'id_coa_pendapatan_ranap',array('widgetOptions'=>array('htmlOptions'=>array('class'=>'span5')))); ?>

	<?php echo $form->textFieldGroup($model,'id_coa_biaya_rajal',array('widgetOptions'=>array('htmlOptions'=>array('class'=>'span5')))); ?>

	<?php echo $form->textFieldGroup($model,'id_coa_biaya_ranap',array('widgetOptions'=>array('htmlOptions'=>array('class'=>'span5')))); ?>

	<?php echo $form->textFieldGroup($model,'id_coa_penyesuaian',array('widgetOptions'=>array('htmlOptions'=>array('class'=>'span5')))); ?>

	<?php echo $form->textFieldGroup($model,'id_coa_biaya_operasional',array('widgetOptions'=>array('htmlOptions'=>array('class'=>'span5')))); ?>

<div class="form-actions">
	<?php $this->widget('booster.widgets.TbButton', array(
			'buttonType'=>'submit',
			'context'=>'primary',
			'label'=>$model->isNewRecord ? 'Tambah' : 'Simpan',
		)); 
		echo CHtml::button('Batal', array(
            'class' => 'btn btn-primary',
            'onclick' => "history.go(-1)",
                )
        );
		?>

		</div>

<?php $this->endWidget(); ?>
