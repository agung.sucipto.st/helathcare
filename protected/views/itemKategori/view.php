<?php
$this->breadcrumbs=array(
	'Kelola Item Kategori'=>array('itemKategori/index'),
	'Detail Item Kategori',
);

$this->title=array(
	'title'=>'Detail Item Kategori',
	'deskripsi'=>'Untuk Melihat Detail Item Kategori'
);
?>

<div class="panel panel-default">
	<div class="panel-heading">
		<h3 class="panel-title pull-left">Detail</h3>
		<a href="<?php echo Yii::app()->createUrl(Yii::app()->controller->id);?>" class="btn btn-primary btn-xs pull-right" style="display:<?php echo $visibleEdit;?>">
			<i class="fa fa-arrow-circle-left"></i>
			<span>Kembali</span>
		</a>
		<a href="<?php echo Yii::app()->createUrl(Yii::app()->controller->id.'/create');?>" class="btn btn-primary btn-xs pull-right" style="display:<?php echo ((Lib::accessBy('ItemKategori','create')==true)?"block":"");?>">
			<i class="fa fa-copy"></i>
			<span>Tambah Data</span>
		</a>
		<a href="<?php echo Yii::app()->createUrl(Yii::app()->controller->id.'/update/id/'.$_GET['id']);?>" class="btn btn-primary btn-xs pull-right" style="display:<?php echo ((Lib::accessBy('ItemKategori','update')==true)?"block":"");?>">
			<i class="fa fa-edit"></i>
			<span>Edit Data</span>
		</a>
		<div style="clear:both"></div>
	</div>
	<div class="panel-body">		
		<div id="example-2_wrapper" class="dataTables_wrapper form-inline dt-bootstrap no-footer">
			<?php $this->widget('booster.widgets.TbDetailView',array(
			'data'=>$model,
			'attributes'=>array(
							'id_item_kategori',
				'nama_item_kategori',
				'jenis',
				'asset',
				'id_coa_persediaan',
				'id_coa_pendapatan_rajal',
				'id_coa_pendapatan_ranap',
				'id_coa_biaya_rajal',
				'id_coa_biaya_ranap',
				'id_coa_penyesuaian',
				'id_coa_biaya_operasional',
			),
			)); ?>
		</div>
	</div>
</div>
