<div class="container">
<center>
	<h2>Bukti Terima Transfer</h2>
</center>
<div class="row">
			<div class="col-sm-6">
			<?php $this->widget('booster.widgets.TbDetailView',array(
			'data'=>$model,
			'attributes'=>array(
				'no_transaksi',
				'waktu_transaksi',
				array(
					"label"=>"Gudang Asal",
					"value"=>$model->gudangAsal->nama_gudang
				),
			),
			)); ?>
			</div>
			<div class="col-sm-6">
			<?php $this->widget('booster.widgets.TbDetailView',array(
			'data'=>$model,
			'attributes'=>array(
				
				array(
					"label"=>"Gudang Tujuan",
					"value"=>$model->gudangTujuan->nama_gudang
				),
				'catatan_transaksi',
				array(
					"label"=>"User",
					"value"=>$model->userCreate->username
				)
			),
			)); ?>
			</div>
		</div>
		<hr>
		<table class="table table-bordered">
			<tr>
				<th class="text-center">Kode Item</th>
				<th class="text-center">Nama Item</th>
				<th class="text-center">Jumlah Transfer</th>
				<th class="text-center">Satuan</th>
			</tr>
			<tbody id="data">
				<?php
				$list=DaftarItemTransaksi::model()->findAll(array("condition"=>"id_item_transaksi='$model->id_item_transaksi'"));
				$subtotal=0;
				foreach($list as $row){
					$subtotal+=$row->jumlah_satuan_besar*$row->harga_transaksi;
					echo'
					<tr>
						<td>'.$row->idItem->kode_item.'</td>
						<td>'.$row->idItem->nama_item.'</td>
						<td>'.$row->jumlah_satuan_besar.'</td>
						<td>'.$row->idItemSatuanBesar->idSatuan->alias_satuan.' ('.(($row->idItemSatuanBesar->parent_id_item_satuan!='')?$row->idItemSatuanBesar->nilai_satuan_konversi.' '.$row->idItemSatuanBesar->parentIdItemSatuan->idSatuan->alias_satuan:$row->idItemSatuanBesar->nilai_satuan_konversi.' '.$row->idItemSatuanBesar->idSatuan->alias_satuan).')</td>
					</tr>';
				}
				?>
			</tbody>
		</table>
<br/>
<div class="row">
	<div class="col-xs-4 text-center">
	Pengirim
	<br/>
	<br/>
	<br/>
	<br/>
	<br/>
	(<?=$model->userCreate->username;?>)
	</div>
	<div class="col-xs-4"></div>
	<div class="col-xs-4 text-center">
	Penerima
	<br/>
	<br/>
	<br/>
	<br/>
	<br/>
	(______________)</div>
</div>
</div>
<?php
Yii::app()->clientScript->registerScript('validatex', '
window.print();
', CClientScript::POS_END);
?>