<link rel="stylesheet" href="<?php echo Yii::app()->theme->baseUrl; ?>/assets/css/bootstrap-select.css">
<script src="<?php echo Yii::app()->theme->baseUrl; ?>/assets/js/bootstrap-select.js"></script>

<?php $form=$this->beginWidget('booster.widgets.TbActiveForm',array(
	'id'=>'item-transaction-form',
	'enableAjaxValidation'=>true,
	'type'=>"horizontal",
)); 
?>

<p class="help-block">Isian dengan tanda <span class="required">*</span> wajib diisi.</p>
<?php echo $form->errorSummary($model); ?>
<div class="row">
	<div class="col-sm-6">
		<?php echo $form->dateTimePickerGroup($model,'waktu_transaksi',array('widgetOptions'=>array('options'=>array('format'=>'yyyy-mm-dd hh:ii','viewFormat'=>'yyyy-mm-dd hh:ii')),'prepend'=>'<i class="glyphicon glyphicon-calendar"></i>')) ;?>
	</div>
	<div class="col-sm-6">
		<?php echo $form->dropDownListGroup($model,'gudang_asal', array('widgetOptions'=>array('data'=>CHtml::listData(Gudang::model()->findAll(),'id_gudang','nama_gudang'), 'htmlOptions'=>array('class'=>'input-large','empty'=>'- Choose -')))); ?>
	</div>
</div>

<div class="row">
	
	<div class="col-sm-6">
		<?php echo $form->dropDownListGroup($model,'gudang_tujuan', array('widgetOptions'=>array('data'=>CHtml::listData(Gudang::model()->findAll(),'id_gudang','nama_gudang'), 'htmlOptions'=>array('class'=>'input-large','empty'=>'- Choose -')))); ?>
	</div>
	<div class="col-sm-6">
		<?php echo $form->textAreaGroup($model,'catatan_transaksi', array('widgetOptions'=>array('htmlOptions'=>array('rows'=>1, 'cols'=>50, 'class'=>'span8')))); ?>
	</div>
</div>
<div class="row">
	<div class="col-sm-6">
		<label>Cari Nama Item / Kode Item</label>
		<?php
		$this->widget('zii.widgets.jui.CJuiAutoComplete',array(
			'name'=>'resep',
			'options'=>array(
				'minLength'=>3,
				'showAnim'=>'fold',
				'select'=>"js:function(event, data) {
					var item=data.item.label;
					addRow(item);
				}"
			),
			'source'=>$this->createUrl("item/getItem"),
			'htmlOptions'=>array(
				'class'=>"form-control",
				"id"=>"searchItem"
			),
		));
		?>
	</div>
</div>

<hr>
<table class="table table-striped">
	<tr>
		<th>Kode Item</th>
		<th>Nama Item</th>
		<th>Stok Gudang Asal</th>
		<th width="100px">Jumlah Transfer</th>
		<th>Satuan</th>
		<th>Sub Total (Rp)</th>
		<th></th>
	</tr>
	<tbody id="data">
		
	</tbody>
</table>
<hr>


<div class="form-actions">
	<?php $this->widget('booster.widgets.TbButton', array(
			'buttonType'=>'submit',
			'context'=>'primary',
			'label'=>$model->isNewRecord ? 'Transfer Benar' : 'Simpan',
			'htmlOptions'=>array(
				//"disabled"=>true,
				"id"=>"submit",
				"onClick"=>"return confirm('Apakah Data Transfer Barang Sudah Benar?');"
			),
		)); 
		echo CHtml::button('Batal', array(
            'class' => 'btn btn-primary',
            'onclick' => "history.go(-1)",
                )
        );
		?>

		</div>

<?php $this->endWidget(); ?>

<?php
Yii::app()->clientScript->registerScript('addResep', '

var rowItem=[];
var Item=[];
function deleteRow(id){
	$("#row"+id).remove();
	rowItem.splice(id, 1);
	Item.splice(id, 1);
}

function addRow(item){
	var gudang = $("#ItemTransaksi_gudang_asal").val();
	if(gudang != ""){
	$.ajax({
		url: "'.Yii::app()->createAbsoluteUrl('item/getItem').'",
		cache: false,
		type: "POST",
		data:"item="+item+"&gudang="+gudang,
		success: function(msg){
			var data=$.parseJSON(msg);
			if(rowItem[data.id_item] == undefined){
				console.log(data);
				rowItem[data.id_item]=data.nama_item;
				Item.push(data.id_item);
				$("#searchItem").val("");
				var inputItem="<input type=\"hidden\" name=\"item[]\" value=\""+data.id_item+"|"+data.id_item_satuan+"|"+data.parent+"|"+data.nilaiKeSatuanKecil+"\"/>"+data.nama_item;
				var stokItem=data.stok;
				
				var qty="<input type=\"number\" name=\"qty"+data.id_item+"\" id=\"qty"+data.id_item+"\" value=\"\" class=\"form-control\" required=\"required\" min=\"1\" max=\""+stokItem+"\" />";
				var satuan=data.satuan;
				
				var remove="<span class=\"btn btn-danger\" onclick=\"deleteRow("+data.id_item+")\"><i class=\"fa fa-times\"></i></span>";
				$("#data").append("<tr id=\"row"+data.id_item+"\"><td>"+data.kode_item+"</td><td>"+inputItem+"</td><td>"+stokItem+"</td><td>"+qty+"</td><td>"+satuan+"</td><td>"+remove+"</td></tr>");
				
			}else{
				$("#searchItem").val("");
			}
		}
	});
	}else{
		$("#searchItem").val("");
		alert("Silahkan Pilih Gudang Asal Barang !");
	}
}

$("#ItemTransaction_warehouse_destination_id").change(function(){
	jQuery.each(Item,function(){
		var id = parseInt(this);
		deleteRow(id);
	});
});

function openSupplier(){
	window.open("'.Yii::app()->createUrl(Yii::app()->controller->module->id.'/supplier/addDirect').'","","width=800,height=600");
}

function reloadData(id){
	getVal(id,"id_supplier","#ItemTransaksi_id_supplier","supplier/getData");
}

function getVal(value,param,id,url){	
	$.ajax({
		url: "'.Yii::app()->createAbsoluteUrl(Yii::app()->controller->module->id).'/"+url,
		cache: false,
		type: "POST",
		data:"ItemTransaksi["+param+"]="+value,
		success: function(msg){
			$(id).html("");
			$(id).html(msg);
			$(id).selectpicker("refresh");
		}
	});
}
', CClientScript::POS_END);
?>
