<?php
$this->breadcrumbs=array(
	'Kelola Terima Transfer'=>array('recieving/index'),
	'Tambah Terima Transfer',
);
$this->title=array(
	'title'=>'Tambah Terima Transfer',
	'deskripsi'=>'Untuk Menambah Terima Transfer'
);


Yii::app()->clientScript->registerScript('search', "
$('.search-form form').submit(function(){
	$.fn.yiiGridView.update('item-transaction-grid', {
		data: $(this).serialize()
	});
	return false;
});

");
?>

<div class="panel panel-default">
	<div class="panel-heading">
		<h3 class="panel-title pull-left">Form Tambah Transfer</h3>
		<a href="<?php echo Yii::app()->createUrl(Yii::app()->controller->id);?>" class="btn btn-primary btn-xs pull-right">
			<i class="fa fa-arrow-circle-left"></i>
			<span>Kembali</span>
		</a>
		<div style="clear:both"></div>
	</div>
	<div class="panel-body">		
		<?php echo $this->renderPartial('_search_transfer', array('data'=>$data)); ?>	
		<?php echo $this->renderPartial('transfer_data', array('data'=>$data)); ?>	
	</div>
</div>