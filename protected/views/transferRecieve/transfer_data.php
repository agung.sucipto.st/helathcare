<div id="example-2_wrapper" class="dataTables_wrapper form-inline dt-bootstrap no-footer">

<?php 
// put this somewhere on top
$pageSize=Yii::app()->user->getState('pageSize',Yii::app()->params['defaultPageSize']); ?>		
<?php		
$this->widget('booster.widgets.TbExtendedGridView', array(
	'id'=>'item-transaction-grid',
	'type' => 'striped',
	'dataProvider' => $data->search(),
	'summaryText'=>'Menampilkan {start}-{end} dari {count} hasil.',
	'responsiveTable' => true,
	'enablePagination' => true,
	'pager' => array(
		'htmlOptions'=>array(
			'class'=>'pagination'
		),
		'maxButtonCount' => 5,
		'cssFile' => true,
		'header' => false,
		'firstPageLabel' => '<<',
		'prevPageLabel' => '<',
		'nextPageLabel' => '>',
		'lastPageLabel' => '>>',
	),
	'columns'=>array(
			array(
				"header"=>$data->getAttributeLabel('waktu_transaksi'),
				"value"=>'Lib::dateIndShortMonth($data->waktu_transaksi)',
				"name"=>'waktu_transaksi',
			),
			array(
				"header"=>$data->getAttributeLabel('gudang_asal'),
				"value"=>'$data->gudangAsal->nama_gudang',
				"name"=>'gudang_asal',
			),
			array(
				"header"=>$data->getAttributeLabel('gudang_tujuan'),
				"value"=>'$data->gudangTujuan->nama_gudang',
				"name"=>'gudang_tujuan',
			),
			array(
				"header"=>$data->getAttributeLabel('no_transaksi'),
				"value"=>function($data){
					if(Lib::accessBy("recieving","print")==true){
						return '<a href="#" onclick="cetak('.$data->id_item_transaksi.');">'.$data->no_transaksi.'</a>';
					}else{
						return $data->no_transaksi;
					}
				},
				"name"=>'no_transaksi',
				"type"=>'raw',
			),
			array(
				"header"=>"Item",
				"type"=>"raw",
				"value"=>function($data){
					$res='<ul>';
					foreach($data->daftarItemTransaksis as $row){
						$res.='<li>'.$row->idItem->nama_item.', '.$row->jumlah_transaksi.' '.$row->idItemSatuanBesar->idSatuan->alias_satuan.'</li>';
					}
					$res.='</ul>';
					return $res;
				}
			),
			array(
				"header"=>"Nominal",
				"type"=>"raw",
				"value"=>'number_format($data->nominal_transaksi,0)',
				"name"=>'nominal_transaksi',
			),
			array(
				"header"=>"User",
				"type"=>"raw",
				"value"=>'$data->userCreate->username."<br/>".Lib::dateIndShortMonth($data->waktu_transaksi,false)',
				"name"=>'user_create',
			),
			array(
				"header"=>"",
				"type"=>"raw",
				"value"=>function($row) {
					return '<a href="#" class="btn btn-success btn-xs" onClick="cetak('.$row->id_item_transaksi.')"><i class="fa fa-print"></i></a>';
				}
			),
			array(
				'class'=>'booster.widgets.TbButtonColumn',
				 'deleteConfirmation'=>'Anda yakin akan menhapus data?',
				'template'=>'{proses}',
				'buttons'=>array
				(
					'proses' => array
					(
						'label'=>'Terima',
						'icon'=>'fa fa-check',
						'url'=>'Yii::app()->createUrl("transferRecieve/create", array("id"=>$data->id_item_transaksi))',
						'options'=>array(
							'class'=>'btn btn-danger btn-xs',
							'onClick'=>'return confirm("Apakah Akan di terima ?")'
						),
					),
				),
				'header'=>CHtml::dropDownList('pageSize',$pageSize,array(10=>10,20=>20,50=>50,100=>100,200=>200,500=>500,1000=>1000),array(
					'onchange'=>"$.fn.yiiGridView.update('item-transaction-grid',{ data:{pageSize: $(this).val() }})",
				)),
			),
	),
));
?>
</div>

<?php
Yii::app()->clientScript->registerScript('validatex', '
function cetak(id){
	var left = (screen.width/2)-(900/2);
	var top = (screen.height/2)-(500/2);
	window.open("'.Yii::app()->createUrl('transfer/print').'/id/"+id,"","width=900,height=500,top="+top+",left="+left);
}
', CClientScript::POS_END);
?>