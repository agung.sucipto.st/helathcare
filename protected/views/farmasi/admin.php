<?php
$this->breadcrumbs=array(
	'Farmasi',
);
$this->title=array(
	'title'=>'Farmasi',
	'deskripsi'=>'Mengelola Transaksi Farmasi'
);

Yii::app()->clientScript->registerScript('search', "
$('.search-form form').submit(function(){
	$.fn.yiiGridView.update('item-transaksi-grid', {
		data: $(this).serialize()
	});
	return false;
});

");
?>

<div class="panel panel-default">
	<div class="panel-heading">
		<h3 class="panel-title pull-left">Farmasi</h3>
		<div style="clear:both"></div>
	</div>
	<div class="panel-body">
		<div class="search-form">
			<?php $this->renderPartial('_search',array(
			'model'=>$trx,
		)); ?>
		</div><!-- search-form -->
		<?php 
		// put this somewhere on top
		$pageSize=Yii::app()->user->getState('pageSize',Yii::app()->params['defaultPageSize']); ?>	
		<?php		
			$this->widget('booster.widgets.TbExtendedGridView', array(
				'id'=>'item-transaksi-grid',
				'type' => 'striped',
				'dataProvider' => $trx->searchPasien(),
				'summaryText'=>'Menampilkan {start}-{end} dari {count} hasil.',
				'selectableRows' => 2,
				'responsiveTable' => true,
				'enablePagination' => true,
				'pager' => array(
					'htmlOptions'=>array(
						'class'=>'pagination'
					),
					'maxButtonCount' => 5,
					'cssFile' => true,
					'header' => false,
					'firstPageLabel' => '<<',
					'prevPageLabel' => '<',
					'nextPageLabel' => '>',
					'lastPageLabel' => '>>',
				),
				'columns'=>array(
						array(
							"header"=>"Waktu Transaksi",
							"value"=>'Lib::dateInd($data->waktu_transaksi,false)'
						),
						array(
							"header"=>"No Registrasi",
							"value"=>'$data->idRegistrasi->no_registrasi'
						),
						array(
							"header"=>"No RM",
							"value"=>'Lib::MRN($data->idRegistrasi->id_pasien)'
						),
						array(
							"header"=>'Nama Pasien',
							"type"=>'Raw',
							"name"=>"nama_lengkap",
							"value"=>function($data){
								if($data->idRegistrasi->jenis_registrasi != 'OTC'){
									$icon='male';
									if($data->idRegistrasi->idPasien->idPersonal->jenis_kelamin=='Perempuan'){
										$icon='<i class="fa fa-venus" style="color:red;font-weight:bold;"></i>';
									}else{
										$icon='<i class="fa fa-mars" style="color:blue;font-weight:bold;"></i>';
									}
									return $icon.$data->idRegistrasi->idPasien->idPersonal->nama_lengkap;	
								}	 else {
									return $data->idRegistrasi->catatan;
								}			
							}
						),
						array(
							"header"=>"Jenis Registrasi",
							"value"=>'$data->idRegistrasi->jenis_registrasi'
						),
						array(
							"header"=>"Layanan",
							"value"=>'$data->idRegistrasi->idDepartemen->nama_departemen'
						),
						array(
							"header"=>"Jaminnan",
							"name"=>"id_penjamin",
							"value"=>function($data){
								if($data->idRegistrasi->jenis_jaminan=="UMUM"){
									echo "Umum";
								}else{
									foreach($data->idRegistrasi->jaminanPasiens as $row){
										echo $row->idPenjamin->nama_penjamin.'<br/>';
									}
								}
							}
						),
						
						array(
							"header"=>"Item",
							"type"=>"raw",
							"value"=>function($data){
								$return="<b>$data->no_transaksi</b><ul>";
								foreach($data->daftarItemTransaksis as $row){
									if ($row->id_daftar_tagihan != ''){
										if($row->is_racikan == 0){
											$return.="<li>R/ ".$row->idItem->nama_item." : ".$row->jumlah_transaksi." ".Item::getParentUnit($row->id_item)->idSatuan->nama_satuan."</li>";
										} else {
											$return.="<li>R/ ".$row->nama_racikan." : ".$row->jumlah_transaksi."</li>";
										}
									}
								}
								$return.="</ul>";
								return $return;
							}
						),
						array(
							"header"=>"Harga",
							"value"=>'number_format($data->nominal_transaksi)'
						),
						array(
							"header"=>"User Input",
							"type"=>"raw",
							"value"=>function($data){
								return $data->userCreate->username.'<br/>'.Lib::dateInd($data->time_create,false);
							}
						),
						array(
							"header"=>"",
							"type"=>"raw",
							"value"=>function($data){
								return '
								<buttn class="btn btn-primary btn-xs" title="Print E-Tiket" onclick="cetak('.$data->id_item_transaksi.');">
									<i class="fa fa-print"></i>
								</button>
								';
							}
						),
						array(
							'class'=>'booster.widgets.TbButtonColumn',
							 'deleteConfirmation'=>'Anda yakin akan menhapus data?',
							'template'=>'{delete}',
							'buttons'=>array
							(
								'delete' => array
								(
									'label'=>'Delete',
									'icon'=>'trash',
									'url'=>'Yii::app()->createUrl("itemTransaksi/hapusR",array("id"=>$data->id_item_transaksi))',
									'visible'=>'$data->xResep()',
									'options'=>array(
										'class'=>'btn btn-default btn-xs delete',
									),
								)
							),
							'header'=>'',
						),
				),
			));
			?>
	</div>
</div>

<?php
Yii::app()->clientScript->registerScript('validatex', '
function cetak(id){
	var left = (screen.width/2)-(900/2);
	var top = (screen.height/2)-(500/2);
	window.open("'.Yii::app()->createUrl('farmasi/printEtiket').'/id/"+id,"","width=900,height=500,top="+top+",left="+left);
}
', CClientScript::POS_END);
?>