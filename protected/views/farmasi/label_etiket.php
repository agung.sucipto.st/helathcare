<html>
	<head>
	<style>
		body{
			font-family:Arial;
			
		}
		.page{
			width:65mm;
			height:auto;
			margin-left:-4px;
		}
		.label{
			width:auto;
			height:39mm;
			padding-top:0.9mm;
			padding-left:1mm;
			padding-right:5px;
			margin-bottom:0mm;
		}
		
		.content{
			width:100%;
			font-size:7pt;
		}
		.label:first-child{
			margin-top:-7px;
		}
		.label:last-child{
			margin-bottom:-10px;
		}
		th{
			text-align:left;
		}
		table{
			font-size:9px;
			font-weight:bold;
		}
		.patient{
			width:100%;
			font-weight:bold;
			font-size:9pt;
			overflow: hidden;
    white-space: nowrap;
    text-overflow: ellipsis;
		}
		.info{
			font-weight:bold;
		}
		
		@media print {
		  #visible{
			  display:none;
		  }
		  #myP{
			  display:none;
		  }
		}
	input[type=checkbox]{
		-webkit-box-shadow: 0px 0px 0px 2px black;
		-moz-box-shadow: 0px 0px 0px 2px black;
		box-shadow: 0px 0px 0px 2px black;
	}
	</style>
	<head>
	<body>
		<div id="visible">
		<input id="visible" type="button" value="print" onclick="window.print();"/>
		<hr>
		</div>
		<div class="page" id="page">
		
		</div>
	
	<script type="text/javascript">
	var data = <?=$data;?>;
	function removeDOM(id){
		document.getElementById(`label${id}`).outerHTML = "";
	}
	
	function generateLabel(){
		document.getElementById("page").innerHTML = "";
		data.forEach((data, indx) => {
			var logo = '<center><img src="<?=Yii::app()->theme->baseUrl;?>/assets/images/admin-logo-small-black.png" width="50px"/></center>';
			var data = `
			<div class="label" id="label${indx}">
				<button id="visible" onClick="removeDOM(${indx})">Hapus Label</button>
				<div class="content">
				<table width="100%">
					<tr>
						<th colspan="2">${data.nama_pasien}</th>
						<td align="right" colspan="2">${logo}</td>
					</tr>
					<tr>
						<th>MRN#</th>
						<td>: ${data.no_rm}</td>
						<th>REG#</th>
						<td>: ${data.no_reg}</td>
					</tr>
					<tr>
						<th>${data.gudang}</th>
						<td>: ${data.waktu_transaksi}</td>
						<th>No Resep#</th>
						<td>: ${data.no_transaksi}</td>
					</tr>
				</table>
				<hr/>
				<table width="100%" style="border-bottom:1px solid #ccc;">
					<tr>
						<th>${data.nama_item}</th>
						<td>QTY : ${data.qty} ${data.satuan}</td>
					</tr><tr>
						<th colspan="2">${data.signa}</th>
					</tr>
				</table>
				<table width="100%" >
					<tr>
						<td><label><input type="checkbox"/>Pagi</label></td>
						<td><label><input type="checkbox"/>Siang</label></td>
						<td><label><input type="checkbox"/>Sore</label></td>
						<td><label><input type="checkbox"/>Malam</label></td>
					</tr>
				</table>
				
				</div>
			</div>`;
			document.getElementById("page").innerHTML+=data;
		});
	}
	generateLabel();
	window.print();
	</script>
	
	</body>
</html>