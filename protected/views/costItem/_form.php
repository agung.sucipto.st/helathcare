<?php $form=$this->beginWidget('booster.widgets.TbActiveForm',array(
	'id'=>'item-transaction-form',
	'enableAjaxValidation'=>true,
	'type'=>"vertical",
)); 
?>

<p class="help-block">Isian dengan tanda <span class="required">*</span> wajib diisi.</p>
<?php echo $form->errorSummary($model); ?>
<div class="row">
	<div class="col-sm-4">
		<?php echo $form->dateTimePickerGroup($model,'item_transaction_time',array('widgetOptions'=>array('options'=>array('format'=>'yyyy-mm-dd hh:ii','viewFormat'=>'yyyy-mm-dd hh:ii')),'prepend'=>'<i class="glyphicon glyphicon-calendar"></i>')) ;?>
	</div>
	<div class="col-sm-4">
		<?php echo $form->dropDownListGroup($model,'warehouse_origin_id', array('widgetOptions'=>array('data'=>CHtml::listData(Warehouse::model()->findAll(array("condition"=>"	is_allow_receiveing='1'")),'warehouse_id','warehouse_name'), 'htmlOptions'=>array('class'=>'input-large','empty'=>'- Choose -')))); ?>
	</div>
	<div class="col-sm-4">
		<?php echo $form->textAreaGroup($model,'item_transaction_note', array('widgetOptions'=>array('htmlOptions'=>array('rows'=>1, 'cols'=>50, 'class'=>'span8')))); ?>
	</div>
</div>
<div class="row">
	<div class="col-sm-4">
		<label>Cari Nama Item / Kode Item</label>
		<?php
		$this->widget('zii.widgets.jui.CJuiAutoComplete',array(
			'name'=>'resep',
			'options'=>array(
				'minLength'=>3,
				'showAnim'=>'fold',
				'select'=>"js:function(event, data) {
					var item=data.item.label;
					addRow(item);
				}"
			),
			'source'=>$this->createUrl("item/getItem"),
			'htmlOptions'=>array(
				'class'=>"form-control",
				"id"=>"searchItem"
			),
		));
		?>
	</div>
</div>

<hr>
<table class="table table-striped">
	<tr>
		<th>Kode Item</th>
		<th>Nama Item</th>
		<th>Stok</th>
		<th>Jumlah CIU</th>
		<th>Satuan</th>
		<th></th>
	</tr>
	<tbody id="data">
		
	</tbody>
</table>
<hr>

<div class="form-actions">
	<?php $this->widget('booster.widgets.TbButton', array(
			'buttonType'=>'submit',
			'context'=>'primary',
			'label'=>$model->isNewRecord ? 'Tambah Cost Item' : 'Simpan',
			'htmlOptions'=>array(
				//"disabled"=>true,
				"id"=>"submit",
				"onClick"=>"return confirm('Apakah Cost Item Usage Sudah Benar?');"
			),
		)); 
		echo CHtml::button('Batal', array(
            'class' => 'btn btn-primary',
            'onclick' => "history.go(-1)",
                )
        );
		?>

		</div>

<?php $this->endWidget(); ?>

<?php
Yii::app()->clientScript->registerScript('addResep', '

var rowItem=[];
var Item=[];
function deleteRow(id){
	$("#row"+id).remove();
	rowItem.splice(id, 1);
	Item.splice(id, 1);
}

function addRow(item){
	var gudang = $("#ItemTransaction_warehouse_origin_id").val();
	if(gudang != ""){
	$.ajax({
		url: "'.Yii::app()->createAbsoluteUrl('item/getItem').'",
		cache: false,
		type: "POST",
		data:"item="+item+"&gudang="+gudang,
		success: function(msg){
			var data=$.parseJSON(msg);
			if(rowItem[data.id_item] == undefined){
				console.log(data);
				rowItem[data.id_item]=data.nama_item;
				Item.push(data.id_item);
				$("#searchItem").val("");
				var inputItem="<input type=\"hidden\" name=\"item[]\" value=\""+data.item_id+"|"+data.item_unit_id+"|"+data.parent+"|"+data.nilaiKeSatuanKecil+"\"/>"+data.nama_item;
				var stokItem=data.stok;
				var qty="<input type=\"number\" name=\"qty"+data.id_item+"\" id=\"qty"+data.id_item+"\" value=\"\" class=\"form-control\" required=\"required\" min=\"1\" onchange=\"cek(this,this.value,"+stokItem+");\"/>";
				var satuan=data.satuan;
				
				var remove="<span class=\"btn btn-danger\" onclick=\"deleteRow("+data.id_item+")\"><i class=\"fa fa-times\"></i></span>";
				$("#data").append("<tr id=\"row"+data.id_item+"\"><td>"+data.code+"</td><td>"+inputItem+"</td><td>"+stokItem+"</td><td>"+qty+"</td><td>"+satuan+"</td><td>"+remove+"</td></tr>");
				
			}else{
				$("#searchItem").val("");
			}
		}
	});
	}else{
		$("#searchItem").val("");
		alert("Silahkan Pilih Gudang Cost Item Usage !");
	}
}

$("#ItemTransaction_warehouse_origin_id").change(function(){
	jQuery.each(Item,function(){
		var id = parseInt(this);
		deleteRow(id);
	});
});

function cek(obj,val,max){
	if(val>max || val == 0){
		alert("Jumlah CIU Tidak Boleh Melebihi Stok !");
		obj.value="";
	}
}
', CClientScript::POS_END);
?>
