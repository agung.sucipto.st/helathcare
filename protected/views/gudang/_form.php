<?php $form=$this->beginWidget('booster.widgets.TbActiveForm',array(
	'id'=>'gudang-form',
	'enableAjaxValidation'=>true,
)); ?>

<p class="help-block">Isian dengan tanda <span class="required">*</span> wajib diisi.</p>

<?php echo $form->errorSummary($model); ?>

	<?php echo $form->textFieldGroup($model,'nama_gudang',array('widgetOptions'=>array('htmlOptions'=>array('class'=>'span5','maxlength'=>100)))); ?>

	<?php echo $form->dropDownListGroup($model,'status', array('widgetOptions'=>array('data'=>array("Aktif"=>"Aktif","Tidak Aktif"=>"Tidak Aktif",), 'htmlOptions'=>array('class'=>'input-large')))); ?>

	<?php echo $form->textFieldGroup($model,'status_penerimaan',array('widgetOptions'=>array('htmlOptions'=>array('class'=>'span5','maxlength'=>1)))); ?>

	<?php echo $form->textFieldGroup($model,'id_gl_unit',array('widgetOptions'=>array('htmlOptions'=>array('class'=>'span5')))); ?>

<div class="form-actions">
	<?php $this->widget('booster.widgets.TbButton', array(
			'buttonType'=>'submit',
			'context'=>'primary',
			'label'=>$model->isNewRecord ? 'Tambah' : 'Simpan',
		)); 
		echo CHtml::button('Batal', array(
            'class' => 'btn btn-primary',
            'onclick' => "history.go(-1)",
                )
        );
		?>

		</div>

<?php $this->endWidget(); ?>
