

<div class="box box-primary">
	<div class="box-body">
		<div class="row">
			<div class="col-sm-1">
				<?php
				$img='';
				$icon='';
				if($model->idPasien->idPersonal->jenis_kelamin=='Perempuan'){
					$img=Yii::app()->theme->baseUrl.'/assets/images/female.png';
					$icon='<i class="fa fa-venus" style="color:red;font-weight:bold;"></i>';
				}else{
					$img=Yii::app()->theme->baseUrl.'/assets/images/male.png';
					$icon='<i class="fa fa-mars" style="color:blue;font-weight:bold;"></i>';
				}	
				echo'<img src="'.$img.'" class="img-responsive"/>';
				?>
			</div>
			<div class="col-sm-7">
				<div class="row">
					<div class="col-sm-12" style="font-size:25px;font-weight:bold;">
					<?php
						if($model->jenis_registrasi != 'OTC') {
							echo Lib::MRN($model->id_pasien).' - '.$icon.strtoupper($model->idPasien->idPersonal->nama_lengkap).", ".$model->idPasien->panggilan;
						}	else {
							echo Lib::MRN($model->id_pasien).' - '.$model->catatan;
						}
					?>
					</div>
				</div>
				<div class="row">
					<div class="col-sm-12">
					<?php
						if($model->jenis_registrasi != 'OTC') {
							
							echo $model->idPasien->idPersonal->jenis_kelamin.' / '.Lib::dateInd($model->idPasien->idPersonal->tanggal_lahir,false).", ".Lib::umurDetail($model->idPasien->idPersonal->tanggal_lahir);	
							?>
							<br/>
					<span class="text-danger text-bold">Alergi</span> : <?=$model->idPasien->idPersonal->alergi;?>
							<?php
						}
					?>
					
					</div>					
				</div>
				
			</div>
			<div class="col-sm-4">
				<div class="row">
					<div class="col-sm-4 text-danger text-bold">No Registrasi</div>
					<div class="col-sm-8">: <?=$model->no_registrasi;?></div>
				</div>
				<div class="row">
					<div class="col-sm-4 text-danger text-bold">Waktu Registrasi</div>
					<div class="col-sm-8">: <?=Lib::dateInd($model->waktu_registrasi,false);?></div>
				</div>
				<div class="row">
					<div class="col-sm-4 text-danger text-bold">Dokter</div>
					<div class="col-sm-8">: 
					<?php
					foreach($model->dpjps as $row){
						echo $row->idDokter->idPersonal->nama_lengkap.'<br/>';
					}
					?>
					</div>
				</div>
				<div class="row">
					<div class="col-sm-4 text-danger text-bold">Layanan</div>
					<div class="col-sm-8">: <?=$model->idDepartemen->nama_departemen;?></div>
				</div>
				<div class="row">
					<div class="col-sm-4 text-danger text-bold">Jenis Jaminan</div>
					<div class="col-sm-8">: 
					<?php
						if($model->jenis_jaminan=="UMUM"){
							echo "Umum";
						}else{
							foreach($model->jaminanPasiens as $row){
								echo $row->idPenjamin->nama_penjamin.'<br/>';
							}
						}
					?>
					</div>
				</div>
				<div class="row">
					<div class="col-sm-4 text-danger text-bold">Catatan</div>
					<div class="col-sm-8">: <?=$model->catatan;?></div>
				</div>
			</div>
		</div>
	</div>
</div>


<div class="nav-tabs-custom">
	<ul class="nav nav-tabs">
		<li class="<?php echo ((Yii::app()->controller->action->id=="daftarTagihan")?"active":"");?>"><a href="<?php echo Yii::app()->createUrl("kasir/daftarTagihan",array("id"=>$model->id_registrasi));?>">Tagihan</a></li>
		<li class="<?php echo ((Yii::app()->controller->action->id=="daftarPembayaran")?"active":"");?>"><a href="<?php echo Yii::app()->createUrl("kasir/daftarPembayaran",array("id"=>$model->id_registrasi));?>">Pembayaran</a></li>
		<li class="<?php echo ((Yii::app()->controller->action->id=="bayar")?"active":"");?>" style="display:<?php echo ((Yii::app()->controller->action->id=="bayar")?"block":"none");?>"><a href="#">Pembayaran</a></li>
	</ul>
	<div class="tab-content">
		<div class="tab-pane active">
			<?php
			$action=Yii::app()->controller->action->id;
			if($action=="daftarTagihan"){
				echo $this->renderPartial('_daftar_tagihan_umum',array("model"=>$model,"tagihan"=>$tagihan));
			}elseif($action=="daftarPembayaran"){
				echo $this->renderPartial('_daftar_pembayaran',array("model"=>$model,"tagihan"=>$tagihan));
			}elseif($action=="bayar"){
				echo $this->renderPartial('_bayar',array("model"=>$model,"tagihan"=>$tagihan));
			}
			?>
		</div><!-- /.tab-pane -->
	</div><!-- /.tab-content -->
</div>