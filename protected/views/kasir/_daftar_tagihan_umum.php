<?php
$this->breadcrumbs=array(
	'Tagihan Pasien',
);
$this->title=array(
	'title'=>'Tagihan Pasien',
	'deskripsi'=>''
);
?>

<?php 
	// put this somewhere on top
	$pageSize=Yii::app()->user->getState('pageSize',Yii::app()->params['defaultPageSize']); ?>		
	<?php
	$groupGridColumns = array(
		array(
			"name"=>"waktu_daftar_tagihan",
			"header"=>"Waktu Tagihan",
			"value"=>'Lib::dateIndShortMonth($data->waktu_daftar_tagihan)',
		),
		array(
			"name"=>"id_jenis_komponen_tagihan",
			"header"=>"Komponen tagihan",
			"value"=>'$data->idJenisKomponenTagihan->jenis_komponen_tagihan',
		),
		array(
			"name"=>"deskripsi_komponen",
			"header"=>"Deskripsi",
			"value"=>'$data->deskripsi_komponen',
		),
		array(
			'name' => 'jumlah',
			'header' => 'Jumlah',
			"value"=>'number_format($data->jumlah,0)',
		),
		array(
			'header' => 'Harga @',
			'name' => 'harga',
			"value"=>'number_format($data->harga,0)',
		),
		array(
			'header' => 'Total Harga',
			'name' => 'jumlah_bayar',
			"value"=>'number_format($data->jumlah_bayar,0)',
		),
		/*
		array(
			'header'=>'Discount',
			'name' => 'discount',
			'type'=>'raw',
			'value'=>function($data){
				$discount=0;
				if($data->discount==""){
					$discount='';
				}else{
					$discount= $data->discount;
				}
				
				return $discount.' <button for="'.$data->id_daftar_tagihan.'" class="btn btn-danger btn-xs" id="setDiscount" data-toggle="tooltip" title="Atur Discount"><i class="fa fa-money"></i></button>';
			},
		),
		*/
		array(
			'header' => 'Total Bayar',
			"value"=>'number_format(($data->harga*$data->jumlah)-$data->discount,0)',
		),
		array(
			'class'=>'booster.widgets.TbButtonColumn',
			 'deleteConfirmation'=>'Anda yakin akan menhapus data?',
			'template'=>'{delete}',
			'buttons'=>array
			(
				'delete' => array
				(
					'label'=>'Delete',
					'icon'=>'trash',
					'visible'=>'($data->id_jenis_komponen_tagihan!="3")',
					'options'=>array(
						'class'=>'btn btn-default btn-xs delete',
					),
				)
			),
			'header'=>''
		),
	);
	$this->widget('booster.widgets.TbExtendedGridView', array(
		'type'=>'',
		'id'=>"daftar-tagihan-grid",
		'dataProvider' => $tagihan->search(),
		'summaryText'=>false,
		'selectableRows' => 2,
		'responsiveTable' => true,
		'enablePagination' => true,
		'pager' => array(
			'htmlOptions'=>array(
				'class'=>'pagination'
			),
			'maxButtonCount' => 5,
			'cssFile' => true,
			'header' => false,
			'firstPageLabel' => '<<',
			'prevPageLabel' => '<',
			'nextPageLabel' => '>',
			'lastPageLabel' => '>>',
		),
		'bulkActions' => array(
			'actionButtons' => array(
				array(
					'buttonType' => 'button',
					'context' => 'primary',
					'size' => 'small',
					'label' => 'Buat Tagihan',
					'click' => 'js:bill',
					'id'=>'id_daftar_tagihan'
				),
			),
			'checkBoxColumnConfig' => array(
				'name' => 'id_daftar_tagihan'
			),
		),
		'columns' => $groupGridColumns,
	));
?>

<?php
Yii::app()->clientScript->registerScript('confirm', '
	var gridId = "daftar-tagihan-grid";
	$(document).ready(function(){  
	  $(function(){					
			$(document).on("click","#setDiscount", function() {
				$.fn.yiiGridView.update(gridId);
        	});			
	   });
	   
	});
	
	
	function bill(values){
		if(confirm("Apakah Yakin Akan Membuat Tagihan?")){
			$.ajax({
				type: "POST",
				url: "'.Yii::app()->createUrl(Yii::app()->controller->id.'/daftarTagihan',array("id"=>$model->id_registrasi)).'",
				data: {"ids":values,"id":"'.$model->id_registrasi.'"},
				dataType:"json",
				success: function(resp){
					if(resp.status=="200"){
						window.location.href="'.Yii::app()->createUrl('kasir/daftarPembayaran',array("id"=>$model->id_registrasi)).'";
					}else{
						alert("Tagihan Gagal Dibuat !");
					}
				}
			});
		}
	}
', CClientScript::POS_END);
?>