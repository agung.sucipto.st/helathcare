<?php
$this->breadcrumbs=array(
	'Pembayaran Tagihan',
);
$this->title=array(
	'title'=>'Pembayaran Tagihan',
	'deskripsi'=>''
);

Yii::app()->clientScript->registerScript('search', "

");
?>


<?php 
	// put this somewhere on top
	$pageSize=Yii::app()->user->getState('pageSize',Yii::app()->params['defaultPageSize']); ?>		
	<?php
	$groupGridColumns = array(
		array(
			"name"=>"no_tagihan",
			'sortable'=>false,
			"header"=>"No Tagihan",
			"value"=>'$data->no_tagihan',
		),
		array(
			"name"=>"waktu_tagihan",
			'sortable'=>false,
			"header"=>"Waktu Tagihan",
			"value"=>'Lib::dateIndShortMonth($data->waktu_tagihan)',
		),
		array(
			'name' => 'total_tagihan',
			'sortable'=>false,
			'header' => 'Total Tagihan',
			"value"=>'number_format($data->total_tagihan,0)',
		),
		array(
			'header' => 'Total Bayar',
			'sortable'=>false,
			"value"=>function($data){
				$total=0;
				$bayar=PembayaranTagihan::model()->findAll(array("condition"=>"id_tagihan_pasien='$data->id_tagihan_pasien'"));
				foreach($bayar as $row){
					$total+=$row->total_dibayarkan-$row->total_dikembalikan;
				}
				return number_format($total,0);
			}
		),
		array(
			"header"=>"User Billed",
			'sortable'=>false,
			"type"=>"raw",
			"value"=>function($data){
				return $data->userCreate->idPegawai->idPersonal->nama_lengkap.'<br/>'.Lib::dateIndShortMonth($data->time_create,false);
			}
		),
		array(
			'header' => 'User Payment',
			'sortable'=>false,
			"value"=>'',
		),
		array(
			'header' => 'Status',
			'sortable'=>false,
			"value"=>'$data->status_tagihan',
		),
		array(
			'header' => '',
			'type' => 'raw',
			"value"=>function($data){
				$button='';
				if($data->status_tagihan=="Belum Lunas"){
					$button.='<a href="'.Yii::app()->createUrl('kasir/bayar',array("id"=>$data->id_tagihan_pasien,"idReg"=>$_GET['id'])).'" class="btn btn-success btn-xs btn-block"><i class="fa fa-money"></i> Bayar</a>';
				}else{
					$button.='<a class="btn btn-default btn-xs btn-block" onclick="openKwitansi('.$data->id_tagihan_pasien.');"><i class="fa fa-print"></i> Cetak Kwitansi</a>';
				}
				$button.='<button class="btn btn-warning btn-xs btn-block" onclick="openBill('.$data->id_tagihan_pasien.');"><i class="fa fa-print"></i> Cetak tagihan</button>';
				
				return $button;
			},
		),
	);
	$this->widget('booster.widgets.TbExtendedGridView', array(
		'type'=>'',
		'id'=>"daftar-tagihan-grid",
		'dataProvider' => $tagihan->search(),
		'summaryText'=>false,
		'selectableRows' => 2,
		'responsiveTable' => true,
		'enablePagination' => true,
		'pager' => array(
			'htmlOptions'=>array(
				'class'=>'pagination'
			),
			'maxButtonCount' => 5,
			'cssFile' => true,
			'header' => false,
			'firstPageLabel' => '<<',
			'prevPageLabel' => '<',
			'nextPageLabel' => '>',
			'lastPageLabel' => '>>',
		),
		'columns' => $groupGridColumns,
	));
?>

<?php
Yii::app()->clientScript->registerScript('validate', '
function openBill(id){
	var left = (screen.width/2)-(600/2);
	var top = (screen.height/2)-(600/2);
	window.open("'.Yii::app()->createUrl('kasir/tagihan',array("id"=>$model->id_pasien,"idReg"=>$_GET['id'])).'&idBill="+id,"","width=600,height=600,top="+top+",left="+left);
}

function openKwitansi(id){
	var left = (screen.width/2)-(800/2);
	var top = (screen.height/2)-(400/2);
	window.open("'.Yii::app()->createUrl('kasir/kwitansi').'?idBill="+id,"","width=800,height=400,top="+top+",left="+left);
}

', CClientScript::POS_END);
?>