<?php
$this->breadcrumbs=array(
	'Pembayaran Pasien',
);
$this->title=array(
	'title'=>'Pembayaran Pasien',
	'deskripsi'=>''
);

Yii::app()->clientScript->registerScript('search', "
$('.search-form form').submit(function(){
	$.fn.yiiGridView.update('registrasi-grid', {
		data: $(this).serialize()
	});
	return false;
});

");
?>

<div class="panel panel-default">
	<div class="panel-heading">
		<h3 class="panel-title pull-left">Pembayaran Pasien</h3>
		<a href="<?php echo Yii::app()->createUrl(Yii::app()->controller->id.'/create');?>" class="btn btn-primary btn-xs pull-right" style="display:<?php echo ((Lib::accessBy('resepPasien','create')==true)?"block":"");?>">
			<i class="fa fa-copy"></i>
			<span>Tambah Data</span>
		</a>
		<div style="clear:both"></div>
	</div>
	<div class="panel-body">
		<div class="search-form">
			<?php $this->renderPartial('_search',array('model'=>$model)); ?>
		</div>
		
		<style>
		.pagination li.selected a{
			background:rgb(235, 235, 236);
		}
		</style>
		
		
		<?php
		if(Yii::app()->user->hasFlash('success')){
			echo'
			<div class="alert alert-success">
				<button type="button" class="close" data-dismiss="alert">&times;</button>
				'.Yii::app()->user->getFlash('success').'
			</div>';
		}
		?>
							
		<div id="example-2_wrapper" class="dataTables_wrapper form-inline dt-bootstrap no-footer">
		
		<?php 
		// put this somewhere on top
		$pageSize=Yii::app()->user->getState('pageSize',Yii::app()->params['defaultPageSize']); ?>		
		<?php		
		$this->widget('booster.widgets.TbExtendedGridView', array(
			'id'=>'registrasi-grid',
			'type' => 'striped',
			'dataProvider' => $model->searchPembayaran(),
			'summaryText'=>'Menampilkan {start}-{end} dari {count} hasil.',
			'selectableRows' => 2,
			'responsiveTable' => true,
			'enablePagination' => true,
			'pager' => array(
				'htmlOptions'=>array(
					'class'=>'pagination'
				),
				'maxButtonCount' => 5,
				'cssFile' => true,
				'header' => false,
				'firstPageLabel' => '<<',
				'prevPageLabel' => '<',
				'nextPageLabel' => '>',
				'lastPageLabel' => '>>',
			),
			'columns'=>array(
					array(
						"header"=>"Waktu Registrasi",
						"name"=>"waktu_registrasi",
						"value"=>'Lib::dateIndShortMonth($data->waktu_registrasi,false)'
					),
					array(
						"header"=>"No Registrasi",
						"name"=>"no_registrasi",
						"value"=>'$data->no_registrasi'
					),
					array(
						"header"=>"No RM",
						"name"=>"id_pasien",
						"value"=>'Lib::MRN($data->id_pasien)'
					),
					array(
						"header"=>'Nama Pasien',
						"type"=>'Raw',
						"name"=>"nama_lengkap",
						"value"=>function($data){
							if($data->jenis_registrasi != 'OTC'){
								$icon='male';
								if($data->idPasien->idPersonal->jenis_kelamin=='Perempuan'){
									$icon='<i class="fa fa-venus" style="color:red;font-weight:bold;"></i>';
								}else{
									$icon='<i class="fa fa-mars" style="color:blue;font-weight:bold;"></i>';
								}
								return $icon.$data->idPasien->idPersonal->nama_lengkap;	
							}	 else {
								return $data->catatan;
							}			
						}
					),
					array(
						"header"=>"Layanan",
						"name"=>"id_departement",
						"value"=>'$data->idDepartemen->nama_departemen'
					),
					array(
						"header"=>"Jaminnan",
						"name"=>"id_penjamin",
						"value"=>function($data){
							if($data->jenis_jaminan=="UMUM"){
								echo "Umum";
							}else{
								foreach($data->jaminanPasiens as $row){
									echo $row->idPenjamin->nama_penjamin.'<br/>';
								}
							}
						}
					),
					array(
						"header"=>"Outstanding",
						"value"=>function($data){
							$criteria=new CDbCriteria;
							$criteria->condition="id_tagihan is NULL AND id_registrasi='$data->id_registrasi'";
							$total=DaftarTagihan::model()->findAll($criteria);
							$outstanding=0;
							foreach($total as $row){
								$outstanding+=$row->jumlah*$row->harga;
							}
							if($outstanding>0){
								return number_format($outstanding,0);
							}else{
								return '';
							}							
						}
					),
					array(
						"header"=>"Bill",
						"value"=>function($data){
							$criteria=new CDbCriteria;
							$criteria->condition="id_registrasi='$data->id_registrasi'";
							$total=TagihanPasien::model()->findAll($criteria);
							$outstanding=0;
							foreach($total as $row){
								$outstanding+=$row->total_tagihan;
							}
							if($outstanding>0){
								return number_format($outstanding,0);
							}else{
								return '';
							}							
						}
					),
					array(
						"header"=>"Payment",
						"value"=>function($data){
							$criteria=new CDbCriteria;
							$criteria->with=array("idTagihanPasien");
							$criteria->condition="idTagihanPasien.id_registrasi='$data->id_registrasi'";
							$total=PembayaranTagihan::model()->findAll($criteria);
							$outstanding=0;
							foreach($total as $row){
								$outstanding+=$row->total_dibayarkan-$row->total_dikembalikan;
							}
							if($outstanding>0){
								return number_format($outstanding,0);
							}else{
								return '';
							}							
						}
					),
					array(
						'header'=>CHtml::dropDownList('pageSize',$pageSize,array(10=>10,20=>20,50=>50,100=>100,200=>200,500=>500,1000=>1000),array(
							'onchange'=>"$.fn.yiiGridView.update('registrasi-grid',{ data:{pageSize: $(this).val() }})",
						)),
						'type'=>'raw',
						'value'=>function($data){
							$label='default';
							if(DaftarTagihan::getListTagihanUnprocess($data->id_registrasi)==true){
								$label='danger';
							}
							return '<a href="'.Yii::app()->createUrl("kasir/daftarTagihan",array("id"=>$data->id_registrasi)).'" class="btn btn-'.$label.' btn-xs" data-toggle="tooltip" title="" data-original-title="Proses Pembayaran" style="display:'.((Lib::accessBy('kasir','daftarTagihan')==true)?"block":"none").';"><i class="fa fa-money"></i></a>';
						}
					),
			),
		));
		?>
		</div>
	</div>
</div>