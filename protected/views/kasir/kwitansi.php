<?php
	$this->pageTitle="Invoice Pasien";
?>

<div class="container">
	<div class="row">
		<div class="col-xs-12">
			<?php $this->widget('booster.widgets.TbDetailView',array(
				'data'=>$tagihan,
					'attributes'=>array(
						array(
							"label"=>"No Tagihan",
							"value"=>$tagihan->no_tagihan,
						),
						array(
							"label"=>"No Registrasi",
							"value"=>$tagihan->idRegistrasi->no_registrasi,
						),
					),
				)); ?>
				
				
				<center>
					<h4>KWITANSI</h4>
					<br/>
				</center>
				
				<?php $this->widget('booster.widgets.TbDetailView',array(
				'data'=>$tagihan,
					'attributes'=>array(
						
						array(
							"label"=>"Sudah terima dari",
							"value"=>$tagihan->idRegistrasi->idPasien->idPersonal->nama_lengkap,
						),
						array(
							"label"=>"Nama Pasien",
							"value"=>$tagihan->idRegistrasi->idPasien->idPersonal->nama_lengkap,
						),
						array(
							"label"=>"Jumlah",
							"value"=>'Rp. '.number_format($tagihan->total_tagihan,0),
						),
						array(
							"label"=>"Terbilang",
							"type"=>"raw",
							"value"=>"<i>".ucwords(Lib::terbilang($tagihan->total_tagihan)).' Rupiah</i>',
						),
						array(
							"label"=>"Untuk Pembayaran",
							"value"=>"Layanan ".$tagihan->idRegistrasi->idDepartemen->nama_departemen." ( ".$tagihan->idRegistrasi->dpjps[0]->idDokter->idPersonal->nama_lengkap." )",
						),
					),
				)); ?>
		</div>
	</div>
	
	
	Waktu Cetak : <?php echo Lib::dateIndShortMonth(date("Y-m-d H:i:s"));?><br/>
	Dicetak Oleh : <?php echo User::model()->findByPk(Yii::app()->user->id)->idPegawai->idPersonal->nama_lengkap;?>
</div>