<?php
$this->breadcrumbs=array(
	'Bayar Tagihan',
);
$this->title=array(
	'title'=>'Bayar Tagihan',
	'deskripsi'=>''
);

Yii::app()->clientScript->registerScript('search', "
	var totalTagihan = $tagihan->total_tagihan;
	
	$('#bayarCash').click(function(){
		if($('#bayarCash').prop('checked')) {
			$('#cash').removeAttr('disabled');
			$('#cash').attr('required','required');
		} else {
			$('#cash').attr('disabled','disabled');
			$('#cash').removeAttr('required');
		}
	});
	
	$('#bayarCard').click(function(){
		if($('#bayarCard').prop('checked')) {
			$('#credit').removeAttr('disabled');
			$('#credit').attr('required','required');
			$('#norek').removeAttr('disabled');
			$('#norek').attr('required','required');
			$('#nocard').removeAttr('disabled');
			$('#nocard').attr('required','required');
			$('#type').removeAttr('disabled');
			$('#batch').removeAttr('disabled');
		} else {
			$('#credit').attr('disabled','disabled');
			$('#credit').removeAttr('required');
			$('#norek').attr('disabled','disabled');
			$('#norek').removeAttr('required');
			$('#nocard').attr('disabled','disabled');
			$('#nocard').removeAttr('required');
			$('#type').attr('disabled','disabled');
			$('#type').removeAttr('required');
			$('#batch').attr('disabled','disabled');
			$('#batch').removeAttr('required');
		}
	});
	
	
	$('#cash').keyup(function(){
		var value=parseInt($(this).val())+parseInt($('#credit').val());
		var sisa = parseInt(totalTagihan)-parseInt(value);
		if(parseInt(value) > parseInt(totalTagihan)){
			$('#sisa').html('0');
			$('#kembalian').html(sisa.toLocaleString());
		} else {
			$('#sisa').html(sisa.toLocaleString());
			$('#kembalian').html('0');
		}
		
		if(sisa > 0){
			$('#save').attr('disabled','disabled');
		} else {
			$('#save').removeAttr('disabled');
		}
	});
	
	$('#credit').keyup(function(){
		var value=parseInt($(this).val())+parseInt($('#cash').val());
		var sisa = parseInt(totalTagihan)-parseInt(value);
		if(parseInt(value) > parseInt(totalTagihan)){
			$('#sisa').html('0');
			$('#kembalian').html(sisa.toLocaleString());
			$('#save').attr('disabled','disabled');
		} else {
			$('#sisa').html(sisa.toLocaleString());
			$('#kembalian').html('0');
			if(sisa > 0){
				$('#save').attr('disabled','disabled');
			}else{
				$('#save').removeAttr('disabled');
			}
		}
	});
");
?>

<div class="row">
	<div class="col-sm-6">
		<?php $this->widget('booster.widgets.TbDetailView',array(
			'data'=>$tagihan,
				'attributes'=>array(
					array(
						"label"=>"No Tagihan",
						"value"=>$tagihan->no_tagihan,
					),
					array(
						"label"=>"Waktu Tagihan",
						"value"=>Lib::dateIndShortMonth($tagihan->waktu_tagihan),
					),
				),
			)); ?>
	</div>
	<div class="col-sm-6">
		<?php $this->widget('booster.widgets.TbDetailView',array(
			'data'=>$tagihan,
				'attributes'=>array(
					array(
						"label"=>"User Billed",
						"type"=>"raw",
						"value"=>function($tagihan){
							return $tagihan->userCreate->idPegawai->idPersonal->nama_lengkap.' / '.Lib::dateIndShortMonth($tagihan->time_create,false);
						}
					),
					array(
						'label' => 'Status',
						"value"=>$tagihan->status_tagihan,
					),
				),
			)); ?>
	</div>
</div>
<br/>

<div class="box box-success">
	<div class="box-body">
		
		
		<?php $form=$this->beginWidget('booster.widgets.TbActiveForm',array(
			'id'=>'pembayaran-tagihan-form',
			'enableAjaxValidation'=>false,
		)); ?>
		<table class="table">
				<tbody>
					<tr>
						<th style="width:30%;font-size:18px;">Total Tagihan</th>
						<th style="width:10%;font-size:18px;">:</th>
						<td style="font-size:18px;">Rp. <?=number_format($tagihan->total_tagihan);?></td>
						
						<th style="width:30%;font-size:18px;">Sisa Tagihan</th>
						<th style="width:10%;font-size:18px;">:</th>
						<td style="font-size:18px;">Rp. <span id="sisa"><?=number_format($tagihan->total_tagihan);?></span></td>
					</tr>
					<tr>
						<th style="width:30%;font-size:18px;">
						<input type="checkbox" id="bayarCash" checked/>
						Pembayaran Cash
						</th>
						<td colspan="2">
							<input type="number" name="PembayaranTagihan[Cash]" id="cash" class="form-control col-md-6" required="required" min="0" placeholder="Pembayaran Tunai"/>
						</td>
						<th style="width:30%;font-size:18px;">Kembalian</th>
						<th style="width:10%;font-size:18px;">:</th>
						<td style="font-size:18px;">
							Rp. <span id="kembalian">0</span>
						</td>
					</tr>
					<tr>
						<th style="width:30%;font-size:18px;">
						<input type="checkbox" id="bayarCard"/>
						Pembayaran Debit / Credit
						</th>
						<td colspan="2">
							<input type="number" name="PembayaranTagihan[Card][bayar]" id="credit" class="form-control col-md-6" required="required" min="0" placeholder="Pembayaran Debit / Credit" value="0" disabled/>
						</td>
						<td>
							<div class="row">
								<div class="col-sm-6">
									<select name="PembayaranTagihan[Card][no_rek]" id="norek" class="form-control" required="required" disabled>
										<?php
										$bank=BankAkun::model()->findAll(array("condition"=>"status='1'"));
										foreach($bank as $row){
											echo'<option value="'.$row->id_bank_akun.'">'.$row->idBank->nama_bank.' - '.$row->no_rekening.'</option>';
										}
										?>
									</select>
								</div>
								<div class="col-sm-6">
									<input type="number" name="PembayaranTagihan[Card][no_card]" id="nocard" class="form-control" required="required" placeholder="No Kartu" disabled/>
								</div>
							</div>
						</td>
						<td>
							<input type="number" name="PembayaranTagihan[Card][batch]" id="batch" class="form-control" required="required" placeholder="No Batch" disabled/>
						</td>
						<td>
							<select name="PembayaranTagihan[Card][type]" id="type" class="form-control" disabled>
								<option>Debit</option>
								<option>Credit</option>
							</select>
						</td>
					</tr>				
				</tbody>
		</table>
		
		<hr>
		<div class="form-actions" style="text-align:center;">
			<?php $this->widget('booster.widgets.TbButton', array(
				'buttonType' => 'submit',
				'context'=>'primary',
				'icon'=>'fa fa-money',
				'label'=>'Bayar',
				'htmlOptions'=>array(
					"id"=>"save",
					"onClick"=>"return confirm('Anda Yakin akan melakukan pembayaran?');"
				)
			)); ?>
		</div>

		<?php $this->endWidget(); ?>

	</div>
</div>
