<?php
	$this->pageTitle="Invoice Pasien";
?>

<center>
	<h4>INVOICE RAWAT JALAN / IGD</h4>
	<br/>
</center>
<div class="container">
	<div class="row">
		<div class="col-xs-12">
			<?php $this->widget('booster.widgets.TbDetailView',array(
				'data'=>$tagihan,
					'attributes'=>array(
						array(
							"label"=>"No Tagihan",
							"value"=>$tagihan->no_tagihan,
						),
						array(
							"label"=>"Nama Pasien",
							"value"=>$tagihan->idRegistrasi->idPasien->idPersonal->nama_lengkap,
						),
						array(
							"label"=>"No RM",
							"value"=>Lib::MRN($tagihan->idRegistrasi->id_pasien),
						),
						array(
							"label"=>"Layanan",
							"value"=>$tagihan->idRegistrasi->idDepartemen->nama_departemen,
						),
						array(
							"label"=>"Waktu Masuk",
							"value"=>Lib::dateIndShortMonth($tagihan->idRegistrasi->waktu_registrasi),
						),
						array(
							"label"=>"Waktu Keluar",
							"value"=>Lib::dateIndShortMonth($tagihan->idRegistrasi->waktu_tutup_registrasi),
						),
					),
				)); ?>
		</div>
	</div>
	<table class="table">
		<tr>
			<th>Keterangan</th>
			<th>QTY</th>
			<th>Harga</th>
			<th>Diskon</th>
			<th width="30%">Subtotal</th>
		</tr>
		<?php
			foreach($list as $row){
				echo'
				<tr>
					<td>'.$row->deskripsi_komponen.'</td>
					<td>'.$row->jumlah.'</td>
					<td style="text-align:right">'.number_format($row->harga,0).'</td>
					<td style="text-align:right">'.number_format($row->discount,0).'</td>
					<td style="text-align:right">'.number_format($row->jumlah_bayar,0).'</td>
				</tr>
				';
			}
		?>
		<tfoot>
			<tr>
				<th colspan="2">Total Tagihan</th>
				<td></td>
				<td></td>
				<td style="text-align:right">Rp. <?=number_format($tagihan->total_tagihan,0);?></td>
			</tr>
			<tr>
				<td colspan="5"><i><?=Ucwords(Lib::terbilang($tagihan->total_tagihan));?> Rupiah</i></td>
			</tr>
			<tr>
				<th colspan="5">PEMBAYARAN</th>
			</tr>
			<?php
				$bayar=PembayaranTagihan::model()->findAll(array("condition"=>"id_tagihan_pasien='$tagihan->id_tagihan_pasien'"));
				foreach($bayar as $row){
					if($row->jenis_pembayaran=="CASH"){
						echo'
						<tr>
							<td rowspan="2">'.Lib::dateIndShortMonth($row->waktu_pembayaran).'</td>
							<td>Tunai</td>
							<th style="text-align:right" colspan="3">Rp. '.number_format($row->total_dibayarkan,0).'</th>
						</tr>
						<tr>
							<td>Kembalian</td>
							<th style="text-align:right" colspan="3"> Rp.'.number_format($row->total_dikembalikan,0).'</th>
						</tr>
						';
					}elseif($row->jenis_pembayaran=="DEBIT" OR $row->jenis_pembayaran=="CREDIT"){
						echo'
						<tr>
							<td>'.Lib::dateIndShortMonth($row->waktu_pembayaran).'</td>
							<td colspan="2">'.$row->jenis_pembayaran.'<br/>'.$row->tujuanTransfer->idBank->nama_bank.' - '.$row->tujuanTransfer->no_rekening.'</td>
							<th style="text-align:right" colspan="2">Rp. '.number_format($row->total_dibayarkan,0).'</th>
						</tr>
						';
					}
					
				}
			?>
		</tfoot>
	</table>
	<br/>
	
	Waktu Cetak : <?php echo Lib::dateIndShortMonth(date("Y-m-d H:i:s"));?><br/>
	Dicetak Oleh : <?php echo User::model()->findByPk(Yii::app()->user->id)->idPegawai->idPersonal->nama_lengkap;?>
</div>