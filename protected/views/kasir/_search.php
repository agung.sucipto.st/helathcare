<?php $form=$this->beginWidget('booster.widgets.TbActiveForm',array(
	'action'=>Yii::app()->createUrl($this->route),
	'method'=>'get',
)); ?>

	<div class="row">
		<div class="col-sm-4">
			<?php echo $form->textFieldGroup($model,'id_pasien',array('widgetOptions'=>array('htmlOptions'=>array('class'=>'span5','maxlength'=>20)))); ?>
		</div>
		<div class="col-sm-4">
			<?php echo $form->textFieldGroup($model,'no_registrasi',array('widgetOptions'=>array('htmlOptions'=>array('class'=>'span5','maxlength'=>20)))); ?>
		</div>
		<div class="col-sm-4">
			<?php echo $form->textFieldGroup($model,'nama_lengkap',array('widgetOptions'=>array('htmlOptions'=>array('class'=>'span5','maxlength'=>20)))); ?>
		</div>
	</div>
		
	<div class="row">
		<div class="col-sm-4">
			<?php echo $form->datePickerGroup($model,'waktu_registrasi',array('widgetOptions'=>array('options'=>array('format'=>'yyyy-mm-dd','viewFormat'=>'yyyy-mm-dd'),'htmlOptions'=>array('class'=>'span5')))); ?>
		</div>
	</div>
		
	<div class="form-actions">
		<?php $this->widget('booster.widgets.TbButton', array(
			'buttonType' => 'submit',
			'context'=>'primary',
			'label'=>'Cari',
		)); ?>
	</div>

<?php $this->endWidget(); ?>
