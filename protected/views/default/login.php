<?php
/* @var $this SiteController */
/* @var $model LoginForm */
/* @var $form CActiveForm  */
$this->breadcrumbs=array(
	'Login',
);
?>
<div class="row">
	<div class="col-sm-4"></div>
	<div class="col-sm-4">
		<center>
			<img src="<?=Yii::app()->theme->baseUrl;?>/assets/logo.png" class="img-responsive"/>
		</center>
	</div>
	<div class="col-sm-4"></div>
</div>

<div class="form-box" id="login-box">
		
		<div class="col-md-12" style="padding:0px">
            <div class="header">
			<i class="icon-lock"></i> Login Area
			</div>
            <?php $form=$this->beginWidget('CActiveForm', array(
					'id'=>'form',
					'htmlOptions'=>array('class'=>'login-form'),
					'enableClientValidation'=>true,
					'clientOptions'=>array(
						'validateOnSubmit'=>true,
					),
				)); 
			?>
                <div class="body bg-gray">
                    <div class="form-group">
						<!--ie8, ie9 does not support html5 placeholder, so we just show field title for that-->
						<label class="control-label visible-ie8 visible-ie9">Username</label>
							<?php echo $form->textField($model,'username', array('class'=>'form-control placeholder-no-fix', 'id'=>'user', 'size'=>'100', 'placeholder'=>'Username', 'required'=>'required')); ?>
							<?php echo $form->error($model,'username'); ?>
					</div>
					<div class="form-group">
						<label class="control-label visible-ie8 visible-ie9">Password</label>
							<?php echo $form->passwordField($model,'password', array('class'=>'form-control placeholder-no-fix', 'id'=>'pwd', 'size'=>'100', 'placeholder'=>'Password', 'required'=>'required')); ?>
							<?php echo $form->error($model,'password'); ?>
					</div> 				
                </div>
				<div class="footer">                                                               
                    <button type="submit" class="btn bg-gray btn-block">Login</button>
                </div>
           <?php $this->endWidget(); ?>
        </div>
		<div style="clear:both"></div>
		
		
        </div>
		
		<br/>
		<br/>
		<center>
		Copyright &copy; 2017
		</center>