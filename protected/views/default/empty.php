<?php
Yii::app()->clientScript->registerScript('validate', '
	if(typeof(EventSource) !== "undefined") {
		var source = new EventSource("'.CController::createUrl('site/checkQuestioner').'");
		source.onmessage = function(event) {
			var msg = $.parseJSON(event.data);
			if(msg.questionercode=="yes"){
				window.location="http://100.100.100.176/irhis/site/questioner";
			}
			if(msg.questionersurvey=="yes"){
				window.location="http://100.100.100.176/irhis/site/questioner";
			}
		};
	} else {
		document.getElementById("result").innerHTML = "Sorry, your browser does not support server-sent events...";
	}
	$(document).ready(function(){
		$("#myModal").modal("show");
		setTimeout(function() {$("#myModal").modal("hide");}, 5000);
	});
', CClientScript::POS_END);
?>


<video width="100%" autoplay loop>
  <source src="<?=Yii::app()->theme->baseUrl;?>/assets/aulia/cop.mp4" type="video/mp4">
  Your browser does not support HTML5 video.
</video>
<br/>
<br/>
<br/>
<br/>
<!--
<div id="myCarousel" class="carousel slide" data-ride="carousel">

  <ol class="carousel-indicators">
    <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
    <li data-target="#myCarousel" data-slide-to="1"></li>
    <li data-target="#myCarousel" data-slide-to="2"></li>
  </ol>

  <div class="carousel-inner" role="listbox">
    <div class="item active">
      <img src="<?=Yii::app()->theme->baseUrl;?>/assets/aulia/1.jpg" class="img-responsive" alt="Aulia">
    </div>
	<div class="item">
      <img src="<?=Yii::app()->theme->baseUrl;?>/assets/aulia/2.jpg" class="img-responsive" alt="Aulia">
    </div>
	<div class="item">
      <img src="<?=Yii::app()->theme->baseUrl;?>/assets/aulia/3.jpg" class="img-responsive" alt="Aulia">
    </div>
  </div>

  <a class="left carousel-control" href="#myCarousel" role="button" data-slide="prev">
    <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
    <span class="sr-only">Previous</span>
  </a>
  <a class="right carousel-control" href="#myCarousel" role="button" data-slide="next">
    <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
    <span class="sr-only">Next</span>
  </a>
</div>
-->


<!-- Modal -->
<div id="myModal" class="modal fade" role="dialog">
  <div class="modal-dialog modal-lg">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title"></h4>
      </div>
      <div class="modal-body">
	  <img src="<?php echo Yii::app()->theme->baseUrl;?>/assets/logo.png" style=" padding: 20px 0 0 40px;"/>
        <p><?php
if(Yii::app()->user->hasFlash('success')){
	echo'
	<h3>
		'.Yii::app()->user->getFlash('success').'
	</h3>';
}
?></p>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>

  </div>
</div>