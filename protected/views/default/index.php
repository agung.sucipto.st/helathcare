<?php
$this->title['title']="Dashboard";
$this->title['deskripsi']="Home";
?>

<?php
$baseUrl = Yii::app()->theme->baseUrl; 
$cs = Yii::app()->getClientScript();
$cs->registerScriptFile($baseUrl.'/assets/highcharts/highcharts.js');
$cs->registerScriptFile($baseUrl.'/assets/highcharts/modules/exporting.js');
?>


<?php
Yii::app()->clientScript->registerScript('validate', '
Highcharts.setOptions({
    colors: Highcharts.map(Highcharts.getOptions().colors, function (color) {
        return {
            radialGradient: {
                cx: 0.5,
                cy: 0.3,
                r: 0.7
            },
            stops: [
                [0, color],
                [1, Highcharts.Color(color).brighten(-0.3).get("gb")] // darken
            ]
        };
    })
});

Highcharts.chart("pie", {
    chart: {
        plotBackgroundColor: null,
        plotBorderWidth: null,
        plotShadow: false,
        type: "pie"
    },
    title: {
        text: "Kunjungan <br/><small>Berdasarkan Jenis Kelamin</small>"
    },
    tooltip: {
        pointFormat: "{series.name}: <b>{point.percentage:.1f}%</b>"
    },
    plotOptions: {
        pie: {
            allowPointSelect: true,
            cursor: "pointer",
            dataLabels: {
                enabled: true,
                format: "<b>{point.name}</b>: {point.percentage:.1f} %",
                style: {
                    color: (Highcharts.theme && Highcharts.theme.contrastTextColor) || "black"
                }
            }
        }
    },
    series: [{
        name: "Jenis Kelamin",
        colorByPoint: true,
        data: ['.implode(",",$pieJk).']
    }]
});

Highcharts.chart("pie2", {
    chart: {
        plotBackgroundColor: null,
        plotBorderWidth: null,
        plotShadow: false,
        type: "pie"
    },
    title: {
        text: "Kunjungan <br/><small>Berdasarkan Jenis Kunjungan</small>"
    },
    tooltip: {
        pointFormat: "{series.name}: <b>{point.percentage:.1f}%</b>"
    },
    plotOptions: {
        pie: {
            allowPointSelect: true,
            cursor: "pointer",
            dataLabels: {
                enabled: true,
                format: "<b>{point.name}</b>: {point.percentage:.1f} %",
                style: {
                    color: (Highcharts.theme && Highcharts.theme.contrastTextColor) || "black"
                }
            }
        }
    },
    series: [{
        name: "Jenis Kunjungan",
        colorByPoint: true,
        data: ['.implode(",",$pieJn).']
    }]
});

Highcharts.chart("pie3", {
    chart: {
        plotBackgroundColor: null,
        plotBorderWidth: null,
        plotShadow: false,
        type: "pie"
    },
    title: {
        text: "Kunjungan <br/><small>Berdasarkan Jenis Pembayaran</small>"
    },
    tooltip: {
        pointFormat: "{series.name}: <b>{point.percentage:.1f}%</b>"
    },
    plotOptions: {
        pie: {
            allowPointSelect: true,
            cursor: "pointer",
            dataLabels: {
                enabled: true,
                format: "<b>{point.name}</b>: {point.percentage:.1f} %",
                style: {
                    color: (Highcharts.theme && Highcharts.theme.contrastTextColor) || "black"
                }
            }
        }
    },
    series: [{
        name: "Jenis Pembayaran",
        colorByPoint: true,
        data: ['.implode(",",$pieJp).']
    }]
});

Highcharts.chart("pie4", {
    chart: {
        plotBackgroundColor: null,
        plotBorderWidth: null,
        plotShadow: false,
        type: "pie"
    },
    title: {
        text: "Kunjungan <br/><small>Berdasarkan Jenis Layanan</small>"
    },
    tooltip: {
        pointFormat: "{series.name}: <b>{point.percentage:.1f}%</b>"
    },
    plotOptions: {
        pie: {
            allowPointSelect: true,
            cursor: "pointer",
            dataLabels: {
                enabled: true,
                format: "<b>{point.name}</b>: {point.percentage:.1f} %",
                style: {
                    color: (Highcharts.theme && Highcharts.theme.contrastTextColor) || "black"
                }
            }
        }
    },
    series: [{
        name: "Jenis Layanan",
        colorByPoint: true,
        data: ['.implode(",",$pieLayanan).']
    }]
});

Highcharts.chart("line", {
    chart: {
        type: "line"
    },
    title: {
        text: "Grafik Pertumbuhan Kunjungan"
    },
    subtitle: {
        text: "Periode '.date('F').'"
    },
    xAxis: {
         categories: ['.implode(",",$hLabel).'],
    },
    yAxis: {
        title: {
            text: "Jumlah Kunjungan"
        }
    },
    plotOptions: {
        line: {
            dataLabels: {
                enabled: true
            },
            enableMouseTracking: false
        }
    },
    series: ['.implode(",",$hValues).']
});

Highcharts.chart("line2", {
    chart: {
        type: "column"
    },
    title: {
        text: "Grafik Pendapatan"
    },
    subtitle: {
        text: "Periode '.date('F').'"
    },
    xAxis: {
         categories: ['.implode(",",$hLabelP).'],
    },
    yAxis: {
        title: {
            text: "Jumlah Pendapatan (Rp)"
        }
    },
    plotOptions: {
        column: {
            pointPadding: 0.2,
            borderWidth: 0
        },
        series: {
            borderWidth: 0,
            dataLabels: {
                enabled: true,
                rotation:0,
                style: {
                  fontSize: "10px",
                  fontFamily: "Verdana, sans-serif"
              }
            }
        }
    },
    series: ['.implode(",",$hValuesP).']
});
', CClientScript::POS_END);
?>

<div class="box box-primary">
	<div class="box-body">
		Dashboard
	</div>
	<hr>
	<div class="container-fluid">
		<div class="row">
			<div class="col-sm-4">
				<div class="small-box bg-yellow">
					<div class="inner">
							<h3>
								<?php echo Pasien::model()->count();?>
							</h3>
							<p>
								Total Pasien
							</p>
						</div>
						<div class="icon">
							<i class="ion ion-person-add"></i>
						</div>
						<a href="#" class="small-box-footer">
							Info <i class="fa fa-arrow-circle-right"></i>
						</a>
					</div>
			</div>
			<div class="col-sm-4">
				<div class="small-box bg-green">
					<div class="inner">
							<h3>
								<?php echo Registrasi::model()->count(array("condition"=>"MONTH(waktu_registrasi)='".date("m")."' AND YEAR(waktu_registrasi)='".date("Y")."' AND (jenis_registrasi='OP' OR jenis_registrasi='IGD') AND status_registrasi!='Batal'"));?>
							</h3>
							<p>
								Kunjungan <?php echo date("F");?>
							</p>
					</div>
					<div class="icon">
						<i class="ion ion-stats-bars"></i>
					</div>
					<a href="#" class="small-box-footer">
						Info <i class="fa fa-arrow-circle-right"></i>
					</a>
				</div>
			</div>
			<div class="col-sm-4">
				<div class="small-box bg-aqua">
					<div class="inner">
							<h3>
								<?php 
								$income=TagihanPasien::model()->find(array("with"=>array("idRegistrasi"),"select"=>"SUM(total_tagihan) AS total_tagihan","condition"=>"MONTH(waktu_tagihan)='".date("m")."' AND YEAR(waktu_tagihan)='".date("Y")."'  AND (idRegistrasi.jenis_registrasi='OP' OR idRegistrasi.jenis_registrasi='IGD')"));
								
								echo number_format($income->total_tagihan,0);
								?>
							</h3>
							<p>
								Pendapatan <?php echo date("F");?>
							</p>
					</div>
					<div class="icon">
						<i class="ion ion-pie-graph"></i>
					</div>
					<a href="#" class="small-box-footer">
						Info <i class="fa fa-arrow-circle-right"></i>
					</a>
				</div>
			</div>
		</div>
		<hr>
		<div class="row">
			<div class="col-md-6">
				<div id="line" style="width: 100%; height: 400px; margin: 0 auto"></div>
			</div>
			<div class="col-md-6">
				<div id="line2" style="width: 100%; height: 400px; margin: 0 auto"></div>
			</div>
		</div>
		<hr>
		<div class="row">
			<div class="col-md-6">
				<div id="pie3" style="width: 100%; height: 400px; margin: 0 auto"></div>
				<table class="table">
					<?php
					$total=0;
					foreach($pieJp as $row) {
						$data = CJSON::decode($row);
						echo'
						<tr>
							<td>'.$data['name'].'</td>
							<td>'.$data['y'].'</td>
						</tr>
						';
						$total+=$data['y'];
					}
					?>
					<tr>
						<th>Total</th>
						<th><?=$total;?></th>
					</tr>
				</table>
			</div>
			<div class="col-md-6">
				<div id="pie2" style="width: 100%; height: 400px; margin: 0 auto"></div>
				<table class="table">
					<?php
					$total=0;
					foreach($pieJn as $row) {
						$data = CJSON::decode($row);
						echo'
						<tr>
							<td>'.$data['name'].'</td>
							<td>'.$data['y'].'</td>
						</tr>
						';
						$total+=$data['y'];
					}
					?>
					<tr>
						<th>Total</th>
						<th><?=$total;?></th>
					</tr>
				</table>
			</div>
		</div>
		<hr>
		<div class="row">
			<div class="col-md-6">
				<div id="pie" style="width: 100%; height: 400px; margin: 0 auto"></div>
				<table class="table">
					<?php
					$total=0;
					foreach($pieJk as $row) {
						$data = CJSON::decode($row);
						echo'
						<tr>
							<td>'.$data['name'].'</td>
							<td>'.$data['y'].'</td>
						</tr>
						';
						$total+=$data['y'];
					}
					?>
					<tr>
						<th>Total</th>
						<th><?=$total;?></th>
					</tr>
				</table>
			</div>
			<div class="col-md-6">
				<div id="pie4" style="width: 100%; height: 400px; margin: 0 auto"></div>
				<table class="table">
					<?php
					$total=0;
					foreach($pieLayanan as $row) {
						$data = CJSON::decode($row);
						echo'
						<tr>
							<td>'.$data['name'].'</td>
							<td>'.$data['y'].'</td>
						</tr>
						';
						$total+=$data['y'];
					}
					?>
					<tr>
						<th>Total</th>
						<th><?=$total;?></th>
					</tr>
				</table>
			</div>
		</div>
	</div>
</div>