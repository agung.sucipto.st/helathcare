<?php
$this->title['title']="Laporan Rujukan";
$this->title['deskripsi']="";
$this->breadcrumbs=array(
	'Laporan Rujukan',
);
?>

<div class="box box-primary">
	<div class="box-body">
	
		<?php

		Yii::app()->clientScript->registerScript('search', "
		$('.search-form form').submit(function(){
			$.fn.yiiGridView.update('registrasi-grid', {
				data: $(this).serialize()
			});
			var tgl = $('#Registrasi_waktu_registrasi').val();
			$('#tgl').html(tgl);
			return false;
		});

		");
		?>
		<div class="search-form">
		<?php
		$form = $this->beginWidget(
			'booster.widgets.TbActiveForm',
			array(
				'id' => 'horizontalForm',
				'type' => 'horizontal',
				'action'=>Yii::app()->createUrl($this->route),
				'method'=>'get',
			)
		); ?>
		
		<div class="row">
			<div class="col-xs-6">
				<?php echo $form->dateRangeGroup(
					$model,
					'waktu_registrasi',
					array(
						'label'=>'Periode Registrasi',
						'wrapperHtmlOptions' => array(
							'class' => '',
						),
						'prepend' => '<i class="glyphicon glyphicon-calendar"></i>'
					)
				); ?>
				<input type="hidden" name="Registrasi[status_keluar]" value="Dirujuk"/>
			</div>
			<div class="col-xs-6">
				<?php $this->widget(
					'booster.widgets.TbButton',
					array(
						'buttonType' => 'submit',
						'context' => 'primary',
						'label' => 'Tampilkan Data'
					)
				); ?>
				<?php $this->widget(
					'booster.widgets.TbButton',
					array('buttonType' => 'reset', 'label' => 'Reset')
				); ?>
			</div>
		</div>
		<?php
		$this->endWidget();
		?>
		</div>
		<?php
			$this->renderPartial('_data_rujukan',array(
				'model'=>$model
			));
		?>
	</div>
</div>