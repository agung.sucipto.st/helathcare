<?php
$this->title['title']="Grafik Jenis Layanan";
$this->title['deskripsi']="";
$this->breadcrumbs=array(
	'Grafik Jenis Layanan',
);
?>

<?php
$baseUrl = Yii::app()->theme->baseUrl; 
$cs = Yii::app()->getClientScript();
$cs->registerScriptFile($baseUrl.'/assets/highcharts/highcharts.js');
$cs->registerScriptFile($baseUrl.'/assets/highcharts/modules/exporting.js');
?>


<?php
Yii::app()->clientScript->registerScript('validate', '
Highcharts.chart("container", {
    chart: {
        type: "column"
    },
    title: {
        text: "'.$title.'"
    },
    xAxis: {
        categories: ['.implode(",",$hLabel).'],
		labels: {
			style: {
				fontSize:"8px"
			}
		}
    },
	plotOptions: {
        column: {
            pointPadding: 0.2,
            borderWidth: 0
        },
        series: {
            borderWidth: 0,
            dataLabels: {
                enabled: true,
                rotation:0,
                style: {
                  fontSize: "10px",
                  fontFamily: "Verdana, sans-serif"
              }
            }
        }
    },
    credits: {
        enabled: false
    },
    series: ['.implode(",",$hValues).']
});

', CClientScript::POS_END);
?>

<div class="box box-primary">
	<div class="box-body">
		<div class="search-form">
		<?php
		$form = $this->beginWidget(
			'booster.widgets.TbActiveForm',
			array(
				'id' => 'horizontalForm',
				'type' => 'horizontal',
				'action'=>Yii::app()->createUrl($this->route),
			)
		); ?>
		
		<div class="row">
			<div class="col-xs-6">
				<?php echo $form->dateRangeGroup(
					$model,
					'waktu_registrasi',
					array(
						'label'=>'Periode Registrasi',
						'wrapperHtmlOptions' => array(
							'class' => '',
						),
						'prepend' => '<i class="glyphicon glyphicon-calendar"></i>'
					)
				); ?>
			</div>
			<div class="col-xs-6">
				<?php $this->widget(
					'booster.widgets.TbButton',
					array(
						'buttonType' => 'submit',
						'context' => 'primary',
						'label' => 'Tampilkan Data'
					)
				); ?>
				<?php $this->widget(
					'booster.widgets.TbButton',
					array('buttonType' => 'reset', 'label' => 'Reset')
				); ?>
			</div>
		</div>
		<?php
		$this->endWidget();
		?>
		</div>
		<div class="row">
			<div class="col-md-12">
				<div id="container" style="width: 100%; height: 400px; margin: 0 auto"></div>
			</div>
		</div>
	</div>
</div>