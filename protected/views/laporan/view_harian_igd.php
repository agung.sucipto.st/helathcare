<?php
$this->title['title']="Laporan Harian Poli / IGD";
$this->title['deskripsi']="";
$this->breadcrumbs=array(
	'Laporan Harian Poli / IGD',
);
?>

<div class="box box-primary">
	<div class="box-body">
	
		<?php

		Yii::app()->clientScript->registerScript('search', "
		$('.search-form form').submit(function(){
			$.fn.yiiGridView.update('registrasi-grid', {
				data: $(this).serialize()
			});
			var tgl = $('#Registrasi_waktu_registrasi').val();
			$('#tgl').html(tgl);
			return false;
		});

		");
		?>
		<div class="search-form">
		<?php
		$form = $this->beginWidget(
			'booster.widgets.TbActiveForm',
			array(
				'id' => 'horizontalForm',
				'type' => 'horizontal',
				'action'=>Yii::app()->createUrl($this->route),
				'method'=>'get',
			)
		); ?>
		
		<div class="row">
			<div class="col-xs-6">
				<?php echo $form->dateRangeGroup(
					$model,
					'waktu_registrasi',
					array(
						'label'=>'Periode Registrasi',
						'wrapperHtmlOptions' => array(
							'class' => '',
						),
						'prepend' => '<i class="glyphicon glyphicon-calendar"></i>'
					)
				); ?>
			</div>
			<div class="col-xs-3">
				<?php echo $form->dropDownListGroup($model,'id_departemen', array('label'=>'Layanan','widgetOptions'=>array('data'=>CHtml::listData(Departemen::model()->findAll(array("condition"=>"id_jenis_departemen='2' OR id_jenis_departemen='1'")),'id_departemen','nama_departemen'), 'htmlOptions'=>array('class'=>'input-large selectpicker show-tick','data-live-search'=>"true",'empty'=>"Pilih Layanan")))); ?>
			</div>
			<div class="col-xs-3">
				<?php $this->widget(
					'booster.widgets.TbButton',
					array(
						'buttonType' => 'submit',
						'context' => 'primary',
						'label' => 'Tampilkan Data'
					)
				); ?>
				<?php $this->widget(
					'booster.widgets.TbButton',
					array('buttonType' => 'reset', 'label' => 'Reset')
				); ?>
			</div>
		</div>
		<?php
		$this->endWidget();
		?>
		</div>
		<hr>
		<?php
			$this->renderPartial('_data_laporan_harian_poli_igd',array(
				'model'=>$model
			));
		?>
	</div>
</div>