<style>
.table thead tr th{
	font-size:12px;
	vertical-align:middle;
	text-align:center;
	background:#3c8dbc;
	color:white;
}
.table tbody tr td{
	font-size:13px;
	vertical-align:top;
	padding:0px 5px;
	white-space:nowrap;
}
</style>
<center>
	<h4>Rekapitulasi Kunjungan<br/><span id="tgl"><?=Lib::dateIndShortMonth(date("Y-m-d")).' - '.Lib::dateIndShortMonth(date("Y-m-d"));?></span></h4>
</center>
<div class="table-responsive" style="width:100%;overflow:auto;">
<?php 
// put this somewhere on top
$pageSize=Yii::app()->user->getState('pageSize',Yii::app()->params['defaultPageSize']); ?>		
<?php		
$this->widget('booster.widgets.TbExtendedGridView', array(
	'id'=>'registrasi-grid',
	'type' => 'striped bordered hover',
	'dataProvider' => $model->searchLaporan(),
	'summaryText'=>'Menampilkan {start}-{end} dari {count} hasil.',
	'selectableRows' => 2,
	'responsiveTable' => true,
	'enablePagination' => true,
	'pager' => array(
		'htmlOptions'=>array(
			'class'=>'pagination'
		),
		'maxButtonCount' => 5,
		'cssFile' => true,
		'header' => false,
		'firstPageLabel' => '<<',
		'prevPageLabel' => '<',
		'nextPageLabel' => '>',
		'lastPageLabel' => '>>',
	),
	'columns'=>array(
			array(
				'header'=>'NO',
				'value' => '$this->grid->dataProvider->pagination->currentPage * $this->grid->dataProvider->pagination->pageSize + ($row + 1)',
			),
			array(
				"header"=>"Waktu Registrasi",
				"name"=>"waktu_registrasi",
				'sortable'=>false,
				"value"=>'Lib::dateIndShortMonth($data->waktu_registrasi,false)'
			),
			array(
				"name"=>"wkt_reg",
				'headerHtmlOptions' => array('style'=>'display:none'),
				'htmlOptions' =>array('style'=>'display:none'),
				"value"=>'Lib::dateIndShortMonth($data->waktu_registrasi,false,false)'
			),
			array(
				"name"=>"lk",
				'headerHtmlOptions' => array('style'=>'display:none'),
				'htmlOptions' =>array('style'=>'display:none'),
				"value"=>function($data){
					if($data->idPasien->idPersonal->jenis_kelamin=='Laki-laki'){
						return 1;
					}
				}
			),
			array(
				"name"=>"pr",
				'headerHtmlOptions' => array('style'=>'display:none'),
				'htmlOptions' =>array('style'=>'display:none'),
				"value"=>function($data){
					if($data->idPasien->idPersonal->jenis_kelamin=='Perempuan'){
						return 1;
					}
				}
			),
			array(
				"header"=>"No Registrasi",
				"name"=>"no_registrasi",
				'sortable'=>false,
				"value"=>'$data->no_registrasi'
			),
			array(
				"header"=>"No RM",
				"name"=>"id_pasien",
				'sortable'=>false,
				"value"=>'Lib::MRN($data->id_pasien)'
			),
			array(
				"header"=>'Nama Pasien',
				"type"=>'Raw',
				'sortable'=>false,
				"name"=>"nama_lengkap",
				"value"=>function($data){
					return $data->idPasien->idPersonal->nama_lengkap.' '.$data->idPasien->panggilan;					
				}
			),
			array(
				"header"=>'Jenis Kelamin',
				"name"=>"jenis_kelamin",
				"type"=>'Raw',
				'sortable'=>false,
				"value"=>'$data->idPasien->idPersonal->jenis_kelamin'
			),
			array(
				"header"=>"Layanan",
				"name"=>"id_departement",
				'sortable'=>false,
				"value"=>'$data->idDepartemen->nama_departemen'
			),
			array(
				"header"=>"Jaminnan",
				"name"=>"id_penjamin",
				'sortable'=>false,
				"value"=>function($data){
					if($data->jenis_jaminan=="UMUM"){
						echo "Umum";
					}else{
						foreach($data->jaminanPasiens as $row){
							echo $row->idPenjamin->nama_penjamin.'<br/>';
						}
					}
				}
			),
			array(
				"header"=>"Diagnosa",
				"value"=>function($data){
					foreach($data->icd10Pasiens as $row){
						echo $row->idIcd10->kode_icd.' '.$row->idIcd10->diagnosa.'<br/>';
					}
				}
			),
			array(
				"header"=>"Umur (Tahun)",
				"value"=>'Lib::Umur($data->idPasien->idPersonal->tanggal_lahir)'
			),
			array(
				"header"=>"Agama",
				"value"=>'$data->idPasien->idPersonal->idAgama->nama'
			),
			array(
				"header"=>"Provinsi",
				"value"=>'$data->idPasien->idPersonal->idProvinsi->nama'
			),
			array(
				"header"=>"Kabupaten",
				"value"=>'$data->idPasien->idPersonal->idKabupaten->nama'
			),
			array(
				"header"=>"Kecamatan",
				"value"=>'$data->idPasien->idPersonal->idKecamatan->nama'
			),
			array(
				"header"=>"Pendidikan",
				"value"=>'$data->idPasien->idPersonal->idPendidikan->nama'
			),
			array(
				"header"=>"Jenis Kunjungan",
				"value"=>function($data){
					if($data->jenis_kunjungan==1){
						return "Baru";
					}else{
						return "Lama";
					}
				}
			),
			array(
				"header"=>"Keterangan Masuk",
				"value"=>'$data->rujukan'
			),
			array(
				"header"=>"Keterangan Pulang",
				"value"=>'($data->status_keluar=="")?"Belum Pulang":$data->status_keluar'
			),
	),
));
?>

</div>