<style>
.table thead tr th{
	vertical-align:middle;
	text-align:center;
	background:#3c8dbc;
	color:white;
	padding:5px;
}
.table tbody tr td{
	vertical-align:top;
	white-space:nowrap;
}
</style>
<center>
	<h4>Laporan Penyakit Terbesar Rawat Jalan<br/><small><?=$title;?></small></h4>
</center>
<div class="table-responsive" style="width:100%;overflow:auto;">
	<table class="table table-striped table-bordered table-hover">
		<thead>
			<tr>
				<th rowspan="2">No</th>
				<th rowspan="2">Kode ICD</th>
				<th rowspan="2">Diagnosa</th>
				<th colspan="2">Jenis Kelamin</th>
				<th colspan="2">Jenis Kunjungan</th>
				<th rowspan="2">Total Kasus</th>
			</tr>
			<tr>
				<th>Laki-laki</th>
				<th>Perempuan</th>
				<th>Baru</th>
				<th>Lama</th>
			</tr>
		</thead>
		<tbody>
			<?php
			$no=0;
			foreach($data as $row){
				$no++;
				echo'
				<tr>
					<td>'.$no.'</td>
					<td>'.$row['kode_icd'].'</td>
					<td>'.$row['diagnosa'].'</td>
					<td style="text-align:center;">'.$row['laki'].'</td>
					<td style="text-align:center;">'.$row['perempuan'].'</td>
					<td style="text-align:center;">'.$row['baru'].'</td>
					<td style="text-align:center;">'.$row['lama'].'</td>
					<td style="text-align:center;">'.$row['total_diagnosa'].'</td>
				</tr>
				';
			}
			?>
		</tbody>
	</table>
</div>