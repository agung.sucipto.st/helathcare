<?php $form=$this->beginWidget('booster.widgets.TbActiveForm',array(
	'action'=>Yii::app()->createUrl($this->route),
	'method'=>'get',
	'type'=>'horizontal',
)); ?>

<div class="row">
	<div class="col-sm-6">
		<?php echo $form->dateRangeGroup(
			$model,
			'item_transaction_time',
			array(
				'label'=>'Periode Penerimaan',
				'wrapperHtmlOptions' => array(
					'class' => '',
				),
				'prepend' => '<i class="glyphicon glyphicon-calendar"></i>'
			)
		); ?>
	</div>
	<div class="col-sm-6">
		<?php echo $form->textFieldGroup($model,'item_transaction_code',array('widgetOptions'=>array('htmlOptions'=>array('class'=>'span5','maxlength'=>45)))); ?>
	</div>
</div>
<div class="row">

	<div class="col-sm-6">
		<?php echo $form->dropDownListGroup($model,'warehouse_origin_id', array('widgetOptions'=>array('data'=>CHtml::listData(Warehouse::model()->findAll(array("condition"=>"	is_allow_receiveing='1'")),'warehouse_id','warehouse_name'), 'htmlOptions'=>array('class'=>'input-large','empty'=>'- Choose -')))); ?>
	</div>
	
	<div class="col-sm-6">
		<?php echo $form->textFieldGroup($model,'item_name',array('widgetOptions'=>array('htmlOptions'=>array('class'=>'span5','maxlength'=>45)))); ?>
	</div>
</div>

		
	
		
	<div class="form-actions">
		<?php $this->widget('booster.widgets.TbButton', array(
			'buttonType' => 'submit',
			'context'=>'primary',
			'label'=>'Search',
		)); ?>
	</div>

<?php $this->endWidget(); ?>
