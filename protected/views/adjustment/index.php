<?php
$this->breadcrumbs=array(
	'Penyesuaian Stok'=>array('listAdjust'),
	'Penyesuaian Stok',
);
$this->title=array(
	'title'=>'Penyesuaian Stok',
	'deskripsi'=>'Untuk Memonitor Stok'
);
?>

<div class="panel panel-default">
	<div class="panel-heading">
		<h3 class="panel-title pull-left">Penyesuaian Stok</h3>
		<div style="clear:both"></div>
	</div>
	<div class="panel-body">
		<?php $form=$this->beginWidget('booster.widgets.TbActiveForm',array(
			'action'=>Yii::app()->createUrl($this->route),
			'method'=>'POST',
			'type'=>'vertical',
		)); ?>

		<div class="row">
			<div class="col-sm-3">
				<?php echo $form->dropDownListGroup($model,'warehouse_destination_id', array('widgetOptions'=>array('data'=>CHtml::listData(Warehouse::model()->findAll(array("condition"=>"	is_allow_receiveing='1'")),'warehouse_id','warehouse_name'), 'htmlOptions'=>array('class'=>'input-large','empty'=>'- Choose -',"required"=>"required")))); ?>
			</div>
			<div class="col-sm-3">
				<label>Cari Nama Item / Kode Item</label>
					<?php
					$this->widget('zii.widgets.jui.CJuiAutoComplete',array(
						'name'=>'item',
						'options'=>array(
							'minLength'=>2,
							'showAnim'=>'fold',
							'select'=>"js:function(event, data) {
							}"
						),
						'source'=>$this->createUrl("item/getItem"),
						'htmlOptions'=>array(
							'class'=>"form-control",
							"id"=>"searchItem",
							"required"=>"required",
						),
					));
					?>
			</div>
			<div class="col-sm-2">
				<br/>
				<?php $this->widget('booster.widgets.TbButton', array(
					'buttonType' => 'submit',
					'context'=>'primary',
					'label'=>'Search',
				)); ?>
			</div>
		</div>

		<?php $this->endWidget(); ?>
	
		<?php
			if(!empty($data)){
				$form=$this->beginWidget('booster.widgets.TbActiveForm',array(
					'action'=>Yii::app()->createUrl($this->route),
					'method'=>'POST',
					'type'=>'vertical',
				)); 
		?>
			<?php echo $form->textFieldGroup($data,'item_name',array('widgetOptions'=>array('htmlOptions'=>array('class'=>'span5','maxlength'=>100,'disabled'=>true)))); ?>
			<?php echo $form->numberFieldGroup($data,'stock',array('widgetOptions'=>array('htmlOptions'=>array('class'=>'span5','maxlength'=>100)))); ?>
			<?php echo $form->numberFieldGroup($data,'stok_perbaikan',array('widgetOptions'=>array('htmlOptions'=>array('class'=>'span5','maxlength'=>100)))); ?>
			<?php echo $form->datePickerGroup($data,'expired',array('widgetOptions'=>array('options'=>array('format'=>'yyyy-mm-dd','viewFormat'=>'yyyy-mm-dd'),'htmlOptions'=>array('class'=>'span5')), 'prepend'=>'<i class="glyphicon glyphicon-calendar"></i>', 'append'=>'Klik Bulan/Tahun untuk merubah Bulan/Tahun.')); ?>
			<input type="hidden" name="warehouse" value="<?=$model->warehouse_destination_id;?>"/>
			<input type="hidden" name="item" value="<?=$data->item_id;?>"/>
			<?php $this->widget('booster.widgets.TbButton', array(
					'buttonType' => 'submit',
					'context'=>'primary',
					'label'=>'Perbarui Stok',
				)); ?>
		<?php
				$this->endWidget();
			}
		?>
			
	</div>
</div>