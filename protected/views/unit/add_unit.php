<script>
<?php
if($show==false){
	
	echo '
	window.opener.change();
	window.self.close();';
}
?>
</script>

<div class="panel panel-default">
	<div class="panel-heading">
		<h3 class="panel-title pull-left">Tambah Satuan</h3>
		<div style="clear:both"></div>
	</div>
	<div class="panel-body">
<?php $form=$this->beginWidget('booster.widgets.TbActiveForm',array(
	'id'=>'item-unit-form',
	'enableAjaxValidation'=>false,
)); ?>

<p class="help-block">Isian dengan tanda <span class="required">*</span> wajib diisi.</p>

<?php echo $form->errorSummary($model); ?>

	<?php echo $form->dropDownListGroup($model,'unit_id', array('widgetOptions'=>array('data'=>CHtml::listData(Unit::model()->findAll(),'unit_id','unit_alias'), 'htmlOptions'=>array('class'=>'input-large','empty'=>'- Choose -')))); ?>

	<?php echo $form->textFieldGroup($model,'unit_amount',array('widgetOptions'=>array('htmlOptions'=>array('class'=>'span5')),'append'=>$parent->unit->unit_alias)); ?>
	<?php echo $form->dropDownListGroup($model,'is_active', array('widgetOptions'=>array('data'=>array("1"=>"Aktif","0"=>"Non Aktif"), 'htmlOptions'=>array('class'=>'input-large','empty'=>'- Choose -')))); ?>

<div class="form-actions">
	<?php $this->widget('booster.widgets.TbButton', array(
			'buttonType'=>'submit',
			'context'=>'primary',
			'label'=>$model->isNewRecord ? 'Tambah' : 'Simpan',
		)); 
		echo CHtml::button('Batal', array(
            'class' => 'btn btn-primary',
            'onclick' => "history.go(-1)",
                )
        );
		?>

		</div>

<?php $this->endWidget(); ?>
</div>