<link rel="stylesheet" href="<?php echo Yii::app()->theme->baseUrl; ?>/assets/css/bootstrap-select.css">
<script src="<?php echo Yii::app()->theme->baseUrl; ?>/assets/js/bootstrap-select.js"></script>

<?php $form=$this->beginWidget('booster.widgets.TbActiveForm',array(
	'action'=>Yii::app()->createUrl($this->route),
	'method'=>'get',
)); ?>

	<div class="row">
		<div class="col-sm-4">
			<?php echo $form->textFieldGroup($model,'id_pasien',array('widgetOptions'=>array('htmlOptions'=>array('class'=>'span5','maxlength'=>20)))); ?>
		</div>
		<div class="col-sm-4">
			<?php echo $form->textFieldGroup($model,'no_registrasi',array('widgetOptions'=>array('htmlOptions'=>array('class'=>'span5','maxlength'=>20)))); ?>
		</div>
		<div class="col-sm-4">
			<?php echo $form->textFieldGroup($model,'nama_lengkap',array('widgetOptions'=>array('htmlOptions'=>array('class'=>'span5','maxlength'=>20)))); ?>
		</div>
	</div>
		
	<div class="row">
		<div class="col-sm-3">
			<?php echo $form->datePickerGroup($model,'waktu_registrasi',array('widgetOptions'=>array('options'=>array('format'=>'yyyy-mm-dd','viewFormat'=>'yyyy-mm-dd'),'htmlOptions'=>array('class'=>'span5')))); ?>
		</div>
		<div class="col-sm-3">
			<?php echo $form->dropDownListGroup($model,'status_registrasi', array('widgetOptions'=>array('data'=>array("Aktif"=>"Aktif","Tutup Kunjungan"=>"Tutup Kunjungan","Batal"=>"Batal"), 'htmlOptions'=>array('class'=>'input-large',"empty"=>"Semua Status Kunjungan")))); ?>
		</div>
		<div class="col-sm-3">
			<?php echo $form->dropDownListGroup($model,'id_departemen', array('label'=>'Layanan','widgetOptions'=>array('data'=>CHtml::listData(Departemen::model()->findAll(array("condition"=>"id_jenis_departemen='2'")),'id_departemen','nama_departemen'), 'htmlOptions'=>array('class'=>'input-large selectpicker show-tick','data-live-search'=>"true",'empty'=>"Pilih Layanan")))); ?>
		</div>
		<div class="col-sm-3">
			<?php echo $form->dropDownListGroup($model,'id_dokter', array('widgetOptions'=>array('data'=>array(), 'htmlOptions'=>array('class'=>'input-large selectpicker show-tick','data-live-search'=>"true",'empty'=>"Pilih Dokter")))); ?>
		</div>
	</div>
		
	<div class="form-actions">
		<?php $this->widget('booster.widgets.TbButton', array(
			'buttonType' => 'submit',
			'context'=>'primary',
			'label'=>'Cari',
		)); ?>
	</div>

<?php $this->endWidget(); ?>

<?php
Yii::app()->clientScript->registerScript('validate', '
$("#Registrasi_id_departemen").change(function(){
	var id = $("#Registrasi_id_departemen").val();
	if(id != ""){
		getVal(id,"id_departemen","#Registrasi_id_dokter","registrasi/getDokter");
	}
});

function getVal(value,param,id,url){	
	$.ajax({
		url: "'.Yii::app()->createAbsoluteUrl('/').'/"+url,
		cache: false,
		type: "POST",
		data:"Registrasi["+param+"]="+value,
		success: function(msg){
			$(id).html("");
			$(id).html(msg);
			$(id).selectpicker("refresh");
		}
	});
}
', CClientScript::POS_END);
?>