<script language="javascript">
<?php
	if($status=="success"){
?>
	window.opener.reloadData(<?=$model->id_supplier;?>);
	window.self.close();
<?php
	}
?>
</script>


<div class="panel panel-default">
	<div class="panel-heading">
		<h3 class="panel-title pull-left">Form Tambah Supplier</h3>
		<div style="clear:both"></div>
	</div>
	<div class="panel-body">	
		

<?php $form=$this->beginWidget('booster.widgets.TbActiveForm',array(
	'id'=>'supplier-form',
	'enableAjaxValidation'=>true,
)); ?>

<p class="help-block">Isian dengan tanda <span class="required">*</span> wajib diisi.</p>

<?php echo $form->errorSummary($model); ?>

	<?php echo $form->textFieldGroup($model,'nama_supplier',array('widgetOptions'=>array('htmlOptions'=>array('class'=>'span5','maxlength'=>100)))); ?>

	<?php echo $form->textAreaGroup($model,'alamat', array('widgetOptions'=>array('htmlOptions'=>array('rows'=>6, 'cols'=>50, 'class'=>'span8')))); ?>

	<?php echo $form->textFieldGroup($model,'no_telpon',array('widgetOptions'=>array('htmlOptions'=>array('class'=>'span5','maxlength'=>45)))); ?>

	<?php echo $form->dropDownListGroup($model,'id_coa_hutang', array('widgetOptions'=>array('data'=>Chtml::listData(GlCoa::model()->findAll(array("condition"=>"id_coa_tipe='2'")),'id_coa', 'nama_coa'), 'htmlOptions'=>array('class'=>'input-large')))); ?>
	
	<?php echo $form->dropDownListGroup($model,'jenis_supplier', array('widgetOptions'=>array('data'=>array("Logistik"=>"Logistik","Farmasi"=>"Farmasi",), 'htmlOptions'=>array('class'=>'input-large')))); ?>

<div class="form-actions">
	<?php $this->widget('booster.widgets.TbButton', array(
			'buttonType'=>'submit',
			'context'=>'primary',
			'label'=>$model->isNewRecord ? 'Tambah' : 'Simpan',
		)); 
		echo CHtml::button('Batal', array(
            'class' => 'btn btn-primary',
            'onclick' => "history.go(-1)",
                )
        );
		?>

		</div>

<?php $this->endWidget(); ?>
	</div>
</div>
