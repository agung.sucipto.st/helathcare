<script>
<?php
if($show==false){
	
	echo '
	window.opener.change();
	window.self.close();';
}
?>
</script>

<div class="panel panel-default">
	<div class="panel-heading">
		<h3 class="panel-title pull-left">Setup Barcode</h3>
		<div style="clear:both"></div>
	</div>
	<div class="panel-body">
<?php $form=$this->beginWidget('booster.widgets.TbActiveForm',array(
	'id'=>'item-unit-form',
	'enableAjaxValidation'=>false,
)); ?>

<?php echo $form->errorSummary($model); ?>
	<?php echo $form->textFieldGroup($model,'barcode',array('widgetOptions'=>array('htmlOptions'=>array('class'=>'span5')),'append'=>'<i class="fa fa-barcode"></i>')); ?>
<div class="form-actions">
	<?php $this->widget('booster.widgets.TbButton', array(
			'buttonType'=>'submit',
			'context'=>'primary',
			'label'=>$model->isNewRecord ? 'Tambah' : 'Simpan',
		)); 
		echo CHtml::button('Batal', array(
            'class' => 'btn btn-primary',
            'onclick' => "window.self.close()",
                )
        );
		?>

		</div>

<?php $this->endWidget(); ?>
</div>