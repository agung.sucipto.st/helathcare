<script>
<?php
if($show==false){
	
	echo '
	window.opener.change();
	window.self.close();';
}
?>
</script>

<div class="panel panel-default">
	<div class="panel-heading">
		<h3 class="panel-title pull-left">Setup Min Max</h3>
		<div style="clear:both"></div>
	</div>
	<div class="panel-body">
<?php $form=$this->beginWidget('booster.widgets.TbActiveForm',array(
	'id'=>'item-unit-form',
	'enableAjaxValidation'=>false,
)); ?>

<?php echo $form->errorSummary($model); ?>
	<?php echo $form->numberFieldGroup($model,'min',array('widgetOptions'=>array('htmlOptions'=>array('class'=>'span5','min'=>"0")),'append'=>Item::getParentUnit($model->item_id)->unit->unit_name)); ?>
	<?php echo $form->numberFieldGroup($model,'max',array('widgetOptions'=>array('htmlOptions'=>array('class'=>'span5','min'=>"0")),'append'=>Item::getParentUnit($model->item_id)->unit->unit_name)); ?>
<div class="form-actions">
	<?php $this->widget('booster.widgets.TbButton', array(
			'buttonType'=>'submit',
			'context'=>'primary',
			'label'=>$model->isNewRecord ? 'Tambah' : 'Simpan',
		)); 
		echo CHtml::button('Batal', array(
            'class' => 'btn btn-primary',
            'onclick' => "window.self.close()",
                )
        );
		?>

		</div>

<?php $this->endWidget(); ?>
</div>