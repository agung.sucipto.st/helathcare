<?php
$this->breadcrumbs=array(
	'Kelola Harga Jual'=>array('setup/harga'),
	'Harga Jual',
);
$this->title=array(
	'title'=>'Tambah Harga Jual',
	'deskripsi'=>'Untuk Memperbarui Harga Jual'
);?>

<div class="panel panel-default">
	<div class="panel-heading">
		<h3 class="panel-title pull-left">Form Update</h3>
		<a href="<?php echo Yii::app()->createUrl(Yii::app()->controller->id.'/harga');?>" class="btn btn-primary btn-xs pull-right">
			<i class="fa fa-arrow-circle-left"></i>
			<span>Kembali</span>
		</a>
		<div style="clear:both"></div>
	</div>
	<div class="panel-body">
		<div class="row">
			<div class="col-sm-4">
				<?php $this->widget('booster.widgets.TbDetailView',array(
					'data'=>$item,
					'attributes'=>array(
						'item_short_name',
						'code',
						array(
							"label"=>"Category",
							"value"=>$item->itemCat->item_cat_name
						),
						array(
							"label"=>"Category",
							"value"=>$item->itemCat->item_cat_name
						),
						array(
							"label"=>"Item Group",
							"value"=>$item->itemGroup->item_group_name
						),
						
					),
					)); ?>
			</div>
			<div class="col-sm-4">
				<?php $this->widget('booster.widgets.TbDetailView',array(
					'data'=>$item,
					'attributes'=>array(
						array(
							"label"=>"Principle",
							"value"=>$item->principle->principle_name
						),
						'item_net_price',
						array(
							"label"=>"Status",
							"value"=>($item->is_active=="1")?"Aktif":"Non Aktif"
						),
						array(
							"label"=>"Jasa",
							"value"=>($item->is_jasa=="1")?"Ya":"Tidak"
						),
						'wacc',
						
					),
					)); ?>
			</div>
			<div class="col-sm-4">
				<?php $this->widget('booster.widgets.TbDetailView',array(
					'data'=>$item,
					'attributes'=>array(
						'predicted_price',
						array(
							"label"=>"Status",
							"value"=>($item->is_active=="1")?"Aktif":"Non Aktif"
						),
						array(
							"label"=>"Created",
							"value"=>$item->userCreated->username.' / '.Lib::dateInd($item->created_at)
						),
						array(
							"label"=>"Modified",
							"value"=>$item->userModified->username.' / '.Lib::dateInd($item->modified_at)
						),
					),
					)); ?>
			</div>
			
		</div>
		
		<?php 
		echo $this->renderPartial('_form', array('mem'=>$mem,'reg'=>$reg)); 
		?>		</div>
</div>