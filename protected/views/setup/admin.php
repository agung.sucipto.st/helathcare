<?php
$this->breadcrumbs=array(
	'Kelola Harga Jual',
);
$this->title=array(
	'title'=>'Kelola Harga Jual',
	'deskripsi'=>'Untuk Mengelola Harga Jual'
);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	$('.import-form').hide();
	return false;
});
$('.btn-cancel').click(function(){
	$('.import-form').toggle();
	return false;
});
$('.import-button').click(function(){
	$('.import-form').toggle();
	$('.search-form').hide();
	return false;
});
$('.search-form form').submit(function(){
	$.fn.yiiGridView.update('item-unit-grid', {
		data: $(this).serialize()
	});
	return false;
});

");
?>

<div class="panel panel-default">
	<div class="panel-heading">
		<h3 class="panel-title pull-left">Data</h3>
		<a href="#" class="btn btn-primary btn-xs pull-right import-button">
			<i class="fa fa-upload"></i>
			<span>Import</span>
		</a>
		<div style="clear:both"></div>
	</div>
	<div class="panel-body">
		<style>
		.pagination li.selected a{
			background:rgb(235, 235, 236);
		}
		</style>
		
		
		<?php
		if(Yii::app()->user->hasFlash('success')){
			echo'
			<div class="alert alert-success">
				<button type="button" class="close" data-dismiss="alert">&times;</button>
				'.Yii::app()->user->getFlash('success').'
			</div>';
		}
		?>

		<div class="import-form" style="display:none">
			<div class="xe-widget xe-counter-block" data-count=".num" data-from="0" data-to="99.9" data-suffix="%" data-duration="2">
			<div class="row">
				<div class="col-sm-7">
					<div class="xe-lower">
							
							<?php $form=$this->beginWidget('booster.widgets.TbActiveForm',array(
								'action'=>Yii::app()->createUrl(Yii::app()->controller->id.'/import'),
								'method'=>'POST',
								'htmlOptions'=>array('enctype'=>'multipart/form-data'),
							)); ?>
							<?php echo $form->fileFieldGroup($model, 'import',
									array(
										'wrapperHtmlOptions' => array(
											'class' => 'form-control',
										),
										'widgetOptions'=>array(
											'htmlOptions'=>array(
												'required'=>true,
												'accept'=>".xls"
											),
										),
										'label'=>''
										,
									)
								); ?>								<div class="text-center">
									<div class="col-sm-6">
										
									</div>
									<div class="col-sm-6">
									<button class="btn btn-success btn-icon btn-cancel">
											<i class="fa fa-times"></i>
											<span>Batal</span>
										</button>
										<button class="btn btn-success btn-icon">
											<i class="fa fa-file-o"></i>
											<span>Import</span>
										</button>
									</div>							
								</div>							
							
						<?php $this->endWidget(); ?>					
						</div>				
				</div>
				<div class="col-sm-5">
					<div class="xe-upper">
						<div class="xe-label">
							<small>Kolom ID ITEM UNIT dan ID ITEM Jangan Dirubah, silahkan ubah pada kolom harga !</small>
							<br/>
							<!-- Excel Button --> 
							<a href="<?=Yii::app()->createUrl("setup/downloadTemplateHarga");?>" class="a-btn-2">
								<span class="a-btn-2-text">Download Template</span> 
							</a>
						</div>
					</div>		
				</div>
			</div>
			</div>
		</div><!-- search-form -->
		<div id="example-2_wrapper" class="dataTables_wrapper form-inline dt-bootstrap no-footer">
		
		<?php 
		// put this somewhere on top
		$pageSize=Yii::app()->user->getState('pageSize',Yii::app()->params['defaultPageSize']); ?>		
		<?php		$this->widget('booster.widgets.TbExtendedGridView', array(
			'id'=>'item-unit-grid',
			'type' => 'striped',
			'dataProvider' => $model->search(),
			'filter'=>$model,
			'summaryText'=>'Menampilkan {start}-{end} dari {count} hasil.',
			'selectableRows' => 2,
			'responsiveTable' => true,
			'enablePagination' => true,
			'pager' => array(
				'htmlOptions'=>array(
					'class'=>'pagination'
				),
				'maxButtonCount' => 5,
				'cssFile' => true,
				'header' => false,
				'firstPageLabel' => '<<',
				'prevPageLabel' => '<',
				'nextPageLabel' => '>',
				'lastPageLabel' => '>>',
			),
			'columns'=>array(
					array(
						"header"=>"Item Code",
						"name"=>"code",
						"value"=>'$data->item->code',
					),
					array(
						"header"=>"Barcode",
						"type"=>"raw",
						"name"=>"barcode",
						"value"=>function($data){
							return'<button class="btn btn-sm btn-warning" data-toggle="tooltip" data-original-title="Klik Untuk Ubah Barcode" onclick="barcode(\''.$data->item_unit_id.'\');"><i class="fa fa-barcode"></i></button> '.$data->barcode;
						},
					),
					array(
						"header"=>"Item Name",
						"name"=>"item_name",
						"value"=>'$data->item->item_name',
					),
					array(
						"header"=>"Unit",
						"value"=>'$data->unit->unit_name',
					),
					'unit_amount',
					array(
						"header"=>"Unit",
						"value"=>'$data->parentItemUnit->unit->unit_name',
					),
					array(
						"header"=>"Harga Regular",
						"value"=>function($data){
							$regular=ItemPrice::model()->find(array("condition"=>"item_id='$data->item_id' AND item_unit_id='$data->item_unit_id' AND price_type='REGULAR'"));
							if(empty($regular)){
								return 'NOT SET';
							}else{
								return number_format($regular->sale,0);
							}
						},
					),
					array(
						"header"=>"Harga Member",
						"value"=>function($data){
							$regular=ItemPrice::model()->find(array("condition"=>"item_id='$data->item_id' AND item_unit_id='$data->item_unit_id' AND price_type='MEMBER'"));
							if(empty($regular)){
								return 'NOT SET';
							}else{
								return number_format($regular->sale,0);
							}
						},
					),
					array(
						'class'=>'booster.widgets.TbButtonColumn',
						 'deleteConfirmation'=>'Anda yakin akan menhapus data?',
						'template'=>'{update}',
						'buttons'=>array
						(
							'update' => array
							(
								'label'=>'Update',
								'icon'=>'pencil',
								'visible'=>'Lib::accessBy("setup","updateHarga")',
								'url'=>'Yii::app()->createUrl("setup/updateHarga",array("item"=>$data->item_id,"unit"=>$data->item_unit_id))',
								'options'=>array(
									'class'=>'btn btn-default btn-xs',
								),
							),
						),
						'header'=>CHtml::dropDownList('pageSize',$pageSize,array(10=>10,20=>20,50=>50,100=>100,200=>200,500=>500,1000=>1000),array(
							'onchange'=>"$.fn.yiiGridView.update('item-unit-grid',{ data:{pageSize: $(this).val() }})",
						)),
					),
			),
		));
		?>
		</div>
	</div>
</div>


<?php
Yii::app()->clientScript->registerScript('validatex', '
function change(){
	$.fn.yiiGridView.update("item-unit-grid", {
		data: $(this).serialize()
	});
}

function barcode(id){
	var left = (screen.width/2)-(350/2);
	var top = (screen.height/2)-(300/2);
	window.open("'.Yii::app()->createUrl('setup/barcode').'/id/"+id,"","width=350,height=300,top="+top+",left="+left);
}
', CClientScript::POS_END);
?>