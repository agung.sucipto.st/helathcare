<?php $form=$this->beginWidget('booster.widgets.TbActiveForm',array(
	'id'=>'item-price-form',
	'enableAjaxValidation'=>true,
)); ?>

<p class="help-block">Isian dengan tanda <span class="required">*</span> wajib diisi.</p>

<?php echo $form->errorSummary($reg); ?>
<div class="container">
<div class="row">
<div class="col-sm-6">
	HARGA REGULAR<br/>
	<hr>
	<?php echo $form->textFieldGroup($reg,'item_price',array('widgetOptions'=>array('htmlOptions'=>array('class'=>'span5','maxlength'=>50)))); ?>
	<?php echo $form->textFieldGroup($reg,'item_tax',array('widgetOptions'=>array('htmlOptions'=>array('class'=>'span5','maxlength'=>50)))); ?>
	<?php echo $form->textFieldGroup($reg,'sale',array('widgetOptions'=>array('htmlOptions'=>array('class'=>'span5','maxlength'=>50)))); ?>
</div>
<div class="col-sm-6">
	HARGA MEMBER<br/>
	<hr>
	<?php echo $form->textFieldGroup($reg,'item_price2',array('widgetOptions'=>array('htmlOptions'=>array('class'=>'span5','maxlength'=>50)))); ?>
	<?php echo $form->textFieldGroup($reg,'item_tax2',array('widgetOptions'=>array('htmlOptions'=>array('class'=>'span5','maxlength'=>50)))); ?>
	<?php echo $form->textFieldGroup($reg,'sale2',array('widgetOptions'=>array('htmlOptions'=>array('class'=>'span5','maxlength'=>50)))); ?>
</div>
</div>
<div class="form-actions">
	<?php $this->widget('booster.widgets.TbButton', array(
			'buttonType'=>'submit',
			'context'=>'primary',
			'label'=>$reg->isNewRecord ? 'Tambah' : 'Simpan',
		)); 
		echo CHtml::button('Batal', array(
            'class' => 'btn btn-primary',
            'onclick' => "history.go(-1)",
                )
        );
		?>

		</div>

<?php $this->endWidget(); ?>
