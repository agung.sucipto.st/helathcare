<?php
$this->breadcrumbs=array(
	'Kelola Stok Awal Item',
);
$this->title=array(
	'title'=>'Kelola Stok Awal Item',
	'deskripsi'=>'Untuk Mengelola Stok Awal Item'
);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	$('.import-form').hide();
	return false;
});
$('.btn-cancel').click(function(){
	$('.import-form').toggle();
	return false;
});
$('.import-button').click(function(){
	$('.import-form').toggle();
	$('.search-form').hide();
	return false;
});
$('.search-form form').submit(function(){
	$.fn.yiiGridView.update('warehouse-item-grid', {
		data: $(this).serialize()
	});
	return false;
});

");
?>

<div class="panel panel-default">
	<div class="panel-heading">
		<h3 class="panel-title pull-left">Data</h3>
		<?php
		if(Lib::accessBy("setup","uploadStokAwal")==true){
		?>
		<a href="#" class="btn btn-primary btn-xs pull-right import-button">
			<i class="fa fa-upload"></i>
			<span>Import</span>
		</a>
		<?php
		}
		?>
		<div style="clear:both"></div>
	</div>
	<div class="panel-body">		
		<style>
		.pagination li.selected a{
			background:rgb(235, 235, 236);
		}
		</style>
		
		
		<?php
		if(Yii::app()->user->hasFlash('success')){
			echo'
			<div class="alert alert-success">
				<button type="button" class="close" data-dismiss="alert">&times;</button>
				'.Yii::app()->user->getFlash('success').'
			</div>';
		}
		?>
		
<div class="import-form" style="display:none">
			<div class="xe-widget xe-counter-block" data-count=".num" data-from="0" data-to="99.9" data-suffix="%" data-duration="2">
			<div class="row">
				<div class="col-sm-7">
					<b>Import Template</b><br/>
					<div class="xe-lower">
							
							<?php $form=$this->beginWidget('booster.widgets.TbActiveForm',array(
								'action'=>Yii::app()->createUrl(Yii::app()->controller->id.'/uploadStokAwal'),
								'method'=>'POST',
								'htmlOptions'=>array('enctype'=>'multipart/form-data'),
							)); ?>
							<?php echo $form->fileFieldGroup($model, 'import',
									array(
										'wrapperHtmlOptions' => array(
											'class' => 'form-control',
										),
										'widgetOptions'=>array(
											'htmlOptions'=>array(
												'required'=>true,
												'accept'=>".xls"
											),
										),
										'label'=>''
										,
									)
								); ?>								<div class="text-center">
									<div class="col-sm-6">
										
									</div>
									<div class="col-sm-6">
									<button class="btn btn-success btn-icon btn-cancel">
											<i class="fa fa-times"></i>
											<span>Batal</span>
										</button>
										<button class="btn btn-success btn-icon">
											<i class="fa fa-file-o"></i>
											<span>Import</span>
										</button>
									</div>							
								</div>							
							
						<?php $this->endWidget(); ?>					
						</div>				
				</div>
				<div class="col-sm-5">
							<?php $form=$this->beginWidget('booster.widgets.TbActiveForm',array(
								'action'=>Yii::app()->createUrl(Yii::app()->controller->id.'/downloadStokAwal'),
								'method'=>'POST',
							)); ?>
							<div class="row">
								<div class="col-sm-7">
									<?php echo $form->dropDownListGroup($model,'warehouse_destination_id', array('label'=>"Download Template",'widgetOptions'=>array('data'=>CHtml::listData(Warehouse::model()->findAll(),'warehouse_id','warehouse_name'), 'htmlOptions'=>array('class'=>'input-large','empty'=>'- Semua -')))); ?>
								</div>

								<div class="col-sm-5">
									<br/>
									<?php $this->widget('booster.widgets.TbButton', array(
										'buttonType' => 'submit',
										'context'=>'primary',
										'label'=>'Download Template',
									)); ?>
								</div>
							</div>
							<span class="text-danger">Jangan Pernah Mengubah ID Warehouse !</span>
							<?php $this->endWidget(); ?>
				</div>
			</div>
			</div>
		</div><!-- search-form -->
		
		<div id="example-2_wrapper" class="dataTables_wrapper form-inline dt-bootstrap no-footer">
		
		<?php 
		// put this somewhere on top
		$pageSize=Yii::app()->user->getState('pageSize',Yii::app()->params['defaultPageSize']); ?>		
		<?php		$this->widget('booster.widgets.TbExtendedGridView', array(
			'id'=>'warehouse-item-grid',
			'type' => 'striped',
			'dataProvider' => $model->search(),
			'filter'=>$model,
			'summaryText'=>'Menampilkan {start}-{end} dari {count} hasil.',
			'selectableRows' => 2,
			'responsiveTable' => true,
			'enablePagination' => true,
			'pager' => array(
				'htmlOptions'=>array(
					'class'=>'pagination'
				),
				'maxButtonCount' => 5,
				'cssFile' => true,
				'header' => false,
				'firstPageLabel' => '<<',
				'prevPageLabel' => '<',
				'nextPageLabel' => '>',
				'lastPageLabel' => '>>',
			),
			'columns'=>array(
					array(
						"header"=>"Item Code",
						"name"=>"code",
						"value"=>'$data->item->code',
					),
					array(
						"header"=>"Item Name",
						"name"=>"item_name",
						"value"=>'$data->item->item_name',
					),
					array(
						"header"=>"Unit",
						"value"=>'Item::getParentUnit($data->item_id)->unit->unit_name',
					),
					array(
						"header"=>"Gudang",
						"name"=>"warehouse_id",
						"value"=>'$data->warehouse->warehouse_name',
						"filter"=>CHtml::listData(Warehouse::model()->findAll(),"warehouse_id","warehouse_name")
					),
					'stock',
					array(
						"value"=>function($data){
							if(Lib::accessBy("setup","updateStokAwal")==true){
								return'<button class="btn btn-sm btn-success" data-toggle="tooltip" data-original-title="Klik Untuk Ubah Stok Awal" onclick="stok(\''.$data->warehouse_item_id.'\');"><i class="fa fa-pencil"></i></button> ';
							}							
						},
						'type'=>'raw',
						'header'=>CHtml::dropDownList('pageSize',$pageSize,array(10=>10,20=>20,50=>50,100=>100,200=>200,500=>500,1000=>1000),array(
							'onchange'=>"$.fn.yiiGridView.update('warehouse-item-grid',{ data:{pageSize: $(this).val() }})",
						)),
					),
			),
		));
		?>
		</div>
	</div>
</div>

<?php
Yii::app()->clientScript->registerScript('validatex', '
function change(){
	$.fn.yiiGridView.update("warehouse-item-grid", {
		data: $(this).serialize()
	});
}

function stok(id){
	var left = (screen.width/2)-(350/2);
	var top = (screen.height/2)-(300/2);
	window.open("'.Yii::app()->createUrl('setup/updateStokAwal').'/id/"+id,"","width=350,height=300,top="+top+",left="+left);
}
', CClientScript::POS_END);
?>