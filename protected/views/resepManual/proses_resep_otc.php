<?php
$this->breadcrumbs=array(
	'Data Registrasi Pasien'=>array('resepBebas/index'),
	'Form Penjualan Resep Bebas',
);
$this->title=array(
	'title'=>'Form Penjualan Resep Bebas',
	'deskripsi'=>'Mengelola Penjualan Bebas'
);?>

<div class="panel panel-default">
	<div class="panel-heading">
		<h3 class="panel-title pull-left">Form Penjualan Resep Bebas</h3>
		<a href="<?php echo Yii::app()->createUrl(Yii::app()->controller->id);?>" class="btn btn-primary btn-xs pull-right">
			<i class="fa fa-arrow-circle-left"></i>
			<span>Kembali</span>
		</a>
		<div style="clear:both"></div>
	</div>
	<div class="panel-body">
		<?php echo $this->renderPartial('buat_resep_otc',array("registrasi"=>$registrasi,"transaksi"=>$transaksi));?>
	</div>
</div>