<div class="box box-primary">
	<div class="box-body">
		<?php
		if(Yii::app()->user->hasFlash('error')){
			echo'
			<div class="alert alert-danger">
				<button type="button" class="close" data-dismiss="alert">&times;</button>
				'.Yii::app()->user->getFlash('error').'
			</div>';
		}
		?>
		<?php
		if(Yii::app()->user->hasFlash('failed')){
			echo'
			<div class="alert alert-danger">
				<button type="button" class="close" data-dismiss="alert">&times;</button>
				'.Yii::app()->user->getFlash('failed').'
			</div>';
		}
		?>
		<?php $form=$this->beginWidget('booster.widgets.TbActiveForm',array(
				'id'=>'resep-pasien-form',
				'enableAjaxValidation'=>false,
			)); ?>
			
			<?php echo $form->errorSummary($transaksi); ?>
		<div class="row">
			<div class="col-sm-6">
				<div class="row">
					<div class="col-sm-8">
						<label>Cari Nama Obat  / Kandungan Obat</label>
						<?php
						$this->widget('zii.widgets.jui.CJuiAutoComplete',array(
							'name'=>'resep',
							'options'=>array(
								'minLength'=>3,
								'showAnim'=>'fold',
								'select'=>"js:function(event, data) {
									var item=data.item.label;
									addRow(item);
								}"
							),
							'source'=>$this->createUrl("registrasi/getObat"),
							'htmlOptions'=>array(
								'class'=>"form-control",
								"id"=>"searchResep"
							),
						));
						?>
					</div>
					<div class="col-sm-4">
						<br />
						<button class="btn btn-primary" onClick="addHeader()" type="button" id="addHead">Tambah Header Racikan</button>
						<button class="btn btn-danger" onClick="closeHeader()" type="button" id="closeHead" style="display:none;">Tutup Racikan</button>
						
					</div>
				</div>
				<br/>
				<small>
					*Klik Tambah Header Untuk Membuat Racikan, Lalu Pilih Item yang akan menjadi komponen racikan.
				</small>
				<br/>
				<small>
					*Klik Tutup Header Racikan Untuk Menginputkan Item non racikan
				</small>
				<br/>
				<small>
					*Untuk Input Item Non Racikan, Pastikan Tulisan Tombol adalah 
					<b>"Tambah Header Racikan"</b>
				</small>
				<br/>
				<small>
					*Komponen Racikan Harus Lebih dari 1 Item
				</small>
			</div>
			
			<div class="col-sm-6">
				<?php echo $form->dropDownListGroup($transaksi,'gudang_asal', array('widgetOptions'=>array('data'=>CHtml::listData(UserGudang::model()->findAll(array("condition"=>"id_user='".Yii::app()->user->id."'")),'idGudang.id_gudang','idGudang.nama_gudang'), 'htmlOptions'=>array('class'=>'input-large','empty'=>'- Pilih Depo -')))); ?>
				<?php echo $form->textFieldGroup($transaksi,'nama_pasien',array('widgetOptions'=>array('htmlOptions'=>array('class'=>'span5','maxlength'=>20)))); ?>
			</div>
		</div>
		
		<table class="table table-stripped">
			<tr>
				<th>Nama Obat</th>
				<th>Stok</th>
				<th width="100px">Qty</th>
				<th>Satuan</th>
				<th>Harga</th>
				<th>Signa</th>
				<th></th>
			</tr>
			<tbody id="data">
				
			</tbody>
		</table>
		
		<hr>
		<div class="form-actions">
			<?php $this->widget('booster.widgets.TbButton', array(
					'buttonType'=>'submit',
					'context'=>'primary',
					'htmlOptions'=>array(
						//"disabled"=>true,
						"id"=>"submit",
						"onClick"=>"return confirm('Apakah Resep Sudah Benar?');"
					),
					'label'=>$transaksi->isNewRecord ? 'Proses Penjualan' : 'Proses Resep',
				)); 
				?>
		</div>
		<?php $this->endWidget(); ?>
	</div>
</div>

<?php
Yii::app()->clientScript->registerScript('addResep', '
var gudang = "";
var rowResep=[];
var racikan = false;
var racikanId = "1";

$("#ItemTransaksi_gudang_asal").change(function(){
	var id = $("#ItemTransaksi_gudang_asal").val();
	gudang = id;
	if(rowResep.length > 0) {
		rowResep.forEach((x, idx) => {
			deleteRow(x.id)
		});
	}
});

function deleteRow(id){
	if(id[0] === `R`){
		jQuery(`.row${id}`).each(function() {
			var val = $(this).attr(`val`);
			$(this).remove();
			rowResep = rowResep.filter(x => x.id != val);
		});
		$(`#row${id}`).remove();
		rowResep.splice(id, 1);
	} else {
		$(`#row${id}`).remove();
		rowResep = rowResep.filter(x => x.id != id);
	}
	cekSubmit();
}

function closeHeader(){
	racikan = false;
	racikanId++;
	$("#closeHead").hide();
	$("#addHead").show();
}


function addHeader(){
	racikan = true;
	$("#addHead").hide();
	$("#closeHead").show();
	rowResep.push({id: "R"+racikanId, name: "Racikan"});
	var inputItem="<input type=\"hidden\" name=\"id_item[R"+racikanId+"]\" value=\"R"+racikanId+"\" class=\"form-control\"/><input type=\"text\" name=\"nameR"+racikanId+"\" value=\"Racikan Ke "+racikanId+"\" class=\"form-control\"/>";
	var stokItem=0;
	var qty="<input type=\"number\" name=\"qtyR"+racikanId+"\" value=\"\" class=\"form-control\" required=\"required\" min=\"1\"/>";
	var satuan="Racikan";
	var harga="<input type=\"number\" name=\"biayaR"+racikanId+"\" value=\"5000\" class=\"form-control\" required=\"required\" min=\"1\" placeholder=\"Biaya Racikan\"/>";
	var signa="<input type=\"text\" name=\"signaR"+racikanId+"\" id=\"signaR"+racikanId+"\" value=\"\" class=\"form-control\" required=\"required\"//>";
	var remove="<span class=\"btn btn-danger\" onclick=\"deleteRow(\'R"+racikanId+"\')\"><i class=\"fa fa-times\"></i></span>";
	$("#data").append("<tr id=\"rowR"+racikanId+"\"><td>"+inputItem+"</td><td>"+stokItem+"</td><td>"+qty+"</td><td>"+satuan+"</td><td>"+harga+"</td><td>"+signa+"</td><td>"+remove+"</td></tr>");
					
	jQuery("#signaR"+racikanId).autocomplete({"minLength":3,"showAnim":"fold","source":"'.Yii::app()->createUrl("registrasi/getSigna").'"});
}

function cekSubmit(){
	console.log(rowResep);
	console.log(rowResep.length);
}

function addRow(item){
	if(gudang !== ""){
	$.ajax({
		url: "'.Yii::app()->createAbsoluteUrl('registrasi/getObat').'",
		cache: false,
		type: "POST",
		data:"item="+item,
		success: function(msg){
			var data=$.parseJSON(msg);
			if(rowResep[data.id_item] == undefined){
				cekSubmit();
				if(!racikan){
					rowResep.push({id: data.id_item, name: data.nama_item});
					$("#searchResep").val("");
					var inputItem="<input type=\"hidden\" name=\"id_item[]\" value=\""+data.id_item+"\"/>"+data.nama_item;
					var stokItem=data.stok;
					var qty="<input type=\"number\" name=\"qty"+data.id_item+"\" value=\"\" class=\"form-control\" required=\"required\" max=\""+stokItem+"\" min=\"1\"/>";
					var satuan=data.satuan;
					var harga=data.harga;
					var signa="<input type=\"text\" name=\"signa"+data.id_item+"\" id=\"signa"+data.id_item+"\" value=\"\" class=\"form-control\" required=\"required\"//>";
					var remove="<span class=\"btn btn-danger\" onclick=\"deleteRow("+data.id_item+")\"><i class=\"fa fa-times\"></i></span>";
					$("#data").append("<tr id=\"row"+data.id_item+"\"><td>"+inputItem+"</td><td>"+stokItem+"</td><td>"+qty+"</td><td>"+satuan+"</td><td>"+harga+"</td><td>"+signa+"</td><td>"+remove+"</td></tr>");
					
					jQuery("#signa"+data.id_item).autocomplete({"minLength":3,"showAnim":"fold","source":"'.Yii::app()->createUrl("registrasi/getSigna").'"});
				} else {
					rowResep.push({id: data.id_item, name: data.nama_item});
					$("#searchResep").val("");
					var inputItem="&nbsp;&nbsp;<i class=\"fa fa-chevron-right\"></i> "+data.nama_item;
					var stokItem=data.stok;
					var qty="<input type=\"number\" name=\"id_item[R"+racikanId+"]["+data.id_item+"][qty]\"  value=\"\" class=\"form-control\" required=\"required\" max=\""+stokItem+"\" min=\"0.1\" step=\"0.1\"/>";
					var satuan=data.satuan;
					var harga=data.harga;
					var remove="<span class=\"btn btn-danger\" onclick=\"deleteRow("+data.id_item+")\"><i class=\"fa fa-times\"></i></span>";
					$("#data").append("<tr id=\"row"+data.id_item+"\" val=\""+data.id_item+"\" class=\"rowR"+racikanId+"\"><td>"+inputItem+"</td><td>"+stokItem+"</td><td>"+qty+"</td><td>"+satuan+"</td><td>"+harga+"</td><td></td><td>"+remove+"</td></tr>");
					
					jQuery("#signa"+data.id_item).autocomplete({"minLength":3,"showAnim":"fold","source":"'.Yii::app()->createUrl("registrasi/getSigna").'"});
				}
			}else{
				$("#searchResep").val("");
			}
		}
	});
	} else {
		$("#searchResep").val("");
		alert("Gudang Asal Harus Dipilih !");
	}
}
', CClientScript::POS_END);
?>