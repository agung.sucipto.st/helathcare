<?php
$this->breadcrumbs=array(
	'Data Registrasi Pasien'=>array('resepManual/index'),
	'Form Penjualan Resep Manual',
);
$this->title=array(
	'title'=>'Form Penjualan Resep Manual',
	'deskripsi'=>'Mengelola Penjualan Menggunakan Registrasi'
);?>

<div class="panel panel-default">
	<div class="panel-heading">
		<h3 class="panel-title pull-left">Form Penjualan Resep Manual</h3>
		<a href="<?php echo Yii::app()->createUrl(Yii::app()->controller->id);?>" class="btn btn-primary btn-xs pull-right">
			<i class="fa fa-arrow-circle-left"></i>
			<span>Kembali</span>
		</a>
		<div style="clear:both"></div>
	</div>
	<div class="panel-body">
		<?php echo $this->renderPartial('buat_resep',array("model"=>$model,"registrasi"=>$registrasi,"transaksi"=>$transaksi));?>
	</div>
</div>