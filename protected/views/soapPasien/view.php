<?php
$this->breadcrumbs=array(
	'Kelola Soap Pasien'=>array('soapPasien/index'),
	'Detail Soap Pasien',
);

$this->title=array(
	'title'=>'Detail Soap Pasien',
	'deskripsi'=>'Untuk Melihat Detail Soap Pasien'
);
?>

<div class="panel panel-default">
	<div class="panel-heading">
		<h3 class="panel-title pull-left">Detail</h3>
		<a href="<?php echo Yii::app()->createUrl(Yii::app()->controller->id);?>" class="btn btn-primary btn-xs pull-right" style="display:<?php echo $visibleEdit;?>">
			<i class="fa fa-arrow-circle-left"></i>
			<span>Kembali</span>
		</a>
		<a href="<?php echo Yii::app()->createUrl(Yii::app()->controller->id.'/create');?>" class="btn btn-primary btn-xs pull-right" style="display:<?php echo ((Lib::accessBy('SoapPasien','create')==true)?"block":"");?>">
			<i class="fa fa-copy"></i>
			<span>Tambah Data</span>
		</a>
		<a href="<?php echo Yii::app()->createUrl(Yii::app()->controller->id.'/update/id/'.$_GET['id']);?>" class="btn btn-primary btn-xs pull-right" style="display:<?php echo ((Lib::accessBy('SoapPasien','update')==true)?"block":"");?>">
			<i class="fa fa-edit"></i>
			<span>Edit Data</span>
		</a>
		<div style="clear:both"></div>
	</div>
	<div class="panel-body">		
		<div id="example-2_wrapper" class="dataTables_wrapper form-inline dt-bootstrap no-footer">
			<?php $this->widget('booster.widgets.TbDetailView',array(
			'data'=>$model,
			'attributes'=>array(
							'id_soap_pasien',
				'id_registrasi',
				'id_pegawai',
				'subjektif',
				'objektif',
				'assesment',
				'planing',
				'instruksi',
				'soap_status',
				'user_create',
				'user_update',
				'user_final',
				'time_create',
				'time_update',
				'time_final',
			),
			)); ?>
		</div>
	</div>
</div>
