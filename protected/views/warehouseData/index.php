<?php

$this->title['title']="Warehouse";

$this->title['deskripsi']="Mengelola Persediaan Barang";

?>



<div class="box box-primary">

	<div class="box-body">

		<div class="row">

			<div class="col-sm-3">

				<div class="small-box bg-green">

					<div class="inner">

							<h3>

								<?php echo Item::model()->count();?>

							</h3>

							<p>

								Master Item

							</p>

						</div>

						<div class="icon">

							<i class="ion ion-briefcase"></i>

						</div>

						<a href="<?=Yii::app()->createUrl('item');?>" class="small-box-footer">

							Info <i class="fa fa-arrow-circle-right"></i>

						</a>

					</div>

			</div>

			<div class="col-sm-3">

				<div class="small-box bg-aqua">

					<div class="inner">

							<h3>

								<?php echo Supplier::model()->count();?>

							</h3>

							<p>

								Supplier

							</p>

						</div>

						<div class="icon">

							<i class="ion ion-compass"></i>

						</div>

						<a href="<?=Yii::app()->createUrl('supplier');?>" class="small-box-footer">

							Info <i class="fa fa-arrow-circle-right"></i>

						</a>

					</div>

			</div>

			<div class="col-sm-3">

				<div class="small-box bg-red">

					<div class="inner">

							<h3>

								<?php 

								$criteria=new CDbCriteria;

								$criteria->with=array("idItem");

								$criteria->select="SUM(stok*idItem.wac) as stock";

								$rp=ItemGudang::model()->find($criteria);

								echo number_format($rp->stock,0);

								?>

							</h3>

							<p>

								Total Persediaan (Rp)

							</p>

						</div>

						<div class="icon">

							<i class="ion ion-calculator"></i>

						</div>

						<a href="#" class="small-box-footer">

							Info <i class="fa fa-arrow-circle-right"></i>

						</a>

					</div>

			</div>

			<div class="col-sm-3">

				<div class="small-box bg-yellow">

					<div class="inner">

							<h3>

								<?php 

								$criteria=new CDbCriteria;

								$criteria->select="SUM(stok) as stock";

								$rp=ItemGudang::model()->find($criteria);

								echo number_format($rp->stock,0);

								?>

							</h3>

							<p>

								Total Persediaan (Stok)

							</p>

						</div>

						<div class="icon">

							<i class="ion ion-stats-bars"></i>

						</div>

						<a href="#" class="small-box-footer">

							Info <i class="fa fa-arrow-circle-right"></i>

						</a>

					</div>

			</div>

		</div>

		

		<div class="row">

			<div class="col-sm-4">

				<h4><i class="fa fa-calendar"></i> 6 Bulan Mendekati Expire</h4>

				<table class="table table-striped">

					<tr>

						<th>Item</th>

						<th>Tanggal Expire</th>

						<th>Stok Saat Ini</th>

					</tr>

					<?php

						$query="select *, DATEDIFF(tanggal_expired,CURDATE()) as selisih from (SELECT DISTINCT(daftar_item_transaksi.id_item),tanggal_expired,item.nama_item FROM `daftar_item_transaksi` 

						INNER JOIN item_transaksi ON daftar_item_transaksi.id_item_transaksi=item_transaksi.id_item_transaksi

						INNER join item on daftar_item_transaksi.id_item=item.id_item

						AND (item_transaksi.id_jenis_item_transaksi='1' OR item_transaksi.id_jenis_item_transaksi='8')

						order by daftar_item_transaksi.tanggal_expired DESC) as new

						where DATEDIFF(tanggal_expired,CURDATE())<=180

						group by new.id_item

						order by selisih ASC

						limit 0,50";

						$exp=Yii::app()->db->createCommand($query)->queryAll();

						if(!empty($exp)){

							foreach($exp as $row){

								echo'

									<tr>

										<th>'.$row['nama_item'].'</th>

										<td>'.$row['tanggal_expired'].'</td>

										<td>'.Item::getStokAll($row['id_item']).'</td>

									</tr>

								';

							}

						}

					?>

				</table>

			</div>

			<div class="col-sm-4">

				<h4><i class="fa fa-medkit"></i> Mendekati / Minimum Stok</h4>

				<table class="table table-striped">

					<tr>

						<th>Item</th>

						<th>Gudang</th>

						<th>Minimum Stok</th>

						<th>Stok Saat Ini</th>

					</tr>

					<?php

						$criteria=new CDbCriteria;

						$criteria->alias="tab";

						$criteria->condition="tab.stok<=tab.min_stok";

						$criteria->order="(tab.stok-tab.min_stok) ASC";

						$criteria->limit=50;

						$min=ItemGudang::model()->findAll($criteria);

						foreach($min as $row){

							echo'

								<tr>

									<th>'.$row->idItem->nama_item.'</th>

									<td>'.$row->idGudang->nama_gudang.'</td>

									<td>'.$row->min_stok.'</td>

									<td>'.$row->stok.'</td>

								</tr>

							';

						}

					?>

				</table>

			</div>

			

			<div class="col-sm-4">

				<h4><i class="fa fa-medkit"></i> Melebihi Maximum Stok</h4>

				<table class="table table-striped">

					<tr>

						<th>Item</th>

						<th>Gudang</th>

						<th>Minimum Stok</th>

						<th>Stok Saat Ini</th>

					</tr>

					<?php

						$criteria=new CDbCriteria;

						$criteria->condition="stok>max_stok AND min_stok!='0'";

						$criteria->order="(max_stok-stok) DESC";

						$criteria->limit=50;

						$min=ItemGudang::model()->findAll($criteria);

						foreach($min as $row){

							echo'

								<tr>

									<th>'.$row->idItem->nama_item.'</th>

									<td>'.$row->idGudang->nama_gudang.'</td>

									<td>'.$row->min_stok.'</td>

									<td>'.$row->stok.'</td>

								</tr>

							';

						}

					?>

				</table>

			</div>

		</div>

	</div>



</div>