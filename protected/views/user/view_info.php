<?php
$this->widget('booster.widgets.TbDetailView',array(
'data'=>$model,
'attributes'=>array(
	array(
		"label"=>"Pegawai",
		"value"=>function($model){
			if(!empty($model->id_pegawai)){
				return $model->idPegawai->idPersonal->nama_lengkap;
			}else{
				return "NOT SET";
			}
		},
	),
	'username',
	'last_login',
	'time_create',
	'time_update',
	'status',
),
)); ?>