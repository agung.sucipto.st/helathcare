<style>
	.checkboxes, .checkboxes ul {
		margin:0;
		padding:0;
		list-style:none
	}
	.checkboxes ul {
		margin-left:1em;
		position:relative
	}
	.checkboxes ul ul {
		margin-left:.5em
	}
	.checkboxes ul:before {
		content:"";
		display:block;
		width:0;
		position:absolute;
		top:0;
		bottom:0;
		left:0;
		border-left:1px solid
	}
	.checkboxes li {
		margin:0;
		padding:0 1em;
		line-height:2em;
		color:#369;
		font-weight:700;
		position:relative
	}
	.checkboxes ul li:before {
		content:"";
		display:block;
		width:10px;
		height:0;
		border-top:1px solid;
		margin-top:-1px;
		position:absolute;
		top:1em;
		left:0
	}
	.checkboxes ul li:last-child:before {
		background:#fff;
		height:auto;
		top:1em;
		bottom:0
	}
	.indicator {
		margin-right:5px;
	}
	.checkboxes li a {
		text-decoration: none;
		color:#369;
	}
	.checkboxes li button, .checkboxes li button:active, .checkboxes li button:focus {
		text-decoration: none;
		color:#369;
		border:none;
		background:transparent;
		margin:0px 0px 0px 0px;
		padding:0px 0px 0px 0px;
		outline: 0;
	}
</style> 
<div class="panel panel-default">
<div class="panel-heading">
Access
</div>
<div class="panel-body">
<form action="" method="POST">
<ul class="checkboxes">
	<?php
	$output="";
	$menus = Access::model()->findAll(array("condition"=>"access_status='1'","order"=>"access_id asc"));
	echo Lib::UserAccessTree($menus);
	
	/*
	$menus = AccessMenuArrange::model()->findAll(array("condition"=>"menu_arrange_parent='0'","order"=>"menu_arrange_order asc"));
	foreach($menus as $rows){
		$child=AccessMenuArrange::model()->findAll(array("condition"=>"menu_arrange_parent='$rows->menu_arrange_id'","order"=>"menu_arrange_order asc"));
		if(Count($child)>0){
			$ua=UserAccess::model()->find(array("condition"=>"id_user='".$_GET['id']."' and access_id='$rows->access_id'"));
			if(!empty($ua)){
				$checked="checked";
			}else{
				$checked='';
			}
			$output.='
			<li>
				 <input type="checkbox" name="access[]" id="access'.$rows->menu_arrange_id.'" value="'.$rows->access_id.'" '.$checked.'> 
				 <label for="access'.$rows->menu_arrange_id.'" >'.((!empty($rows->menu_arrange_name)?$rows->menu_arrange_name:$rows->access->access_name)).'</label>
				<ul>';
				foreach($child as $xData){
					$subchild=AccessMenuArrange::model()->findAll(array("condition"=>"menu_arrange_parent='$xData->menu_arrange_id'","order"=>"menu_arrange_order asc"));
					if(Count($subchild)>0){
							$ua=UserAccess::model()->find(array("condition"=>"id_user='".$_GET['id']."' and access_id='$xData->access_id'"));
							if(!empty($ua)){
								$checked="checked";
							}else{
								$checked='';
							}
							$output.='
							<li>
							 <input type="checkbox" name="access[]" id="access'.$xData->menu_arrange_id.'" value="'.$xData->access_id.'" '.$checked.'> 
							 <label for="access'.$xData->menu_arrange_id.'" >'.((!empty($xData->menu_arrange_name)?$xData->menu_arrange_name:$xData->access->access_name)).'</label>
							
							<ul>';
							foreach($subchild as $subData){
								$ua=UserAccess::model()->find(array("condition"=>"id_user='".$_GET['id']."' and access_id='$subData->access_id'"));
								if(!empty($ua)){
									$checked="checked";
								}else{
									$checked='';
								}
								$output.='
								<li>
								 <input type="checkbox" name="access[]" id="access'.$subData->menu_arrange_id.'" value="'.$subData->access_id.'" '.$checked.'> 
								 <label for="access'.$subData->menu_arrange_id.'" >'.((!empty($subData->menu_arrange_name)?$subData->menu_arrange_name:$subData->access->access_name)).'</label>';
								 if(!empty($subData->access_id)){
									$subMn=Access::model()->findAll(array("condition"=>"access_parent='$subData->access_id'"));
									  if(count($subMn)>0){
										   $output.='<ul>';
										   foreach($subMn as $rMn){
											   $ua=UserAccess::model()->find(array("condition"=>"id_user='".$_GET['id']."' and access_id='$rMn->access_id'"));
												if(!empty($ua)){
													$checked="checked";
												}else{
													$checked='';
												}
											   $output.='
													<li>
													 <input type="checkbox" name="access[]" id="accessSub'.$rMn->access_id.'" value="'.$rMn->access_id.'" '.$checked.'> 
													 <label for="accessSub'.$rMn->access_id.'" >'.$rMn->access_name.'</label>
													</li>';
										   }
											$output.='</ul>';
									  }
									
								 }
								 $output.='
								</li>';
							}
							$output.='</ul></li>';
					}else{
						$ua=UserAccess::model()->find(array("condition"=>"id_user='".$_GET['id']."' and access_id='$xData->access_id'"));
						if(!empty($ua)){
							$checked="checked";
						}else{
							$checked='';
						}
						$output.='
								<li>
								 <input type="checkbox" name="access[]" id="access'.$xData->menu_arrange_id.'" value="'.$xData->access_id.'" '.$checked.'> 
								 <label for="access'.$xData->menu_arrange_id.'" >'.((!empty($xData->menu_arrange_name)?$xData->menu_arrange_name:$xData->access->access_name)).'</label>';
								 if(!empty($xData->access_id)){
									$subMn=Access::model()->findAll(array("condition"=>"access_parent='$xData->access_id'"));
									  if(count($subMn)>0){
										   $output.='<ul>';
										   foreach($subMn as $rMn){
											   $ua=UserAccess::model()->find(array("condition"=>"id_user='".$_GET['id']."' and access_id='$rMn->access_id'"));
												if(!empty($ua)){
													$checked="checked";
												}else{
													$checked='';
												}
											   $output.='
													<li>
													 <input type="checkbox" name="access[]" id="accessSub'.$rMn->access_id.'" value="'.$rMn->access_id.'" '.$checked.'> 
													 <label for="accessSub'.$rMn->access_id.'" >'.$rMn->access_name.'</label>
													</li>';
										   }
											$output.='</ul>';
									  }
									
								 }
								 $output.='
								</li>';
					}
				}
				$output.='</ul></li>';
		}else{
			$output.='
			<li>
			 <input type="checkbox" name="access[]" id="access'.$rows->menu_arrange_id.'" value="'.$rows->access_id.'"> 
			 <label for="access'.$rows->menu_arrange_id.'" >'.((!empty($rows->menu_arrange_name)?$rows->menu_arrange_name:$rows->access->access_name)).'</label>';
			 if(!empty($rows->access_id)){
				$subMn=Access::model()->findAll(array("condition"=>"access_parent='$rows->access_id'"));
				  if(count($subMn)>0){
					   $output.='<ul>';
					   foreach($subMn as $rMn){
							$ua=UserAccess::model()->find(array("condition"=>"id_user='".$_GET['id']."' and access_id='$rMn->access_id'"));
							if(!empty($ua)){
								$checked="checked";
							}else{
								$checked='';
							}
						   $output.='
								<li>
								 <input type="checkbox" name="access[]" id="accessSub'.$rMn->access_id.'" value="'.$rMn->access_id.'" '.$checked.'> 
								 <label for="accessSub'.$rMn->access_id.'" >'.$rMn->access_name.'</label>
								</li>';
					   }
						$output.='</ul>';
				  }
				
			 }
			 echo'
			</li>
			';
		}
	}
	echo $output;
	//*/
	?>
</ul>
		<hr>
		<input type="submit" value="Update Access" class="btn btn-primary"/>
		<?php
		echo CHtml::button('Cancel', array(
            'class' => 'btn btn-primary',
            'onclick' => "history.go(-1)",
                )
        );
		?>
</form>

</div>
</div>

<script>
(function($){
$('ul.checkboxes input[type="checkbox"]').each (
 function () {
  $(this).bind('click change', function (){
   if($(this).is(':checked')) {
	$(this).siblings('ul').find('input[type="checkbox"]').attr('checked', 'checked');
	$(this).parents('ul').siblings('input[type="checkbox"]').attr('checked', 'checked');
   } else {
	$(this).siblings('ul').find('input[type="checkbox"]').removeAttr('checked', 'checked');
   }
  });
 }
);
 
})(jQuery);


$.fn.extend({
    treed: function (o) {
      
      var openedClass = 'glyphicon-minus-sign';
      var closedClass = 'glyphicon-plus-sign';
      
      if (typeof o != 'undefined'){
        if (typeof o.openedClass != 'undefined'){
        openedClass = o.openedClass;
        }
        if (typeof o.closedClass != 'undefined'){
        closedClass = o.closedClass;
        }
      };
      
        //initialize each of the top levels
        var tree = $(this);
        tree.addClass("tree");
        tree.find('li').has("ul").each(function () {
            var branch = $(this); //li with children ul
            branch.prepend("<i class='indicator glyphicon " + closedClass + "'></i>");
            branch.addClass('branch');
            branch.on('click', function (e) {
                if (this == e.target) {
                    var icon = $(this).children('i:first');
                    icon.toggleClass(openedClass + " " + closedClass);
                    $(this).children().children().toggle();
                }
            })
            branch.children().children().toggle();
        });
        //fire event from the dynamically added icon
      tree.find('.branch .indicator').each(function(){
        $(this).on('click', function () {
            $(this).closest('li').click();
        });
      });
        //fire event to open branch if the li contains an anchor instead of text
        tree.find('.branch>a').each(function () {
            $(this).on('click', function (e) {
                $(this).closest('li').click();
                e.preventDefault();
            });
        });
        //fire event to open branch if the li contains a button instead of text
        tree.find('.branch>button').each(function () {
            $(this).on('click', function (e) {
                $(this).closest('li').click();
                e.preventDefault();
            });
        });
    }
});

$('.checkboxes').treed();

</script>