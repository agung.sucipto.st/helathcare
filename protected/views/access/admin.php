<?php
$this->breadcrumbs=array(
	'Master Data'=>array('masterData/index'),
	'Kelola Akses',
);
$this->title=array(
	'title'=>'Akses',
	'deskripsi'=>'Untuk Mengelola Akses'
);

?>

<div class="panel panel-default">
	<div class="panel-heading">
		<h3 class="panel-title pull-left"></h3>
		<a href="<?php echo Yii::app()->createUrl(Yii::app()->controller->id.'/create');?>" class="btn btn-primary btn-xs pull-right">
			<i class="fa fa-copy"></i>
			<span>Tambah Data</span>
		</a>
		<div style="clear:both"></div>
	</div>
	<div class="panel-body">
		<style>
		.pagination li.selected a{
			background:rgb(235, 235, 236);
		}
		</style>
		
		<?php
		if(Yii::app()->user->hasFlash('success')){
			echo'
			<div class="alert alert-success">
				<button type="button" class="close" data-dismiss="alert">&times;</button>
				'.Yii::app()->user->getFlash('success').'
			</div>';
		}
		?>
							
		<div id="example-2_wrapper" class="dataTables_wrapper form-inline dt-bootstrap no-footer">
		
		<?php 
		// put this somewhere on top
		$pageSize=Yii::app()->user->getState('pageSize',Yii::app()->params['defaultPageSize']); ?>		
		<?php		
		$this->widget('booster.widgets.TbExtendedGridView', array(
			'id'=>'access-grid',
			'type' => 'striped',
			'dataProvider' => $model->search(),
			'filter'=>$model,
			'summaryText'=>'Menampilkan {start}-{end} dari {count} hasil.',
			'selectableRows' => 2,
			'responsiveTable' => true,
			'enablePagination' => true,
			'pager' => array(
				'htmlOptions'=>array(
					'class'=>'pagination'
				),
				'maxButtonCount' => 5,
				'cssFile' => true,
				'header' => false,
				'firstPageLabel' => '<<',
				'prevPageLabel' => '<',
				'nextPageLabel' => '>',
				'lastPageLabel' => '>>',
			),
			
			'columns'=>array(
					'access_name',
					'access_controller',
					'access_action',
					array(
						'name'=>'access_status',
						'value'=>function($data){
							return ($data->access_status==0)?"Non Aktif":"Aktif";
						},
						'filter'=>array(0=>"Non Aktif",1=>"Aktif")
					),
					array(
						'name'=>'access_visible',
						'value'=>function($data){
							return ($data->access_visible==0)?"Tidak Tampil Menu":"Tampil Menu";
						},
						'filter'=>array(0=>"Tidak Tampil Menu",1=>"Tampil Menu")
					),
					array(
						'class'=>'booster.widgets.TbButtonColumn',
						 'deleteConfirmation'=>'Anda yakin akan menhapus data?',
						'template'=>'{view}{update}{delete}',
						'buttons'=>array
						(
							'view' => array
							(
								'label'=>'View',
								'icon'=>'search',
								'options'=>array(
									'class'=>'btn btn-default btn-xs',
								),
							),
							'update' => array
							(
								'label'=>'Update',
								'icon'=>'pencil',
								'options'=>array(
									'class'=>'btn btn-default btn-xs',
								),
							),
							'delete' => array
							(
								'label'=>'Delete',
								'icon'=>'trash',
								'visible'=>'$data->getEmpty($data->access_id)',
								'options'=>array(
									'class'=>'btn btn-default btn-xs delete',
								),
							)
						),
						'header'=>CHtml::dropDownList('pageSize',$pageSize,array(10=>10,20=>20,50=>50,100=>100,200=>200,500=>500,1000=>1000),array(
							'onchange'=>"$.fn.yiiGridView.update('access-grid',{ data:{pageSize: $(this).val() }})",
						)),
					),
			),
		));
		?>
		</div>
	</div>
</div>