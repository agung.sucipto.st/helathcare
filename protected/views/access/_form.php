<?php $form=$this->beginWidget('booster.widgets.TbActiveForm',array(
	'id'=>'access-form',
	'enableAjaxValidation'=>true,
)); ?>

<p class="help-block">Isian dengan tanda <span class="required">*</span> wajib diisi.</p>

<?php echo $form->errorSummary($model); ?>

	<?php echo $form->textFieldGroup($model,'access_name',array('widgetOptions'=>array('htmlOptions'=>array('class'=>'span5','maxlength'=>50)))); ?>

	<?php echo $form->textFieldGroup($model,'access_controller',array('widgetOptions'=>array('htmlOptions'=>array('class'=>'span5','maxlength'=>50)))); ?>
	<?php 
		$data = glob(Yii::app()->basePath.'/protected/controller'."*");
		print_R($data);
	?>

	<?php echo $form->textFieldGroup($model,'access_action',array('widgetOptions'=>array('htmlOptions'=>array('class'=>'span5','maxlength'=>50)))); ?>
	<?php echo $form->dropDownListGroup($model,'access_parent', array('widgetOptions'=>array('data'=>CHtml::listData(Access::model()->findAll(array("condition"=>"access_status='1'")),'access_id','access_name'), 'htmlOptions'=>array('class'=>'input-large','empty'=>'- Pilih -')))); ?>
	<?php echo $form->dropDownListGroup($model,'access_parent', array('widgetOptions'=>array('data'=>CHtml::listData(Access::model()->findAll(array("condition"=>"access_status='1'")),'access_id','access_name'), 'htmlOptions'=>array('class'=>'input-large','empty'=>'- Pilih -')))); ?>
	<?php echo $form->textFieldGroup($model,'access_status',array('widgetOptions'=>array('htmlOptions'=>array('class'=>'span5')))); ?>

	<?php echo $form->textFieldGroup($model,'access_visible',array('widgetOptions'=>array('htmlOptions'=>array('class'=>'span5')))); ?>

<div class="form-actions">
	<?php $this->widget('booster.widgets.TbButton', array(
			'buttonType'=>'submit',
			'context'=>'primary',
			'label'=>$model->isNewRecord ? 'Tambah' : 'Simpan',
		)); 
		echo CHtml::button('Batal', array(
            'class' => 'btn btn-primary',
            'onclick' => "history.go(-1)",
                )
        );
		?>

		</div>

<?php $this->endWidget(); ?>
