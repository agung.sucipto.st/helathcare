<?php
$this->breadcrumbs=array(
	'Kelola Akses'=>array('index'),
	'Detail Akses',
);

$this->title=array(
	'title'=>'Detail Akses',
	'deskripsi'=>'Untuk Melihat Detail Akses'
);
?>

<div class="panel panel-default">
	<div class="panel-heading">
		<h3 class="panel-title pull-left">Detail</h3>
		<a href="<?php echo Yii::app()->createUrl(Yii::app()->controller->module->id.'/'.Yii::app()->controller->id);?>" class="btn btn-primary btn-xs pull-right">
			<i class="fa fa-arrow-circle-left"></i>
			<span>Kembali</span>
		</a>
		<a href="<?php echo Yii::app()->createUrl(Yii::app()->controller->module->id.'/'.Yii::app()->controller->id.'/create',array('parent'=>$model->access_id));?>" class="btn btn-primary btn-xs pull-right">
			<i class="fa fa-copy"></i>
			<span>Tambah Data</span>
		</a>
		<a href="<?php echo Yii::app()->createUrl(Yii::app()->controller->module->id.'/'.Yii::app()->controller->id.'/update/id/'.$_GET['id']);?>" class="btn btn-primary btn-xs pull-right">
			<i class="fa fa-edit"></i>
			<span>Edit Data</span>
		</a>
		<div style="clear:both"></div>
	</div>
	<div class="panel-body">		
		<div id="example-2_wrapper" class="dataTables_wrapper form-inline dt-bootstrap no-footer">
			<?php $this->widget('booster.widgets.TbDetailView',array(
			'data'=>$model,
			'attributes'=>array(
				'access_id',
				'access_name',
				'access_controller',
				'access_action',
				'access_status',
				'access_visible',
			),
			)); ?>
			
		<?php 
		// put this somewhere on top
		$pageSize=Yii::app()->user->getState('pageSize',Yii::app()->params['defaultPageSize']); ?>		
		<?php		
		$this->widget('booster.widgets.TbExtendedGridView', array(
			'id'=>'access-grid',
			'type' => 'striped',
			'dataProvider' => $data->search(),
			'summaryText'=>'Menampilkan {start}-{end} dari {count} hasil.',
			'selectableRows' => 2,
			'responsiveTable' => true,
			'enablePagination' => true,
			'pager' => array(
				'htmlOptions'=>array(
					'class'=>'pagination'
				),
				'maxButtonCount' => 5,
				'cssFile' => true,
				'header' => false,
				'firstPageLabel' => '<<',
				'prevPageLabel' => '<',
				'nextPageLabel' => '>',
				'lastPageLabel' => '>>',
			),
			
			'columns'=>array(
					'access_name',
					'access_controller',
					'access_action',
					array(
						'name'=>'access_status',
						'value'=>function($data){
							return ($data->access_status==0)?"Non Aktif":"Aktif";
						},
						'filter'=>array(0=>"Non Aktif",1=>"Aktif")
					),
					array(
						'name'=>'access_visible',
						'value'=>function($data){
							return ($data->access_visible==0)?"Tidak Tampil Menu":"Tampil Menu";
						},
						'filter'=>array(0=>"Tidak Tampil Menu",1=>"Tampil Menu")
					),
					array(
						'value'=>"",
						'header'=>CHtml::dropDownList('pageSize',$pageSize,array(10=>10,20=>20,50=>50,100=>100,200=>200,500=>500,1000=>1000),array(
							'onchange'=>"$.fn.yiiGridView.update('access-grid',{ data:{pageSize: $(this).val() }})",
						)),
					),
			),
		));
		?>
		</div>
	</div>
</div>
