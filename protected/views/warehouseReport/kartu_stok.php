<?php
$this->breadcrumbs=array(
	'Kartu Stok',
);
$this->title=array(
	'title'=>'Kartu Stok',
	'deskripsi'=>'Untuk Memonitor Stok'
);
?>

<div class="panel panel-default">
	<div class="panel-heading">
		<h3 class="panel-title pull-left">Kartu Stok</h3>
		<div style="clear:both"></div>
	</div>
	<div class="panel-body">
		<?php $form=$this->beginWidget('booster.widgets.TbActiveForm',array(
			'action'=>Yii::app()->createUrl($this->route),
			'method'=>'POST',
			'type'=>'vertical',
		)); ?>

		<div class="row">
			<div class="col-sm-4">
				<?php echo $form->dateRangeGroup(
					$model,
					'waktu_transaksi',
					array(
						'label'=>'Periode Penerimaan',
						'wrapperHtmlOptions' => array(
							'class' => '',
						),
						'prepend' => '<i class="glyphicon glyphicon-calendar"></i>'
					)
				); ?>
			</div>
			<div class="col-sm-3">
				<?php echo $form->dropDownListGroup($model,'gudang_tujuan', array('label'=>"Gudang",'widgetOptions'=>array('data'=>CHtml::listData(Gudang::model()->findAll(),'id_gudang','nama_gudang'), 'htmlOptions'=>array('class'=>'input-large','empty'=>'- Choose -',"required"=>"required")))); ?>
			</div>
			<div class="col-sm-3">
				<label>Cari Nama Item / Kode Item</label>
					<?php
					$this->widget('zii.widgets.jui.CJuiAutoComplete',array(
						'name'=>'item',
						'options'=>array(
							'minLength'=>2,
							'showAnim'=>'fold',
							'select'=>"js:function(event, data) {
							}"
						),
						'source'=>$this->createUrl("item/getItem"),
						'htmlOptions'=>array(
							'class'=>"form-control",
							"id"=>"searchItem",
							"required"=>"required",
						),
					));
					?>
			</div>
			<div class="col-sm-2">
				<br/>
				<?php $this->widget('booster.widgets.TbButton', array(
					'buttonType' => 'submit',
					'context'=>'primary',
					'label'=>'Search',
				)); ?>
			</div>
		</div>

		<?php $this->endWidget(); ?>
		<?php
		if($data!=null){
		?>
		<h4>Kartu Stok : <?=$item->kode_item;?> - <?=$item->nama_item;?>, Satuan : <?=Item::getParentUnit($item->id_item)->idSatuan->nama_satuan;?></h4>
		<hr>
		<table class="table table-bordered">
			<tr>
				<th class="text-center">Keterangan</th>
				<th class="text-center">Waktu</th>
				<th class="text-center">Stok Awal</th>
				<th class="text-center">Masuk</th>
				<th class="text-center">Keluar</th>
				<th class="text-center">Stok Akhir</th>
			</tr>
			<?php
			foreach($data as $row){
				echo'
				<tr>
					<td>';
					echo $row->idItemTransaksi->idJenisItemTransaksi->nama_jenis_transaksi.'<br />';
					if($row->idItemTransaksi->id_jenis_item_transaksi==1){
						// penerimaan
						echo $row->idItemTransaksi->idSupplier->nama_supplier.' - '.$row->jumlah_satuan_kecil.' '.$row->idItemSatuanBesar->idSatuan->nama_satuan.' = '.$row->jumlah_transaksi.' '.$row->idItemSatuanKecil->idSatuan->nama_satuan;
						$before=$row->jumlah_sebelum_transaksi;
						$currentMin=0;
						$currentPlus=$row->jumlah_transaksi;
						$after=$row->jumlah_setelah_transaksi;
					}elseif($row->idItemTransaksi->id_jenis_item_transaksi==14){
						// batal penerimaan
						echo $row->idItemTransaksi->idSupplier->nama_supplier.' - '.$row->jumlah_satuan_kecil.' '.$row->idItemSatuanBesar->idSatuan->nama_satuan.' = '.$row->jumlah_transaksi.' '.$row->idItemSatuanKecil->idSatuan->nama_satuan;
						$before=$row->jumlah_sebelum_transaksi;
						$currentMin=$row->jumlah_transaksi;
						$currentPlus=0;
						$after=$row->jumlah_setelah_transaksi;
					}elseif($row->idItemTransaksi->id_jenis_item_transaksi==11){
						// penggunaan ruangan
						echo $row->idItemTransaksi->idRegistrasi->no_registrasi.' '.$row->idItemTransaksi->idRegistrasi->idPasien->idPersonal->nama_lengkap.' - '.$row->jumlah_satuan_kecil.' '.$row->idItemSatuanBesar->idSatuan->nama_satuan.' = '.$row->jumlah_transaksi.' '.$row->idItemSatuanKecil->idSatuan->nama_satuan;
						$before=$row->jumlah_sebelum_transaksi;
						$currentMin=$row->jumlah_transaksi;
						$currentPlus=0;
						$after=$row->jumlah_setelah_transaksi;
					}elseif($row->idItemTransaksi->id_jenis_item_transaksi==22){
						// batal penggunaan ruangan
						echo $row->idItemTransaksi->idRegistrasi->no_registrasi.' '.$row->idItemTransaksi->idRegistrasi->idPasien->idPersonal->nama_lengkap.' - '.$row->jumlah_satuan_kecil.' '.$row->idItemSatuanBesar->idSatuan->nama_satuan.' = '.$row->jumlah_transaksi.' '.$row->idItemSatuanKecil->idSatuan->nama_satuan;
						$before=$row->jumlah_sebelum_transaksi;
						$currentMin=0;
						$currentPlus=$row->jumlah_transaksi;
						$after=$row->jumlah_setelah_transaksi;
					}elseif($row->idItemTransaksi->id_jenis_item_transaksi==2){
						// penjualan resep
						echo $row->idItemTransaksi->idRegistrasi->no_registrasi.' '.$row->idItemTransaksi->idRegistrasi->idPasien->idPersonal->nama_lengkap.' - '.$row->jumlah_satuan_kecil.' '.$row->idItemSatuanBesar->idSatuan->nama_satuan.' = '.$row->jumlah_transaksi.' '.$row->idItemSatuanKecil->idSatuan->nama_satuan;
						$before=$row->jumlah_sebelum_transaksi;
						$currentMin=$row->jumlah_transaksi;
						$currentPlus=0;
						$after=$row->jumlah_setelah_transaksi;
					}elseif($row->idItemTransaksi->id_jenis_item_transaksi==15){
						// batal penjualan resep
						echo $row->idItemTransaksi->idRegistrasi->no_registrasi.' '.$row->idItemTransaksi->idRegistrasi->idPasien->idPersonal->nama_lengkap.' - '.$row->jumlah_satuan_kecil.' '.$row->idItemSatuanBesar->idSatuan->nama_satuan.' = '.$row->jumlah_transaksi.' '.$row->idItemSatuanKecil->idSatuan->nama_satuan;
						$before=$row->jumlah_sebelum_transaksi;
						$currentMin=0;
						$currentPlus=$row->jumlah_transaksi;
						$after=$row->jumlah_setelah_transaksi;
					}elseif($row->idItemTransaksi->id_jenis_item_transaksi==3){
						// transfer
						echo $row->idItemTransaksi->gudangTujuan->nama_gudang.' - '.$row->jumlah_satuan_kecil.' '.$row->idItemSatuanBesar->idSatuan->nama_satuan.' = '.$row->jumlah_transaksi.' '.$row->idItemSatuanKecil->idSatuan->nama_satuan;
						$before=$row->jumlah_sebelum_transaksi;
						$currentMin=$row->jumlah_transaksi;
						$currentPlus=0;
						$after=$row->jumlah_setelah_transaksi;
					}elseif($row->idItemTransaksi->id_jenis_item_transaksi==16){
						// batal transfer
						echo $row->idItemTransaksi->gudangTujuan->nama_gudang.' - '.$row->jumlah_satuan_kecil.' '.$row->idItemSatuanBesar->idSatuan->nama_satuan.' = '.$row->jumlah_transaksi.' '.$row->idItemSatuanKecil->idSatuan->nama_satuan;
						$before=$row->jumlah_sebelum_transaksi;
						$currentMin=0;
						$currentPlus=$row->jumlah_transaksi;
						$after=$row->jumlah_setelah_transaksi;
					}elseif($row->idItemTransaksi->id_jenis_item_transaksi==4){
						// terima transfer
						echo 'Dari Gudang :'.$row->idItemTransaksi->gudangAsal->nama_gudang.' - '.$row->jumlah_satuan_kecil.' '.$row->idItemSatuanBesar->idSatuan->nama_satuan.' = '.$row->jumlah_transaksi.' '.$row->idItemSatuanKecil->idSatuan->nama_satuan;
						$before=$row->jumlah_sebelum_transaksi;
						$currentMin=0;
						$currentPlus=$row->jumlah_transaksi;
						$after=$row->jumlah_setelah_transaksi;
					}
					
					echo '</td>
					<td>'.$row->idItemTransaksi->waktu_transaksi.'</td>
					<td>';
						echo $before;			
					echo'</td>
					<td>';
						echo $currentPlus;
					echo'
					</td>
					<td>';
						echo $currentMin;
					echo'
					</td>
					<td>';
						echo $after;	
					echo'
					</td>
				</tr>
				';
			}
			?>
		</table>		
		<?php
		}else{
			
		?>
			<h4>Kartu Stok : <?=$item->kode_item;?> - <?=$item->nama_item;?>, Satuan : <?=Item::getParentUnit($item->id_item)->idSatuan->nama_satuan;?></h4>
		<?php
		}
		?>
	</div>
</div>