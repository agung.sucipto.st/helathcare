<?php
$this->breadcrumbs=array(
	'Stok Status',
);
$this->title=array(
	'title'=>'Stok Status',
	'deskripsi'=>'Untuk Memonitor Stok'
);
?>

<div class="panel panel-default">
	<div class="panel-heading">
		<h3 class="panel-title pull-left">Stok Status</h3>
		<div style="clear:both"></div>
	</div>
	<div class="panel-body">
		<?php $form=$this->beginWidget('booster.widgets.TbActiveForm',array(
			'action'=>Yii::app()->createUrl($this->route),
			'method'=>'POST',
			'type'=>'vertical',
		)); ?>

		<div class="row">
			<div class="col-sm-3">
				<?php echo $form->dropDownListGroup($model,'gudang_tujuan', array('label'=>'Gudang','widgetOptions'=>array('data'=>CHtml::listData(Gudang::model()->findAll(),'id_gudang','nama_gudang'), 'htmlOptions'=>array('class'=>'input-large','empty'=>'- Semua -')))); ?>
			</div>
		
			<div class="col-sm-2">
				<br/>
				<?php $this->widget('booster.widgets.TbButton', array(
					'buttonType' => 'submit',
					'context'=>'primary',
					'label'=>'Search',
				)); ?>
			</div>
		</div>

		<?php $this->endWidget(); ?>
		<?php
		if($data!=null){
			$gudang=Gudang::model()->findByPk($warehouse);
		?>
		<h4>Stok Status : <?=(!empty($gudang))?$gudang->nama_gudang:"Semua Gudang";?></h4>
		<hr>
		<table class="table table-bordered">
			<tr>
				<th class="text-center">Kode</th>
				<th class="text-center">Item</th>
				<th class="text-center">Kategori</th>
				<th class="text-center">Grup</th>
				<th class="text-center">Satuan</th>
				<th class="text-center">WACC</th>
				<th class="text-center">Stok</th>
				<th class="text-center">Expire</th>
				<th class="text-center">Total Persediaan (Rp)</th>
			</tr>
			<?php
			$total=0;
			foreach($data as $row){
				
				echo'
				<tr>
					<td>'.$row->idItem->kode_item.'</td>
					<td>'.$row->idItem->nama_item.'</td>
					<td>'.$row->idItem->idItemKategori->nama_item_kategori.'</td>
					<td>'.$row->idItem->idItemGrup->nama_item_grup.'</td>
					<td>'.Item::getParentUnit($row->id_item)->idSatuan->nama_satuan.'</td>
					<td>'.$row->idItem->wac.'</td>
					<td>';
					if(!empty($gudang)){
						echo $row->stok;
						$stock=$row->stok;
					}else{
						echo Item::getStokAll($row->id_item);
						$stock = Item::getStokAll($row->id_item);
					}
					$total+=$row->idItem->wac*$stock;
					echo'</td>
					<td>'.Item::getLastExp($row->id_item)->tanggal_expired.'</td>
					<td class="text-right">'.number_format(($stock*$row->idItem->wac),0).'</td>
					
				</tr>';
			}
			?>
			<tr>
				<th colspan="8" class="text-right">Total</th>
				<th class="text-right"><?=number_format($total,0);?></th>
			</tr>
		</table>		
		<?php
		}
		?>
	</div>
</div>