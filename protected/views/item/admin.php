<?php
$this->breadcrumbs=array(
	'Kelola Item',
);
$this->title=array(
	'title'=>'Kelola Item',
	'deskripsi'=>'Untuk Mengelola Item'
);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	$('.import-form').hide();
	return false;
});
$('.btn-cancel').click(function(){
	$('.import-form').toggle();
	return false;
});
$('.import-button').click(function(){
	$('.import-form').toggle();
	$('.search-form').hide();
	return false;
});
$('.search-form form').submit(function(){
	$.fn.yiiGridView.update('item-grid', {
		data: $(this).serialize()
	});
	return false;
});

");
?>

<div class="panel panel-default">
	<div class="panel-heading">
		<h3 class="panel-title pull-left">Data</h3>
		<a href="<?php echo Yii::app()->createUrl(Yii::app()->controller->id.'/create');?>" class="btn btn-primary btn-xs pull-right" style="display:<?php echo ((Lib::accessBy('Item','create')==true)?"block":"");?>">
			<i class="fa fa-copy"></i>
			<span>Tambah Data</span>
		</a>
		<a href="#" class="btn btn-primary btn-xs pull-right search-button">
			<i class="fa fa-search"></i>
			<span>Search</span>
		</a>
		<a href="#" class="btn btn-primary btn-xs pull-right import-button">
			<i class="fa fa-upload"></i>
			<span>Import</span>
		</a>
		<div style="clear:both"></div>
	</div>
	<div class="panel-body">
		<div class="search-form" style="display:none">
			<?php $this->renderPartial('_search',array(
			'model'=>$model,
		)); ?>
		</div><!-- search-form -->
		
		
		<div class="import-form" style="display:none">
			<div class="xe-widget xe-counter-block" data-count=".num" data-from="0" data-to="99.9" data-suffix="%" data-duration="2">
			<div class="row">
				<div class="col-sm-7">
					<div class="xe-lower">
							
							<?php $form=$this->beginWidget('booster.widgets.TbActiveForm',array(
								'action'=>Yii::app()->createUrl(Yii::app()->controller->id.'/import'),
								'method'=>'POST',
								'htmlOptions'=>array('enctype'=>'multipart/form-data'),
							)); ?>
							<?php echo $form->fileFieldGroup($model, 'import',
									array(
										'wrapperHtmlOptions' => array(
											'class' => 'form-control',
										),
										'widgetOptions'=>array(
											'htmlOptions'=>array(
												'required'=>true,
												'accept'=>".xls"
											),
										),
										'label'=>''
										,
									)
								); ?>								<div class="text-center">
									<div class="col-sm-6">
										
									</div>
									<div class="col-sm-6">
									<button class="btn btn-success btn-icon btn-cancel">
											<i class="fa fa-times"></i>
											<span>Batal</span>
										</button>
										<button class="btn btn-success btn-icon">
											<i class="fa fa-file-o"></i>
											<span>Import</span>
										</button>
									</div>							
								</div>							
							
						<?php $this->endWidget(); ?>					
						</div>				
				</div>
				<div class="col-sm-5">
					<div class="xe-upper">
						<div class="xe-label">
							<!-- Excel Button --> 
							<a href="<?=Yii::app()->theme->baseUrl;?>/assets/upload_obat.xls" class="a-btn-2">
								<span class="a-btn-2-text">Download Template</span> 
							</a>
						</div>
					</div>		
				</div>
			</div>
			</div>
		</div><!-- search-form -->
		<style>
		.pagination li.selected a{
			background:rgb(235, 235, 236);
		}
		</style>
		
		
		<?php
		if(Yii::app()->user->hasFlash('success')){
			echo'
			<div class="alert alert-success">
				<button type="button" class="close" data-dismiss="alert">&times;</button>
				'.Yii::app()->user->getFlash('success').'
			</div>';
		}
		?>
							
		<div id="example-2_wrapper" class="dataTables_wrapper form-inline dt-bootstrap no-footer">
		
		<?php 
		// put this somewhere on top
		$pageSize=Yii::app()->user->getState('pageSize',Yii::app()->params['defaultPageSize']); ?>		
		<?php		$this->widget('booster.widgets.TbExtendedGridView', array(
			'id'=>'item-grid',
			'type' => 'striped',
			'dataProvider' => $model->search(),
			'filter'=>$model,
			'summaryText'=>'Menampilkan {start}-{end} dari {count} hasil.',
			'selectableRows' => 2,
			'responsiveTable' => true,
			'enablePagination' => true,
			'pager' => array(
				'htmlOptions'=>array(
					'class'=>'pagination'
				),
				'maxButtonCount' => 5,
				'cssFile' => true,
				'header' => false,
				'firstPageLabel' => '<<',
				'prevPageLabel' => '<',
				'nextPageLabel' => '>',
				'lastPageLabel' => '>>',
			),
			'columns'=>array(
					array(
						"header"=>"Kategori",
						"name"=>"id_item_kategori",
						"value"=>'$data->idItemKategori->nama_item_kategori',
						"filter"=>CHtml::listData(ItemKategori::model()->findAll(),'id_item_kategori','nama_item_kategori')
					),
					array(
						"header"=>"Grup",
						"name"=>"id_item_grup",
						"value"=>'$data->idItemGrup->nama_item_grup',
						"filter"=>CHtml::listData(ItemGrup::model()->findAll(),'id_item_grup','nama_item_grup')
					),
					'nama_item',
					'wac',
					array(
						"header"=>"Status",
						"value"=>'$data->status',
						"filter"=>array("Aktif"=>"Aktif","Tidak Aktif"=>"Tidak Aktif"),
						"name"=>"status"
					),
					array(
						'class'=>'booster.widgets.TbButtonColumn',
						 'deleteConfirmation'=>'Anda yakin akan menhapus data?',
						'template'=>'{price}{view}{update}{delete}',
						'buttons'=>array
						(
							'price' => array
							(
								'label'=>'Atur Harga',
								'icon'=>'fa fa-money',
								'url'=>'Yii::app()->createUrl("item/price", array("id"=>$data->id_item))',
								'visible'=>'$data->getAllowView()',
								'options'=>array(
									'class'=>'btn btn-default btn-xs',
								),
							),
							'view' => array
							(
								'label'=>'View',
								'icon'=>'search',
								'visible'=>'$data->getAllowView()',
								'options'=>array(
									'class'=>'btn btn-default btn-xs',
								),
							),
							'update' => array
							(
								'label'=>'Update',
								'icon'=>'pencil',
								'visible'=>'$data->getAllowUpdate()',
								'options'=>array(
									'class'=>'btn btn-default btn-xs',
								),
							),
							'delete' => array
							(
								'label'=>'Delete',
								'icon'=>'trash',
								'visible'=>'$data->getAllowDelete($data->id_item)',
								'options'=>array(
									'class'=>'btn btn-default btn-xs delete',
								),
							)
						),
						'header'=>CHtml::dropDownList('pageSize',$pageSize,array(10=>10,20=>20,50=>50,100=>100,200=>200,500=>500,1000=>1000),array(
							'onchange'=>"$.fn.yiiGridView.update('item-grid',{ data:{pageSize: $(this).val() }})",
						)),
					),
			),
		));
		?>
		</div>
	</div>
</div>