<?php $form=$this->beginWidget('booster.widgets.TbActiveForm',array(
	'id'=>'item-form',
	'enableAjaxValidation'=>true,
)); ?>

<p class="help-block">Isian dengan tanda <span class="required">*</span> wajib diisi.</p>
<?php echo $form->errorSummary($model); ?>
<div class="row">
	<div class="col-sm-3">
		<?php echo $form->dropDownListGroup($model,'id_item_kategori', array('widgetOptions'=>array('data'=>CHtml::listData(ItemKategori::model()->findAll(),'id_item_kategori','nama_item_kategori'), 'htmlOptions'=>array('class'=>'input-large','empty'=>'- Choose -')))); ?>
	</div>
	
	<div class="col-sm-3">
		<?php 
		if($model->isNewRecord){
			$data=ItemGrup::model()->findAll();
		}else{
			$data=ItemGrup::model()->findAll(array("condition"=>"id_item_kategori='$model->id_item_kategori'"));
		}
		echo $form->dropDownListGroup($model,'id_item_grup', array('widgetOptions'=>array('data'=>CHtml::listData($data,'id_item_grup','nama_item_grup'), 'htmlOptions'=>array('class'=>'input-large','empty'=>'- Choose -')))); ?>
	</div>
	<div class="col-sm-3">
		<?php echo $form->textFieldGroup($model,'kode_item',array('widgetOptions'=>array('htmlOptions'=>array('class'=>'span5','maxlength'=>100)))); ?>
	</div>
	<div class="col-sm-3">
		<?php echo $form->textFieldGroup($model,'nama_item',array('widgetOptions'=>array('htmlOptions'=>array('class'=>'span5','maxlength'=>100)))); ?>
	</div>
</div>
<div class="row">
	<div class="col-sm-3">
		<?php echo $form->dropDownListGroup($model,'id_principal', array('widgetOptions'=>array('data'=>CHtml::listData(Principal::model()->findAll(),'id_principal','nama_principal'), 'htmlOptions'=>array('class'=>'input-large','empty'=>'- Choose -')))); ?>
	</div>
	<div class="col-sm-3">
		<?php echo $form->textFieldGroup($model,'harga_dasar_satuan_kecil',array('widgetOptions'=>array('htmlOptions'=>array('class'=>'span5','maxlength'=>10)))); ?>
	</div>
	<div class="col-sm-3">
		<?php echo $form->textFieldGroup($model,'zat_aktif',array('widgetOptions'=>array('htmlOptions'=>array('class'=>'span5','maxlength'=>200)))); ?>
	</div>
	<div class="col-sm-3">
		<?php echo $form->dropDownListGroup($model,'status', array('widgetOptions'=>array('data'=>array("Aktif"=>"Aktif","Tidak Aktif"=>"Tidak Aktif"), 'htmlOptions'=>array('class'=>'input-large','empty'=>'- Choose -')))); ?>
	</div>
</div>
<?php
if($model->isNewRecord){
?>
<div class="row">
	<div class="col-sm-6">
		<label>Input Satuan Barang Dari Yang Terkecil *</label>
		<table class="table">
			<tr>
				<th>Isi</th>
				<th>Satuan Besar</th>
				<th>Isi</th>
				<th>Satuan Kecil</th>
				<th>Action</th>
			</tr>
			<tbody id="tab">
			</tbody>
		</table>
		
		<span class="btn btn-xs btn-success" onCLick="getUnit();">Pilih Satuan</span>
		
	</div>
</div>
<?php
}
?>
<hr>
<div class="form-actions">
	<?php $this->widget('booster.widgets.TbButton', array(
			'buttonType'=>'submit',
			'context'=>'primary',
			'label'=>$model->isNewRecord ? 'Tambah' : 'Simpan',
		)); 
		echo CHtml::button('Batal', array(
            'class' => 'btn btn-primary',
            'onclick' => "history.go(-1)",
                )
        );
		?>

		</div>

<?php $this->endWidget(); ?>


<?php
Yii::app()->clientScript->registerScript('validatex', '
$("#Item_id_item_kategori").change(function(){
	var id = $("#Item_id_item_kategori").val();
	getVal(id,"id_item_kategori","#Item_id_item_grup","item/getGroup");
});

function getVal(value,param,id,url){	
	$.ajax({
		url: "'.Yii::app()->createAbsoluteUrl('/').'/"+url,
		cache: false,
		type: "POST",
		data:"Item["+param+"]="+value,
		success: function(msg){
			$(id).html("");
			$(id).html(msg);
		}
	});
}

var idKecil=null;
var namaKecil=null;
var index=0;

function removeRow(id){
	$("#"+id).remove();
}

function setVal(id,name){
	idKecil=id;
	namaKecil=name;
}
function setUnit(id,name){
	var row = $("#tab").find("tr");
	if(row.length == 0){
		idKecil=id;
		namaKecil=name;
	}
	index++;
	$("#tab").append("<tr id=\'row"+id+"\'><td>1<input type=\'hidden\' name=\'Item[SatuanID]["+index+"]\' value=\'"+id+"\' class=\'form-control\'/></td><td>"+name+"</td><td><input type=\'number\' name=\'Item[Satuan]["+index+"]\' value=\'1\' class=\'form-control\'/></td><td>"+namaKecil+"</td><th><span class=\'btn btn-xs btn-danger\' onClick=\'removeRow(\"row"+id+"\")\'><i class=\'fa fa-times\'></i></span></th></tr>");
}

function getUnit(){
	var left = (screen.width/2)-(500/2);
	var top = (screen.height/2)-(400/2);
	window.open("'.Yii::app()->createUrl('satuan/getUnit').'","","width=500,height=400,top="+top+",left="+left);
}
', CClientScript::POS_END);
?>
