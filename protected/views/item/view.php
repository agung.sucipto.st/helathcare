<?php
$this->breadcrumbs=array(
	'Kelola Item'=>array('item/index'),
	'Detail Item',
);

$this->title=array(
	'title'=>'Detail Item',
	'deskripsi'=>'Untuk Melihat Detail Item'
);
?>

<div class="panel panel-default">
	<div class="panel-heading">
		<h3 class="panel-title pull-left">Detail</h3>
		<a href="<?php echo Yii::app()->createUrl(Yii::app()->controller->id);?>" class="btn btn-primary btn-xs pull-right" style="display:<?php echo $visibleEdit;?>">
			<i class="fa fa-arrow-circle-left"></i>
			<span>Kembali</span>
		</a>
		<a href="<?php echo Yii::app()->createUrl(Yii::app()->controller->id.'/create');?>" class="btn btn-primary btn-xs pull-right" style="display:<?php echo ((Lib::accessBy('Item','create')==true)?"block":"");?>">
			<i class="fa fa-copy"></i>
			<span>Tambah Data</span>
		</a>
		<a href="<?php echo Yii::app()->createUrl(Yii::app()->controller->id.'/update/id/'.$_GET['id']);?>" class="btn btn-primary btn-xs pull-right" style="display:<?php echo ((Lib::accessBy('Item','update')==true)?"block":"");?>">
			<i class="fa fa-edit"></i>
			<span>Edit Data</span>
		</a>
		<div style="clear:both"></div>
	</div>
	<div class="panel-body">		
		<div id="example-2_wrapper" class="dataTables_wrapper form-inline dt-bootstrap no-footer">
		
			<h1><?=$model->nama_item;?> - <?=$model->kode_item;?></h1>
			<div class="row">
				<div class="col-md-6">
					<?php $this->widget('booster.widgets.TbDetailView',array(
					'data'=>$model,
					'attributes'=>array(
						array(
							"label"=>"Principal",
							"value"=>$model->idPrincipal->nama_principal
						),
						array(
							"label"=>"Kategori",
							"value"=>$model->idItemKategori->nama_item_kategori
						),
						array(
							"label"=>"Grup",
							"value"=>$model->idItemGrup->nama_item_grup
						),
						'kode_item',
						'nama_item',
						'harga_dasar_satuan_kecil',
						'status',
						'zat_aktif',
						'wac',
					),
					)); ?>
				</div>
				<div class="col-md-6">
					<button class="btn btn-xs btn-primary" data-toggle="tooltip" data-original-title="Tambah Satuan" onclick="addUnit('<?=$model->id_item;?>');"><i class="fa fa-plus"></i> Tambah Satuan</button>
					<table class="table">
						<tr>
							<th>Isi</th>
							<th>Satuan Besar</th>
							<th>Isi</th>
							<th>Satuan Kecil</th>
							<th>Status</th>
							<th>Action</th>
						</tr>
						<tbody id="tab">
							<?php
							$unit = ItemSatuan::model()->findAll(array("condition"=>"id_item='$model->id_item'","order"=>"id_item_satuan ASC"));
							foreach($unit as $row){
								echo'
								<tr>
									<td>1</td>
									<td>'.$row->idSatuan->alias_satuan.'</td>
									<td>'.$row->nilai_satuan_konversi.'</td>
									<td>'.(($row->parent_id_item_satuan==null)?$row->idSatuan->alias_satuan:$row->parentIdItemSatuan->idSatuan->alias_satuan).'</td>
									<td>'.$row->status.'</td>
									<td>';
									$cek=DaftarItemTransaksi::model()->count(array("condition"=>"id_item_satuan_kecil='$row->id_item_satuan' OR id_item_satuan_besar='$row->id_item_satuan'"));
									if($cek==0){
									echo'<button class="btn btn-xs btn-success" data-toggle="tooltip" data-original-title="Ubah Satuan" onclick="changeUnit(\''.$row->id_item_satuan.'\');"><i class="fa fa-edit"></i></button>';
									if(count($unit)!=1){
									echo' <a href="'.Yii::app()->createUrl('satuan/deleteUnit',array("id"=>$row->id_item_satuan)).'" class="btn btn-xs btn-danger" data-toggle="tooltip" data-original-title="Hapus Satuan" onclick="return confirm(\'Apakah Yakin akan menghapus satuan?\');"><i class="fa fa-trash"></i></a>';
									}
									}
									echo'
									</td>
								</tr>
								';
							}
							?>
						</tbody>
					</table>
				</div>
			</div>
		</div>
	</div>
</div>

<?php
Yii::app()->clientScript->registerScript('validatex', '
function change(){
	location.reload();
}
function changeUnit(id){
	var left = (screen.width/2)-(500/2);
	var top = (screen.height/2)-(400/2);
	window.open("'.Yii::app()->createUrl('satuan/changeUnit').'/id/"+id,"","width=500,height=400,top="+top+",left="+left);
}

function addUnit(id){
	var left = (screen.width/2)-(500/2);
	var top = (screen.height/2)-(400/2);
	window.open("'.Yii::app()->createUrl('satuan/addUnit').'/id/"+id,"","width=500,height=400,top="+top+",left="+left);
}
', CClientScript::POS_END);
?>