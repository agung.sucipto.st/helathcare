<?php
$this->breadcrumbs=array(
	'Kelola Item'=>array('item/index'),
	'Detail Item',
);

$this->title=array(
	'title'=>'Detail Item',
	'deskripsi'=>'Untuk Melihat Detail Item'
);
?>

<div class="panel panel-default">
	<div class="panel-heading">
		<h3 class="panel-title pull-left">Detail</h3>
		<a href="<?php echo Yii::app()->createUrl(Yii::app()->controller->id);?>" class="btn btn-primary btn-xs pull-right" style="display:<?php echo $visibleEdit;?>">
			<i class="fa fa-arrow-circle-left"></i>
			<span>Kembali</span>
		</a>
		<a href="<?php echo Yii::app()->createUrl(Yii::app()->controller->id.'/create');?>" class="btn btn-primary btn-xs pull-right" style="display:<?php echo ((Lib::accessBy('Item','create')==true)?"block":"");?>">
			<i class="fa fa-copy"></i>
			<span>Tambah Data</span>
		</a>
		<a href="<?php echo Yii::app()->createUrl(Yii::app()->controller->id.'/update/id/'.$_GET['id']);?>" class="btn btn-primary btn-xs pull-right" style="display:<?php echo ((Lib::accessBy('Item','update')==true)?"block":"");?>">
			<i class="fa fa-edit"></i>
			<span>Edit Data</span>
		</a>
		<div style="clear:both"></div>
	</div>
	<div class="panel-body">		
		<div id="example-2_wrapper" class="dataTables_wrapper form-inline dt-bootstrap no-footer">
		
			<h1><?=$model->nama_item;?> - <?=$model->kode_item;?></h1>
			<div class="row">
				<div class="col-md-4">
					<?php $this->widget('booster.widgets.TbDetailView',array(
					'data'=>$model,
					'attributes'=>array(
						array(
							"label"=>"Principal",
							"value"=>$model->idPrincipal->nama_principal
						),
						array(
							"label"=>"Kategori",
							"value"=>$model->idItemKategori->nama_item_kategori
						),
						array(
							"label"=>"Grup",
							"value"=>$model->idItemGrup->nama_item_grup
						),
						'kode_item',
						'nama_item',
						'harga_dasar_satuan_kecil',
						'status',
						'zat_aktif',
						'wac',
					),
					)); ?>
				</div>
				<div class="col-md-8">
					<select id="grup" class="form-control">
						<?php
							$x=KelompokTarif::model()->findAll(array("condition"=>"status='Aktif'"));
							
							foreach($x as $row){
								if($grup->id_kelompok_tarif==$row->id_kelompok_tarif) {
									echo'<option value="'.$row->id_kelompok_tarif.'" selected>'.$row->nama_kelompok_tarif.'</option>';
								} else {
									echo'<option value="'.$row->id_kelompok_tarif.'">'.$row->nama_kelompok_tarif.'</option>';
								}
							}
						?>
					</select>
					<hr>
					<form method="POST">
					<table class="table table-bordered table-striped">
						<tr>
							<th>Kelas</th>
							<th>Harga Dasar</th>
							<th>Margin(%)</th>
							<th>PPN(%)</th>
							<th>Tarif</th>
						</tr>
						<?php
							$kelas=Kelas::model()->findAll();
							foreach($kelas as $row) {
								$tarif=TarifItem::model()->find(
									array("condition" => "id_item='$model->id_item' AND id_kelompok_tarif='$grup->id_kelompok_tarif' AND id_kelas='$row->id_kelas'")
								);
								echo'
								<tr>
									<td>'.$row->nama_kelas.'</td>
									<td>
										<input type="number" name="Price['.$row->id_kelas.'][harga_dasar]" value="'.$tarif->harga_dasar.'" class="form-control"/>
									</td>
									<td>
										<input type="number" name="Price['.$row->id_kelas.'][margin_persen]" value="'.number_format($tarif->margin_persen,2).'" class="form-control" required required min="1" max="100" step="0.01" />
									</td>
									<td>
										<input type="number" name="Price['.$row->id_kelas.'][ppn]" value="'.$tarif->ppn_persen.'" class="form-control" readonly/>
									</td>
									<td>
										<input type="number" name="Price['.$row->id_kelas.'][tarif]" value="'.$tarif->tarif.'" class="form-control" readonly/>
									</td>
								</tr>
								';
							}
						?>
					</table>
					<hr>
					<button class="btn btn-primary">
						Simpan
					</button>
					</form>
				</div>
			</div>
		</div>
	</div>
</div>

<?php
Yii::app()->clientScript->registerScript('validatexaaa', '
	$("#grup").change(function(){
	var id = $("#grup").val();
	window.location = "'.Yii::app()->createUrl('item/price', array("id"=>$model->id_item)).'?idGrup="+id;
});
', CClientScript::POS_END);
?>