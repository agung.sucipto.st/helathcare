<?php
$this->breadcrumbs=array(
	'Akses Menu',
);
$this->title=array(
	'title'=>'Kelola Akses Menu',
	'deskripsi'=>'Untuk Mengelola Akses Menu'
);

?>


<div class="panel panel-default">
	<div class="panel-heading">
		<h3 class="panel-title pull-left">Data</h3>
		<a href="<?php echo Yii::app()->createUrl(Yii::app()->controller->module->id.'/'.Yii::app()->controller->id.'/create');?>" class="btn btn-primary btn-xs pull-right">
			<i class="fa fa-copy"></i>
			<span>Tambah Header Menu</span>
		</a>
		<div style="clear:both"></div>
	</div>
	<div class="panel-body">
		<style>
		.pagination li.selected a{
			background:rgb(235, 235, 236);
		}
		</style>
		<?php
		if(Yii::app()->user->hasFlash('berhasil')){
		echo'
		<div class="alert alert-success">
			<button type="button" class="close" data-dismiss="alert">&times;</button>
			<i class="fa fa-warning"></i> '.Yii::app()->user->getFlash('berhasil').'
		</div>';
		}
		?>
		
		<div class="col-lg-3">		
			<div class="panel panel-default">
				<div class="panel-heading">
				Menu Akses
				</div>
				<div class="panel-body">
				<style>
				.checkbox, .radio {
					padding-left: 30px;
				}
				</style>
				<form action="" method="POST">
					<?php
					$arrayIn=array();
					$in=AccessMenuArrange::model()->findAll(array("condition"=>"access_id!=''"));
					foreach($in as $d){
						$arrayIn[]=$d->access_id;
					}
					if(!empty($arrayIn)){
						$menu = Access::model()->findAll(array('condition'=>"access_visible='1' and access_id NOT IN (".implode(",",$arrayIn).")"));
					}else{
						$menu = Access::model()->findAll(array('condition'=>"access_visible='1'"));
					}
					
					foreach($menu as $r){
						echo'<div class="checkbox">
							<label><input type="checkbox" value="'.$r->access_id.'" name="menu[]" >'.$r->access_name.'</label>
						</div>';
					}
					?>
					
					<button class="btn btn-primary pull-right" type="submit" name="defaultMenu">Tambahkan Ke Menu</button>
				</form>

				</div>
			</div>
		</div>
		<div class="col-lg-9">
			<select id="menuurl" class="form-control">
				<?php
					$group=AccessMenuGroup::model()->findAll();
					foreach($group as $row){
						if($row->menu_group_id==$id){
							echo'<option value="'.$row->menu_group_id.'" selected>'.$row->menu_group_name.'</option>';
						}else{
							echo'<option value="'.$row->menu_group_id.'">'.$row->menu_group_name.'</option>';
						}
					}
				?>
			</select>
			<hr>
			<div class="dd" id="nestable_list_1">
				<?php
				$menu_items = AccessMenuArrange::model()->findAll(array("condition"=>"menu_group_id='$id'","order"=>"menu_arrange_order ASC"));
				$data = Lib::MenuManager($menu_items);
				echo $data;
				?>
			</div>
			<form class="form" id="user-form" action="#" method="post">		
				<input type="hidden" id="nestable_list_1_output" name="AccessMenuArrange[save]" class="form-control col-md-12 margin-bottom-10" value="">
				<div class="form-actions">
				<hr>
				<br/>
				<br/>
					<button class="btn btn-primary" id="yw0" type="submit" name="yt0">Simpan Perubahan</button>
				</div>
			</form>	
		</div>
	</div>
</div>

<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->theme->baseUrl; ?>/assets/js/jquery-nestable/jquery.nestable.css"/>
<script src="<?php echo Yii::app()->theme->baseUrl; ?>/assets/js/jquery-nestable/jquery.nestable.js"></script>
<script src="<?php echo Yii::app()->theme->baseUrl; ?>/assets/js/jquery-nestable/ui-nestable.js"></script>

<script>
$(document).ready(function(){
	UINestable.init();
	$("#menuurl").change(function() {	
		var url=$("#menuurl").val();
		window.location='<?php echo Yii::app()->createUrl(Yii::app()->controller->id.'/index/id/');?>/'+url;
	});
});
</script>