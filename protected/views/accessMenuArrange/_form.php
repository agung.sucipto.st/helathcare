<?php $form=$this->beginWidget('booster.widgets.TbActiveForm',array(
	'id'=>'access-menu-arrange-form',
	'enableAjaxValidation'=>true,
)); ?>

<p class="help-block">Isian dengan tanda <span class="required">*</span> wajib diisi.</p>

<?php echo $form->errorSummary($model); ?>

	<?php echo $form->textFieldGroup($model,'menu_arrange_name',array('widgetOptions'=>array('htmlOptions'=>array('class'=>'span5','maxlength'=>50)))); ?>
	
	<?php echo $form->dropDownListGroup($model,'menu_group_id', array('widgetOptions'=>array('data'=>CHtml::listData(AccessMenuGroup::model()->findAll(),'menu_group_id','menu_group_name'), 'htmlOptions'=>array('class'=>'input-large','empty'=>'- Choose -')))); ?>


<div class="form-actions">
	<?php $this->widget('booster.widgets.TbButton', array(
			'buttonType'=>'submit',
			'context'=>'primary',
			'label'=>$model->isNewRecord ? 'Tambah' : 'Simpan',
		)); 
		echo CHtml::button('Batal', array(
            'class' => 'btn btn-primary',
            'onclick' => "history.go(-1)",
                )
        );
		?>

		</div>

<?php $this->endWidget(); ?>
