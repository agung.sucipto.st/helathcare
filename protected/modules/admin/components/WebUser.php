<?php
class WebUser extends CWebUser
{
		
	public function getAuth()
    {
		$module=Yii::app()->controller->module->id;
		$controller=Yii::app()->controller->id;
		$action=Yii::app()->controller->action->id;
		$akses = UserAccess::model()->find(array("with"=>array("access"),'condition'=>"id_user='".Yii::app()->user->id."' and access.access_module='$module' and access.access_controller='$controller' and access.access_action='$action'"));
		
		if(!empty($akses))
			return true;
		else
			return false;
    }
	
	public function userAccess()
    {
		$module=Yii::app()->controller->module->id;
		$controller=Yii::app()->controller->id;
		$action=Yii::app()->controller->action->id;
		$allow=array();
		$akses = UserAccess::model()->findAll(array("with"=>array("access"),'condition'=>"id_user='".Yii::app()->user->id."' and access.access_visible='1'"));
		$output='';
		foreach($akses as $row){
			$allow[]=$row->access_id;
		}
		
		$parent=array();
		$subparent=array();
		$menus = AccessMenuArrange::model()->findAll(array("condition"=>"menu_arrange_parent='0'","order"=>"menu_arrange_order asc"));
		foreach($menus as $rows){
			$child=AccessMenuArrange::model()->findAll(array("condition"=>"menu_arrange_parent='$rows->menu_arrange_id'","order"=>"menu_arrange_order asc"));
			if(Count($child)>0){
				$parentChecker=0;
				foreach($child as $data){
					$acc=(!empty($data->menu_arrange_name)?"":$data->access_id);
					
					if(in_array($acc,$allow)){
						$parentChecker++;
						$parent[$rows->menu_arrange_id]["status"][]=$data->access->access_controller;
					}
					$subchild=AccessMenuArrange::model()->findAll(array("condition"=>"menu_arrange_parent='$data->menu_arrange_id'","order"=>"menu_arrange_order asc"));
					if(Count($subchild)>0){
						$subparentChecker=0;
						foreach($subchild as $Sdata){
							$acc=(!empty($Sdata->menu_arrange_name)?"":$Sdata->access_id);
							
							if(in_array($acc,$allow)){
								$parentChecker++;
								$parent[$rows->menu_arrange_id]["status"][]=$Sdata->access->access_controller;
							}
						}
					}
				}
				
				if($parentChecker>0){
					$parent[$rows->menu_arrange_id]["parent"]=true;
				}else{
					$parent[$rows->menu_arrange_id]["parent"]=false;
				}
				
				
				if($parent[$rows->menu_arrange_id]["parent"]==true){
					$output.='<li class="treeview '.(in_array(Yii::app()->controller->id,$parent[$rows->menu_arrange_id]["status"])?"active":"").'">
					<a href="#">
						<i class="fa fa-folder-o"></i>
						<span class="title">'.((!empty($rows->menu_arrange_name)?$rows->menu_arrange_name:$rows->access->access_name)).'</span>
						<i class="fa pull-right fa-angle-left"></i>
					</a>
					<ul class="treeview-menu" '.(in_array(Yii::app()->controller->id,$parent[$rows->menu_arrange_id]["status"])?'style="display: block;"':"").'>';
					foreach($child as $xData){
						$subchild=AccessMenuArrange::model()->findAll(array("condition"=>"menu_arrange_parent='$xData->menu_arrange_id'","order"=>"menu_arrange_order asc"));
						if(Count($subchild)>0){
							$subparentChecker=0;
							foreach($subchild as $Sdata){
								$acc=(!empty($Sdata->menu_arrange_name)?"":$Sdata->access_id);
								
								if(in_array($acc,$allow)){
									$subparentChecker++;
									$subparent[$xData->menu_arrange_id]["status"][]=$Sdata->access->access_controller;
								}
							}
							
							if($subparentChecker>0){
								$subparent[$xData->menu_arrange_id]["parent"]=true;
							}else{
								$subparent[$xData->menu_arrange_id]["parent"]=false;
							}
							
							
							if($subparent[$xData->menu_arrange_id]["parent"]==true){
								$output.='<li class="treeview '.(in_array(Yii::app()->controller->id,$subparent[$xData->menu_arrange_id]["status"])?"active":"").'">
								<a href="#">
									<i class="fa fa-folder-o"></i>
									<span class="title">'.((!empty($xData->menu_arrange_name)?$xData->menu_arrange_name:$xData->access->access_name)).'</span>
									<i class="fa pull-right fa-angle-left"></i>
								</a>
								<ul class="treeview-menu" '.(in_array(Yii::app()->controller->id,$subparent[$xData->menu_arrange_id]["status"])?'style="display: block;"':"").'>';
								foreach($subchild as $subData){
									$acc=(!empty($subData->menu_arrange_name)?"":$subData->access_id);
									if(in_array($acc,$allow)){
									$output.='<li '.((Yii::app()->controller->id==$subData->access->access_controller)?'class="active"':'').'>
										<a href="'.((!empty($subData->menu_arrange_name)?"#":Yii::app()->createUrl($subData->access->access_module."/".$subData->access->access_controller."/".$subData->access->access_action))).'">
											<i class="fa fa-angle-right"></i>
											<span class="title">'.((!empty($subData->menu_arrange_name)?$subData->menu_arrange_name:$subData->access->access_name)).'</span>
										</a>
									</li>';
									}
								}
								$output.='</ul></li>';
							}
						}else{
							$acc=(!empty($xData->menu_arrange_name)?"":$xData->access_id);
							if(in_array($acc,$allow)){
							$output.='<li '.((Yii::app()->controller->id==$xData->access->access_controller)?'class="active"':'').'>
								<a href="'.((!empty($xData->menu_arrange_name)?"#":Yii::app()->createUrl($xData->access->access_module."/".$xData->access->access_controller."/".$xData->access->access_action))).'">
									<i class="fa fa-angle-right"></i>
									<span class="title">'.((!empty($xData->menu_arrange_name)?$xData->menu_arrange_name:$xData->access->access_name)).'</span>
								</a>
							</li>';
							}
						}
					}
					$output.='</ul></li>';
				}
				
			}else{
				$acc=(!empty($rows->menu_arrange_name)?"":$rows->access_id);
				if(in_array($acc,$allow)){
				$output.='<li '.((Yii::app()->controller->id==$rows->access->access_controller)?'class="active"':'').'>
					<a href="'.((!empty($rows->menu_arrange_name)?"#":Yii::app()->createUrl($rows->access->access_module."/".$rows->access->access_controller."/".$rows->access->access_action))).'">
						<i class="fa fa-list"></i>
						<span class="title">'.((!empty($rows->menu_arrange_name)?$rows->menu_arrange_name:$rows->access->access_name)).'</span>
					</a>
				</li>';
				}
			}
		}
		return $output;
    }
}
