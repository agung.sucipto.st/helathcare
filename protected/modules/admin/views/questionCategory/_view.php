<div class="view">
		<b><?php echo CHtml::encode($data->getAttributeLabel('qc_id')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->qc_id),array('view','id'=>$data->qc_id)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('qc_name')); ?>:</b>
	<?php echo CHtml::encode($data->qc_name); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('create_at')); ?>:</b>
	<?php echo CHtml::encode($data->create_at); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('modified_at')); ?>:</b>
	<?php echo CHtml::encode($data->modified_at); ?>
	<br />

</div>