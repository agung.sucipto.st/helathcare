<?php
$this->breadcrumbs=array(
	'Kelola Questioner Category'=>array('index'),
	'Detail Questioner Category',
);

$this->title=array(
	'title'=>'Detail Questioner Category',
	'deskripsi'=>'Untuk Melihat Detail Questioner Category'
);
?>

<div class="panel panel-default">
	<div class="panel-heading">
		<h3 class="panel-title pull-left">Detail</h3>
		<a href="<?php echo Yii::app()->createUrl(Yii::app()->controller->module->id.'/'.Yii::app()->controller->id);?>" class="btn btn-primary btn-xs pull-right">
			<i class="fa fa-arrow-circle-left"></i>
			<span>Kembali</span>
		</a>
		<a href="<?php echo Yii::app()->createUrl(Yii::app()->controller->module->id.'/'.Yii::app()->controller->id.'/update/id/'.$_GET['id']);?>" class="btn btn-primary btn-xs pull-right">
			<i class="fa fa-edit"></i>
			<span>Edit Data</span>
		</a>
		<div style="clear:both"></div>
	</div>
	<div class="panel-body">		
		<div id="example-2_wrapper" class="dataTables_wrapper dt-bootstrap no-footer">
			<?php
			if(Yii::app()->user->hasFlash('notification')){
				echo'
				<div class="alert alert-danger">
					<button type="button" class="close" data-dismiss="alert">&times;</button>
					'.Yii::app()->user->getFlash('notification').'
				</div>';
			}
			?>
			<?php $this->widget('booster.widgets.TbDetailView',array(
			'data'=>$data,
			'attributes'=>array(
				'qc_name',
				'create_at',
				'modified_at',
			),
			)); ?>
			
			
			<?php $form=$this->beginWidget('booster.widgets.TbActiveForm',array(
				'id'=>'question-quiz-form',
				'enableAjaxValidation'=>true,
			)); ?>


			<?php echo $form->errorSummary($model); ?>

			<?php echo $form->textFieldGroup($model,'qq_question',array('widgetOptions'=>array('htmlOptions'=>array('class'=>'span5','maxlength'=>200)))); ?>

			<div class="form-actions">
				<?php $this->widget('booster.widgets.TbButton', array(
						'buttonType'=>'submit',
						'context'=>'primary',
						'label'=>$model->isNewRecord ? 'Tambah' : 'Simpan',
					)); 
					?>

					</div>

			<?php $this->endWidget(); ?>
			
			
			
			<?php 
		// put this somewhere on top
		$pageSize=Yii::app()->user->getState('pageSize',Yii::app()->params['defaultPageSize']); ?>		
		<?php		$this->widget('booster.widgets.TbExtendedGridView', array(
			'id'=>'question-quiz-grid',
			'type' => 'striped',
			'dataProvider' => $question->search(),
			'filter'=>$question,
			'summaryText'=>'Menampilkan {start}-{end} dari {count} hasil.',
			'selectableRows' => 2,
			'responsiveTable' => true,
			'enablePagination' => true,
			'pager' => array(
				'htmlOptions'=>array(
					'class'=>'pagination'
				),
				'maxButtonCount' => 5,
				'cssFile' => true,
				'header' => false,
				'firstPageLabel' => '<<',
				'prevPageLabel' => '<',
				'nextPageLabel' => '>',
				'lastPageLabel' => '>>',
			),
			'columns'=>array(
					array(
						"header"=>"Question",
						"type"=>"raw",
						"value"=>function($data){
							$qo=QuestionOption::model()->findAll(array("condition"=>"qq_id='$data->qq_id'"));
							$out='<a href="'.Yii::app()->createUrl(Yii::app()->controller->module->id."/".Yii::app()->controller->id."/addOption",array("id"=>$data->qq_id)).'">Tambah Optional</a><br/>';
							$out.=$data->qq_question.'<br/><ul>';
							if(!empty($qo)){
								foreach($qo as $r){
									$out.='<li>'.$r->qo_name.' &nbsp;<a class="btn btn-xs btn-success" href="'.Yii::app()->createUrl(Yii::app()->controller->module->id."/".Yii::app()->controller->id."/updateOption",array("id"=>$r->qo_id)).'">Update</a> &nbsp; <a class="btn btn-xs btn-danger" href="'.Yii::app()->createUrl(Yii::app()->controller->module->id."/".Yii::app()->controller->id."/deleteOption",array("id"=>$r->qo_id)).'" onClick="return confirm(\'Apakah Yakin akan menghapus data?\');">Delete</a></li>';
								}
							}
							$out.='</ul>';
							return $out;
						}
					),
					'qq_order'
					,
					array(
						'class'=>'booster.widgets.TbButtonColumn',
						 'deleteConfirmation'=>'Anda yakin akan menhapus data?',
						'template'=>'{update}{delete}',
						'buttons'=>array
						(
							'update' => array
							(
								'label'=>'Update',
								'icon'=>'pencil',
								'url'=>'Yii::app()->createUrl(Yii::app()->controller->module->id."/questionQuiz/update",array("id"=>$data->qq_id))',
								'options'=>array(
									'class'=>'btn btn-default btn-xs',
								),
							),
							'delete' => array
							(
								'label'=>'Delete',
								'icon'=>'trash',
								'url'=>'Yii::app()->createUrl(Yii::app()->controller->module->id."/questionQuiz/delete",array("id"=>$data->qq_id))',
								'options'=>array(
									'class'=>'btn btn-default btn-xs delete',
								),
							)
						),
						'header'=>CHtml::dropDownList('pageSize',$pageSize,array(10=>10,20=>20,50=>50,100=>100,200=>200,500=>500,1000=>1000),array(
							'onchange'=>"$.fn.yiiGridView.update('question-quiz-grid',{ data:{pageSize: $(this).val() }})",
						)),
					),
			),
		));
		?>
		</div>
	</div>
</div>
