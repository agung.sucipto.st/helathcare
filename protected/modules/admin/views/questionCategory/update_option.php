<?php
$this->breadcrumbs=array(
	'Kelola Questioner Category'=>array('index'),
	'Perbarui Questioner Option',
);
$this->title=array(
	'title'=>'Tambah Questioner Option',
	'deskripsi'=>'Untuk Perbarui Questioner Option'
);?>

<div class="panel panel-default">
	<div class="panel-heading">
		<h3 class="panel-title pull-left">Form Tambah</h3>
		<a href="<?php echo Yii::app()->createUrl(Yii::app()->controller->module->id.'/'.Yii::app()->controller->id);?>" class="btn btn-primary btn-xs pull-right">
			<i class="fa fa-arrow-circle-left"></i>
			<span>Kembali</span>
		</a>
		<div style="clear:both"></div>
	</div>
	<div class="panel-body">		
		<?php $form=$this->beginWidget('booster.widgets.TbActiveForm',array(
			'id'=>'question-option-form',
			'enableAjaxValidation'=>false,
		)); ?>

		<p class="help-block">Isian dengan tanda <span class="required">*</span> wajib diisi.</p>

		<?php echo $form->errorSummary($model); ?>

			<?php echo $form->textFieldGroup($model,'qo_name',array('widgetOptions'=>array('htmlOptions'=>array('class'=>'span5','maxlength'=>100)))); ?>
			<?php echo $form->textFieldGroup($model,'qo_alert',array('widgetOptions'=>array('htmlOptions'=>array('class'=>'span5','maxlength'=>100)))); ?>
		<div class="form-actions">
			<?php $this->widget('booster.widgets.TbButton', array(
					'buttonType'=>'submit',
					'context'=>'primary',
					'label'=>$model->isNewRecord ? 'Tambah' : 'Simpan',
				)); 
				echo CHtml::button('Batal', array(
					'class' => 'btn btn-primary',
					'onclick' => "history.go(-1)",
						)
				);
				?>

				</div>

		<?php $this->endWidget(); ?>

	</div>
</div>