<div class="view">
		<b><?php echo CHtml::encode($data->getAttributeLabel('hd_cat_id')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->hd_cat_id),array('view','id'=>$data->hd_cat_id)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('hd_cat_name')); ?>:</b>
	<?php echo CHtml::encode($data->hd_cat_name); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('hd_cat_description')); ?>:</b>
	<?php echo CHtml::encode($data->hd_cat_description); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('hd_cat_parent')); ?>:</b>
	<?php echo CHtml::encode($data->hd_cat_parent); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('create_at')); ?>:</b>
	<?php echo CHtml::encode($data->create_at); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('modified_at')); ?>:</b>
	<?php echo CHtml::encode($data->modified_at); ?>
	<br />

</div>