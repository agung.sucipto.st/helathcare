<div class="view">
		<b><?php echo CHtml::encode($data->getAttributeLabel('gmk_id')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->gmk_id),array('view','id'=>$data->gmk_id)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('gmk_name')); ?>:</b>
	<?php echo CHtml::encode($data->gmk_name); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('gmk_description')); ?>:</b>
	<?php echo CHtml::encode($data->gmk_description); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('create_at')); ?>:</b>
	<?php echo CHtml::encode($data->create_at); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('modified_at')); ?>:</b>
	<?php echo CHtml::encode($data->modified_at); ?>
	<br />

</div>