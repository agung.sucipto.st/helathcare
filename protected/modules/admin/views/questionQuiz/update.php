<?php
$this->breadcrumbs=array(
	'Perbarui Question Quiz',
);

$this->title=array(
	'title'=>'Perbarui Question Quiz',
	'deskripsi'=>'Untuk Memperbarui Question Qui'
);?>

<div class="panel panel-default">
	<div class="panel-heading">
		<h3 class="panel-title pull-left">Form Perbarui</h3>
		<a href="#" onclick="self.history.back();" class="btn btn-primary btn-xs pull-right">
			<i class="fa fa-arrow-circle-left"></i>
			<span>Kembali</span>
		</a>
		<div style="clear:both"></div>
	</div>
	<div class="panel-body">		
		<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>	</div>
</div>