<div class="view">
		<b><?php echo CHtml::encode($data->getAttributeLabel('qq_id')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->qq_id),array('view','id'=>$data->qq_id)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('qc_id')); ?>:</b>
	<?php echo CHtml::encode($data->qc_id); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('qq_question')); ?>:</b>
	<?php echo CHtml::encode($data->qq_question); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('create_at')); ?>:</b>
	<?php echo CHtml::encode($data->create_at); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('modified_at')); ?>:</b>
	<?php echo CHtml::encode($data->modified_at); ?>
	<br />

</div>