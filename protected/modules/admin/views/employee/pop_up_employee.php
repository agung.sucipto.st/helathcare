<script language="javascript">
function selectTextPopUp(value){
	window.opener.selectText(value);
	window.self.close();
}
</script>
<br/>
<div class="container">
	<div class="panel panel-default">
		<div class="panel-title">	
			
		</div>
		<div class="panel-body">	
		<?php $this->widget('booster.widgets.TbGridView',array(
			'id'=>'employee-grid',
			'dataProvider'=>$model->search(),
			'filter'=>$model,
			'columns'=>array(
					'emp_id',
					array(
						"header"=>$model->getAttributeLabel("org_id"),
						"value"=>'$data->org->org_name',
						"name"=>"org_id",
						"filter"=>CHtml::listData(Organization::Model()->findAll(),"org_id","org_name")
					),
					'emp_full_name',
					array(
						'header'=>CHtml::dropDownList('pageSize',$pageSize,array(10=>10,20=>20,50=>50,100=>100,200=>200,500=>500,1000=>1000),array(
							'onchange'=>"$.fn.yiiGridView.update('employee-grid',{ data:{pageSize: $(this).val() }})",
						)),
						'type'=>'raw',
						'value' => 'CHtml::link(\'<b class="btn btn-success "><i class="fa fa-check"></i> Pilih</b> \',"javascript: selectTextPopUp(\'$data->emp_id\')") ',
					),
			),
		)); ?>
		</div>
	</div>
</div>