<div class="view">
		<b><?php echo CHtml::encode($data->getAttributeLabel('emp_id')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->emp_id),array('view','id'=>$data->emp_id)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('att_id')); ?>:</b>
	<?php echo CHtml::encode($data->att_id); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('org_id')); ?>:</b>
	<?php echo CHtml::encode($data->org_id); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('emp_identity_type')); ?>:</b>
	<?php echo CHtml::encode($data->emp_identity_type); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('emp_identity_number')); ?>:</b>
	<?php echo CHtml::encode($data->emp_identity_number); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('emp_full_name')); ?>:</b>
	<?php echo CHtml::encode($data->emp_full_name); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('emp_nick_name')); ?>:</b>
	<?php echo CHtml::encode($data->emp_nick_name); ?>
	<br />

	<?php /*
	<b><?php echo CHtml::encode($data->getAttributeLabel('emp_gender')); ?>:</b>
	<?php echo CHtml::encode($data->emp_gender); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('emp_place_of_birth')); ?>:</b>
	<?php echo CHtml::encode($data->emp_place_of_birth); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('emp_date_of_birth')); ?>:</b>
	<?php echo CHtml::encode($data->emp_date_of_birth); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('emp_rel_id')); ?>:</b>
	<?php echo CHtml::encode($data->emp_rel_id); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('emp_prov_id')); ?>:</b>
	<?php echo CHtml::encode($data->emp_prov_id); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('emp_city_id')); ?>:</b>
	<?php echo CHtml::encode($data->emp_city_id); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('emp_domicile_address')); ?>:</b>
	<?php echo CHtml::encode($data->emp_domicile_address); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('emp_address')); ?>:</b>
	<?php echo CHtml::encode($data->emp_address); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('emp_phone_number')); ?>:</b>
	<?php echo CHtml::encode($data->emp_phone_number); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('emp_email')); ?>:</b>
	<?php echo CHtml::encode($data->emp_email); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('emp_type')); ?>:</b>
	<?php echo CHtml::encode($data->emp_type); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('emp_status')); ?>:</b>
	<?php echo CHtml::encode($data->emp_status); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('emp_photo')); ?>:</b>
	<?php echo CHtml::encode($data->emp_photo); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('emp_join')); ?>:</b>
	<?php echo CHtml::encode($data->emp_join); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('emp_create')); ?>:</b>
	<?php echo CHtml::encode($data->emp_create); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('emp_update')); ?>:</b>
	<?php echo CHtml::encode($data->emp_update); ?>
	<br />

	*/ ?>
</div>