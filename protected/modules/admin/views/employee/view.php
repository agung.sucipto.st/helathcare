<?php
$this->breadcrumbs=array(
	'Kelola Employee'=>array('index'),
	'Detail Employee',
);

$this->title=array(
	'title'=>'Detail Employee',
	'deskripsi'=>'Untuk Melihat Detail Employee'
);
?>

<div class="panel panel-default">
	<div class="panel-heading">
		<h3 class="panel-title pull-left">Detail</h3>
		<a href="<?php echo Yii::app()->createUrl(Yii::app()->controller->module->id.'/'.Yii::app()->controller->id.'/create');?>" class="btn btn-primary btn-xs pull-right">
			<i class="fa fa-plus"></i>
			<span>Tambah</span>
		</a>
		
		<a href="<?php echo Yii::app()->createUrl(Yii::app()->controller->module->id.'/'.Yii::app()->controller->id.'/update',array("id"=>$model->emp_id));?>" class="btn btn-primary btn-xs pull-right">
			<i class="fa fa-pencil"></i>
			<span>Edit</span>
		</a>
		
		<a href="<?php echo Yii::app()->createUrl(Yii::app()->controller->module->id.'/'.Yii::app()->controller->id);?>" class="btn btn-primary btn-xs pull-right">
			<i class="fa fa-arrow-circle-left"></i>
			<span>Kembali</span>
		</a>
		<div style="clear:both"></div>
	</div>
	<div class="panel-body">		
		<div id="example-2_wrapper" class="dataTables_wrapper form-inline dt-bootstrap no-footer">
			<div class="row">
				<div class="col-md-8">
				<?php $this->widget('booster.widgets.TbDetailView',array(
				'data'=>$model,
				'attributes'=>array(
								'emp_id',
					'att_id',
					'org_id',
					'emp_identity_type',
					'emp_identity_number',
					'emp_full_name',
					'emp_nick_name',
					'emp_gender',
					'emp_place_of_birth',
					'emp_date_of_birth',
					'emp_rel_id',
					'emp_prov_id',
					'emp_city_id',
					'emp_domicile_address',
					'emp_address',
					'emp_phone_number',
					'emp_email',
					'emp_type',
					'emp_group',
					'emp_last_education',
					'emp_status',
					'emp_join',
				),
				)); ?>
				</div>
				<div class="col-md-4">
					<?php
					if($model->emp_photo!=""){
							echo '<img src="'.Yii::app()->baseUrl.'/Upload/'.$model->emp_id.'/'.$model->emp_photo.'" class="img-responsive">';
						}else{
							if($model->emp_gender=="Male"){
								echo '<img src="'.Yii::app()->theme->baseUrl.'/assets/images/male.png" class="img-responsive">';
							}else{
								echo '<img src="'.Yii::app()->theme->baseUrl.'/assets/images/female.png" class="img-responsive">';
							}
						}
					?>
				</div>
		</div>
	</div>
</div>
