<?php
$this->breadcrumbs=array(
	'Kelola Employee'=>array('index'),
	'Detail Employee',
);

$this->title=array(
	'title'=>'Detail Employee',
	'deskripsi'=>'Untuk Melihat Detail Employee'
);
?>

<div class="panel panel-default">
	<div class="panel-heading">
		<h3 class="panel-title pull-left">Detail</h3>
		
		<a href="<?php echo Yii::app()->createUrl(Yii::app()->controller->module->id.'/'.Yii::app()->controller->id);?>" class="btn btn-primary btn-xs pull-right">
			<i class="fa fa-arrow-circle-left"></i>
			<span>Kembali</span>
		</a>
		<div style="clear:both"></div>
	</div>
	<div class="panel-body">	
		<h4>	
		Kepegawaian Berdasarkan Umur dan Cara Pembayaran
		</h4>
		<table class="table table-bordered">
			<tr>
				<th rowspan="3" colspan="2">Tenaga Kerja</th>
				<th rowspan="3">Kelompok Umur</th>
				<th colspan="6">Hubungan Kerja</th>
				<th rowspan="3">Jumlah</th>
			</tr>
			<tr>
				<th colspan="3">Tetap</th>
				<th colspan="3">Tidak Tetap</th>
			</tr>
			<tr>
				<th>CPUH</th>
				<th>CPUBR</th>
				<th>CPUBL</th>
				<th>CPUH</th>
				<th>CPUBR</th>
				<th>CPUBL</th>
			</tr>
			<tr>
				<td rowspan="6">WNI</td>
				<td rowspan="3">Laki Laki</td>
				<td>> 18</td>
				<td></td>
				<td></td>
				<td></td>
				<td></td>
				<td></td>
				<td>
				<?php
				$allEmp=0;
				$a=0;
				$b=0;
				$c=0;
				$emp=Employee::model()->findAll(array("condition"=>"emp_gender='Male'"));
				foreach($emp as $row){
					$umur=Lib::Umur($row->emp_date_of_birth);
					if($umur>18){
						$a++;
					}elseif($umur<=18 and $umur>=15){
						$b++;
					}elseif($umur<15){
						$c++;
					}
				}
				echo $a;
				?>
				</td>
				<td></td>
			</tr>
			<tr>
				<td>> 15 s.d 18</td>
				<td></td>
				<td></td>
				<td></td>
				<td></td>
				<td></td>
				<td>
				<?php
				echo $b;
				?>
				</td>
				<td></td>
			</tr>
			<tr>
				<td>< 15</td>
				<td></td>
				<td></td>
				<td></td>
				<td></td>
				<td></td>
				<td>
				<?php
				echo $c;
				?>
				</td>
				<td></td>
			</tr>
			<tr>
				<td rowspan="3">Wanita</td>
				<td>> 18</td>
				<td></td>
				<td></td>
				<td></td>
				<td></td>
				<td></td>
				<td>
				<?php
				$d=0;
				$e=0;
				$f=0;
				$emp=Employee::model()->findAll(array("condition"=>"emp_gender='Female'"));
				foreach($emp as $row){
					$umur=Lib::Umur($row->emp_date_of_birth);
					if($umur>18){
						$d++;
					}elseif($umur<=18 and $umur>=15){
						$e++;
					}elseif($umur<15){
						$f++;
					}
				}
				echo $d;
				?>
				</td>
				<td></td>
			</tr>
			<tr>
				<td>> 15 s.d 18</td>
				<td></td>
				<td></td>
				<td></td>
				<td></td>
				<td></td>
				<td>
				<?php
				echo $e;
				?>
				</td>
				<td></td>
			</tr>
			<tr>
				<td>< 15</td>
				<td></td>
				<td></td>
				<td></td>
				<td></td>
				<td></td>
				<td>
				<?php
				echo $f;
				?>
				</td>
				<td></td>
			</tr>
			<tr>
				<td colspan="8">Jumlah</td>
				<td>
				<?php
				echo ($a+$b+$c+$d+$e+$f+$g);
				?>
				</td>
				<td></td>
			</tr>
		</table>
	</div>
</div>