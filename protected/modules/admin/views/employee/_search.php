<?php $form=$this->beginWidget('booster.widgets.TbActiveForm',array(
	'action'=>Yii::app()->createUrl($this->route),
	'method'=>'get',
)); ?>

		<?php echo $form->textFieldGroup($model,'emp_id',array('widgetOptions'=>array('htmlOptions'=>array('class'=>'span5','maxlength'=>20)))); ?>

		<?php echo $form->dropDownListGroup($model,'org_id', array('widgetOptions'=>array('data'=>CHtml::listData(Organization::Model()->findAll(),"org_id","org_name"), 'htmlOptions'=>array('class'=>'input-large','empty'=>'- Organization -')))); ?>

		<?php echo $form->textFieldGroup($model,'emp_full_name',array('widgetOptions'=>array('htmlOptions'=>array('class'=>'span5','maxlength'=>50)))); ?>

		<?php echo $form->dropDownListGroup($model,'emp_status', array('widgetOptions'=>array('data'=>array("Active"=>"Active","Inactive"=>"Inactive",), 'htmlOptions'=>array('class'=>'input-large')))); ?>


	<div class="form-actions">
		<?php $this->widget('booster.widgets.TbButton', array(
			'buttonType' => 'submit',
			'context'=>'primary',
			'label'=>'Search',
		)); ?>
	</div>

<?php $this->endWidget(); ?>
