<?php $form=$this->beginWidget('booster.widgets.TbActiveForm',array(
	'id'=>'employee-form',
	'enableAjaxValidation'=>true,
	'htmlOptions'=>array('enctype'=>'multipart/form-data'),
)); ?>

<p class="help-block">Isian dengan tanda <span class="required">*</span> wajib diisi.</p>

<?php echo $form->errorSummary($model); ?>
	<div class="row">
		<div class="col-md-4">
			<?php if($model->isNewRecord){echo $form->textFieldGroup($model,'emp_id',array('widgetOptions'=>array('htmlOptions'=>array('class'=>'span5','maxlength'=>20)))); }else{
				echo $form->textFieldGroup($model,'emp_id',array('widgetOptions'=>array('htmlOptions'=>array('class'=>'span5','maxlength'=>20,'disabled'=>'disabled')))); 
			}?>
		</div>
		<div class="col-md-4">
			<?php echo $form->textFieldGroup($model,'att_id',array('widgetOptions'=>array('htmlOptions'=>array('class'=>'span5','maxlength'=>8)))); ?>
		</div>
		<div class="col-md-4">
			
			
			<?php
			$org=new Organization;
			$parent=Organization::model()->findAll();
				foreach($parent as $row){
					$data[$row->org_parent_id][] = $row;
				}
			?>
			<?php echo $form->dropDownListGroup($model,'org_id', array('widgetOptions'=>array('data'=>$org->getParent($data,0,1), 'htmlOptions'=>array('class'=>'input-large','empty'=>'- Organization -')))); ?>
		</div>
	</div>
	
	<div class="row">
		<div class="col-md-4">
			<?php echo $form->dropDownListGroup($model,'emp_identity_type', array('widgetOptions'=>array('data'=>array("KTP"=>"KTP","Passport"=>"Passport","SIM"=>"SIM",), 'htmlOptions'=>array('class'=>'input-large')))); ?>
		</div>
		<div class="col-md-8">
			<?php echo $form->textFieldGroup($model,'emp_identity_number',array('widgetOptions'=>array('htmlOptions'=>array('class'=>'span5','maxlength'=>50)))); ?>
		</div>
	</div>
	
	<div class="row">
		<div class="col-md-3">
			<?php echo $form->textFieldGroup($model,'emp_full_name',array('widgetOptions'=>array('htmlOptions'=>array('class'=>'span5','maxlength'=>50)))); ?>

		</div>
		<div class="col-md-3">
			<?php echo $form->textFieldGroup($model,'emp_nick_name',array('widgetOptions'=>array('htmlOptions'=>array('class'=>'span5','maxlength'=>50)))); ?>

		</div>
		<div class="col-md-3">
			<?php echo $form->dropDownListGroup($model,'emp_gender', array('widgetOptions'=>array('data'=>array("Male"=>"Male","Female"=>"Female",), 'htmlOptions'=>array('class'=>'input-large')))); ?>
		</div>
		<div class="col-md-3">
			<?php echo $form->dropDownListGroup($model,'emp_last_education', array('widgetOptions'=>array('data'=>array("SD"=>"SD","SLTP"=>"SLTP","SLTA"=>"SLTA","D1"=>"D1","D2"=>"D2","D3"=>"D3","S1"=>"S1","S2"=>"S2","S3"=>"S3","Tidak Sekolah"=>"Tidak Sekolah"), 'htmlOptions'=>array('class'=>'input-large')))); ?>
		</div>
	</div>

	<div class="row">
		<div class="col-md-4">
			<?php echo $form->textFieldGroup($model,'emp_place_of_birth',array('widgetOptions'=>array('htmlOptions'=>array('class'=>'span5','maxlength'=>100)))); ?>
		</div>
		<div class="col-md-8">
			<?php echo $form->datePickerGroup($model,'emp_date_of_birth',array('widgetOptions'=>array('options'=>array('format'=>'yyyy-mm-dd','viewFormat'=>'yyyy-mm-dd'),'htmlOptions'=>array('class'=>'span5')), 'prepend'=>'<i class="glyphicon glyphicon-calendar"></i>', 'append'=>'Klik Bulan/Tahun untuk merubah Bulan/Tahun.')); ?>
		</div>
	</div>
	
	<div class="row">
		<div class="col-md-4">
			<?php echo $form->textFieldGroup($model,'emp_rel_id',array('widgetOptions'=>array('htmlOptions'=>array('class'=>'span5')))); ?>
		</div>
		<div class="col-md-4">
			<?php echo $form->textFieldGroup($model,'emp_prov_id',array('widgetOptions'=>array('htmlOptions'=>array('class'=>'span5')))); ?>
		</div>
		<div class="col-md-4">
			<?php echo $form->textFieldGroup($model,'emp_city_id',array('widgetOptions'=>array('htmlOptions'=>array('class'=>'span5')))); ?>
		</div>
	</div>

	<div class="row">
		<div class="col-md-6">
			<?php echo $form->textFieldGroup($model,'emp_domicile_address',array('widgetOptions'=>array('htmlOptions'=>array('class'=>'span5','maxlength'=>100)))); ?>

		</div>
		<div class="col-md-6">
			<?php echo $form->textFieldGroup($model,'emp_address',array('widgetOptions'=>array('htmlOptions'=>array('class'=>'span5','maxlength'=>100)))); ?>
		</div>
	</div>
	
	<div class="row">
		<div class="col-md-6">
			<?php echo $form->textFieldGroup($model,'emp_phone_number',array('widgetOptions'=>array('htmlOptions'=>array('class'=>'span5','maxlength'=>20)))); ?>
		</div>
		<div class="col-md-6">
			<?php echo $form->textFieldGroup($model,'emp_email',array('widgetOptions'=>array('htmlOptions'=>array('class'=>'span5','maxlength'=>50)))); ?>
		</div>
	</div>
	
	<div class="row">
		<div class="col-md-2">
			<?php echo $form->dropDownListGroup($model,'emp_type', array('widgetOptions'=>array('data'=>array("Training"=>"Training","Contract"=>"Contract",), 'htmlOptions'=>array('class'=>'input-large')))); ?>
		</div>
		<div class="col-md-2">
			<?php echo $form->dropDownListGroup($model,'emp_group', array('widgetOptions'=>array('data'=>array("Back Office"=>"Back Office","Front Office"=>"Front Office",), 'htmlOptions'=>array('class'=>'input-large')))); ?>
		</div>
		<div class="col-md-6">
			<?php echo $form->datePickerGroup($model,'emp_join',array('widgetOptions'=>array('options'=>array('format'=>'yyyy-mm-dd','viewFormat'=>'yyyy-mm-dd'),'htmlOptions'=>array('class'=>'span5')), 'prepend'=>'<i class="glyphicon glyphicon-calendar"></i>', 'append'=>'Klik Bulan/Tahun untuk merubah Bulan/Tahun.')); ?>
		</div>
		<div class="col-md-2">
			<?php echo $form->dropDownListGroup($model,'emp_status', array('widgetOptions'=>array('data'=>array("Active"=>"Active","Inactive"=>"Inactive",), 'htmlOptions'=>array('class'=>'input-large')))); ?>
		</div>
	</div>
	
	<?php echo $form->fileFieldGroup($model, 'emp_photo',
			array(
				'wrapperHtmlOptions' => array(
					'class' => 'col-sm-5',
				),
				
			)
		); ?>

	

<div class="form-actions">
	<?php $this->widget('booster.widgets.TbButton', array(
			'buttonType'=>'submit',
			'context'=>'primary',
			'label'=>$model->isNewRecord ? 'Tambah' : 'Simpan',
		)); 
		echo CHtml::button('Batal', array(
            'class' => 'btn btn-primary',
            'onclick' => "history.go(-1)",
                )
        );
		?>

		</div>

<?php $this->endWidget(); ?>
