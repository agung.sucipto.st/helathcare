<?php
$this->breadcrumbs=array(
	'Kelola Jenis Diet'=>array('index'),
	'Tambah Jenis Diet',
);
$this->title=array(
	'title'=>'Tambah Jenis Diet',
	'deskripsi'=>'Untuk Menambah Jenis Diet'
);?>

<div class="panel panel-default">
	<div class="panel-heading">
		<h3 class="panel-title pull-left">Form Tambah</h3>
		<a href="<?php echo Yii::app()->createUrl(Yii::app()->controller->module->id.'/'.Yii::app()->controller->id);?>" class="btn btn-primary btn-xs pull-right">
			<i class="fa fa-arrow-circle-left"></i>
			<span>Kembali</span>
		</a>
		<div style="clear:both"></div>
	</div>
	<div class="panel-body">		
		<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>	</div>
</div>