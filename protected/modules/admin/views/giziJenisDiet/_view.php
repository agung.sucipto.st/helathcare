<div class="view">
		<b><?php echo CHtml::encode($data->getAttributeLabel('gjd_id')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->gjd_id),array('view','id'=>$data->gjd_id)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('gjd_name')); ?>:</b>
	<?php echo CHtml::encode($data->gjd_name); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('gjd_description')); ?>:</b>
	<?php echo CHtml::encode($data->gjd_description); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('create_at')); ?>:</b>
	<?php echo CHtml::encode($data->create_at); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('modified_at')); ?>:</b>
	<?php echo CHtml::encode($data->modified_at); ?>
	<br />

</div>