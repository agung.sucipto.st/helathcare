<div class="view">
		<b><?php echo CHtml::encode($data->getAttributeLabel('access_id')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->access_id),array('view','id'=>$data->access_id)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('access_name')); ?>:</b>
	<?php echo CHtml::encode($data->access_name); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('access_module')); ?>:</b>
	<?php echo CHtml::encode($data->access_module); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('access_controller')); ?>:</b>
	<?php echo CHtml::encode($data->access_controller); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('access_action')); ?>:</b>
	<?php echo CHtml::encode($data->access_action); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('access_parent')); ?>:</b>
	<?php echo CHtml::encode($data->access_parent); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('access_status')); ?>:</b>
	<?php echo CHtml::encode($data->access_status); ?>
	<br />

	<?php /*
	<b><?php echo CHtml::encode($data->getAttributeLabel('access_visible')); ?>:</b>
	<?php echo CHtml::encode($data->access_visible); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('create_at')); ?>:</b>
	<?php echo CHtml::encode($data->create_at); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('modified_at')); ?>:</b>
	<?php echo CHtml::encode($data->modified_at); ?>
	<br />

	*/ ?>
</div>