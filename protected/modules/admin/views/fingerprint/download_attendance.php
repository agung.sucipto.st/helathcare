<?php
$this->breadcrumbs=array(
	'Manage Fingerprint'=>array('index'),
	'Download Fingerprint',
);

$this->title=array(
	'title'=>'Download Fingerprint Data',
	'deskripsi'=>'For Download Fingerprint Data'
);

Yii::app()->clientScript->registerScript('validate', '

$("#Fingerprint_fp_id").change(function(){
	var id = $("#Fingerprint_fp_id").val();
	if(id==""){
		alert("Choose Fingerprint !");
	}else{
		changeValue(id);
	}
});

function changeValue(value){	
	$(".append").hide();	
	$.ajax({
		url: "'.Yii::app()->createAbsoluteUrl(Yii::app()->controller->module->id.'/fingerprint/cekStatus').'",
		cache: false,
		type: "POST",
		data:"id="+value,
		success: function(msg){
			if(msg=="Failed"){
				$("#Download").hide();
				alert("Fingerprint Machine Not Connected To The Network !");
			}else{
				$("#Download").show();
			}
		}
	});
}

', CClientScript::POS_END);
?>

<div class="panel panel-default">
	<div class="panel-heading">
		<h3 class="panel-title pull-left">Download Machine Data</h3>
		<a href="<?php echo Yii::app()->createUrl(Yii::app()->controller->module->id.'/'.Yii::app()->controller->id);?>" class="btn btn-primary btn-xs pull-right">
			<i class="fa fa-arrow-circle-left"></i>
			<span>Return</span>
		</a>
	<div style="clear:both"></div>
	</div>
	<div class="panel-body">

		<?php
		if(Yii::app()->user->hasFlash('result')){
			echo'
			<div class="row">
			<div class="alert alert-info">
				<button type="button" class="close" data-dismiss="alert">&times;</button>
				'.Yii::app()->user->getFlash('result').'
			</div>
			</div>
			';
		}
		?>
		
		<?php $form=$this->beginWidget('booster.widgets.TbActiveForm',array(
			'id'=>'fingerprint-form',
			'htmlOptions'=>array('enctype'=>'multipart/form-data'),
		)); ?>


<?php echo $form->errorSummary($model); ?>
	<?php echo $form->dropDownListGroup($model,'fp_id', array('widgetOptions'=>array('data'=>CHtml::listData(Fingerprint::model()->findAll(),'fp_id','fp_name'), 'htmlOptions'=>array('class'=>'input-large','empty'=>'- Choose Fingerprint Machine -')))); ?>

	<?php 
	
	if($model->isNewRecord)echo $form->datePickerGroup($model,'fp_name',array('label'=>"From",'widgetOptions'=>array('options'=>array('format'=>'yyyy-mm-dd','viewFormat'=>'yyyy-mm-dd'),'htmlOptions'=>array('class'=>'span5',"value"=>date("Y-m-d"))), 'prepend'=>'<i class="glyphicon glyphicon-calendar"></i>', 'append'=>'Click Month/Year to change Month/Year.')); 
	if($model->isNewRecord)echo $form->datePickerGroup($model,'fp_location',array('label'=>"To",'widgetOptions'=>array('options'=>array('format'=>'yyyy-mm-dd','viewFormat'=>'yyyy-mm-dd'),'htmlOptions'=>array('class'=>'span5',"value"=>date("Y-m-d"))), 'prepend'=>'<i class="glyphicon glyphicon-calendar"></i>', 'append'=>'Click Month/Year to change Month/Year.')); 
	?>
	

<div class="form-actions">
	<?php $this->widget('booster.widgets.TbButton', array(
			'buttonType'=>'submit',
			'context'=>'primary',
			'htmlOptions'=>array("id"=>"Download"),
			'label'=>$model->isNewRecord ? 'Download' : 'Save',
		)); 
		?>

		</div>

<?php $this->endWidget(); ?>

	</div>
</div>
