<div class="view">
		<b><?php echo CHtml::encode($data->getAttributeLabel('fp_id')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->fp_id),array('view','id'=>$data->fp_id)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('fp_name')); ?>:</b>
	<?php echo CHtml::encode($data->fp_name); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('fp_location')); ?>:</b>
	<?php echo CHtml::encode($data->fp_location); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('fp_ip_address')); ?>:</b>
	<?php echo CHtml::encode($data->fp_ip_address); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('fp_create')); ?>:</b>
	<?php echo CHtml::encode($data->fp_create); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('fp_update')); ?>:</b>
	<?php echo CHtml::encode($data->fp_update); ?>
	<br />

</div>