<?php
$this->breadcrumbs=array(
	'Kelola Fingerprint'=>array('index'),
	'Detail Fingerprint',
);

$this->title=array(
	'title'=>'Detail Fingerprint',
	'deskripsi'=>'Untuk Melihat Detail Fingerprint'
);
?>

<div class="panel panel-default">
	<div class="panel-heading">
		<h3 class="panel-title pull-left">Detail</h3>
		<a href="<?php echo Yii::app()->createUrl(Yii::app()->controller->module->id.'/'.Yii::app()->controller->id);?>" class="btn btn-primary btn-xs pull-right">
			<i class="fa fa-arrow-circle-left"></i>
			<span>Kembali</span>
		</a>
		<div style="clear:both"></div>
	</div>
	<div class="panel-body">		
		<div id="example-2_wrapper" class="dataTables_wrapper form-inline dt-bootstrap no-footer">
			<?php $this->widget('booster.widgets.TbDetailView',array(
			'data'=>$model,
			'attributes'=>array(
				'fp_name',
				'fp_location',
				'fp_ip_address',
			),
			)); ?>
			
			<?php 
			// put this somewhere on top
			$pageSize=Yii::app()->user->getState('pageSize',Yii::app()->params['defaultPageSize']); ?>		
			<?php		
			$this->widget('booster.widgets.TbExtendedGridView', array(
				'id'=>'attendance-temp-grid',
				'type' => 'striped',
				'dataProvider' => $data->search(),
				'filter'=>$data,
				'summaryText'=>'Menampilkan {start}-{end} dari {count} hasil.',
				'selectableRows' => 2,
				'responsiveTable' => true,
				'enablePagination' => true,
				'pager' => array(
					'htmlOptions'=>array(
						'class'=>'pagination'
					),
					'maxButtonCount' => 5,
					'cssFile' => true,
					'header' => false,
					'firstPageLabel' => '<<',
					'prevPageLabel' => '<',
					'nextPageLabel' => '>',
					'lastPageLabel' => '>>',
				),
				
				'columns'=>array(
						array(
							"header"=>"Employee",
							"value"=>function($data){
								$emp=Employee::model()->findByAttributes(array("att_id"=>$data->emp_id));
								return $emp->emp_id.' - '.$emp->emp_full_name.' | '.$data->emp_id;
							},
							"name"=>"emp_id"
						),
						'att_date',
						'att_time',
				),
			));
			?>
		</div>
	</div>
</div>
