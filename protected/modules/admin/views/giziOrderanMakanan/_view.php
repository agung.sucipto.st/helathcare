<div class="view">
		<b><?php echo CHtml::encode($data->getAttributeLabel('gom_id')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->gom_id),array('view','id'=>$data->gom_id)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('gr_id')); ?>:</b>
	<?php echo CHtml::encode($data->gr_id); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('gom_date')); ?>:</b>
	<?php echo CHtml::encode($data->gom_date); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('gom_status')); ?>:</b>
	<?php echo CHtml::encode($data->gom_status); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('create_at')); ?>:</b>
	<?php echo CHtml::encode($data->create_at); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('modified_at')); ?>:</b>
	<?php echo CHtml::encode($data->modified_at); ?>
	<br />

</div>