<?php
$this->breadcrumbs=array(
	'Kelola Work Hour'=>array('index'),
	'Detail Work Hour',
);

$this->title=array(
	'title'=>'Detail Work Hour',
	'deskripsi'=>'Untuk Melihat Detail Work Hour'
);
?>

<div class="panel panel-default">
	<div class="panel-heading">
		<h3 class="panel-title pull-left">Detail</h3>
		<a href="<?php echo Yii::app()->createUrl(Yii::app()->controller->module->id.'/'.Yii::app()->controller->id);?>" class="btn btn-primary btn-xs pull-right">
			<i class="fa fa-arrow-circle-left"></i>
			<span>Kembali</span>
		</a>
		<div style="clear:both"></div>
	</div>
	<div class="panel-body">		
		<div id="example-2_wrapper" class="dataTables_wrapper form-inline dt-bootstrap no-footer">
			<?php $this->widget('booster.widgets.TbDetailView',array(
			'data'=>$model,
			'attributes'=>array(
							'wh_id',
				'wh_att_in',
				'wh_att_break_in',
				'wh_att_break_out',
				'wh_att_out',
				'wh_attend',
				'wh_create',
				'wh_update',
			),
			)); ?>
		</div>
	</div>
</div>
