<div class="view">
		<b><?php echo CHtml::encode($data->getAttributeLabel('wh_id')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->wh_id),array('view','id'=>$data->wh_id)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('wh_att_in')); ?>:</b>
	<?php echo CHtml::encode($data->wh_att_in); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('wh_att_break_in')); ?>:</b>
	<?php echo CHtml::encode($data->wh_att_break_in); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('wh_att_break_out')); ?>:</b>
	<?php echo CHtml::encode($data->wh_att_break_out); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('wh_att_out')); ?>:</b>
	<?php echo CHtml::encode($data->wh_att_out); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('wh_attend')); ?>:</b>
	<?php echo CHtml::encode($data->wh_attend); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('wh_create')); ?>:</b>
	<?php echo CHtml::encode($data->wh_create); ?>
	<br />

	<?php /*
	<b><?php echo CHtml::encode($data->getAttributeLabel('wh_update')); ?>:</b>
	<?php echo CHtml::encode($data->wh_update); ?>
	<br />

	*/ ?>
</div>