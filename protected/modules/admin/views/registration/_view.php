<div class="view">
		<b><?php echo CHtml::encode($data->getAttributeLabel('reg_id')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->reg_id),array('view','id'=>$data->reg_id)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('p_id')); ?>:</b>
	<?php echo CHtml::encode($data->p_id); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('reg_no')); ?>:</b>
	<?php echo CHtml::encode($data->reg_no); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('class_id')); ?>:</b>
	<?php echo CHtml::encode($data->class_id); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('bed_id')); ?>:</b>
	<?php echo CHtml::encode($data->bed_id); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('reg_guarator')); ?>:</b>
	<?php echo CHtml::encode($data->reg_guarator); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('reg_date_in')); ?>:</b>
	<?php echo CHtml::encode($data->reg_date_in); ?>
	<br />

	<?php /*
	<b><?php echo CHtml::encode($data->getAttributeLabel('reg_type')); ?>:</b>
	<?php echo CHtml::encode($data->reg_type); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('reg_doctor')); ?>:</b>
	<?php echo CHtml::encode($data->reg_doctor); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('reg_first_diagonsis')); ?>:</b>
	<?php echo CHtml::encode($data->reg_first_diagonsis); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('reg_end_diagonsis')); ?>:</b>
	<?php echo CHtml::encode($data->reg_end_diagonsis); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('reg_is_out')); ?>:</b>
	<?php echo CHtml::encode($data->reg_is_out); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('create_at')); ?>:</b>
	<?php echo CHtml::encode($data->create_at); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('modified_at')); ?>:</b>
	<?php echo CHtml::encode($data->modified_at); ?>
	<br />

	*/ ?>
</div>