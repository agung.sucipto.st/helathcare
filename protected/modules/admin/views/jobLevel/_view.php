<div class="view">
		<b><?php echo CHtml::encode($data->getAttributeLabel('jl_id')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->jl_id),array('view','id'=>$data->jl_id)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('jl_name')); ?>:</b>
	<?php echo CHtml::encode($data->jl_name); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('jl_description')); ?>:</b>
	<?php echo CHtml::encode($data->jl_description); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('jl_order')); ?>:</b>
	<?php echo CHtml::encode($data->jl_order); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('jl_create')); ?>:</b>
	<?php echo CHtml::encode($data->jl_create); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('jl_update')); ?>:</b>
	<?php echo CHtml::encode($data->jl_update); ?>
	<br />

</div>