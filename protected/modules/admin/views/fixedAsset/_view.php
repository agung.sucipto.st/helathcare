<div class="view">
		<b><?php echo CHtml::encode($data->getAttributeLabel('fa_id')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->fa_id),array('view','id'=>$data->fa_id)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('fa_jenis')); ?>:</b>
	<?php echo CHtml::encode($data->fa_jenis); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('fa_merk')); ?>:</b>
	<?php echo CHtml::encode($data->fa_merk); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('fa_tipe')); ?>:</b>
	<?php echo CHtml::encode($data->fa_tipe); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('fa_unit')); ?>:</b>
	<?php echo CHtml::encode($data->fa_unit); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('fa_lantai')); ?>:</b>
	<?php echo CHtml::encode($data->fa_lantai); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('fa_ruang')); ?>:</b>
	<?php echo CHtml::encode($data->fa_ruang); ?>
	<br />

	<?php /*
	<b><?php echo CHtml::encode($data->getAttributeLabel('fa_harga_dasar')); ?>:</b>
	<?php echo CHtml::encode($data->fa_harga_dasar); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('fa_harga_perolehan')); ?>:</b>
	<?php echo CHtml::encode($data->fa_harga_perolehan); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('fa_tgl_pembukuan')); ?>:</b>
	<?php echo CHtml::encode($data->fa_tgl_pembukuan); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('fa_tgl_efektif')); ?>:</b>
	<?php echo CHtml::encode($data->fa_tgl_efektif); ?>
	<br />

	*/ ?>
</div>