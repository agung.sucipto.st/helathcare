<?php
$this->breadcrumbs=array(
	'Kelola Fixed Asset'=>array('index'),
	'Detail Fixed Asset',
);

$this->title=array(
	'title'=>'Detail Fixed Asset',
	'deskripsi'=>'Untuk Melihat Detail Fixed Asset'
);
?>

<div class="panel panel-default">
	<div class="panel-heading">
		<h3 class="panel-title pull-left">Detail</h3>
		<a href="<?php echo Yii::app()->createUrl(Yii::app()->controller->module->id.'/'.Yii::app()->controller->id);?>" class="btn btn-primary btn-xs pull-right">
			<i class="fa fa-arrow-circle-left"></i>
			<span>Kembali</span>
		</a>
		<a href="<?php echo Yii::app()->createUrl(Yii::app()->controller->module->id.'/'.Yii::app()->controller->id.'/create');?>" class="btn btn-primary btn-xs pull-right">
			<i class="fa fa-copy"></i>
			<span>Tambah Data</span>
		</a>
		<a href="<?php echo Yii::app()->createUrl(Yii::app()->controller->module->id.'/'.Yii::app()->controller->id.'/update/id/'.$_GET['id']);?>" class="btn btn-primary btn-xs pull-right">
			<i class="fa fa-edit"></i>
			<span>Edit Data</span>
		</a>
		<div style="clear:both"></div>
	</div>
	<div class="panel-body">		
		<div id="example-2_wrapper" class="dataTables_wrapper form-inline dt-bootstrap no-footer">
			<?php $this->widget('booster.widgets.TbDetailView',array(
			'data'=>$model,
			'attributes'=>array(
							'fa_id',
				'fa_jenis',
				'fa_merk',
				'fa_tipe',
				'fa_unit',
				'fa_lantai',
				'fa_ruang',
				'fa_harga_dasar',
				'fa_harga_perolehan',
				'fa_tgl_pembukuan',
				'fa_tgl_efektif',
			),
			)); ?>
		</div>
	</div>
</div>
