<html>
	<style>
		body{
			font-family:Arial;
			
		}
		
		 @font-face { 
		font-family: IDAutomationHC39M;  
		src: url(<?php echo Yii::app()->theme->baseUrl;?>/assets/fonts/IDAutomationHC39M.woff);
		}
		.barcode {
		  font-family: 'IDAutomationHC39M'; 
		  width:auto;
		  font-size:10pt;
		}
		.barcode-info {
		  font-size:8pt;
		}
		.page{
			width:100mm;
			height:auto;
			margin-left:-4px;
		}
		.label{
			width:auto;
			height:44mm;
			padding:2mm;
			margin-bottom:2mm;
		}
		/*
		.label:nth-child(odd){
			background:pink;
		}
		.label:nth-child(even){
			background:green;
		}
		*/
		.content{
			width:100%;
		}
		.label:first-child{
			margin-top:-8px;
		}
		.label:last-child{
			margin-bottom:0px;
		}
		th{
			text-align:left;
		}
		table{
			font-size:10px;
			font-weight:bold;
		}
	</style>
	<body>
		<div class="page">
			<?php
			foreach($model as $row){
			$count=strlen($row->fa_jenis.' / '.$row->fa_merk.' / '.$row->fa_tipe);
			if($count>=72){
				$dot="...";
			}else{
				$dot="";
			}
			$item=substr($row->fa_jenis.' / '.$row->fa_merk.' / '.$row->fa_tipe,0,72).$dot;
			
			
			echo'
			<div class="label">
				<div class="content">
					<center>
						<span class="barcode">('.$row->fa_id.')</span><br/>
						<table>
								<tr>
									<th style="width:25mm;">Kode</th>
									<td>:</td>
									<td>'.$row->fa_id.'</td>
									<td rowspan="6"><img src="'.Yii::app()->theme->baseUrl.'/assets/logo_black.png" width="80px"/></td>
								</tr>
								<tr>
									<th>Nama Barang</th>
									<td>:</td>
									<td>'.$item.'</td>
									
								</tr>
								<tr>
									<th>Unit Pemakai</th>
									<td>:</td>
									<td style="font-size:8px">'.$row->fa_unit.'</td>
								</tr>
								<tr>
									<th>Lantai</th>
									<td>:</td>
									<td style="font-size:8px">'.$row->fa_lantai.'</td>
								</tr>
								<tr>
									<th>Ruang</th>
									<td>:</td>
									<td style="font-size:8px">'.$row->fa_ruang.'</td>
								</tr>
								<tr>
									<th>Tgl. Pembukuan</th>
									<td>:</td>
									<td>'.$row->fa_tgl_pembukuan.'</td>
								</tr>
								<tr>
									<th>Tgl. Efektif</th>
									<td>:</td>
									<td>'.$row->fa_tgl_efektif.'</td>
								</tr>
						</table>
					</center>
					
				</div>
			</div>';
			}
			?>
			
			
		</div>
	</body>
</html>