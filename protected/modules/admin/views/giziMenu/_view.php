<div class="view">
		<b><?php echo CHtml::encode($data->getAttributeLabel('gm_id')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->gm_id),array('view','id'=>$data->gm_id)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('gmk_id')); ?>:</b>
	<?php echo CHtml::encode($data->gmk_id); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('gm_name')); ?>:</b>
	<?php echo CHtml::encode($data->gm_name); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('gm_description')); ?>:</b>
	<?php echo CHtml::encode($data->gm_description); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('create_at')); ?>:</b>
	<?php echo CHtml::encode($data->create_at); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('modified_at')); ?>:</b>
	<?php echo CHtml::encode($data->modified_at); ?>
	<br />

</div>