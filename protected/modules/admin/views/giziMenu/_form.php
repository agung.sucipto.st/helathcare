<?php $form=$this->beginWidget('booster.widgets.TbActiveForm',array(
	'id'=>'gizi-menu-form',
	'enableAjaxValidation'=>true,
)); ?>

<p class="help-block">Isian dengan tanda <span class="required">*</span> wajib diisi.</p>

<?php echo $form->errorSummary($model); ?>

	<?php echo $form->dropDownListGroup($model,'gmk_id', array('widgetOptions'=>array('data'=>CHtml::listData(GiziMenuKategori::model()->findAll(),'gmk_id','gmk_name'), 'htmlOptions'=>array('class'=>'input-large','empty'=>'- Choose Category -')))); ?>
	<?php echo $form->textFieldGroup($model,'gm_name',array('widgetOptions'=>array('htmlOptions'=>array('class'=>'span5','maxlength'=>50)))); ?>

	<?php echo $form->textAreaGroup($model,'gm_description', array('widgetOptions'=>array('htmlOptions'=>array('rows'=>6, 'cols'=>50, 'class'=>'span8')))); ?>

<div class="form-actions">
	<?php $this->widget('booster.widgets.TbButton', array(
			'buttonType'=>'submit',
			'context'=>'primary',
			'label'=>$model->isNewRecord ? 'Tambah' : 'Simpan',
		)); 
		echo CHtml::button('Batal', array(
            'class' => 'btn btn-primary',
            'onclick' => "history.go(-1)",
                )
        );
		?>

		</div>

<?php $this->endWidget(); ?>
