<div class="view">
		<b><?php echo CHtml::encode($data->getAttributeLabel('goml_id')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->goml_id),array('view','id'=>$data->goml_id)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('gom_id')); ?>:</b>
	<?php echo CHtml::encode($data->gom_id); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('gmhl_id')); ?>:</b>
	<?php echo CHtml::encode($data->gmhl_id); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('gwt_id')); ?>:</b>
	<?php echo CHtml::encode($data->gwt_id); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('create_at')); ?>:</b>
	<?php echo CHtml::encode($data->create_at); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('modified_at')); ?>:</b>
	<?php echo CHtml::encode($data->modified_at); ?>
	<br />

</div>