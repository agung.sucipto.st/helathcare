<?php
$this->breadcrumbs=array(
	'Kelola Shift Group'=>array('index'),
	'Manage Work Hour',
);
$this->title=array(
	'title'=>'Manage Work Hour',
	'deskripsi'=>'Mengatur Waktu Kerja'
);?>

<div class="panel panel-default">
	<div class="panel-heading">
		<h3 class="panel-title pull-left">Form Tambah</h3>
		<a href="<?php echo Yii::app()->createUrl(Yii::app()->controller->module->id.'/'.Yii::app()->controller->id);?>" class="btn btn-primary btn-xs pull-right">
			<i class="fa fa-arrow-circle-left"></i>
			<span>Kembali</span>
		</a>
		<div style="clear:both"></div>
	</div>
	<div class="panel-body">	
		<?php $form=$this->beginWidget('booster.widgets.TbActiveForm',array(
			'id'=>'shift-form',
			'enableAjaxValidation'=>true,
		)); ?>
		<table class="table table-stripped table-hover">
		<?php
		foreach($wh as $row){
			echo'
			<tr>
				<td>'.$row->wh_att_in.'</td>
				<td>'.$row->wh_att_break_in.'</td>
				<td>'.$row->wh_att_break_out.'</td>
				<td>'.$row->wh_att_out.'</td>
				<td>'.$row->wh_attend.'</td>
				<td><button type="submit" value="'.$row->wh_id.'" class="btn btn-primary" name="Shift[wh_id]">Choose</button></td>
			</tr>
			';
		}
		?>
		</table>
		<?php $this->endWidget(); ?>
	</div>
</div>