<?php $form=$this->beginWidget('booster.widgets.TbActiveForm',array(
	'id'=>'shift-group-form',
	'enableAjaxValidation'=>true,
)); ?>

<p class="help-block">Isian dengan tanda <span class="required">*</span> wajib diisi.</p>

<?php echo $form->errorSummary($model); ?>

	<?php echo $form->dropDownListGroup($model,'org_id', array('widgetOptions'=>array('data'=>CHtml::listData(Organization::Model()->findAll(),"org_id","org_name"), 'htmlOptions'=>array('class'=>'input-large','empty'=>'- Organization -')))); ?>

	<?php echo $form->textFieldGroup($model,'shift_group_name',array('widgetOptions'=>array('htmlOptions'=>array('class'=>'span5','maxlength'=>50)))); ?>
	
	<?php echo $form->numberFieldGroup($model,'shift_group_periode',array('widgetOptions'=>array('htmlOptions'=>array('class'=>'span5','maxlength'=>50)))); ?>

	<?php echo $form->dropDownListGroup($model,'shift_group_type', array('widgetOptions'=>array('data'=>array("Weekly"=>"Weekly","Daily"=>"Daily",), 'htmlOptions'=>array('class'=>'input-large')))); ?>

	<?php echo $form->dropDownListGroup($model,'shift_group_status', array('widgetOptions'=>array('data'=>array("Active"=>"Active","Inactive"=>"Inactive",), 'htmlOptions'=>array('class'=>'input-large')))); ?>

<div class="form-actions">
	<?php $this->widget('booster.widgets.TbButton', array(
			'buttonType'=>'submit',
			'context'=>'primary',
			'label'=>$model->isNewRecord ? 'Tambah' : 'Simpan',
		)); 
		echo CHtml::button('Batal', array(
            'class' => 'btn btn-primary',
            'onclick' => "history.go(-1)",
                )
        );
		?>

		</div>

<?php $this->endWidget(); ?>
