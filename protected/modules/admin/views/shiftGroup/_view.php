<div class="view">
		<b><?php echo CHtml::encode($data->getAttributeLabel('shift_group_id')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->shift_group_id),array('view','id'=>$data->shift_group_id)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('dept_id')); ?>:</b>
	<?php echo CHtml::encode($data->dept_id); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('shift_group_name')); ?>:</b>
	<?php echo CHtml::encode($data->shift_group_name); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('shift_group_type')); ?>:</b>
	<?php echo CHtml::encode($data->shift_group_type); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('shift_group_status')); ?>:</b>
	<?php echo CHtml::encode($data->shift_group_status); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('shift_group_create')); ?>:</b>
	<?php echo CHtml::encode($data->shift_group_create); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('shift_group_update')); ?>:</b>
	<?php echo CHtml::encode($data->shift_group_update); ?>
	<br />

</div>