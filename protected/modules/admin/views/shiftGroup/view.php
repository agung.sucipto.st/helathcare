<?php
Yii::app()->clientScript->registerScript('validate', '

function openDialog(){
	var url=$(this).attr("href");
	window.open(url,"","width=800,height=600");
}

function cekIuran(id){
	$.ajax({
		url: "'.Yii::app()->createAbsoluteUrl(Yii::app()->controller->module->id.'/iuran/cekIuran').'",
		cache: false,
		type: "POST",
		data:"id="+id,
		success: function(msg){
			$("#Iuran_jumlah_iuran").val(msg);
		}
	});
}

', CClientScript::POS_END);
?>
<?php
$this->breadcrumbs=array(
	'Kelola Shift Group'=>array('index'),
	'Detail Shift Group',
);

$this->title=array(
	'title'=>'Detail Shift Group',
	'deskripsi'=>'Untuk Melihat Detail Shift Group'
);
?>

<div class="panel panel-default">
	<div class="panel-heading">
		<h3 class="panel-title pull-left">Detail</h3>
		<a href="<?php echo Yii::app()->createUrl(Yii::app()->controller->module->id.'/'.Yii::app()->controller->id);?>" class="btn btn-primary btn-xs pull-right">
			<i class="fa fa-arrow-circle-left"></i>
			<span>Kembali</span>
		</a>
		<div style="clear:both"></div>
	</div>
	<div class="panel-body">		
		<div id="example-2_wrapper" class="dataTables_wrapper form-inline dt-bootstrap no-footer">
			<?php $this->widget('booster.widgets.TbDetailView',array(
			'data'=>$model,
			'attributes'=>array(
				'org_id',
				'shift_group_name',
				'shift_group_type',
				'shift_group_status',
				'shift_group_create',
				'shift_group_update',
			),
			)); ?>
			
			<?php 
				$pageSize=Yii::app()->user->getState('pageSize',Yii::app()->params['defaultPageSize']);
			?>		
			<?php	
			if($model->shift_group_type=="Weekly"){
			$this->widget('booster.widgets.TbExtendedGridView', array(
				'id'=>'shift-grid',
				'type' => 'striped',
				'dataProvider' => $shift->search(),
				'summaryText'=>"",
				'selectableRows' => 2,
				'responsiveTable' => true,
				'enablePagination' => true,
				'pager' => array(
					'htmlOptions'=>array(
						'class'=>'pagination'
					),
					'maxButtonCount' => 5,
					'cssFile' => true,
					'header' => false,
					'firstPageLabel' => '<<',
					'prevPageLabel' => '<',
					'nextPageLabel' => '>',
					'lastPageLabel' => '>>',
				),
				'columns'=>array(
						'shift_day',
						'shift_att_in',
						'shift_att_break_in',
						'shift_att_break_out',
						'shift_att_out',
						'shift_attend',
						array(
							'class'=>'booster.widgets.TbButtonColumn',
							 'deleteConfirmation'=>'Anda yakin akan menhapus data?',
							'template'=>'{work}{update}{delete}',
							'buttons'=>array
							(
								'work' => array
								(
									'label'=>'Work Hour',
									'icon'=>'time',
									'url'=>'Yii::app()->createUrl(Yii::app()->controller->module->id."/shiftGroup/add",array("id"=>$data->shift_id))',
									'options'=>array(
										'class'=>'btn btn-default btn-xs',
									),
								),
								'update' => array
								(
									'label'=>'Update',
									'icon'=>'pencil',
									'url'=>'Yii::app()->createUrl(Yii::app()->controller->module->id."/shift/update",array("id"=>$data->shift_id))',
									'options'=>array(
										'class'=>'btn btn-default btn-xs',
									),
								),
								'delete' => array
								(
									'label'=>'Delete',
									'icon'=>'trash',
									'url'=>'Yii::app()->createUrl(Yii::app()->controller->module->id."/shift/delete",array("id"=>$data->shift_id))',
									'options'=>array(
										'class'=>'btn btn-default btn-xs delete',
									),
								)
							),
						),
				),
			));
			}else{
			$this->widget('booster.widgets.TbExtendedGridView', array(
				'id'=>'shift-grid',
				'type' => 'striped',
				'dataProvider' => $shift->search(),
				'summaryText'=>"",
				'selectableRows' => 2,
				'responsiveTable' => true,
				'enablePagination' => true,
				'pager' => array(
					'htmlOptions'=>array(
						'class'=>'pagination'
					),
					'maxButtonCount' => 5,
					'cssFile' => true,
					'header' => false,
					'firstPageLabel' => '<<',
					'prevPageLabel' => '<',
					'nextPageLabel' => '>',
					'lastPageLabel' => '>>',
				),
				'columns'=>array(
						'shift_periode',
						'shift_att_in',
						'shift_att_break_in',
						'shift_att_break_out',
						'shift_att_out',
						'shift_attend',
						array(
							'class'=>'booster.widgets.TbButtonColumn',
							 'deleteConfirmation'=>'Anda yakin akan menhapus data?',
							'template'=>'{work}{update}{delete}',
							'buttons'=>array
							(
								'work' => array
								(
									'label'=>'Work Hour',
									'icon'=>'time',
									'url'=>'Yii::app()->createUrl(Yii::app()->controller->module->id."/shiftGroup/add",array("id"=>$data->shift_id))',
									'options'=>array(
										'class'=>'btn btn-default btn-xs',
									),
								),
								'update' => array
								(
									'label'=>'Update',
									'icon'=>'pencil',
									'url'=>'Yii::app()->createUrl(Yii::app()->controller->module->id."/shift/update",array("id"=>$data->shift_id))',
									'options'=>array(
										'class'=>'btn btn-default btn-xs',
									),
								),
								'delete' => array
								(
									'label'=>'Delete',
									'icon'=>'trash',
									'url'=>'Yii::app()->createUrl(Yii::app()->controller->module->id."/shift/delete",array("id"=>$data->shift_id))',
									'options'=>array(
										'class'=>'btn btn-default btn-xs delete',
									),
								)
							),
						),
				),
			));
			}
			?>
		</div>
	</div>
</div>
