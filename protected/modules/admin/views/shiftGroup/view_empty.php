			<?php $this->widget('booster.widgets.TbDetailView',array(
			'data'=>$model,
			'attributes'=>array(
				'org_id',
				'shift_group_name',
				'shift_group_type',
				'shift_group_status',
			),
			)); ?>
			
			<?php 
				$pageSize=Yii::app()->user->getState('pageSize',Yii::app()->params['defaultPageSize']);
			?>		
			<?php	
			if($model->shift_group_type=="Weekly"){
			$this->widget('booster.widgets.TbExtendedGridView', array(
				'id'=>'shift-grid',
				'type' => 'striped',
				'dataProvider' => $shift->search(),
				'summaryText'=>"",
				'selectableRows' => 2,
				'responsiveTable' => true,
				'enablePagination' => true,
				'pager' => array(
					'htmlOptions'=>array(
						'class'=>'pagination'
					),
					'maxButtonCount' => 5,
					'cssFile' => true,
					'header' => false,
					'firstPageLabel' => '<<',
					'prevPageLabel' => '<',
					'nextPageLabel' => '>',
					'lastPageLabel' => '>>',
				),
				'columns'=>array(
						'shift_day',
						'shift_att_in',
						'shift_att_break_in',
						'shift_att_break_out',
						'shift_att_out',
						'shift_attend',
						
				),
			));
			}else{
			$this->widget('booster.widgets.TbExtendedGridView', array(
				'id'=>'shift-grid',
				'type' => 'striped',
				'dataProvider' => $shift->search(),
				'summaryText'=>"",
				'selectableRows' => 2,
				'responsiveTable' => true,
				'enablePagination' => true,
				'pager' => array(
					'htmlOptions'=>array(
						'class'=>'pagination'
					),
					'maxButtonCount' => 5,
					'cssFile' => true,
					'header' => false,
					'firstPageLabel' => '<<',
					'prevPageLabel' => '<',
					'nextPageLabel' => '>',
					'lastPageLabel' => '>>',
				),
				'columns'=>array(
						'shift_periode',
						'shift_att_in',
						'shift_att_break_in',
						'shift_att_break_out',
						'shift_att_out',
						'shift_attend',
						
				),
			));
			}
			?>
			
			