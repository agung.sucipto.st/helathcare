<div class="view">
		<b><?php echo CHtml::encode($data->getAttributeLabel('code_id')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->code_id),array('view','id'=>$data->code_id)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('code_type')); ?>:</b>
	<?php echo CHtml::encode($data->code_type); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('emp_id')); ?>:</b>
	<?php echo CHtml::encode($data->emp_id); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('code_location')); ?>:</b>
	<?php echo CHtml::encode($data->code_location); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('code_note')); ?>:</b>
	<?php echo CHtml::encode($data->code_note); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('code_status')); ?>:</b>
	<?php echo CHtml::encode($data->code_status); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('create_at')); ?>:</b>
	<?php echo CHtml::encode($data->create_at); ?>
	<br />

	<?php /*
	<b><?php echo CHtml::encode($data->getAttributeLabel('modified_at')); ?>:</b>
	<?php echo CHtml::encode($data->modified_at); ?>
	<br />

	*/ ?>
</div>