<link rel="stylesheet" href="<?php echo Yii::app()->theme->baseUrl; ?>/assets/css/bootstrap-select.css">
<script src="<?php echo Yii::app()->theme->baseUrl; ?>/assets/js/bootstrap-select.js"></script>
	
<?php $form=$this->beginWidget('booster.widgets.TbActiveForm',array(
	'id'=>'code-report-form',
	'enableAjaxValidation'=>true,
)); ?>

<p class="help-block">Isian dengan tanda <span class="required">*</span> wajib diisi.</p>

<?php echo $form->errorSummary($model); ?>

	<?php echo $form->dropDownListGroup($model,'code_type', array('widgetOptions'=>array('data'=>array("RED"=>"RED","BLUE"=>"BLUE","PINK"=>"PINK","YELLOW"=>"YELLOW","BLACK"=>"BLACK","GREEN"=>"GREEN",), 'htmlOptions'=>array('class'=>'input-large')))); ?>

	<?php echo $form->dropDownListGroup($model,'code_location', array('widgetOptions'=>array('data'=>array("LG"=>"LG","G"=>"G","LT1"=>"LT1","LT2"=>"LT2","LT3"=>"LT3","LT4"=>"LT4","LT5"=>"LT5","LT6"=>"LT6","LT7"=>"LT7","LT8"=>"LT8","LT9"=>"LT9","FBG"=>"FBG","FB1"=>"FB1","FB2"=>"FB2",), 'htmlOptions'=>array('class'=>'input-large',"empty"=>"Location")))); ?>
				<?php 
				$emp=Employee::model()->findAll(array("condition"=>"emp_status='Active'","order"=>"emp_full_name ASC"));
				$employee=array();
				foreach($emp as $row){
						$employee[$row->emp_id]=$row->emp_full_name.' | '.$row->emp_id;
				}
				echo $form->dropDownListGroup($model,'emp_id', array('widgetOptions'=>array('data'=>$employee, 'htmlOptions'=>array('class'=>'input-large selectpicker show-tick','data-live-search'=>"true",'empty'=>'Choose Group'))));?>

	<?php echo $form->textAreaGroup($model,'code_note', array('widgetOptions'=>array('htmlOptions'=>array('rows'=>6, 'cols'=>50, 'class'=>'span8')))); ?>

	<?php echo $form->dropDownListGroup($model,'code_status', array('widgetOptions'=>array('data'=>array("Active"=>"Active","In Active"=>"In Active",), 'htmlOptions'=>array('class'=>'input-large')))); ?>

	<?php echo $form->textFieldGroup($model,'create_at',array('widgetOptions'=>array('htmlOptions'=>array('class'=>'span5')))); ?>

	<?php echo $form->textFieldGroup($model,'modified_at',array('widgetOptions'=>array('htmlOptions'=>array('class'=>'span5')))); ?>

<div class="form-actions">
	<?php $this->widget('booster.widgets.TbButton', array(
			'buttonType'=>'submit',
			'context'=>'primary',
			'label'=>$model->isNewRecord ? 'Tambah' : 'Simpan',
		)); 
		echo CHtml::button('Batal', array(
            'class' => 'btn btn-primary',
            'onclick' => "history.go(-1)",
                )
        );
		?>

		</div>

<?php $this->endWidget(); ?>
