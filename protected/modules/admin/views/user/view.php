<?php
$this->breadcrumbs=array(
	'Kelola User'=>array('index'),
	'Detail User',
);

$this->title=array(
	'title'=>'Detail User',
	'deskripsi'=>'Untuk Melihat Detail User'
);
?>

<div class="panel panel-default">
	<div class="panel-heading">
		<h3 class="panel-title pull-left">Detail</h3>
		<a href="<?php echo Yii::app()->createUrl(Yii::app()->controller->module->id.'/'.Yii::app()->controller->id);?>" class="btn btn-primary btn-xs pull-right">
			<i class="fa fa-arrow-circle-left"></i>
			<span>Kembali</span>
		</a>
		<div style="clear:both"></div>
	</div>
	<div class="panel-body">		
		<div id="example-2_wrapper" class="dataTables_wrapper form-inline dt-bootstrap no-footer">			
			<!-- MAILBOX BEGIN -->
			<div class="mailbox row">
				<div class="col-xs-12">
					<div class="box box-solid">
						<div class="box-body">
							<div class="row">
								<div class="col-md-3 col-sm-4">
									<!-- BOXES are complex enough to move the .box-header around.
										 This is an example of having the box header within the box body -->
									<div class="box-header">
										<i class="fa fa-user"></i>
										<h3 class="box-title">User Information</h3>
									</div>
									<div style="margin-top: 15px;">
										<ul class="nav nav-pills nav-stacked">
											<li <?php echo (!isset($_GET['page']) OR ($_GET['page']=="info"))? 'class="active"' : "" ;?>><a href="<?php echo Yii::app()->createUrl(Yii::app()->controller->module->id.'/'.Yii::app()->controller->id."/view",array("id"=>$model->id_user,"modul"=>"info"));?>"><i class="fa fa-info"></i> Information</a></li>
											<li <?php echo ($_GET['page']=="access")? 'class="active"' : "" ;?>><a href="<?php echo Yii::app()->createUrl(Yii::app()->controller->module->id.'/'.Yii::app()->controller->id."/view",array("id"=>$model->id_user,"page"=>"access"));?>"><i class="fa fa-key"></i> Access</a></li>
											<li ><a href=""><i class="fa fa-gear"></i> Password </a></li>
										</ul>
									</div>
								</div><!-- /.col (LEFT) -->
								<div class="col-md-9 col-sm-8">
									<?php
									$modul=isset($_GET['page'])?$_GET['page']:'';
									switch($modul){
										default;
										$this->renderPartial('view_info', array('model'=>$model));
										break;
										case"access";
										$this->renderPartial('view_access', array('model'=>$model));
										break;
									}
									?>
								</div><!-- /.col (RIGHT) -->
							</div><!-- /.row -->
						</div><!-- /.box-body -->
						
					</div><!-- /.box -->
				</div><!-- /.col (MAIN) -->
			</div>
			<!-- MAILBOX END -->
		</div>
	</div>
</div>
