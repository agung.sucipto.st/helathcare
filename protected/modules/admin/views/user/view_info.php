<?php $this->widget('booster.widgets.TbDetailView',array(
'data'=>$model,
'attributes'=>array(
	array(
		"label"=>"Employee",
		"value"=>function($model){
			if(!empty($model->emp_id)){
				return $model->emp->emp_full_name;
			}else{
				return "NOT SET";
			}
		},
	),
	'username',
	'last_login',
	'created_at',
	'modified_at',
	'status',
),
)); ?>