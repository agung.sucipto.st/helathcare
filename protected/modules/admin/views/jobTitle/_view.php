<div class="view">
		<b><?php echo CHtml::encode($data->getAttributeLabel('jt_id')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->jt_id),array('view','id'=>$data->jt_id)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('jl_id')); ?>:</b>
	<?php echo CHtml::encode($data->jl_id); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('jt_name')); ?>:</b>
	<?php echo CHtml::encode($data->jt_name); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('jt_create')); ?>:</b>
	<?php echo CHtml::encode($data->jt_create); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('jt_update')); ?>:</b>
	<?php echo CHtml::encode($data->jt_update); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('org_id')); ?>:</b>
	<?php echo CHtml::encode($data->org_id); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('superior_job_id')); ?>:</b>
	<?php echo CHtml::encode($data->superior_job_id); ?>
	<br />

</div>