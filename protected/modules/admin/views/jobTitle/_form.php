<?php $form=$this->beginWidget('booster.widgets.TbActiveForm',array(
	'id'=>'job-title-form',
	'enableAjaxValidation'=>true,
)); ?>

<p class="help-block">Isian dengan tanda <span class="required">*</span> wajib diisi.</p>

<?php echo $form->errorSummary($model); ?>
	
	
	<?php
	$org=new Organization;
	$parent=Organization::model()->findAll();
		foreach($parent as $row){
			$data[$row->org_parent_id][] = $row;
		}
	?>
	<?php echo $form->dropDownListGroup($model,'org_id', array('widgetOptions'=>array('data'=>$org->getParent($data,0,1), 'htmlOptions'=>array('class'=>'input-large','empty'=>'- Organization -')))); ?>
	
	<?php echo $form->dropDownListGroup($model,'jl_id', array('widgetOptions'=>array('data'=>CHtml::listData(JobLevel::model()->findAll(),'jl_id','jl_name'), 'htmlOptions'=>array('class'=>'input-large','empty'=>'- Job Level -')))); ?>

	<?php echo $form->textFieldGroup($model,'jt_name',array('widgetOptions'=>array('htmlOptions'=>array('class'=>'span5','maxlength'=>50)))); ?>
	
	<?php echo $form->dropDownListGroup($model,'superior_job_id', array('widgetOptions'=>array('data'=>CHtml::listData(JobTitle::model()->findAll(),'jt_id','jt_name'), 'htmlOptions'=>array('class'=>'input-large','empty'=>'- Job Level -')))); ?>

<div class="form-actions">
	<?php $this->widget('booster.widgets.TbButton', array(
			'buttonType'=>'submit',
			'context'=>'primary',
			'label'=>$model->isNewRecord ? 'Tambah' : 'Simpan',
		)); 
		echo CHtml::button('Batal', array(
            'class' => 'btn btn-primary',
            'onclick' => "history.go(-1)",
                )
        );
		?>

		</div>

<?php $this->endWidget(); ?>
