<?php
$this->breadcrumbs=array(
	'Manage Access Menu',
);
$this->title=array(
	'title'=>'Manage Access Menu',
	'deskripsi'=>'For Manage Access Menu'
);

?>


<div class="panel panel-default">
	<div class="panel-heading">
		<h3 class="panel-title pull-left">Data</h3>
		<a href="<?php echo Yii::app()->createUrl(Yii::app()->controller->module->id.'/'.Yii::app()->controller->id.'/create');?>" class="btn btn-primary btn-xs pull-right">
			<i class="fa fa-copy"></i>
			<span>Tambah Header Menu</span>
		</a>
		<div style="clear:both"></div>
	</div>
	<div class="panel-body">
		<style>
		.pagination li.selected a{
			background:rgb(235, 235, 236);
		}
		</style>
		<?php
		if(Yii::app()->user->hasFlash('berhasil')){
		echo'
		<div class="alert alert-success">
			<button type="button" class="close" data-dismiss="alert">&times;</button>
			<i class="fa fa-warning"></i> '.Yii::app()->user->getFlash('berhasil').'
		</div>';
		}
		?>
		
		<div class="col-lg-3">		
			<div class="panel panel-default">
				<div class="panel-heading">
				Menu Access
				</div>
				<div class="panel-body">
				<style>
				.checkbox, .radio {
					padding-left: 30px;
				}
				</style>
				<form action="" method="POST">
					<?php
					$arrayIn=array();
					$in=AccessMenuArrange::model()->findAll(array("condition"=>"access_id!=''"));
					foreach($in as $d){
						$arrayIn[]=$d->access_id;
					}
					if(!empty($arrayIn)){
						$menu = Access::model()->findAll(array('condition'=>"access_visible='1' and access_id NOT IN (".implode(",",$arrayIn).")"));
					}else{
						$menu = Access::model()->findAll(array('condition'=>"access_visible='1'"));
					}
					
					foreach($menu as $r){
						echo'<div class="checkbox">
							<label><input type="checkbox" value="'.$r->access_id.'" name="menu[]" >'.$r->access_name.'</label>
						</div>';
					}
					?>
					
					<button class="btn btn-primary pull-right" type="submit" name="defaultMenu">Tambahkan Ke Menu</button>
				</form>

				</div>
			</div>
		</div>
		<div class="col-lg-9">
				<div class="dd" id="nestable_list_1">
					<ol class="dd-list">
						<?php
						$output="";
						$menus = AccessMenuArrange::model()->findAll(array("condition"=>"menu_arrange_parent='0'","order"=>"menu_arrange_order asc"));
						foreach($menus as $rows){
							$child=AccessMenuArrange::model()->findAll(array("condition"=>"menu_arrange_parent='$rows->menu_arrange_id'","order"=>"menu_arrange_order asc"));
							if(Count($child)>0){
								$output.='<li class="dd-item dd3-item" data-id="'.$rows->menu_arrange_id.'">
											<div class="dd-handle dd3-handle">
											</div>
											<div class="dd3-content" style="padding:0px;height:auto;padding-left:30px">
												 <div class="panel panel-default" style="margin-bottom:0px;border:none;">
													<div class="panel-heading" style="border:none;padding-top:6px;height:28px;">
														<h4 class="panel-title">
														'.((!empty($rows->menu_arrange_name)?$rows->menu_arrange_name:$rows->access->access_name)).'
														</h4>
													</div>
												</div>
											</div>
									
									<ol class="dd-list">';
									foreach($child as $xData){
										$subchild=AccessMenuArrange::model()->findAll(array("condition"=>"menu_arrange_parent='$xData->menu_arrange_id'","order"=>"menu_arrange_order asc"));
										if(Count($subchild)>0){
												$output.='<li class="dd-item dd3-item" data-id="'.$xData->menu_arrange_id.'">
												<div class="dd-handle dd3-handle">
												</div>
												<div class="dd3-content" style="padding:0px;height:auto;padding-left:30px">
													 <div class="panel panel-default" style="margin-bottom:0px;border:none;">
														<div class="panel-heading" style="border:none;padding-top:6px;height:28px;">
															<h4 class="panel-title">
															'.((!empty($xData->menu_arrange_name)?$xData->menu_arrange_name:$xData->access->access_name)).'
															</h4>
														</div>
													</div>
												</div>
												
												<ol class="dd-list">';
												foreach($subchild as $subData){
													
													$output.='<li class="dd-item dd3-item" data-id="'.$subData->menu_arrange_id.'">
													<div class="dd-handle dd3-handle">
													</div>
													<div class="dd3-content" style="padding:0px;height:auto;padding-left:30px">
														 <div class="panel panel-default" style="margin-bottom:0px;border:none;">
															<div class="panel-heading" style="border:none;padding-top:6px;height:28px;">
																<h4 class="panel-title">
																'.((!empty($subData->menu_arrange_name)?$subData->menu_arrange_name:$subData->access->access_name)).'
																</h4>
															</div>
														</div>
													</div>
													</li>';
												}
												$output.='</ol></li>';
										}else{
											
											$output.='<li class="dd-item dd3-item" data-id="'.$xData->menu_arrange_id.'">
													<div class="dd-handle dd3-handle">
													</div>
													<div class="dd3-content" style="padding:0px;height:auto;padding-left:30px">
														 <div class="panel panel-default" style="margin-bottom:0px;border:none;">
															<div class="panel-heading" style="border:none;padding-top:6px;height:28px;">
																<h4 class="panel-title">
																'.((!empty($xData->menu_arrange_name)?$xData->menu_arrange_name:$xData->access->access_name)).'
																</h4>
															</div>
														</div>
													</div>
													</li>';
											
										}
									}
									$output.='</ol></li>';
								
								
							}else{
								$output.='<li class="dd-item dd3-item" data-id="'.$rows->menu_arrange_id.'">
								<div class="dd-handle dd3-handle">
								</div>
								<div class="dd3-content" style="padding:0px;height:auto;padding-left:30px">
									 <div class="panel panel-default" style="margin-bottom:0px;border:none;">
										<div class="panel-heading" style="border:none;padding-top:6px;height:28px;">
											<h4 class="panel-title">
											'.((!empty($rows->menu_arrange_name)?$rows->menu_arrange_name:$rows->access->access_name)).'
											</h4>
										</div>
									</div>
								</div>
								</li>';
							}
						}
						echo $output;
						?>
					
					</ol>
				</div>
			<form class="form" id="user-form" action="#" method="post">		
				<input type="hidden" id="nestable_list_1_output" name="AccessMenuArrange[save]" class="form-control col-md-12 margin-bottom-10" value="">
				<div class="form-actions">
				<hr>
				<br/>
				<br/>
					<button class="btn btn-primary" id="yw0" type="submit" name="yt0">Save Change</button>
				</div>
			</form>	
		</div>
	</div>
</div>

<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->theme->baseUrl; ?>/assets/js/jquery-nestable/jquery.nestable.css"/>
<script src="<?php echo Yii::app()->theme->baseUrl; ?>/assets/js/jquery-nestable/jquery.nestable.js"></script>
<script src="<?php echo Yii::app()->theme->baseUrl; ?>/assets/js/jquery-nestable/ui-nestable.js"></script>

<script>
$(document).ready(function(){
	UINestable.init();
});
</script>