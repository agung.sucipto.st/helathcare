<div class="view">
		<b><?php echo CHtml::encode($data->getAttributeLabel('menu_arrange_id')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->menu_arrange_id),array('view','id'=>$data->menu_arrange_id)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('menu_arrange_name')); ?>:</b>
	<?php echo CHtml::encode($data->menu_arrange_name); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('access_id')); ?>:</b>
	<?php echo CHtml::encode($data->access_id); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('menu_arrange_parent')); ?>:</b>
	<?php echo CHtml::encode($data->menu_arrange_parent); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('create_at')); ?>:</b>
	<?php echo CHtml::encode($data->create_at); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('modified_at')); ?>:</b>
	<?php echo CHtml::encode($data->modified_at); ?>
	<br />

</div>