<div class="view">
		<b><?php echo CHtml::encode($data->getAttributeLabel('gwt_id')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->gwt_id),array('view','id'=>$data->gwt_id)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('gwt_name')); ?>:</b>
	<?php echo CHtml::encode($data->gwt_name); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('gwt_description')); ?>:</b>
	<?php echo CHtml::encode($data->gwt_description); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('create_at')); ?>:</b>
	<?php echo CHtml::encode($data->create_at); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('modified_at')); ?>:</b>
	<?php echo CHtml::encode($data->modified_at); ?>
	<br />

</div>