<?php
$this->breadcrumbs=array(
	'Kelola Help Desk File',
);
$this->title=array(
	'title'=>'Kelola Help Desk File',
	'deskripsi'=>'Untuk Mengelola Help Desk File'
);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	$('.import-form').hide();
	return false;
});
$('.btn-cancel').click(function(){
	$('.import-form').toggle();
	return false;
});
$('.import-button').click(function(){
	$('.import-form').toggle();
	$('.search-form').hide();
	return false;
});
$('.search-form form').submit(function(){
	$.fn.yiiGridView.update('help-desk-files-grid', {
		data: $(this).serialize()
	});
	return false;
});

");
?>

<div class="panel panel-default">
	<div class="panel-heading">
		<h3 class="panel-title pull-left">Data</h3>
		<a href="<?php echo Yii::app()->createUrl(Yii::app()->controller->module->id.'/'.Yii::app()->controller->id.'/create');?>" class="btn btn-primary btn-xs pull-right">
			<i class="fa fa-copy"></i>
			<span>Tambah Data</span>
		</a>
		<div style="clear:both"></div>
	</div>
	<div class="panel-body">
		<div class="search-form" style="display:none">
			<?php $this->renderPartial('_search',array(
			'model'=>$model,
		)); ?>
		</div><!-- search-form -->
		<div class="import-form" style="display:none">
			<div class="xe-widget xe-counter-block" data-count=".num" data-from="0" data-to="99.9" data-suffix="%" data-duration="2">
			<div class="row">
				<div class="col-sm-7">
					<div class="xe-lower">
							
							<?php $form=$this->beginWidget('booster.widgets.TbActiveForm',array(
								'action'=>Yii::app()->createUrl(Yii::app()->controller->id.'/import'),
								'method'=>'POST',
								'htmlOptions'=>array('enctype'=>'multipart/form-data'),
							)); ?>
							<?php echo $form->fileFieldGroup($model, 'import',
									array(
										'wrapperHtmlOptions' => array(
											'class' => 'form-control',
										),
										'widgetOptions'=>array(
											'htmlOptions'=>array(
												'required'=>true,
												'accept'=>".xls"
											),
										),
										'label'=>''
										,
									)
								); ?>								<div class="text-center">
									<div class="col-sm-6">
										
									</div>
									<div class="col-sm-6">
									<button class="btn btn-success btn-icon btn-cancel">
											<i class="fa fa-times"></i>
											<span>Batal</span>
										</button>
										<button class="btn btn-success btn-icon">
											<i class="fa fa-file-o"></i>
											<span>Import</span>
										</button>
									</div>							
								</div>							
							
						<?php $this->endWidget(); ?>					</div>				
				</div>
				<div class="col-sm-5">
					<div class="xe-upper">
						<div class="xe-label">
							<!-- Excel Button --> 
							<a href="<?php echo Yii::app()->createUrl(Yii::app()->controller->id.'/downloadTemplate');?>" class="a-btn-2">
								<img src="<?php echo Yii::app()->theme->baseUrl; ?>/assets/images/button-xls.png" alt="Photos" /><span class="a-btn-2-text">Download Template</span> 
							</a>
						</div>
					</div>		
				</div>
			</div>

		

			</div>
		</div><!-- search-form -->
		<style>
		.pagination li.selected a{
			background:rgb(235, 235, 236);
		}
		</style>
		
		<div class="alert alert-info" id="info"></div>
		
		<?php
		if(Yii::app()->user->hasFlash('success')){
			echo'
			<div class="alert alert-success">
				<button type="button" class="close" data-dismiss="alert">&times;</button>
				'.Yii::app()->user->getFlash('success').'
			</div>';
		}
		?>
							
		<div id="example-2_wrapper" class="dataTables_wrapper form-inline dt-bootstrap no-footer">
		
		<?php 
		// put this somewhere on top
		$pageSize=Yii::app()->user->getState('pageSize',Yii::app()->params['defaultPageSize']); ?>		
		<?php		$this->widget('booster.widgets.TbExtendedGridView', array(
			'id'=>'help-desk-files-grid',
			'type' => 'striped',
			'dataProvider' => $model->search(),
			'filter'=>$model,
			'summaryText'=>'Menampilkan {start}-{end} dari {count} hasil.',
			'selectableRows' => 2,
			'responsiveTable' => true,
			'enablePagination' => true,
			'pager' => array(
				'htmlOptions'=>array(
					'class'=>'pagination'
				),
				'maxButtonCount' => 5,
				'cssFile' => true,
				'header' => false,
				'firstPageLabel' => '<<',
				'prevPageLabel' => '<',
				'nextPageLabel' => '>',
				'lastPageLabel' => '>>',
			),
			'bulkActions' => array(
			'actionButtons' => array(
				array(
					'buttonType' => 'button',
					'context' => 'primary',
					'size' => 'small',
					'label' => 'Hapus yang ditandai',
					'click' => 'js:confirmDelete',
					'id'=>'hdf_id'
					
					),
				/*array(
					'buttonType' => 'button',
					'context' => 'primary',
					'size' => 'small',
					'label' => 'Export Excel',
					'click' => 'js:exportDataExcel',
					'id'=>'exportExcel'
					),
				array(
					'buttonType' => 'button',
					'context' => 'primary',
					'size' => 'small',
					'label' => 'Export PDF',
					'click' => 'js:exportDataPdf',
					'id'=>'exportPdf'
					)*/
			),
				'checkBoxColumnConfig' => array(
					'name' => 'hdf_id'
				),
			),
			'columns'=>array(
										'hdf_id',
					'hdsk_id',
					'hdf_files',
					'create_at',
					'modified_at',
					array(
						'class'=>'booster.widgets.TbButtonColumn',
						 'deleteConfirmation'=>'Anda yakin akan menhapus data?',
						'template'=>'{view}{update}{delete}',
						'buttons'=>array
						(
							'view' => array
							(
								'label'=>'View',
								'icon'=>'search',
								'options'=>array(
									'class'=>'btn btn-default btn-xs',
								),
							),
							'update' => array
							(
								'label'=>'Update',
								'icon'=>'pencil',
								'options'=>array(
									'class'=>'btn btn-default btn-xs',
								),
							),
							'delete' => array
							(
								'label'=>'Delete',
								'icon'=>'trash',
								'options'=>array(
									'class'=>'btn btn-default btn-xs delete',
								),
							)
						),
						'header'=>CHtml::dropDownList('pageSize',$pageSize,array(10=>10,20=>20,50=>50,100=>100,200=>200,500=>500,1000=>1000),array(
							'onchange'=>"$.fn.yiiGridView.update('help-desk-files-grid',{ data:{pageSize: $(this).val() }})",
						)),
					),
			),
		));
		?>
				
		<form action="" method="POST" id="formDownload" target="_blank">
			<input type="hidden" name="ids" id="ids"/>
		</form>
		
		<script type="text/javascript">
			$("#info").hide();
			var gridId = "help-desk-files-grid";
			var defaultMessage='';
			var values;
			function confirmDelete(data){
				values=data;
				if(defaultMessage!=''){
					$(".modal-body").html(defaultMessage);
					$(".modal-footer").show();
				}
				$("#modalKonfirmasi").modal('show');
			}
			$(function(){
				$("#modalKonfirmasiOk").click(function(){
					if(defaultMessage==''){
						defaultMessage=$(".modal-body").html();
					}
					$(".modal-body").html('Sedang Menghapus data...');
					$(".modal-footer").hide();
					batchActions(values);
				});
			});
			function batchActions(values){				
					$.ajax({
						type: "POST",
						url: '<?php echo Yii::app()->createUrl(Yii::app()->controller->id.'/batchDelete');?>',
						data: {"ids":values},
						dataType:'json',
						success: function(resp){
							if(resp.status == "success"){
							   $.fn.yiiGridView.update(gridId);
							   if(resp.data.failureCount>0){
									$("#info").html(resp.data.successCount+' Berhasil Di Hapus & '+resp.data.failureCount+' Gagal Di Hapus !<br/>Data yang gagal di hapus : '+resp.data.dataError);
							   }else{
									$("#info").html(resp.data.successCount+' Berhasil Di Hapus & '+resp.data.failureCount+' Gagal Di Hapus');
							   }
							   $("#info").fadeIn(5000);
							   $("#info").fadeOut(3000);
							}else{
							   $("#info").html(resp.msg);
							   $("#info").fadeIn(5000);
							   $("#info").fadeOut(5000);
							}
							$("#modalKonfirmasi").modal('hide');
						}
					});
			}
			function exportDataExcel(values){	
				$("#formDownload").attr('action','<?php echo Yii::app()->createUrl(Yii::app()->controller->id.'/exportExcel');?>');
				$("#ids").val(values.toString());
				$("#formDownload").submit();
			}
			
			function exportDataPdf(values){	
				$("#formDownload").attr('action','<?php echo Yii::app()->createUrl(Yii::app()->controller->id.'/exportPdf');?>');
				$("#ids").val(values.toString());
				$("#formDownload").submit();
			}
		</script>
		</div>
	</div>
</div>