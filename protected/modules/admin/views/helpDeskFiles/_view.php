<div class="view">
		<b><?php echo CHtml::encode($data->getAttributeLabel('hdf_id')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->hdf_id),array('view','id'=>$data->hdf_id)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('hdsk_id')); ?>:</b>
	<?php echo CHtml::encode($data->hdsk_id); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('hdf_files')); ?>:</b>
	<?php echo CHtml::encode($data->hdf_files); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('create_at')); ?>:</b>
	<?php echo CHtml::encode($data->create_at); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('modified_at')); ?>:</b>
	<?php echo CHtml::encode($data->modified_at); ?>
	<br />

</div>