<div class="view">
		<b><?php echo CHtml::encode($data->getAttributeLabel('qo_id')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->qo_id),array('view','id'=>$data->qo_id)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('qq_id')); ?>:</b>
	<?php echo CHtml::encode($data->qq_id); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('qo_name')); ?>:</b>
	<?php echo CHtml::encode($data->qo_name); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('create_at')); ?>:</b>
	<?php echo CHtml::encode($data->create_at); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('modified_at')); ?>:</b>
	<?php echo CHtml::encode($data->modified_at); ?>
	<br />

</div>