<?php
$this->breadcrumbs=array(
	'Daftar registrasi Pasien',
);
$this->title=array(
	'title'=>'Daftar registrasi Pasien',
	'deskripsi'=>'Untuk Melihat Daftar registrasi Pasien'
);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	$('.import-form').hide();
	return false;
});
$('.btn-cancel').click(function(){
	$('.import-form').toggle();
	return false;
});
$('.import-button').click(function(){
	$('.import-form').toggle();
	$('.search-form').hide();
	return false;
});
$('.search-form form').submit(function(){
	$.fn.yiiGridView.update('survey-grid', {
		data: $(this).serialize()
	});
	return false;
});
$('#activeKan').click(function(){
	var device = $('#devId').val();
	var id = $('#activeKan').attr('data');
	var url = '".Yii::app()->createAbsoluteUrl(Yii::app()->controller->module->id."/".Yii::app()->controller->id."/setStatus")."'+'/id/'+id+'/sd/'+device;
	window.location = url;
});


$('#dissmiss').click(function(){
	var id = $('#dissmiss').attr('data');
	
	
	$('#noReg').html('');
	$('#noMr').html('');
	$('#Nama').html('');
	$('#Layanan').html('');
	$('#Jaminan').html('');
	$('#questionerList').html('');
	$.ajax({
        url: '".CController::createUrl('processAlert')."',
        type: 'post',
        data: 'id='+id+'&act=0',
        success: function (event) {                
			$('#myModalAlert').modal('hide');
			modals=null;
        },
        error: function(jqXHR, textStatus, errorThrown) {
           console.log(textStatus, errorThrown);
        }
    });
});

$('#dissmiss2').click(function(){
	var id = $('#dissmiss2').attr('data');
	
	
	$('#noReg').html('');
	$('#noMr').html('');
	$('#Nama').html('');
	$('#Layanan').html('');
	$('#Jaminan').html('');
	$('#questionerList').html('');
	$.ajax({
        url: '".CController::createUrl('processAlert')."',
        type: 'post',
        data: 'id='+id+'&act=2' ,
        success: function (event) {                
			$('#myModalAlert').modal('hide');
			modals=null;
        },
        error: function(jqXHR, textStatus, errorThrown) {
           console.log(textStatus, errorThrown);
        }
    });
});
");
?>
<script>
function pop(id){
	$.fn.yiiGridView.update('survey-grid');
	$('#myModal').modal('show');
	$('#activeKan').attr('data',id);
	
	$.ajax({
        url: "<?php echo CController::createUrl('getDevice');?>",
        type: "post",
        data: "id=1" ,
        success: function (event) {                
			$('#devId').html(event);
        },
        error: function(jqXHR, textStatus, errorThrown) {
           console.log(textStatus, errorThrown);
        }
    });
	
	$.ajax({
        url: "<?php echo CController::createUrl('getAction');?>",
        type: "post",
        data: "id=1" ,
        success: function (event) {
			if(event!='no'){
				$('#activeKan').show();
			}else{
				$('#activeKan').hide();
			}
			
        },
        error: function(jqXHR, textStatus, errorThrown) {
           console.log(textStatus, errorThrown);
        }
    });
	
	$.ajax({
        url: "<?php echo CController::createUrl('getCurrent');?>",
        type: "post",
        data: "id=1" ,
        success: function (event) {    
			if(event!=''){
				$('#notification').show();
				$('#notification').html(event);
			}
			
        },
        error: function(jqXHR, textStatus, errorThrown) {
           console.log(textStatus, errorThrown);
        }
    });
	
}

var modals= null;
function check(){
	$.ajax({
        url: "<?php echo CController::createUrl('getAlert');?>",
        type: "post",
        data: "id=1" ,
        success: function (event) { 
			var msg = $.parseJSON(event);
			if(msg.data=='yes'){
				new Audio("<?=Yii::app()->theme->baseUrl;?>/assets/audio/notif.mp3").play();
				if(modals==null){
					$('#myModalAlert').modal('show');
					$("#noReg").html(msg.no_reg);
					$("#noMr").html(msg.no_mr);
					$("#Nama").html(msg.nama);
					$("#Layanan").html(msg.layanan);
					$("#Jaminan").html(msg.jaminan);
					$('#dissmiss').attr('data',msg.sv_id);
					$('#dissmiss2').attr('data',msg.sv_id);
					if(msg.option.length>0){
						for(i=0;i<msg.option.length;i++){
							var list = msg.option[i];
							$("#questionerList").append(list);
						}
					}
					
					modals = true;
				}
			}else{
				$('#myModalAlert').modal('hide');
				$("#noReg").html('');
				$("#noMr").html('');
				$("#Nama").html('');
				$("#Layanan").html('');
				$("#Jaminan").html('');
				$("#questionerList").html('');
			}
						
        },
        error: function(jqXHR, textStatus, errorThrown) {
           console.log(textStatus, errorThrown);
        }
    });
}

var interval = setInterval(check, 1000);
</script>	
<div class="panel panel-default">
	<div class="panel-heading">
		<h3 class="panel-title pull-left">Data</h3>
		<a href="<?php echo Yii::app()->createUrl(Yii::app()->controller->module->id.'/'.Yii::app()->controller->id.'/rekapitulasi');?>" class="btn btn-primary btn-xs pull-right">
			<i class="fa fa-question-circle"></i>
			<span>Rekapitulasi</span>
		</a>
		<a href="<?php echo Yii::app()->createUrl(Yii::app()->controller->module->id.'/'.Yii::app()->controller->id.'/questionerList');?>" class="btn btn-primary btn-xs pull-right">
			<i class="fa fa-question-circle"></i>
			<span>Questioner List</span>
		</a>
		<a href="<?php echo Yii::app()->createUrl(Yii::app()->controller->module->id.'/'.Yii::app()->controller->id.'/createManual');?>" class="btn btn-danger btn-xs pull-right">
			<i class="fa fa-plus"></i>
			<span>Questioner Manual</span>
		</a>
		<a href="<?php echo Yii::app()->createUrl(Yii::app()->controller->module->id.'/'.Yii::app()->controller->id.'/manual');?>" class="btn btn-danger btn-xs pull-right">
			<i class="fa fa-list"></i>
			<span>Questioner Manual List</span>
		</a>
		<div style="clear:both"></div>
	</div>
	<div class="panel-body">
		<style>
		.pagination li.selected a{
			background:rgb(235, 235, 236);
		}
		</style>
		
		<div class="alert alert-info" id="info"></div>
		
		<div class="row">
		<div class="search-form">
		<?php $form=$this->beginWidget('booster.widgets.TbActiveForm',array(
			'action'=>Yii::app()->createUrl($this->route),
			'method'=>'get',
		)); ?>

				<div class="col-md-3">
				<?php echo $form->datePickerGroup($model,'reg_date_in',array("label"=>"From",'widgetOptions'=>array('options'=>array('format'=>'yyyy-mm-dd','viewFormat'=>'yyyy-mm-dd'),'htmlOptions'=>array('class'=>'span5',"placeholder"=>"From")), 'prepend'=>'<i class="glyphicon glyphicon-calendar"></i>', )); ?>
				</div>
				<div class="col-md-3">
				<?php echo $form->datePickerGroup($model,'create_at',array("label"=>"To",'widgetOptions'=>array('options'=>array('format'=>'yyyy-mm-dd','viewFormat'=>'yyyy-mm-dd'),'htmlOptions'=>array('class'=>'span5',"placeholder"=>"To")), 'prepend'=>'<i class="glyphicon glyphicon-calendar"></i>')); ?>
				</div>
				<div class="col-md-3">
				<?php echo $form->dropDownListGroup($model,'reg_type', array("label"=>"Jenis Registrasi",'widgetOptions'=>array('data'=>array("INPATIENT"=>"INPATIENT","OUTPATIENT"=>"OUTPATIENT"), 'htmlOptions'=>array('class'=>'input-large','empty'=>'- Choose -')))); ?>
				</div>
				<div class="col-md-3" style="padding-top:25px">
				<?php $this->widget('booster.widgets.TbButton', array(
					'buttonType' => 'submit',
					'context'=>'primary',
					'label'=>'Search',
				)); ?>
				</div>

		<?php $this->endWidget(); ?>
		</div>
		</div>
		
		<?php
		if(Yii::app()->user->hasFlash('success')){
			echo'
			<div class="alert alert-success">
				<button type="button" class="close" data-dismiss="alert">&times;</button>
				'.Yii::app()->user->getFlash('success').'
			</div>';
		}
		?>
							
		<div id="example-2_wrapper" class="dataTables_wrapper form-inline dt-bootstrap no-footer">
		
		<?php 
		// put this somewhere on top
		$pageSize=Yii::app()->user->getState('pageSize',Yii::app()->params['defaultPageSize']); ?>		
		<?php		
		$this->widget('booster.widgets.TbExtendedGridView', array(
			'id'=>'survey-grid',
			'type' => 'striped',
			'dataProvider' => $model->searchNonCreate(),
			'filter'=>$model,
			'summaryText'=>'Menampilkan {start}-{end} dari {count} hasil.',
			'selectableRows' => 2,
			'afterAjaxUpdate' => 'reinstallDatePicker',
			'responsiveTable' => true,
			'enablePagination' => true,
			'pager' => array(
				'htmlOptions'=>array(
					'class'=>'pagination'
				),
				'maxButtonCount' => 5,
				'cssFile' => true,
				'header' => false,
				'firstPageLabel' => '<<',
				'prevPageLabel' => '<',
				'nextPageLabel' => '>',
				'lastPageLabel' => '>>',
			),
			'bulkActions' => array(
			'actionButtons' => array(
				array(
					'buttonType' => 'button',
					'context' => 'primary',
					'size' => 'small',
					'label' => 'Hapus yang ditandai',
					'click' => 'js:confirmDelete',
					'id'=>'sv_id'
					
					),
				/*array(
					'buttonType' => 'button',
					'context' => 'primary',
					'size' => 'small',
					'label' => 'Export Excel',
					'click' => 'js:exportDataExcel',
					'id'=>'exportExcel'
					),
				array(
					'buttonType' => 'button',
					'context' => 'primary',
					'size' => 'small',
					'label' => 'Export PDF',
					'click' => 'js:exportDataPdf',
					'id'=>'exportPdf'
					)*/
			),
				'checkBoxColumnConfig' => array(
					'name' => 'sv_id'
				),
			),
			'columns'=>array(
					array(
						"header"=>"No Reg",
						"value"=>'$data->reg_no',
						"name"=>"reg_no"
					),
					array(
						"header"=>"MRN",
						"value"=>'Lib::MRN($data->reg->p->p_id)',
						"name"=>"p_id"
					),
					array(
						"header"=>"Nama Pasien",
						"value"=>'$data->reg->p->p_name." ".$data->reg->p->p_title',
						"name"=>"p_name"
					),
					array(
						"header"=>"Jenis Kelamin",
						"value"=>'($data->reg->p->p_gender=="Male")?"L":"P"',
						"name"=>"p_gender",
						"filter"=>array("Male"=>"L","Female"=>"P")
					),
					array(
						"header"=>"Date",
						"value"=>'Lib::dateInd($data->reg_date_in)',
						"name"=>"reg_date_in",
						/*
						'filter' => $this->widget('zii.widgets.jui.CJuiDatePicker', 
							array(
								'model' => $model,
								'attribute' => 'reg_date_in',
								'htmlOptions' => array(
									'id' => 'registrastion_reg_date_in',
									'dateFormat' => 'yy-mm-dd',
									'class'=>'form-control'
								),
								'options' => array(
									'showOn' => 'focus', 
									'dateFormat' => 'yy-mm-dd',
									'showOtherMonths' => true,
									'selectOtherMonths' => false,
									'changeMonth' => false,
									'changeYear' => false,
								)
							),
						true),
						*/
						
					),
					array(
						"header"=>"Out",
						"value"=>'Lib::dateInd($data->reg_is_out)',
						"name"=>"reg_is_out"
					),
					array(
						"header"=>"Guarator",
						"value"=>'$data->reg_guarator',
						"name"=>"reg_guarator"
					),
					array(
						"header"=>"Type",
						"value"=>'$data->reg_type',
						"name"=>"reg_type",
						"filter"=>array("INPATIENT"=>"INPATIENT","OUTPATIENT"=>"OUTPATIENT")
					),
					array(
						"header"=>"Departement",
						"value"=>'$data->reg_dept',
						"name"=>"reg_dept"
					),
					array(
						"header"=>"Questioner",
						"type"=>"raw",
						"name"=>"reg_questioner",
						"filter"=>array("Active"=>"Active","Inactive"=>"Inactive"),
						"value"=>function($data){
							if($data->reg_questioner=="Inactive"){
								return '<button data="'.Yii::app()->createUrl(Yii::app()->controller->module->id."/".Yii::app()->controller->id."/setStatus",array("id"=>$data->reg_id)).'" class="btn btn-success" onClick="pop('.$data->reg_id.');">Aktifkan Questioner</button>';
							}else{
								return '<a href="'.Yii::app()->createUrl(Yii::app()->controller->module->id."/".Yii::app()->controller->id."/setStatus",array("id"=>$data->reg_id)).'" class="btn btn-danger" onclick="return confirm(\'Apakah anda yakin akan menonaktifkan questioner?\');">Nonaktifkan Questioner</a>';
							}
						},
					),
					array(
						'class'=>'booster.widgets.TbButtonColumn',
						 'deleteConfirmation'=>'Anda yakin akan menhapus data?',
						'template'=>'{view}',
						'buttons'=>array
						(
							'view' => array
							(
								'label'=>'Isi Questioner',
								'icon'=>'question-sign',
								'url'=>'Yii::app()->createUrl(Yii::app()->controller->module->id."/".Yii::app()->controller->id."/createQuestioner",array("id"=>$data->reg_id))',
								'options'=>array(
									'class'=>'btn btn-default btn-xs',
								),
							),
						),
						'header'=>CHtml::dropDownList('pageSize',$pageSize,array(10=>10,20=>20,50=>50,100=>100,200=>200,500=>500,1000=>1000),array(
							'onchange'=>"$.fn.yiiGridView.update('survey-grid',{ data:{pageSize: $(this).val() }})",
						)),
					),
			),
		));
		?>
		
		<?php
		Yii::app()->clientScript->registerScript('re-install-date-picker', "
		function reinstallDatePicker() {
		  $('#registrastion_reg_date_in').datepicker({dateFormat: 'yy-mm-dd'});
		}
		");
		?>
				
		<form action="" method="POST" id="formDownload" target="_blank">
			<input type="hidden" name="ids" id="ids"/>
		</form>
		
		<script type="text/javascript">
			$("#info").hide();
			var gridId = "survey-grid";
			var defaultMessage='';
			var values;
			function confirmDelete(data){
				values=data;
				if(defaultMessage!=''){
					$(".modal-body").html(defaultMessage);
					$(".modal-footer").show();
				}
				$("#modalKonfirmasi").modal('show');
			}
			$(function(){
				$("#modalKonfirmasiOk").click(function(){
					if(defaultMessage==''){
						defaultMessage=$(".modal-body").html();
					}
					$(".modal-body").html('Sedang Menghapus data...');
					$(".modal-footer").hide();
					batchActions(values);
				});
			});
			function batchActions(values){				
					$.ajax({
						type: "POST",
						url: '<?php echo Yii::app()->createUrl(Yii::app()->controller->id.'/batchDelete');?>',
						data: {"ids":values},
						dataType:'json',
						success: function(resp){
							if(resp.status == "success"){
							   $.fn.yiiGridView.update(gridId);
							   if(resp.data.failureCount>0){
									$("#info").html(resp.data.successCount+' Berhasil Di Hapus & '+resp.data.failureCount+' Gagal Di Hapus !<br/>Data yang gagal di hapus : '+resp.data.dataError);
							   }else{
									$("#info").html(resp.data.successCount+' Berhasil Di Hapus & '+resp.data.failureCount+' Gagal Di Hapus');
							   }
							   $("#info").fadeIn(5000);
							   $("#info").fadeOut(3000);
							}else{
							   $("#info").html(resp.msg);
							   $("#info").fadeIn(5000);
							   $("#info").fadeOut(5000);
							}
							$("#modalKonfirmasi").modal('hide');
						}
					});
			}
			function exportDataExcel(values){	
				$("#formDownload").attr('action','<?php echo Yii::app()->createUrl(Yii::app()->controller->id.'/exportExcel');?>');
				$("#ids").val(values.toString());
				$("#formDownload").submit();
			}
			
			function exportDataPdf(values){	
				$("#formDownload").attr('action','<?php echo Yii::app()->createUrl(Yii::app()->controller->id.'/exportPdf');?>');
				$("#ids").val(values.toString());
				$("#formDownload").submit();
			}
		</script>
		</div>
	</div>
</div>


<div id="myModal" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Questioner Device</h4>
      </div>
      <div class="modal-body">
	  <div class="alert alert-info" id="notification" style="display:none;"></div>
	  <select name="device" class="form-control" id="devId">
       <?php
			$device=SurveyDevice::model()->findAll(array("condition"=>"sd_status='Active'"));
			$display=0;
			foreach($device as $row){
				$cek=Registration::model()->count(array("condition"=>"reg_questioner='Active' and sd_id='$row->sd_id'"));
				if($cek==0){
					$display++;
					echo'<option value="'.$row->sd_id.'">'.$row->sd_name.' | '.$row->sd_location.'</option>';
				}
			}
	   ?>
	   </select>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-success" id="activeKan" data="">Active</button>
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>

  </div>
</div>


<div id="myModalAlert" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Questioner Alert</h4>
      </div>
      <div class="modal-body">
		<table class="table table-stripped">
			<tr>
				<th>No Registrasi</td><td id="noReg"></td><th>MR</td><td id="noMr"></td>
			</tr><tr>
				<th>Nama</td><td id="Nama"></td>
			</tr><tr>
				<th>Layanan</td><td id="Layanan"></td>
			</tr><tr>
				<th>Jaminan</td><td id="Jaminan"></td>
			</tr><tr>
				<th>Questioner</td><td><ul id="questionerList"></ul></td>
			</tr>
			
		</table>
      </div>
      <div class="modal-footer">
	  <button type="button" class="btn btn-info" id="dissmiss2" data="">Konfirmasi + Berikan Gimic</button>
	  <button type="button" class="btn btn-success" id="dissmiss" data="">Konfirmasi</button>
      </div>
    </div>

  </div>
</div>