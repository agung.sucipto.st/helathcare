<?php
$this->breadcrumbs=array(
	'Registrasi Pasien'=>array('index'),
	'Kelola Survey',
);
$this->title=array(
	'title'=>'Kelola Survey Manual',
	'deskripsi'=>'Untuk Mengelola Survey'
);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	$('.import-form').hide();
	return false;
});
$('.btn-cancel').click(function(){
	$('.import-form').toggle();
	return false;
});
$('.import-button').click(function(){
	$('.import-form').toggle();
	$('.search-form').hide();
	return false;
});
$('.search-form form').submit(function(){
	$.fn.yiiGridView.update('survey-grid', {
		data: $(this).serialize()
	});
	return false;
});

");
?>

<div class="panel panel-default">
	<div class="panel-heading">
		<h3 class="panel-title pull-left">Data</h3>
		<a href="<?php echo Yii::app()->createUrl(Yii::app()->controller->module->id.'/'.Yii::app()->controller->id.'/rekapitulasi');?>" class="btn btn-primary btn-xs pull-right">
			<i class="fa fa-question-circle"></i>
			<span>Rekapitulasi</span>
		</a>
		<a href="<?php echo Yii::app()->createUrl(Yii::app()->controller->module->id.'/'.Yii::app()->controller->id);?>" class="btn btn-primary btn-xs pull-right">
			<i class="fa fa-arrow-circle-left"></i>
			<span>Kembali</span>
		</a>
		<a href="<?php echo Yii::app()->createUrl(Yii::app()->controller->module->id.'/'.Yii::app()->controller->id.'/createManual');?>" class="btn btn-danger btn-xs pull-right">
			<i class="fa fa-plus"></i>
			<span>Questioner Manual</span>
		</a>
		<div style="clear:both"></div>
	</div>
	<div class="panel-body">
		<style>
		.pagination li.selected a{
			background:rgb(235, 235, 236);
		}
		</style>
		
		<div class="alert alert-info" id="info"></div>
		<div class="row">
		<div class="search-form">
	
							
		<div id="example-2_wrapper" class="dataTables_wrapper form-inline dt-bootstrap no-footer">
		
		<?php 
		// put this somewhere on top
		$pageSize=Yii::app()->user->getState('pageSize',Yii::app()->params['defaultPageSize']); ?>		
		<?php		
		$this->widget('booster.widgets.TbExtendedGridView', array(
			'id'=>'survey-grid',
			'type' => 'striped',
			'dataProvider' => $model->searchManual(),
			'filter'=>$model,
			'summaryText'=>'Menampilkan {start}-{end} dari {count} hasil.',
			'selectableRows' => 2,
			'responsiveTable' => true,
			'enablePagination' => true,
			'pager' => array(
				'htmlOptions'=>array(
					'class'=>'pagination'
				),
				'maxButtonCount' => 5,
				'cssFile' => true,
				'header' => false,
				'firstPageLabel' => '<<',
				'prevPageLabel' => '<',
				'nextPageLabel' => '>',
				'lastPageLabel' => '>>',
			),
			'bulkActions' => array(
			'actionButtons' => array(
				
				array(
					'buttonType' => 'button',
					'context' => 'primary',
					'size' => 'small',
					'label' => 'Export Excel',
					'click' => 'js:exportDataExcel',
					'id'=>'exportExcel'
					),
				array(
					'buttonType' => 'button',
					'context' => 'primary',
					'size' => 'small',
					'label' => 'Print',
					'click' => 'js:printMe',
					'id'=>'printDataMe'
					)
			),
				'checkBoxColumnConfig' => array(
					'name' => 'sv_id'
				),
			),
			'columns'=>array(
					
					array(
						"header"=>"Nama Pasien",
						"type"=>"raw",
						"value"=>function($data){
							if($data->reg_id!=''){
								return $data->reg->p->p_name." ".$data->reg->p->p_title;
							}else{
								return $data->sv_person_name.'<br/>'.$data->sv_person_number;
							}
						},
						"name"=>"p_name"
					),
					array(
						"header"=>"Questioner",
						"type"=>"raw",
						"name"=>"sv_status",
						"filter"=>array("Open"=>"Open","Finish"=>"Finish",""=>""),
						"value"=>function($data){
							if($data->sv_status==""){
								return '<a href="'.Yii::app()->createUrl(Yii::app()->controller->module->id."/".Yii::app()->controller->id."/changeStatus",array("id"=>$data->sv_id)).'" class="btn btn-success" onclick="return confirm(\'Apakah anda yakin akan mengaktifkan questioner?\');">Aktifkan Questioner</a>';
							}elseif($data->sv_status=="Open"){
								return '<a href="'.Yii::app()->createUrl(Yii::app()->controller->module->id."/".Yii::app()->controller->id."/changeStatus",array("id"=>$data->sv_id)).'" class="btn btn-danger" onclick="return confirm(\'Apakah anda yakin akan menonaktifkan questioner?\');">Nonaktifkan Questioner</a>';
							}else{
								return $data->sv_status;
							}
						},
					),
					'sv_note',
					array(
						'class'=>'booster.widgets.TbButtonColumn',
						 'deleteConfirmation'=>'Anda yakin akan menhapus data?',
						'template'=>'{view}{delete}',
						'buttons'=>array
						(
							'view' => array
							(
								'label'=>'Isi Questioner',
								'icon'=>'search',
								'url'=>'Yii::app()->createUrl(Yii::app()->controller->module->id."/".Yii::app()->controller->id."/view",array("id"=>$data->sv_id))',
								'options'=>array(
									'class'=>'btn btn-default btn-xs',
								),
							),
							'delete' => array
							(
								'label'=>'Hapus',
								'icon'=>'trash',
								'options'=>array(
									'class'=>'btn btn-default btn-xs delete',
								),
							),
						),
						'header'=>CHtml::dropDownList('pageSize',$pageSize,array(10=>10,20=>20,50=>50,100=>100,200=>200,500=>500,1000=>1000),array(
							'onchange'=>"$.fn.yiiGridView.update('survey-grid',{ data:{pageSize: $(this).val() }})",
						)),
					),
			),
		));
		?>
				
		<form action="" method="POST" id="formDownload" target="_blank">
			<input type="hidden" name="ids" id="ids"/>
		</form>
		
		<script type="text/javascript">
			$("#info").hide();
			var gridId = "survey-grid";
			var defaultMessage='';
			var values;
			
			function exportDataExcel(values){	
				$("#formDownload").attr('action','<?php echo Yii::app()->createUrl(Yii::app()->controller->module->id."/".Yii::app()->controller->id.'/exportExcel');?>');
				$("#ids").val(values.toString());
				$("#formDownload").submit();
			}
			
			function printMe(values){	
				$("#formDownload").attr('action','<?php echo Yii::app()->createUrl(Yii::app()->controller->module->id."/".Yii::app()->controller->id.'/print');?>');
				$("#ids").val(values.toString());
				$("#formDownload").submit();
			}
		</script>
		</div>
	</div>
</div>