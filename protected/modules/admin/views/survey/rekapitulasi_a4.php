 <?php
$baseUrl = Yii::app()->theme->baseUrl; 
$cs = Yii::app()->getClientScript();
$cs->registerScriptFile($baseUrl.'/assets/js/jquery-1.11.1.min.js');
$cs->registerScriptFile($baseUrl.'/assets/js/jquery.PrintArea.js');

$cs->registerScriptFile($baseUrl.'/assets/highcharts/highcharts.js');
$cs->registerScriptFile($baseUrl.'/assets/highcharts/modules/exporting.js');
?>

	<style>
		@media print {
		  #cetak,#cetak2{
			  display:none;
		  }
		  textarea{
				border:white;
		  }
		}
		
		.papersize{
			width:277mm;
			height:190mm;
			padding:10mm
		}
	</style>
<?php
Yii::app()->clientScript->registerScript('validate', '
(function($) {
	// fungsi dijalankan setelah seluruh dokumen ditampilkan
	$(document).ready(function(e) {
		 
		// aksi ketika tombol cetak ditekan
		$("#cetak").bind("click", function(event) {
			$(".papersize").printArea();
		});
	});
}) (jQuery);

Highcharts.chart("container", {
    chart: {
        type: "column"
    },
    title: {
        text: "Grafik '.$title.'"
    },
    xAxis: {
        categories: ['.implode(",",$hLabel).'],
		labels: {
			style: {
				fontSize:"8px"
			}
		}
    },
    credits: {
        enabled: false
    },
    series: ['.implode(",",$hValues).']
});


jQuery("body").on("change","#Survey_qc_id",function(){jQuery.ajax({"type":"POST","url":"'.Yii::app()->createAbsoluteUrl(Yii::app()->controller->module->id.'/'.Yii::app()->controller->id.'/combo').'","cache":false,"data":jQuery(this).parents("form").serialize(),"success":function(html){jQuery("#update").html(html)}});return false;});
', CClientScript::POS_END);
?>
<button id="cetak" class="btn btn-success">Cetak</button>
<a id="cetak2" class="btn btn-danger" href="<?php echo Yii::app()->createUrl(Yii::app()->controller->module->id.'/'.Yii::app()->controller->id."/rekapitulasi");?>">Back</a>
			<div class="papersize">
		
				<center>
					<div id="container" style="width: 1200px; height: 400px; margin: 0 auto"></div>
				</center>
				<hr>
				<table class="table">
				<?php
				echo'<tr><th>Notasi</th>';
				echo'<th>Penilaian</th>';
				foreach($detail as $x=>$y){
					echo"<td>$y</td>";
				}
				echo"<td>Tidak Ada Jawaban</td>";
				echo"<td>Total Koresponden</td>";
				echo'</tr>';
				foreach($table as $index=>$val){
					echo "<tr><th>$alias[$index]</th>";
					echo "<th>$index</th>";
					$tot=0;
					foreach($detail as $x=>$y){
						if($val[$y]==0){
							echo"<td>0</td>";
							$tot+=0;
						}else{
							echo"<td>$val[$y]</td>";
							$tot+=$val[$y];
						}
						
						
					}
					echo"<td>".($group-$tot)."</td>";
					echo"<td>".($group)."</td>";
					echo'</tr>';
				}
				
				?>
				</table>
			
			</div>
