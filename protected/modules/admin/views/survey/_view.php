<div class="view">
		<b><?php echo CHtml::encode($data->getAttributeLabel('sv_id')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->sv_id),array('view','id'=>$data->sv_id)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('reg_id')); ?>:</b>
	<?php echo CHtml::encode($data->reg_id); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('sv_time')); ?>:</b>
	<?php echo CHtml::encode($data->sv_time); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('sv_status')); ?>:</b>
	<?php echo CHtml::encode($data->sv_status); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('create_at')); ?>:</b>
	<?php echo CHtml::encode($data->create_at); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('modified_at')); ?>:</b>
	<?php echo CHtml::encode($data->modified_at); ?>
	<br />

</div>