<?php
$baseUrl = Yii::app()->theme->baseUrl; 
$cs = Yii::app()->getClientScript();
$cs->registerScriptFile($baseUrl.'/assets/js/jquery-1.11.1.min.js');
$cs->registerScriptFile($baseUrl.'/assets/morris/raphael-min.js');
$cs->registerScriptFile($baseUrl.'/assets/morris/morris.js');
$cs->registerScriptFile($baseUrl.'/assets/js/jquery.PrintArea.js');
$cs->registerCssFile($baseUrl.'/assets/morris/morris.css');
?>
	<style>
		@media print {
		  #cetak,#cetak2{
			  display:none;
		  }
		  textarea{
				border:white;
		  }
		}
		
		.papersize{
			width:277mm;
			height:190mm;
			padding:10mm
		}
	</style>
<?php
$legend=array();
$labels=array();
foreach($detail as $idx=>$row){
	$legend[]='"'.$idx.'"';
	$labels[]='"'.$row.'"';
}
$color=array('"#0b62a4"','"#7a92a3"','"#4da74d"','"#afd8f8"','"#e4b448"','"#e191fb"','"#e48612"','"#e41212"');
Yii::app()->clientScript->registerScript('validate', '
(function($) {
	// fungsi dijalankan setelah seluruh dokumen ditampilkan
	$(document).ready(function(e) {
		 
		// aksi ketika tombol cetak ditekan
		$("#cetak").bind("click", function(event) {
			$(".papersize").printArea();
		});
	});
}) (jQuery);
var colors_array= ['.implode(",",$color).'];	
window.m=Morris.Bar({
  element: "bar-example",
  barColors: colors_array,
  data: [
    '.implode(",",$data).'
  ],
  xkey: "y",
  ykeys: ['.implode(",",$legend).'],
  labels: ['.implode(",",$labels).'],
  hideHover: true,
  resize: true,
  grid: true,
});



$(window).on("resize", function(){
  m.redraw();
});

$(window).on("print", function(){
  m.redraw();
});

jQuery("body").on("change","#Survey_qc_id",function(){jQuery.ajax({"type":"POST","url":"'.Yii::app()->createAbsoluteUrl(Yii::app()->controller->module->id.'/'.Yii::app()->controller->id.'/combo').'","cache":false,"data":jQuery(this).parents("form").serialize(),"success":function(html){jQuery("#update").html(html)}});return false;});
', CClientScript::POS_END);
?>

			<div class="papersize">
			<center><h4>Grafik <?php echo $title;?></h4></center>
		
				<center>
					<div id="bar-example">
					</div>
				</center>
				<hr>
				<table class="table">
				<?php
				echo'<tr><th>Notasi</th>';
				echo'<th>Penilaian</th>';
				foreach($detail as $x=>$y){
					echo"<td>$y</td>";
				}
				echo"<td>Tidak Ada Jawaban</td>";
				echo"<td>Total Koresponden</td>";
				echo'</tr>';
				foreach($table as $index=>$val){
					echo "<tr><th>$alias[$index]</th>";
					echo "<th>$index</th>";
					$tot=0;
					foreach($detail as $x=>$y){
						if($val[$y]==0){
							echo"<td>0</td>";
							$tot+=0;
						}else{
							echo"<td>$val[$y]</td>";
							$tot+=$val[$y];
						}
						
						
					}
					echo"<td>".($group-$tot)."</td>";
					echo"<td>".($group)."</td>";
					echo'</tr>';
				}
				
				?>
				</table>
				
				<textarea style="width:100%;"></textarea>
			</div>
