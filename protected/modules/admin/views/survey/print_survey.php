<html>
	<style>
	@media print {
	   thead {display: table-header-group;}
	}
	table{
		border:1px solid #999999;
		width:100%;
		font-size:10px;
		border-collapse:collapse;
	}
	td,th{
		border:1px solid #999999;
		border-collapse:collapse;
		padding:2px;
		padding-left:5px;
	}
	th{
		text-align:center;
		padding:10px;
	}
	</style>
	<body>
		<center>
		<h2>Rekapitulasi Survey</h2>
		<h4>Aulia Hospital pekanbaru</h4>
		</center>
		<div class="table">
			<table>
			<thead>
			  <tr>
				 <th>No</th>
				 <th style="width:180px;">No Reg.</th>
				 <th>MRN</th>
				 <th>Nama Pasien</th>
				 <th>Jenis Kelamin</th>
				 <th>Penjamin</th>
				 <th>Type</th>
				 <th>Departement</th>
				 <th>Kritik & Saran</th>
			  </tr>
			</thead>
			<tbody>
			<?php
			$no=0;
			foreach($model as $data){
				$no++;
				echo'
				<tr>
					 <td>'.$no.'</td>
					 <td>'.$data->reg->reg_no."<br/>".Lib::dateInd($data->reg->reg_date_in,false).' '.((!empty($data->reg_is_out))?Lib::dateInd($data->reg->reg_is_out,false):"").'</td>
					 <td>'.Lib::MRN($data->reg->p->p_id).'</td>
					 <td>'.$data->reg->p->p_name." ".$data->reg->p->p_title.'</td>
					 <td>'.(($data->reg->p->p_gender=="Male")?"L":"P").'</td>
					 <td>'.$data->reg->reg_guarator.'</td>
					 <td>'.$data->reg->reg_type.'</td>
					 <td>'.$data->reg->reg_dept.'</td>
					 <td>'.$data->sv_note.'</td>
				  </tr>
				';
			}
			?>
			 <tbody>
			 </table>
		</div>
	</body>
</html>