<?php
$this->breadcrumbs=array(
	'Registrasi Pasien'=>array('index'),
	'Isi Questioner',
);

$this->title=array(
	'title'=>'Isi Questioner',
	'deskripsi'=>'Untuk Mengsi Questioner'
);
?>
<?php

Yii::app()->clientScript->registerScript('validate', '


function question(id) {
    window.location.href = "'.Yii::app()->createUrl(Yii::app()->controller->module->id."/".Yii::app()->controller->id."/".Yii::app()->controller->action->id."/id/".$_GET['id']).'/type/"+id;
}	

jQuery("#Survey_create_at").datetimepicker({"format":"yyyy-mm-dd hh:ii","viewFormat":"yyyy-mm-dd hh:ii","language":"ID"});
', CClientScript::POS_END);
?>

<div class="panel panel-default">
	<div class="panel-heading">
		<h3 class="panel-title pull-left">Detail</h3>
		<a href="<?php echo Yii::app()->createUrl(Yii::app()->controller->module->id.'/'.Yii::app()->controller->id);?>" class="btn btn-primary btn-xs pull-right">
			<i class="fa fa-arrow-circle-left"></i>
			<span>Kembali</span>
		</a>
		<div style="clear:both"></div>
	</div>
	<div class="panel-body">		
		<div id="example-2_wrapper" class="dataTables_wrapper dt-bootstrap no-footer">
			<?php $this->widget('booster.widgets.TbDetailView',array(
			'data'=>$model,
			'attributes'=>array(
				array(
						"label"=>"No Reg",
						"value"=>$model->reg_no,
					),
					array(
						"label"=>"MRN",
						"value"=>Lib::MRN($model->p->p_id),
					),
					array(
						"label"=>"Nama Pasien",
						"value"=>$model->p->p_name." ".$model->p->p_title,
					),
					array(
						"label"=>"Jenis Kelamin",
						"value"=>($model->p->p_gender=="Male")?"Laki-laki":"Perempuan",
					),
					array(
						"label"=>"Date",
						"value"=>Lib::dateInd($model->reg_date_in),
					),
					array(
						"label"=>"Guarator",
						"value"=>$model->reg_guarator,
					),
					array(
						"label"=>"Type",
						"value"=>$model->reg_type,
					),
			),
			)); ?>
			<hr>
			<h2>
			Questioner
			<select name="qc_id" class="form-control" onChange="question(this.value);">
			<?php
			$cat=QuestionCategory::model()->findAll(array("condition"=>"qc_status='Active'"));
			foreach($cat as $row){
				if(isset($_GET['type']) AND $_GET['type']==$row->qc_id){
					echo'<option value="'.$row->qc_id.'" selected>'.$row->qc_name.'</option>';
				}else{
					echo'<option value="'.$row->qc_id.'">'.$row->qc_name.'</option>';
				}
			}
			?>
			</select>
			</h2>
			<?php $form=$this->beginWidget('booster.widgets.TbActiveForm',array(
				'action'=>"",
				'method'=>'POST',
			)); ?>
			
			<?php echo $form->dateTimePickerGroup($reg,'reg_is_out',array('widgetOptions'=>array('options'=>array('format'=>'yyyy-mm-dd hh:ii','viewFormat'=>'yyyy-mm-dd hh:ii')))) ;?>
			
			<Label>Kritik & Saran</label>
			<input type="hidden" name="qc_id" value="<?php echo $_GET['type'];?>"/>
			<textarea class="form-control" name="note"></textarea>
			<table class="table table-striped">
			<?php
			$question=QuestionQuiz::model()->findAll(array("condition"=>"qc_id='$_GET[type]'","order"=>"qq_order ASC"));
			foreach($question as $row){
				echo'
				<tr>
					<th>'.$row->qq_question.'</th>';
					$option=QuestionOption::model()->findAll(array("condition"=>"qq_id='$row->qq_id'"));
					if(!empty($option)){
						foreach($option as $subRow){
							echo'<td>
							<label>
							  <input type="radio" name="qq['.$row->qq_id.']" value="'.$subRow->qo_id.'"/> &nbsp;'.$subRow->qo_name.'
							</label>
						  </td>';
						}
					}
				echo'
				</tr>
				';
			}
			?>
			</table>
			<input type="submit" class="btn btn-success pull-right" value="Simpan Questioner" name="Finish"/>
			<input type="submit" class="btn btn-danger pull-right" value="Draft" name="Open"/>
			<?php $this->endWidget(); ?>
		</div>
	</div>
</div>
