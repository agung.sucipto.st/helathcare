<?php
$this->breadcrumbs=array(
	'Registrasi Pasien'=>array('index'),
	'Questioner List'=>array('questionerList'),
	'Detail Questioner',
);

$this->title=array(
	'title'=>'Detail Questioner',
	'deskripsi'=>'Untuk melihat Detail Questioner'
);
?>

<?php

Yii::app()->clientScript->registerScript('validate', '


function question(id) {
    window.location.href = "'.Yii::app()->createUrl(Yii::app()->controller->module->id."/".Yii::app()->controller->id."/".Yii::app()->controller->action->id."/id/".$_GET['id']).'/type/"+id;
}	
', CClientScript::POS_END);
?>


<div class="panel panel-default">
	<div class="panel-heading">
		<h3 class="panel-title pull-left">Detail</h3>
		<a href="<?php echo Yii::app()->createUrl(Yii::app()->controller->module->id.'/'.Yii::app()->controller->id."/questionerList");?>" class="btn btn-primary btn-xs pull-right">
			<i class="fa fa-arrow-circle-left"></i>
			<span>Kembali</span>
		</a>
		<div style="clear:both"></div>
	</div>
	<div class="panel-body">		
		<div id="example-2_wrapper" class="dataTables_wrapper dt-bootstrap no-footer">
			<?php $this->widget('booster.widgets.TbDetailView',array(
			'data'=>$model,
			'attributes'=>array(
				array(
						"label"=>"No Reg",
						"value"=>$model->reg->reg_no,
					),
					array(
						"label"=>"MRN",
						"value"=>Lib::MRN($model->reg->p->p_id),
					),
					array(
						"label"=>"Nama Pasien",
						"value"=>$model->reg->p->p_name." ".$model->reg->p->p_title,
					),
					array(
						"label"=>"Jenis Kelamin",
						"value"=>($model->reg->p->p_gender=="Male")?"Laki-laki":"Perempuan",
					),
					array(
						"label"=>"Date IN",
						"value"=>Lib::dateInd($model->reg->reg_date_in),
					),
					array(
						"label"=>"Date OUT",
						"value"=>Lib::dateInd($model->reg->reg_is_out),
					),
					array(
						"label"=>"Guarator",
						"value"=>$model->reg->reg_guarator,
					),
					array(
						"label"=>"Type",
						"value"=>$model->reg->reg_type,
					),
					array(
						"label"=>"Departement",
						"value"=>$model->reg->reg_dept,
					),
					
					'sv_status',
					array(
						"label"=>"Create",
						"value"=>Lib::dateInd($model->create_at),
					),
					array(
						"label"=>"Modified",
						"value"=>Lib::dateInd($model->modified_at),
					),
			),
			)); ?>
			<hr>
			<h2>
			Questioner <?php echo $model->qc->qc_name;?>
			</h2>
			<?php $form=$this->beginWidget('booster.widgets.TbActiveForm',array(
				'action'=>"",
				'method'=>'POST',
			)); ?>
			
			<?php echo $form->dateTimePickerGroup($reg,'reg_is_out',array('widgetOptions'=>array('options'=>array('format'=>'yyyy-mm-dd hh:ii','viewFormat'=>'yyyy-mm-dd hh:ii')))) ;?>
			<Label>Kritik & Saran</label>
			<textarea class="form-control" name="note"><?php echo $model->sv_note;?></textarea>
			<table class="table table-striped">
			<?php
			$question=QuestionQuiz::model()->findAll(array("condition"=>"qc_id='$model->qc_id'","order"=>"qq_order ASC"));
			foreach($question as $row){
				echo'
				<tr>
					<th>'.$row->qq_question.'</th>';
					$option=QuestionOption::model()->findAll(array("condition"=>"qq_id='$row->qq_id'"));
					if(!empty($option)){
						foreach($option as $subRow){
							echo'<td>
							<label>';
							$cek=SurveyAnswer::model()->find(array("condition"=>"sv_id='$model->sv_id' AND qq_id='$row->qq_id' AND qo_id='$subRow->qo_id'"));
							if(!empty($cek)){
								echo'
							  <input type="radio" name="qq['.$row->qq_id.']" value="'.$subRow->qo_id.'" checked/> &nbsp;'.$subRow->qo_name;
							}else{
								echo'
							  <input type="radio" name="qq['.$row->qq_id.']" value="'.$subRow->qo_id.'"/> &nbsp;'.$subRow->qo_name;
							}
							
							 echo'
							</label>
						  </td>';
						}
					}
				echo'
				</tr>
				';
			}
			?>
			</table>
			<input type="submit" class="btn btn-success pull-right" value="Revisi Questioner" name="Finish"/>
			<input type="submit" class="btn btn-danger pull-right" value="Draft" name="Open"/>
			<?php $this->endWidget(); ?>
		</div>
	</div>
</div>
