<?php
$this->breadcrumbs=array(
	'Registrasi Pasien'=>array('index'),
	'Kelola Survey',
);
$this->title=array(
	'title'=>'Kelola Survey',
	'deskripsi'=>'Untuk Mengelola Survey'
);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	$('.import-form').hide();
	return false;
});
$('.btn-cancel').click(function(){
	$('.import-form').toggle();
	return false;
});
$('.import-button').click(function(){
	$('.import-form').toggle();
	$('.search-form').hide();
	return false;
});
$('.search-form form').submit(function(){
	$.fn.yiiGridView.update('survey-grid', {
		data: $(this).serialize()
	});
	return false;
});

");
?>

<div class="panel panel-default">
	<div class="panel-heading">
		<h3 class="panel-title pull-left">Data</h3>
		<a href="<?php echo Yii::app()->createUrl(Yii::app()->controller->module->id.'/'.Yii::app()->controller->id.'/rekapitulasi');?>" class="btn btn-primary btn-xs pull-right">
			<i class="fa fa-question-circle"></i>
			<span>Rekapitulasi</span>
		</a>
		<a href="<?php echo Yii::app()->createUrl(Yii::app()->controller->module->id.'/'.Yii::app()->controller->id);?>" class="btn btn-primary btn-xs pull-right">
			<i class="fa fa-arrow-circle-left"></i>
			<span>Kembali</span>
		</a>
		<div style="clear:both"></div>
	</div>
	<div class="panel-body">
		<style>
		.pagination li.selected a{
			background:rgb(235, 235, 236);
		}
		</style>
		
		<div class="alert alert-info" id="info"></div>
		<div class="row">
		<div class="search-form">
		<?php $form=$this->beginWidget('booster.widgets.TbActiveForm',array(
			'action'=>Yii::app()->createUrl($this->route),
			'method'=>'get',
		)); ?>

				<div class="col-md-3">
				<?php echo $form->datePickerGroup($model,'reg_date_in',array("label"=>"From",'widgetOptions'=>array('options'=>array('format'=>'yyyy-mm-dd','viewFormat'=>'yyyy-mm-dd'),'htmlOptions'=>array('class'=>'span5',"placeholder"=>"From")), 'prepend'=>'<i class="glyphicon glyphicon-calendar"></i>', )); ?>
				<?php echo $form->datePickerGroup($model,'create_at',array("label"=>"To",'widgetOptions'=>array('options'=>array('format'=>'yyyy-mm-dd','viewFormat'=>'yyyy-mm-dd'),'htmlOptions'=>array('class'=>'span5',"placeholder"=>"To")), 'prepend'=>'<i class="glyphicon glyphicon-calendar"></i>')); ?>
				</div>
				<div class="col-md-6">
				<?php echo $form->dropDownListGroup($model,'type_tgl', array("label"=>"Jenis Tanggal Rekap",'widgetOptions'=>array('data'=>array("reg_date_in"=>"Tanggal Registrasi Pasien","reg_is_out"=>"Tanggal keluar Pasien","sv_time"=>"Tanggal Pembuatan Questioner"), 'htmlOptions'=>array('class'=>'input-large','empty'=>'- Choose -')))); ?>
				<?php echo $form->dropDownListGroup($model,'reg_type', array("label"=>"Jenis Registrasi",'widgetOptions'=>array('data'=>array("INPATIENT"=>"INPATIENT","OUTPATIENT"=>"OUTPATIENT"), 'htmlOptions'=>array('class'=>'input-large','empty'=>'- Choose -')))); ?>
				</div>
				<div class="col-md-1" style="padding-top:25px">
				<?php $this->widget('booster.widgets.TbButton', array(
					'buttonType' => 'submit',
					'context'=>'primary',
					'label'=>'Search',
				)); ?>
				</div>

		<?php $this->endWidget(); ?>
		</div>
		</div>
		<hr>

		<?php
		if(Yii::app()->user->hasFlash('success')){
			echo'
			<div class="alert alert-success">
				<button type="button" class="close" data-dismiss="alert">&times;</button>
				'.Yii::app()->user->getFlash('success').'
			</div>';
		}
		?>
							
		<div id="example-2_wrapper" class="dataTables_wrapper form-inline dt-bootstrap no-footer">
		
		<?php 
		// put this somewhere on top
		$pageSize=Yii::app()->user->getState('pageSize',Yii::app()->params['defaultPageSize']); ?>		
		<?php		
		$this->widget('booster.widgets.TbExtendedGridView', array(
			'id'=>'survey-grid',
			'type' => 'striped',
			'dataProvider' => $model->search(),
			'filter'=>$model,
			'summaryText'=>'Menampilkan {start}-{end} dari {count} hasil.',
			'selectableRows' => 2,
			'responsiveTable' => true,
			'enablePagination' => true,
			'pager' => array(
				'htmlOptions'=>array(
					'class'=>'pagination'
				),
				'maxButtonCount' => 5,
				'cssFile' => true,
				'header' => false,
				'firstPageLabel' => '<<',
				'prevPageLabel' => '<',
				'nextPageLabel' => '>',
				'lastPageLabel' => '>>',
			),
			'bulkActions' => array(
			'actionButtons' => array(
				
				array(
					'buttonType' => 'button',
					'context' => 'primary',
					'size' => 'small',
					'label' => 'Export Excel',
					'click' => 'js:exportDataExcel',
					'id'=>'exportExcel'
					),
				array(
					'buttonType' => 'button',
					'context' => 'primary',
					'size' => 'small',
					'label' => 'Print',
					'click' => 'js:printMe',
					'id'=>'printDataMe'
					)
			),
				'checkBoxColumnConfig' => array(
					'name' => 'sv_id'
				),
			),
			'columns'=>array(
					array(
						"header"=>"No Reg",
						"value"=>function($data){
							if($data->reg_id!=''){
								return $data->reg->reg_no;
							}else{
								return'-';
							}
						},
						"name"=>"reg_no"
					),
					array(
						"header"=>"MRN",
						"value"=>function($data){
							if($data->reg_id!=''){
								return Lib::MRN($data->reg->p->p_id);
							}else{
								return'-';
							}
						},
						"name"=>"p_id"
					),
					array(
						"header"=>"Nama Pasien",
						"type"=>"raw",
						"value"=>function($data){
							if($data->reg_id!=''){
								return $data->reg->p->p_name." ".$data->reg->p->p_title;
							}else{
								return $data->sv_person_name.'<br/>'.$data->sv_person_number;
							}
						},
						"name"=>"p_name"
					),
					array(
						"header"=>"Jenis Kelamin",
						"value"=>function($data){
							if($data->reg_id!=''){
								return ($data->reg->p->p_gender=="Male")?"L":"P";
							}else{
								return '-';
							}
						},
						"name"=>"p_gender",
						"filter"=>array("Male"=>"L","Female"=>"P")
					),
					array(
						"header"=>"Date",
						"value"=>function($data){
							if($data->reg_id!=''){
								return Lib::dateInd($data->reg->reg_date_in);
							}else{
								return '-';
							}
						},
						"name"=>"reg_date_in"
					),
					array(
						"header"=>"Out",
						"value"=>function($data){
							if($data->reg_id!=''){
								return Lib::dateInd($data->reg->reg_is_out);
							}else{
								return '-';
							}
						},
						"name"=>"reg_is_out"
					),
					array(
						"header"=>"Guarator",
						"value"=>function($data){
							if($data->reg_id!=''){
								return $data->reg->reg_guarator;
							}else{
								return '-';
							}
						},
						"name"=>"reg_guarator"
					),
					array(
						"header"=>"Type",
						"value"=>function($data){
							if($data->reg_id!=''){
								return $data->reg->reg_type;
							}else{
								return '-';
							}
						},
						"name"=>"reg_type",
						"filter"=>array("INPATIENT"=>"INPATIENT","OUTPATIENT"=>"OUTPATIENT")
					),
					array(
						"header"=>"Departement",
						"value"=>function($data){
							if($data->reg_id!=''){
								return $data->reg->reg_dept;
							}else{
								return '-';
							}
						},
						"name"=>"reg_dept"
					),
					array(
						"header"=>"Questioner Time",
						"value"=>'Lib::dateInd($data->sv_time)',
						"name"=>"sv_time"
					),
					'sv_status',
					'sv_note',
					array(
						'class'=>'booster.widgets.TbButtonColumn',
						 'deleteConfirmation'=>'Anda yakin akan menhapus data?',
						'template'=>'{view}{delete}',
						'buttons'=>array
						(
							'view' => array
							(
								'label'=>'Isi Questioner',
								'icon'=>'search',
								'url'=>'Yii::app()->createUrl(Yii::app()->controller->module->id."/".Yii::app()->controller->id."/view",array("id"=>$data->sv_id))',
								'options'=>array(
									'class'=>'btn btn-default btn-xs',
								),
							),
							'delete' => array
							(
								'label'=>'Hapus',
								'icon'=>'trash',
								'options'=>array(
									'class'=>'btn btn-default btn-xs delete',
								),
							),
						),
						'header'=>CHtml::dropDownList('pageSize',$pageSize,array(10=>10,20=>20,50=>50,100=>100,200=>200,500=>500,1000=>1000),array(
							'onchange'=>"$.fn.yiiGridView.update('survey-grid',{ data:{pageSize: $(this).val() }})",
						)),
					),
			),
		));
		?>
				
		<form action="" method="POST" id="formDownload" target="_blank">
			<input type="hidden" name="ids" id="ids"/>
		</form>
		
		<script type="text/javascript">
			$("#info").hide();
			var gridId = "survey-grid";
			var defaultMessage='';
			var values;
			
			function exportDataExcel(values){	
				$("#formDownload").attr('action','<?php echo Yii::app()->createUrl(Yii::app()->controller->module->id."/".Yii::app()->controller->id.'/exportExcel');?>');
				$("#ids").val(values.toString());
				$("#formDownload").submit();
			}
			
			function printMe(values){	
				$("#formDownload").attr('action','<?php echo Yii::app()->createUrl(Yii::app()->controller->module->id."/".Yii::app()->controller->id.'/print');?>');
				$("#ids").val(values.toString());
				$("#formDownload").submit();
			}
		</script>
		</div>
	</div>
</div>