<?php
$this->breadcrumbs=array(
	'Kelola Questioner'=>array('index'),
	'Questioner Chart',
);

$this->title=array(
	'title'=>'Detail Questioner',
	'deskripsi'=>'Untuk Melihat Grafik Questioner'
);
?>

<?php
$baseUrl = Yii::app()->theme->baseUrl; 
$cs = Yii::app()->getClientScript();
$cs->registerScriptFile($baseUrl.'/assets/js/jquery.PrintArea.js');
$cs->registerScriptFile($baseUrl.'/assets/highcharts/highcharts.js');
$cs->registerScriptFile($baseUrl.'/assets/highcharts/modules/exporting.js');
?>

<?php
Yii::app()->clientScript->registerScript('validate', '
(function($) {
	// fungsi dijalankan setelah seluruh dokumen ditampilkan
	$(document).ready(function(e) {
		 
		// aksi ketika tombol cetak ditekan
		$("#cetak").bind("click", function(event) {
			$("#printarea").printArea();
		});
	});
}) (jQuery);

Highcharts.chart("container", {
    chart: {
        type: "column"
    },
    title: {
        text: "Grafik '.$title.'"
    },
    xAxis: {
        categories: ['.implode(",",$hLabel).'],
		labels: {
			style: {
				fontSize:"8px"
			}
		}
    },
    credits: {
        enabled: false
    },
    series: ['.implode(",",$hValues).']
});


jQuery("body").on("change","#Survey_qc_id",function(){jQuery.ajax({"type":"POST","url":"'.Yii::app()->createAbsoluteUrl(Yii::app()->controller->module->id.'/'.Yii::app()->controller->id.'/combo').'","cache":false,"data":jQuery(this).parents("form").serialize(),"success":function(html){jQuery("#update").html(html)}});return false;});
', CClientScript::POS_END);
?>

<div class="panel panel-default">
	<div class="panel-heading">
		<h3 class="panel-title pull-left">Detail</h3>
		<a href="<?php echo Yii::app()->createUrl(Yii::app()->controller->module->id.'/'.Yii::app()->controller->id);?>" class="btn btn-primary btn-xs pull-right">
			<i class="fa fa-arrow-circle-left"></i>
			<span>Kembali</span>
		</a>
		<div style="clear:both"></div>
	</div>
	<div class="panel-body">		
		<div id="example-2_wrapper" class="dataTables_wrapper dt-bootstrap no-footer">
		
			<div class="row">
	
			<?php $form=$this->beginWidget('booster.widgets.TbActiveForm',array(
				'action'=>Yii::app()->createUrl($this->route),
				'method'=>'POST',
			)); ?>

					<div class="col-md-4">
					<?php echo $form->datePickerGroup($model,'sv_time',array("label"=>"From",'widgetOptions'=>array('options'=>array('format'=>'yyyy-mm-dd','viewFormat'=>'yyyy-mm-dd'),'htmlOptions'=>array('class'=>'span5',"placeholder"=>"From")), 'prepend'=>'<i class="glyphicon glyphicon-calendar"></i>', )); ?>
					</div>
					<div class="col-md-4">
					<?php echo $form->datePickerGroup($model,'create_at',array("label"=>"To",'widgetOptions'=>array('options'=>array('format'=>'yyyy-mm-dd','viewFormat'=>'yyyy-mm-dd'),'htmlOptions'=>array('class'=>'span5',"placeholder"=>"To","value"=>date("Y-m-d"))), 'prepend'=>'<i class="glyphicon glyphicon-calendar"></i>')); ?>
					</div>
					<div class="col-md-4">
					<?php echo $form->dropDownListGroup($model,'type_tgl', array("label"=>"Jenis Tanggal Rekap",'widgetOptions'=>array('data'=>array("reg_date_in"=>"Tanggal Registrasi Pasien","reg_is_out"=>"Tanggal Keluar Pasien","sv_time"=>"Tanggal Pembuatan Questioner"), 'htmlOptions'=>array('class'=>'input-large')))); ?>
					</div>
					<div class="col-md-4">
		
					<?php echo $form->dropDownListGroup($model,'qc_id', array('widgetOptions'=>array('data'=>CHtml::listData(QuestionCategory::model()->findAll(),'qc_id','qc_name'), 'htmlOptions'=>array('class'=>'input-large',"empty"=>"Pilih Kategori")))); ?>
					
					</div>
					<div class="col-md-4">
					
					<?php 
					$question=array();
					if($model->qc_id!=""){
						$dataX=QuestionQuiz::model()->findAll(array("condition"=>"qc_id='$model->qc_id'","order"=>"qq_order,qc_id ASC"));		
						$question[]="-Semua-";
						foreach($dataX as $dataX)
						{
							$question[$dataX->qq_id]=$dataX->qq_question;
						}
					}
					echo $form->dropDownListGroup($model,'modified_at', array("label"=>"Questioner",'widgetOptions'=>array('data'=>$question, 'htmlOptions'=>array('class'=>'input-large',"id"=>"update",'multiple' => true))));

					?>
					</div>
					
					<div class="col-md-4">
					<?php
					echo $form->dropDownListGroup($model,'reg_id', array("label"=>"Print Type",'widgetOptions'=>array('data'=>array('Screen'=>"Screen","A4"=>"A4"), 'htmlOptions'=>array('class'=>'input-large'))));

					?>
					</div>
					
					<div class="col-md-12" style="padding-top:25px">
					<?php $this->widget('booster.widgets.TbButton', array(
						'buttonType' => 'submit',
						'context'=>'primary',
						'label'=>'Rekap Questioner',
					)); ?>
					</div>

			<?php $this->endWidget(); ?>
			</div>
			<hr>
			<div id="printarea">
			<div class="row">
				<div class="col-md-12">
					<div id="container" style="width: 1200px; height: 400px; margin: 0 auto"></div>
				</div>
				
				<hr>
				<table class="table">
				<?php
				echo'<tr><th>Notasi</th>';
				echo'<th>Penilaian</th>';
				foreach($detail as $x=>$y){
					echo"<td>$y</td>";
				}
				echo"<td>Tidak Ada Jawaban</td>";
				echo"<td>Total Koresponden</td>";
				echo'</tr>';
				foreach($table as $index=>$val){
					echo "<tr><th>$alias[$index]</th>";
					echo "<th>$index</th>";
					$tot=0;
					foreach($detail as $x=>$y){
						if($val[$y]==0){
							echo"<td>0</td>";
							$tot+=0;
						}else{
							echo"<td>$val[$y]</td>";
							$tot+=$val[$y];
						}
					}
					echo"<td>".($group-$tot)."</td>";
					echo"<td>".($group)."</td>";
					echo'</tr>';
				}
				
				?>
				</table>
			</div>
			</div>
		</div>
	</div>
</div>
