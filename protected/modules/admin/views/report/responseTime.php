<?php
$this->breadcrumbs=array(
	'Response Time Report',
);

$this->title=array(
	'title'=>'Response Time Report',
	'deskripsi'=>'Untuk melihat Response Time Report'
);
?>

<?php

Yii::app()->clientScript->registerScript('validate', '

', CClientScript::POS_END);
?>

<script src="<?php echo Yii::app()->theme->baseUrl."/assets/js/";?>tableHeadFixer.js"></script>
<style>	
	#fixTable {
		width: 1800px !important;
	}
</style>
<script>
	$(document).ready(function() {
		$("#fixTable").tableHeadFixer({"left" : 5}); 
	});
</script>

<div class="panel panel-default">
	<div class="panel-heading">
		<h3 class="panel-title pull-left">Response Time Report</h3>
		
		<div style="clear:both"></div>
	</div>
	<div class="panel-body">		
		<div id="example-2_wrapper" class="dataTables_wrapper dt-bootstrap no-footer">
			<div class="row">
	
			<?php $form=$this->beginWidget('booster.widgets.TbActiveForm',array(
				'action'=>Yii::app()->createUrl($this->route),
				'method'=>'POST',
			)); ?>

					<div class="col-md-4">
					<?php echo $form->datePickerGroup($model,'reg_start',array("label"=>"From",'widgetOptions'=>array('options'=>array('format'=>'yyyy-mm-dd','viewFormat'=>'yyyy-mm-dd'),'htmlOptions'=>array('class'=>'span5',"placeholder"=>"From")), 'prepend'=>'<i class="glyphicon glyphicon-calendar"></i>', )); ?>
					</div>
					<div class="col-md-4">
					<?php echo $form->datePickerGroup($model,'reg_end',array("label"=>"To",'widgetOptions'=>array('options'=>array('format'=>'yyyy-mm-dd','viewFormat'=>'yyyy-mm-dd'),'htmlOptions'=>array('class'=>'span5',"placeholder"=>"To")), 'prepend'=>'<i class="glyphicon glyphicon-calendar"></i>')); ?>
					</div>					
					
					<div class="col-md-12" style="padding-top:25px">
					<?php $this->widget('booster.widgets.TbButton', array(
						'buttonType' => 'submit',
						'context'=>'primary',
						'label'=>'Rekap Response Time',
					)); ?>
					<?php $this->widget('booster.widgets.TbButton', array(
						'buttonType' => 'submit',
						'context'=>'success',
						'label'=>'Export Excel',
						'htmlOptions'=>array(
							'name'=>"export"
						),
					)); ?>
					</div>

			<?php $this->endWidget(); ?>
			</div>
			
			<hr>
			
			<div style="width:100%;height:500px;overflow:auto">
				<table class="table table-bordered table-striped table-condensed table-hover" id="fixTable">
					<thead>
					<tr>
						<th rowspan="2">No</th>
						<th rowspan="2">Tgl Reg</th>
						<th rowspan="2">No MR</th>
						<th rowspan="2">Nama Pasien</th>
						<th rowspan="2">Poli/Dokter</th>
						<?php
						$svc=Service::model()->findAll(array("order"=>"svc_order asc"));
						$count=count($svc);
						$svcIndex=0;
						foreach($svc as $row){
							$svcIndex++;
							if($svcIndex!=$count){
								echo'<th colspan="4">'.$row->svc_name.'</th>';
								echo'<th rowspan="2">Waiting</th>';
							}else{
								echo'<th colspan="4">'.$row->svc_name.'</th>';
							}
						}
						?>
					</tr>
					<tr>
						<?php
						$svc=Service::model()->findAll(array("order"=>"svc_order asc"));

						foreach($svc as $row){
							echo'<th>Mulai</th>';
							echo'<th>Selesai</th>';
							echo'<th>Selisih</th>';
							echo'<th>Catatan</th>';
						}
						?>
					</tr>
					</thead>
					<?php
					
					$no=0;
					$nData=count($data);
					for($i=0;$i<$nData;$i++){
						$no++;
						echo'<tr>';
							echo'<td>'.$no.'</td>';
							echo'<td>'.Lib::dateInd($data[$i]->reg_date_in,false).'</td>';
							echo'<td>'.$data[$i]->p->p_name.' '.$data[$i]->p->p_title.'</td>';
							echo'<td>'.Lib::MRN($data[$i]->p_id).'</td>';
							echo'<td><b>'.$data[$i]->reg_dept.'</b><br/>'.$data[$i]->reg_doctor.'</td>';
							
							
							$svc=Service::model()->findAll(array("order"=>"svc_order asc"));
							$subcount=count($svc);
							$subsvcIndex=0;
							foreach($svc as $subRow){
								$subsvcIndex++;
								$get=ResponseTime::model()->find(array("condition"=>"reg_id='".$data[$i]->reg_id."' and svc_id='$subRow->svc_id'"));
								if($subsvcIndex!=$subcount){
									if(!empty($get)){
										$next=ResponseTime::model()->find(array("condition"=>"reg_id='".$data[$i]->reg_id."' and svc_id>'".$subRow->svc_id."' order by svc_id asc"));
										if(!empty($next)){
											$wait=Lib::timeDiff($get->end_time,$next->start_time);
										}else{
											$wait="";
										}
										echo'<td>'.$get->start_time.'</td>';
										echo'<td>'.$get->end_time.'</td>';
										echo'<td>'.Lib::timeDiff($get->start_time,$get->end_time).'</td>';
										echo'<td>'.$get->note.'</td>';
										echo'<td>'.$wait.'</td>';
									}else{
										echo'<td></td>';
										echo'<td></td>';
										echo'<td></td>';
										echo'<td></td>';
										echo'<td></td>';
									}
								}else{
									if(!empty($get)){
										echo'<td>'.$get->start_time.'</td>';
										echo'<td>'.$get->end_time.'</td>';
										echo'<td>'.Lib::timeDiff($get->start_time,$get->end_time).'</td>';
										echo'<td>'.$get->note.'</td>';
									}else{
										echo'<td></td>';
										echo'<td></td>';
										echo'<td></td>';
										echo'<td></td>';	
									}
								}
							}
							
						echo'</tr>';
					}
					?>
				</table>
			</div>
		</div>
	</div>
</div>
