<?php
$cs=Yii::app()->clientScript;
$cs->scriptMap=array(
    'jquery.js'=>false,
    'bootstrap.min.js' => false,
    'bootstrap-noconflict.js' => false,
    'bootbox.min.js' => false,
    'notify.min.js' => false,
    'bootstrap.min.css' => true,
    'bootstrap-yii.css' => false,
    'font-awasome.min.css' => false,
    'jquery-ui-bootstrap.css' => false,
);?>

<style>
	#fixTable{
		border:1px solid #999999;
		width:100%;
		font-size:11px;
		border-collapse:collapse;
	}
	#fixTable th{
		border:1px solid #999999;
		border-collapse:collapse;
		padding:2px;
		padding-left:5px;
	}
	
	#fixTable td{
		border:1px solid #999999;
		border-collapse:collapse;
		padding-left:5px;
		padding-right:5px;
	}
	#fixTable th{
		text-align:center;
	}
	.table{
		padding:5px;
		font-size:10px;
	}
	.table th{
		text-align:right;
	}
	
	.table td{
		padding:5px;
	}
	.table tr{
		border-bottom:1px solid #ccc;
	}
</style> 

<center>
	<h4>JOURNEY TIME PELAYANAN PASIEN DI INSTALASI RAWAT JALAN AULIA HOSPITAL</h4>
</center>

<table class="table table-bordered table-striped table-condensed table-hover" id="fixTable">
	<thead>
	<tr>
		<th rowspan="2">No</th>
		<th rowspan="2">Tgl Reg</th>
		<th rowspan="2">No MR</th>
		<th rowspan="2">Nama Pasien</th>
		<th rowspan="2">Poli/Dokter</th>
		<?php
		$svc=Service::model()->findAll(array("order"=>"svc_order asc"));
		$count=count($svc);
		$svcIndex=0;
		foreach($svc as $row){
			$svcIndex++;
			if($svcIndex!=$count){
				echo'<th colspan="4">'.$row->svc_name.'</th>';
				echo'<th rowspan="2">Waiting</th>';
			}else{
				echo'<th colspan="4">'.$row->svc_name.'</th>';
			}
		}
		?>
	</tr>
	<tr>
		<?php
		$svc=Service::model()->findAll(array("order"=>"svc_order asc"));

		foreach($svc as $row){
			echo'<th>Mulai</th>';
			echo'<th>Selesai</th>';
			echo'<th>Selisih</th>';
			echo'<th>Catatan</th>';
		}
		?>
	</tr>
	</thead>
	<?php
	
	$no=0;
	$nData=count($data);
	for($i=0;$i<$nData;$i++){
		$no++;
		echo'<tr>';
			echo'<td>'.$no.'</td>';
			echo'<td>'.Lib::dateInd($data[$i]->reg_date_in,false).'</td>';
			echo'<td>'.$data[$i]->p->p_name.' '.$data[$i]->p->p_title.'</td>';
			echo'<td>'.Lib::MRN($data[$i]->p_id).'</td>';
			echo'<td><b>'.$data[$i]->reg_dept.'</b><br/>'.$data[$i]->reg_doctor.'</td>';
			
			
			$svc=Service::model()->findAll(array("order"=>"svc_order asc"));
			$subcount=count($svc);
			$subsvcIndex=0;
			foreach($svc as $subRow){
				$subsvcIndex++;
				$get=ResponseTime::model()->find(array("condition"=>"reg_id='".$data[$i]->reg_id."' and svc_id='$subRow->svc_id'"));
				if($subsvcIndex!=$subcount){
					if(!empty($get)){
						$next=ResponseTime::model()->find(array("condition"=>"reg_id='".$data[$i]->reg_id."' and svc_id>'".$subRow->svc_id."' order by svc_id asc"));
						if(!empty($next)){
							$wait=Lib::timeDiff($get->end_time,$next->start_time);
						}else{
							$wait="";
						}
						echo'<td>'.$get->start_time.'</td>';
						echo'<td>'.$get->end_time.'</td>';
						echo'<td>'.Lib::timeDiff($get->start_time,$get->end_time).'</td>';
						echo'<td>'.$get->note.'</td>';
						echo'<td>'.$wait.'</td>';
					}else{
						echo'<td></td>';
						echo'<td></td>';
						echo'<td></td>';
						echo'<td></td>';
						echo'<td></td>';
					}
				}else{
					if(!empty($get)){
						echo'<td>'.$get->start_time.'</td>';
						echo'<td>'.$get->end_time.'</td>';
						echo'<td>'.Lib::timeDiff($get->start_time,$get->end_time).'</td>';
						echo'<td>'.$get->note.'</td>';
					}else{
						echo'<td></td>';
						echo'<td></td>';
						echo'<td></td>';
						echo'<td></td>';	
					}
				}
			}
			
		echo'</tr>';
	}
	?>
</table>