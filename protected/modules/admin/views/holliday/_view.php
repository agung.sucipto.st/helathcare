<div class="view">
		<b><?php echo CHtml::encode($data->getAttributeLabel('hd_id')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->hd_id),array('view','id'=>$data->hd_id)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('hd_name')); ?>:</b>
	<?php echo CHtml::encode($data->hd_name); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('hd_start')); ?>:</b>
	<?php echo CHtml::encode($data->hd_start); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('hd_end')); ?>:</b>
	<?php echo CHtml::encode($data->hd_end); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('hd_create')); ?>:</b>
	<?php echo CHtml::encode($data->hd_create); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('hd_update')); ?>:</b>
	<?php echo CHtml::encode($data->hd_update); ?>
	<br />

</div>