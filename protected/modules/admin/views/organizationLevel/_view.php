<div class="view">
		<b><?php echo CHtml::encode($data->getAttributeLabel('org_lv_id')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->org_lv_id),array('view','id'=>$data->org_lv_id)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('org_lv_name')); ?>:</b>
	<?php echo CHtml::encode($data->org_lv_name); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('org_lv_order')); ?>:</b>
	<?php echo CHtml::encode($data->org_lv_order); ?>
	<br />

</div>