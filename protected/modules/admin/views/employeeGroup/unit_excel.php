<?php
$cs=Yii::app()->clientScript;
$cs->scriptMap=array(
    'jquery.js'=>false,
    'bootstrap.min.js' => false,
    'bootstrap-noconflict.js' => false,
    'bootbox.min.js' => false,
    'notify.min.js' => false,
    'bootstrap.min.css' => true,
    'bootstrap-yii.css' => false,
    'jquery-ui-bootstrap.css' => false,
);?>
<style>

	span {
		color: red;
		font-weight:bold;
	}

	.table{
		padding:5px;
		font-size:10px;
		border:1px solid #ccc;
		border-collapse:collapse;
		width:100%;
	}
	.table td{
		padding-left:10px;		
		padding-right:10px;		
	}
	.table th{
		text-align:center;
	}
</style>
<center>
		<h4>Rekapitulasi Kehadiran 
		<?=$model->org->org_name?></h4>
		<?=Lib::dateInd($model->emp_group_start,false)?> s.d <?=Lib::dateInd($model->emp_group_end,false)?>
	</center>

<div id="parent">
<table class="table table-bordered table-striped table-hover table-responsive" id="fixTable custom" border="1px solid #ccc">
<thead>
<tr>
	<th>No</th>
	<th>ID / Nama</th>
	<?php
	$a=explode("-",$model->emp_group_start);
	$b=explode("-",$model->emp_group_end);
	$day=Lib::selisih2tanggal($model->emp_group_start,$model->emp_group_end);
	for($i=0;$i<=$day;$i++){
		$next=date("d",mktime(0,0,0,$a[1],$a[2]+$i,$a[0]));
		$nextD=date("Y-m-d",mktime(0,0,0,$a[1],$a[2]+$i,$a[0]));
		echo'<th>'.$next.'<br/>'.substr(Lib::getNamaHari($nextD),0,1).'</th>';
	}
	?>
	<th>Jml Telat</th><th>%</th>
	<th>Jml Lupa Absen Masuk</th><th>%</th>
	<th>Jml Lupa Absen Pulang</th><th>%</th>
</tr>
</thead>
<?php
$totalOn=0;
$totalLate=0;
$totalFast=0;
$totalAM=0;
$totalAP=0;
$totalNo=0;
$totalAtt=0;
$NOMOR=0;
foreach($data as $dx){
	$nAtt=0;
	$nLate=0;
	$nOn=0;
	$nFast=0;
	$nNo=0;
	$nAM=0;
	$nAP=0;
	$no=0;
	$status=array();
	$notAttendance=0;
	$arrayWO=array();
	$arrayRO=array();
	$NOMOR++;
	echo'<tr>';
	echo'<td>'.$NOMOR.'</td>';
	echo'<td>'.$dx->emp_id.'<br/>'.$dx->emp->emp_full_name.'</td>';
	for($i=0;$i<=$day;$i++){
		$nextD=date("Y-m-d",mktime(0,0,0,$a[1],$a[2]+$i,$a[0]));
		$row=Attendance::model()->find(array("condition"=>"att_date=:date AND att_emp_id=:id","params"=>array("date"=>$nextD,"id"=>$dx->emp_id),"order"=>"att_date ASC"));
		
		$nmDay=Lib::getDay($row->att_date);
		$holliday=Holliday::model()->find(array("condition"=>"hd_start='$row->att_date'"));
		if($nmDay=="Sun" OR !empty($holliday)){
			if($row->att_should_attend=="No"){
				if(!empty($holliday)){
					$style='style="background:orange;color:black;"';
					$holly="".$holliday->hd_name;
				}elseif($nmDay=="Sun"){
					$style='style="background:#b0dcb0;color:black;"';
					$holly="Off";
				}else{
					$style='style="background:pink;color:black;"';
					$holly="";
				}
			}else{
				if(!empty($holliday)){
					$style='style="background:orange;color:black;"';
					$holly="".$holliday->hd_name;
				}elseif($nmDay=="Sun"){
					$style='style="background:#b0dcb0;color:black;"';
					$holly="";
				}
			}
		}else{
			if($row->att_should_attend=="No"){
				$style='style="background:pink;color:black;"';
				$holly="Off";
			}else{
				$style="";
				$holly="";
			}
		}
		$no++;
		$statQuick='';
		$statLate='';
		$reason=array();
		$ot='';
		$quick='';
		$late=Lib::Late($row->shift->shift_att_in,$row->att_in);
		if($row->att_sts_id==""){
			if($row->att_should_attend=="Yes"){
				$nAtt++;
				if($row->att_out!='' AND $row->att_out!='00:00:00'){
					$quick=Lib::QuickHome($row->shift->shift_att_out,$row->att_out,$row->shift->shift_att_in);
					if($row->att_is_overtime==1){
						$ot=Lib::OverTime($row->shift->shift_att_out,$row->att_out);
					}else{
						$ot='';
					}
					if($quick=="00:00:00"){
						$statQuick='';
						$quick='';
					}else{
						if($row->att_is_possible_quick_home==1){
							$reason[]=$row->att_reason_possible_quick_home;
							$quick='';
						}else{
							$statQuick='style="background:red;color:white;"';
						}
						$nFast++;
					}
				}else{
					$ot='';
					$quick='';
				}
				
				if($late=="00:00:00" OR $late==""){
					$statLate='';
					$late='';
					$nOn++;
				}else{
					if($row->att_is_possible_late==1){
						$statLate='';
						$reason[]=$row->att_reason_possible_late;
						$late='';
					}else{
						$statLate='style="background:red;color:white;"';
						$nLate++;
					}
					
				}
				$work_hour=Lib::WO($row->att_in,$row->att_out,$row->shift->shift_att_in,$row->shift->shift_att_out);
				$real_hour=Lib::WO($row->att_in,$row->att_out,$row->shift->shift_att_in,$row->att_out);
				$arrayWO[]=$work_hour;
				$arrayRO[]=$real_hour;
				
				
				if(($row->att_out=='' OR $row->att_out=='00:00:00') AND ($row->att_in=='' OR $row->att_in=='00:00:00')){
					$nNo++;
				}elseif($row->att_in=='' OR $row->att_in=='00:00:00'){
					$nAM++;
				}elseif($row->att_out=='' OR $row->att_out=='00:00:00'){
					$nAP++;
				}
				echo'<td>'.(($late!="00:00:00" AND $late!="")?"<span>".$row->att_in."</span>":$row->att_in).' - '.$row->att_out;
				if($row->att_is_overtime==1){
					echo " - L";
				}
				echo'</td>';
			}else{
				$late='';
				$quick='';
				$ot='';
				$reason='';
				$work_hour='-';
				$real_hour='-';
				echo'<td>-</td>';
			}
			
			
		}else{
			/*
			$double=Attendance::model()->find(array("condition"=>"att_att_id='$row->att_id'"));
			if(empty($double)){
				$nNo++;
			}
			*/
			$late='';
			$quick='';
			$ot='';
			$reason='';
			$work_hour='-';
			$real_hour='-';
			$status[$row->attSts->att_sts_name]++;
			//echo'<td>'.$row->attSts->att_sts_name.'</td>';
			echo'<td>';
			echo substr($row->attSts->att_sts_name,0,1);
			echo '</td>';
		}
	}
	echo'<td>';
	if($nLate>0){
		echo $nLate;
	}
	echo'</td>';
	
	echo'<td>';
	if($nLate>0){
		echo number_format(($nLate/$nAtt)*100,2);
	}
	echo'</td>';
	echo'<td>';
	if($nAM>0){
		echo $nAM;
	}
	echo'</td>';
	
	echo'<td>';
	if($nAM>0){
		echo number_format(($nAM/$nAtt)*100,2);
	}
	echo'</td>';
	echo'<td>';
	if($nAP>0){
		echo $nAP;
	}
	echo'</td>';
	
	echo'<td>';
	if($nAP>0){
		echo number_format(($nAP/$nAtt)*100,2);
	}
	echo'</td>';
	echo'</tr>';
	$totalOn+=number_format(($nOn/$nAtt)*100,2);
	$totalLate+=number_format(($nLate/$nAtt)*100,2);
	$totalFast+=number_format(($nFast/$nAtt)*100,2);
	$totalAM+=number_format(($nAM/$nAtt)*100,2);
	$totalAP+=number_format(($nAP/$nAtt)*100,2);
	$totalNo+=number_format(($nNo/$nAtt)*100,2);
	$totalAtt+=number_format((($nAtt-$nNo)/$nAtt)*100,2);
}
?>


</table>
</div>
<hr>
	
<table class="table table-striped table-bordered">
	<tr>
		<th>On Time</th>
		<td><?php echo number_format($totalOn/count($data),2);?> %</td>
	</tr>
	<tr>
		<th>Late</th>
		<td><?php echo number_format($totalLate/count($data),2);?> %</td>
	</tr>
	<tr>
		<th>Quick Home</th>
		<td><?php echo number_format($totalFast/count($data),2);?> %</td>
	</tr>
	<tr>
		<th>Lupa Absen Masuk</th>
		<td><?php echo number_format($totalAM/count($data),2);?> %</td>
	</tr>
	<tr>
		<th>Lupa Absen Pulang</th>
		<td><?php echo number_format($totalAP/count($data),2);?> %</td>
	</tr>
	<tr>
		<th>Not Present</th>
		<td><?php echo number_format($totalNo/count($data),2);?> %</td>
	</tr>
	<tr>
		<th>Total Attendance</th>
		<td><?php echo number_format($totalAtt/count($data),2);?> %</td>
	</tr>
</table>