
<link rel="stylesheet" href="<?php echo Yii::app()->theme->baseUrl; ?>/assets/css/bootstrap-select.css">
<script src="<?php echo Yii::app()->theme->baseUrl; ?>/assets/js/bootstrap-select.js"></script>

<?php
Yii::app()->clientScript->registerScript('validatexx', '

$("#EmployeeGroup_emp_id_0").click(function () {    
     $("input:checkbox").prop("checked", this.checked);    
 });


$("#EmployeeGroup_shift_group_id").change(function(){
	var id = $("#EmployeeGroup_shift_group_id").val();
	changeValue(id);
});

function changeValue(value){	
	$(".append").hide();	
	$.ajax({
		url: "'.Yii::app()->createAbsoluteUrl(Yii::app()->controller->module->id.'/shiftGroup/viewGroup').'",
		cache: false,
		type: "POST",
		data:"id="+value,
		success: function(msg){
			$(".append").html(msg);
			$(".append").show();
		}
	});
}
', CClientScript::POS_END);
?>
<?php $form=$this->beginWidget('booster.widgets.TbActiveForm',array(
	'id'=>'employee-group-form',
	'type' => 'horizontal',

	'enableAjaxValidation'=>true,
)); ?>



<?php echo $form->errorSummary($model); ?>

	<?php
	$org=new Organization;
	$parent=Organization::model()->findAll();
		foreach($parent as $row){
			$data[$row->org_parent_id][] = $row;
		}
	?>
	<?php echo $form->dropDownListGroup($model,'org_id', array('widgetOptions'=>array('data'=>$org->getParent($data,0,1), 'htmlOptions'=>array('class'=>'input-large','empty'=>'- Organization -')))); ?>
	
	
	

	<?php 
	echo $form->datePickerGroup($model,'emp_group_start',array('widgetOptions'=>array('options'=>array('format'=>'yyyy-mm-dd','viewFormat'=>'yyyy-mm-dd'),'htmlOptions'=>array('class'=>'span5')), 'prepend'=>'<i class="glyphicon glyphicon-calendar"></i>', 'append'=>'Click Month/Year to change Month/Year.'));	?>

	<?php 
	echo $form->datePickerGroup($model,'emp_group_end',array('widgetOptions'=>array('options'=>array('format'=>'yyyy-mm-dd','viewFormat'=>'yyyy-mm-dd'),'htmlOptions'=>array('class'=>'span5')), 'prepend'=>'<i class="glyphicon glyphicon-calendar"></i>', 'append'=>'Click Month/Year to change Month/Year.'));?>
	
	<?php
	if($model->isNewRecord){
		$eg=EmployeeGroup::model()->find(array("condition"=>"org_id='$_GET[org_id]'","order"=>"emp_group_id DESC","limit"=>1));
		$el=EmployeeGroupList::model()->findAll(array("with"=>array("emp"),"condition"=>"emp_group_id='$eg->emp_group_id' and emp.emp_status='Active'"));
		$emp=Employee::model()->findAll(array("condition"=>"org_id='$eg->org_id' and emp_status='Active'"));
		$data=array();
		$data[]="Check All";
		foreach($el as $row){
			$data[$row->emp_id]=$row->emp->emp_full_name;
		}
		foreach($emp as $row){
			$data[$row->emp_id]=$row->emp_full_name;
		}
	}
	?>
	
	<?php echo $form->checkboxListGroup(
		$model,
		'emp_id',
		array(
			'widgetOptions' => array(
				'data' => $data
			),
		)
	); ?>
	
	<?php
	
	$dataXX=ShiftGroup::model()->findAll(array("condition"=>"shift_group_status='Active'"));
	$SG=array();
	foreach($dataXX as $row){
		$SG[$row->shift_group_id]=$row->shift_group_name.' | '.$row->org->org_name;
	}
	echo $form->dropDownListGroup($model,'shift_group_id', array('widgetOptions'=>array('data'=>$SG, 'htmlOptions'=>array('class'=>'input-large selectpicker show-tick','data-live-search'=>"true",'empty'=>'Choose Group'))));
	?>
	<div class="append" style="height:400px;overflow:auto;display:none;">
			<hr>
			</div>

<div class="form-actions">
	<?php $this->widget('booster.widgets.TbButton', array(
			'buttonType'=>'submit',
			'context'=>'primary',
			'label'=>$model->isNewRecord ? 'Tambah' : 'Simpan',
		)); 
		echo CHtml::button('Batal', array(
            'class' => 'btn btn-primary',
            'onclick' => "history.go(-1)",
                )
        );
		?>

		</div>

<?php $this->endWidget(); ?>