<?php
$this->breadcrumbs=array(
	'Kelola Employee Group',
);
$this->title=array(
	'title'=>'Kelola Employee Group',
	'deskripsi'=>'Untuk Mengelola Employee Group'
);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	$('.import-form').hide();
	return false;
});
$('.btn-cancel').click(function(){
	$('.import-form').toggle();
	return false;
});
$('.import-button').click(function(){
	$('.import-form').toggle();
	$('.search-form').hide();
	return false;
});
$('.search-form form').submit(function(){
	$.fn.yiiGridView.update('employee-group-grid', {
		data: $(this).serialize()
	});
	return false;
});

");
?>

<div class="panel panel-default">
	<div class="panel-heading">
		<h3 class="panel-title pull-left">Data</h3>
		<?php
		if(isset($_GET['org'])){
		?>
		<a href="<?php echo Yii::app()->createUrl(Yii::app()->controller->module->id.'/'.Yii::app()->controller->id.'/viewGrafik',array("org_id"=>$_GET['org']));?>" class="btn btn-primary btn-xs pull-right">
			<i class="fa fa-bar-chart-o"></i>
			<span>Grafik Keterlambatan</span>
		</a>
		
		<a href="<?php echo Yii::app()->createUrl(Yii::app()->controller->module->id.'/'.Yii::app()->controller->id.'/create',array("org_id"=>$_GET['org']));?>" class="btn btn-primary btn-xs pull-right">
			<i class="fa fa-copy"></i>
			<span>Tambah Data</span>
		</a>
		
		<a href="<?php echo Yii::app()->createUrl(Yii::app()->controller->module->id.'/'.Yii::app()->controller->id);?>" class="btn btn-primary btn-xs pull-right">
			<i class="fa fa-arrow-circle-left"></i>
			<span>Kembali</span>
		</a>
		<?php
		}
		?>
		<div style="clear:both"></div>
	</div>
	<div class="panel-body">
		
		<style>
		.pagination li.selected a{
			background:rgb(235, 235, 236);
		}
		</style>
		
		<div class="alert alert-info" id="info"></div>
		
		<?php
		if(Yii::app()->user->hasFlash('success')){
			echo'
			<div class="alert alert-success">
				<button type="button" class="close" data-dismiss="alert">&times;</button>
				'.Yii::app()->user->getFlash('success').'
			</div>';
		}
		?>
							
		<div id="example-2_wrapper" class="dataTables_wrapper form-inline dt-bootstrap no-footer">
		<?php
		if(isset($_GET['org'])){
		$data=Organization::model()->findbyPk($_GET['org']);
		$this->widget('booster.widgets.TbDetailView',array(
			'data'=>$data,
			'attributes'=>array(
				array(
					"label"=>"Organization",
					"value"=>$data->org_name
				),
			),
			)); 
		}?>
		<?php 
		// put this somewhere on top
		$pageSize=Yii::app()->user->getState('pageSize',Yii::app()->params['defaultPageSize']); ?>		
		<?php		
		if(isset($_GET['org'])){
			$this->widget('booster.widgets.TbExtendedGridView', array(
				'id'=>'employee-group-grid',
				'type' => 'striped',
				'dataProvider' => $model->search(),
				'filter'=>$model,
				'summaryText'=>'Menampilkan {start}-{end} dari {count} hasil.',
				'selectableRows' => 2,
				'responsiveTable' => true,
				'enablePagination' => true,
				'pager' => array(
					'htmlOptions'=>array(
						'class'=>'pagination'
					),
					'maxButtonCount' => 5,
					'cssFile' => true,
					'header' => false,
					'firstPageLabel' => '<<',
					'prevPageLabel' => '<',
					'nextPageLabel' => '>',
					'lastPageLabel' => '>>',
				),
				
				'columns'=>array(
						array(
							'header'=>$model->getAttributeLabel('emp_group_start'),
							'value'=>'Lib::dateInd($data->emp_group_start,false)',
							'name'=>'emp_group_start'
						),
						array(
							'header'=>$model->getAttributeLabel('emp_group_end'),
							'value'=>'Lib::dateInd($data->emp_group_end,false)',
							'name'=>'emp_group_end'
						),
						array(
							'class'=>'booster.widgets.TbButtonColumn',
							 'deleteConfirmation'=>'Anda yakin akan menhapus data?',
							'template'=>'{view}{update}{delete}',
							'buttons'=>array
							(
								'view' => array
								(
									'label'=>'View',
									'icon'=>'search',
									'options'=>array(
										'class'=>'btn btn-default btn-xs',
									),
								),
								'update' => array
								(
									'label'=>'Update',
									'icon'=>'pencil',
									'options'=>array(
										'class'=>'btn btn-default btn-xs',
									),
								),
								'delete' => array
								(
									'label'=>'Delete',
									'icon'=>'trash',
									'options'=>array(
										'class'=>'btn btn-default btn-xs delete',
									),
								)
							),
							'header'=>CHtml::dropDownList('pageSize',$pageSize,array(10=>10,20=>20,50=>50,100=>100,200=>200,500=>500,1000=>1000),array(
								'onchange'=>"$.fn.yiiGridView.update('departement-grid',{ data:{pageSize: $(this).val() }})",
							)),
						),
				),
			));
		}else{
			$this->widget('booster.widgets.TbExtendedGridView', array(
				'id'=>'employee-group-grid',
				'type' => 'striped',
				'dataProvider' => $model->searchByOrganization(),
				'filter'=>$model,
				'summaryText'=>'Menampilkan {start}-{end} dari {count} hasil.',
				'selectableRows' => 2,
				'responsiveTable' => true,
				'enablePagination' => true,
				'pager' => array(
					'htmlOptions'=>array(
						'class'=>'pagination'
					),
					'maxButtonCount' => 5,
					'cssFile' => true,
					'header' => false,
					'firstPageLabel' => '<<',
					'prevPageLabel' => '<',
					'nextPageLabel' => '>',
					'lastPageLabel' => '>>',
				),
				
				'columns'=>array(
						array(
							'header'=>$model->getAttributeLabel("org_id"),
							'name'=>"org_id",
							'value'=>'$data->org->org_name'
						),
						array(
							'class'=>'booster.widgets.TbButtonColumn',
							 'deleteConfirmation'=>'Anda yakin akan menhapus data?',
							'template'=>'{view}',
							'buttons'=>array
							(
								'view' => array
								(
									'label'=>'View',
									'icon'=>'search',
									'url'=>'Yii::app()->createUrl(Yii::app()->controller->module->id."/".Yii::app()->controller->id."/index",array("org"=>$data->org_id))',
									'options'=>array(
										'class'=>'btn btn-default btn-xs',
									),
								),
							),
						),
				),
			));
		}
		?>
				
		<form action="" method="POST" id="formDownload" target="_blank">
			<input type="hidden" name="ids" id="ids"/>
		</form>
		
		<script type="text/javascript">
			$("#info").hide();
			var gridId = "employee-group-grid";
			var defaultMessage='';
			var values;
			function confirmDelete(data){
				values=data;
				if(defaultMessage!=''){
					$(".modal-body").html(defaultMessage);
					$(".modal-footer").show();
				}
				$("#modalKonfirmasi").modal('show');
			}
			$(function(){
				$("#modalKonfirmasiOk").click(function(){
					if(defaultMessage==''){
						defaultMessage=$(".modal-body").html();
					}
					$(".modal-body").html('Sedang Menghapus data...');
					$(".modal-footer").hide();
					batchActions(values);
				});
			});
			function batchActions(values){				
					$.ajax({
						type: "POST",
						url: '<?php echo Yii::app()->createUrl(Yii::app()->controller->id.'/batchDelete');?>',
						data: {"ids":values},
						dataType:'json',
						success: function(resp){
							if(resp.status == "success"){
							   $.fn.yiiGridView.update(gridId);
							   if(resp.data.failureCount>0){
									$("#info").html(resp.data.successCount+' Berhasil Di Hapus & '+resp.data.failureCount+' Gagal Di Hapus !<br/>Data yang gagal di hapus : '+resp.data.dataError);
							   }else{
									$("#info").html(resp.data.successCount+' Berhasil Di Hapus & '+resp.data.failureCount+' Gagal Di Hapus');
							   }
							   $("#info").fadeIn(5000);
							   $("#info").fadeOut(3000);
							}else{
							   $("#info").html(resp.msg);
							   $("#info").fadeIn(5000);
							   $("#info").fadeOut(5000);
							}
							$("#modalKonfirmasi").modal('hide');
						}
					});
			}
			function exportDataExcel(values){	
				$("#formDownload").attr('action','<?php echo Yii::app()->createUrl(Yii::app()->controller->id.'/exportExcel');?>');
				$("#ids").val(values.toString());
				$("#formDownload").submit();
			}
			
			function exportDataPdf(values){	
				$("#formDownload").attr('action','<?php echo Yii::app()->createUrl(Yii::app()->controller->id.'/exportPdf');?>');
				$("#ids").val(values.toString());
				$("#formDownload").submit();
			}
		</script>
		</div>
	</div>
</div>