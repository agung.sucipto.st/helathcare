
<?php

Yii::app()->clientScript->registerScript('validatex', '
function openSearchGroup(){
	window.open("'.Yii::app()->createUrl(Yii::app()->controller->module->id.'/shiftGroup/list').'","","width=800,height=600");
}

function selectTextGroup(id){
	$("#EmployeeGroup_shift_group_id").val(id);
	changeValue(id);
}

$(document).on("change","#shift", function() {
	var id=$(this).attr("rel");
	var value=$(this).val();
	$.ajax({
		url: "'.Yii::app()->createAbsoluteUrl(Yii::app()->controller->module->id.'/attendance/updateOne').'",
		cache: false,
		type: "POST",
		data:"id="+id+"&value="+value,
		success: function(msg){
			$("#display"+id).html(msg);
		}
	});
}); 

$(document).on("change",".create", function() {
	var id=$(this).attr("data");
	var tgl=$(this).attr("date");
	var value=$(this).val();
	var cId=id.replace(/\./g,"");
	var cTgl=tgl.replace(/-/g,"");
	$.ajax({
		url: "'.Yii::app()->createAbsoluteUrl(Yii::app()->controller->module->id.'/attendance/createOne').'",
		cache: false,
		type: "POST",
		data:"id="+id+"&value="+value+"&tgl="+tgl,
		dataType:"json",
		success: function(msg){
			$("#display"+cId+""+cTgl).html(msg.data.value);
		}
	});
}); 

$("#EmployeeGroupList_shift_group_id").change(function(){
	var id = $("#EmployeeGroupList_shift_group_id").val();
	changeValue(id);
});

function changeValue(value){	
	$(".append").hide();	
	$.ajax({
		url: "'.Yii::app()->createAbsoluteUrl(Yii::app()->controller->module->id.'/shiftGroup/viewGroup').'",
		cache: false,
		type: "POST",
		data:"id="+value,
		success: function(msg){
			$(".append").html(msg);
			$(".append").show();
		}
	});
}

', CClientScript::POS_END);
?>
<script src="<?php echo Yii::app()->theme->baseUrl."/assets/js/";?>tableHeadFixer.js"></script>
<style>
	#parent {
		height: 400px;
	}
	
	#fixTable {
		width: 1800px !important;
	}
</style>
<script>
	$(document).ready(function() {
		$("#fixTable").tableHeadFixer({"left" : 2,"right":1}); 
	});
</script>
<?php
$this->breadcrumbs=array(
	'Kelola Employee Group'=>array('index'),
	'Detail Employee Group',
);

$this->title=array(
	'title'=>'Detail Employee Group',
	'deskripsi'=>'Untuk Melihat Detail Employee Group'
);
?>

<div class="panel panel-default">
	<div class="panel-heading">
		<h3 class="panel-title pull-left">Detail</h3>
		<a href="<?php echo Yii::app()->createUrl(Yii::app()->controller->module->id.'/'.Yii::app()->controller->id);?>" class="btn btn-primary btn-xs pull-right">
			<i class="fa fa-arrow-circle-left"></i>
			<span>Kembali</span>
		</a>
		<div style="clear:both"></div>
	</div>
	<div class="panel-body">	
			<?php $this->widget('booster.widgets.TbDetailView',array(
			'data'=>$model,
			'attributes'=>array(
				array(
					"label"=>$model->getAttributeLabel("org_id"),
					"value"=>$model->org->org_name
				),
				'emp_group_start',
				'emp_group_end',
			),
			)); ?>
			
			<?php $form=$this->beginWidget('booster.widgets.TbActiveForm',array(
				'id'=>'employee-group-list-form',
				'enableAjaxValidation'=>true,
			)); ?>

			<?php echo $form->errorSummary($EGL); ?>
			
			<?php	
			$parent=Organization::levelCheck($model->org_id,3);
			$notIn=Employee::empID($data);
			if(!empty($notIn)){
				$emp=Employee::model()->findAll(array("condition"=>"emp_status='Active' and org_id IN(".implode(",",$parent).") and emp_id NOT IN(".implode(",",$notIn).")","order"=>"RIGHT(emp_id,5) ASC"));
			}else{
				$emp=Employee::model()->findAll(array("condition"=>"emp_status='Active' and org_id IN(".implode(",",$parent).")","order"=>"RIGHT(emp_id,5) ASC"));
			}
			
			$employee=array();
			foreach($emp as $row){
					$employee[$row->emp_id]=$row->emp_id.' | '.$row->emp_full_name;
			}
			
			$dataXX=ShiftGroup::model()->findAll(array("condition"=>"shift_group_status='Active' and org_id IN(".implode(",",$parent).")"));
			$SG=array();
			foreach($dataXX as $row){
				$SG[$row->shift_group_id]=$row->shift_group_name.' | '.$row->org->org_name;
			}
			if(!empty($emp)){
			echo $form->dropDownListGroup($EGL,'emp_id', array('append'=>'<input type="submit" value="Tambah" class="btn btn-primary btn-xs" >','widgetOptions'=>array('data'=>$employee, 'htmlOptions'=>array('class'=>'input-large','empty'=>'Choose Employee'))));
			
			echo $form->dropDownListGroup($EGL,'shift_group_id', array('append'=>'<input type="button" value="Cari" onClick="openSearchGroup()" >','widgetOptions'=>array('data'=>$SG, 'htmlOptions'=>array('class'=>'input-large','empty'=>'Choose Group'))));
			}
			?>
			<div class="append" style="height:400px;overflow:auto;display:none;">
			<hr>
			</div>

			<?php $this->endWidget(); ?>
			<?php
			$dataX=Shift::model()->findAll(array("with"=>array("shiftGroup"),"condition"=>"shift_status='Active' and shiftGroup.org_id IN(".implode(",",$parent).")"));
			$shift=array();
			foreach($dataX as $rowX){
				$shift[$rowX->shiftGroup->shift_group_id][$rowX->shift_id]=$rowX->shift_att_in.' - '.$rowX->shift_att_out;
			}
			?>
			<div class="table-responsive" id="parent" style="overflow:auto;">
			<table class="table table-bordered table-striped table-hover table-responsive" id="fixTable">
			<thead>
			<tr>
				<th rowspan="2">No</th>
				<th rowspan="2">ID / Nama</th>
				<?php
				$month=Lib::getMonthDate($model->emp_group_start,$model->emp_group_end);
				foreach($month as $index=>$rows){
					echo'<th colspan="'.$rows.'">'.Lib::getBulan($index).'</th>';
				}
				?>
				<th rowspan="2">Jam Kerja</th>
			</tr>
			<tr>
				<?php
				$a=explode("-",$model->emp_group_start);
				$b=explode("-",$model->emp_group_end);
				$day=Lib::selisih2tanggal($model->emp_group_start,$model->emp_group_end);
				for($i=0;$i<=$day;$i++){
					$next=date("d",mktime(0,0,0,$a[1],$a[2]+$i,$a[0]));
					$nextD=date("Y-m-d",mktime(0,0,0,$a[1],$a[2]+$i,$a[0]));
					echo'<td>'.$next.'<br/>'.Lib::getNamaHari($nextD).'</td>';
				}
				?>
			</tr>
			</thead>
			<?php
			$no=0;
			foreach($data as $row){
				$arrayWO=array();
				$no++;
				echo'
				<tr>
					<td>'.$no.'</td>
					<td>'.$row->emp_id.'<br/>'.$row->emp->emp_full_name.'</td>';
					$a=explode("-",$model->emp_group_start);
					$b=explode("-",$model->emp_group_end);
					$day=Lib::selisih2tanggal($model->emp_group_start,$model->emp_group_end);
					for($i=0;$i<=$day;$i++){
											
						$next=date("Y-m-d",mktime(0,0,0,$a[1],$a[2]+$i,$a[0]));
						$stt=Attendance::model()->find(array("condition"=>"att_emp_id='$row->emp_id' AND att_date='$next'"));
						$work_hour=Lib::WO($stt->shift->shift_att_in,$stt->shift->shift_att_out,$stt->shift->shift_att_in,$stt->shift->shift_att_out);
						$arrayWO[]=$work_hour;
						if(!empty($stt)){
							echo'<td><select name="Attendance[shift_id]['.$stt->att_id.']" id="shift" rel="'.$stt->att_id.'" class="form-group">
						<option>-Choose Shift-</option>
						';
						}else{
							echo'<td><select name="Attendance[shift_id][]" id="" rel="" data="'.$row->emp_id.'" date="'.$next.'" class="form-group create">
						<option>-Choose Shift-</option>
						';
						}
						$group=array();
						foreach($shift as $idx=>$data){
							$SGX=ShiftGroup::model()->findbyPk($idx);
							echo'<optgroup label="'.$SGX->shift_group_name.'">';
							foreach($data as $x=>$y){
								if($x==$stt->shift_id){
									echo'<option value="'.$x.'" selected>'.$y.'</option>';
								}else{
									echo'<option value="'.$x.'">'.$y.'</option>';
								}
							}
							echo'</optgroup>';
						}
						echo'
						</select>';
						if(!empty($stt)){
							echo'<span id="display'.$stt->att_id.'">';
							echo $stt->shift->shift_att_in.' - '.$stt->shift->shift_att_out;
						}else{
							echo'<span id="display'.str_replace(".","",$row->emp_id).str_replace("-","",$next).'">';
							echo'<label class="text-danger">Not Set</label>';
						}
						echo'</span></td>';
						
					}
				echo'
					<td class="total'.$row->emp_id.'">'.Lib::TotalJam($arrayWO).'</td>
				</tr>
				';
			}
			?>
			</table>
			</div>
	</div>
</div>
