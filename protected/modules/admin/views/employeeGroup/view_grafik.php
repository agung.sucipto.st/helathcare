<?php
$this->breadcrumbs=array(
	'Manage Attendance'=>array('index'),
	'Unit Late Chart',
);

$this->title=array(
	'title'=>'Unit Late Chart',
	'deskripsi'=>'For View Chart Unit'
);
?>

<div class="panel panel-default">
	<div class="panel-heading">
		<h3 class="panel-title pull-left">Unit Late Chart</h3>
		<a href="<?php echo Yii::app()->createUrl(Yii::app()->controller->module->id.'/'.Yii::app()->controller->id.'/index/org/'.$_GET['org_id']);?>" class="btn btn-primary pull-right btn-xs">
			<i class="fa fa-arrow-left"></i>
			<span>Return</span>
		</a>
		<div style="clear:both"></div>
	</div>
	<div class="panel-body">	
	
	<?php
		if(isset($_GET['org_id'])){
		$data=Organization::model()->findbyPk($_GET['org_id']);
		$this->widget('booster.widgets.TbDetailView',array(
			'data'=>$data,
			'attributes'=>array(
				array(
					"label"=>"Organization",
					"value"=>$data->org_name
				),
			),
			)); 
		}?>
		<hr>
		<?php $form=$this->beginWidget('booster.widgets.TbActiveForm',array(
	'id'=>'attendance-form',
	'enableAjaxValidation'=>true,
	'htmlOptions'=>array('enctype'=>'multipart/form-data'),
)); ?>


<?php echo $form->errorSummary($model); ?>
	
	<?php 
	$periode=array();
	$dataEmp=EmployeeGroup::model()->findAll(array("condition"=>"org_id=:org","order"=>"emp_group_start asc","params"=>array("org"=>$_GET['org_id'])));
	foreach($dataEmp as $row){
		$periode[$row->emp_group_id]=$row->emp_group_start;
	}
	if($model->isNewRecord)echo $form->dropDownListGroup($model,'emp_group_start', array('widgetOptions'=>array('data'=>$periode, 'htmlOptions'=>array('class'=>'input-large','empty'=>'Choose Periode','multiple' => true))));
	?>
	

	
<div class="form-actions">
	<?php $this->widget('booster.widgets.TbButton', array(
			'buttonType'=>'submit',
			'context'=>'primary',
			'label'=>$model->isNewRecord ? 'View' : 'Save',
		)); 
		
		?>

		</div>

<?php $this->endWidget(); ?>


	<?php 
	if(!empty($list)){
		echo $this->renderPartial('_view_grafik', array('list'=>$list,'org'=>$data->org_name));
	}
	?>

	</div>
</div>
