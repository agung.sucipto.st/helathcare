<link rel="stylesheet" href="<?php echo Yii::app()->theme->baseUrl; ?>/assets/css/bootstrap.min.css">
<link rel="stylesheet" href="<?php echo Yii::app()->theme->baseUrl; ?>/assets/css/bootstrap-select.css">
<script src="<?php echo Yii::app()->theme->baseUrl; ?>/assets/js/bootstrap.min.js"></script>
<script src="<?php echo Yii::app()->theme->baseUrl; ?>/assets/js/bootstrap-select.js"></script>

<?php
Yii::app()->clientScript->registerScript('validatex', '

function pop(id){
	window.open("'.Yii::app()->createUrl(Yii::app()->controller->module->id.'/attendance/setAttendance').'/from/'.$model->emp_group_start.'/to/'.$model->emp_group_end.'/id/"+id,"","width=800,height=800,scrollbars=yes");
}


function openSearchGroup(){
	window.open("'.Yii::app()->createUrl(Yii::app()->controller->module->id.'/shiftGroup/list').'","","width=800,height=600,scrollbars=yes");
}

function selectTextGroup(id){
	$("#EmployeeGroup_shift_group_id").val(id);
	changeValue(id);
}


$("#EmployeeGroupList_shift_group_id").change(function(){
	var id = $("#EmployeeGroupList_shift_group_id").val();
	changeValue(id);
});

function changeValue(value){	
	$(".append").hide();	
	$.ajax({
		url: "'.Yii::app()->createAbsoluteUrl(Yii::app()->controller->module->id.'/shiftGroup/viewGroup').'",
		cache: false,
		type: "POST",
		data:"id="+value,
		success: function(msg){
			$(".append").html(msg);
			$(".append").show();
		}
	});
}

', CClientScript::POS_END);
?>

<?php
$this->breadcrumbs=array(
	'Kelola Employee Group'=>array('index'),
	'Kelola Employee Group Organization'=>array('index','org'=>$model->org_id),
	'Detail Employee Group',
);

$this->title=array(
	'title'=>'Detail Employee Group',
	'deskripsi'=>'Untuk Melihat Detail Employee Group'
);
?>

<div class="panel panel-default">
	<div class="panel-heading">
		<h3 class="panel-title pull-left">Detail</h3>
		<a href="<?php echo Yii::app()->createUrl(Yii::app()->controller->module->id.'/'.Yii::app()->controller->id.'/unit',array("id"=>$model->emp_group_id,"excel"=>"true"));?>" class="btn btn-primary btn-xs pull-right" target="_blank">
			<i class="fa fa-download"></i>
			<span>Excel Summary Kehadiran</span>
		</a>
		
		<a href="<?php echo Yii::app()->createUrl(Yii::app()->controller->module->id.'/'.Yii::app()->controller->id.'/unit',array("id"=>$model->emp_group_id));?>" class="btn btn-primary btn-xs pull-right" target="_blank">
			<i class="fa fa-print"></i>
			<span>Print Summary Kehadiran</span>
		</a>
		<a href="<?php echo Yii::app()->createUrl(Yii::app()->controller->module->id.'/'.Yii::app()->controller->id."/index",array('org'=>$model->org_id));?>" class="btn btn-primary btn-xs pull-right">
			<i class="fa fa-arrow-circle-left"></i>
			<span>Kembali</span>
		</a>
		<div style="clear:both"></div>
	</div>
	<div class="panel-body">	
			<?php $this->widget('booster.widgets.TbDetailView',array(
			'data'=>$model,
			'attributes'=>array(
				array(
					"label"=>$model->getAttributeLabel("org_id"),
					"value"=>$model->org->org_name
				),
				'emp_group_start',
				'emp_group_end',
			),
			)); ?>
			
			<?php $form=$this->beginWidget('booster.widgets.TbActiveForm',array(
				'id'=>'employee-group-list-form',
				'enableAjaxValidation'=>true,
			)); ?>

			<?php echo $form->errorSummary($EGL); ?>
			
			<?php
			$parent=Organization::levelCheck($model->org_id,3);
			$notIn=Employee::empID($dataArrayEmployee);
			
			if(!empty($notIn)){
			//	$emp=Employee::model()->findAll(array("condition"=>"emp_status='Active' and org_id IN(".implode(",",$parent).") and emp_id NOT IN(".implode(",",$notIn).")","order"=>"RIGHT(emp_id,5) ASC"));
				//$emp=Employee::model()->findAll(array("condition"=>"emp_status='Active' and org_id IN(".implode(",",$parent).") and emp_id NOT IN(".implode(",",$notIn).")","order"=>"emp_full_name ASC"));
				$emp=Employee::model()->findAll(array("condition"=>"emp_status='Active' AND emp_id NOT IN(".implode(",",$notIn).")","order"=>"emp_full_name ASC"));
			}else{
				//$emp=Employee::model()->findAll(array("condition"=>"emp_status='Active' and org_id IN(".implode(",",$parent).")","order"=>"RIGHT(emp_id,5) ASC"));
				//$emp=Employee::model()->findAll(array("condition"=>"emp_status='Active' and org_id IN(".implode(",",$parent).")","order"=>"emp_full_name ASC"));
				$emp=Employee::model()->findAll(array("condition"=>"emp_status='Active'","order"=>"emp_full_name ASC"));
			}
			
			$employee=array();
			foreach($emp as $row){
					$employee[$row->emp_id]=$row->emp_full_name.' | '.$row->emp_id;
			}
			
			//$dataXX=ShiftGroup::model()->findAll(array("condition"=>"shift_group_status='Active' and org_id IN(".implode(",",$parent).")"));
			$dataXX=ShiftGroup::model()->findAll(array("condition"=>"shift_group_status='Active'"));
			$SG=array();
			foreach($dataXX as $row){
				$SG[$row->shift_group_id]=$row->shift_group_name.' | '.$row->org->org_name;
			}
			if(!empty($emp)){
			echo $form->dropDownListGroup($EGL,'emp_id', array('append'=>'<input type="submit" value="Tambah" class="btn btn-primary btn-xs" >','widgetOptions'=>array('data'=>$employee, 'htmlOptions'=>array('class'=>'input-large selectpicker show-tick','data-live-search'=>"true",'empty'=>'Choose Employee'))));
			
			echo $form->dropDownListGroup($EGL,'shift_group_id', array('append'=>'<input type="button" value="Cari" onClick="openSearchGroup()" >','widgetOptions'=>array('data'=>$SG, 'htmlOptions'=>array('class'=>'input-large','empty'=>'Choose Group'))));
			}
			?>
			<div class="append" style="height:400px;overflow:auto;display:none;">
			<hr>
			</div>

			<?php $this->endWidget(); ?>
			<?php
			$dataX=Shift::model()->findAll(array("with"=>array("shiftGroup"),"condition"=>"shift_status='Active' and shiftGroup.org_id IN(".implode(",",$parent).")"));
			$shift=array();
			foreach($dataX as $rowX){
				$shift[$rowX->shiftGroup->shift_group_id][$rowX->shift_id]=$rowX->shift_att_in.' - '.$rowX->shift_att_out;
			}
			?>
			
			
			
		<?php 
		$pageSize=Yii::app()->user->getState('pageSize',Yii::app()->params['defaultPageSize']); 
		?>		
		<?php		
		$this->widget('booster.widgets.TbExtendedGridView', array(
			'id'=>'employee-group-list-grid',
			'type' => 'striped',
			'dataProvider' => $data->search(),
			'filter'=>$data,
			'summaryText'=>'Menampilkan {start}-{end} dari {count} hasil.',
			'selectableRows' => 2,
			'responsiveTable' => true,
			'enablePagination' => true,
			'pager' => array(
				'htmlOptions'=>array(
					'class'=>'pagination'
				),
				'maxButtonCount' => 5,
				'cssFile' => true,
				'header' => false,
				'firstPageLabel' => '<<',
				'prevPageLabel' => '<',
				'nextPageLabel' => '>',
				'lastPageLabel' => '>>',
			),
			'bulkActions' => array(
			'actionButtons' => array(
				array(
					'buttonType' => 'button',
					'context' => 'primary',
					'size' => 'small',
					'label' => 'Cetak Absensi',
					'click' => 'js:printAbsensi',
					'id'=>'egl_id'
					
					),
			),
				'checkBoxColumnConfig' => array(
					'name' => 'egl_id'
				),
			),
			'columns'=>array(
					array(
						"header"=>$data->getAttributeLabel('emp_id'),
						"type"=>"raw",
						"name"=>"emp_id",
						"value"=>function($data){
							return '<span onclick="pop(\''.$data->emp_id.'\');">'.$data->emp_id.'</span>';
						}
					),
					array(
						"header"=>$data->getAttributeLabel('emp_id'),
						"type"=>"raw",
						"name"=>"emp_name",
						"value"=>function($data){
							return '<span onclick="pop(\''.$data->emp_id.'\');">'.$data->emp->emp_full_name.'</span>';
						}
					),
					array(
						'class'=>'booster.widgets.TbButtonColumn',
						 'deleteConfirmation'=>'Anda yakin akan menhapus data?',
						'template'=>'{print}{delete}',
						'buttons'=>array
						(
							'print' => array
							(
								'label'=>'Cetak Absensi',
								'icon'=>'print',
								'url'=>'Yii::app()->createUrl(Yii::app()->controller->module->id."/attendance/viewSummaryPersonal",array("from"=>$data->empGroup->emp_group_start,"to"=>$data->empGroup->emp_group_end,"id"=>$data->emp_id,"print"=>"yes"))',
								'options'=>array(
									'class'=>'btn btn-default btn-xs',
									'target'=>"_blank"
								),
							),
							'setup' => array
							(
								'label'=>'Atur jadwal',
								'icon'=>'calendar',
								'url'=>'#',
								'options'=>array(
									'class'=>'btn btn-default btn-xs',
									'onclick'=>function($data){
										return 'pop($data->emp_id);';
									}
								),
							),
							'delete' => array
							(
								'label'=>'Delete',
								'icon'=>'trash',
								'url'=>'Yii::app()->createUrl(Yii::app()->controller->module->id."/".Yii::app()->controller->id."/view",array("id"=>$_GET["id"],"delete"=>$data->egl_id))',
								'options'=>array(
									'class'=>'btn btn-default btn-xs delete',
								),
							)
						),
						'header'=>CHtml::dropDownList('pageSize',$pageSize,array(10=>10,20=>20,50=>50,100=>100,200=>200,500=>500,1000=>1000),array(
							'onchange'=>"$.fn.yiiGridView.update('employee-group-list-grid',{ data:{pageSize: $(this).val() }})",
						)),
					),
			),
		));
		?>
		
		<form action="" method="POST" id="formDownload" target="_blank">
			<input type="hidden" name="ids" id="ids"/>
		</form>
		
		<script type="text/javascript">
			var values;
			
			
			function printAbsensi(values){	
				$("#formDownload").attr('action','<?php echo Yii::app()->createUrl(Yii::app()->controller->module->id."/attendance/downloadSelected",array("from"=>$model->emp_group_start,"to"=>$model->emp_group_end));?>');
				$("#ids").val(values.toString());
				$("#formDownload").submit();
			}
		</script>
	</div>
</div>
