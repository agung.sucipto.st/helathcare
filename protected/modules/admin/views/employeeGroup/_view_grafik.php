<div class="container">





<?php
$arrayLate=array();

foreach($list as $x){
	
	$model=EmployeeGroup::model()->findbyPk($x);
	$a=explode("-",$model->emp_group_start);
	$b=explode("-",$model->emp_group_end);
	$day=Lib::selisih2tanggal($model->emp_group_start,$model->emp_group_end);
	$data=EmployeeGroupList::model()->findAll(array("condition"=>"emp_group_id='$x'"));

	foreach($data as $dx){
		
		$nAtt=0;
		$nLate=0;
		
		for($i=0;$i<=$day;$i++){
			$nextD=date("Y-m-d",mktime(0,0,0,$a[1],$a[2]+$i,$a[0]));
			$row=Attendance::model()->find(array("condition"=>"att_date=:date AND att_emp_id=:id","params"=>array("date"=>$nextD,"id"=>$dx->emp_id),"order"=>"att_date ASC"));
		
			$late=Lib::Late($row->shift->shift_att_in,$row->att_in,(($row->att_date>='2017-09-12')?0:5));
			if($row->att_sts_id==""){
				if($row->att_should_attend=="Yes"){
					$nAtt++;
					
					
					if($late=="00:00:00" OR $late==""){
						$statLate='';
						$late='';
						$arrayLate[substr($row->att_date,0,7)]+=0;
						$nOn++;
					}else{
						if($row->att_is_possible_late==1){
							$statLate='';
							$reason[]=$row->att_reason_possible_late;
							$late='';
							$arrayLate[substr($row->att_date,0,7)]+=0;
						}else{
							$statLate='style="background:red;color:white;"';
							$nLate++;
							$arrayLate[substr($row->att_date,0,7)]++;
						}
						
					}
				}else{
					$arrayLate[substr($row->att_date,0,7)]+=0;
					$late='';
					$quick='';
					$ot='';
					$reason='';
					$work_hour='-';
					$real_hour='-';
				}
				
				
			}else{
				$late='';
				$quick='';
				$ot='';
				$reason='';
				$work_hour='-';
				$real_hour='-';
				$status[$row->attSts->att_sts_name]++;
			}
		}
	}
}

?>



<?php
$baseUrl = Yii::app()->theme->baseUrl; 
$cs = Yii::app()->getClientScript();
$cs->registerScriptFile($baseUrl.'/assets/js/jquery.PrintArea.js');
$cs->registerScriptFile($baseUrl.'/assets/highcharts/highcharts.js');
$cs->registerScriptFile($baseUrl.'/assets/highcharts/modules/exporting.js');
?>


<?php
$emp=Employee::model()->findbyPk($emp);
$cat=array();
$val=array();
foreach($arrayLate as $i=>$y){
	$cat[]='"'.$i.'"';
	$val[]=$y;
}
Yii::app()->clientScript->registerScript('validate', '
Highcharts.chart("container2", {
    chart: {
        type: "line"
    },
    title: {
        text: "Chart Late '.$org.'"
    },
    subtitle: {
        text: "Source: WorldClimate.com"
    },
    xAxis: {
        categories: ['.implode(",",$cat).'],
    },
    yAxis: {
        title: {
            text: "Sum Of Late"
        }
    },
    plotOptions: {
        line: {
            dataLabels: {
                enabled: true
            },
            enableMouseTracking: false
        }
    },
    series: [{name: "Late",data: ['.implode(",",$val).']},]
});

', CClientScript::POS_END);
?>

<div id="container2" style="height: 400px; margin: 0 auto;width:900px;"></div>
</div>