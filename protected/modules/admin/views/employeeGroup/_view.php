<div class="view">
		<b><?php echo CHtml::encode($data->getAttributeLabel('emp_group_id')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->emp_group_id),array('view','id'=>$data->emp_group_id)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('shift_group_id')); ?>:</b>
	<?php echo CHtml::encode($data->shift_group_id); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('emp_id')); ?>:</b>
	<?php echo CHtml::encode($data->emp_id); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('emp_group_start')); ?>:</b>
	<?php echo CHtml::encode($data->emp_group_start); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('emp_group_end')); ?>:</b>
	<?php echo CHtml::encode($data->emp_group_end); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('emp_group_create')); ?>:</b>
	<?php echo CHtml::encode($data->emp_group_create); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('emp_group_update')); ?>:</b>
	<?php echo CHtml::encode($data->emp_group_update); ?>
	<br />

</div>