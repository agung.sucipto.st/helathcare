<?php
$this->breadcrumbs=array(
	'Kelola Employee Group'=>array('index'),
	'Kelola Employee Group Organization'=>array('index','org'=>$_GET['org_id']),
	'Tambah Employee Group',
);
$this->title=array(
	'title'=>'Tambah Employee Group',
	'deskripsi'=>'Untuk Menambah Employee Group'
);?>

<div class="panel panel-default">
	<div class="panel-heading">
		<h3 class="panel-title pull-left">Form Tambah</h3>
		<a href="<?php echo Yii::app()->createUrl(Yii::app()->controller->module->id.'/'.Yii::app()->controller->id."/index",array('org'=>$_GET['org_id']));?>" class="btn btn-primary btn-xs pull-right">
			<i class="fa fa-arrow-circle-left"></i>
			<span>Kembali</span>
		</a>
		<div style="clear:both"></div>
	</div>
	<div class="panel-body">		
		<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>	</div>
</div>