<?php
$this->breadcrumbs=array(
	'Kelola Menu Harian'=>array('index'),
	'Detail Menu Harian',
);

$this->title=array(
	'title'=>'Detail Menu Harian',
	'deskripsi'=>'Untuk Melihat Detail Menu Harian'
);

Yii::app()->clientScript->registerScript('add', "
$('.add-button').click(function(){
	$('.add-form').toggle();
	return false;
});
$('.add-form form').submit(function(){
	$.fn.yiiGridView.update('gizi-menu-harian-list-grid', {
		
	});
	return false;
});

");
?>

<div class="panel panel-default">
	<div class="panel-heading">
		<h3 class="panel-title pull-left">Detail</h3>
		<a href="<?php echo Yii::app()->createUrl(Yii::app()->controller->module->id.'/'.Yii::app()->controller->id);?>" class="btn btn-primary btn-xs pull-right">
			<i class="fa fa-arrow-circle-left"></i>
			<span>Kembali</span>
		</a>
		<a href="<?php echo Yii::app()->createUrl(Yii::app()->controller->module->id.'/'.Yii::app()->controller->id.'/create');?>" class="btn btn-primary btn-xs pull-right">
			<i class="fa fa-copy"></i>
			<span>Tambah Data</span>
		</a>
		<a href="<?php echo Yii::app()->createUrl(Yii::app()->controller->module->id.'/'.Yii::app()->controller->id.'/update/id/'.$_GET['id']);?>" class="btn btn-primary btn-xs pull-right">
			<i class="fa fa-edit"></i>
			<span>Edit Data</span>
		</a>
		<div style="clear:both"></div>
	</div>
	<div class="panel-body">		
			
			<?php $this->widget('booster.widgets.TbDetailView',array(
			'data'=>$model,
			'attributes'=>array(
				array(
					"label"=>$model->getAttributeLabel("id_user"),
					"value"=>$model->idUser->username
				),
				array(
					"label"=>$model->getAttributeLabel("gmh_date"),
					"value"=>Lib::dateInd($model->gmh_date)
				),
				array(
					"label"=>$model->getAttributeLabel("create_at"),
					"value"=>Lib::dateInd($model->create_at)
				),
				array(
					"label"=>$model->getAttributeLabel("modified_at"),
					"value"=>Lib::dateInd($model->modified_at)
				),
			),
			)); ?>
			<a href="" class="btn btn-primary btn-xs pull-right add-button">
				<i class="fa fa-plus"></i>
				<span>Tambah Menu Harian</span>
			</a>
			<div class="add-form" style="display:none">
				<?php $this->renderPartial('_add_form',array(
				'add'=>$add,
			)); ?>
			</div><!-- search-form -->
			<?php 
			$pageSize=Yii::app()->user->getState('pageSize',Yii::app()->params['defaultPageSize']); ?>		
			<?php		
			$this->widget('booster.widgets.TbExtendedGridView', array(
				'id'=>'gizi-menu-harian-list-grid',
				'type' => 'striped',
				'dataProvider' => $data->search(),
				'filter'=>$data,
				'summaryText'=>'Menampilkan {start}-{end} dari {count} hasil.',
				'selectableRows' => 2,
				'responsiveTable' => true,
				'enablePagination' => true,
				'pager' => array(
					'htmlOptions'=>array(
						'class'=>'pagination'
					),
					'maxButtonCount' => 5,
					'cssFile' => true,
					'header' => false,
					'firstPageLabel' => '<<',
					'prevPageLabel' => '<',
					'nextPageLabel' => '>',
					'lastPageLabel' => '>>',
				),
				
				'columns'=>array(
						array(
							"header"=>$data->getAttributeLabel("gjd_id"),
							"value"=>'$data->gjd->gjd_name',
							"name"=>"gjd",
							"filter"=>CHtml::listData(GiziJenisDiet::model()->findAll(),'gjd_id','gjd_name')
						),
						array(
							"header"=>$data->getAttributeLabel("gm_id"),
							"value"=>'$data->gm->gm_name',
							"name"=>"gm",
							"filter"=>CHtml::listData(GiziMenu::model()->findAll(),'gm_id','gm_name')
						),
						array(
							'class'=>'booster.widgets.TbButtonColumn',
							 'deleteConfirmation'=>'Anda yakin akan menhapus data?',
							'template'=>'{delete}',
							'buttons'=>array
							(
								
								'delete' => array
								(
									'label'=>'Delete',
									'icon'=>'trash',
									'url'=>'Yii::app()->createUrl(Yii::app()->controller->module->id."/giziMenuHarianList/delete/",array("id"=>$data->gmhl_id))',
									'options'=>array(
										'class'=>'btn btn-default btn-xs delete',
									),
								)
							),
							'header'=>CHtml::dropDownList('pageSize',$pageSize,array(10=>10,20=>20,50=>50,100=>100,200=>200,500=>500,1000=>1000),array(
								'onchange'=>"$.fn.yiiGridView.update('gizi-menu-harian-list-grid',{ data:{pageSize: $(this).val() }})",
							)),
						),
				),
			));
			?>
	</div>
</div>
