<div class="view">
		<b><?php echo CHtml::encode($data->getAttributeLabel('gmh_id')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->gmh_id),array('view','id'=>$data->gmh_id)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('id_user')); ?>:</b>
	<?php echo CHtml::encode($data->id_user); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('gmh_date')); ?>:</b>
	<?php echo CHtml::encode($data->gmh_date); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('create_at')); ?>:</b>
	<?php echo CHtml::encode($data->create_at); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('modified_at')); ?>:</b>
	<?php echo CHtml::encode($data->modified_at); ?>
	<br />

</div>