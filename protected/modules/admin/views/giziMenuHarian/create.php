<?php
$this->breadcrumbs=array(
	'Kelola Menu Harian'=>array('index'),
	'Tambah Menu Harian',
);
$this->title=array(
	'title'=>'Tambah Menu Harian',
	'deskripsi'=>'Untuk Menambah Menu Harian'
);?>

<div class="panel panel-default">
	<div class="panel-heading">
		<h3 class="panel-title pull-left">Form Tambah</h3>
		<a href="<?php echo Yii::app()->createUrl(Yii::app()->controller->module->id.'/'.Yii::app()->controller->id);?>" class="btn btn-primary btn-xs pull-right">
			<i class="fa fa-arrow-circle-left"></i>
			<span>Kembali</span>
		</a>
		<div style="clear:both"></div>
	</div>
	<div class="panel-body">		
		<?php
		if(Yii::app()->user->hasFlash('info')){
			echo'
			<div class="alert alert-info">
				<button type="button" class="close" data-dismiss="alert">&times;</button>
				<i class="fa fa-warning"></i>
				'.Yii::app()->user->getFlash('info').'
			</div>';
			
		}
		?>
		<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>	</div>
</div>