<?php $form=$this->beginWidget('booster.widgets.TbActiveForm',array(
	'id'=>'gizi-menu-harian-list-form',
	'enableAjaxValidation'=>true,
	'type'=>"vertical"
)); ?>


<?php echo $form->errorSummary($add); ?>
	
	<?php echo $form->dropDownListGroup($add,'gjd_id', array('widgetOptions'=>array('data'=>CHtml::listData(GiziJenisDiet::model()->findAll(),'gjd_id','gjd_name'), 'htmlOptions'=>array('class'=>'input-large','empty'=>'- Choose -')))); ?>
	
	<?php echo $form->dropDownListGroup($add,'gm_id', array('widgetOptions'=>array('data'=>CHtml::listData(GiziMenu::model()->findAll(),'gm_id','gm_name'), 'htmlOptions'=>array('class'=>'input-large','empty'=>'- Choose -')))); ?>

<div class="form-actions">
	<?php $this->widget('booster.widgets.TbButton', array(
			'buttonType'=>'submit',
			'context'=>'primary',
			'label'=>$add->isNewRecord ? 'Tambah' : 'Simpan',
		)); 
		?>

		</div>

<?php $this->endWidget(); ?>

