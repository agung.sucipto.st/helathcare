<?php
$this->breadcrumbs=array(
	'Kelola Help Desk',
);
$this->title=array(
	'title'=>'Kelola Help Desk',
	'deskripsi'=>'Untuk Mengelola Help Desk'
);

Yii::app()->clientScript->registerScript('search', "
$('.search-form').show();
$('.search-button').click(function(){
	$('.search-form').toggle();
	$('.import-form').hide();
	return false;
});
$('.btn-cancel').click(function(){
	$('.import-form').toggle();
	return false;
});
$('.import-button').click(function(){
	$('.import-form').toggle();
	$('.search-form').hide();
	return false;
});
$('.search-form form').submit(function(){
	$.fn.yiiGridView.update('help-desk-grid', {
		data: $(this).serialize()
	});
	return false;
});

");
?>

<div class="panel panel-default">
	<div class="panel-heading">
		<h3 class="panel-title pull-left">Data</h3>
		<a href="<?php echo Yii::app()->createUrl(Yii::app()->controller->module->id.'/'.Yii::app()->controller->id.'/grafik');?>" class="btn btn-primary btn-xs pull-right">
			<i class="fa fa-bar-chart-o"></i>
			<span>Grafik</span>
		</a>
		<a href="<?php echo Yii::app()->createUrl(Yii::app()->controller->module->id.'/'.Yii::app()->controller->id.'/create');?>" class="btn btn-primary btn-xs pull-right">
			<i class="fa fa-copy"></i>
			<span>Tambah Data</span>
		</a>
		
		<a href="#" class="btn btn-primary btn-xs pull-right search-button">
			<i class="fa fa-search"></i>
			<span>Search</span>
		</a>
		<a href="#" class="btn btn-primary btn-xs pull-right import-button">
			<i class="fa fa-upload"></i>
			<span>Import</span>
		</a>
		<div style="clear:both"></div>
	</div>
	<div class="panel-body">
		<div class="search-form" style="display:none">
			<?php $this->renderPartial('_search',array(
			'model'=>$model,
		)); ?>
		</div><!-- search-form -->
		<div class="import-form" style="display:none">
			<div class="xe-widget xe-counter-block" data-count=".num" data-from="0" data-to="99.9" data-suffix="%" data-duration="2">
			<div class="row">
				<div class="col-sm-7">
					<div class="xe-lower">
							
							<?php $form=$this->beginWidget('booster.widgets.TbActiveForm',array(
								'action'=>Yii::app()->createUrl(Yii::app()->controller->module->id.'/'.Yii::app()->controller->id.'/import'),
								'method'=>'POST',
								'htmlOptions'=>array('enctype'=>'multipart/form-data'),
							)); ?>
							<?php echo $form->fileFieldGroup($model, 'import',
									array(
										'wrapperHtmlOptions' => array(
											'class' => 'form-control',
										),
										'widgetOptions'=>array(
											'htmlOptions'=>array(
												'required'=>true,
												'accept'=>".xls"
											),
										),
										'label'=>''
										,
									)
								); ?>								<div class="text-center">
									<div class="col-sm-6">
										
									</div>
									<div class="col-sm-6">
									<button class="btn btn-success btn-icon btn-cancel">
											<i class="fa fa-times"></i>
											<span>Batal</span>
										</button>
										<button class="btn btn-success btn-icon">
											<i class="fa fa-file-o"></i>
											<span>Import</span>
										</button>
									</div>							
								</div>							
							
						<?php $this->endWidget(); ?>					</div>				
				</div>
				<div class="col-sm-5">
					<div class="xe-upper">
						<div class="xe-label">
							<!-- Excel Button --> 
							<a href="<?php echo Yii::app()->createUrl(Yii::app()->controller->module->id.'/'.Yii::app()->controller->id.'/downloadTemplate');?>" class="a-btn-2">
								<img src="<?php echo Yii::app()->theme->baseUrl; ?>/assets/images/button-xls.png" alt="Photos" /><span class="a-btn-2-text">Download Template</span> 
							</a>
						</div>
					</div>		
				</div>
			</div>

		

			</div>
		</div><!-- search-form -->
		<style>
		.pagination li.selected a{
			background:rgb(235, 235, 236);
		}
		</style>
		
		<div class="alert alert-info" id="info"></div>
		
		<?php
		if(Yii::app()->user->hasFlash('success')){
			echo'
			<div class="alert alert-success">
				<button type="button" class="close" data-dismiss="alert">&times;</button>
				'.Yii::app()->user->getFlash('success').'
			</div>';
		}
		?>
							
		<div id="example-2_wrapper" class="dataTables_wrapper form-inline dt-bootstrap no-footer">
		
		<?php 
		// put this somewhere on top
		$pageSize=Yii::app()->user->getState('pageSize',Yii::app()->params['defaultPageSize']); ?>		
		<?php		
		$this->widget('booster.widgets.TbExtendedGridView', array(
			'id'=>'help-desk-grid',
			'type' => 'striped',
			'dataProvider' => $model->search(),
			'filter'=>$model,
			'summaryText'=>'Menampilkan {start}-{end} dari {count} hasil.',
			'selectableRows' => 2,
			'responsiveTable' => true,
			'enablePagination' => true,
			'pager' => array(
				'htmlOptions'=>array(
					'class'=>'pagination'
				),
				'maxButtonCount' => 5,
				'cssFile' => true,
				'header' => false,
				'firstPageLabel' => '<<',
				'prevPageLabel' => '<',
				'nextPageLabel' => '>',
				'lastPageLabel' => '>>',
			),
			'bulkActions' => array(
			'actionButtons' => array(
				array(
					'buttonType' => 'button',
					'context' => 'primary',
					'size' => 'small',
					'label' => 'Hapus yang ditandai',
					'click' => 'js:confirmDelete',
					'id'=>'hdsk_id'
					
					),
				array(
					'buttonType' => 'button',
					'context' => 'primary',
					'size' => 'small',
					'label' => 'Export Excel',
					'click' => 'js:exportDataExcel',
					'id'=>'exportExcel'
					),
				array(
					'buttonType' => 'button',
					'context' => 'primary',
					'size' => 'small',
					'label' => 'Export PDF',
					'click' => 'js:exportDataPdf',
					'id'=>'exportPdf'
					),
				array(
					'buttonType' => 'button',
					'context' => 'primary',
					'size' => 'small',
					'label' => 'Print',
					'click' => 'js:printData',
					'id'=>'printData'
					)
			),
				'checkBoxColumnConfig' => array(
					'name' => 'hdsk_id'
				),
			),
			'columns'=>array(
					array(
						"header"=>$model->getAttributeLabel("hdsk_time"),
						"value"=>'Lib::dateInd($data->hdsk_time)',
						"name"=>"hdsk_time"
					),
					array(
						"header"=>$model->getAttributeLabel("hd_cat_id"),
						"value"=>'$data->hdCat->hd_cat_name',
						"name"=>"hd_cat_id",
						"filter"=>$model->getStructure()
					),
					array(
						"header"=>$model->getAttributeLabel("hdsk_content"),
						"value"=>'Lib::cut_text($data->hdsk_content,30)',
						"name"=>"hdsk_content",
					),
					array(
						"header"=>$model->getAttributeLabel("org_id"),
						"value"=>'$data->hdskUserReport->org->org_name." <br/>(".$data->hdskUserReport->emp_full_name.")"',
						"name"=>"org_id",
						"type"=>"raw",
						"filter"=>CHtml::listData(Organization::Model()->findAll(),"org_id","org_name")
					),
					array(
						"header"=>$model->getAttributeLabel("hdsk_status"),
						"value"=>'$data->hdsk_status',
						"name"=>"hdsk_status",
						"filter"=>array("Un Report"=>"Un Report","Report"=>"Report","Un Read"=>"Un Read","Read"=>"Read","Pending"=>"Pending","Progress"=>"Progress","Solved"=>"Solved","Reject"=>"Reject","Cancel"=>"Cancel","Waiting"=>"Waiting","Unknown"=>"Unknown",)
					),
					'hdsk_percentage',
					array(
						"header"=>$model->getAttributeLabel("hdsk_priority"),
						"value"=>'$data->hdsk_priority',
						"name"=>"hdsk_priority",
						"filter"=>array("Low"=>"Low","Mid"=>"Mid","High"=>"High","Very High"=>"Very High","Right Now"=>"Right Now",)
					),
					'hdsk_duedate',
					'hdsk_note',
					array(
						'class'=>'booster.widgets.TbButtonColumn',
						 'deleteConfirmation'=>'Anda yakin akan menhapus data?',
						'template'=>'{view}{update}{delete}',
						'buttons'=>array
						(
							'view' => array
							(
								'label'=>'View',
								'icon'=>'search',
								'options'=>array(
									'class'=>'btn btn-default btn-xs',
								),
							),
							'update' => array
							(
								'label'=>'Update',
								'icon'=>'pencil',
								'options'=>array(
									'class'=>'btn btn-default btn-xs',
								),
							),
							'delete' => array
							(
								'label'=>'Delete',
								'icon'=>'trash',
								'options'=>array(
									'class'=>'btn btn-default btn-xs delete',
								),
							)
						),
						'header'=>CHtml::dropDownList('pageSize',$pageSize,array(10=>10,20=>20,50=>50,100=>100,200=>200,500=>500,1000=>1000),array(
							'onchange'=>"$.fn.yiiGridView.update('help-desk-grid',{ data:{pageSize: $(this).val() }})",
						)),
					),
			),
		));
		?>
				
		<form action="" method="POST" id="formDownload" target="_blank">
			<input type="hidden" name="ids" id="ids"/>
		</form>
		
		<script type="text/javascript">
			$("#info").hide();
			var gridId = "help-desk-grid";
			var defaultMessage='';
			var values;
			function confirmDelete(data){
				values=data;
				if(defaultMessage!=''){
					$(".modal-body").html(defaultMessage);
					$(".modal-footer").show();
				}
				$("#modalKonfirmasi").modal('show');
			}
			$(function(){
				$("#modalKonfirmasiOk").click(function(){
					if(defaultMessage==''){
						defaultMessage=$(".modal-body").html();
					}
					$(".modal-body").html('Sedang Menghapus data...');
					$(".modal-footer").hide();
					batchActions(values);
				});
			});
			function batchActions(values){				
					$.ajax({
						type: "POST",
						url: '<?php echo Yii::app()->createUrl(Yii::app()->controller->module->id.'/'.Yii::app()->controller->id.'/batchDelete');?>',
						data: {"ids":values},
						dataType:'json',
						success: function(resp){
							if(resp.status == "success"){
							   $.fn.yiiGridView.update(gridId);
							   if(resp.data.failureCount>0){
									$("#info").html(resp.data.successCount+' Berhasil Di Hapus & '+resp.data.failureCount+' Gagal Di Hapus !<br/>Data yang gagal di hapus : '+resp.data.dataError);
							   }else{
									$("#info").html(resp.data.successCount+' Berhasil Di Hapus & '+resp.data.failureCount+' Gagal Di Hapus');
							   }
							   $("#info").fadeIn(5000);
							   $("#info").fadeOut(3000);
							}else{
							   $("#info").html(resp.msg);
							   $("#info").fadeIn(5000);
							   $("#info").fadeOut(5000);
							}
							$("#modalKonfirmasi").modal('hide');
						}
					});
			}
			function exportDataExcel(values){	
				$("#formDownload").attr('action','<?php echo Yii::app()->createUrl(Yii::app()->controller->module->id.'/'.Yii::app()->controller->id.'/exportExcel');?>');
				$("#ids").val(values.toString());
				$("#formDownload").submit();
			}
			
			function exportDataPdf(values){	
				$("#formDownload").attr('action','<?php echo Yii::app()->createUrl(Yii::app()->controller->module->id.'/'.Yii::app()->controller->id.'/exportPdf');?>');
				$("#ids").val(values.toString());
				$("#formDownload").submit();
			}
			
			function printData(values){	
				$("#formDownload").attr('action','<?php echo Yii::app()->createUrl(Yii::app()->controller->module->id.'/'.Yii::app()->controller->id.'/print');?>');
				$("#ids").val(values.toString());
				$("#formDownload").submit();
			}
		</script>
		</div>
	</div>
</div>