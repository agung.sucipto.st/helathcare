<div class="view">
		<b><?php echo CHtml::encode($data->getAttributeLabel('hdsk_id')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->hdsk_id),array('view','id'=>$data->hdsk_id)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('hdsk_time')); ?>:</b>
	<?php echo CHtml::encode($data->hdsk_time); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('hd_cat_id')); ?>:</b>
	<?php echo CHtml::encode($data->hd_cat_id); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('hdsk_content')); ?>:</b>
	<?php echo CHtml::encode($data->hdsk_content); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('hdsk_user_report')); ?>:</b>
	<?php echo CHtml::encode($data->hdsk_user_report); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('hdsk_user_recieve')); ?>:</b>
	<?php echo CHtml::encode($data->hdsk_user_recieve); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('hdsk_user_execute')); ?>:</b>
	<?php echo CHtml::encode($data->hdsk_user_execute); ?>
	<br />

	<?php /*
	<b><?php echo CHtml::encode($data->getAttributeLabel('hdsk_status')); ?>:</b>
	<?php echo CHtml::encode($data->hdsk_status); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('hdsk_percentage')); ?>:</b>
	<?php echo CHtml::encode($data->hdsk_percentage); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('hdsk_priority')); ?>:</b>
	<?php echo CHtml::encode($data->hdsk_priority); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('hdsk_solved_time')); ?>:</b>
	<?php echo CHtml::encode($data->hdsk_solved_time); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('hdsk_followup_time')); ?>:</b>
	<?php echo CHtml::encode($data->hdsk_followup_time); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('hdsk_duedate')); ?>:</b>
	<?php echo CHtml::encode($data->hdsk_duedate); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('hdsk_note')); ?>:</b>
	<?php echo CHtml::encode($data->hdsk_note); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('hdsk_approval')); ?>:</b>
	<?php echo CHtml::encode($data->hdsk_approval); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('create_at')); ?>:</b>
	<?php echo CHtml::encode($data->create_at); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('modified_at')); ?>:</b>
	<?php echo CHtml::encode($data->modified_at); ?>
	<br />

	*/ ?>
</div>