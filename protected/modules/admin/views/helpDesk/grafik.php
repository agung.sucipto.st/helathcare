<?php
$this->breadcrumbs=array(
	'Kelola Help Desk'=>array('index'),
	'Help Desk Chart',
);

$this->title=array(
	'title'=>'Detail Help Desk',
	'deskripsi'=>'Untuk Melihat Grafik Help Desk'
);
?>

<?php
$baseUrl = Yii::app()->theme->baseUrl; 
$cs = Yii::app()->getClientScript();
$cs->registerScriptFile($baseUrl.'/assets/morris/raphael-min.js');
$cs->registerScriptFile($baseUrl.'/assets/morris/morris.js');
$cs->registerCssFile($baseUrl.'/assets/morris/morris.css');
?>

<?php
$bar=array();
$legend=array();
foreach($detail as $idx=>$row){
	$bar[]="$idx:$row";
	$legend[]='"'.$idx.'"';
}
$color=array('"blue"','"green"','"pink"','"purple"','"orange"','"black"','"red"');
Yii::app()->clientScript->registerScript('validate', '
var colors_array= ['.implode(",",$color).'];	
var browsersChart = Morris.Donut({
  element: "donut-example",
  colors: colors_array,
 data: [
		'.implode(",",$data).'
	  ],
});
/*
browsersChart.options.data.forEach(function(label, i) {
var legendItem = $("<span></span>").text( label["label"] + " ( " +label["value"] + " )" ).prepend("<br><span>&nbsp;</span>");
legendItem.find("span")
  .css("backgroundColor", browsersChart.options.colors[i])
  .css("width", "20px")
  .css("display", "inline-block")
  .css("margin", "5px");
$("#legend").append(legendItem)
});
*/
Morris.Bar({
  element: "bar-example",
  barColors: colors_array,
  data: [
    { y: "Helpdesk", '.implode(",",$bar).' },
  ],
  xkey: "y",
  ykeys: ['.implode(",",$legend).'],
  labels: ['.implode(",",$legend).']
});

$("#print").click(function(){
    printMe();
});

function printMe() {
    xepOnline.Formatter.Format("donut-example", {render:"download", srctype:"svg"});
}	
', CClientScript::POS_END);
?>

<div class="panel panel-default">
	<div class="panel-heading">
		<h3 class="panel-title pull-left">Detail</h3>
		<a href="<?php echo Yii::app()->createUrl(Yii::app()->controller->module->id.'/'.Yii::app()->controller->id);?>" class="btn btn-primary btn-xs pull-right">
			<i class="fa fa-arrow-circle-left"></i>
			<span>Kembali</span>
		</a>
		<a href="<?php echo Yii::app()->createUrl(Yii::app()->controller->module->id.'/'.Yii::app()->controller->id.'/create');?>" class="btn btn-primary btn-xs pull-right">
			<i class="fa fa-copy"></i>
			<span>Tambah Data</span>
		</a>
		<div style="clear:both"></div>
	</div>
	<div class="panel-body">		
		<div id="example-2_wrapper" class="dataTables_wrapper form-inline dt-bootstrap no-footer">
		
			<div class="row">
	
			<?php $form=$this->beginWidget('booster.widgets.TbActiveForm',array(
				'action'=>Yii::app()->createUrl($this->route),
				'method'=>'POST',
			)); ?>

					<div class="col-md-3">
					<?php echo $form->datePickerGroup($model,'hdsk_time',array("label"=>"From",'widgetOptions'=>array('options'=>array('format'=>'yyyy-mm-dd','viewFormat'=>'yyyy-mm-dd'),'htmlOptions'=>array('class'=>'span5')), 'prepend'=>'<i class="glyphicon glyphicon-calendar"></i>', )); ?>
					</div>
					<div class="col-md-3">
					<?php echo $form->datePickerGroup($model,'hdsk_time_end',array("label"=>"To",'widgetOptions'=>array('options'=>array('format'=>'yyyy-mm-dd','viewFormat'=>'yyyy-mm-dd'),'htmlOptions'=>array('class'=>'span5')), 'prepend'=>'<i class="glyphicon glyphicon-calendar"></i>')); ?>
					</div>
					<div class="col-md-1" style="padding-top:25px">
					<?php $this->widget('booster.widgets.TbButton', array(
						'buttonType' => 'submit',
						'context'=>'primary',
						'label'=>'Search',
					)); ?>
					</div>

			<?php $this->endWidget(); ?>
			</div>
			<hr>

			<center><h4>Grafik HelpDesk<br/><?php echo $title;?></h4></center>
			<div class="row">
				<div class="col-md-4">
					<div id="donut-example">
					</div>
				</div>
				<div class="col-md-5">
					<div id="bar-example">
					</div>
				</div>
				<div class="col-md-3">
					<div id="legend" class="donut-legend"></div>
					<table class="table">
						<?php
						$index=0;
						$case=0;
						foreach($detail as $idx=>$row){
							echo'
							<tr>
								<td style="background:'.str_replace('"',"",$color[$index]).'">&nbsp;</div>
								<td>'.$idx.'</div>
								<td>'.$row.'</div>
							</tr>
							';
							$case+=$row;
							$index++;
						}
						?>
							<tr>
								<td>&nbsp;</div>
								<td>Total</div>
								<td><?php echo $case;?></div>
							</tr>
					</table>
				</div>
			</div>
		</div>
	</div>
</div>
