<div class="row">
	
<?php $form=$this->beginWidget('booster.widgets.TbActiveForm',array(
	'action'=>Yii::app()->createUrl($this->route),
	'method'=>'get',
)); ?>

		<div class="col-md-3">
		<?php echo $form->datePickerGroup($model,'hdsk_time',array("label"=>"From",'widgetOptions'=>array('options'=>array('format'=>'yyyy-mm-dd','viewFormat'=>'yyyy-mm-dd'),'htmlOptions'=>array('class'=>'span5')), 'prepend'=>'<i class="glyphicon glyphicon-calendar"></i>', )); ?>
		</div>
		<div class="col-md-3">
		<?php echo $form->datePickerGroup($model,'hdsk_time_end',array("label"=>"To",'widgetOptions'=>array('options'=>array('format'=>'yyyy-mm-dd','viewFormat'=>'yyyy-mm-dd'),'htmlOptions'=>array('class'=>'span5')), 'prepend'=>'<i class="glyphicon glyphicon-calendar"></i>')); ?>
		</div>
		<div class="col-md-3">
		<?php echo $form->dropDownListGroup($model,'org_id', array('widgetOptions'=>array('data'=>CHtml::listData(Organization::Model()->findAll(),"org_id","org_name"), 'htmlOptions'=>array('class'=>'input-large','empty'=>'- Choose -')))); ?>
		</div>
		<div class="col-md-2">
		<?php echo $form->dropDownListGroup($model,'hdsk_status', array('widgetOptions'=>array('data'=>array("Un Report"=>"Un Report","Report"=>"Report","Un Read"=>"Un Read","Read"=>"Read","Pending"=>"Pending","Progress"=>"Progress","Solved"=>"Solved","Reject"=>"Reject","Cancel"=>"Cancel","Waiting"=>"Waiting","Unknown"=>"Unknown",), 'htmlOptions'=>array('class'=>'input-large','empty'=>'- Choose -')))); ?>
		</div>
		<div class="col-md-1" style="padding-top:25px">
		<?php $this->widget('booster.widgets.TbButton', array(
			'buttonType' => 'submit',
			'context'=>'primary',
			'label'=>'Search',
		)); ?>
		</div>

<?php $this->endWidget(); ?>
</div>
<hr>