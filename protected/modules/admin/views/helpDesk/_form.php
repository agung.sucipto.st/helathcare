<link rel="stylesheet" href="<?php echo Yii::app()->theme->baseUrl; ?>/assets/css/bootstrap-select.css">
<script src="<?php echo Yii::app()->theme->baseUrl; ?>/assets/js/bootstrap-select.js"></script>
<script language="javascript" type="text/javascript" src="<?php echo Yii::app()->baseUrl; ?>/tinymce/plugin-tiny.js"></script>

<?php $form=$this->beginWidget('booster.widgets.TbActiveForm',array(
	'id'=>'help-desk-form',
	'enableAjaxValidation'=>true,
)); ?>

<p class="help-block">Isian dengan tanda <span class="required">*</span> wajib diisi.</p>

<?php echo $form->errorSummary($model); ?>

	<div class="row">
	<div class="col-md-3">
	<?php echo $form->dateTimePickerGroup($model,'hdsk_time',array('widgetOptions'=>array('options'=>array('format'=>'yyyy-mm-dd hh:ii','viewFormat'=>'yyyy-mm-dd hh:ii'), 'htmlOptions'=>array('value'=>($model->isNewRecord)?date("Y-m-d H:i:s"):$model->hdsk_time,'disable'=>($model->isNewRecord)?"false":"true")))) ;?>
	</div>
	<div class="col-md-3">
	<?php echo $form->dropDownListGroup($model,'hd_cat_id', array('widgetOptions'=>array('data'=>$model->getStructure(), 'htmlOptions'=>array('class'=>'input-large','empty'=>'- Choose -')))); ?>
	</div>
	<?php
	$emp=Employee::model()->findAll(array("condition"=>"emp_status='Active'","order"=>"emp_full_name ASC"));	
	$employee=array();
	foreach($emp as $row){
			$employee[$row->emp_id]=$row->emp_full_name.' | '.$row->emp_id;
	}
	
	$emp=Employee::model()->findAll(array("condition"=>"emp_status='Active' and org_id='11'","order"=>"emp_full_name ASC"));	
	$employeeRec=array();
	foreach($emp as $row){
			$employeeRec[$row->emp_id]=$row->emp_full_name.' | '.$row->emp_id;
	}
	?>
	<div class="col-md-3">
	<?php echo $form->dropDownListGroup($model,'hdsk_user_report', array('widgetOptions'=>array('data'=>$employee, 'htmlOptions'=>array('class'=>'input-large form-group selectpicker show-tick',"data-live-search"=>"true",'empty'=>'- Choose -'))));?>
	</div>
	<div class="col-md-3">
	<?php echo $form->dropDownListGroup($model,'hdsk_user_recieve', array('widgetOptions'=>array('data'=>$employeeRec, 'htmlOptions'=>array('class'=>'input-large form-group selectpicker show-tick',"data-live-search"=>"true",'empty'=>'- Choose -'))));
	?>
	</div>
	</div>
	<?php echo $form->textAreaGroup($model,'hdsk_content', array('widgetOptions'=>array('htmlOptions'=>array('rows'=>6, 'cols'=>50, 'class'=>'span8')))); ?>
	
	


	<?php echo $form->dropDownListGroup($model,'hdsk_status', array('widgetOptions'=>array('data'=>array("Un Report"=>"Un Report","Report"=>"Report","Un Read"=>"Un Read","Read"=>"Read","Pending"=>"Pending","Progress"=>"Progress","Solved"=>"Solved","Reject"=>"Reject","Cancel"=>"Cancel","Waiting"=>"Waiting","Unknown"=>"Unknown",), 'htmlOptions'=>array('class'=>'input-large')))); ?>

	<?php echo $form->dropDownListGroup($model,'hdsk_priority', array('widgetOptions'=>array('data'=>array("Low"=>"Low","Mid"=>"Mid","High"=>"High","Very High"=>"Very High","Right Now"=>"Right Now",), 'htmlOptions'=>array('class'=>'input-large')))); ?>

	<?php echo $form->datePickerGroup($model,'hdsk_duedate',array('widgetOptions'=>array('options'=>array('format'=>'yyyy-mm-dd','viewFormat'=>'yyyy-mm-dd'),'htmlOptions'=>array('class'=>'span5',"value"=>date("Y-m-d"))), 'prepend'=>'<i class="glyphicon glyphicon-calendar"></i>', 'append'=>'Klik Bulan/Tahun untuk merubah Bulan/Tahun.')); ?>
<div class="form-actions">
	<?php $this->widget('booster.widgets.TbButton', array(
			'buttonType'=>'submit',
			'context'=>'primary',
			'label'=>$model->isNewRecord ? 'Tambah' : 'Simpan',
		)); 
		echo CHtml::button('Batal', array(
            'class' => 'btn btn-primary',
            'onclick' => "history.go(-1)",
                )
        );
		?>

		</div>

<?php $this->endWidget(); ?>
