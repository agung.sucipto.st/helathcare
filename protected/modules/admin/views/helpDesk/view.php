<?php
$this->breadcrumbs=array(
	'Kelola Help Desk'=>array('index'),
	'Detail Help Desk',
);

$this->title=array(
	'title'=>'Detail Help Desk',
	'deskripsi'=>'Untuk Melihat Detail Help Desk'
);
?>

<div class="panel panel-default">
	<div class="panel-heading">
		<h3 class="panel-title pull-left">Detail</h3>
		<a href="<?php echo Yii::app()->createUrl(Yii::app()->controller->module->id.'/'.Yii::app()->controller->id);?>" class="btn btn-primary btn-xs pull-right">
			<i class="fa fa-arrow-circle-left"></i>
			<span>Kembali</span>
		</a>
		<a href="<?php echo Yii::app()->createUrl(Yii::app()->controller->module->id.'/'.Yii::app()->controller->id.'/create');?>" class="btn btn-primary btn-xs pull-right">
			<i class="fa fa-copy"></i>
			<span>Tambah Data</span>
		</a>
		<a href="<?php echo Yii::app()->createUrl(Yii::app()->controller->module->id.'/'.Yii::app()->controller->id.'/update/id/'.$_GET['id']);?>" class="btn btn-primary btn-xs pull-right">
			<i class="fa fa-edit"></i>
			<span>Edit Data</span>
		</a>
		<div style="clear:both"></div>
	</div>
	<div class="panel-body">		
		<div id="example-2_wrapper" class="dataTables_wrapper form-inline dt-bootstrap no-footer">
			<?php $this->widget('booster.widgets.TbDetailView',array(
			'data'=>$model,
			'attributes'=>array(
				'hdsk_id',
				'hdsk_time',
				array(
					"label"=>$model->getAttributeLabel("hd_cat_id"),
					"value"=>$model->hdCat->hd_cat_name,
				),
				array(
					"label"=>$model->getAttributeLabel("hdsk_content"),
					"value"=>$model->hdsk_content,
					"type"=>"raw"
				),
				array(
					"label"=>$model->getAttributeLabel("hdsk_user_report"),
					"value"=>$model->hdskUserReport->emp_full_name,
				),
				array(
					"label"=>$model->getAttributeLabel("hdsk_user_recieve"),
					"value"=>$model->hdskUserRecieve->emp_full_name,
				),
				array(
					"label"=>$model->getAttributeLabel("hdsk_user_execute"),
					"value"=>$model->hdskUserExecute->emp_full_name,
				),
				'hdsk_status',
				'hdsk_percentage',
				'hdsk_priority',
				'hdsk_solved_time',
				'hdsk_followup_time',
				'hdsk_duedate',
				'hdsk_note',
				'hdsk_approval',
				'create_at',
				'modified_at',
			),
			)); ?>
		</div>
	</div>
</div>
