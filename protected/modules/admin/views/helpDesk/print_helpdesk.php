<html>
	<style>
	@media print {
	   thead {display: table-header-group;}
	}
	table{
		border:1px solid #999999;
		width:100%;
		font-size:10px;
		border-collapse:collapse;
	}
	td,th{
		border:1px solid #999999;
		border-collapse:collapse;
		padding:2px;
		padding-left:5px;
	}
	th{
		text-align:center;
		padding:10px;
	}
	</style>
	<body>
		<center>
		<h2>Rekapitulasi Helpdesk</h2>
		<h4>Aulia Hospital pekanbaru</h4>
		</center>
		<div class="table">
			<table>
			<thead>
			  <tr>
				 <th>No</th>
				 <th style="width:180px;">Waktu</th>
				 <th>Kategori</th>
				 <th>Laporan</th>
				 <th>Unit</th>
				 <th>Status</th>
				 <th>Prioritas</th>
				 <th>Duedate</th>
				 <th>Catatan</th>
			  </tr>
			</thead>
			<tbody>
			<?php
			$no=0;
			foreach($model as $data){
				$no++;
				echo'
				<tr>
					 <td>'.$no.'</td>
					 <td>'.Lib::dateInd($data->hdsk_time).'</td>
					 <td>'.$data->hdCat->hd_cat_name.'</td>
					 <td>'.strip_tags($data->hdsk_content).'</td>
					 <td>'.$data->hdskUserReport->org->org_name." <br/>(".$data->hdskUserReport->emp_full_name.")".'</td>
					 <td>'.$data->hdsk_status.'</td>
					 <td>'.$data->hdsk_priority.'</td>
					 <td>'.Lib::dateInd($data->hdsk_duedate,false).'</td>
					 <td>'.$data->hdsk_note.'</td>
				  </tr>
				';
			}
			?>
			 <tbody>
			 </table>
		</div>
	</body>
</html>