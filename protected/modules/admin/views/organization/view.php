<?php
$this->breadcrumbs=array(
	'Kelola Organization'=>array('index'),
	'Detail Organization',
);

$this->title=array(
	'title'=>'Detail Organization',
	'deskripsi'=>'Untuk Melihat Detail Organization'
);
?>

<div class="panel panel-default">
	<div class="panel-heading">
		<h3 class="panel-title pull-left">Detail</h3>
		<a href="<?php echo Yii::app()->createUrl(Yii::app()->controller->module->id.'/'.Yii::app()->controller->id);?>" class="btn btn-primary btn-xs pull-right">
			<i class="fa fa-arrow-circle-left"></i>
			<span>Kembali</span>
		</a>
		<div style="clear:both"></div>
	</div>
	<div class="panel-body">		
		<div id="example-2_wrapper" class="dataTables_wrapper form-inline dt-bootstrap no-footer">
			<?php $this->widget('booster.widgets.TbDetailView',array(
			'data'=>$model,
			'attributes'=>array(
				'org_lv_id',
				'org_name',
				'org_parent_id',
			),
			)); ?>
		</div>
	</div>
</div>
