<div class="view">
		<b><?php echo CHtml::encode($data->getAttributeLabel('org_id')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->org_id),array('view','id'=>$data->org_id)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('org_lv_id')); ?>:</b>
	<?php echo CHtml::encode($data->org_lv_id); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('org_name')); ?>:</b>
	<?php echo CHtml::encode($data->org_name); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('org_parent_id')); ?>:</b>
	<?php echo CHtml::encode($data->org_parent_id); ?>
	<br />

</div>