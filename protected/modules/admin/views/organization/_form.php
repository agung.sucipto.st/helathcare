<?php $form=$this->beginWidget('booster.widgets.TbActiveForm',array(
	'id'=>'organization-form',
	'enableAjaxValidation'=>true,
)); ?>

<p class="help-block">Isian dengan tanda <span class="required">*</span> wajib diisi.</p>

<?php echo $form->errorSummary($model); ?>

	<?php echo $form->dropDownListGroup($model,'org_lv_id', array('widgetOptions'=>array('data'=>CHtml::listData(OrganizationLevel::model()->findAll(),'org_lv_id','org_lv_name'), 'htmlOptions'=>array('class'=>'input-large','empty'=>'- Organization Level -')))); ?>
	
	<?php echo $form->textFieldGroup($model,'org_name',array('widgetOptions'=>array('htmlOptions'=>array('class'=>'span5','maxlength'=>30)))); ?>

	<?php
	$parent=Organization::model()->findAll();
		foreach($parent as $row){
			$data[$row->org_parent_id][] = $row;
		}
	?>
	<?php echo $form->dropDownListGroup($model,'org_parent_id', array('widgetOptions'=>array('data'=>$model->getParent($data,0,0), 'htmlOptions'=>array('class'=>'input-large','empty'=>'- Organization -')))); ?>

<div class="form-actions">
	<?php $this->widget('booster.widgets.TbButton', array(
			'buttonType'=>'submit',
			'context'=>'primary',
			'label'=>$model->isNewRecord ? 'Tambah' : 'Simpan',
		)); 
		echo CHtml::button('Batal', array(
            'class' => 'btn btn-primary',
            'onclick' => "history.go(-1)",
                )
        );
		?>

		</div>

<?php $this->endWidget(); ?>
