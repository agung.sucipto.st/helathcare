<?php
$this->breadcrumbs=array(
	'Kelola Hisys',
);
$this->title=array(
	'title'=>'Kelola Hisys',
	'deskripsi'=>'Untuk Mengelola Hisys'
);
?>

<?php
Yii::app()->clientScript->registerScript('validatex', '

function pop(){
	window.open("'.Yii::app()->createUrl(Yii::app()->controller->module->id.'/hisys/printLabelRm').'","width=800,height=800");
}
', CClientScript::POS_END);
?>

<div class="panel panel-default">
	<div class="panel-heading">
		<h3 class="panel-title pull-left">Data</h3>
		<a href="<?php echo Yii::app()->createUrl(Yii::app()->controller->module->id.'/'.Yii::app()->controller->id.'/create');?>" class="btn btn-primary btn-xs pull-right">
			<i class="fa fa-copy"></i>
			<span>Tambah Data</span>
		</a>
		
		<div style="clear:both"></div>
	</div>
	<div class="panel-body">
		<span class="btn btn-app btn-primary" onclick="pop();">
		<i class="fa fa-copy"></i> Print Label Rekam Medik
	  </span>
	</div>
</div>