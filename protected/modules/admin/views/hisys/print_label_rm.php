<html>
	<style>
		body{
			font-family:Arial;
			
		}
		
		 @font-face { 
		font-family: IDAutomationHC39M;  
		src: url(<?php echo Yii::app()->theme->baseUrl;?>/assets/fonts/IDAutomationHC39M.woff);
		}
		.barcode {
		  font-family: 'IDAutomationHC39M'; 
		  width:auto;
		  font-size:10pt;
		}
		.barcode-info {
		  font-size:8pt;
		}
		.page{
			width:55mm;
			height:auto;
			margin-left:-4px;
		}
		.label{
			width:auto;
			height:18mm;
			padding-top:0.9mm;
			padding-left:2px;
			margin-bottom:0mm;
		}
		/*
		.label:nth-child(odd){
			background:pink;
		}
		.label:nth-child(even){
			background:green;
		}
		*/
		.content{
			width:100%;
			font-size:7pt;
		}
		.label:first-child{
			margin-top:-7px;
		}
		.label:last-child{
			margin-bottom:-10px;
		}
		th{
			text-align:left;
		}
		table{
			font-size:10px;
			font-weight:bold;
		}
		.patient{
			width:100%;
			font-weight:bold;
			font-size:9pt;
			overflow: hidden;
    white-space: nowrap;
    text-overflow: ellipsis;
		}
		.info{
			font-size:6pt;
			font-weight:bold;
		}
		
		@media print {
		  #visible{
			  display:none;
		  }
		}
	</style>
	<body>
		<form method="POST" action="" id="visible">
			<input type="url" name="url" placeholder="Copy Paste Link Label RM dari HiSys" size="50" value="<?php echo (isset($_POST['url'])?$_POST['url']:"");?>" autofocus/>
			<input type="number" name="n" placeholder="Jumlah Label" value="7" value="<?php echo (isset($_POST['n'])?$_POST['n']:"");?>"/>
			<input type="submit" value="Generate Label"/>
		</form>
		<button id="visible" onclick="print();">Print</button>
		<div class="page">
		<?php
			if(!empty($data)){
				for($i=0;$i<$data['n'];$i++){
					echo'
					<div class="label">
						<div class="content">
						<div class="wristband_container3">
							<span class="patient" style="text-align: left;">'.$data['nama'].'</span><br/>
							<span style="text-align: left;">'.$data['rm'].'</span><br/>
							<span style="text-align: left;">'.$data['ttl'].'</span><br/>
							<span>'.$data['dept'].'</span><br/>
							<span class="tanggal">'.$data['regis'].'</span><br/>
							<span class="info">'.$data['penjamin'].'</span>
						</div>
						</div>
					</div>
					';
				}
			}
		?>
			
		</div>
	</body>
</html>