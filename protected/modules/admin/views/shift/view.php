<?php
$this->breadcrumbs=array(
	'Kelola Shift'=>array('index'),
	'Detail Shift',
);

$this->title=array(
	'title'=>'Detail Shift',
	'deskripsi'=>'Untuk Melihat Detail Shift'
);
?>

<div class="panel panel-default">
	<div class="panel-heading">
		<h3 class="panel-title pull-left">Detail</h3>
		<a href="<?php echo Yii::app()->createUrl(Yii::app()->controller->module->id.'/'.Yii::app()->controller->id);?>" class="btn btn-primary btn-xs pull-right">
			<i class="fa fa-arrow-circle-left"></i>
			<span>Kembali</span>
		</a>
		<div style="clear:both"></div>
	</div>
	<div class="panel-body">		
		<div id="example-2_wrapper" class="dataTables_wrapper form-inline dt-bootstrap no-footer">
			<?php $this->widget('booster.widgets.TbDetailView',array(
			'data'=>$model,
			'attributes'=>array(
							'shift_id',
				'shift_group_id',
				'shift_day',
				'shift_periode',
				'shift_att_in',
				'shift_att_break_in',
				'shift_att_break_out',
				'shift_att_out',
				'shift_attend',
				'shift_create',
				'shift_update',
			),
			)); ?>
		</div>
	</div>
</div>
