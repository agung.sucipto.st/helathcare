<div class="view">
		<b><?php echo CHtml::encode($data->getAttributeLabel('shift_id')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->shift_id),array('view','id'=>$data->shift_id)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('shift_group_id')); ?>:</b>
	<?php echo CHtml::encode($data->shift_group_id); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('shift_day')); ?>:</b>
	<?php echo CHtml::encode($data->shift_day); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('shift_periode')); ?>:</b>
	<?php echo CHtml::encode($data->shift_periode); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('shift_att_in')); ?>:</b>
	<?php echo CHtml::encode($data->shift_att_in); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('shift_att_break_in')); ?>:</b>
	<?php echo CHtml::encode($data->shift_att_break_in); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('shift_att_break_out')); ?>:</b>
	<?php echo CHtml::encode($data->shift_att_break_out); ?>
	<br />

	<?php /*
	<b><?php echo CHtml::encode($data->getAttributeLabel('shift_att_out')); ?>:</b>
	<?php echo CHtml::encode($data->shift_att_out); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('shift_attend')); ?>:</b>
	<?php echo CHtml::encode($data->shift_attend); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('shift_create')); ?>:</b>
	<?php echo CHtml::encode($data->shift_create); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('shift_update')); ?>:</b>
	<?php echo CHtml::encode($data->shift_update); ?>
	<br />

	*/ ?>
</div>