<script language="javascript">
function selectTextPopUp(value){
	window.opener.selectTextGroup(value);
	window.self.close();
}
</script>
<br/>
<div class="container">
	<div class="panel panel-default">
		<div class="panel-title">	
			
		</div>
		<div class="panel-body">	
		<?php $this->widget('booster.widgets.TbGridView',array(
			'id'=>'shift-grid',
			'dataProvider'=>$model->search(),
			'filter'=>$model,
			'columns'=>array(
					array(
						"header"=>"Organization",
						"value"=>'$data->shiftGroup->org->org_name',
					),
					array(
						"header"=>"Group",
						"value"=>'$data->shiftGroup->shift_group_name',
						"name"=>"shift_group_id",
						"filter"=>CHtml::listData(ShiftGroup::model()->findAll(),"shift_group_id","shift_group_name")
					),
					array(
						"header"=>"Day/Periode",
						"value"=>function($data){
							if($data->shiftGroup->shift_group_type=="Weekly"){
								return $data->shift_day;
							}else{
								return $data->shift_periode;
							}
						}
					),
					'shift_att_in',
					'shift_att_out',
					'shift_attend',
					array(
						'header'=>CHtml::dropDownList('pageSize',$pageSize,array(10=>10,20=>20,50=>50,100=>100,200=>200,500=>500,1000=>1000),array(
							'onchange'=>"$.fn.yiiGridView.update('shift-grid',{ data:{pageSize: $(this).val() }})",
						)),
						'type'=>'raw',
						'value' => 'CHtml::link(\'<b class="btn btn-success "><i class="fa fa-check"></i> Pilih</b> \',"javascript: selectTextPopUp(\'$data->shift_id\')") ',
					),
			),
		)); ?>
		</div>
	</div>
</div>