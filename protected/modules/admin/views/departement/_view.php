<div class="view">
		<b><?php echo CHtml::encode($data->getAttributeLabel('dept_id')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->dept_id),array('view','id'=>$data->dept_id)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('dept_name')); ?>:</b>
	<?php echo CHtml::encode($data->dept_name); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('dept_description')); ?>:</b>
	<?php echo CHtml::encode($data->dept_description); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('dept_parent_id')); ?>:</b>
	<?php echo CHtml::encode($data->dept_parent_id); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('dept_create')); ?>:</b>
	<?php echo CHtml::encode($data->dept_create); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('dept_update')); ?>:</b>
	<?php echo CHtml::encode($data->dept_update); ?>
	<br />

</div>