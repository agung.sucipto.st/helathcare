<div class="container">

<?php
$arrayLate=array();


foreach($data as $row){
	$nmDay=Lib::getDay($row->att_date);
	$holliday=Holliday::model()->find(array("condition"=>"hd_start='$row->att_date'"));
	
	$no++;
	$statQuick='';
	$statLate='';
	$reason=array();
	$ot='';
	$quick='';
	$late=Lib::Late($row->shift->shift_att_in,$row->att_in);
	if($row->att_sts_id==""){
		if($row->att_should_attend=="Yes"){
			$nAtt++;
			if($row->att_out!='' AND $row->att_out!='00:00:00'){
				$quick=Lib::QuickHome($row->shift->shift_att_out,$row->att_out,$row->shift->shift_att_in);
				if($row->att_is_overtime==1){
					$ot=Lib::OverTime($row->shift->shift_att_out,$row->att_out);
				}else{
					$ot='';
				}
				if($quick=="00:00:00"){
					$statQuick='';
					$quick='';
				}else{
					if($row->att_is_possible_quick_home==1){
						$reason[]=$row->att_reason_possible_quick_home;
						$quick='';
					}else{
						$statQuick='style="background:red;color:white;"';
					}
					$nFast++;
				}
			}else{
				$ot='';
				$quick='';
			}
			
			if($late=="00:00:00"){
				$statLate='';
				$late='';
				$nOn++;
				$arrayLate[substr($row->att_date,0,7)]+=0;
			}else{
				if($late>"00:00:00"){
					if($row->att_is_possible_late==1){
						$statLate='';
						$reason[]=$row->att_reason_possible_late;
						$late='';
					}else{
						$statLate='style="background:red;color:white;"';
						$nLate++;
						$arrayLate[substr($row->att_date,0,7)]++;
					}
				}
			}
			$work_hour=Lib::WO($row->att_in,$row->att_out,$row->shift->shift_att_in,$row->shift->shift_att_out);
			$real_hour=Lib::WO($row->att_in,$row->att_out,$row->shift->shift_att_in,$row->att_out);
			$arrayWO[]=$work_hour;
			$arrayRO[]=$real_hour;
			if(($row->att_out=='' OR $row->att_out=='00:00:00') AND ($row->att_in=='' OR $row->att_in=='00:00:00')){
				$nNo++;
			}elseif($row->att_in=='' OR $row->att_in=='00:00:00'){
				$nAM++;
			}elseif($row->att_out=='' OR $row->att_out=='00:00:00'){
				$nAP++;
			}
		}else{
			$late='';
			$quick='';
			$ot='';
			$reason='';
			$work_hour='-';
			$real_hour='-';
		}
		
	}else{
		if($row->att_should_attend=="Yes"){
			$nAtt++;
		}
		/*
		$double=Attendance::model()->find(array("condition"=>"att_att_id='$row->att_id'"));
		if(empty($double)){
			$nNo++;
		}
		*/
		$late='';
		$quick='';
		$ot='';
		$reason='';
		$work_hour='-';
		$real_hour='-';
	}
	
}

?>


<?php
$baseUrl = Yii::app()->theme->baseUrl; 
$cs = Yii::app()->getClientScript();
$cs->registerScriptFile($baseUrl.'/assets/js/jquery.PrintArea.js');
$cs->registerScriptFile($baseUrl.'/assets/highcharts/highcharts.js');
$cs->registerScriptFile($baseUrl.'/assets/highcharts/modules/exporting.js');
?>


<?php
$emp=Employee::model()->findbyPk($emp);
$cat=array();
$val=array();
foreach($arrayLate as $i=>$y){
	$cat[]='"'.$i.'"';
	$val[]=$y;
}
Yii::app()->clientScript->registerScript('validate', '
Highcharts.chart("container2", {
    chart: {
        type: "line"
    },
    title: {
        text: "Personal Late '.$emp->emp_full_name.'<br/> '.Lib::dateInd($from,false).' - '.Lib::dateInd($to,false).'"
    },
    subtitle: {
        text: ""
    },
    xAxis: {
        categories: ['.implode(",",$cat).'],
    },
    yAxis: {
        title: {
            text: "Sum Of Late"
        }
    },
    plotOptions: {
        line: {
            dataLabels: {
                enabled: true
            },
            enableMouseTracking: false
        }
    },
    series: [{name: "Late",data: ['.implode(",",$val).']},]
});
', CClientScript::POS_END);
?>

<div id="container2" style="height: 400px; margin: 0 auto"></div>
</div>