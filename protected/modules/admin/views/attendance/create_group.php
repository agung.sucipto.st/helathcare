<?php
$this->breadcrumbs=array(
	'Manage Attendance'=>array('index'),
	'Create Attendance',
);
$this->title=array(
	'title'=>'Create Attendance',
	'deskripsi'=>'For Create Attendance'
);?>

<div class="fr">
	<a href="<?php echo Yii::app()->createUrl(Yii::app()->controller->module->id.'/'.Yii::app()->controller->id);?>" class="btn btn-red btn-icon btn-icon-standalone">
		<i class="fa-arrow-circle-left"></i>
		<span>Return</span>
	</a>
</div>
<div style="clear:both"></div>
<div class="panel panel-default">
	<div class="panel-heading">
		<h3 class="panel-title">Form Create</h3>
	</div>
	<div class="panel-body">		
		<?php $form=$this->beginWidget('booster.widgets.TbActiveForm',array(
			'id'=>'attendance-form',
			'enableAjaxValidation'=>true,
		)); ?>

		<p class="help-block">Required Form <span class="required">*</span> cannot be blank !.</p>

		<?php echo $form->errorSummary($model); ?>
			
			<?php echo $form->datePickerGroup($model,'att_date',array('widgetOptions'=>array('options'=>array('format'=>'yyyy-mm-dd','viewFormat'=>'yyyy-mm-dd'),'htmlOptions'=>array('class'=>'span5',"value"=>date("Y-m-d"))), 'prepend'=>'<i class="glyphicon glyphicon-calendar"></i>', 'append'=>'Click Month/Year to change Month/Year.')); ?>
			
			<?php	
			$data=Shift::model()->findAll();
			$shift=array();
			foreach($data as $row){
				$shift[$row->shift_id]=$row->shift_name.' | '.$row->shift_start.'-'.$row->shift_end;
			}
			echo $form->dropDownListGroup($model,'att_shift_id', array('widgetOptions'=>array('data'=>$shift, 'htmlOptions'=>array('class'=>'input-large','empty'=>'Choose Shift'))));
			?>
			
			<?php
			echo'
			<div class="form-group">
				<label class="control-label required" for="Attendance_emp_id">'.$model->getAttributeLabel('att_emp_id').'</label>
				<ul style="list-style:none;margin-left:-35px;">
			';
				$data=Employee::model()->findAll(array("condition"=>"emp_status='Active'","order"=>"RIGHT(emp_id,5) ASC"));
				$employee=array();
				foreach($data as $row){
					$checkAttandance=Attendance::model()->count(array("condition"=>"att_emp_id='$row->emp_id' AND att_date='".date("Y-m-d")."'"));
					if($checkAttandance==0){						
						echo "<li>";
						echo '<label><input type="checkbox" name="att_emp_id[]" value="'.$row->emp_id.'"> '.$row->emp_id.' | '.$row->emp_full_name.'</label>';
						echo '</li>';
					}
				}
			echo'
				</ul>
			</div>';
			?>
			
			<?php echo $form->timePickerGroup($model,'att_in',array('widgetOptions'=>array('options'=>array('format'=>'hh:mm:ss','viewFormat'=>'hh:mm:ss','minuteStep'=>5,'showMeridian'=>false)))); ?>
			
			<?php echo $form->timePickerGroup($model,'att_out',array('widgetOptions'=>array('options'=>array('format'=>'hh:mm:ss','viewFormat'=>'hh:mm:ss','minuteStep'=>5,'showMeridian'=>false)))); ?>
			
			
			<?php echo $form->dropDownListGroup($model,'att_status', array('widgetOptions'=>array('data'=>array(""=>"Choose Status","Sick"=>"Sick","Permission"=>"Permission","Without Explantion"=>"Without Explantion"), 'htmlOptions'=>array('class'=>'input-large')))); ?>

		<div class="form-actions">
			<?php $this->widget('booster.widgets.TbButton', array(
					'buttonType'=>'submit',
					'context'=>'primary',
					'label'=>$model->isNewRecord ? 'Create' : 'Save',
				)); 
				echo CHtml::button('Cancel', array(
					'class' => 'btn btn-primary',
					'onclick' => "history.go(-1)",
						)
				);
				?>

				</div>

		<?php $this->endWidget(); ?>

	</div>
</div>

<?php
Yii::app()->clientScript->registerScript('form-validate', "
$('#attendance-form').submit(function(){
	var sukses=0;
	$(this).find(':checkbox').each(function(){
		if($(this).prop('checked')){
			sukses++;
		}
	});
	if(sukses==0){
		alert('Employee Must Checked !');
		return false;
	}
});
");
?>