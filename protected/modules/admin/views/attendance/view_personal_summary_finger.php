

<?php
$this->breadcrumbs=array(
	'Attendance'=>array('index'),
	'Employee Attendance Report'=>array('employeeReport'),
	'Attendance List'=>array('viewReport'),
	'Personal Attendance List'
);
$this->title=array(
	'title'=>'Employee Attendance List',
	'deskripsi'=>'For View Employee Attendance'
);
?>




<div class="panel panel-default">
	<div class="panel-heading">
		<h3 class="panel-title pull-left">Detail</h3>
		<a href="<?php echo Yii::app()->createUrl(Yii::app()->controller->module->id.'/'.Yii::app()->controller->id."/summaryReportPersonal");?>" class="btn btn-primary btn-xs pull-right">
			<i class="fa fa-arrow-circle-left"></i>
			<span>Return</span>
		</a>
		<a href="<?php echo Yii::app()->createUrl(Yii::app()->controller->module->id.'/'.Yii::app()->controller->id."/viewSummaryPersonal",array("from"=>$_GET['from'],"to"=>$_GET['to'],"id"=>$_GET['id'],"print"=>"yes"));?>" class="btn btn-primary btn-xs pull-right" target="_blank">
			<i class="fa fa-print"></i>
			<span>Print</span>
		</a>
		<div style="clear:both"></div>
	</div>
	<div class="panel-body">	
		<?php 
		$employee=Employee::model()->findbyPk($_GET['id']);
		$this->widget('booster.widgets.TbDetailView',array(
			'data'=>$employee,
			'attributes'=>array(
			'emp_id',
			'emp_full_name',
			'emp_nick_name',
			),
			)); ?>
		<?php
		if(Yii::app()->user->hasFlash('notification')){
			echo'
			<div class="alert alert-warning">
				<button type="button" class="close" data-dismiss="alert">&times;</button>
				'.Yii::app()->user->getFlash('notification').'
			</div>';
		}
		?>
		<center>
			<h4>Attendance <br/><?php echo Lib::yearMonth($_GET['periode']);?></h4>
		</center>
		<?php $form=$this->beginWidget('booster.widgets.TbActiveForm',array(
			'id'=>'attendance-form',
		)); ?>
		<table class="table table-bordered table-hover">
			<tr>
				<th>No</th>
				<th>Date</th>
				<th>Shift</th>
				<th>In</th>
				<th>Out</th>
				<th>Finger Time</th>
				<th>Real Hour</th>
				<th>Work Hour</th>
				<th>Late</th>
				<th>Quick Return</th>
				<th>Over Time</th>
				<th>Status</th>
				<th>Note</th>
				<th>Manage</th>
			</tr>
			<?php	
	$data=Shift::model()->findAll();
	$shift=array();
	foreach($data as $row){
		$shift[$row->shift_id]=$row->shiftGroup->shift_group_name.' | '.$row->shiftGroup->org->org_name.' | '.$row->shift_att_in.' - '.$row->shift_att_out;
	}
	
	?>
		<?php
			$nLate=0;
			$nOn=0;
			$nFast=0;
			$nNo=0;
			$nAM=0;
			$nAP=0;
			$nAtt=0;
			$no=0;
			$arrayWO=array();
			$arrayRO=array();
			foreach($model as $row){
				$nmDay=Lib::getDay($row->att_date);
				$holliday=Holliday::model()->find(array("condition"=>"hd_start='$row->att_date'"));
				if($nmDay=="Sun" OR !empty($holliday)){
					if($row->att_should_attend=="No"){
						if(!empty($holliday)){
							$style='style="background:orange;color:black;"';
							$holly="".$holliday->hd_name;
						}elseif($nmDay=="Sun"){
							$style='style="background:#b0dcb0;color:black;"';
							$holly="Off";
						}else{
							$style='style="background:pink;color:black;"';
							$holly="";
						}
					}else{
						if(!empty($holliday)){
							$style='style="background:orange;color:black;"';
							$holly="".$holliday->hd_name;
						}elseif($nmDay=="Sun"){
							$style='style="background:#b0dcb0;color:black;"';
							$holly="";
						}
					}
				}else{
					if($row->att_should_attend=="No"){
						$style='style="background:pink;color:black;"';
						$holly="Off";
					}else{
						$style="";
						$holly="";
					}
				}
				$no++;
				$statQuick='';
				$statLate='';
				$reason=array();
				$ot='';
				$quick='';
				$late=Lib::Late($row->shift->shift_att_in,$row->att_in,(($row->att_date>='2017-09-12')?0:5));
				if($row->att_sts_id==""){
					if($row->att_should_attend=="Yes"){
						$nAtt++;
						if($row->att_out!='' AND $row->att_out!='00:00:00'){
							$quick=Lib::QuickHome($row->shift->shift_att_out,$row->att_out,$row->shift->shift_att_in);
							if($row->att_is_overtime==1){
								$ot=Lib::OverTime($row->shift->shift_att_out,$row->att_out);
							}else{
								$ot='';
							}
							if($quick=="00:00:00"){
								$statQuick='';
								$quick='';
							}else{
								if($row->att_is_possible_quick_home==1){
									$reason[]=$row->att_reason_possible_quick_home;
									$quick='';
								}else{
									$statQuick='style="background:red;color:white;"';
								}
								$nFast++;
							}
						}else{
							$ot='';
							$quick='';
							$nAP++;
						}
						if($row->att_in==''){
							$nAM++;
						}
						
						if($late=="00:00:00"){
							$statLate='';
							$late='';
							$nOn++;
						}else{
							if($late>"00:00:00"){
								if($row->att_is_possible_late==1){
									$statLate='';
									$reason[]=$row->att_reason_possible_late;
									$late='';
								}else{
									$statLate='style="background:red;color:white;"';
									$nLate++;
								}
							}
						}
						$work_hour=Lib::WO($row->att_in,$row->att_out,$row->shift->shift_att_in,$row->shift->shift_att_out);
						$real_hour=Lib::WO($row->att_in,$row->att_out,$row->shift->shift_att_in,$row->att_out);
						$arrayWO[]=$work_hour;
						$arrayRO[]=$real_hour;
					}else{
						$late='';
						$quick='';
						$ot='';
						$reason='';
						$work_hour='-';
						$real_hour='-';
					}
					if(($row->att_out=='' AND $row->att_out=='00:00:00') OR ($row->att_in=='' AND $row->att_in=='00:00:00')){
						$nNo++;
					}elseif($row->att_in=='' OR $row->att_in=='00:00:00'){
							$nAM++;
					}elseif($row->att_out=='' OR $row->att_out=='00:00:00'){
						$nAP++;
					}
					
				}else{
					if($row->att_should_attend=="Yes"){
						$nAtt++;
					}
					$nNo++;
					$late='';
					$quick='';
					$ot='';
					$reason='';
					$work_hour='-';
					$real_hour='-';
				}
				
				echo'
				<tr '.$style.'>
					<td>'.$no.'</td>
					<td>'.Lib::dateInd($row->att_date).'<br/><b>'.$holly.'</b></td>
					<td><select name="Attendance[shift_id]['.$row->att_id.']" class="form-group">
					<option>-Choose Shift-</option>
					';
					foreach($shift as $idx=>$data){
						if($idx==$row->shift_id){
							echo'<option value="'.$idx.'" selected>'.$data.'</option>';
						}else{
							echo'<option value="'.$idx.'">'.$data.'</option>';
						}
					}
					echo'
					</select></td>
					<td '.$statLate.'>'.$row->att_in.'</td>
					<td '.$statQuick.'>';
						if($row->att_sts_id==""){
							echo(($row->att_out!='')?$row->att_out:'<a href="'.Yii::app()->createUrl(Yii::app()->controller->module->id."/".Yii::app()->controller->id."/backHome",array("id"=>$row->att_id)).'" onClick="return confirm(\'End of the work?\');" class="btn btn-red btn-icon btn-icon-standalone" data-toggle="tooltip" data-original-title="Click To End of Work Today"><i class="fa fa-home"></i></a>');
						}
					echo'</td>
					<td>';
					$dF=array();
					$ex=explode("-",$row->att_date);
					$nextTo=date("Y-m-d",mktime(0,0,0,$ex[1],$ex[2]+1,$ex[0]));
					$prevTo=date("Y-m-d",mktime(0,0,0,$ex[1],$ex[2]-1,$ex[0]));
					$cekF=AttendanceTemp::model()->findAll(array("condition"=>"emp_id='".$row->attEmp->att_id."' and att_date between '$prevTo' and '$nextTo'"));
					foreach($cekF as $rF){
						$dF[]=$rF->att_date.', '.$rF->att_time;
					}
					echo implode("<br/>",$dF);
					
					echo'</td>
					<td>'.$real_hour.'</td>
					<td>'.$work_hour.'</td>
					<td>'.$late.'</td>
					<td>'.$quick.'</td>
					<td>'.$ot.'</td>
					<td><b>'.$row->attSts->att_sts_name.'</b><br/><small>'.$row->att_note.'</small></td>
					<td>'.((!empty($reason)) ? implode($reason,"<br/>") : '').'</td>
					<td><a href="'.Yii::app()->createUrl(Yii::app()->controller->module->id."/".Yii::app()->controller->id."/update",array("id"=>$row->att_id,"from"=>$_GET['from'],"to"=>$_GET['to'])).'" onClick="return confirm(\'Edit This Attendance?\');" class="btn btn-primary btn-xs" data-toggle="tooltip" data-original-title="Click To Edit Attendance Today"><i class="fa fa-edit"></i></a>&nbsp;<a href="'.Yii::app()->createUrl(Yii::app()->controller->module->id."/".Yii::app()->controller->id."/doubleShift",array("id"=>$row->att_id,"from"=>$_GET['from'],"to"=>$_GET['to'])).'" onClick="return confirm(\'Manage Double Shift?\');" class="btn btn-primary btn-xs" title="Click To Manage Double Shift"><i class="fa fa-refresh"></i></a></td>
				</tr>

				';
			}
		?>
			<tr>
				<td colspan="5">Jam Kerja</td>
				<td><?php echo Lib::TotalJam($arrayRO);?></td>
				<td><?php echo Lib::TotalJam($arrayWO);?></td>
				<td colspan="7"></td>
			</tr>
			
		</table>
			<button type="submit" class="btn btn-primary">Update</button>
		<?php $this->endWidget(); ?>
		<hr>
		<table class="table table-striped table-bordered">
			<tr>
				<th>On Time</th>
				<td><?php echo $nOn;?></td>
				<td><?php echo number_format(($nOn/$nAtt)*100,2);?> %</td>
			</tr>
			<tr>
				<th>Late</th>
				<td><?php echo $nLate;?></td>
				<td><?php echo number_format(($nLate/$nAtt)*100,2);?> %</td>
			</tr>
			<tr>
				<th>Quick Home</th>
				<td><?php echo $nFast;?></td>
				<td><?php echo number_format(($nFast/$nAtt)*100,2);?> %</td>
			</tr>
			<tr>
				<th>Lupa Absen Masuk</th>
				<td><?php echo $nAM;?></td>
				<td><?php echo number_format(($nAM/$nAtt)*100,2);?> %</td>
			</tr>
			<tr>
				<th>Lupa Absen Pulang</th>
				<td><?php echo $nAP;?></td>
				<td><?php echo number_format(($nAP/$nAtt)*100,2);?> %</td>
			</tr>
			<tr>
				<th>Not Present</th>
				<td><?php echo $nNo;?></td>
				<td><?php echo number_format(($nNo/$nAtt)*100,2);?> %</td>
			</tr>
			<tr>
				<th>Total Attendance</th>
				<td><?php echo $nAtt-$nNo;?> dari <?php echo $nAtt;?></td>
				<td><?php echo number_format((($nAtt-$nNo)/$nAtt)*100,2);?> %</td>
			</tr>
		</table>
	</div>
</div>
