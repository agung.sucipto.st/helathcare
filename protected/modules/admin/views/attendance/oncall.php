<?php
$this->breadcrumbs=array(
	'Manage Attendance'=>array('index'),
	'Manage Oncall',
);

$this->title=array(
	'title'=>'Manage Oncall',
	'deskripsi'=>'For Manage Oncall'
);?>


<div class="panel panel-default">
	<div class="panel-heading">
		<h3 class="panel-title">Form On Call</h3>
	</div>
	<div class="panel-body">	
		<?php $form=$this->beginWidget('booster.widgets.TbActiveForm',array(
			'id'=>'attendance-form',
			'enableAjaxValidation'=>true,
			'htmlOptions'=>array('enctype'=>'multipart/form-data'),
		)); ?>

		<?php echo $form->errorSummary($model); ?>
		
		<?php 
		if(!$model->isNewRecord){
			$this->widget('booster.widgets.TbDetailView',array(
				'data'=>$model,
				'attributes'=>array(
						array(
							"label"=>"Name",
							"value"=>$model->attEmp->emp_full_name
						),
						array(
							"label"=>"Date",
							"value"=>Lib::dateInd($model->att_date)
						)
					),
				));
				
				echo"<hr>";
		}
		
		?>
		
		<?php echo $form->datePickerGroup($model,'oncall_date',array('widgetOptions'=>array('options'=>array('format'=>'yyyy-mm-dd','viewFormat'=>'yyyy-mm-dd'),'htmlOptions'=>array('class'=>'span5',"value"=>$model->att_date)), 'prepend'=>'<i class="glyphicon glyphicon-calendar"></i>', 'append'=>'Click Month/Year to change Month/Year.')); ?>
		
		<?php echo $form->datePickerGroup($model,'oncall_date_to',array('widgetOptions'=>array('options'=>array('format'=>'yyyy-mm-dd','viewFormat'=>'yyyy-mm-dd'),'htmlOptions'=>array('class'=>'span5',"value"=>$model->att_date)), 'prepend'=>'<i class="glyphicon glyphicon-calendar"></i>', 'append'=>'Diisi hanya jika waktu oncall lintas hari')); ?>
		
		<?php echo $form->timePickerGroup($model,'oncall_start',array('widgetOptions'=>array('options'=>array('format'=>'hh:mm:ss','viewFormat'=>'hh:mm:ss','minuteStep'=>5,'showMeridian'=>false)))); ?>
	
		<?php echo $form->timePickerGroup($model,'oncall_end',array('widgetOptions'=>array('options'=>array('format'=>'hh:mm:ss','viewFormat'=>'hh:mm:ss','minuteStep'=>5,'showMeridian'=>false)))); ?>
		
		<div class="form-actions">
		<?php $this->widget('booster.widgets.TbButton', array(
				'buttonType'=>'submit',
				'context'=>'primary',
				'label'=>$model->isNewRecord ? 'Create' : 'Save',
			)); 
			echo CHtml::button('Cancel', array(
				'class' => 'btn btn-primary',
				'onclick' => "history.go(-1)",
					)
			);
			?>

			</div>

	<?php $this->endWidget(); ?>
	</div>
</div>