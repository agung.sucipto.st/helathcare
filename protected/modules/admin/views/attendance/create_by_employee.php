<?php
$this->breadcrumbs=array(
	'Manage Attendance'=>array('index'),
	'Create Attendance',
);
$this->title=array(
	'title'=>'Create Attendance',
	'deskripsi'=>'For Create Attendance'
);?>

<div class="fr">
	<a href="<?php echo Yii::app()->createUrl(Yii::app()->controller->module->id.'/'.Yii::app()->controller->id);?>" class="btn btn-red btn-icon btn-icon-standalone">
		<i class="fa-arrow-circle-left"></i>
		<span>Return</span>
	</a>
</div>
<div style="clear:both"></div>
<div class="panel panel-default">
	<div class="panel-heading">
		<h3 class="panel-title">Form Create</h3>
	</div>
	<div class="panel-body">		
		<?php $form=$this->beginWidget('booster.widgets.TbActiveForm',array(
			'id'=>'attendance-form',
			'enableAjaxValidation'=>true,
			'htmlOptions'=>array('enctype'=>'multipart/form-data'),
		)); ?>

		<p class="help-block">Required Form <span class="required">*</span> cannot be blank !.</p>

		<?php echo $form->errorSummary($model); ?>
			
			<?php 
				$data=Employee::model()->findByPk($_GET['id']);
				$this->widget('booster.widgets.TbDetailView',array(
					'data'=>$data,
					'attributes'=>array(
							array(
								"label"=>"Name",
								"value"=>$data->emp_full_name
							),
							array(
								"label"=>"Date",
								"value"=>Lib::dateInd($_GET['date'])
							)
						),
					));
					
					echo"<hr>";
			 ?>
			
			<?php	
			$data=Shift::model()->findAll();
			$shift=array();
			foreach($data as $row){
				$shift[$row->shift_id]=$row->shift_name.' | '.$row->shift_start.'-'.$row->shift_end;
			}
			//if($model->isNewRecord)
				echo $form->dropDownListGroup($model,'att_shift_id', array('widgetOptions'=>array('data'=>$shift, 'htmlOptions'=>array('class'=>'input-large','empty'=>'Choose Shift'))));
			?>
			
			<?php echo $form->timePickerGroup($model,'att_in',array('widgetOptions'=>array('options'=>array('format'=>'hh:mm:ss','viewFormat'=>'hh:mm:ss','minuteStep'=>5,'showMeridian'=>false)))); ?>
			
			<?php echo $form->timePickerGroup($model,'att_out',array('widgetOptions'=>array('options'=>array('format'=>'hh:mm:ss','viewFormat'=>'hh:mm:ss','minuteStep'=>5,'showMeridian'=>false)))); ?>
			
			<?php
			if($model->isNewRecord){
				$displayNote='display:none;';
				$displayOvertimeDocs='display:none;';
				$displayReasonQuick='display:none;';
				$displayReasonLate='display:none;';
			}else{		
				if($model->att_status!=''){
					$displayNote='display:block;';
				}else{
					$displayNote='display:none;';
				}
				if($model->att_is_overtime=='1'){
					$displayOvertimeDocs='display:block;';
				}else{
					$displayOvertimeDocs='display:none;';
				}
				if($model->att_is_possible_late=='1'){
					$displayReasonLate='display:block;';
				}else{
					$displayReasonLate='display:none;';
				}
				if($model->att_is_possible_quick_home=='1'){
					$displayReasonQuick='display:block;';
				}else{
					$displayReasonQuick='display:none;';
				}
			}
			?>
			
			<?php echo $form->dropDownListGroup($model,'att_status', array('widgetOptions'=>array('data'=>array(""=>"Choose Status","Sick"=>"Sick","Permission"=>"Permission","Without Explantion"=>"Without Explantion"), 'htmlOptions'=>array('class'=>'input-large')))); ?>

			<div id="att_note" style="<?php echo $displayNote;?>">
			<?php echo $form->textFieldGroup($model,'att_note',array('widgetOptions'=>array('htmlOptions'=>array('class'=>'span5')))); ?>
			</div>
			
			<?php echo $form->dropDownListGroup($model,'att_is_overtime', array('widgetOptions'=>array('data'=>array(""=>"Choose Option","1"=>"Yes","0"=>"No"), 'htmlOptions'=>array('class'=>'input-large')))); ?>

			<div id="att_overtime_docs" style="<?php echo $displayOvertimeDocs;?>">
			<?php echo $form->textFieldGroup($model,'att_overtime_docs',array('widgetOptions'=>array('htmlOptions'=>array('class'=>'span5')))); ?>
			</div>
			
			<?php echo $form->dropDownListGroup($model,'att_is_possible_late', array('widgetOptions'=>array('data'=>array(""=>"Choose Option","1"=>"Yes","0"=>"No"), 'htmlOptions'=>array('class'=>'input-large')))); ?>

			<div id="att_reason_possible_late" style="<?php echo $displayReasonLate;?>">
			<?php echo $form->textFieldGroup($model,'att_reason_possible_late',array('widgetOptions'=>array('htmlOptions'=>array('class'=>'span5')))); ?>
			</div>
			
			<?php echo $form->dropDownListGroup($model,'att_is_possible_quick_home', array('widgetOptions'=>array('data'=>array(""=>"Choose Option","1"=>"Yes","0"=>"No"), 'htmlOptions'=>array('class'=>'input-large')))); ?>

			<div id="att_reason_possible_quick_home" style="<?php echo $displayReasonQuick;?>">
			<?php echo $form->textFieldGroup($model,'att_reason_possible_quick_home',array('widgetOptions'=>array('htmlOptions'=>array('class'=>'span5')))); ?>
			</div>

		<div class="form-actions">
			<?php $this->widget('booster.widgets.TbButton', array(
					'buttonType'=>'submit',
					'context'=>'primary',
					'label'=>$model->isNewRecord ? 'Create' : 'Save',
				)); 
				echo CHtml::button('Cancel', array(
					'class' => 'btn btn-primary',
					'onclick' => "history.go(-1)",
						)
				);
				?>

				</div>

		<?php $this->endWidget(); ?>


		<?php Yii::app()->clientScript->registerScript('script',"	
			$('#Attendance_att_status').change(function(){
				if($(this).val()!=''){
					$('#att_note').show();
					$('#Attendance_att_note').attr('required','required');
				}else{
					$('#att_note').hide();
					$('#Attendance_att_note').val('');
				}
			});
			$('#Attendance_att_is_overtime').change(function(){
				if($(this).val()=='1'){
					$('#att_overtime_docs').show();
					$('#Attendance_att_overtime_docs').attr('required','required');
				}else{
					$('#att_overtime_docs').hide();
					$('#Attendance_att_overtime_docs').val('');
				}
			});
			$('#Attendance_att_is_possible_late').change(function(){
				if($(this).val()=='1'){
					$('#att_reason_possible_late').show();
					$('#Attendance_att_reason_possible_late').attr('required','required');
				}else{
					$('#att_reason_possible_late').hide();
					$('#Attendance_att_reason_possible_late').val('');
				}
			});
			$('#Attendance_att_is_possible_quick_home').change(function(){
				if($(this).val()=='1'){
					$('#att_reason_possible_quick_home').show();
					$('#Attendance_att_reason_possible_quick_home').attr('required','required');
				}else{
					$('#att_reason_possible_quick_home').hide();
					$('#Attendance_att_reason_possible_quick_home').val('');
				}
			});
		")?>
	</div>
</div>