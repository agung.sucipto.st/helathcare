<div class="container">

<?php
$arrayLate=array();
$arrayEarly=array();


foreach($data as $row){
	if($row->att_sts_id==""){
		if($row->att_should_attend=="Yes"){	
			if($row->att_in!=''){
			if($row->att_in<$row->shift->shift_att_in){
				$Best='00:30:00';
				$now=Lib::timeDiff($row->shift->shift_att_in,$row->att_in);
				if($now>=$Best){
					$arrayEarly[substr($row->att_date,0,7)]++;
				}
			}else{
				$arrayEarly[substr($row->att_date,0,7)]+=0;
			}
			}
			
		}		
	}	
}

?>


<?php
$baseUrl = Yii::app()->theme->baseUrl; 
$cs = Yii::app()->getClientScript();
$cs->registerScriptFile($baseUrl.'/assets/js/jquery.PrintArea.js');
$cs->registerScriptFile($baseUrl.'/assets/highcharts/highcharts.js');
$cs->registerScriptFile($baseUrl.'/assets/highcharts/modules/exporting.js');
?>


<?php
$emp=Employee::model()->findbyPk($emp);
$cat=array();
$val=array();
foreach($arrayEarly as $i=>$y){
	$cat[]='"'.$i.'"';
	$val[]=$y;
}
Yii::app()->clientScript->registerScript('validate', '
Highcharts.chart("container2", {
    chart: {
        type: "line"
    },
    title: {
        text: "Personal Early Come '.$emp->emp_full_name.'<br/> '.Lib::dateInd($from,false).' - '.Lib::dateInd($to,false).' <br/> 30 Minutes Early From Schedule"
    },
    subtitle: {
        text: ""
    },
    xAxis: {
        categories: ['.implode(",",$cat).'],
    },
    yAxis: {
        title: {
            text: "Sum Of Early"
        }
    },
    plotOptions: {
        line: {
            dataLabels: {
                enabled: true
            },
            enableMouseTracking: false
        }
    },
    series: [{name: "Early",data: ['.implode(",",$val).']},]
});
', CClientScript::POS_END);
?>

<div id="container2" style="height: 400px; margin: 0 auto"></div>
</div>