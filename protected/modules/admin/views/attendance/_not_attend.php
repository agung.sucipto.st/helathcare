<div class="container">
<?php
if(isset($_GET['back'])){
	?>
<a href="<?php echo Yii::app()->createUrl(Yii::app()->controller->module->id.'/'.Yii::app()->controller->id.'/notAttend');?>" class="btn btn-primary pull-right btn-xs hidden-print">
	<i class="fa fa-arrow-left"></i>
	<span>Return</span>
</a>

<?php
}
?>

<center>
	<h4>Not Attend Between <?=Lib::dateInd($from,false);?> - <?=Lib::dateInd($to,false);?></h4>
</center>

<hr>
<?php
if(!empty($data)){
	echo'<table class="table table-bordered">';
		echo'<tr>
			<th>No</th>
			<th>Date</th>
			<th>NIK</th>
			<th>Employee Name</th>
			<th>Status</th>
		</tr>';
		$no=0;
		foreach($data as $row){
			$no++;
			echo'
			<tr>
				<td>'.$no.'</td>
				<td>'.Lib::dateInd($row->att_date,false).'</td>
				<td>'.$row->att_emp_id.'</td>
				<td>'.$row->attEmp->emp_full_name.'</td>
				<td>'.$row->attSts->att_sts_name.'</td>
			</tr>
			';
		}
	echo'</table>';
}else{
	echo'<div class="well">No Data Available.</div>';
}
?>
</div>