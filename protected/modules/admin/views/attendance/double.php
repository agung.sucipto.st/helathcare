<?php
$cs=Yii::app()->clientScript;
$cs->scriptMap=array(
    'jquery.js'=>false,
    'bootstrap.min.js' => false,
    'bootstrap-noconflict.js' => false,
    'bootbox.min.js' => false,
    'notify.min.js' => false,
    'bootstrap.min.css' => true,
    'bootstrap-yii.css' => false,
    'jquery-ui-bootstrap.css' => false,
);?>
<?php
$this->breadcrumbs=array(
	'Manage Attendance'=>array('index'),
	'Manage Double Shift',
);

$this->title=array(
	'title'=>'Manage Double Shift',
	'deskripsi'=>'For Manage Double Shift'
);?>

<link rel="stylesheet" href="<?php echo Yii::app()->theme->baseUrl; ?>/assets/css/bootstrap.min.css">
<link rel="stylesheet" href="<?php echo Yii::app()->theme->baseUrl; ?>/assets/css/bootstrap-select.css">
 <script src="<?php echo Yii::app()->theme->baseUrl; ?>/assets/js/jquery-1.11.1.min.js"></script>
<script src="<?php echo Yii::app()->theme->baseUrl; ?>/assets/js/bootstrap.min.js"></script>
<script src="<?php echo Yii::app()->theme->baseUrl; ?>/assets/js/bootstrap-select.js"></script>

<div class="panel panel-default">
	<div class="panel-heading">
		<h3 class="panel-title">Form Double Shift</h3>
	</div>
	<div class="panel-body">	
		<?php $form=$this->beginWidget('booster.widgets.TbActiveForm',array(
			'id'=>'attendance-form',
			'enableAjaxValidation'=>true,
			'htmlOptions'=>array('enctype'=>'multipart/form-data'),
		)); ?>

		<?php echo $form->errorSummary($model); ?>

		<?php 
		if(!$model->isNewRecord){
			$this->widget('booster.widgets.TbDetailView',array(
				'data'=>$model,
				'attributes'=>array(
						array(
							"label"=>"Name",
							"value"=>$model->attEmp->emp_full_name
						),
						array(
							"label"=>"Date",
							"value"=>Lib::dateInd($model->att_date)
						)
					),
				));
				
				echo"<hr>";
		}
		
		?>
		
		<?php
		$atten=Attendance::model()->findAll(array("alias"=>"attendance","with"=>array("attEmp"),"condition"=>"attendance.att_date>='$model->att_date' and attEmp.org_id='".$model->attEmp->org_id."'"));
		$data=array();
		foreach($atten as $row){
			$data[$row->att_id]=$row->att_emp_id.' | '.$row->attEmp->emp_full_name.' | '.$row->shift->shift_att_in.' - '.$row->shift->shift_att_out.' | '.Lib::dateInd($row->att_date,true);
		}
		echo $form->dropDownListGroup($model,'att_att_id', array('widgetOptions'=>array('data'=>$data, 'htmlOptions'=>array('class'=>'input-large selectpicker show-tick','data-live-search'=>"true",'empty'=>"Choose Shift")))); ?>
		
		<div class="form-actions">
		<?php $this->widget('booster.widgets.TbButton', array(
				'buttonType'=>'submit',
				'context'=>'primary',
				'label'=>$model->isNewRecord ? 'Create' : 'Save',
			)); 
			echo CHtml::button('Cancel', array(
				'class' => 'btn btn-primary',
				'onclick' => "history.go(-1)",
					)
			);
			?>

			</div>

	<?php $this->endWidget(); ?>
	</div>
</div>