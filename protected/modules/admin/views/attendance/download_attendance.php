<?php
$this->breadcrumbs=array(
	'Manage Attendance'=>array('index'),
	'Download Attendance',
);

$this->title=array(
	'title'=>'Download Attendance',
	'deskripsi'=>'For Download Attendance'
);
?>
<?php
Yii::app()->clientScript->registerScript('validate', '

function openSearch(){
	window.open("'.Yii::app()->createUrl(Yii::app()->controller->module->id.'/employee/list').'","","width=800,height=600");
}

function selectText(id){
	$("#Attendance_att_emp_id").val(id);
}

', CClientScript::POS_END);
?>


<div style="clear:both"></div>
<div class="panel panel-default">
	<div class="panel-heading">
		<h3 class="panel-title pull-left">ReAttendance Machine Data</h3>
		<a href="<?php echo Yii::app()->createUrl(Yii::app()->controller->module->id.'/'.Yii::app()->controller->id);?>" class="btn btn-primary btn-xs pull-right">
			<i class="fa fa-arrow-circle-left"></i>
			<span>Return</span>
		</a>
		<div style="clear:both"></div>
	</div>
	<div class="panel-body">	
	<?php
		if(Yii::app()->user->hasFlash('result')){
			echo'
			<div class="row">
			<div class="alert alert-info">
				<button type="button" class="close" data-dismiss="alert">&times;</button>
				'.Yii::app()->user->getFlash('result').'
			</div>
			</div>
			';
		}
		?>
		
		<?php $form=$this->beginWidget('booster.widgets.TbActiveForm',array(
	'id'=>'attendance-form',
	'enableAjaxValidation'=>true,
	'htmlOptions'=>array('enctype'=>'multipart/form-data'),
)); ?>


<?php echo $form->errorSummary($model); ?>
	<?php	
	$data=Employee::model()->findAll(array("condition"=>"emp_status='Active'","order"=>"RIGHT(emp_id,5) ASC"));
	$employee=array();
	foreach($data as $row){
	
			$employee[$row->emp_id]=$row->emp_id.' | '.$row->emp_full_name;

	}
	if($model->isNewRecord){
	echo $form->dropDownListGroup($model,'att_emp_id', array('append'=>'<input type="button" value="Cari" onClick="openSearch()" >','widgetOptions'=>array('data'=>$employee, 'htmlOptions'=>array('class'=>'input-large','empty'=>'Choose Employee'))));
	}
	?>
	<?php 

	if($model->isNewRecord)echo $form->datePickerGroup($model,'att_date',array('label'=>"From",'widgetOptions'=>array('options'=>array('format'=>'yyyy-mm-dd','viewFormat'=>'yyyy-mm-dd'),'htmlOptions'=>array('class'=>'span5',"value"=>date("Y-m-d"))), 'prepend'=>'<i class="glyphicon glyphicon-calendar"></i>', 'append'=>'Click Month/Year to change Month/Year.')); 
	if($model->isNewRecord)echo $form->datePickerGroup($model,'att_date_to',array('label'=>"To",'widgetOptions'=>array('options'=>array('format'=>'yyyy-mm-dd','viewFormat'=>'yyyy-mm-dd'),'htmlOptions'=>array('class'=>'span5',"value"=>date("Y-m-d"))), 'prepend'=>'<i class="glyphicon glyphicon-calendar"></i>', 'append'=>'Click Month/Year to change Month/Year.')); 
	?>
	

<div class="form-actions">
	<?php $this->widget('booster.widgets.TbButton', array(
			'buttonType'=>'submit',
			'context'=>'primary',
			'htmlOptions'=>array(),
			'label'=>$model->isNewRecord ? 'Download' : 'Save',
		)); 
		?>

		</div>

<?php $this->endWidget(); ?>

	</div>
</div>
