<style>
	#custom{
		border:1px solid #999999;
		width:100%;
		font-size:10px;
	}
	#custom td,th{
		border:1px solid #999999;
		border-collapse:collapse;
		padding:2px;
		padding-left:5px;
	}
	#custom th{
		text-align:center;
	}
</style>

	<div class="container">	
		<div class="row">
			<div class="col-md-3">
				<img src="<?php echo Yii::app()->theme->baseUrl;?>/assets/images/logo@2x.png" width="180" alt="">
			</div>
			<div class="col-md-9">
					<center>
			<h4>Attendance <br/><?php echo Lib::yearMonth($_GET['periode']);?></h4>
		</center>
			</div>
		</div>
	
		<?php 
		$employee=Employee::model()->findbyPk($_GET['id']);
		?>
		<table>
			<tr>
				<td style="width:20%;padding:2px;">ID</td>
				<td style="width:2%">:</td>
				<td><?php echo $employee->emp_id;?></td>
			</tr>
			<tr>
				<td style="width:20%;padding:2px;">Name</td>
				<td style="width:2%">:</td>
				<td><?php echo $employee->emp_full_name;?></td>
			</tr>
		</table>
		<?php
		if(Yii::app()->user->hasFlash('notification')){
			echo'
			<div class="alert alert-warning">
				<button type="button" class="close" data-dismiss="alert">&times;</button>
				'.Yii::app()->user->getFlash('notification').'
			</div>';
		}
		?>
		
		<table id="custom">
			<tr>
				<th style="width:5%">No</th>
				<th style="width:30%">Date</th>
				<!--<th style="width:30%">Shift</th>-->
				<th style="width:5%">In</th>
				<th style="width:5%">Out</th>
				<th style="width:5%">Late</th>
				<th style="width:5%">Quick Return</th>
				<th style="width:5%">Over Time</th>
				<th style="width:10%">Status</th>
				<th style="width:5%">Note</th>
			</tr>
		<?php
			$no=0;
			foreach($model as $row){
				$no++;
				$statQuick='';
				$statLate='';
				$reason=array();
				$ot='';
				$quick='';
				$late=Lib::Late($row->attShift->shift_start,$row->att_in,(($row->att_date>='2017-09-12')?0:5));
				if($row->att_status==""){
					if($row->att_out!=''){
						$quick=Lib::QuickHome($row->attShift->shift_end,$row->att_out);
						if($row->att_is_overtime==1){
							$ot=Lib::OverTime($row->attShift->shift_end,$row->att_out);
						}else{
							$ot='';
						}
						if($quick=="00:00:00"){
							$statQuick='';
							$quick='';
						}else{
							if($row->att_is_possible_quick_home==1){
								$reason[]=$row->att_reason_possible_quick_home;
								$quick='';
							}else{
								$statQuick='style="background:red;color:white;"';
							}
						}
					}else{
						$ot='';
						$quick='';
					}
					
					if($late=="00:00:00"){
						$statLate='';
						$late='';
					}else{
						if($row->att_is_possible_late==1){
							$statLate='';
							$reason[]=$row->att_reason_possible_late;
							$late='';
						}else{
							$statLate='style="background:red;color:white;"';
						}
					}
				}else{
					$late='';
					$quick='';
					$ot='';
					$reason='';
				}
				
				echo'
				<tr>
					<td>'.$no.'</td>
					<td>'.Lib::dateInd($row->att_date).'</td>
					<!--<td>'.$row->attShift->shift_name.' ('.$row->attShift->shift_start.'-'.$row->attShift->shift_end.')</td>-->
					<td '.$statLate.'>'.$row->att_in.'</td>
					<td '.$statQuick.'>'.$row->att_out.'</td>
					<td>'.$late.'</td>
					<td>'.$quick.'</td>
					<td>'.$ot.'</td>
					<td><b>'.$row->att_status.'</b><br/><small>'.$row->att_note.'</small></td>
					<td>'.((!empty($reason)) ? implode($reason,"<br/>") : '').'</td>
					
				</tr>

				';
			}
		?>
		</table>
	</div>
