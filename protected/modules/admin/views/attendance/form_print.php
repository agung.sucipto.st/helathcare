<style>
	#custom{
		border:1px solid #999999;
		width:100%;
		font-size:10px;
	}
	#custom td,th{
		border:1px solid #999999;
		border-collapse:collapse;
		padding:2px;
		padding-left:5px;
	}
	#custom th{
		text-align:center;
	}
</style>
<div class="container">
	<div class="row">
		<div class="col-md-3">
			<img src="<?php echo Yii::app()->theme->baseUrl;?>/assets/images/logo@2x.png" width="180" alt="">
		</div>
		<div class="col-md-9">
			<center>
				<h4>Attendance <br/><?php echo Lib::dateInd($_POST['Attendance']['att_date'],true);?></h4>
			</center>
		</div>
	</div>
	<table id="custom">
		<tr>
			<th rowspan="2">No</th>
			<th rowspan="2">Employee ID</th>
			<th rowspan="2">Name</th>
			<th colspan="4">Attendance</th>
			<th rowspan="2">Status</th>
		</tr>
		<tr>
			<th style="width:100px;">IN</th>
			<th>SIGN</th>
			<th style="width:100px;">OUT</th>
			<th>SIGN</th>
		</tr>
	<?php
		$no=0;
		foreach($model as $row){
			$no++;

			echo'
			<tr>
				<td>'.$no.'</td>
				<td>'.$row->emp_id.'</td>
				<td>'.$row->emp_full_name.'</td>
				<td>&nbsp;</td>
				<td>&nbsp;</td>
				<td>&nbsp;</td>
				<td>&nbsp;</td>
				<td>&nbsp;</td>
			</tr>

			';
		}
	?>
	</table>
</div>
