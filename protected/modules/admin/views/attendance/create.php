<?php
$this->breadcrumbs=array(
	'Manage Attendance'=>array('index'),
	'Create Attendance',
);
$this->title=array(
	'title'=>'Create Attendance',
	'deskripsi'=>'For Create Attendance'
);?>

<div class="fr">
	<a href="<?php echo Yii::app()->createUrl(Yii::app()->controller->module->id.'/'.Yii::app()->controller->id);?>" class="btn btn-red btn-icon btn-icon-standalone">
		<i class="fa-arrow-circle-left"></i>
		<span>Return</span>
	</a>
</div>
<div style="clear:both"></div>
<div class="panel panel-default">
	<div class="panel-heading">
		<h3 class="panel-title">Form Create</h3>
	</div>
	<div class="panel-body">		
		<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>	</div>
</div>