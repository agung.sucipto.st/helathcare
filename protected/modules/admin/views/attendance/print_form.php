<?php
$this->breadcrumbs=array(
	'Manage Attendance'=>array('index'),
	'Print Form Attendance',
);
$this->title=array(
	'title'=>'Print Form Attendance',
	'deskripsi'=>'For Print Form Attendance'
);?>

<div class="fr">
	<a href="<?php echo Yii::app()->createUrl(Yii::app()->controller->module->id.'/'.Yii::app()->controller->id);?>" class="btn btn-red btn-icon btn-icon-standalone">
		<i class="fa-arrow-circle-left"></i>
		<span>Return</span>
	</a>
</div>
<div style="clear:both"></div>
<div class="panel panel-default">
	<div class="panel-heading">
		<h3 class="panel-title">Print Form</h3>
	</div>
	<div class="panel-body">		
		<?php $form=$this->beginWidget('booster.widgets.TbActiveForm',array(
			'id'=>'attendance-form',
			'enableAjaxValidation'=>true,
			'htmlOptions'=>array(
				'target'=>'_blank'
			)
		)); ?>

		<p class="help-block">Required Form <span class="required">*</span> cannot be blank !.</p>

		<?php echo $form->errorSummary($model); ?>
			
			<?php echo $form->datePickerGroup($model,'att_date',array('widgetOptions'=>array('options'=>array('format'=>'yyyy-mm-dd','viewFormat'=>'yyyy-mm-dd'),'htmlOptions'=>array('class'=>'span5',"value"=>date("Y-m-d"))), 'prepend'=>'<i class="glyphicon glyphicon-calendar"></i>', 'append'=>'Click Month/Year to change Month/Year.')); ?>

		<div class="form-actions">
			<?php $this->widget('booster.widgets.TbButton', array(
					'buttonType'=>'submit',
					'context'=>'primary',
					'label'=>$model->isNewRecord ? 'Print' : 'Save',
				)); 
				echo CHtml::button('Cancel', array(
					'class' => 'btn btn-primary',
					'onclick' => "history.go(-1)",
						)
				);
				?>

				</div>

		<?php $this->endWidget(); ?>

	</div>
</div>