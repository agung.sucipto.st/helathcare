<?php
$cs=Yii::app()->clientScript;
$cs->scriptMap=array(
    'jquery.js'=>false,
    'bootstrap.min.js' => false,
    'bootstrap-noconflict.js' => false,
    'bootbox.min.js' => false,
    'notify.min.js' => false,
    'bootstrap.min.css' => true,
    'bootstrap-yii.css' => false,
    'jquery-ui-bootstrap.css' => false,
);?>
<style>
	#custom{
		border:1px solid #999999;
		width:100%;
		font-size:9px;
		border-collapse:collapse;
	}
	#custom th{
		border:1px solid #999999;
		border-collapse:collapse;
		padding:2px;
		padding-left:5px;
	}
	
	#custom td{
		border:1px solid #999999;
		border-collapse:collapse;
		padding-left:5px;
		padding-right:5px;
	}
	#custom th{
		text-align:center;
	}
	.paper{
		width:210mm;
		height:297mm;
	}
	.container{
		margin:0px;
	}
	.table{
		padding:5px;
		font-size:10px;
	}
	.table th{
		text-align:right;
	}
	
	.table td{
		padding:5px;
	}
	.table tr{
		border-bottom:1px solid #ccc;
	}
</style> 

<div class="paper">
		<table width="100%">
			<tr>
				<td style="width:20%;">
					<img src="<?php echo Yii::app()->theme->baseUrl;?>/assets/images/logo@2x.png" width="180" alt="">
				</td>
				<td style="width:80%;text-align:center;">
					<h4>Attendance <br/><?php echo Lib::yearMonth($_GET['periode']);?></h4>
				</td>
			</tr>
		</table>
	
		<?php 
		$employee=Employee::model()->findbyPk($id);
		?>
		<table>
			<tr>
				<td style="width:20%;padding:2px;">ID</td>
				<td style="width:2%">:</td>
				<td><?php echo $employee->emp_id;?></td>
			</tr>
			<tr>
				<td style="width:20%;padding:2px;">Name</td>
				<td style="width:2%">:</td>
				<td><?php echo $employee->emp_full_name;?></td>
			</tr>
		</table>
		<?php
		if(Yii::app()->user->hasFlash('notification')){
			echo'
			<div class="alert alert-warning">
				<button type="button" class="close" data-dismiss="alert">&times;</button>
				'.Yii::app()->user->getFlash('notification').'
			</div>';
		}
		?>
		
		<table id="custom">
			<tr>
				<th style="width:5%">No</th>
				<th style="width:20%">Date</th>
				<th style="width:10%">Shift</th>
				<th style="width:5%">In</th>
				<th style="width:5%">Out</th>
				<th>Real Hour</th>
				<th>Work Hour</th>
				<th style="width:5%">Late</th>
				<th style="width:5%">Quick Return</th>
				<th style="width:5%">Over Time</th>
				<th style="width:10%">Status</th>
				<th style="width:10%">Information</th>
				<th style="width:5%">Note</th>
			</tr>
		<?php
		$nLate=0;
			$nOn=0;
			$nFast=0;
			$nNo=0;
			$nAM=0;
			$nAP=0;
			$no=0;
			$notAttendance=0;
			$arrayWO=array();
			$arrayRO=array();
			foreach($model as $row){
				$nmDay=Lib::getDay($row->att_date);
				$holliday=Holliday::model()->find(array("condition"=>"hd_start='$row->att_date'"));
				if($nmDay=="Sun" OR !empty($holliday)){
					if($row->att_should_attend=="No"){
						if(!empty($holliday)){
							$style='style="background:orange;color:black;"';
							$holly="".$holliday->hd_name;
						}elseif($nmDay=="Sun"){
							$style='style="background:#b0dcb0;color:black;"';
							$holly="Off";
						}else{
							$style='style="background:pink;color:black;"';
							$holly="";
						}
					}else{
						if(!empty($holliday)){
							$style='style="background:orange;color:black;"';
							$holly="".$holliday->hd_name;
						}elseif($nmDay=="Sun"){
							$style='style="background:#b0dcb0;color:black;"';
							$holly="";
						}
					}
				}else{
					if($row->att_should_attend=="No"){
						$style='style="background:pink;color:black;"';
						$holly="Off";
					}else{
						$style="";
						$holly="";
					}
				}
				$no++;
				$statQuick='';
				$statLate='';
				$reason=array();
				$ot='';
				$quick='';
				$late=Lib::Late($row->shift->shift_att_in,$row->att_in,(($row->att_date>='2017-09-12')?0:5));
				if($row->att_sts_id==""){
					if($row->att_should_attend=="Yes"){
						$nAtt++;
						if($row->att_out!='' AND $row->att_out!='00:00:00'){
							$quick=Lib::QuickHome($row->shift->shift_att_out,$row->att_out,$row->shift->shift_att_in);
							if($row->att_is_overtime==1){
								$ot=Lib::OverTime($row->shift->shift_att_out,$row->att_out);
							}else{
								$ot='';
							}
							if($quick=="00:00:00"){
								$statQuick='';
								$quick='';
							}else{
								if($row->att_is_possible_quick_home==1){
									$reason[]=$row->att_reason_possible_quick_home;
									$quick='';
								}else{
									$statQuick='style="background:red;color:white;"';
								}
								$nFast++;
							}
						}else{
							$ot='';
							$quick='';
						}
						
						if($late=="00:00:00"){
							$statLate='';
							$late='';
							$nOn++;
						}else{
							if($late>"00:00:00"){
								if($row->att_is_possible_late==1){
									$statLate='';
									$reason[]=$row->att_reason_possible_late;
									$late='';
								}else{
									$statLate='style="background:red;color:white;"';
									$nLate++;
								}
							}
						}
						$work_hour=Lib::WO($row->att_in,$row->att_out,$row->shift->shift_att_in,$row->shift->shift_att_out);
						$real_hour=Lib::WO($row->att_in,$row->att_out,$row->shift->shift_att_in,$row->att_out);
						$arrayWO[]=$work_hour;
						$arrayRO[]=$real_hour;
						if(($row->att_out=='' OR $row->att_out=='00:00:00') AND ($row->att_in=='' OR $row->att_in=='00:00:00')){
							$nNo++;
						}elseif($row->att_in=='' OR $row->att_in=='00:00:00'){
							$nAM++;
						}elseif($row->att_out=='' OR $row->att_out=='00:00:00'){
							$nAP++;
						}
					}else{
						$late='';
						$quick='';
						$ot='';
						$reason='';
						$work_hour='-';
						$real_hour='-';
					}
					
				}else{
					if($row->att_should_attend=="Yes"){
						$nAtt++;
					}
					/*
					$double=Attendance::model()->find(array("condition"=>"att_att_id='$row->att_id'"));
					if(empty($double)){
						$nNo++;
					}
					*/
					$late='';
					$quick='';
					$ot='';
					$reason='';
					$work_hour='-';
					$real_hour='-';
				}
				
				echo'
				<tr '.$style.'>
					<td>'.$no.'</td>
					<td>'.Lib::dateInd($row->att_date).'<br/><b>'.$holly.'</b></td>
					<td>'.$row->shift->shift_att_in.'-'.$row->shift->shift_att_out.'</td>
					
					<td '.$statLate.'>'.$row->att_in.'</td>
					<td '.$statQuick.'>'.$row->att_out.'</td>
					<td>'.$real_hour.'</td>
					<td>'.$work_hour.'</td>
					<td>'.$late.'</td>
					<td>'.$quick.'</td>
					<td>'.$ot.'</td>
					<td><b>'.$row->attSts->att_sts_name.'</b><br/><small>'.$row->att_note.'</small></td>
					<td>';
					if(!empty($row->att_att_id)){
						$double=Attendance::model()->findbyPk($row->att_att_id);
						echo "Menggantikan Shift : ".$double->attEmp->emp_full_name."<br/>".$double->shift->shift_att_in.' - '.$double->shift->shift_att_out;
					}else{
						$double=Attendance::model()->find(array("condition"=>"att_att_id='$row->att_id'"));
						if(!empty($double)){
						echo "Digantikan Oleh : ".$double->attEmp->emp_full_name."<br/>".$double->shift->shift_att_in.' - '.$double->shift->shift_att_out;
						}
					}
					if($row->att_is_overtime==1){
						echo "Lembur";
					}
					
					echo'</td>
					<td>'.((!empty($reason)) ? implode($reason,"<br/>") : '').'</td>
					
				</tr>
				';
			}
		?>
			<tr>
				<td colspan="5">Jam Kerja</td>
				<td><?php echo Lib::TotalJam($arrayRO);?></td>
				<td><?php echo Lib::TotalJam($arrayWO);?></td>
				<td colspan="6"></td>
			</tr>
		</table>
		<hr>
		<table id="custom">
			<tr>
				<th>On Time</th>
				<td><?php echo $nOn;?></td>
				<td><?php echo number_format(($nOn/$nAtt)*100,2);?> %</td>
			</tr>
			<tr>
				<th>Late</th>
				<td><?php echo $nLate;?></td>
				<td><?php echo number_format(($nLate/$nAtt)*100,2);?> %</td>
			</tr>
			<tr>
				<th>Quick Home</th>
				<td><?php echo $nFast;?></td>
				<td><?php echo number_format(($nFast/$nAtt)*100,2);?> %</td>
			</tr>
			<tr>
				<th>Lupa Absen Masuk</th>
				<td><?php echo $nAM;?></td>
				<td><?php echo number_format(($nAM/$nAtt)*100,2);?> %</td>
			</tr>
			<tr>
				<th>Lupa Absen Pulang</th>
				<td><?php echo $nAP;?></td>
				<td><?php echo number_format(($nAP/$nAtt)*100,2);?> %</td>
			</tr>
			<tr>
				<th>Not Present</th>
				<td><?php echo $nNo;?></td>
				<td><?php echo number_format(($nNo/$nAtt)*100,2);?> %</td>
			</tr>
			<tr>
				<th>Total Attendance</th>
				<td><?php echo $nAtt-$nNo;?> dari <?php echo $nAtt;?></td>
				<td><?php echo number_format((($nAtt-$nNo)/$nAtt)*100,2);?> %</td>
			</tr>
		</table>
</div>