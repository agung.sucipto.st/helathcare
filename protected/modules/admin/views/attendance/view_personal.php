<?php
$this->breadcrumbs=array(
	'Attendance'=>array('index'),
	'Employee Attendance Report'=>array('employeeReport'),
	'Attendance List'=>array('viewReport'),
	'Personal Attendance List'
);
$this->title=array(
	'title'=>'Employee Attendance List',
	'deskripsi'=>'For View Employee Attendance'
);
?>


<div class="fr">
	<a href="<?php echo Yii::app()->createUrl(Yii::app()->controller->module->id.'/'.Yii::app()->controller->id."/viewReport",array("id"=>$_GET['id']));?>" class="btn btn-red btn-icon btn-icon-standalone">
		<i class="fa-arrow-circle-left"></i>
		<span>Return</span>
	</a>
	<a href="<?php echo Yii::app()->createUrl(Yii::app()->controller->module->id.'/'.Yii::app()->controller->id."/viewPersonal",array("periode"=>$_GET['periode'],"id"=>$_GET['id'],"print"=>"yes"));?>" class="btn btn-secondary btn-icon btn-icon-standalone" target="_blank">
		<i class="fa-print"></i>
		<span>Print</span>
	</a>
</div>
<div style="clear:both"></div>
<div class="panel panel-default">
	<div class="panel-heading">
		<h3 class="panel-title">Detail</h3>
	</div>
	<div class="panel-body">	
		<?php 
		$employee=Employee::model()->findbyPk($_GET['id']);
		$this->widget('booster.widgets.TbDetailView',array(
			'data'=>$employee,
			'attributes'=>array(
			'emp_id',
			'emp_full_name',
			'emp_nick_name',
			),
			)); ?>
		<?php
		if(Yii::app()->user->hasFlash('notification')){
			echo'
			<div class="alert alert-warning">
				<button type="button" class="close" data-dismiss="alert">&times;</button>
				'.Yii::app()->user->getFlash('notification').'
			</div>';
		}
		?>
		<center>
			<h4>Attendance <br/><?php echo Lib::yearMonth($_GET['periode']);?></h4>
		</center>
		<table class="table table-bordered table-striped">
			<tr>
				<th>No</th>
				<th>Date</th>
				<th>Shift</th>
				<th>In</th>
				<th>Out</th>
				<th>Late</th>
				<th>Quick Return</th>
				<th>Over Time</th>
				<th>Status</th>
				<th>Note</th>
				<th>Manage</th>
			</tr>
		<?php
			$no=0;
			foreach($model as $row){
				$no++;
				$statQuick='';
				$statLate='';
				$reason=array();
				$ot='';
				$quick='';
				$late=Lib::Late($row->shift->shift_att_in,$row->att_in,(($row->att_date>='2017-09-12')?0:5));
				if($row->att_sts_id==""){
					if($row->att_out!=''){
						$quick=Lib::QuickHome($row->shift->shift_att_out,$row->att_out);
						if($row->att_is_overtime==1){
							$ot=Lib::OverTime($row->shift->shift_att_out,$row->att_out);
						}else{
							$ot='';
						}
						if($quick=="00:00:00"){
							$statQuick='';
							$quick='';
						}else{
							if($row->att_is_possible_quick_home==1){
								$reason[]=$row->att_reason_possible_quick_home;
								$quick='';
							}else{
								$statQuick='style="background:red;color:white;"';
							}
						}
					}else{
						$ot='';
						$quick='';
					}
					
					if($late=="00:00:00"){
						$statLate='';
						$late='';
					}else{
						if($row->att_is_possible_late==1){
							$statLate='';
							$reason[]=$row->att_reason_possible_late;
							$late='';
						}else{
							$statLate='style="background:red;color:white;"';
						}
					}
				}else{
					$late='';
					$quick='';
					$ot='';
					$reason='';
				}
				
				echo'
				<tr>
					<td>'.$no.'</td>
					<td>'.Lib::dateInd($row->att_date).'</td>
					<td>'.$row->shift->shiftGroup->shift_group_name.' ('.$row->shift->shift_att_in.'-'.$row->shift->shift_att_out.')</td>
					<td '.$statLate.'>'.$row->att_in.'</td>
					<td '.$statQuick.'>';
						if($row->att_sts_id==""){
							echo(($row->att_out!='')?$row->att_out:'<a href="'.Yii::app()->createUrl(Yii::app()->controller->module->id."/".Yii::app()->controller->id."/backHome",array("id"=>$row->att_id)).'" onClick="return confirm(\'End of the work?\');" class="btn btn-red btn-icon btn-icon-standalone" data-toggle="tooltip" data-original-title="Click To End of Work Today"><i class="fa-home"></i></a>');
						}
					echo'</td>
					<td>'.$late.'</td>
					<td>'.$quick.'</td>
					<td>'.$ot.'</td>
					<td><b>'.$row->attSts->att_sts_name.'</b><br/><small>'.$row->att_note.'</small></td>
					<td>'.((!empty($reason)) ? implode($reason,"<br/>") : '').'</td>
					<td><a href="'.Yii::app()->createUrl(Yii::app()->controller->module->id."/".Yii::app()->controller->id."/update",array("id"=>$row->att_id)).'" onClick="return confirm(\'Edit This Attendance?\');" class="btn btn-secondary btn-icon btn-icon-standalone" data-toggle="tooltip" data-original-title="Click To Edit Attendance Today"><i class="fa-edit"></i></a></td>
				</tr>

				';
			}
		?>
		</table>
	</div>
</div>
