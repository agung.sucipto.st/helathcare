<?php
$this->breadcrumbs=array(
	'Manage Attendance'=>array('index'),
	'Detail Attendance',
);

$this->title=array(
	'title'=>'Detail Attendance',
	'deskripsi'=>'For View Detail Attendance'
);
?>


<div class="panel panel-default">
	<div class="panel-heading">
		<h3 class="panel-title pull-left">Detail</h3>
		<a href="<?php echo Yii::app()->createUrl(Yii::app()->controller->module->id.'/'.Yii::app()->controller->id);?>" class="btn btn-primary pull-right btn-xs">
			<i class="fa fa-arrow-left"></i>
			<span>Return</span>
		</a>
		<div style="clear:both"></div>
	</div>
	<div class="panel-body">	
		<?php
		if(Yii::app()->user->hasFlash('notification')){
			echo'
			<div class="alert alert-warning">
				<button type="button" class="close" data-dismiss="alert">&times;</button>
				'.Yii::app()->user->getFlash('notification').'
			</div>';
		}
		
		?>
		<center>
			<h4>Attendance <br/><?php echo Lib::dateInd($_GET['periode'],true);?></h4>
		</center>
		<table class="table table-bordered table-striped">
			<tr>
				<th>No</th>
				<th>Employee ID</th>
				<th>Name</th>
				<th>Shift</th>
				<th>In</th>
				<th>Out</th>
				<th>Late</th>
				<th>Quick Return</th>
				<th>Over Time</th>
				<th>Status</th>
				<th>Note</th>
				<th>Manage</th>
			</tr>
		<?php
			$employee=array();
			$no=0;
			foreach($model as $row){
				$no++;
				$statQuick='';
				$statLate='';
				$reason=array();
				$ot='';
				$quick='';
				$late=Lib::Late($row->shift->shift_att_in,$row->att_in);
				if($row->att_sts_id==""){
					if($row->att_out!=''){
						$quick=Lib::QuickHome($row->shift->shift_att_out,$row->att_out);
						if($row->att_is_overtime==1){
							$ot=Lib::OverTime($row->shift->shift_att_out,$row->att_out);
						}else{
							$ot='';
						}
						if($quick=="00:00:00"){
							$statQuick='';
							$quick='';
						}else{
							if($row->att_is_possible_quick_home==1){
								$reason[]=$row->att_reason_possible_quick_home;
								$quick='';
							}else{
								$statQuick='style="background:red;color:white;"';
							}
						}
					}else{
						$ot='';
						$quick='';
					}
					
					if($late=="00:00:00"){
						$statLate='';
						$late='';
					}else{
						if($row->att_is_possible_late==1){
							$statLate='';
							$reason[]=$row->att_reason_possible_late;
							$late='';
						}else{
							$statLate='style="background:red;color:white;"';
						}
					}
				}else{
					$late='';
					$quick='';
					$ot='';
					$reason='';
				}
				$employee[]="'$row->att_emp_id'";
				echo'
				<tr>
					<td>'.$no.'</td>
					<td>'.$row->att_emp_id.'</td>
					<td>'.$row->attEmp->emp_full_name.'</td>
					<td>'.$row->shift->shift_att_in.'-'.$row->shift->shift_att_out.'</td>
					<td '.$statLate.'>'.$row->att_in.'</td>
					<td '.$statQuick.'>';
						if($row->att_sts_id==""){
							echo(($row->att_out!='')?$row->att_out:'<a href="'.Yii::app()->createUrl(Yii::app()->controller->module->id."/".Yii::app()->controller->id."/backHome",array("id"=>$row->att_id)).'" onClick="return confirm(\'End of the work?\');" class="btn btn-danger" data-toggle="tooltip" data-original-title="Click To End of Work Today"><i class="fa fa-home"></i></a>');
						}
					echo'</td>
					<td>'.$late.'</td>
					<td>'.$quick.'</td>
					<td>'.$ot.'</td>
					<td><b>'.$row->attSts->att_sts_name.'</b><br/><small>'.$row->att_note.'</small></td>
					<td>'.((!empty($reason)) ? implode($reason,"<br/>") : '').'</td>
					<td><a href="'.Yii::app()->createUrl(Yii::app()->controller->module->id."/".Yii::app()->controller->id."/update",array("id"=>$row->att_id)).'" onClick="return confirm(\'Edit This Attendance?\');" class="btn btn-primary btn-icon btn-icon-standalone" data-toggle="tooltip" data-original-title="Click To Edit Attendance Today"><i class="fa fa-edit"></i></a></td>
				</tr>

				';
			}
			$criteria=new CDbCriteria;
			$criteria->condition="emp_status='Active' AND emp_join < '".$_GET["periode"]."'";
			$criteria->addCondition("emp_id NOT IN(".(empty($employee)?"":implode(",",$employee)).")");
			$criteria->order="RIGHT(emp_id,5) ASC";
			$dataEmployee=Employee::model()->findAll($criteria);
			foreach($dataEmployee as $row){
				echo'
				<tr>
					<td colspan="11">
						'.$row->emp_id.' - '.$row->emp_full_name.'
					</td>
					<td>
						<a href="'.Yii::app()->createUrl(Yii::app()->controller->module->id."/".Yii::app()->controller->id."/createByEmployee",array("id"=>$row->emp_id,"date"=>$_GET["periode"])).'" class="btn btn-primary btn-icon btn-icon-standalone" data-toggle="tooltip" data-original-title="Click To Create Attendance Today"><i class="fa-plus"></i></a>
					</td>
				</tr>
				';
			}			
		?>
		</table>
	</div>
</div>
