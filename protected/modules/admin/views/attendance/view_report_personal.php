
<?php
$this->breadcrumbs=array(
	'Attendance'=>array('index'),
	'Employee Attendance Report'=>array('employeeReport'),
	'Attendance List'
);
$this->title=array(
	'title'=>'Employee Attendance List',
	'deskripsi'=>'For View Employee Attendance'
);
?>

<div class="fr">
	<a href="<?php echo Yii::app()->createUrl(Yii::app()->controller->module->id.'/'.Yii::app()->controller->id."/employeeReport");?>" class="btn btn-red btn-icon btn-icon-standalone">
		<i class="fa-arrow-circle-left"></i>
		<span>Return</span>
	</a>
</div>
<div style="clear:both"></div>
<div class="panel panel-default">
	<div class="panel-heading">
		<h3 class="panel-title">Data Attendance</h3>
	</div>
	<div class="panel-body">
	
		
		<style>
		.pagination li.selected a{
			background:rgb(235, 235, 236);
		}
		</style>
		
		
		<?php
		if(Yii::app()->user->hasFlash('success')){
			echo'
			<div class="alert alert-success">
				<button type="button" class="close" data-dismiss="alert">&times;</button>
				'.Yii::app()->user->getFlash('success').'
			</div>';
		}
		?>
							
		<div id="example-2_wrapper" class="dataTables_wrapper form-inline dt-bootstrap no-footer">
		
		<?php 
		$employee=Employee::model()->findbyPk($_GET['id']);
		$this->widget('booster.widgets.TbDetailView',array(
			'data'=>$employee,
			'attributes'=>array(
			'emp_id',
			'emp_full_name',
			'emp_nick_name',
			),
			)); ?>
		
		<?php 
		// put this somewhere on top
		$pageSize=Yii::app()->user->getState('pageSize',Yii::app()->params['defaultPageSize']); ?>		
		<?php		
		$this->widget('booster.widgets.TbExtendedGridView', array(
			'id'=>'attendance-grid',
			'type' => 'striped',
			'dataProvider' => $model->searchByEmployee($_GET['id']),
			'filter'=>$model,
			'summaryText'=>'Menampilkan {start}-{end} dari {count} hasil.',
			'selectableRows' => 2,
			'responsiveTable' => true,
			'enablePagination' => true,
			'pager' => array(
				'htmlOptions'=>array(
					'class'=>'pagination'
				),
				'maxButtonCount' => 5,
				'cssFile' => true,
				'header' => false,
				'firstPageLabel' => '<<',
				'prevPageLabel' => '<',
				'nextPageLabel' => '>',
				'lastPageLabel' => '>>',
			),
			
			'columns'=>array(
					array(
						"header"=>"Group",
						"value"=>'Lib::yearMonth($data->att_date,true)'
					),
					array(
						'class'=>'booster.widgets.TbButtonColumn',
						 'deleteConfirmation'=>'Anda yakin akan menghapus data?',
						'template'=>'{view}',
						'buttons'=>array
						(
							'view' => array
							(
								'label'=>'Attandance Detail',
								'url'=>'Yii::app()->createUrl(Yii::app()->controller->module->id."/".Yii::app()->controller->id."/viewPersonal",array("periode"=>$data->att_date,"id"=>$_GET[id]))',
								'icon'=>'search',
								'options'=>array(
									'class'=>'btn btn-white btn-xs',
								),
							),
						),
						'header'=>CHtml::dropDownList('pageSize',$pageSize,array(10=>10,20=>20,50=>50,100=>100,200=>200,500=>500,1000=>1000),array(
							'onchange'=>"$.fn.yiiGridView.update('attendance-grid',{ data:{pageSize: $(this).val() }})",
						)),
					),
			),
		));
		?>
				
		<form action="" method="POST" id="formDownload" target="_blank">
			<input type="hidden" name="ids" id="ids"/>
		</form>
		
		<script type="text/javascript">
			$("#info").hide();
			var gridId = "attendance-grid";
			var defaultMessage='';
			var values;
			function confirmDelete(data){
				values=data;
				if(defaultMessage!=''){
					$(".modal-body").html(defaultMessage);
					$(".modal-footer").show();
				}
				$("#modalKonfirmasi").modal('show');
			}
			$(function(){
				$("#modalKonfirmasiOk").click(function(){
					if(defaultMessage==''){
						defaultMessage=$(".modal-body").html();
					}
					$(".modal-body").html('Sedang Menghapus data...');
					$(".modal-footer").hide();
					batchActions(values);
				});
			});
			function batchActions(values){				
					$.ajax({
						type: "POST",
						url: '<?php echo Yii::app()->createUrl(Yii::app()->controller->module->id.'/'.Yii::app()->controller->id.'/batchDelete');?>',
						data: {"ids":values},
						dataType:'json',
						success: function(resp){
							if(resp.status == "success"){
							   $.fn.yiiGridView.update(gridId);
							   if(resp.data.failureCount>0){
									$("#info").html(resp.data.successCount+' Berhasil Di Hapus & '+resp.data.failureCount+' Gagal Di Hapus !<br/>Data yang gagal di hapus : '+resp.data.dataError);
							   }else{
									$("#info").html(resp.data.successCount+' Berhasil Di Hapus & '+resp.data.failureCount+' Gagal Di Hapus');
							   }
							   $("#info").fadeIn(5000);
							   $("#info").fadeOut(3000);
							}else{
							   $("#info").html(resp.msg);
							   $("#info").fadeIn(5000);
							   $("#info").fadeOut(5000);
							}
							$("#modalKonfirmasi").modal('hide');
						}
					});
			}
			function exportDataExcel(values){	
				$("#formDownload").attr('action','<?php echo Yii::app()->createUrl(Yii::app()->controller->module->id.'/'.Yii::app()->controller->id.'/exportExcel');?>');
				$("#ids").val(values.toString());
				$("#formDownload").submit();
			}
			
			function exportDataPdf(values){	
				$("#formDownload").attr('action','<?php echo Yii::app()->createUrl(Yii::app()->controller->module->id.'/'.Yii::app()->controller->id.'/exportPdf');?>');
				$("#ids").val(values.toString());
				$("#formDownload").submit();
			}
		</script>
		</div>
	</div>
</div>