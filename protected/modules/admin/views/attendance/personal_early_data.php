<link rel="stylesheet" href="<?php echo Yii::app()->theme->baseUrl; ?>/assets/css/bootstrap-select.css">
<script src="<?php echo Yii::app()->theme->baseUrl; ?>/assets/js/bootstrap-select.js"></script>
<?php
$this->breadcrumbs=array(
	'Manage Attendance'=>array('index'),
	'Employee Early Come',
);

$this->title=array(
	'title'=>'Employee Early Come',
	'deskripsi'=>'For View Employee Early Come'
);
?>

<div class="panel panel-default">
	<div class="panel-heading">
		<h3 class="panel-title pull-left">Employee Early Come</h3>
		<a href="<?php echo Yii::app()->createUrl(Yii::app()->controller->module->id.'/'.Yii::app()->controller->id);?>" class="btn btn-primary pull-right btn-xs">
			<i class="fa fa-arrow-left"></i>
			<span>Return</span>
		</a>
		<div style="clear:both"></div>
	</div>
	<div class="panel-body">	
		<?php $form=$this->beginWidget('booster.widgets.TbActiveForm',array(
	'id'=>'attendance-form',
	'enableAjaxValidation'=>true,
	'htmlOptions'=>array('enctype'=>'multipart/form-data'),
)); ?>


<?php echo $form->errorSummary($model); ?>
	
	<?php 

	if($model->isNewRecord)echo $form->datePickerGroup($model,'att_date',array('label'=>"From",'widgetOptions'=>array('options'=>array('format'=>'yyyy-mm-dd','viewFormat'=>'yyyy-mm-dd'),'htmlOptions'=>array('class'=>'span5')), 'prepend'=>'<i class="glyphicon glyphicon-calendar"></i>', 'append'=>'Click Month/Year to change Month/Year.')); 
	if($model->isNewRecord)echo $form->datePickerGroup($model,'att_date_to',array('label'=>"To",'widgetOptions'=>array('options'=>array('format'=>'yyyy-mm-dd','viewFormat'=>'yyyy-mm-dd'),'htmlOptions'=>array('class'=>'span5')), 'prepend'=>'<i class="glyphicon glyphicon-calendar"></i>', 'append'=>'Click Month/Year to change Month/Year.')); 
	?>
	

<div class="form-actions">
	<?php $this->widget('booster.widgets.TbButton', array(
			'buttonType'=>'submit',
			'context'=>'primary',
			'label'=>$model->isNewRecord ? 'View' : 'Save',
		)); 
		$this->widget('booster.widgets.TbButton', array(
			'buttonType'=>'submit',
			'context'=>'primary',
			'htmlOptions'=>array(
				"name"=>"print"
			),
			'label'=>$model->isNewRecord ? 'Print' : 'Save',
		)); 
		
		$this->widget('booster.widgets.TbButton', array(
			'buttonType'=>'submit',
			'context'=>'primary',
			'htmlOptions'=>array(
				"name"=>"excel"
			),
			'label'=>$model->isNewRecord ? 'Excel' : 'Save',
		)); 
		
		?>

		</div>

<?php $this->endWidget(); ?>


	<?php echo $this->renderPartial('_personal_early_data', array('data'=>$data,'from'=>$from,'to'=>$to,'emp'=>$emp)); ?>

	</div>
</div>
