<link rel="stylesheet" href="<?php echo Yii::app()->theme->baseUrl; ?>/assets/css/bootstrap.min.css">
<link rel="stylesheet" href="<?php echo Yii::app()->theme->baseUrl; ?>/assets/css/bootstrap-select.css">
 <script src="<?php echo Yii::app()->theme->baseUrl; ?>/assets/js/jquery-1.11.1.min.js"></script>
<script src="<?php echo Yii::app()->theme->baseUrl; ?>/assets/js/bootstrap.min.js"></script>
<script src="<?php echo Yii::app()->theme->baseUrl; ?>/assets/js/bootstrap-select.js"></script>

<?php

Yii::app()->clientScript->registerScript('validatex', '
$(document).on("change","#shift", function() {
	var attid=$(this).attr("rel");
	var shiftid=$(this).val();
	$("#loader"+attid).show();
	$.ajax({
		url: "'.Yii::app()->createAbsoluteUrl(Yii::app()->controller->module->id.'/attendance/reattendance').'",
		cache: false,
		type: "POST",
		data:"attid="+attid+"&shiftid="+shiftid,
		success: function(msg){
			var obj = JSON.parse(msg);
			$("#in"+attid).html(obj.in);
			$("#out"+attid).html(obj.out);
			$("#kehadiran"+attid).html(obj.kehadiran);
			$("#ket"+attid).html(obj.ket);
			$("#loader"+attid).hide();
			$("#LateSign"+attid).css("background","none");
			$("#QuickSign"+attid).css("background","none");
		}
	});
});

$(document).on("change","#status", function() {
	var attid=$(this).attr("rel");
	var stsid=$(this).val();
	$("#loader"+attid).show();

	$.ajax({
		url: "'.Yii::app()->createAbsoluteUrl(Yii::app()->controller->module->id.'/attendance/setStatus').'",
		cache: false,
		type: "POST",
		data:"attid="+attid+"&stsid="+stsid,
		success: function(msg){
			var obj = JSON.parse(msg);
			$("#in"+attid).html(obj.in);
			$("#out"+attid).html(obj.out);
			$("#ket"+attid).html(obj.ket);
			$("#loader"+attid).hide();
			$("#LateSign"+attid).css("background","none");
			$("#QuickSign"+attid).css("background","none");
		}
	});
});

$(document).on("click","#reload", function() {
	var attid=$(this).attr("rel");
	var shiftid=$(this).attr("data");
	$("#loader"+attid).show();
	
	$.ajax({
		url: "'.Yii::app()->createAbsoluteUrl(Yii::app()->controller->module->id.'/attendance/reattendance').'",
		cache: false,
		type: "POST",
		data:"attid="+attid+"&shiftid="+shiftid,
		success: function(msg){
			var obj = JSON.parse(msg);
			$("#in"+attid).html(obj.in);
			$("#out"+attid).html(obj.out);
			$("#kehadiran"+attid).html(obj.kehadiran);
			$("#ket"+attid).html(obj.ket);
			$("#loader"+attid).hide();
			$("#LateSign"+attid).css("background","none");
			$("#QuickSign"+attid).css("background","none");
		}
	});
	
}); 


', CClientScript::POS_END);
?>
<?php
$cs=Yii::app()->clientScript;
$cs->scriptMap=array(
    'jquery.js'=>false,
    'bootstrap.min.js' => false,
    'bootstrap-noconflict.js' => false,
    'bootbox.min.js' => false,
    'notify.min.js' => false,
);?>

<div class="panel panel-default">
	<div class="panel-heading">
		<h3 class="panel-title pull-left">Atur Jadwal Kerja <?php echo Lib::dateInd($_GET['from']); ?> s/d <?php echo Lib::dateInd($_GET['to']); ?> </h3>
		<a href="<?php echo Yii::app()->createUrl(Yii::app()->controller->module->id.'/'.Yii::app()->controller->id."/viewSummaryPersonal",array("from"=>$_GET['from'],"to"=>$_GET['to'],"id"=>$_GET['id'],"print"=>"yes"));?>" class="btn btn-primary btn-xs pull-right" target="_blank">
			<i class="fa fa-print"></i>
			<span>Print</span>
		</a>
		<div style="clear:both"></div>
	</div>
	<div class="panel-body">	
		<?php 
		$employee=Employee::model()->findbyPk($_GET['id']);
		$this->widget('booster.widgets.TbDetailView',array(
			'data'=>$employee,
			'attributes'=>array(
			'emp_id',
			'emp_full_name',
			'emp_nick_name',
			),
			)); ?>
		<?php
		if(Yii::app()->user->hasFlash('notification')){
			echo'
			<div class="alert alert-warning">
				<button type="button" class="close" data-dismiss="alert">&times;</button>
				'.Yii::app()->user->getFlash('notification').'
			</div>';
		}
		?>
		<center>
			<h4>Attendance</h4>
		</center>
		<?php $form=$this->beginWidget('booster.widgets.TbActiveForm',array(
			'id'=>'attendance-form',
		)); ?>
		<table class="table table-bordered table-hover">
			<tr>
				<th>No</th>
				<th>Date</th>
				<th>Shift</th>
				<th>In</th>
				<th>Out</th>
				<th>Finger Time</th>
				<th>Kehadiran</th>
				<th>Information</th>
				<th>Action</th>
			</tr>
			<?php	

	
	$dataX=Shift::model()->findAll(array("with"=>array("shiftGroup"),"condition"=>"shift_status='Active'","group"=>"shift_att_in,shift_att_out","order"=>"shift_att_in ASC"));
	$shift=array();
	$shiftidx=array();
	foreach($dataX as $rowX){
		$shift[$rowX->shift_id]=$rowX->shift_att_in.' - '.$rowX->shift_att_out;
		$shiftidx[]=$rowX->shift_id;
	}
	
	$status=array();
	$dataStatus=AttendanceStatus::model()->findAll();
	foreach($dataStatus as $rowY){
		$status[$rowY->att_sts_id]=$rowY->att_sts_name;
	}
	?>
		<?php
			$nLate=0;
			$nOn=0;
			$nFast=0;
			$nNo=0;
			$nAM=0;
			$nAP=0;
			$nAtt=0;
			$no=0;
			$arrayWO=array();
			$arrayRO=array();
			foreach($model as $row){
				$nmDay=Lib::getDay($row->att_date);
				$holliday=Holliday::model()->find(array("condition"=>"hd_start='$row->att_date'"));
				if($nmDay=="Sun" OR !empty($holliday)){
					if($row->att_should_attend=="No"){
						if(!empty($holliday)){
							$style='style="background:orange;color:black;"';
							$holly="".$holliday->hd_name;
						}elseif($nmDay=="Sun"){
							$style='style="background:#b0dcb0;color:black;"';
							$holly="Off";
						}else{
							$style='style="background:pink;color:black;"';
							$holly="";
						}
					}else{
						if(!empty($holliday)){
							$style='style="background:orange;color:black;"';
							$holly="".$holliday->hd_name;
						}elseif($nmDay=="Sun"){
							$style='style="background:#b0dcb0;color:black;"';
							$holly="";
						}
					}
				}else{
					if($row->att_should_attend=="No"){
						$style='style="background:pink;color:black;"';
						$holly="Off";
					}else{
						$style="";
						$holly="";
					}
				}
				$no++;
				$statQuick='';
				$statLate='';
				$reason=array();
				$ot='';
				$quick='';
				$late=Lib::Late($row->shift->shift_att_in,$row->att_in);
				
				if($row->att_sts_id==""){
					if($row->att_should_attend=="Yes"){
						$att=1;
						$nAtt++;
						if($row->att_out!='' AND $row->att_out!='00:00:00'){
							$quick=Lib::QuickHome($row->shift->shift_att_out,$row->att_out,$row->shift->shift_att_in);
							if($row->att_is_overtime==1){
								$ot=Lib::OverTime($row->shift->shift_att_out,$row->att_out);
							}else{
								$ot='';
							}
							if($quick=="00:00:00"){
								$statQuick='';
								$quick='';
							}else{
								if($row->att_is_possible_quick_home==1){
									$reason[]=$row->att_reason_possible_quick_home;
									$quick='';
								}else{
									$statQuick='style="background:red;color:white;"';
								}
								$nFast++;
							}
						}else{
							$ot='';
							$quick='';
						}
						
						if($late=="00:00:00"){
							$statLate='';
							$late='';
							$nOn++;
						}else{
							if($late>"00:00:00"){
								if($row->att_is_possible_late==1){
									$statLate='';
									$reason[]=$row->att_reason_possible_late;
									$late='';
								}else{
									$statLate='style="background:red;color:white;"';
									$nLate++;
								}
							}
						}
						
						
						if(($row->att_out=='' OR $row->att_out=='00:00:00') AND ($row->att_in=='' OR $row->att_in=='00:00:00')){
							$nNo++;
						}elseif($row->att_in=='' OR $row->att_in=='00:00:00'){
							$nAM++;
						}elseif($row->att_out=='' OR $row->att_out=='00:00:00'){
							$nAP++;
						}
					}else{
						$att=0;
						$late='';
						$quick='';
						$ot='';
						$reason='';
					}
					
				}else{
					if($row->att_should_attend=="Yes"){
						$nAtt++;
					}
					/*
					$double=Attendance::model()->find(array("condition"=>"att_att_id='$row->att_id'"));
					if(empty($double)){
						$nNo++;
					}
					*/
					$late='';
					$quick='';
					$ot='';
					$reason='';
					$work_hour='-';
					$real_hour='-';
				}
				
				echo'
				<tr '.$style.'>
					<td>'.$no.'</td>
					<td>'.Lib::dateInd($row->att_date).'<br/><b>'.$holly.'</b></td>
					<td><select name="Attendance[shift_id]['.$row->att_id.']" class="form-group selectpicker show-tick " data-live-search="true" id="shift" rel="'.$row->att_id.'">
					<option>-Choose Shift-</option>
					';
					
						if(!in_array($row->shift_id,$shiftidx)){
							$get=Shift::model()->findbyPk($row->shift_id);
							echo'<option value="'.$get->shift_id.'" selected>'.$get->shift_att_in.' - '.$get->shift_att_out.'</option>';
						}
						foreach($shift as $x=>$y){
							if($x==$row->shift_id){
								echo'<option value="'.$x.'" selected>'.$y.'</option>';
							}else{
								echo'<option value="'.$x.'">'.$y.'</option>';
							}
						}
					echo'
					</select>
					
					<span class="btn btn-primary" id="reload" rel="'.$row->att_id.'" data="'.$row->shift_id.'" title="Klik Untuk Memperbarui Jam masuk dan Pulang"><i class="fa fa-refresh"></i></span>
					<br/>
					<img src="'.Yii::app()->theme->baseUrl.'/assets/loading.gif'.'" style="display:none" id="loader'.$row->att_id.'"/>
					</td>
					<td '.$statLate.' id="LateSign'.$row->att_id.'"><span id="in'.$row->att_id.'">'.$row->att_in.'</span></td>
					<td '.$statQuick.' id="QuickSign'.$row->att_id.'"><span id="out'.$row->att_id.'">';
						if($row->att_sts_id==""){
							echo(($row->att_out!='')?$row->att_out:'<a href="'.Yii::app()->createUrl(Yii::app()->controller->module->id."/".Yii::app()->controller->id."/backHome",array("id"=>$row->att_id)).'" onClick="return confirm(\'End of the work?\');" class="btn btn-red btn-icon btn-icon-standalone" data-toggle="tooltip" data-original-title="Click To End of Work Today"><i class="fa fa-home"></i></a>');
						}
						
					echo'</span></td>
					
					<td>';
					$dF=array();
					$ex=explode("-",$row->att_date);
					$nextTo=date("Y-m-d",mktime(0,0,0,$ex[1],$ex[2]+1,$ex[0]));
					$prevTo=date("Y-m-d",mktime(0,0,0,$ex[1],$ex[2]-1,$ex[0]));
					$cekF=AttendanceTemp::model()->findAll(array("condition"=>"emp_id='".$row->attEmp->att_id."' and att_date between '$prevTo' and '$nextTo'"));
					foreach($cekF as $rF){
						$dF[]=$rF->att_date.', '.$rF->att_time;
					}
					echo implode("<br/>",$dF);
					
					echo'</td>
					<td><span id="kehadiran'.$row->att_id.'">
					<select class="form-group selectpicker show-tick" data-live-search="true" id="status" rel="'.$row->att_id.'">
					<option value="null">-Choose Status-</option>
					';
						foreach($status as $x=>$y){
							if($x==$row->att_sts_id){
								echo'<option value="'.$x.'" selected>'.$y.'</option>';
							}else{
								echo'<option value="'.$x.'">'.$y.'</option>';
							}
						}
					echo'
					</select>
					</span></td>
					<td><span id="ket'.$row->att_id.'">';
					if(!empty($row->att_att_id)){
						$double=Attendance::model()->findbyPk($row->att_att_id);
						echo "Menggantikan Shift : ".$double->attEmp->emp_full_name."<br/>".$double->shift->shift_att_in.' - '.$double->shift->shift_att_out;
					}else{
						$double=Attendance::model()->find(array("condition"=>"att_att_id='$row->att_id'"));
						if(!empty($double)){
						echo "Digantikan Oleh : ".$double->attEmp->emp_full_name."<br/>".$double->shift->shift_att_in.' - '.$double->shift->shift_att_out;
						}
					}
					
					if($row->att_is_overtime==1){
						echo "Lembur ";
					}
					
					if($row->att_sts_id!=""){
						echo $row->attSts->att_sts_name;
					}
					echo $late;
					
					
					if($row->oncall_is=="1"){
						echo"On Call Pada : ".Lib::dateInd($row->oncall_date)."<br/> Dari : ".$row->oncall_start." Sampai : ".$row->oncall_end;
					}
					echo'</span></td>
					<td><a href="'.Yii::app()->createUrl(Yii::app()->controller->module->id."/".Yii::app()->controller->id."/update",array("id"=>$row->att_id,"from"=>$_GET['from'],"to"=>$_GET['to'],"back"=>"setAttendance")).'" onClick="return confirm(\'Edit This Attendance?\');" class="btn btn-primary btn-xs" title="Click To Edit Attendance Today"><i class="fa fa-edit"></i></a>
					
					<a href="'.Yii::app()->createUrl(Yii::app()->controller->module->id."/".Yii::app()->controller->id."/doubleShift",array("id"=>$row->att_id,"from"=>$_GET['from'],"to"=>$_GET['to'],"back"=>"setAttendance")).'" onClick="return confirm(\'Manage Double Shift?\');" class="btn btn-primary btn-xs" title="Click To Manage Double Shift"><i class="fa fa-refresh"></i></a>
					
					<a href="'.Yii::app()->createUrl(Yii::app()->controller->module->id."/".Yii::app()->controller->id."/onCall",array("id"=>$row->att_id,"from"=>$_GET['from'],"to"=>$_GET['to'],"back"=>"setAttendance")).'" onClick="return confirm(\'Manage On Call?\');" class="btn btn-danger btn-xs" title="Click To Manage onCall"><i class="fa fa-arrow-up"></i></a>
					
					';
					
					echo'</td>
				
				</tr>

				';
			}
		?>
		
			
		</table>
			<button type="submit" class="btn btn-primary">Update</button>
		<?php $this->endWidget(); ?>
		<hr>
		<table class="table table-striped table-bordered">
			<tr>
				<th>On Time</th>
				<td><span id="nOn"><?php echo $nOn;?></span></td>
				<td><span id="On"><?php echo number_format(($nOn/$nAtt)*100,2);?></span> %</td>
			</tr>
			<tr>
				<th>Late</th>
				<td><span id="nLate"><?php echo $nLate;?></span></td>
				<td><span id="Late"><?php echo number_format(($nLate/$nAtt)*100,2);?></span> %</td>
			</tr>
			<tr>
				<th>Quick Home</th>
				<td><span id="nFast"><?php echo $nFast;?></span></td>
				<td><span id="Fast"><?php echo number_format(($nFast/$nAtt)*100,2);?></span> %</td>
			</tr>
			<tr>
				<th>Lupa Absen Masuk</th>
				<td><span id="nAM"><?php echo $nAM;?></span></td>
				<td><span id="AM"><?php echo number_format(($nAM/$nAtt)*100,2);?></span> %</td>
			</tr>
			<tr>
				<th>Lupa Absen Pulang</th>
				<td><span id="nAP"><?php echo $nAP;?></span></td>
				<td><span id="AP"><?php echo number_format(($nAP/$nAtt)*100,2);?></span> %</td>
			</tr>
			<tr>
				<th>Not Present</th>
				<td><span id="nNo"><?php echo $nNo;?></span></td>
				<td><span id="No"><?php echo number_format(($nNo/$nAtt)*100,2);?></span> %</td>
			</tr>
			<tr>
				<th>Total Attendance</th>
				<td><span id="Total"><?php echo $nAtt-$nNo;?> dari <?php echo $nAtt;?></span></td>
				<td><span id="nTotal"><?php echo number_format((($nAtt-$nNo)/$nAtt)*100,2);?></span> %</td>
			</tr>
		</table>
	</div>
</div>
