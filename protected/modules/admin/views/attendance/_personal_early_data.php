<div class="container">

<?php
$arrayEarly=array();
$month=array();
$name=array();
foreach($data as $row){
	$arrayEarly[$row['att_emp_id']][$row['att_date']]=$row['early'];
	$month[]=$row['att_date'];
	$name[$row['att_emp_id']]=$row['emp_full_name'];
}

$uniqMonth=array_unique($month);
?>
<center>
	<h4>Rekapitulasi Jumlah Awal Kehadiran >= 30 menit</h4>
</center>
<table class="table table-bordered table-stripped">
	<tr>
		<th>No</th>
		<th>NIK</th>
		<th>Nama</th>
		<?php
		
		foreach($uniqMonth as $row){
			echo"<th>".Lib::dateInd($row,false)."</th>";
		}
		?>
	</tr>
	<?php
	$no=0;
	foreach($arrayEarly as $index=>$row){
		$no++;
		echo'
		<tr>
			<td>'.$no.'</td>
			<td>'.$index.'</td>
			<td>'.$name[$index].'</td>';
			foreach($uniqMonth as $data){
				echo"<td>".$arrayEarly[$index][$data]."</td>";
			}
			echo'
		</tr>
		';
	}
	?>
</table>

</div>