<?php
$this->breadcrumbs=array(
	'Attendance'=>array('index'),
	'Employee Attendance Report'
);
$this->title=array(
	'title'=>'Employee Attendance Report',
	'deskripsi'=>'For View Employee Attendance'
);
?>

<div class="fr">
	<a href="<?php echo Yii::app()->createUrl(Yii::app()->controller->module->id.'/'.Yii::app()->controller->id);?>" class="btn btn-red btn-icon btn-icon-standalone">
		<i class="fa-arrow-circle-left"></i>
		<span>Return</span>
	</a>
</div>
<div style="clear:both"></div>
<div class="panel panel-default">
	<div class="panel-heading">
		<h3 class="panel-title">Data Employee</h3>
	</div>
	<div class="panel-body">
		<style>
		.pagination li.selected a{
			background:rgb(235, 235, 236);
		}
		</style>
		
		<div class="alert alert-info" id="info"></div>
		
		<?php
		if(Yii::app()->user->hasFlash('success')){
			echo'
			<div class="alert alert-success">
				<button type="button" class="close" data-dismiss="alert">&times;</button>
				'.Yii::app()->user->getFlash('success').'
			</div>';
		}
		?>
							
		<div id="example-2_wrapper" class="dataTables_wrapper form-inline dt-bootstrap no-footer">
		
		<?php 
		// put this somewhere on top
		$pageSize=Yii::app()->user->getState('pageSize',Yii::app()->params['defaultPageSize']); ?>		
		<?php		
		$this->widget('booster.widgets.TbExtendedGridView', array(
			'id'=>'employee-grid',
			'type' => 'striped',
			'dataProvider' => $model->search(),
			'filter'=>$model,
			'summaryText'=>'Menampilkan {start}-{end} dari {count} hasil.',
			'selectableRows' => 2,
			'responsiveTable' => true,
			'enablePagination' => true,
			'pager' => array(
				'htmlOptions'=>array(
					'class'=>'pagination'
				),
				'maxButtonCount' => 5,
				'cssFile' => true,
				'header' => false,
				'firstPageLabel' => '<<',
				'prevPageLabel' => '<',
				'nextPageLabel' => '>',
				'lastPageLabel' => '>>',
			),
		
			'columns'=>array(
					'emp_id',
					'emp_full_name',
					'emp_phone_number',
					'emp_email',
					array(
						'header'=>$model->getAttributeLabel('emp_type'),
						'value'=>'$data->emp_type',
						'name'=>'emp_type',
						'filter'=>array("Training"=>"Training","Contract"=>"Contract")
					),
					array(
						'header'=>$model->getAttributeLabel('emp_status'),
						'value'=>'$data->emp_status',
						'name'=>'emp_status',
						'filter'=>array("Active"=>"Active","Inactive"=>"Inactive")
					),
					array(
						'header'=>$model->getAttributeLabel('emp_dept_id'),
						'value'=>'',
						'name'=>'emp_dept_id',
						'filter'=>CHtml::listData(Departement::model()->findAll(),'dept_id','dept_name')
					),
					array(
						'class'=>'booster.widgets.TbButtonColumn',
						 'deleteConfirmation'=>'Anda yakin akan menghapus data?',
						'template'=>'{view}',
						'buttons'=>array
						(
							'view' => array
							(
								'label'=>'View',
								'icon'=>'search',
								'url'=>'Yii::app()->createUrl(Yii::app()->controller->module->id."/".Yii::app()->controller->id."/viewReport",array("id"=>$data->emp_id))',
								'options'=>array(
									'class'=>'btn btn-white btn-xs',
								),
							),
						),
						'header'=>CHtml::dropDownList('pageSize',$pageSize,array(10=>10,20=>20,50=>50,100=>100,200=>200,500=>500,1000=>1000),array(
							'onchange'=>"$.fn.yiiGridView.update('employee-grid',{ data:{pageSize: $(this).val() }})",
						)),
					),
			),
		));
		?>
		</div>
	</div>
</div>