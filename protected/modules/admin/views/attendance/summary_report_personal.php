<link rel="stylesheet" href="<?php echo Yii::app()->theme->baseUrl; ?>/assets/css/bootstrap-select.css">
<script src="<?php echo Yii::app()->theme->baseUrl; ?>/assets/js/bootstrap-select.js"></script>

<?php
Yii::app()->clientScript->registerScript('validate', '

function openSearch(){
	window.open("'.Yii::app()->createUrl(Yii::app()->controller->module->id.'/employee/list').'","","width=800,height=600");
}

function selectText(id){
	$("#Attendance_att_emp_id").val(id);
}

', CClientScript::POS_END);
?>
<?php
$this->breadcrumbs=array(
	'Manage Attendance'=>array('index'),
	'Employee Report',
);

$this->title=array(
	'title'=>'Employee Report',
	'deskripsi'=>'For View Detail Attendance'
);
?>


<div class="panel panel-default">
	<div class="panel-heading">
		<h3 class="panel-title pull-left">Summary Personal</h3>
		<a href="<?php echo Yii::app()->createUrl(Yii::app()->controller->module->id.'/'.Yii::app()->controller->id);?>" class="btn btn-primary pull-right btn-xs">
			<i class="fa fa-arrow-left"></i>
			<span>Return</span>
		</a>
		<div style="clear:both"></div>
	</div>
	<div class="panel-body">	
		<?php $form=$this->beginWidget('booster.widgets.TbActiveForm',array(
	'id'=>'attendance-form',
	'enableAjaxValidation'=>true,
	'htmlOptions'=>array('enctype'=>'multipart/form-data'),
)); ?>


<?php echo $form->errorSummary($model); ?>
	<?php	
	$data=Employee::model()->findAll(array("condition"=>"emp_status='Active'","order"=>"RIGHT(emp_id,5) ASC"));
	$employee=array();
	foreach($data as $row){
	
			$employee[$row->emp_id]=$row->emp_id.' | '.$row->emp_full_name;

	}
	if($model->isNewRecord){
	echo $form->dropDownListGroup($model,'att_emp_id', array('append'=>'<input type="button" value="Cari" onClick="openSearch()" >','widgetOptions'=>array('data'=>$employee, 'htmlOptions'=>array('class'=>'input-large selectpicker show-tick','data-live-search'=>"true",'empty'=>'Choose Employee'))));
	}
	?>
	<?php 

	if($model->isNewRecord)echo $form->datePickerGroup($model,'att_date',array('label'=>"From",'widgetOptions'=>array('options'=>array('format'=>'yyyy-mm-dd','viewFormat'=>'yyyy-mm-dd'),'htmlOptions'=>array('class'=>'span5',"value"=>"")), 'prepend'=>'<i class="glyphicon glyphicon-calendar"></i>', 'append'=>'Click Month/Year to change Month/Year.')); 
	if($model->isNewRecord)echo $form->datePickerGroup($model,'att_shift_id',array('label'=>"To",'widgetOptions'=>array('options'=>array('format'=>'yyyy-mm-dd','viewFormat'=>'yyyy-mm-dd'),'htmlOptions'=>array('class'=>'span5',"value"=>"")), 'prepend'=>'<i class="glyphicon glyphicon-calendar"></i>', 'append'=>'Click Month/Year to change Month/Year.')); 
	?>
	

<div class="form-actions">
	<?php $this->widget('booster.widgets.TbButton', array(
			'buttonType'=>'submit',
			'context'=>'primary',
			'label'=>$model->isNewRecord ? 'Generate' : 'Save',
		)); 
		
		$this->widget('booster.widgets.TbButton', array(
			'buttonType'=>'submit',
			'context'=>'primary',
			'htmlOptions'=>array("name"=>"finger"),
			'label'=>$model->isNewRecord ? 'Finger Only' : 'Save',
		)); 
		echo CHtml::button('Cancel', array(
            'class' => 'btn btn-primary',
            'onclick' => "history.go(-1)",
                )
        );
		?>

		</div>

<?php $this->endWidget(); ?>

	</div>
</div>
