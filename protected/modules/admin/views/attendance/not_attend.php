<?php
$this->breadcrumbs=array(
	'Manage Attendance'=>array('index'),
	'Not Attend Report',
);

$this->title=array(
	'title'=>'Not Attend Report',
	'deskripsi'=>'For View Employee Who No Attend'
);
?>

<div class="panel panel-default">
	<div class="panel-heading">
		<h3 class="panel-title pull-left">Not Attend Report</h3>
		<a href="<?php echo Yii::app()->createUrl(Yii::app()->controller->module->id.'/'.Yii::app()->controller->id);?>" class="btn btn-primary pull-right btn-xs">
			<i class="fa fa-arrow-left"></i>
			<span>Return</span>
		</a>
		<div style="clear:both"></div>
	</div>
	<div class="panel-body">	
		<?php $form=$this->beginWidget('booster.widgets.TbActiveForm',array(
	'id'=>'attendance-form',
	'enableAjaxValidation'=>true,
	'htmlOptions'=>array('enctype'=>'multipart/form-data'),
)); ?>


<?php echo $form->errorSummary($model); ?>
	
	<?php 

	if($model->isNewRecord)echo $form->datePickerGroup($model,'att_date',array('label'=>"From",'widgetOptions'=>array('options'=>array('format'=>'yyyy-mm-dd','viewFormat'=>'yyyy-mm-dd'),'htmlOptions'=>array('class'=>'span5')), 'prepend'=>'<i class="glyphicon glyphicon-calendar"></i>', 'append'=>'Click Month/Year to change Month/Year.')); 
	if($model->isNewRecord)echo $form->datePickerGroup($model,'att_date_to',array('label'=>"To",'widgetOptions'=>array('options'=>array('format'=>'yyyy-mm-dd','viewFormat'=>'yyyy-mm-dd'),'htmlOptions'=>array('class'=>'span5')), 'prepend'=>'<i class="glyphicon glyphicon-calendar"></i>', 'append'=>'Click Month/Year to change Month/Year.')); 
	$status=array();
	$dataStatus=AttendanceStatus::model()->findAll();
	foreach($dataStatus as $row){
		$status[$row->att_sts_id]=$row->att_sts_name;
	}
	if($model->isNewRecord)echo $form->dropDownListGroup($model,'att_sts_id', array('widgetOptions'=>array('data'=>$status, 'htmlOptions'=>array('class'=>'input-large','empty'=>'Choose Status'))));
	?>
	

<div class="form-actions">
	<?php $this->widget('booster.widgets.TbButton', array(
			'buttonType'=>'submit',
			'context'=>'primary',
			'label'=>$model->isNewRecord ? 'View' : 'Save',
		)); 
		$this->widget('booster.widgets.TbButton', array(
			'buttonType'=>'submit',
			'context'=>'primary',
			'htmlOptions'=>array(
				"name"=>"print"
			),
			'label'=>$model->isNewRecord ? 'Print' : 'Save',
		)); 
		
		?>

		</div>

<?php $this->endWidget(); ?>


	<?php echo $this->renderPartial('_not_attend', array('data'=>$data,'from'=>$from,'to'=>$to)); ?>

	</div>
</div>
