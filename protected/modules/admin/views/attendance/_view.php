<div class="view">

		<b><?php echo CHtml::encode($data->getAttributeLabel('att_id')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->att_id),array('view','id'=>$data->att_id)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('att_shift_id')); ?>:</b>
	<?php echo CHtml::encode($data->att_shift_id); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('att_emp_id')); ?>:</b>
	<?php echo CHtml::encode($data->att_emp_id); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('att_date')); ?>:</b>
	<?php echo CHtml::encode($data->att_date); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('att_in')); ?>:</b>
	<?php echo CHtml::encode($data->att_in); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('att_out')); ?>:</b>
	<?php echo CHtml::encode($data->att_out); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('att_status')); ?>:</b>
	<?php echo CHtml::encode($data->att_status); ?>
	<br />

	<?php /*
	<b><?php echo CHtml::encode($data->getAttributeLabel('att_create')); ?>:</b>
	<?php echo CHtml::encode($data->att_create); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('att_update')); ?>:</b>
	<?php echo CHtml::encode($data->att_update); ?>
	<br />

	*/ ?>

</div>