		<?php
		$getEmployee=Attendance::model()->findAll(array("condition"=>"att_date between '$_GET[from]' AND '$_GET[to]'","group"=>"att_emp_id","order"=>"RIGHT(att_emp_id,5) ASC"));
		$date=Attendance::model()->findAll(array("select"=>"att_date","condition"=>"att_date between '$_GET[from]' AND '$_GET[to]'","group"=>"att_date","order"=>"att_date asc"));
		?>
		<center>
			<h4>Attendance <br/><?php echo Lib::dateInd($_GET['from']);?>-<?php echo Lib::dateInd($_GET['to']);?></h4>
		</center>
		<table class="table table-bordered table-striped">
			<tr>
				<th rowspan="2">No</th>
				<th rowspan="2">Employee ID</th>
				<th colspan="<?php echo count($date);?>">Tanggal</th>
				<th rowspan="2">Jumlah Tidak Hadir</th>
				<th rowspan="2">Total Hadir</th>
			</tr>
			<tr>
				<?php
				foreach($date as $row){
					echo'<td>'.$row->att_date.'</td>';
				}
				?>
			</tr>
			<?php
			$no=0;
			foreach($getEmployee as $mainRow){
				$akumulasi=0;
				$no++;
				echo'<tr>';
					echo'<td>'.$no.'</td>';
					echo'<td>'.$mainRow->attEmp->emp_full_name.'</td>';
					foreach($date as $row){
						$nmDay=Lib::getDay($row->att_date);
						$holliday=Holliday::model()->find(array("condition"=>"hd_start='$row->att_date'"));
						if($nmDay=="Sun" OR !empty($holliday)){
							$style='style="background:pink;color:black;"';
							if(!empty($holliday)){
								$holly=$holliday->hd_name;
							}else{
								$holly="Minggu";
							}
							
						}else{
							$style="";
							$holly="";
						}
						$stt=Attendance::model()->find(array("condition"=>"att_emp_id='$mainRow->att_emp_id' AND att_date='$row->att_date'"));
						if($stt->att_should_attend=="Yes"){
							if($stt->att_sts_id!=""){
								$akumulasi++;
								$status=$stt->attSts->att_sts_name;
							}elseif($stt->att_in==""){
								$akumulasi++;
								$status="Tidak Absen";
							}else{
								$status="";
							}
						}else{
							$status=$holly;
						}
						echo'<td>'.$status.'</td>';
					}
					echo'<td>'.$akumulasi.'</td>';
					echo'<td>'.(25-$akumulasi).'</td>';
				echo'<tr>';
			}
			?>
		</table>
