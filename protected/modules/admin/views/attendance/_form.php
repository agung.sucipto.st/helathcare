<?php
Yii::app()->clientScript->registerScript('validate', '

function openSearchGroup(){
	window.open("'.Yii::app()->createUrl(Yii::app()->controller->module->id.'/shift/list').'","","width=800,height=600");
}

function selectTextGroup(id){
	$("#Attendance_shift_id").val(id);
}

', CClientScript::POS_END);
?>

<?php $form=$this->beginWidget('booster.widgets.TbActiveForm',array(
	'id'=>'attendance-form',
	'enableAjaxValidation'=>true,
	'htmlOptions'=>array('enctype'=>'multipart/form-data'),
)); ?>

<p class="help-block">Required Form <span class="required">*</span> cannot be blank !.</p>

<?php echo $form->errorSummary($model); ?>
	
	<?php 
	if(!$model->isNewRecord){
		$this->widget('booster.widgets.TbDetailView',array(
			'data'=>$model,
			'attributes'=>array(
					array(
						"label"=>"Name",
						"value"=>$model->attEmp->emp_full_name
					),
					array(
						"label"=>"Date",
						"value"=>Lib::dateInd($model->att_date)
					)
				),
			));
			
			echo"<hr>";
	}
	if($model->isNewRecord)echo $form->datePickerGroup($model,'att_date',array('widgetOptions'=>array('options'=>array('format'=>'yyyy-mm-dd','viewFormat'=>'yyyy-mm-dd'),'htmlOptions'=>array('class'=>'span5',"value"=>date("Y-m-d"))), 'prepend'=>'<i class="glyphicon glyphicon-calendar"></i>', 'append'=>'Click Month/Year to change Month/Year.')); ?>
	


	<?php	
	$data=Employee::model()->findAll(array("condition"=>"emp_status='Active'","order"=>"RIGHT(emp_id,5) ASC"));
	$employee=array();
	foreach($data as $row){
		$checkAttandance=Attendance::model()->count(array("condition"=>"att_emp_id='$row->emp_id' AND att_date='".date("Y-m-d")."'"));
		if($checkAttandance==0){
			$employee[$row->emp_id]=$row->emp_id.' | '.$row->emp_full_name;
		}
	}
	if($model->isNewRecord){
	echo $form->dropDownListGroup($model,'att_emp_id', array('widgetOptions'=>array('data'=>$employee, 'htmlOptions'=>array('class'=>'input-large','empty'=>'Choose Employee'))));
	}
	?>
	
	<?php	
	$data=Shift::model()->findAll();
	$shift=array();
	foreach($data as $row){
		$shift[$row->shift_id]=$row->shiftGroup->shift_group_name.' | '.$row->shiftGroup->org->org_name.' | '.$row->shift_att_in.' - '.$row->shift_att_out;
	}
	echo $form->dropDownListGroup($model,'shift_id', array('append'=>'<input type="button" value="Cari" onClick="openSearchGroup()" >','widgetOptions'=>array('data'=>$shift, 'htmlOptions'=>array('class'=>'input-large','empty'=>'Choose Shift'))));
	
	?>
	
	<?php echo $form->timePickerGroup($model,'att_in',array('widgetOptions'=>array('options'=>array('format'=>'hh:mm:ss','viewFormat'=>'hh:mm:ss','minuteStep'=>5,'showMeridian'=>false)))); ?>
	
	<?php echo $form->timePickerGroup($model,'att_out',array('widgetOptions'=>array('options'=>array('format'=>'hh:mm:ss','viewFormat'=>'hh:mm:ss','minuteStep'=>5,'showMeridian'=>false)))); ?>
	
	<?php
	if($model->isNewRecord){
		$displayNote='display:none;';
		$displayOvertimeDocs='display:none;';
	}else{		
		if($model->att_sts_id!=''){
			$displayNote='display:block;';
		}else{
			$displayNote='display:none;';
		}
		if($model->att_is_overtime=='1'){
			$displayOvertimeDocs='display:block;';
		}else{
			$displayOvertimeDocs='display:none;';
		}
		if($model->att_is_possible_late=='1'){
			$displayReasonLate='display:block;';
		}else{
			$displayReasonLate='display:none;';
		}
		if($model->att_is_possible_quick_home=='1'){
			$displayReasonQuick='display:block;';
		}else{
			$displayReasonQuick='display:none;';
		}
	}
	?>
	
	<?php	
	$data=AttendanceStatus::model()->findAll();
	$employee=array();
	foreach($data as $row){
		$employee[$row->att_sts_id]=$row->att_sts_name;
	}
	echo $form->dropDownListGroup($model,'att_sts_id', array('widgetOptions'=>array('data'=>$employee, 'htmlOptions'=>array('class'=>'input-large','empty'=>'Choose Staatus'))));
	
	?>

	<div id="att_note" style="<?php echo $displayNote;?>">
	<?php echo $form->textFieldGroup($model,'att_note',array('widgetOptions'=>array('htmlOptions'=>array('class'=>'span5')))); ?>
	</div>
	
	<?php echo $form->dropDownListGroup($model,'att_is_overtime', array('widgetOptions'=>array('data'=>array(""=>"Choose Option","1"=>"Yes","0"=>"No"), 'htmlOptions'=>array('class'=>'input-large')))); ?>

	<div id="att_overtime_docs" style="<?php echo $displayOvertimeDocs;?>">
	<?php echo $form->textFieldGroup($model,'att_overtime_docs',array('widgetOptions'=>array('htmlOptions'=>array('class'=>'span5')))); ?>
	</div>
	
	<?php echo $form->dropDownListGroup($model,'att_is_possible_late', array('widgetOptions'=>array('data'=>array(""=>"Choose Option","1"=>"Yes","0"=>"No"), 'htmlOptions'=>array('class'=>'input-large')))); ?>

	<div id="att_reason_possible_late" style="<?php echo $displayReasonLate;?>">
	<?php echo $form->textFieldGroup($model,'att_reason_possible_late',array('widgetOptions'=>array('htmlOptions'=>array('class'=>'span5')))); ?>
	</div>
	
	<?php echo $form->dropDownListGroup($model,'att_is_possible_quick_home', array('widgetOptions'=>array('data'=>array(""=>"Choose Option","1"=>"Yes","0"=>"No"), 'htmlOptions'=>array('class'=>'input-large')))); ?>

	<div id="att_reason_possible_quick_home" style="<?php echo $displayReasonQuick;?>">
	<?php echo $form->textFieldGroup($model,'att_reason_possible_quick_home',array('widgetOptions'=>array('htmlOptions'=>array('class'=>'span5')))); ?>
	</div>
	
	<?php echo $form->dropDownListGroup($model,'att_should_attend', array('widgetOptions'=>array('data'=>array(""=>"Choose Option","Yes"=>"Yes","No"=>"No"), 'htmlOptions'=>array('class'=>'input-large')))); ?>

<div class="form-actions">
	<?php $this->widget('booster.widgets.TbButton', array(
			'buttonType'=>'submit',
			'context'=>'primary',
			'label'=>$model->isNewRecord ? 'Create' : 'Save',
		)); 
		echo CHtml::button('Cancel', array(
            'class' => 'btn btn-primary',
            'onclick' => "history.go(-1)",
                )
        );
		?>

		</div>

<?php $this->endWidget(); ?>


<?php Yii::app()->clientScript->registerScript('script',"	
	$('#Attendance_att_sts_id').change(function(){
		if($(this).val()!=''){
			$('#att_note').show();
			$('#Attendance_att_note').attr('required','required');
		}else{
			$('#att_note').hide();
			$('#Attendance_att_note').val('');
		}
	});
	$('#Attendance_att_is_overtime').change(function(){
		if($(this).val()=='1'){
			$('#att_overtime_docs').show();
			$('#Attendance_att_overtime_docs').attr('required','required');
		}else{
			$('#att_overtime_docs').hide();
			$('#Attendance_att_overtime_docs').val('');
		}
	});
	$('#Attendance_att_is_possible_late').change(function(){
		if($(this).val()=='1'){
			$('#att_reason_possible_late').show();
			$('#Attendance_att_reason_possible_late').attr('required','required');
		}else{
			$('#att_reason_possible_late').hide();
			$('#Attendance_att_reason_possible_late').val('');
		}
	});
	$('#Attendance_att_is_possible_quick_home').change(function(){
		if($(this).val()=='1'){
			$('#att_reason_possible_quick_home').show();
			$('#Attendance_att_reason_possible_quick_home').attr('required','required');
		}else{
			$('#att_reason_possible_quick_home').hide();
			$('#Attendance_att_reason_possible_quick_home').val('');
		}
	});
")?>