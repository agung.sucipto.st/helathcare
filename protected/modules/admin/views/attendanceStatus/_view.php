<div class="view">
		<b><?php echo CHtml::encode($data->getAttributeLabel('att_sts_id')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->att_sts_id),array('view','id'=>$data->att_sts_id)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('att_sts_name')); ?>:</b>
	<?php echo CHtml::encode($data->att_sts_name); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('att_create')); ?>:</b>
	<?php echo CHtml::encode($data->att_create); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('att_update')); ?>:</b>
	<?php echo CHtml::encode($data->att_update); ?>
	<br />

</div>