<?php
$this->breadcrumbs=array(
	'Kelola Attendance Statuse'=>array('index'),
	'Detail Attendance Statuse',
);

$this->title=array(
	'title'=>'Detail Attendance Statuse',
	'deskripsi'=>'Untuk Melihat Detail Attendance Statuse'
);
?>

<div class="panel panel-default">
	<div class="panel-heading">
		<h3 class="panel-title pull-left">Detail</h3>
		<a href="<?php echo Yii::app()->createUrl(Yii::app()->controller->module->id.'/'.Yii::app()->controller->id);?>" class="btn btn-primary btn-xs pull-right">
			<i class="fa fa-arrow-circle-left"></i>
			<span>Kembali</span>
		</a>
		<div style="clear:both"></div>
	</div>
	<div class="panel-body">		
		<div id="example-2_wrapper" class="dataTables_wrapper form-inline dt-bootstrap no-footer">
			<?php $this->widget('booster.widgets.TbDetailView',array(
			'data'=>$model,
			'attributes'=>array(
							'att_sts_id',
				'att_sts_name',
				'att_create',
				'att_update',
			),
			)); ?>
		</div>
	</div>
</div>
