<?php

class AdminModule extends CWebModule
{
	public function init()
	{
		// this method is called when the module is being created
		// you may place code here to customize the module or the application

		// import the module-level models and components
		$this->setImport(array(
			'admin.models.*',
			'admin.components.*',
		));
		$this->setComponents(array(
            'errorHandler' => array(
                'errorAction' => Yii::app()->createUrl('admin/default/error')),
            'user' => array(
                'class' => 'CWebUser',             
                'loginUrl' => Yii::app()->createUrl('admin/default/login'),   
            )
        ));
		Yii::app()->theme = 'adminLTE';
		Yii::app()->components = array('booster' => array(
			'class' => 'booster.components.Booster',
			'responsiveCss'=>true
		));
		Yii::app()->aliases=array(
        'booster' => 'ext.booster');
		Yii::app()->import = array(
								'booster.components.*',
								'booster.behaviors.*',
								'booster.helpers.*',
								'booster.widgets.*',
							);
		Yii::app()->user->setStateKeyPrefix('_admin');
	}

	public function beforeControllerAction($controller, $action)
	{
		if(parent::beforeControllerAction($controller, $action))
		{
			$route = $controller->id . '/' . $action->id;
            $publicPages = array(
                'default/login',
                'default/error',
            );
            if (Yii::app()->user->isGuest && !in_array($route, $publicPages)){            
                Yii::app()->getModule('admin')->user->loginRequired();                
            }
            else
                return true;
		}
		else
			return false;
	}
}
