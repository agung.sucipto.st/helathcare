<?php
class EmployeeController extends Controller
{
	/**
	* @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	* using two-column layout. See 'protected/views/layouts/column2.php'.
	*/
	public $layout='//layouts/admin/main';

	/**
	* @return array action filters
	*/
	public function filters()
	{
		return array(
		'accessControl', // perform access control for CRUD operations
		);
	}

	/**
	* Specifies the access control rules.
	* This method is used by the 'accessControl' filter.
	* @return array access control rules
	*/
	public function accessRules()
	{
		return array(
		array('allow',  // allow all users to perform 'index' and 'view' actions
		'actions'=>array('index','view','list'),
		'expression'=>'$user->getAuth()',
		),
		array('allow', // allow admin user to perform 'admin' and 'delete' actions
		'actions'=>array('create','update','admin','delete','summary','BatchDelete','exportExcel','exportPdf','import','downloadTemplate'),
		'expression'=>'$user->getAuth()',
		),

		array('deny',  // deny all users
		'users'=>array('*'),
		),
		);
	}

	public function actionList()
	{	
		$titleBefore=explode(' - ',$this->pageTitle);
		$this->pageTitle=$titleBefore[0]." - Employee";
		
		$this->layout='//layouts/admin/empty';
		$model=new Employee('search');
		$model->unsetAttributes();  // clear any default values
		if(isset($_GET['Employee']))
		$model->attributes=$_GET['Employee'];
		if (isset($_GET['pageSize'])) {
			Yii::app()->user->setState('pageSize',(int)$_GET['pageSize']); 
			unset($_GET['pageSize']);
		}
		$this->render('pop_up_employee',array(
		'model'=>$model
		));
	}	
	/**
	* Displays a particular model.
	* @param integer $id the ID of the model to be displayed
	*/
	public function actionView($id)
	{
		$this->render('view',array(
		'model'=>$this->loadModel($id),
		));
	}
	
	public function actionSummary()
	{
		$this->render('summary');
	}

	/**
	* Creates a new model.
	* If creation is successful, the browser will be redirected to the 'view' page.
	*/
	public function actionCreate()
	{
		$model=new Employee;

		// Uncomment the following line if AJAX validation is needed
		$this->performAjaxValidation($model);

		if(isset($_POST['Employee']))
		{
			$model->attributes=$_POST['Employee'];
			$simpanSementara=CUploadedFile::getInstance($model,'emp_photo');
			if(!empty($simpanSementara)){
				$model->emp_photo=Lib::simbolRemoving($model->emp_full_name).'-'.date('YmdHis').'.'.$simpanSementara->getExtensionName();
				
			}
			if($model->save()){
				$dir=Yii::app()->basePath.'/../Upload/'.$model->emp_id;
				
				if(!empty($simpanSementara)){
					if (!is_dir($dir)){
						mkdir($dir);         
					}
					$simpanSementara->saveAs(Yii::app()->basePath.'/../Upload/'.$model->emp_id.'/'.$model->emp_photo.'');
					if($simpanSementara->getSize()>100000){
						$namaBaru=$name=$model->emp_photo; //name pada input type file
						$direktori=Yii::app()->basePath.'/../Upload/'.$model->emp_id.'/'; //tempat upload foto
						$dest=Yii::app()->basePath.'/../Upload/'.$model->emp_id.'/'; //tempat upload foto
						$quality=25; //konversi kualitas gambar dalam satuan %
						Lib::UploadCompress($namaBaru,$name,$direktori,$quality,$dest);
					}
				}
				$this->redirect(array('view','id'=>$model->emp_id));
			}
		}

		$this->render('create',array(
		'model'=>$model,
		));
	}

	/**
	* Updates a particular model.
	* If update is successful, the browser will be redirected to the 'view' page.
	* @param integer $id the ID of the model to be updated
	*/
	public function actionUpdate($id)
	{
		$model=$this->loadModel($id);
		$file=$model->emp_photo;
		// Uncomment the following line if AJAX validation is needed
		$this->performAjaxValidation($model);
	
		if(isset($_POST['Employee']))
		{
			$model->attributes=$_POST['Employee'];
			$simpanSementara=CUploadedFile::getInstance($model,'emp_photo');
			if(!empty($simpanSementara)){
				@unlink(Yii::app()->basePath.'/../Upload/'.$model->emp_id.'/'.$file);
				$model->emp_photo=Lib::simbolRemoving($model->emp_full_name).'-'.date('YmdHis').'.'.$simpanSementara->getExtensionName();
			}else{
				$model->emp_photo=$file;
			}
			$model->att_id=$_POST['Employee']['att_id'];
			$model->emp_last_education=$_POST['Employee']['emp_last_education'];
			$model->emp_date_of_birth=$_POST['Employee']['emp_date_of_birth'];
			if($model->save()){
				$dir=Yii::app()->basePath.'/../Upload/'.$model->emp_id;
				
				if(!empty($simpanSementara)){
					if (!is_dir($dir)){
						mkdir($dir);         
					}
					$simpanSementara->saveAs(Yii::app()->basePath.'/../Upload/'.$model->emp_id.'/'.$model->emp_photo.'');
					if($simpanSementara->getSize()>100000){
						$namaBaru=$name=$model->emp_photo; //name pada input type file
						$direktori=Yii::app()->basePath.'/../Upload/'.$model->emp_id.'/'; //tempat upload foto
						$dest=Yii::app()->basePath.'/../Upload/'.$model->emp_id.'/'; //tempat upload foto
						$quality=25; //konversi kualitas gambar dalam satuan %
						Lib::UploadCompress($namaBaru,$name,$direktori,$quality,$dest);
					}
				}
				$this->redirect(array('view','id'=>$model->emp_id));
			}
		}

		$this->render('update',array(
		'model'=>$model,
		));
	}

	/**
	* Deletes a particular model.
	* If deletion is successful, the browser will be redirected to the 'admin' page.
	* @param integer $id the ID of the model to be deleted
	*/
	public function actionDelete($id)
	{
		if(Yii::app()->request->isPostRequest)
		{
		// we only allow deletion via POST request		
		$this->loadModel($id)->delete();

		// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
		if(!isset($_GET['ajax']))
		$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
		}
		else
		throw new CHttpException(400,'Invalid request. Please do not repeat this request again.');
	}

	/**
	* Lists all models.
	*/
	public function actionAdmin()
	{
		$dataProvider=new CActiveDataProvider('Employee');
		$this->render('index',array(
		'dataProvider'=>$dataProvider,
		));
	}

	/**
	* Manages all models.
	*/
	public function actionIndex()
	{
		$model=new Employee('search');
		$model->unsetAttributes();  // clear any default values
		if(isset($_GET['Employee']))
		$model->attributes=$_GET['Employee'];
		if (isset($_GET['pageSize'])) {
			Yii::app()->user->setState('pageSize',(int)$_GET['pageSize']); 
			unset($_GET['pageSize']);
		}
		$this->render('admin',array(
		'model'=>$model,
		));
	}
	
	public function actionBatchDelete()
	{
		$request = Yii::app()->getRequest();
		if($request->getIsPostRequest()){
			if(isset($_POST['ids'])){
				$ids = $_POST['ids'];
			}
			if (empty($ids)) {
				echo CJSON::encode(array('status' => 'failure', 'msg' => 'you should at least choice one item'));
				die();
			}
			$successCount = $failureCount = 0;
			$dataError=array();
			foreach ($ids as $id) {
				$model = $this->loadModel($id);
				$temp = $this->loadModel($id);
				if($model->delete()){
					$successCount++ ;
				}else{
					$failureCount++;
					$dataError[]=$model->emp_id;
				}
			}
			echo CJSON::encode(array('status' => 'success',
				'data' => array(
					'successCount' => $successCount,
					'failureCount' => $failureCount,
					'dataError' => $dataError,
				)));
			die();
		}else{
			throw new CHttpException(400,'Invalid request. Please do not repeat this request again.');
		}
	}
	
	public function actionExportExcel()
	{
		ini_set("memory_limit","8056M");
		set_time_limit(100000);
		Yii::import('ext.phpexcel.XPHPExcel');    
		$objPHPExcel= XPHPExcel::createPHPExcel();
		$objPHPExcel->getProperties()->setCreator("File")
		->setLastModifiedBy("File")
		->setTitle("Dokumen")
		->setSubject("Dokumen")
		->setDescription("File")
		->setKeywords("File")
		->setCategory("File");
		$model=new Employee;

		$objPHPExcel->setActiveSheetIndex(0)->setCellValue('A1',"No");
		$objPHPExcel->setActiveSheetIndex(0)->setCellValue('B1',$model->getAttributeLabel('emp_id'));
		$objPHPExcel->setActiveSheetIndex(0)->setCellValue('C1',$model->getAttributeLabel('att_id'));
		$objPHPExcel->setActiveSheetIndex(0)->setCellValue('D1',$model->getAttributeLabel('org_id'));
		$objPHPExcel->setActiveSheetIndex(0)->setCellValue('E1',$model->getAttributeLabel('emp_identity_type'));
		$objPHPExcel->setActiveSheetIndex(0)->setCellValue('F1',$model->getAttributeLabel('emp_identity_number'));
		$objPHPExcel->setActiveSheetIndex(0)->setCellValue('G1',$model->getAttributeLabel('emp_full_name'));
		$objPHPExcel->setActiveSheetIndex(0)->setCellValue('H1',$model->getAttributeLabel('emp_nick_name'));
		$objPHPExcel->setActiveSheetIndex(0)->setCellValue('I1',$model->getAttributeLabel('emp_gender'));
		$objPHPExcel->setActiveSheetIndex(0)->setCellValue('J1',$model->getAttributeLabel('emp_place_of_birth'));
		$objPHPExcel->setActiveSheetIndex(0)->setCellValue('K1',$model->getAttributeLabel('emp_date_of_birth'));
		$objPHPExcel->setActiveSheetIndex(0)->setCellValue('L1',$model->getAttributeLabel('emp_rel_id'));
		$objPHPExcel->setActiveSheetIndex(0)->setCellValue('M1',$model->getAttributeLabel('emp_prov_id'));
		$objPHPExcel->setActiveSheetIndex(0)->setCellValue('N1',$model->getAttributeLabel('emp_city_id'));
		$objPHPExcel->setActiveSheetIndex(0)->setCellValue('O1',$model->getAttributeLabel('emp_domicile_address'));
		$objPHPExcel->setActiveSheetIndex(0)->setCellValue('P1',$model->getAttributeLabel('emp_address'));
		$objPHPExcel->setActiveSheetIndex(0)->setCellValue('Q1',$model->getAttributeLabel('emp_phone_number'));
		$objPHPExcel->setActiveSheetIndex(0)->setCellValue('R1',$model->getAttributeLabel('emp_email'));
		$objPHPExcel->setActiveSheetIndex(0)->setCellValue('S1',$model->getAttributeLabel('emp_type'));
		$objPHPExcel->setActiveSheetIndex(0)->setCellValue('T1',$model->getAttributeLabel('emp_status'));
		$objPHPExcel->setActiveSheetIndex(0)->setCellValue('U1',$model->getAttributeLabel('emp_photo'));
		$objPHPExcel->setActiveSheetIndex(0)->setCellValue('V1',$model->getAttributeLabel('emp_join'));
		$objPHPExcel->setActiveSheetIndex(0)->setCellValue('W1',$model->getAttributeLabel('emp_create'));
		$objPHPExcel->setActiveSheetIndex(0)->setCellValue('X1',$model->getAttributeLabel('emp_update'));
		
		$criteria=new CDbCriteria;
		$criteria->addCondition("emp_id IN (".$_POST['ids'].")");
		$data= Employee::model()->findAll($criteria);    
		$j=1;
		$no=0;
		foreach($data as $row) {
			$j++;
			$no++;
			$objPHPExcel->setActiveSheetIndex(0)->setCellValue('A'.$j, $no);
			$objPHPExcel->setActiveSheetIndex(0)->setCellValue('B'.$j, $row->emp_id);
			$objPHPExcel->setActiveSheetIndex(0)->setCellValue('C'.$j, $row->att_id);
			$objPHPExcel->setActiveSheetIndex(0)->setCellValue('D'.$j, $row->org_id);
			$objPHPExcel->setActiveSheetIndex(0)->setCellValue('E'.$j, $row->emp_identity_type);
			$objPHPExcel->setActiveSheetIndex(0)->setCellValue('F'.$j, $row->emp_identity_number);
			$objPHPExcel->setActiveSheetIndex(0)->setCellValue('G'.$j, $row->emp_full_name);
			$objPHPExcel->setActiveSheetIndex(0)->setCellValue('H'.$j, $row->emp_nick_name);
			$objPHPExcel->setActiveSheetIndex(0)->setCellValue('I'.$j, $row->emp_gender);
			$objPHPExcel->setActiveSheetIndex(0)->setCellValue('J'.$j, $row->emp_place_of_birth);
			$objPHPExcel->setActiveSheetIndex(0)->setCellValue('K'.$j, $row->emp_date_of_birth);
			$objPHPExcel->setActiveSheetIndex(0)->setCellValue('L'.$j, $row->emp_rel_id);
			$objPHPExcel->setActiveSheetIndex(0)->setCellValue('M'.$j, $row->emp_prov_id);
			$objPHPExcel->setActiveSheetIndex(0)->setCellValue('N'.$j, $row->emp_city_id);
			$objPHPExcel->setActiveSheetIndex(0)->setCellValue('O'.$j, $row->emp_domicile_address);
			$objPHPExcel->setActiveSheetIndex(0)->setCellValue('P'.$j, $row->emp_address);
			$objPHPExcel->setActiveSheetIndex(0)->setCellValue('Q'.$j, $row->emp_phone_number);
			$objPHPExcel->setActiveSheetIndex(0)->setCellValue('R'.$j, $row->emp_email);
			$objPHPExcel->setActiveSheetIndex(0)->setCellValue('S'.$j, $row->emp_type);
			$objPHPExcel->setActiveSheetIndex(0)->setCellValue('T'.$j, $row->emp_status);
			$objPHPExcel->setActiveSheetIndex(0)->setCellValue('U'.$j, $row->emp_photo);
			$objPHPExcel->setActiveSheetIndex(0)->setCellValue('V'.$j, $row->emp_join);
			$objPHPExcel->setActiveSheetIndex(0)->setCellValue('W'.$j, $row->emp_create);
			$objPHPExcel->setActiveSheetIndex(0)->setCellValue('X'.$j, $row->emp_update);
		}
		$styleArray = array(
			'font' => array(
				'bold' => true,
			),
		);
		$styleArrayData = array(
			'borders' => array(
				'allborders' => array(
					'style' => PHPExcel_Style_Border::BORDER_THIN
				),
			),
		);
		$objPHPExcel->getActiveSheet()->getStyle('A1:X24')->applyFromArray($styleArray);
		$objPHPExcel->getActiveSheet()->getStyle('A1:X'.$j)->applyFromArray($styleArrayData);
		// Rename worksheet
		$objPHPExcel->getActiveSheet()->setTitle('Default');

		// Set active sheet index to the first sheet, so Excel opens this as the first sheet
		$objPHPExcel->setActiveSheetIndex(0);
		 		 
		// Redirect output to a client�s web browser (Excel5)
		header('Content-Type: application/vnd.ms-excel');
		header('Content-Disposition: attachment;filename="Template Employee '.date('Y-m-d H:i:s').'.xls"');
		header('Cache-Control: max-age=0');
		// If you're serving to IE 9, then the following may be needed
		header('Cache-Control: max-age=1');
		 
		// If you're serving to IE over SSL, then the following may be needed
		header ('Expires: Mon, 26 Jul 1997 05:00:00 GMT'); // Date in the past
		header ('Last-Modified: '.gmdate('D, d M Y H:i:s').' GMT'); // always modified
		header ('Cache-Control: cache, must-revalidate'); // HTTP/1.1
		header ('Pragma: public'); // HTTP/1.0
		 
		$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
		$objWriter->save('php://output');
	    Yii::app()->end();
	}
	
	public function actionExportPdf()
	{
		Yii::import('ext.MPDF57.*');
		include("mpdf.php");
		ini_set("memory_limit","8056M");
		set_time_limit(100000);
		$mpdf=new mPDF('utf-8', 'A4-L', 0, null, 3,3,10,20,15,15 );
		
		$model=new Employee;
		
		$criteria=new CDbCriteria;
		$criteria->addCondition("emp_id IN (".$_POST['ids'].")");
		$data= Employee::model()->findAll($criteria); 
		
		$j = 1;
		$no=0;
		
		$html='
		<style type="text/css" media="print,screen" >
		th {
			font-family:Arial;
			color:black;
			background-color:lightgrey;
			padding:5px;
		}
		td{
			padding:5px;
		}
		thead {
			display:table-header-group;
		}
		tbody {
			display:table-row-group;
		}
		</style>
		<h2 style="text-align: center;">Employee</h2>
		<br>
		<table border="1" style="border:1px; width:100%; vertical-align: text-top;border-collapse: collapse;">
			<thead>
				<tr>
					<th>No</th>
										<th>'.$model->getAttributeLabel('emp_id').'</th>
					<th>'.$model->getAttributeLabel('att_id').'</th>
					<th>'.$model->getAttributeLabel('org_id').'</th>
					<th>'.$model->getAttributeLabel('emp_identity_type').'</th>
					<th>'.$model->getAttributeLabel('emp_identity_number').'</th>
					<th>'.$model->getAttributeLabel('emp_full_name').'</th>
					<th>'.$model->getAttributeLabel('emp_nick_name').'</th>
					<th>'.$model->getAttributeLabel('emp_gender').'</th>
					<th>'.$model->getAttributeLabel('emp_place_of_birth').'</th>
					<th>'.$model->getAttributeLabel('emp_date_of_birth').'</th>
					<th>'.$model->getAttributeLabel('emp_rel_id').'</th>
					<th>'.$model->getAttributeLabel('emp_prov_id').'</th>
					<th>'.$model->getAttributeLabel('emp_city_id').'</th>
					<th>'.$model->getAttributeLabel('emp_domicile_address').'</th>
					<th>'.$model->getAttributeLabel('emp_address').'</th>
					<th>'.$model->getAttributeLabel('emp_phone_number').'</th>
					<th>'.$model->getAttributeLabel('emp_email').'</th>
					<th>'.$model->getAttributeLabel('emp_type').'</th>
					<th>'.$model->getAttributeLabel('emp_status').'</th>
					<th>'.$model->getAttributeLabel('emp_photo').'</th>
					<th>'.$model->getAttributeLabel('emp_join').'</th>
					<th>'.$model->getAttributeLabel('emp_create').'</th>
					<th>'.$model->getAttributeLabel('emp_update').'</th>
				</tr>
			</thead>
			<tbody>';
			
			$no=0;
			foreach($data as $row){
			$no++;
			$html.='
				<tr>
					<td>'.$no.'</td>
										<td>'.$row->emp_id.'</td>
					<td>'.$row->att_id.'</td>
					<td>'.$row->org_id.'</td>
					<td>'.$row->emp_identity_type.'</td>
					<td>'.$row->emp_identity_number.'</td>
					<td>'.$row->emp_full_name.'</td>
					<td>'.$row->emp_nick_name.'</td>
					<td>'.$row->emp_gender.'</td>
					<td>'.$row->emp_place_of_birth.'</td>
					<td>'.$row->emp_date_of_birth.'</td>
					<td>'.$row->emp_rel_id.'</td>
					<td>'.$row->emp_prov_id.'</td>
					<td>'.$row->emp_city_id.'</td>
					<td>'.$row->emp_domicile_address.'</td>
					<td>'.$row->emp_address.'</td>
					<td>'.$row->emp_phone_number.'</td>
					<td>'.$row->emp_email.'</td>
					<td>'.$row->emp_type.'</td>
					<td>'.$row->emp_status.'</td>
					<td>'.$row->emp_photo.'</td>
					<td>'.$row->emp_join.'</td>
					<td>'.$row->emp_create.'</td>
					<td>'.$row->emp_update.'</td>
				</tr>';
			}	
			$html.='
			</tbody>
		</table>	
		';
		ini_set("memory_limit","256M");
		$mpdf->Bookmark('Start of the document');
		$mpdf->SetDisplayMode('fullpage','two');
		$mpdf->setFooter('Tanggal {DATE j-m-Y}||Halaman {PAGENO} dari {nbpg}') ;
		$mpdf->simpleTables = true;
		$mpdf->WriteHTML('<sheet-size="Letter" />');
		$mpdf->WriteHTML($html);
		$mpdf->Output();
	}
	
	public function actionImport()
	{
		ini_set("memory_limit","8056M");
		set_time_limit(100000);
		$model=new Employee;
		if(isset($_POST['Employee']))
		{
		$fileUpload=CUploadedFile::getInstance($model,'import');
		$nama=date('YmdHis').'_'.$fileUpload;
		$path=Yii::app()->basePath . '/../temp_excel/'.$nama.'';
		$fileUpload->saveAs($path);
			
		Yii::import('ext.phpexcelreader.JPhpExcelReader');
		$data=new JPhpExcelReader($path);
		$baris=$data->sheets[0][numRows];
		$kolom=$data->sheets[0][numCols];
		$sukses=0;
		$gagal=0;
		for($i=2;$i<=$baris; $i++){
			$model=new Employee;
						$model->emp_id= $data->sheets[0]['cells'][$i][1];
			$model->att_id= $data->sheets[0]['cells'][$i][2];
			$model->org_id= $data->sheets[0]['cells'][$i][3];
			$model->emp_identity_type= $data->sheets[0]['cells'][$i][4];
			$model->emp_identity_number= $data->sheets[0]['cells'][$i][5];
			$model->emp_full_name= $data->sheets[0]['cells'][$i][6];
			$model->emp_nick_name= $data->sheets[0]['cells'][$i][7];
			$model->emp_gender= $data->sheets[0]['cells'][$i][8];
			$model->emp_place_of_birth= $data->sheets[0]['cells'][$i][9];
			$model->emp_date_of_birth= $data->sheets[0]['cells'][$i][10];
			$model->emp_rel_id= $data->sheets[0]['cells'][$i][11];
			$model->emp_prov_id= $data->sheets[0]['cells'][$i][12];
			$model->emp_city_id= $data->sheets[0]['cells'][$i][13];
			$model->emp_domicile_address= $data->sheets[0]['cells'][$i][14];
			$model->emp_address= $data->sheets[0]['cells'][$i][15];
			$model->emp_phone_number= $data->sheets[0]['cells'][$i][16];
			$model->emp_email= $data->sheets[0]['cells'][$i][17];
			$model->emp_type= $data->sheets[0]['cells'][$i][18];
			$model->emp_status= $data->sheets[0]['cells'][$i][19];
			$model->emp_photo= $data->sheets[0]['cells'][$i][20];
			$model->emp_join= $data->sheets[0]['cells'][$i][21];
			$model->emp_create= $data->sheets[0]['cells'][$i][22];
			$model->emp_update= $data->sheets[0]['cells'][$i][23];
			$cek=Employee::model()->findbyPk($data->sheets[0]['cells'][$i][1]);
			if(count($cek)==0){
				if ($model->save()){
				$sukses++;
				}else{
				$gagal++;
				}
			} else {
				$gagal++;
			}
		}
		@unlink($path);
		 Yii::app()->user->setFlash('success', "Proses import data selesai<br/>
         Jumlah data yang sukses diimport : ".$sukses." Jumlah data yang gagal diimport : ".$gagal."
         ");
		$this->redirect(array('index'));
		}
	}
	
	public function actionDownloadTemplate(){
		Yii::import('ext.phpexcel.XPHPExcel');    
		$objPHPExcel= XPHPExcel::createPHPExcel();
		$objPHPExcel->getProperties()->setCreator("File")
		->setLastModifiedBy("File")
		->setTitle("Dokumen Rahasia")
		->setSubject("Dokumen Rahasia")
		->setDescription("File")
		->setKeywords("File")
		->setCategory("File");
		$model=new Employee;
		//loop here		
				$objPHPExcel->setActiveSheetIndex(0)->setCellValue('A1',$model->getAttributeLabel('emp_id'));
		$objPHPExcel->setActiveSheetIndex(0)->setCellValue('B1',$model->getAttributeLabel('att_id'));
		$objPHPExcel->setActiveSheetIndex(0)->setCellValue('C1',$model->getAttributeLabel('org_id'));
		$objPHPExcel->setActiveSheetIndex(0)->setCellValue('D1',$model->getAttributeLabel('emp_identity_type'));
		$objPHPExcel->setActiveSheetIndex(0)->setCellValue('E1',$model->getAttributeLabel('emp_identity_number'));
		$objPHPExcel->setActiveSheetIndex(0)->setCellValue('F1',$model->getAttributeLabel('emp_full_name'));
		$objPHPExcel->setActiveSheetIndex(0)->setCellValue('G1',$model->getAttributeLabel('emp_nick_name'));
		$objPHPExcel->setActiveSheetIndex(0)->setCellValue('H1',$model->getAttributeLabel('emp_gender'));
		$objPHPExcel->setActiveSheetIndex(0)->setCellValue('I1',$model->getAttributeLabel('emp_place_of_birth'));
		$objPHPExcel->setActiveSheetIndex(0)->setCellValue('J1',$model->getAttributeLabel('emp_date_of_birth'));
		$objPHPExcel->setActiveSheetIndex(0)->setCellValue('K1',$model->getAttributeLabel('emp_rel_id'));
		$objPHPExcel->setActiveSheetIndex(0)->setCellValue('L1',$model->getAttributeLabel('emp_prov_id'));
		$objPHPExcel->setActiveSheetIndex(0)->setCellValue('M1',$model->getAttributeLabel('emp_city_id'));
		$objPHPExcel->setActiveSheetIndex(0)->setCellValue('N1',$model->getAttributeLabel('emp_domicile_address'));
		$objPHPExcel->setActiveSheetIndex(0)->setCellValue('O1',$model->getAttributeLabel('emp_address'));
		$objPHPExcel->setActiveSheetIndex(0)->setCellValue('P1',$model->getAttributeLabel('emp_phone_number'));
		$objPHPExcel->setActiveSheetIndex(0)->setCellValue('Q1',$model->getAttributeLabel('emp_email'));
		$objPHPExcel->setActiveSheetIndex(0)->setCellValue('R1',$model->getAttributeLabel('emp_type'));
		$objPHPExcel->setActiveSheetIndex(0)->setCellValue('S1',$model->getAttributeLabel('emp_status'));
		$objPHPExcel->setActiveSheetIndex(0)->setCellValue('T1',$model->getAttributeLabel('emp_photo'));
		$objPHPExcel->setActiveSheetIndex(0)->setCellValue('U1',$model->getAttributeLabel('emp_join'));
		$objPHPExcel->setActiveSheetIndex(0)->setCellValue('V1',$model->getAttributeLabel('emp_create'));
		$objPHPExcel->setActiveSheetIndex(0)->setCellValue('W1',$model->getAttributeLabel('emp_update'));
		
		$data= Employee::model()->findAll(array('limit'=>10));    
		$j = 1;
		$no=0;
		foreach($data as $row) {
			$j++;
			$no++;
						$objPHPExcel->setActiveSheetIndex(0)->setCellValue('A'.$j, $row->emp_id);
			$objPHPExcel->setActiveSheetIndex(0)->setCellValue('B'.$j, $row->att_id);
			$objPHPExcel->setActiveSheetIndex(0)->setCellValue('C'.$j, $row->org_id);
			$objPHPExcel->setActiveSheetIndex(0)->setCellValue('D'.$j, $row->emp_identity_type);
			$objPHPExcel->setActiveSheetIndex(0)->setCellValue('E'.$j, $row->emp_identity_number);
			$objPHPExcel->setActiveSheetIndex(0)->setCellValue('F'.$j, $row->emp_full_name);
			$objPHPExcel->setActiveSheetIndex(0)->setCellValue('G'.$j, $row->emp_nick_name);
			$objPHPExcel->setActiveSheetIndex(0)->setCellValue('H'.$j, $row->emp_gender);
			$objPHPExcel->setActiveSheetIndex(0)->setCellValue('I'.$j, $row->emp_place_of_birth);
			$objPHPExcel->setActiveSheetIndex(0)->setCellValue('J'.$j, $row->emp_date_of_birth);
			$objPHPExcel->setActiveSheetIndex(0)->setCellValue('K'.$j, $row->emp_rel_id);
			$objPHPExcel->setActiveSheetIndex(0)->setCellValue('L'.$j, $row->emp_prov_id);
			$objPHPExcel->setActiveSheetIndex(0)->setCellValue('M'.$j, $row->emp_city_id);
			$objPHPExcel->setActiveSheetIndex(0)->setCellValue('N'.$j, $row->emp_domicile_address);
			$objPHPExcel->setActiveSheetIndex(0)->setCellValue('O'.$j, $row->emp_address);
			$objPHPExcel->setActiveSheetIndex(0)->setCellValue('P'.$j, $row->emp_phone_number);
			$objPHPExcel->setActiveSheetIndex(0)->setCellValue('Q'.$j, $row->emp_email);
			$objPHPExcel->setActiveSheetIndex(0)->setCellValue('R'.$j, $row->emp_type);
			$objPHPExcel->setActiveSheetIndex(0)->setCellValue('S'.$j, $row->emp_status);
			$objPHPExcel->setActiveSheetIndex(0)->setCellValue('T'.$j, $row->emp_photo);
			$objPHPExcel->setActiveSheetIndex(0)->setCellValue('U'.$j, $row->emp_join);
			$objPHPExcel->setActiveSheetIndex(0)->setCellValue('V'.$j, $row->emp_create);
			$objPHPExcel->setActiveSheetIndex(0)->setCellValue('W'.$j, $row->emp_update);
		}
		$styleArray = array(
			'font' => array(
				'bold' => true,
			),
		);
		$styleArrayData = array(
			'borders' => array(
				'allborders' => array(
					'style' => PHPExcel_Style_Border::BORDER_THIN
				),
			),
		);
		$objPHPExcel->getActiveSheet()->getStyle('A1:W23')->applyFromArray($styleArray);
		$objPHPExcel->getActiveSheet()->getStyle('A1:W'.$j)->applyFromArray($styleArrayData);
		// Rename worksheet
		$objPHPExcel->getActiveSheet()->setTitle('Default');

		// Set active sheet index to the first sheet, so Excel opens this as the first sheet
		$objPHPExcel->setActiveSheetIndex(0);
		 		 
		// Redirect output to a client�s web browser (Excel5)
		header('Content-Type: application/vnd.ms-excel');
		header('Content-Disposition: attachment;filename="Template Employee '.date('Y-m-d H:i:s').'.xls"');
		header('Cache-Control: max-age=0');
		// If you're serving to IE 9, then the following may be needed
		header('Cache-Control: max-age=1');
		 
		// If you're serving to IE over SSL, then the following may be needed
		header ('Expires: Mon, 26 Jul 1997 05:00:00 GMT'); // Date in the past
		header ('Last-Modified: '.gmdate('D, d M Y H:i:s').' GMT'); // always modified
		header ('Cache-Control: cache, must-revalidate'); // HTTP/1.1
		header ('Pragma: public'); // HTTP/1.0
		 
		$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
		$objWriter->save('php://output');
	    Yii::app()->end();
	}
	

	public function loadModel($id)
	{
		$model=Employee::model()->findByPk($id);
		if($model===null)
		throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}

	/**
	* Performs the AJAX validation.
	* @param CModel the model to be validated
	*/
	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='employee-form')
		{
		echo CActiveForm::validate($model);
		Yii::app()->end();
		}
	}
}
