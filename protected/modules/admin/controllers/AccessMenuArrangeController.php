<?php
class AccessMenuArrangeController extends Controller
{
	/**
	* @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	* using two-column layout. See 'protected/views/layouts/column2.php'.
	*/
	public $layout='//layouts/admin/main';

	/**
	* @return array action filters
	*/
	public function filters()
	{
		return array(
		'accessControl', // perform access control for CRUD operations
		);
	}

	/**
	* Specifies the access control rules.
	* This method is used by the 'accessControl' filter.
	* @return array access control rules
	*/
	public function accessRules()
	{
		return array(
		array('allow',  // allow all users to perform 'index' and 'view' actions
		'actions'=>array('index','view'),
		'expression'=>'$user->getAuth()',
		),
		array('allow', // allow admin user to perform 'admin' and 'delete' actions
		'actions'=>array('create','update','admin','delete','BatchDelete','exportExcel','exportPdf','import','downloadTemplate'),
		'expression'=>'$user->getAuth()',
		),

		array('deny',  // deny all users
		'users'=>array('*'),
		),
		);
	}

	/**
	* Displays a particular model.
	* @param integer $id the ID of the model to be displayed
	*/
	public function actionView($id)
	{
		$this->render('view',array(
		'model'=>$this->loadModel($id),
		));
	}

	/**
	* Creates a new model.
	* If creation is successful, the browser will be redirected to the 'view' page.
	*/
	public function actionCreate()
	{
		$model=new AccessMenuArrange;

		// Uncomment the following line if AJAX validation is needed
		$this->performAjaxValidation($model);

		if(isset($_POST['AccessMenuArrange']))
		{
			$model->attributes=$_POST['AccessMenuArrange'];
			$cek=AccessMenuArrange::model()->find(array("condition"=>"menu_arrange_parent='0'","order"=>"menu_arrange_order desc"));
			$model->menu_arrange_parent=0;
			$model->menu_arrange_order=$cek->menu_arrange_order+1;
			$model->create_at=date("Y-m-d H:i:s");
			$model->modified_at=date("Y-m-d H:i:s");
			if($model->save()){
				$this->redirect(array('index'));
			}
		}

		$this->render('create',array(
		'model'=>$model,
		));
	}

	/**
	* Updates a particular model.
	* If update is successful, the browser will be redirected to the 'view' page.
	* @param integer $id the ID of the model to be updated
	*/
	public function actionUpdate($id)
	{
		$model=$this->loadModel($id);

		// Uncomment the following line if AJAX validation is needed
		$this->performAjaxValidation($model);
	
		if(isset($_POST['AccessMenuArrange']))
		{
			$model->attributes=$_POST['AccessMenuArrange'];
			if($model->save()){
				$this->redirect(array('view','id'=>$model->menu_arrange_id));
			}
		}

		$this->render('update',array(
		'model'=>$model,
		));
	}

	/**
	* Deletes a particular model.
	* If deletion is successful, the browser will be redirected to the 'admin' page.
	* @param integer $id the ID of the model to be deleted
	*/
	public function actionDelete($id)
	{
		if(Yii::app()->request->isPostRequest)
		{
		// we only allow deletion via POST request		
		$this->loadModel($id)->delete();

		// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
		if(!isset($_GET['ajax']))
		$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
		}
		else
		throw new CHttpException(400,'Invalid request. Please do not repeat this request again.');
	}

	/**
	* Lists all models.
	*/
	public function actionAdmin()
	{
		$dataProvider=new CActiveDataProvider('AccessMenuArrange');
		$this->render('index',array(
		'dataProvider'=>$dataProvider,
		));
	}

	/**
	* Manages all models.
	*/
	public function actionIndex()
	{
		if(isset($_POST['menu'])){
			if(!empty($_POST['menu'])){
				foreach($_POST['menu'] as $row){
					$model= new AccessMenuArrange;
					$cek=AccessMenuArrange::model()->find(array("condition"=>"menu_arrange_parent='0'","order"=>"menu_arrange_order desc"));
					$model->access_id=$row;
					$model->menu_arrange_parent=0;
					$model->menu_arrange_order=$cek->menu_arrange_order+1;
					$model->create_at=date("Y-m-d H:i:s");
					$model->modified_at=date("Y-m-d H:i:s");
					$model->save();
				}
			}
			
		}
		
		if(isset($_POST['delete']))
		{
			$model=$this->loadModel($_POST['id']);
			$anak=AccessMenuArrange::model()->findAll(array('condition'=>"menu_arrange_parent='$model->menu_id'"));
			if(count($anak)>0){
				foreach($anak as $row){
					$subanak=AccessMenuArrange::model()->findAll(array('condition'=>"menu_arrange_parent='$row->menu_arrange_id'"));
					if(count($subanak)>0){
						foreach($subanak as $sub){
							$nsubanak=AccessMenuArrange::model()->findAll(array('condition'=>"menu_arrange_parent='$sub->menu_arrange_id'"));
							if(count($nsubanak)>0){
								foreach($nsubanak as $nsub){
									$this->loadModel($nsub->menu_arrange_id)->delete();
								}
							}
							$this->loadModel($sub->menu_arrange_id)->delete();
						}
					}
					$this->loadModel($row->menu_arrange_id)->delete();
				}
				$this->loadModel($_POST['id'])->delete();
				Yii::app()->user->setFlash('berhasil', "Proses Sukses, Data Menu & Sub Menu Berhasil Di Hapus !!!");
			}else{
				$this->loadModel($_POST['id'])->delete();
				Yii::app()->user->setFlash('berhasil', "Proses Sukses, Data Menu Berhasil Di Hapus !!!");
			}
		}
		
		if(isset($_POST['AccessMenuArrange']['save'])){
			$data=json_decode($_POST['AccessMenuArrange']['save']);
			$order=0;
			foreach($data as $menu){
				$order++;
				if(!isset($menu->children)){
					$update=AccessMenuArrange::model()->findbyPk($menu->id);
					$update->menu_arrange_order=$order;
					$update->menu_arrange_parent=0;
					$update->save();
				}elseif(isset($menu->children)){
					$update=AccessMenuArrange::model()->findbyPk($menu->id);
					$update->menu_arrange_order=$order;
					$update->menu_arrange_parent=0;
					$update->save();
					$ordersub=0;
					foreach($menu->children as $submenu){
						if(isset($submenu->children)){
							$ordersub++;
							$updatesub=AccessMenuArrange::model()->findbyPk($submenu->id);
							$updatesub->menu_arrange_order=$ordersub;
							$updatesub->menu_arrange_parent=$menu->id;
							$updatesub->save();
							$ordersubT=0;
							foreach($submenu->children as $ssubmenu){
								$ordersubT++;
								$updatesubT=AccessMenuArrange::model()->findbyPk($ssubmenu->id);
								$updatesubT->menu_arrange_order=$ordersubT;
								$updatesubT->menu_arrange_parent=$submenu->id;
								$updatesubT->save();
							}
						}elseif(!isset($submenu->children)){
							$ordersub++;
							$updatesub=AccessMenuArrange::model()->findbyPk($submenu->id);
							$updatesub->menu_arrange_order=$ordersub;
							$updatesub->menu_arrange_parent=$menu->id;
							$updatesub->save();
						}
					}
				}
			}
			Yii::app()->user->setFlash('berhasil', "Proses Sukses, Data Berhasil Di Perbarui !!!");
		}

		$this->render('admin');
	}
	
	public function actionBatchDelete()
	{
		$request = Yii::app()->getRequest();
		if($request->getIsPostRequest()){
			if(isset($_POST['ids'])){
				$ids = $_POST['ids'];
			}
			if (empty($ids)) {
				echo CJSON::encode(array('status' => 'failure', 'msg' => 'you should at least choice one item'));
				die();
			}
			$successCount = $failureCount = 0;
			$dataError=array();
			foreach ($ids as $id) {
				$model = $this->loadModel($id);
				$temp = $this->loadModel($id);
				if($model->delete()){
					$successCount++ ;
				}else{
					$failureCount++;
					$dataError[]=$model->menu_arrange_id;
				}
			}
			echo CJSON::encode(array('status' => 'success',
				'data' => array(
					'successCount' => $successCount,
					'failureCount' => $failureCount,
					'dataError' => $dataError,
				)));
			die();
		}else{
			throw new CHttpException(400,'Invalid request. Please do not repeat this request again.');
		}
	}
	
	public function actionExportExcel()
	{
		ini_set("memory_limit","8056M");
		set_time_limit(100000);
		Yii::import('ext.phpexcel.XPHPExcel');    
		$objPHPExcel= XPHPExcel::createPHPExcel();
		$objPHPExcel->getProperties()->setCreator("File")
		->setLastModifiedBy("File")
		->setTitle("Dokumen")
		->setSubject("Dokumen")
		->setDescription("File")
		->setKeywords("File")
		->setCategory("File");
		$model=new AccessMenuArrange;

		$objPHPExcel->setActiveSheetIndex(0)->setCellValue('A1',"No");
		$objPHPExcel->setActiveSheetIndex(0)->setCellValue('B1',$model->getAttributeLabel('menu_arrange_id'));
		$objPHPExcel->setActiveSheetIndex(0)->setCellValue('C1',$model->getAttributeLabel('menu_arrange_name'));
		$objPHPExcel->setActiveSheetIndex(0)->setCellValue('D1',$model->getAttributeLabel('access_id'));
		$objPHPExcel->setActiveSheetIndex(0)->setCellValue('E1',$model->getAttributeLabel('menu_arrange_parent'));
		$objPHPExcel->setActiveSheetIndex(0)->setCellValue('F1',$model->getAttributeLabel('create_at'));
		$objPHPExcel->setActiveSheetIndex(0)->setCellValue('G1',$model->getAttributeLabel('modified_at'));
		
		$criteria=new CDbCriteria;
		$criteria->addCondition("menu_arrange_id IN (".$_POST['ids'].")");
		$data= AccessMenuArrange::model()->findAll($criteria);    
		$j=1;
		$no=0;
		foreach($data as $row) {
			$j++;
			$no++;
			$objPHPExcel->setActiveSheetIndex(0)->setCellValue('A'.$j, $no);
			$objPHPExcel->setActiveSheetIndex(0)->setCellValue('B'.$j, $row->menu_arrange_id);
			$objPHPExcel->setActiveSheetIndex(0)->setCellValue('C'.$j, $row->menu_arrange_name);
			$objPHPExcel->setActiveSheetIndex(0)->setCellValue('D'.$j, $row->access_id);
			$objPHPExcel->setActiveSheetIndex(0)->setCellValue('E'.$j, $row->menu_arrange_parent);
			$objPHPExcel->setActiveSheetIndex(0)->setCellValue('F'.$j, $row->create_at);
			$objPHPExcel->setActiveSheetIndex(0)->setCellValue('G'.$j, $row->modified_at);
		}
		$styleArray = array(
			'font' => array(
				'bold' => true,
			),
		);
		$styleArrayData = array(
			'borders' => array(
				'allborders' => array(
					'style' => PHPExcel_Style_Border::BORDER_THIN
				),
			),
		);
		$objPHPExcel->getActiveSheet()->getStyle('A1:G7')->applyFromArray($styleArray);
		$objPHPExcel->getActiveSheet()->getStyle('A1:G'.$j)->applyFromArray($styleArrayData);
		// Rename worksheet
		$objPHPExcel->getActiveSheet()->setTitle('Default');

		// Set active sheet index to the first sheet, so Excel opens this as the first sheet
		$objPHPExcel->setActiveSheetIndex(0);
		 		 
		// Redirect output to a client�s web browser (Excel5)
		header('Content-Type: application/vnd.ms-excel');
		header('Content-Disposition: attachment;filename="Template Access Menu Arrange '.date('Y-m-d H:i:s').'.xls"');
		header('Cache-Control: max-age=0');
		// If you're serving to IE 9, then the following may be needed
		header('Cache-Control: max-age=1');
		 
		// If you're serving to IE over SSL, then the following may be needed
		header ('Expires: Mon, 26 Jul 1997 05:00:00 GMT'); // Date in the past
		header ('Last-Modified: '.gmdate('D, d M Y H:i:s').' GMT'); // always modified
		header ('Cache-Control: cache, must-revalidate'); // HTTP/1.1
		header ('Pragma: public'); // HTTP/1.0
		 
		$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
		$objWriter->save('php://output');
	    Yii::app()->end();
	}
	
	public function actionExportPdf()
	{
		Yii::import('ext.MPDF57.*');
		include("mpdf.php");
		ini_set("memory_limit","8056M");
		set_time_limit(100000);
		$mpdf=new mPDF('utf-8', 'A4-L', 0, null, 3,3,10,20,15,15 );
		
		$model=new AccessMenuArrange;
		
		$criteria=new CDbCriteria;
		$criteria->addCondition("menu_arrange_id IN (".$_POST['ids'].")");
		$data= AccessMenuArrange::model()->findAll($criteria); 
		
		$j = 1;
		$no=0;
		
		$html='
		<style type="text/css" media="print,screen" >
		th {
			font-family:Arial;
			color:black;
			background-color:lightgrey;
			padding:5px;
		}
		td{
			padding:5px;
		}
		thead {
			display:table-header-group;
		}
		tbody {
			display:table-row-group;
		}
		</style>
		<h2 style="text-align: center;">Access Menu Arrange</h2>
		<br>
		<table border="1" style="border:1px; width:100%; vertical-align: text-top;border-collapse: collapse;">
			<thead>
				<tr>
					<th>No</th>
										<th>'.$model->getAttributeLabel('menu_arrange_id').'</th>
					<th>'.$model->getAttributeLabel('menu_arrange_name').'</th>
					<th>'.$model->getAttributeLabel('access_id').'</th>
					<th>'.$model->getAttributeLabel('menu_arrange_parent').'</th>
					<th>'.$model->getAttributeLabel('create_at').'</th>
					<th>'.$model->getAttributeLabel('modified_at').'</th>
				</tr>
			</thead>
			<tbody>';
			
			$no=0;
			foreach($data as $row){
			$no++;
			$html.='
				<tr>
					<td>'.$no.'</td>
										<td>'.$row->menu_arrange_id.'</td>
					<td>'.$row->menu_arrange_name.'</td>
					<td>'.$row->access_id.'</td>
					<td>'.$row->menu_arrange_parent.'</td>
					<td>'.$row->create_at.'</td>
					<td>'.$row->modified_at.'</td>
				</tr>';
			}	
			$html.='
			</tbody>
		</table>	
		';
		ini_set("memory_limit","256M");
		$mpdf->Bookmark('Start of the document');
		$mpdf->SetDisplayMode('fullpage','two');
		$mpdf->setFooter('Tanggal {DATE j-m-Y}||Halaman {PAGENO} dari {nbpg}') ;
		$mpdf->simpleTables = true;
		$mpdf->WriteHTML('<sheet-size="Letter" />');
		$mpdf->WriteHTML($html);
		$mpdf->Output();
	}
	
	public function actionImport()
	{
		ini_set("memory_limit","8056M");
		set_time_limit(100000);
		$model=new AccessMenuArrange;
		if(isset($_POST['AccessMenuArrange']))
		{
		$fileUpload=CUploadedFile::getInstance($model,'import');
		$nama=date('YmdHis').'_'.$fileUpload;
		$path=Yii::app()->basePath . '/../temp_excel/'.$nama.'';
		$fileUpload->saveAs($path);
			
		Yii::import('ext.phpexcelreader.JPhpExcelReader');
		$data=new JPhpExcelReader($path);
		$baris=$data->sheets[0][numRows];
		$kolom=$data->sheets[0][numCols];
		$sukses=0;
		$gagal=0;
		for($i=2;$i<=$baris; $i++){
			$model=new AccessMenuArrange;
						$model->menu_arrange_id= $data->sheets[0]['cells'][$i][1];
			$model->menu_arrange_name= $data->sheets[0]['cells'][$i][2];
			$model->access_id= $data->sheets[0]['cells'][$i][3];
			$model->menu_arrange_parent= $data->sheets[0]['cells'][$i][4];
			$cek=AccessMenuArrange::model()->findbyPk($data->sheets[0]['cells'][$i][1]);
			if(count($cek)==0){
				if ($model->save()){
				$sukses++;
				}else{
				$gagal++;
				}
			} else {
				$gagal++;
			}
		}
		@unlink($path);
		 Yii::app()->user->setFlash('success', "Proses import data selesai<br/>
         Jumlah data yang sukses diimport : ".$sukses." Jumlah data yang gagal diimport : ".$gagal."
         ");
		$this->redirect(array('index'));
		}
	}
	
	public function actionDownloadTemplate(){
		Yii::import('ext.phpexcel.XPHPExcel');    
		$objPHPExcel= XPHPExcel::createPHPExcel();
		$objPHPExcel->getProperties()->setCreator("File")
		->setLastModifiedBy("File")
		->setTitle("Dokumen Rahasia")
		->setSubject("Dokumen Rahasia")
		->setDescription("File")
		->setKeywords("File")
		->setCategory("File");
		$model=new AccessMenuArrange;
		//loop here		
				$objPHPExcel->setActiveSheetIndex(0)->setCellValue('A1',$model->getAttributeLabel('menu_arrange_id'));
		$objPHPExcel->setActiveSheetIndex(0)->setCellValue('B1',$model->getAttributeLabel('menu_arrange_name'));
		$objPHPExcel->setActiveSheetIndex(0)->setCellValue('C1',$model->getAttributeLabel('access_id'));
		$objPHPExcel->setActiveSheetIndex(0)->setCellValue('D1',$model->getAttributeLabel('menu_arrange_parent'));
		
		$data= AccessMenuArrange::model()->findAll(array('limit'=>10));    
		$j = 1;
		$no=0;
		foreach($data as $row) {
			$j++;
			$no++;
						$objPHPExcel->setActiveSheetIndex(0)->setCellValue('A'.$j, $row->menu_arrange_id);
			$objPHPExcel->setActiveSheetIndex(0)->setCellValue('B'.$j, $row->menu_arrange_name);
			$objPHPExcel->setActiveSheetIndex(0)->setCellValue('C'.$j, $row->access_id);
			$objPHPExcel->setActiveSheetIndex(0)->setCellValue('D'.$j, $row->menu_arrange_parent);
		}
		$styleArray = array(
			'font' => array(
				'bold' => true,
			),
		);
		$styleArrayData = array(
			'borders' => array(
				'allborders' => array(
					'style' => PHPExcel_Style_Border::BORDER_THIN
				),
			),
		);
		$objPHPExcel->getActiveSheet()->getStyle('A1:D4')->applyFromArray($styleArray);
		$objPHPExcel->getActiveSheet()->getStyle('A1:D'.$j)->applyFromArray($styleArrayData);
		// Rename worksheet
		$objPHPExcel->getActiveSheet()->setTitle('Default');

		// Set active sheet index to the first sheet, so Excel opens this as the first sheet
		$objPHPExcel->setActiveSheetIndex(0);
		 		 
		// Redirect output to a client�s web browser (Excel5)
		header('Content-Type: application/vnd.ms-excel');
		header('Content-Disposition: attachment;filename="Template Access Menu Arrange '.date('Y-m-d H:i:s').'.xls"');
		header('Cache-Control: max-age=0');
		// If you're serving to IE 9, then the following may be needed
		header('Cache-Control: max-age=1');
		 
		// If you're serving to IE over SSL, then the following may be needed
		header ('Expires: Mon, 26 Jul 1997 05:00:00 GMT'); // Date in the past
		header ('Last-Modified: '.gmdate('D, d M Y H:i:s').' GMT'); // always modified
		header ('Cache-Control: cache, must-revalidate'); // HTTP/1.1
		header ('Pragma: public'); // HTTP/1.0
		 
		$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
		$objWriter->save('php://output');
	    Yii::app()->end();
	}
	

	public function loadModel($id)
	{
		$model=AccessMenuArrange::model()->findByPk($id);
		if($model===null)
		throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}

	/**
	* Performs the AJAX validation.
	* @param CModel the model to be validated
	*/
	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='access-menu-arrange-form')
		{
		echo CActiveForm::validate($model);
		Yii::app()->end();
		}
	}
}
