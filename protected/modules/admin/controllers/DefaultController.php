<?php

class DefaultController extends Controller
{
	public $layout='//layouts/admin/main';
	public function actionIndex()
	{
		/*
		$patient=Patient2::model()->findAll();
		foreach($patient as $row){
			$cek=Patient::model()->findByPk($row->p_id);
			if(empty($cek)){
				$new=new Patient;
				$new->p_id=$row->p_id;
				$new->p_title=$row->p_title;
				$new->p_name=$row->p_name;
				$new->p_gender=$row->p_gender;
				$new->p_date_of_birth=$row->p_date_of_birth;
				$new->p_address=$row->p_address;
				$new->modified_at=$row->modified_at;
				$new->create_at=$row->create_at;
				$new->save();
			}
		}
		
		
		$reg=Registration2::model()->findAll();
		foreach($reg as $row){
			$cek=Registration::model()->findByPk($row->reg_id);
			if(empty($cek)){
				$new=new Registration;
				$new->reg_id=$row->reg_id;
				$new->p_id=$row->p_id;
				$new->reg_no=$row->reg_no;
				$new->reg_guarator=$row->reg_guarator;
				$new->reg_date_in=$row->reg_date_in;
				$new->reg_type=$row->reg_type;
				$new->reg_dept=$row->reg_dept;
				$new->modified_at=$row->modified_at;
				$new->create_at=$row->create_at;
				$new->save();
			}
		}
		/*
		$d=array("Sangat Puas","Puas","Cukup","Kurang Puas");
		$q=QuestionQuiz::model()->findAll();
		foreach($q as $r){
			foreach($d as $z){
			$n=new QuestionOption;
			$n->qq_id=$r->qq_id;
			$n->qo_name=$z;
			$n->modified_at=date("Y-m-d H:i:s");
			$n->modified_at=date("Y-m-d H:i:s");
			$n->save();
			}
		}
		*/
		$this->render('index');
	}
	
	public function actionLogin()
	{
		$model=new LoginForm;
		$this->layout='//layouts/admin/login';
		// if it is ajax validation request
		if(isset($_POST['ajax']) && $_POST['ajax']==='login-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}

		// collect user input data
		if(isset($_POST['LoginForm']))
		{
			$model->attributes=$_POST['LoginForm'];
			// validate user input and redirect to the previous page if valid
			if($model->validate() && $model->login()){
				$update=User::model()->findbyPk(Yii::app()->user->id);
				$now=date('Y-m-d H:i:s');
				$update->last_login=$now;
				$update->save();
				$this->redirect(array('default/index'));
			}
		}
		// display the login form
		$this->render('login',array('model'=>$model));
	}
	
	public function actionLogout() {
        Yii::app()->user->logout(false);
       $this->redirect(array('default/index'));
    }
}