<?php
class EmployeeGroupController extends Controller
{
	/**
	* @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	* using two-column layout. See 'protected/views/layouts/column2.php'.
	*/
	public $layout='//layouts/admin/main';

	/**
	* @return array action filters
	*/
	public function filters()
	{
		return array(
		'accessControl', // perform access control for CRUD operations
		);
	}

	/**
	* Specifies the access control rules.
	* This method is used by the 'accessControl' filter.
	* @return array access control rules
	*/
	public function accessRules()
	{
		return array(
		array('allow',  // allow all users to perform 'index' and 'view' actions
		'actions'=>array('index','view'),
		'expression'=>'$user->getAuth()',
		),
		array('allow', // allow admin user to perform 'admin' and 'delete' actions
		'actions'=>array('create','update','admin','delete','BatchDelete','exportExcel','exportPdf','import','downloadTemplate','unit','viewGrafik'),
		'users'=>array('*'),
		),

		array('deny',  // deny all users
		'users'=>array('*'),
		),
		);
	}

	/**
	* Displays a particular model.
	* @param integer $id the ID of the model to be displayed
	*/
	
	
	public function actionViewGrafik($org_id)
	{
		$form=new EmployeeGroup;
		$list=array();
		if(isset($_POST['EmployeeGroup'])){
			$form->Attributes=$_POST['EmployeeGroup'];
			foreach($_POST['EmployeeGroup']['emp_group_start'] as $row){
				$list[]=$row;
			}
		}else{
			$list=null;
		}
		$this->render('view_grafik',array(
		'model'=>$form,'list'=>$list
		));
	}
	
	
	public function actionUnit($id)
	{
		$data=EmployeeGroupList::model()->findAll(array("condition"=>"emp_group_id='$id'"));
		$this->layout='//layouts/admin/pure_blank';
		
		
		if(!isset($_GET['excel'])){
			$this->render('unit',array(
			'model'=>$this->loadModel($id),'data'=>$data
			));
		}else{
			header("Content-Type:   application/vnd.ms-excel; charset=utf-8");
			header("Content-type:   application/x-msexcel; charset=utf-8");
			header("Content-Disposition: attachment; filename=Attendance_Unit.xls"); 
			header("Expires: 0");
			header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
			header("Cache-Control: private",false);
			$this->layout='//layouts/admin/realy_blank';
			$this->render('unit_excel',array(
			'model'=>$this->loadModel($id),'data'=>$data
			));
			exit;
		}
	}
	
	public function actionView($id)
	{
		$model=$this->loadModel($id);
		
		$form=new EmployeeGroupList;
		$form->emp_group_id=$id;
		
		if(isset($_POST['EmployeeGroupList'])){
			$form->attributes=$_POST['EmployeeGroupList'];
			$cek=EmployeeGroupList::model()->find(array("condition"=>"emp_id='$form->emp_id' AND emp_group_id='$id'"));
			if(empty($cek)){
				$form->save();
			}
			if($_POST['EmployeeGroupList']['shift_group_id']!=""){
			$a=$model->emp_group_start;
			$b=$model->emp_group_end;
			$shift=ShiftGroup::model()->findbyPk($_POST['EmployeeGroupList']['shift_group_id']);
			
			$day=Lib::selisih2tanggal($a,$b);
			
			$date=explode("-",$a);
			if($shift->shift_group_type=="Weekly"){
				
				for($i=0;$i<=$day;$i++){
					$next=date("Y-m-d",mktime(0,0,0,$date[1],$date[2]+$i,$date[0]));
					$nmDay=Lib::getDay($next);
					$dtShift=Shift::model()->find(array("condition"=>"shift_group_id='$shift->shift_group_id' AND shift_day='$nmDay'","order"=>"shift_id ASC"));
					
					$new=Attendance::model()->find(array("condition"=>"att_emp_id='$form->emp_id' AND att_date='$next'"));
					if(empty($new)){
						$new=new Attendance;
					}
					$new->shift_id=$dtShift->shift_id;
					$new->att_emp_id=$form->emp_id;
					$new->att_date=$next;
					$new->att_date_to=null;
					$new->att_should_attend=$dtShift->shift_attend;
					$new->save();
				}
			}else{
				
				$dtShift=Shift::model()->findAll(array("condition"=>"shift_group_id='$shift->shift_group_id'","order"=>"shift_id ASC"));
				
				$periode=array();
				foreach($dtShift as $row){
					$periode[]=$row;
				}
				$max=count($periode);
				$index=0;
				for($i=0;$i<=$day;$i++){
					$next=date("Y-m-d",mktime(0,0,0,$date[1],$date[2]+$i,$date[0]));
					
					$new=Attendance::model()->find(array("condition"=>"att_emp_id='$form->emp_id' AND att_date='$next'"));
					if(empty($new)){
						$new=new Attendance;
					}
					
					
					$new->shift_id=$periode[$index]->shift_id;
					$new->att_emp_id=$form->emp_id;
					$new->att_date=$next;
					if($periode[$index]->shift_att_in>$periode[$index]->shift_att_out){
						$ex=explode("-",$next);
						$nextTo=date("Y-m-d",mktime(0,0,0,$ex[1],$ex[2]+1,$ex[0]));
						$new->att_date_to=$nextTo;
					}else{
						$new->att_date_to=null;
					}
					$new->att_should_attend=$periode[$index]->shift_attend;
					$new->save();
					$index++;
					if($index==$max){
						$index=0;
					}
				}
			}
			}
		}
		
		if(isset($_GET['delete'])){
			EmployeeGroupList::model()->findbypk($_GET['delete'])->delete();
			$this->redirect(array('view','id'=>$id));
		}
		
		$data=new EmployeeGroupList('search');
		$data->unsetAttributes();  // clear any default values
		$data->emp_group_id=$id;
		
		$dataArrayEmployee=EmployeeGroupList::model()->findAll(array("condition"=>"emp_group_id='$id'"));
		if(isset($_GET['EmployeeGroupList']))
		$data->attributes=$_GET['EmployeeGroupList'];
		if (isset($_GET['pageSize'])) {
			Yii::app()->user->setState('pageSize',(int)$_GET['pageSize']); 
			unset($_GET['pageSize']);
		}
		
		$this->render('view',array(
		'model'=>$this->loadModel($id),'EGL'=>$form,'data'=>$data,'dataArrayEmployee'=>$dataArrayEmployee
		));
	}

	/**
	* Creates a new model.
	* If creation is successful, the browser will be redirected to the 'view' page.
	*/
	public function actionCreate($org_id)
	{
		$model=new EmployeeGroup;
		
		// Uncomment the following line if AJAX validation is needed
		$this->performAjaxValidation($model);
		$model->org_id=$org_id;
		if(isset($_POST['EmployeeGroup']))
		{
			$model->attributes=$_POST['EmployeeGroup'];
			$model->org_id=$org_id;
			$model->emp_group_status='Active';
			$model->shift_group_id=null;
			if($model->save()){
				
				if($_POST['EmployeeGroup']['emp_id']!=""){
					foreach($_POST['EmployeeGroup']['emp_id'] as $row){
						if($row!="0"){
						$form=new EmployeeGroupList;
						$form->emp_group_id=$model->emp_group_id;
						$form->emp_id=$row;
						$cek=EmployeeGroupList::model()->find(array("condition"=>"emp_id='$row' AND emp_group_id='$model->emp_group_id'"));
						if(empty($cek)){
							$form->save();
						}
						}
					}
				}
				if($_POST['EmployeeGroup']['shift_group_id']!=""){
					$a=$model->emp_group_start;
					$b=$model->emp_group_end;
					$shift=ShiftGroup::model()->findbyPk($_POST['EmployeeGroup']['shift_group_id']);
					
					$day=Lib::selisih2tanggal($a,$b);
					
					$date=explode("-",$a);
					if($shift->shift_group_type=="Weekly"){
						
						for($i=0;$i<=$day;$i++){
							$next=date("Y-m-d",mktime(0,0,0,$date[1],$date[2]+$i,$date[0]));
							$nmDay=Lib::getDay($next);
							$dtShift=Shift::model()->find(array("condition"=>"shift_group_id='$shift->shift_group_id' AND shift_day='$nmDay'","order"=>"shift_id ASC"));
							foreach($_POST['EmployeeGroup']['emp_id'] as $row){
								if($row!="0"){
									$new=Attendance::model()->find(array("condition"=>"att_emp_id='$row' AND att_date='$next'"));
									if(empty($new)){
										$new=new Attendance;
									}
									$new->shift_id=$dtShift->shift_id;
									$new->att_emp_id=$row;
									$new->att_date=$next;
									$new->att_date_to=null;
									$new->att_should_attend=$dtShift->shift_attend;
									if($new->save()){
										$update=Attendance::model()->findbyPk($new->att_id);
										$update->shift_id=$new->shift_id;
										$shift=Shift::model()->findbyPk($new->shift_id);
										if($shift->shift_att_in>$shift->shift_att_out){
											$ex=explode("-",$update->att_date);
											$nextTo=date("Y-m-d",mktime(0,0,0,$ex[1],$ex[2]+1,$ex[0]));
											$update->att_date_to=$nextTo;
										}else{
											$update->att_date_to=null;
										}
										
										$update->att_should_attend=$shift->shift_attend;
										if($update->att_should_attend=="No" or $update->att_sts_id!=''){
											$update->att_in=null;
											$update->att_out=null;
											$update->fp_id=null;
										}else{
											if($update->att_date_to==""){
												$dataIN=Attendance::checkIn($shift->shift_att_in,$update->att_date,$update->attEmp->att_id);
												$dataOUT=Attendance::checkOut($shift->shift_att_out,$update->att_date,$update->attEmp->att_id);
												$update->att_in=$dataIN;
												$update->att_out=$dataOUT;
												$update->fp_id=$dataIN->fp_id;
												$update->att_update=date("Y-m-d H:i:s");
											}else{
												$dataIN=Attendance::checkIn($shift->shift_att_in,$update->att_date,$update->attEmp->att_id);
												$dataOUT=Attendance::checkOut($shift->shift_att_out,$update->att_date_to,$update->attEmp->att_id);
												$update->att_in=$dataIN;
												$update->att_out=$dataOUT;
												$update->fp_id=$dataIN->fp_id;
												$update->att_update=date("Y-m-d H:i:s");
											}
											
										}
										$update->save();
									}
								}
							}
						}
					}else{
						
						$dtShift=Shift::model()->findAll(array("condition"=>"shift_group_id='$shift->shift_group_id'","order"=>"shift_id ASC"));
						
						$periode=array();
						foreach($dtShift as $row){
							$periode[]=$row;
						}
						$max=count($periode);
						$index=0;
						for($i=0;$i<=$day;$i++){
							$next=date("Y-m-d",mktime(0,0,0,$date[1],$date[2]+$i,$date[0]));
							foreach($_POST['EmployeeGroup']['emp_id'] as $row){
								if($row!="0"){
									$new=Attendance::model()->find(array("condition"=>"att_emp_id='$row' AND att_date='$next'"));
									if(empty($new)){
										$new=new Attendance;
									}							
									$new->shift_id=$periode[$index]->shift_id;
									$new->att_emp_id=$row;
									$new->att_date=$next;
									if($periode[$index]->shift_att_in>$periode[$index]->shift_att_out){
										$ex=explode("-",$next);
										$nextTo=date("Y-m-d",mktime(0,0,0,$ex[1],$ex[2]+1,$ex[0]));
										$new->att_date_to=$nextTo;
									}else{
										$new->att_date_to=null;
									}
									$new->att_should_attend=$periode[$index]->shift_attend;
									if($new->save()){
										$update=Attendance::model()->findbyPk($new->att_id);
										$update->shift_id=$new->shift_id;
										$shift=Shift::model()->findbyPk($new->shift_id);
										if($shift->shift_att_in>$shift->shift_att_out){
											$ex=explode("-",$update->att_date);
											$nextTo=date("Y-m-d",mktime(0,0,0,$ex[1],$ex[2]+1,$ex[0]));
											$update->att_date_to=$nextTo;
										}else{
											$update->att_date_to=null;
										}
										
										$update->att_should_attend=$shift->shift_attend;
										if($update->att_should_attend=="No" or $update->att_sts_id!=''){
											$update->att_in=null;
											$update->att_out=null;
											$update->fp_id=null;
										}else{
											if($update->att_date_to==""){
												$dataIN=Attendance::checkIn($shift->shift_att_in,$update->att_date,$update->attEmp->att_id);
												$dataOUT=Attendance::checkOut($shift->shift_att_out,$update->att_date,$update->attEmp->att_id);
												$update->att_in=$dataIN;
												$update->att_out=$dataOUT;
												$update->fp_id=$dataIN->fp_id;
												$update->att_update=date("Y-m-d H:i:s");
											}else{
												$dataIN=Attendance::checkIn($shift->shift_att_in,$update->att_date,$update->attEmp->att_id);
												$dataOUT=Attendance::checkOut($shift->shift_att_out,$update->att_date_to,$update->attEmp->att_id);
												$update->att_in=$dataIN;
												$update->att_out=$dataOUT;
												$update->fp_id=$dataIN->fp_id;
												$update->att_update=date("Y-m-d H:i:s");
											}
											
										}
										$update->save();
									}
								}
							}
							$index++;
							if($index==$max){
								$index=0;
							}
						}
					}
				}else{
					$a=$model->emp_group_start;
					$b=$model->emp_group_end;
					$shift=ShiftGroup::model()->find(array("condition"=>"shift_group_for='Back Office'"));
					
					$day=Lib::selisih2tanggal($a,$b);
					
					$date=explode("-",$a);
					if($shift->shift_group_type=="Weekly"){
						
						for($i=0;$i<=$day;$i++){
							$next=date("Y-m-d",mktime(0,0,0,$date[1],$date[2]+$i,$date[0]));
							$nmDay=Lib::getDay($next);
							$dtShift=Shift::model()->find(array("condition"=>"shift_group_id='$shift->shift_group_id' AND shift_day='$nmDay'","order"=>"shift_id ASC"));
							foreach($_POST['EmployeeGroup']['emp_id'] as $row){
								if($row!="0"){
									$new=Attendance::model()->find(array("condition"=>"att_emp_id='$row' AND att_date='$next'"));
									if(empty($new)){
										$new=new Attendance;
									}
									$new->shift_id=$dtShift->shift_id;
									$new->att_emp_id=$row;
									$new->att_date=$next;
									$new->att_date_to=null;
									$new->att_should_attend=$dtShift->shift_attend;
									if($new->save()){
										$update=Attendance::model()->findbyPk($new->att_id);
										$update->shift_id=$new->shift_id;
										$shift=Shift::model()->findbyPk($new->shift_id);
										if($shift->shift_att_in>$shift->shift_att_out){
											$ex=explode("-",$update->att_date);
											$nextTo=date("Y-m-d",mktime(0,0,0,$ex[1],$ex[2]+1,$ex[0]));
											$update->att_date_to=$nextTo;
										}else{
											$update->att_date_to=null;
										}
										
										$update->att_should_attend=$shift->shift_attend;
										if($update->att_should_attend=="No" or $update->att_sts_id!=''){
											$update->att_in=null;
											$update->att_out=null;
											$update->fp_id=null;
										}else{
											if($update->att_date_to==""){
												$dataIN=Attendance::checkIn($shift->shift_att_in,$update->att_date,$update->attEmp->att_id);
												$dataOUT=Attendance::checkOut($shift->shift_att_out,$update->att_date,$update->attEmp->att_id);
												$update->att_in=$dataIN;
												$update->att_out=$dataOUT;
												$update->fp_id=$dataIN->fp_id;
												$update->att_update=date("Y-m-d H:i:s");
											}else{
												$dataIN=Attendance::checkIn($shift->shift_att_in,$update->att_date,$update->attEmp->att_id);
												$dataOUT=Attendance::checkOut($shift->shift_att_out,$update->att_date_to,$update->attEmp->att_id);
												$update->att_in=$dataIN;
												$update->att_out=$dataOUT;
												$update->fp_id=$dataIN->fp_id;
												$update->att_update=date("Y-m-d H:i:s");
											}
											
										}
										$update->save();
									}
								}
							}
						}
					}
				}
				$this->redirect(array('view','id'=>$model->emp_group_id));
			}
		}

		$this->render('create',array(
		'model'=>$model,
		));
	}

	/**
	* Updates a particular model.
	* If update is successful, the browser will be redirected to the 'view' page.
	* @param integer $id the ID of the model to be updated
	*/
	public function actionUpdate($id)
	{
		$model=$this->loadModel($id);

		// Uncomment the following line if AJAX validation is needed
		$this->performAjaxValidation($model);
	
		if(isset($_POST['EmployeeGroup']))
		{
			$model->attributes=$_POST['EmployeeGroup'];
			if($model->save()){
				$this->redirect(array('view','id'=>$model->emp_group_id));
			}
		}

		$this->render('update',array(
		'model'=>$model,
		));
	}

	/**
	* Deletes a particular model.
	* If deletion is successful, the browser will be redirected to the 'admin' page.
	* @param integer $id the ID of the model to be deleted
	*/
	public function actionDelete($id)
	{
		if(Yii::app()->request->isPostRequest)
		{
		// we only allow deletion via POST request		
		$this->loadModel($id)->delete();

		// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
		if(!isset($_GET['ajax']))
		$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
		}
		else
		throw new CHttpException(400,'Invalid request. Please do not repeat this request again.');
	}

	/**
	* Lists all models.
	*/
	public function actionAdmin()
	{
		$dataProvider=new CActiveDataProvider('EmployeeGroup');
		$this->render('index',array(
		'dataProvider'=>$dataProvider,
		));
	}

	/**
	* Manages all models.
	*/
	public function actionIndex()
	{
		if(isset($_GET['org'])){
			$model=new EmployeeGroup('search');
			$model->unsetAttributes();
			$model->org_id=$_GET['org'];
			if (isset($_GET['pageSize'])) {
				Yii::app()->user->setState('pageSize',(int)$_GET['pageSize']); 
				unset($_GET['pageSize']);
			}
		}else{
			
			$model=new EmployeeGroup('searchByOrganization');
		}
		
		  // clear any default values
		if(isset($_GET['EmployeeGroup']))
		$model->attributes=$_GET['EmployeeGroup'];
		
		$this->render('admin',array(
		'model'=>$model,
		));
	}
	
	public function actionBatchDelete()
	{
		$request = Yii::app()->getRequest();
		if($request->getIsPostRequest()){
			if(isset($_POST['ids'])){
				$ids = $_POST['ids'];
			}
			if (empty($ids)) {
				echo CJSON::encode(array('status' => 'failure', 'msg' => 'you should at least choice one item'));
				die();
			}
			$successCount = $failureCount = 0;
			$dataError=array();
			foreach ($ids as $id) {
				$model = $this->loadModel($id);
				$temp = $this->loadModel($id);
				if($model->delete()){
					$successCount++ ;
				}else{
					$failureCount++;
					$dataError[]=$model->emp_group_id;
				}
			}
			echo CJSON::encode(array('status' => 'success',
				'data' => array(
					'successCount' => $successCount,
					'failureCount' => $failureCount,
					'dataError' => $dataError,
				)));
			die();
		}else{
			throw new CHttpException(400,'Invalid request. Please do not repeat this request again.');
		}
	}
	
	public function actionExportExcel()
	{
		ini_set("memory_limit","8056M");
		set_time_limit(100000);
		Yii::import('ext.phpexcel.XPHPExcel');    
		$objPHPExcel= XPHPExcel::createPHPExcel();
		$objPHPExcel->getProperties()->setCreator("File")
		->setLastModifiedBy("File")
		->setTitle("Dokumen")
		->setSubject("Dokumen")
		->setDescription("File")
		->setKeywords("File")
		->setCategory("File");
		$model=new EmployeeGroup;

		$objPHPExcel->setActiveSheetIndex(0)->setCellValue('A1',"No");
		$objPHPExcel->setActiveSheetIndex(0)->setCellValue('B1',$model->getAttributeLabel('emp_group_id'));
		$objPHPExcel->setActiveSheetIndex(0)->setCellValue('C1',$model->getAttributeLabel('shift_group_id'));
		$objPHPExcel->setActiveSheetIndex(0)->setCellValue('D1',$model->getAttributeLabel('emp_id'));
		$objPHPExcel->setActiveSheetIndex(0)->setCellValue('E1',$model->getAttributeLabel('emp_group_start'));
		$objPHPExcel->setActiveSheetIndex(0)->setCellValue('F1',$model->getAttributeLabel('emp_group_end'));
		$objPHPExcel->setActiveSheetIndex(0)->setCellValue('G1',$model->getAttributeLabel('emp_group_create'));
		$objPHPExcel->setActiveSheetIndex(0)->setCellValue('H1',$model->getAttributeLabel('emp_group_update'));
		
		$criteria=new CDbCriteria;
		$criteria->addCondition("emp_group_id IN (".$_POST['ids'].")");
		$data= EmployeeGroup::model()->findAll($criteria);    
		$j=1;
		$no=0;
		foreach($data as $row) {
			$j++;
			$no++;
			$objPHPExcel->setActiveSheetIndex(0)->setCellValue('A'.$j, $no);
			$objPHPExcel->setActiveSheetIndex(0)->setCellValue('B'.$j, $row->emp_group_id);
			$objPHPExcel->setActiveSheetIndex(0)->setCellValue('C'.$j, $row->shift_group_id);
			$objPHPExcel->setActiveSheetIndex(0)->setCellValue('D'.$j, $row->emp_id);
			$objPHPExcel->setActiveSheetIndex(0)->setCellValue('E'.$j, $row->emp_group_start);
			$objPHPExcel->setActiveSheetIndex(0)->setCellValue('F'.$j, $row->emp_group_end);
			$objPHPExcel->setActiveSheetIndex(0)->setCellValue('G'.$j, $row->emp_group_create);
			$objPHPExcel->setActiveSheetIndex(0)->setCellValue('H'.$j, $row->emp_group_update);
		}
		$styleArray = array(
			'font' => array(
				'bold' => true,
			),
		);
		$styleArrayData = array(
			'borders' => array(
				'allborders' => array(
					'style' => PHPExcel_Style_Border::BORDER_THIN
				),
			),
		);
		$objPHPExcel->getActiveSheet()->getStyle('A1:H8')->applyFromArray($styleArray);
		$objPHPExcel->getActiveSheet()->getStyle('A1:H'.$j)->applyFromArray($styleArrayData);
		// Rename worksheet
		$objPHPExcel->getActiveSheet()->setTitle('Default');

		// Set active sheet index to the first sheet, so Excel opens this as the first sheet
		$objPHPExcel->setActiveSheetIndex(0);
		 		 
		// Redirect output to a client�s web browser (Excel5)
		header('Content-Type: application/vnd.ms-excel');
		header('Content-Disposition: attachment;filename="Template Employee Group '.date('Y-m-d H:i:s').'.xls"');
		header('Cache-Control: max-age=0');
		// If you're serving to IE 9, then the following may be needed
		header('Cache-Control: max-age=1');
		 
		// If you're serving to IE over SSL, then the following may be needed
		header ('Expires: Mon, 26 Jul 1997 05:00:00 GMT'); // Date in the past
		header ('Last-Modified: '.gmdate('D, d M Y H:i:s').' GMT'); // always modified
		header ('Cache-Control: cache, must-revalidate'); // HTTP/1.1
		header ('Pragma: public'); // HTTP/1.0
		 
		$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
		$objWriter->save('php://output');
	    Yii::app()->end();
	}
	
	public function actionExportPdf()
	{
		Yii::import('ext.MPDF57.*');
		include("mpdf.php");
		ini_set("memory_limit","8056M");
		set_time_limit(100000);
		$mpdf=new mPDF('utf-8', 'A4-L', 0, null, 3,3,10,20,15,15 );
		
		$model=new EmployeeGroup;
		
		$criteria=new CDbCriteria;
		$criteria->addCondition("emp_group_id IN (".$_POST['ids'].")");
		$data= EmployeeGroup::model()->findAll($criteria); 
		
		$j = 1;
		$no=0;
		
		$html='
		<style type="text/css" media="print,screen" >
		th {
			font-family:Arial;
			color:black;
			background-color:lightgrey;
			padding:5px;
		}
		td{
			padding:5px;
		}
		thead {
			display:table-header-group;
		}
		tbody {
			display:table-row-group;
		}
		</style>
		<h2 style="text-align: center;">Employee Group</h2>
		<br>
		<table border="1" style="border:1px; width:100%; vertical-align: text-top;border-collapse: collapse;">
			<thead>
				<tr>
					<th>No</th>
										<th>'.$model->getAttributeLabel('emp_group_id').'</th>
					<th>'.$model->getAttributeLabel('shift_group_id').'</th>
					<th>'.$model->getAttributeLabel('emp_id').'</th>
					<th>'.$model->getAttributeLabel('emp_group_start').'</th>
					<th>'.$model->getAttributeLabel('emp_group_end').'</th>
					<th>'.$model->getAttributeLabel('emp_group_create').'</th>
					<th>'.$model->getAttributeLabel('emp_group_update').'</th>
				</tr>
			</thead>
			<tbody>';
			
			$no=0;
			foreach($data as $row){
			$no++;
			$html.='
				<tr>
					<td>'.$no.'</td>
										<td>'.$row->emp_group_id.'</td>
					<td>'.$row->shift_group_id.'</td>
					<td>'.$row->emp_id.'</td>
					<td>'.$row->emp_group_start.'</td>
					<td>'.$row->emp_group_end.'</td>
					<td>'.$row->emp_group_create.'</td>
					<td>'.$row->emp_group_update.'</td>
				</tr>';
			}	
			$html.='
			</tbody>
		</table>	
		';
		ini_set("memory_limit","256M");
		$mpdf->Bookmark('Start of the document');
		$mpdf->SetDisplayMode('fullpage','two');
		$mpdf->setFooter('Tanggal {DATE j-m-Y}||Halaman {PAGENO} dari {nbpg}') ;
		$mpdf->simpleTables = true;
		$mpdf->WriteHTML('<sheet-size="Letter" />');
		$mpdf->WriteHTML($html);
		$mpdf->Output();
	}
	
	public function actionImport()
	{
		ini_set("memory_limit","8056M");
		set_time_limit(100000);
		$model=new EmployeeGroup;
		if(isset($_POST['EmployeeGroup']))
		{
		$fileUpload=CUploadedFile::getInstance($model,'import');
		$nama=date('YmdHis').'_'.$fileUpload;
		$path=Yii::app()->basePath . '/../temp_excel/'.$nama.'';
		$fileUpload->saveAs($path);
			
		Yii::import('ext.phpexcelreader.JPhpExcelReader');
		$data=new JPhpExcelReader($path);
		$baris=$data->sheets[0][numRows];
		$kolom=$data->sheets[0][numCols];
		$sukses=0;
		$gagal=0;
		for($i=2;$i<=$baris; $i++){
			$model=new EmployeeGroup;
						$model->emp_group_id= $data->sheets[0]['cells'][$i][1];
			$model->shift_group_id= $data->sheets[0]['cells'][$i][2];
			$model->emp_id= $data->sheets[0]['cells'][$i][3];
			$model->emp_group_start= $data->sheets[0]['cells'][$i][4];
			$model->emp_group_end= $data->sheets[0]['cells'][$i][5];
			$model->emp_group_create= $data->sheets[0]['cells'][$i][6];
			$model->emp_group_update= $data->sheets[0]['cells'][$i][7];
			$cek=EmployeeGroup::model()->findbyPk($data->sheets[0]['cells'][$i][1]);
			if(count($cek)==0){
				if ($model->save()){
				$sukses++;
				}else{
				$gagal++;
				}
			} else {
				$gagal++;
			}
		}
		@unlink($path);
		 Yii::app()->user->setFlash('success', "Proses import data selesai<br/>
         Jumlah data yang sukses diimport : ".$sukses." Jumlah data yang gagal diimport : ".$gagal."
         ");
		$this->redirect(array('index'));
		}
	}
	
	public function actionDownloadTemplate(){
		Yii::import('ext.phpexcel.XPHPExcel');    
		$objPHPExcel= XPHPExcel::createPHPExcel();
		$objPHPExcel->getProperties()->setCreator("File")
		->setLastModifiedBy("File")
		->setTitle("Dokumen Rahasia")
		->setSubject("Dokumen Rahasia")
		->setDescription("File")
		->setKeywords("File")
		->setCategory("File");
		$model=new EmployeeGroup;
		//loop here		
				$objPHPExcel->setActiveSheetIndex(0)->setCellValue('A1',$model->getAttributeLabel('emp_group_id'));
		$objPHPExcel->setActiveSheetIndex(0)->setCellValue('B1',$model->getAttributeLabel('shift_group_id'));
		$objPHPExcel->setActiveSheetIndex(0)->setCellValue('C1',$model->getAttributeLabel('emp_id'));
		$objPHPExcel->setActiveSheetIndex(0)->setCellValue('D1',$model->getAttributeLabel('emp_group_start'));
		$objPHPExcel->setActiveSheetIndex(0)->setCellValue('E1',$model->getAttributeLabel('emp_group_end'));
		$objPHPExcel->setActiveSheetIndex(0)->setCellValue('F1',$model->getAttributeLabel('emp_group_create'));
		$objPHPExcel->setActiveSheetIndex(0)->setCellValue('G1',$model->getAttributeLabel('emp_group_update'));
		
		$data= EmployeeGroup::model()->findAll(array('limit'=>10));    
		$j = 1;
		$no=0;
		foreach($data as $row) {
			$j++;
			$no++;
						$objPHPExcel->setActiveSheetIndex(0)->setCellValue('A'.$j, $row->emp_group_id);
			$objPHPExcel->setActiveSheetIndex(0)->setCellValue('B'.$j, $row->shift_group_id);
			$objPHPExcel->setActiveSheetIndex(0)->setCellValue('C'.$j, $row->emp_id);
			$objPHPExcel->setActiveSheetIndex(0)->setCellValue('D'.$j, $row->emp_group_start);
			$objPHPExcel->setActiveSheetIndex(0)->setCellValue('E'.$j, $row->emp_group_end);
			$objPHPExcel->setActiveSheetIndex(0)->setCellValue('F'.$j, $row->emp_group_create);
			$objPHPExcel->setActiveSheetIndex(0)->setCellValue('G'.$j, $row->emp_group_update);
		}
		$styleArray = array(
			'font' => array(
				'bold' => true,
			),
		);
		$styleArrayData = array(
			'borders' => array(
				'allborders' => array(
					'style' => PHPExcel_Style_Border::BORDER_THIN
				),
			),
		);
		$objPHPExcel->getActiveSheet()->getStyle('A1:G7')->applyFromArray($styleArray);
		$objPHPExcel->getActiveSheet()->getStyle('A1:G'.$j)->applyFromArray($styleArrayData);
		// Rename worksheet
		$objPHPExcel->getActiveSheet()->setTitle('Default');

		// Set active sheet index to the first sheet, so Excel opens this as the first sheet
		$objPHPExcel->setActiveSheetIndex(0);
		 		 
		// Redirect output to a client�s web browser (Excel5)
		header('Content-Type: application/vnd.ms-excel');
		header('Content-Disposition: attachment;filename="Template Employee Group '.date('Y-m-d H:i:s').'.xls"');
		header('Cache-Control: max-age=0');
		// If you're serving to IE 9, then the following may be needed
		header('Cache-Control: max-age=1');
		 
		// If you're serving to IE over SSL, then the following may be needed
		header ('Expires: Mon, 26 Jul 1997 05:00:00 GMT'); // Date in the past
		header ('Last-Modified: '.gmdate('D, d M Y H:i:s').' GMT'); // always modified
		header ('Cache-Control: cache, must-revalidate'); // HTTP/1.1
		header ('Pragma: public'); // HTTP/1.0
		 
		$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
		$objWriter->save('php://output');
	    Yii::app()->end();
	}
	

	public function loadModel($id)
	{
		$model=EmployeeGroup::model()->findByPk($id);
		if($model===null)
		throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}

	/**
	* Performs the AJAX validation.
	* @param CModel the model to be validated
	*/
	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='employee-group-form')
		{
		echo CActiveForm::validate($model);
		Yii::app()->end();
		}
	}
}
