<?php

class HisysController extends Controller
{
	public $layout='//layouts/admin/main';
	public function actionIndex()
	{
		$this->render('index');
	}
	
	public function actionPrintLabelRm()
	{
		$this->layout='//layouts/admin/blank';
		if(isset($_POST['url'])){
			$url=$_POST['url'];
			$ex=explode("&",$url);
			$pid=trim(str_replace("pid=","",$ex[1]));
			$regpid=trim(str_replace("regpid=","",$ex[2]));
			$token=trim(str_replace("AULIA=","",$ex[4]));
			
			$tgl="http://100.100.100.1/print.php?mod=patient_popup&cmd=slip_antrian&pid=$pid&regpid=$regpid&AULIA=$token";
			$reg=file_get_contents(trim($tgl));
			$getTgl=explode('<td width="25%" style="font-weight: bold;border:none">Tanggal</td>',$reg);
			$getTgl=explode('<td  style="border:none">:',$getTgl[1]);
			$getTgl=explode('</td>',$getTgl[1]);
		
			$regis=$_POST['date'];
			$n=$_POST['n'];
			$data=file_get_contents($url);
			$explode=explode('<div class="wristband_container3">',$data);
			$namaUnfix=explode('<span class="patient" style="text-align: left;">',$explode[1]);
			$nama=explode('</span>',$namaUnfix[1]);
			$rmUnfix=explode('<span style="text-align: left;">',$nama[1]);
			$ttlUnfix=explode('<span style="text-align: left;">',$nama[2]);
			$deptUnfix=explode('<span>',$nama[3]);
			$penjaminUnfix=explode('<span>',$nama[6]);
			$namafix=$nama[0];
			$rmfix=$rmUnfix[1];
			$ttlfix=$ttlUnfix[1];
			$deptfix=$deptUnfix[1];
			$penjaminfix=$penjaminUnfix[1];
			$gelar=explode(",",$namafix);
			$gelar=explode("(",$gelar[1]);
			$gelar=trim($gelar[0]);
			
			$jk=explode("(",$namafix);
			$jk=explode(")",$jk[1]);
			$jk=trim($jk[0]);
			
			$namafix=explode(",",$namafix);
			
			$namafix=((strlen($namafix[0])>20)?substr($namafix[0],0,20)."..":$namafix[0])." ,".$gelar." (".$jk.")";
			$data=array(
				"nama"=>$namafix,
				"rm"=>$rmfix,
				"ttl"=>$ttlfix,
				"dept"=>$deptfix,
				"penjamin"=>$penjaminfix,
				"regis"=>str_replace("","",trim($getTgl[0])),
				"n"=>$n
			);
		}else{
			$data=null;
		}
		$this->render('print_label_rm',array("data"=>$data));
	}
}