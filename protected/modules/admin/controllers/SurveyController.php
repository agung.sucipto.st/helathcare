<?php
class SurveyController extends Controller
{
	/**
	* @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	* using two-column layout. See 'protected/views/layouts/column2.php'.
	*/
	public $layout='//layouts/admin/main';

	/**
	* @return array action filters
	*/
	public function filters()
	{
		return array(
		'accessControl', // perform access control for CRUD operations
		);
	}

	/**
	* Specifies the access control rules.
	* This method is used by the 'accessControl' filter.
	* @return array access control rules
	*/
	public function accessRules()
	{
		return array(
		array('allow',  // allow all users to perform 'index' and 'view' actions
		'actions'=>array('setStatus','createManual','manual','changeStatus','getDevice','getCurrent','getAction','getAlert','processAlert'),
		'users'=>array('*'),
		),
		array('allow',  // allow all users to perform 'index' and 'view' actions
		'actions'=>array('index','view'),
		'expression'=>'$user->getAuth()',
		),
		array('allow', // allow admin user to perform 'admin' and 'delete' actions
		'actions'=>array('create','update','admin','delete','BatchDelete','exportExcel','exportPdf','import','downloadTemplate','createQuestioner','questionerList','rekapitulasi','print'),
		'expression'=>'$user->getAuth()',
		),
		array('allow', // allow authenticated user to perform 'create' and 'update' actions
		'actions'=>array('combo'),
		'users'=>array('@'),
		),
		array('deny',  // deny all users
		'users'=>array('*'),
		),
		);
	}
	
	public function actionGetDevice()
	{	
			$device=SurveyDevice::model()->findAll(array("condition"=>"sd_status='Active'"));
			$display=0;
			$cek=Registration::model()->findAll(array("condition"=>"reg_questioner='Active'"));
			$temp=array();
			foreach($cek as $row){
				$temp[]=$row->sd_id;
			}
			print_R($cek);
			foreach($device as $row){
				
				if(!in_array($row->sd_id,$temp)){
					$display++;
					echo'<option value="'.$row->sd_id.'">'.$row->sd_name.' | '.$row->sd_location.'</option>';
				}
			}
	}
	
	public function actionGetCurrent()
	{	
		$model=Registration::model()->find(array("condition"=>"reg_questioner='Active'"));
		$survey=Survey::model()->find(array("condition"=>"sv_status='Open' and reg_id is null"));
		if(!empty($model) AND empty($survey)){
			echo "<b>Pasien ".Lib::MRN($model->p_id)." - ".$model->p->p_name." Sedang Mengisi Questioner</b>";
		}elseif(empty($model) AND !empty($survey)){
			echo "<b>Pasien ".$survey->sv_person_name." Sedang Mengisi Questioner</b>";
		}else{
			
		}
	}
	
	public function actionGetAction()
	{	
		$device=SurveyDevice::model()->findAll(array("condition"=>"sd_status='Active'"));
		$display=0;
		foreach($device as $row){
			$cek=Registration::model()->count(array("condition"=>"reg_questioner='Active' and sd_id='$row->sd_id'"));
			if($cek==0){
				$display++;
				echo'<option value="'.$row->sd_id.'">'.$row->sd_name.' | '.$row->sd_location.'</option>';
			}
		}
		if($display>0){
			echo"yes";
		}else{
			echo"no";
		}
	}
	
	public function actionGetAlert()
	{	
		$survey=Survey::model()->find(array("condition"=>"sv_alert='1' and date(sv_time)='".date("Y-m-d")."'","order"=>"sv_time DESC"));
		$data=array();
		if(!empty($survey)){
			$data['data']='yes';
			$data['sv_id']=$survey->sv_id;
			$data['no_reg']=$survey->reg->reg_no;
			$data['no_mr']=Lib::MRN($survey->reg->p_id);
			$data['nama']=$survey->reg->p->p_name.' '.$survey->reg->p->p_title;
			$data['layanan']=$survey->reg->reg_dept;
			$data['jaminan']=$survey->reg->reg_guarator;
			$list=SurveyAnswer::model()->findAll(array("condition"=>"sv_id='$survey->sv_id'"));
			foreach($list as $row){
				if($row->qo->qo_alert>0){
					$data['option'][]="<li style='color:red;'>".$row->qq->qq_question." : ".$row->qo->qo_name."</li>";
				}else{
					$data['option'][]="<li>".$row->qq->qq_question." : ".$row->qo->qo_name."</li>";
				}
				
			}
		}else{
			$data['data']='no';
		}
		echo json_encode($data);
	}
	
	public function actionProcessAlert()
	{	
		$data=Survey::model()->findByPk($_POST['id']);
		$data->sv_alert=$_POST['act'];
		$data->save();
	}
	
	public function actionSetStatus($id)
	{	
		$data=Registration::model()->findByPk($id);
		if($data->reg_questioner=="Active"){
			$data->reg_questioner="Inactive";
			$data->sd_id=NULL;
		}else{
			$data->reg_questioner="Active";
			$data->sd_id=$_GET['sd'];
			//$reg=Survey::model()->updateAll(array('sv_status' => ''), "sv_status ='Open'");
		}
		$data->save();
		$this->redirect(array('index'));
	}
	
	public function actionChangeStatus($id)
	{	
		$data=Survey::model()->findByPk($id);
		if($data->sv_status=="Open"){
			$data->sv_status="";
		}else{
			$data->sv_status="Open";
			//$reg=Registration::model()->updateAll(array('reg_questioner' => 'Inactive','sd_id'=>NULL), "reg_questioner ='Active'");
		}
		$data->save();
		$this->redirect(array('manual'));
	}
	
	public function actionPrint()
	{		
		$criteria=new CDbCriteria;
		$dIn=array();
		$ex=explode(",",$_POST['ids']);
		foreach($ex as $d){
			$dIn[]="'$d'";
		}
		$criteria->with=array("reg");
		$criteria->order="DATE(reg_date_in) ASC";
		$in=implode(",",$dIn);
		$criteria->addCondition("sv_id IN (".$in.")");
		$data= Survey::model()->findAll($criteria); 
		
		$this->layout='//layouts/admin/blank';
		$this->render('print_survey',array(
		'model'=>$data,
		));
		
	}
	
	public function actionCombo()
	{
	//please enter current controller name because yii send multi dim array 
	//$data=QuestionQuiz::model()->findAll('qc_id=:parent_id', array(':parent_id'=>(int) $_POST['Survey']['qc_id']));
	$data=QuestionQuiz::model()->findAll(array("condition"=>"qc_id='".$_POST['Survey']['qc_id']."'","order"=>"qq_order,qc_id asc"));

	$data=CHtml::listData($data,'qq_id','qq_question');
	echo CHtml::tag('option',
	array('value'=>""),"-Semua-",true);
	foreach($data as $value=>$name)
	{
	echo CHtml::tag('option',
	array('value'=>$value),CHtml::encode($name),true);
	}
	} 

	/**
	* Displays a particular model.
	* @param integer $id the ID of the model to be displayed
	*/
	
	public function actionRekapitulasi()
	{
		$model=new Survey();
		$model->sv_time=date("Y-m")."-01";
		$model->create_at=date("Y-m-d");
		$data=array();
		$detail=array();
		$info=null;
		if(isset($_POST['Survey'])){
			if($_POST['Survey']['type_tgl']=="sv_time"){
				$type=$_POST['Survey']['type_tgl'];
				$info="Input Questioner";
			}else{
				$type="reg.".$_POST['Survey']['type_tgl'];
				if($_POST['Survey']['type_tgl']=="reg_date_in"){
					$info="Tanggal Registrasi";
				}else{
					$info="Tanggal Pasien Pulang";
				}
			}
			$model->type_tgl=$_POST['Survey']['type_tgl'];
		}else{
			$type="reg.reg_date_in";
		}
		
		
		if(!isset($_POST['Survey']['sv_time']) AND !isset($_POST['Survey']['create_at']) AND !isset($_POST['Survey']['qc_id'])){
			//kondisi tanpa submit atau default first load
			$cat=QuestionCategory::model()->find(array("condition"=>"qc_status='Active' and qc_id='2'"));
			$title=$cat->qc_name."<br/>".Lib::dateInd(date("Y-m"),false);
			$survey=SurveyAnswer::model()->findAll(array("with"=>array("sv","sv.reg","qq"),"condition"=>"DATE($type) between '".date("Y-m")."-01' AND '".date("Y-m-d")."' AND sv.qc_id='$cat->qc_id'","order"=>"qq.qq_order,qq.qc_id ASC"));
			$group=Survey::model()->count(array("with"=>array("reg"),"condition"=>"DATE($type) between '".date("Y-m")."-01' AND '".date("Y-m-d")."' AND qc_id='$cat->qc_id'"));
		}elseif(isset($_POST['Survey']['sv_time']) AND isset($_POST['Survey']['create_at']) AND $_POST['Survey']['qc_id']==""){
			//kondisi tanggal mulai + selesai dengan semua kategori
			$cat=QuestionCategory::model()->findbyPk($_POST['Survey']['qc_id']);
			$model->attributes=$_POST['Survey'];
			$model->sv_time=$_POST['Survey']['sv_time'];
			$model->create_at=$_POST['Survey']['create_at'];
			$model->qc_id=$_POST['Survey']['qc_id'];
			$title="Keseluruhan<br/>".Lib::dateInd($_POST[Survey][sv_time],false)." s.d ".Lib::dateInd($_POST[Survey][create_at],false)." (".$info.")";
			$survey=SurveyAnswer::model()->findAll(array("with"=>array("sv","sv.reg","qq"),"condition"=>"DATE($type) between '".$_POST[Survey][sv_time]."' AND '".$_POST[Survey][create_at]."'","order"=>"qq.qq_order,qq.qc_id ASC"));
			$group=Survey::model()->count(array("with"=>array("reg"),"condition"=>"DATE($type) between '".$_POST[Survey][sv_time]."' AND '".$_POST[Survey][create_at]."'"));
		}elseif(isset($_POST['Survey']['sv_time']) AND isset($_POST['Survey']['create_at']) AND $_POST['Survey']['qc_id']!=""){
			
			if($_POST['Survey']['modified_at']!=''){
				//kondisi tanggal mulai + selesai dengan kategori dan sub questioner
				$cat=QuestionCategory::model()->findbyPk($_POST['Survey']['qc_id']);
				$model->attributes=$_POST['Survey'];
				$model->sv_time=$_POST['Survey']['sv_time'];
				$model->create_at=$_POST['Survey']['create_at'];
				$model->qc_id=$_POST['Survey']['qc_id'];
				$title=$cat->qc_name."<br/>".Lib::dateInd($_POST[Survey][sv_time],false)." s.d ".Lib::dateInd($_POST[Survey][create_at],false)." (".$info.")";
				$survey=SurveyAnswer::model()->findAll(array("alias"=>"p","with"=>array("sv","sv.reg","qq"),"condition"=>"DATE($type) between '".$_POST[Survey][sv_time]."' AND '".$_POST[Survey][create_at]."' AND p.qq_id IN(".implode(",",$_POST[Survey][modified_at]).")","order"=>"qq.qq_order,qq.qc_id ASC"));
			
				$group=Survey::model()->count(array("with"=>array("reg"),"condition"=>"DATE($type) between '".$_POST[Survey][sv_time]."' AND '".$_POST[Survey][create_at]."' AND qc_id='$cat->qc_id'"));

			}else{
				//kondisi tanggal mulai + selesai dengan kategori tanpa sub questioner
				$cat=QuestionCategory::model()->findbyPk($_POST['Survey']['qc_id']);
				$model->attributes=$_POST['Survey'];
				$model->sv_time=$_POST['Survey']['sv_time'];
				$model->create_at=$_POST['Survey']['create_at'];
				$model->qc_id=$_POST['Survey']['qc_id'];
				$title=$cat->qc_name."<br/>".Lib::dateInd($_POST[Survey][sv_time],false)." s.d ".Lib::dateInd($_POST[Survey][create_at],false)." (".$info.")";
				$survey=SurveyAnswer::model()->findAll(array("with"=>array("sv","sv.reg","qq"),"condition"=>"DATE($type) between '".$_POST[Survey][sv_time]."' AND '".$_POST[Survey][create_at]."' AND sv.qc_id='$model->qc_id'","order"=>"qq.qq_order,qq.qc_id ASC"));
				
				$group=Survey::model()->count(array("with"=>array("reg"),"condition"=>"DATE($type) between '".$_POST[Survey][sv_time]."' AND '".$_POST[Survey][create_at]."' AND qc_id='$cat->qc_id'"));
			}
		}
		$dataX=array();
		$alias=array();
		foreach($survey as $row){
			$dataX[$row->qq->qq_question][$row->qo->qo_name]++;	
		}
		$notation=1;
		$highcartLabel=array();
		$highcartSeries=array();
		$highcartSeriesValues=array();
		foreach($dataX as $index=>$val){
			$alias[$index]=$notation;
			$highcartLabel[]='"'.$index.'"';
			$notation++;
			$data[$index].="{ y: '".$alias[$index]."', ";
			
			$temp=array();
			foreach($val as $r=>$i){
				$temp[].=str_replace(" ","",$r).":'$i'";
				$detail[str_replace(" ","",$r)]=$r;
				$highcartSeries[$index][$r]=$i;
			}
			$data[$index].=implode(", ",$temp);
			$data[$index].='}';
		}
		
		foreach($detail as $dRow){
			$highcartSeriesValues[$dRow]="{name: \"$dRow\",data: [";
			$tempData=array();
			foreach($highcartLabel as $subRow){
				$tempData[]=($highcartSeries[str_replace('"',"",$subRow)][$dRow]=="")?0:$highcartSeries[str_replace('"',"",$subRow)][$dRow];
			}
			$highcartSeriesValues[$dRow].=implode(",",$tempData);
			$highcartSeriesValues[$dRow].="]}";
		}
		
		
		if(!isset($_POST['Survey'])){
			$this->render('rekapitulasi',array(
			'data'=>$data,"title"=>$title,'detail'=>$detail,"model"=>$model,"table"=>$dataX,"group"=>$group,"alias"=>$alias,'hLabel'=>$highcartLabel,'hSeries'=>$highcartSeries,'hValues'=>$highcartSeriesValues
			));
		}else{
			if($_POST['Survey']['reg_id']=="Screen"){
				$this->render('rekapitulasi',array(
					'data'=>$data,"title"=>$title,'detail'=>$detail,"model"=>$model,"table"=>$dataX,"group"=>$group,"alias"=>$alias,'hLabel'=>$highcartLabel,'hSeries'=>$highcartSeries,'hValues'=>$highcartSeriesValues
				));
			}else{
				$this->layout='//layouts/admin/empty';
				$this->render('rekapitulasi_a4',array(
					'data'=>$data,"title"=>$title,'detail'=>$detail,"model"=>$model,"table"=>$dataX,"group"=>$group,"alias"=>$alias,'hLabel'=>$highcartLabel,'hSeries'=>$highcartSeries,'hValues'=>$highcartSeriesValues
				));
			}
		}
	}
	
	
	public function actionView($id)
	{
		$reg=new Survey;
		if(isset($_POST['qq'])){
			foreach($_POST['qq'] as $index=>$row){
				if(!empty($row) AND !empty($row)){
					$answer=SurveyAnswer::model()->find(array("condition"=>"sv_id='$id' AND qq_id='$index'"));
					if(empty($answer)){
						$answer=new SurveyAnswer;
						$answer->sv_id=$id;
						$answer->qq_id=$index;
					}
					$answer->qo_id=$row;
					$answer->modified_at=date("Y-m-d H:i:s");
					$answer->save();
				}
			}
			$survey=$this->loadModel($id);
			$survey->sv_note=$_POST['note'];
			$survey->modified_at=date("Y-m-d H:i:s");
			if(isset($_POST['Finish'])){
				$survey->sv_status="Finish";
			}else{
				$survey->sv_status="Open";
			}
			if($_POST['Survey']['reg_is_out']){
				$nReg=Registration::model()->findbyPk($survey->reg_id);
				$nReg->reg_is_out=$_POST['Survey']['reg_is_out'];
				$nReg->save();
			}
			$survey->save();
		}
		
		
		
		$this->render('view',array(
		'model'=>$this->loadModel($id),"reg"=>$reg
		));
	}
	
	
	public function actionCreateQuestioner($id,$type=0)
	{
		$reg=new Survey;
		if($type==0){
			$cat=QuestionCategory::model()->find(array("condition"=>"qc_status='Active'"));
			$this->redirect(array('createQuestioner','id'=>$id,"type"=>$cat->qc_id));
		}
		if(isset($_POST['qq'])){
			$survey=new Survey;
			$survey->reg_id=$id;
			$survey->sv_time=date("Y-m-d H:i:s");
			if(isset($_POST['Finish'])){
				$survey->sv_status="Finish";
			}else{
				$survey->sv_status="Open";
			}
			
			if($_POST['Survey']['reg_is_out']){
				$nReg=Registration::model()->findbyPk($survey->reg_id);
				$nReg->reg_is_out=$_POST['Survey']['reg_is_out'];
				$nReg->save();
			}
			$survey->sv_note=$_POST['note'];
			$survey->qc_id=$_POST['qc_id'];
			$survey->create_at=date("Y-m-d H:i:s");
			$survey->modified_at=date("Y-m-d H:i:s");
			if($survey->save()){
				foreach($_POST['qq'] as $index=>$row){
					if(!empty($row)){
						$answer=new SurveyAnswer;
						$answer->sv_id=$survey->sv_id;
						$answer->qq_id=$index;
						$answer->qo_id=$row;
						$answer->create_at=date("Y-m-d H:i:s");
						$answer->modified_at=date("Y-m-d H:i:s");
						$answer->save();
					}
				}
				$survey->save();
				$this->redirect(array('view','id'=>$survey->sv_id));
			}
		}
		$this->render('createQuestioner',array("model"=>Registration::model()->findbyPk($id),"reg"=>$reg));
	}

	/**
	* Creates a new model.
	* If creation is successful, the browser will be redirected to the 'view' page.
	*/
	public function actionCreate()
	{
		$model=new Survey;

		// Uncomment the following line if AJAX validation is needed
		$this->performAjaxValidation($model);

		if(isset($_POST['Survey']))
		{
			$model->attributes=$_POST['Survey'];
			if($model->save()){
				$this->redirect(array('view','id'=>$model->sv_id));
			}
		}

		$this->render('create',array(
		'model'=>$model,
		));
	}
	
	
	public function actionCreateManual()
	{
		$model=new Survey;

		// Uncomment the following line if AJAX validation is needed
		$this->performAjaxValidation($model);

		if(isset($_POST['Survey']))
		{
			$model->attributes=$_POST['Survey'];
			$model->sv_person_name=$_POST['Survey']['sv_person_name'];
			$model->sv_person_number=$_POST['Survey']['sv_person_number'];
			$model->sv_status="Open";
			if($model->save()){
				$reg=Registration::model()->updateAll(array('reg_questioner' => 'Inactive'), "reg_questioner ='Active'");
				$this->redirect(array('manual'));
			}
		}

		$this->render('create_manual',array(
		'model'=>$model,
		));
	}

	/**
	* Updates a particular model.
	* If update is successful, the browser will be redirected to the 'view' page.
	* @param integer $id the ID of the model to be updated
	*/
	public function actionUpdate($id)
	{
		$model=$this->loadModel($id);

		// Uncomment the following line if AJAX validation is needed
		$this->performAjaxValidation($model);
	
		if(isset($_POST['Survey']))
		{
			$model->attributes=$_POST['Survey'];
			if($model->save()){
				$this->redirect(array('view','id'=>$model->sv_id));
			}
		}

		$this->render('update',array(
		'model'=>$model,
		));
	}

	/**
	* Deletes a particular model.
	* If deletion is successful, the browser will be redirected to the 'admin' page.
	* @param integer $id the ID of the model to be deleted
	*/
	public function actionDelete($id)
	{
		if(Yii::app()->request->isPostRequest)
		{
		// we only allow deletion via POST request	
		SurveyAnswer::model()->deleteAll(array('condition'=>"sv_id='$id'"));
		$this->loadModel($id)->delete();

		// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
		if(!isset($_GET['ajax']))
		$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
		}
		else
		throw new CHttpException(400,'Invalid request. Please do not repeat this request again.');
	}

	/**
	* Lists all models.
	*/
	public function actionAdmin()
	{
		$dataProvider=new CActiveDataProvider('Survey');
		$this->render('index',array(
		'dataProvider'=>$dataProvider,
		));
	}

	/**
	* Manages all models.
	*/
	public function actionIndex()
	{
		$model=new Survey('searchNonCreate');
		$model->unsetAttributes();  // clear any default values
		if(isset($_GET['Survey']))
		$model->attributes=$_GET['Survey'];
		if (isset($_GET['pageSize'])) {
			Yii::app()->user->setState('pageSize',(int)$_GET['pageSize']); 
			unset($_GET['pageSize']);
		}
		$this->render('admin',array(
		'model'=>$model,
		));
	}
	
	public function actionManual()
	{
		$model=new Survey('searchManual');
		$model->unsetAttributes();  // clear any default values
		$model->reg_id='';  // clear any default values
		if(isset($_GET['Survey']))
		$model->attributes=$_GET['Survey'];
		if (isset($_GET['pageSize'])) {
			Yii::app()->user->setState('pageSize',(int)$_GET['pageSize']); 
			unset($_GET['pageSize']);
		}
		$this->render('admin_list_manual',array(
		'model'=>$model,
		));
	}
	
	
	public function actionQuestionerList()
	{
		$model=new Survey('search');
		$model->unsetAttributes();  // clear any default values
		if(isset($_GET['Survey']))
		$model->attributes=$_GET['Survey'];
		if (isset($_GET['pageSize'])) {
			Yii::app()->user->setState('pageSize',(int)$_GET['pageSize']); 
			unset($_GET['pageSize']);
		}
		$this->render('admin_list',array(
		'model'=>$model,
		));
	}
	
	public function actionBatchDelete()
	{
		$request = Yii::app()->getRequest();
		if($request->getIsPostRequest()){
			if(isset($_POST['ids'])){
				$ids = $_POST['ids'];
			}
			if (empty($ids)) {
				echo CJSON::encode(array('status' => 'failure', 'msg' => 'you should at least choice one item'));
				die();
			}
			$successCount = $failureCount = 0;
			$dataError=array();
			foreach ($ids as $id) {
				$model = $this->loadModel($id);
				$temp = $this->loadModel($id);
				if($model->delete()){
					$successCount++ ;
				}else{
					$failureCount++;
					$dataError[]=$model->sv_id;
				}
			}
			echo CJSON::encode(array('status' => 'success',
				'data' => array(
					'successCount' => $successCount,
					'failureCount' => $failureCount,
					'dataError' => $dataError,
				)));
			die();
		}else{
			throw new CHttpException(400,'Invalid request. Please do not repeat this request again.');
		}
	}
	
	public function actionExportExcel()
	{
		ini_set("memory_limit","8056M");
		set_time_limit(100000);
		Yii::import('ext.phpexcel.XPHPExcel');    
		$objPHPExcel= XPHPExcel::createPHPExcel();
		$objPHPExcel->getProperties()->setCreator("File")
		->setLastModifiedBy("File")
		->setTitle("Dokumen")
		->setSubject("Dokumen")
		->setDescription("File")
		->setKeywords("File")
		->setCategory("File");
		$model=new Survey;

		$objPHPExcel->setActiveSheetIndex(0)->setCellValue('A1',"No");
		$objPHPExcel->setActiveSheetIndex(0)->setCellValue('B1',$model->getAttributeLabel('sv_id'));
		$objPHPExcel->setActiveSheetIndex(0)->setCellValue('C1',$model->getAttributeLabel('reg_id'));
		$objPHPExcel->setActiveSheetIndex(0)->setCellValue('D1',$model->getAttributeLabel('sv_time'));
		$objPHPExcel->setActiveSheetIndex(0)->setCellValue('E1',$model->getAttributeLabel('sv_status'));
		$objPHPExcel->setActiveSheetIndex(0)->setCellValue('F1',$model->getAttributeLabel('create_at'));
		$objPHPExcel->setActiveSheetIndex(0)->setCellValue('G1',$model->getAttributeLabel('modified_at'));
		
		$criteria=new CDbCriteria;
		$criteria->addCondition("sv_id IN (".$_POST['ids'].")");
		$data= Survey::model()->findAll($criteria);    
		$j=1;
		$no=0;
		foreach($data as $row) {
			$j++;
			$no++;
			$objPHPExcel->setActiveSheetIndex(0)->setCellValue('A'.$j, $no);
			$objPHPExcel->setActiveSheetIndex(0)->setCellValue('B'.$j, $row->sv_id);
			$objPHPExcel->setActiveSheetIndex(0)->setCellValue('C'.$j, $row->reg_id);
			$objPHPExcel->setActiveSheetIndex(0)->setCellValue('D'.$j, $row->sv_time);
			$objPHPExcel->setActiveSheetIndex(0)->setCellValue('E'.$j, $row->sv_status);
			$objPHPExcel->setActiveSheetIndex(0)->setCellValue('F'.$j, $row->create_at);
			$objPHPExcel->setActiveSheetIndex(0)->setCellValue('G'.$j, $row->modified_at);
		}
		$styleArray = array(
			'font' => array(
				'bold' => true,
			),
		);
		$styleArrayData = array(
			'borders' => array(
				'allborders' => array(
					'style' => PHPExcel_Style_Border::BORDER_THIN
				),
			),
		);
		$objPHPExcel->getActiveSheet()->getStyle('A1:G7')->applyFromArray($styleArray);
		$objPHPExcel->getActiveSheet()->getStyle('A1:G'.$j)->applyFromArray($styleArrayData);
		// Rename worksheet
		$objPHPExcel->getActiveSheet()->setTitle('Default');

		// Set active sheet index to the first sheet, so Excel opens this as the first sheet
		$objPHPExcel->setActiveSheetIndex(0);
		 		 
		// Redirect output to a client�s web browser (Excel5)
		header('Content-Type: application/vnd.ms-excel');
		header('Content-Disposition: attachment;filename="Template Survey '.date('Y-m-d H:i:s').'.xls"');
		header('Cache-Control: max-age=0');
		// If you're serving to IE 9, then the following may be needed
		header('Cache-Control: max-age=1');
		 
		// If you're serving to IE over SSL, then the following may be needed
		header ('Expires: Mon, 26 Jul 1997 05:00:00 GMT'); // Date in the past
		header ('Last-Modified: '.gmdate('D, d M Y H:i:s').' GMT'); // always modified
		header ('Cache-Control: cache, must-revalidate'); // HTTP/1.1
		header ('Pragma: public'); // HTTP/1.0
		 
		$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
		$objWriter->save('php://output');
	    Yii::app()->end();
	}
	
	public function actionExportPdf()
	{
		Yii::import('ext.MPDF57.*');
		include("mpdf.php");
		ini_set("memory_limit","8056M");
		set_time_limit(100000);
		$mpdf=new mPDF('utf-8', 'A4-L', 0, null, 3,3,10,20,15,15 );
		
		$model=new Survey;
		
		$criteria=new CDbCriteria;
		$criteria->addCondition("sv_id IN (".$_POST['ids'].")");
		$data= Survey::model()->findAll($criteria); 
		
		$j = 1;
		$no=0;
		
		$html='
		<style type="text/css" media="print,screen" >
		th {
			font-family:Arial;
			color:black;
			background-color:lightgrey;
			padding:5px;
		}
		td{
			padding:5px;
		}
		thead {
			display:table-header-group;
		}
		tbody {
			display:table-row-group;
		}
		</style>
		<h2 style="text-align: center;">Survey</h2>
		<br>
		<table border="1" style="border:1px; width:100%; vertical-align: text-top;border-collapse: collapse;">
			<thead>
				<tr>
					<th>No</th>
										<th>'.$model->getAttributeLabel('sv_id').'</th>
					<th>'.$model->getAttributeLabel('reg_id').'</th>
					<th>'.$model->getAttributeLabel('sv_time').'</th>
					<th>'.$model->getAttributeLabel('sv_status').'</th>
					<th>'.$model->getAttributeLabel('create_at').'</th>
					<th>'.$model->getAttributeLabel('modified_at').'</th>
				</tr>
			</thead>
			<tbody>';
			
			$no=0;
			foreach($data as $row){
			$no++;
			$html.='
				<tr>
					<td>'.$no.'</td>
										<td>'.$row->sv_id.'</td>
					<td>'.$row->reg_id.'</td>
					<td>'.$row->sv_time.'</td>
					<td>'.$row->sv_status.'</td>
					<td>'.$row->create_at.'</td>
					<td>'.$row->modified_at.'</td>
				</tr>';
			}	
			$html.='
			</tbody>
		</table>	
		';
		ini_set("memory_limit","256M");
		$mpdf->Bookmark('Start of the document');
		$mpdf->SetDisplayMode('fullpage','two');
		$mpdf->setFooter('Tanggal {DATE j-m-Y}||Halaman {PAGENO} dari {nbpg}') ;
		$mpdf->simpleTables = true;
		$mpdf->WriteHTML('<sheet-size="Letter" />');
		$mpdf->WriteHTML($html);
		$mpdf->Output();
	}
	
	public function actionImport()
	{
		ini_set("memory_limit","8056M");
		set_time_limit(100000);
		$model=new Survey;
		if(isset($_POST['Survey']))
		{
		$fileUpload=CUploadedFile::getInstance($model,'import');
		$nama=date('YmdHis').'_'.$fileUpload;
		$path=Yii::app()->basePath . '/../temp_excel/'.$nama.'';
		$fileUpload->saveAs($path);
			
		Yii::import('ext.phpexcelreader.JPhpExcelReader');
		$data=new JPhpExcelReader($path);
		$baris=$data->sheets[0][numRows];
		$kolom=$data->sheets[0][numCols];
		$sukses=0;
		$gagal=0;
		for($i=2;$i<=$baris; $i++){
			$model=new Survey;
						$model->sv_id= $data->sheets[0]['cells'][$i][1];
			$model->reg_id= $data->sheets[0]['cells'][$i][2];
			$model->sv_time= $data->sheets[0]['cells'][$i][3];
			$model->sv_status= $data->sheets[0]['cells'][$i][4];
			$cek=Survey::model()->findbyPk($data->sheets[0]['cells'][$i][1]);
			if(count($cek)==0){
				if ($model->save()){
				$sukses++;
				}else{
				$gagal++;
				}
			} else {
				$gagal++;
			}
		}
		@unlink($path);
		 Yii::app()->user->setFlash('success', "Proses import data selesai<br/>
         Jumlah data yang sukses diimport : ".$sukses." Jumlah data yang gagal diimport : ".$gagal."
         ");
		$this->redirect(array('index'));
		}
	}
	
	public function actionDownloadTemplate(){
		Yii::import('ext.phpexcel.XPHPExcel');    
		$objPHPExcel= XPHPExcel::createPHPExcel();
		$objPHPExcel->getProperties()->setCreator("File")
		->setLastModifiedBy("File")
		->setTitle("Dokumen Rahasia")
		->setSubject("Dokumen Rahasia")
		->setDescription("File")
		->setKeywords("File")
		->setCategory("File");
		$model=new Survey;
		//loop here		
				$objPHPExcel->setActiveSheetIndex(0)->setCellValue('A1',$model->getAttributeLabel('sv_id'));
		$objPHPExcel->setActiveSheetIndex(0)->setCellValue('B1',$model->getAttributeLabel('reg_id'));
		$objPHPExcel->setActiveSheetIndex(0)->setCellValue('C1',$model->getAttributeLabel('sv_time'));
		$objPHPExcel->setActiveSheetIndex(0)->setCellValue('D1',$model->getAttributeLabel('sv_status'));
		
		$data= Survey::model()->findAll(array('limit'=>10));    
		$j = 1;
		$no=0;
		foreach($data as $row) {
			$j++;
			$no++;
						$objPHPExcel->setActiveSheetIndex(0)->setCellValue('A'.$j, $row->sv_id);
			$objPHPExcel->setActiveSheetIndex(0)->setCellValue('B'.$j, $row->reg_id);
			$objPHPExcel->setActiveSheetIndex(0)->setCellValue('C'.$j, $row->sv_time);
			$objPHPExcel->setActiveSheetIndex(0)->setCellValue('D'.$j, $row->sv_status);
		}
		$styleArray = array(
			'font' => array(
				'bold' => true,
			),
		);
		$styleArrayData = array(
			'borders' => array(
				'allborders' => array(
					'style' => PHPExcel_Style_Border::BORDER_THIN
				),
			),
		);
		$objPHPExcel->getActiveSheet()->getStyle('A1:D4')->applyFromArray($styleArray);
		$objPHPExcel->getActiveSheet()->getStyle('A1:D'.$j)->applyFromArray($styleArrayData);
		// Rename worksheet
		$objPHPExcel->getActiveSheet()->setTitle('Default');

		// Set active sheet index to the first sheet, so Excel opens this as the first sheet
		$objPHPExcel->setActiveSheetIndex(0);
		 		 
		// Redirect output to a client�s web browser (Excel5)
		header('Content-Type: application/vnd.ms-excel');
		header('Content-Disposition: attachment;filename="Template Survey '.date('Y-m-d H:i:s').'.xls"');
		header('Cache-Control: max-age=0');
		// If you're serving to IE 9, then the following may be needed
		header('Cache-Control: max-age=1');
		 
		// If you're serving to IE over SSL, then the following may be needed
		header ('Expires: Mon, 26 Jul 1997 05:00:00 GMT'); // Date in the past
		header ('Last-Modified: '.gmdate('D, d M Y H:i:s').' GMT'); // always modified
		header ('Cache-Control: cache, must-revalidate'); // HTTP/1.1
		header ('Pragma: public'); // HTTP/1.0
		 
		$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
		$objWriter->save('php://output');
	    Yii::app()->end();
	}
	

	public function loadModel($id)
	{
		$model=Survey::model()->findByPk($id);
		if($model===null)
		throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}

	/**
	* Performs the AJAX validation.
	* @param CModel the model to be validated
	*/
	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='survey-form')
		{
		echo CActiveForm::validate($model);
		Yii::app()->end();
		}
	}
}
