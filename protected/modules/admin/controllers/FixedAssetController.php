<?php
class FixedAssetController extends Controller
{
	/**
	* @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	* using two-column layout. See 'protected/views/layouts/column2.php'.
	*/
	public $layout='//layouts/admin/main';

	/**
	* @return array action filters
	*/
	public function filters()
	{
		return array(
		'accessControl', // perform access control for CRUD operations
		);
	}

	/**
	* Specifies the access control rules.
	* This method is used by the 'accessControl' filter.
	* @return array access control rules
	*/
	public function accessRules()
	{
		return array(
		array('allow',  // allow all users to perform 'index' and 'view' actions
		'actions'=>array('index','view'),
		'expression'=>'$user->getAuth()',
		),
		array('allow', // allow admin user to perform 'admin' and 'delete' actions
		'actions'=>array('create','update','admin','delete','BatchDelete','exportExcel','exportPdf','import','downloadTemplate','printLabel'),
		'expression'=>'$user->getAuth()',
		),

		array('deny',  // deny all users
		'users'=>array('*'),
		),
		);
	}

	/**
	* Displays a particular model.
	* @param integer $id the ID of the model to be displayed
	*/
	public function actionView($id)
	{
		$this->render('view',array(
		'model'=>$this->loadModel($id),
		));
	}

	/**
	* Creates a new model.
	* If creation is successful, the browser will be redirected to the 'view' page.
	*/
	public function actionCreate()
	{
		$model=new FixedAsset;

		// Uncomment the following line if AJAX validation is needed
		$this->performAjaxValidation($model);

		if(isset($_POST['FixedAsset']))
		{
			$model->attributes=$_POST['FixedAsset'];
			if($model->save()){
				$this->redirect(array('view','id'=>$model->fa_id));
			}
		}

		$this->render('create',array(
		'model'=>$model,
		));
	}

	/**
	* Updates a particular model.
	* If update is successful, the browser will be redirected to the 'view' page.
	* @param integer $id the ID of the model to be updated
	*/
	public function actionUpdate($id)
	{
		$model=$this->loadModel($id);

		// Uncomment the following line if AJAX validation is needed
		$this->performAjaxValidation($model);
	
		if(isset($_POST['FixedAsset']))
		{
			$model->attributes=$_POST['FixedAsset'];
			if($model->save()){
				$this->redirect(array('view','id'=>$model->fa_id));
			}
		}

		$this->render('update',array(
		'model'=>$model,
		));
	}

	/**
	* Deletes a particular model.
	* If deletion is successful, the browser will be redirected to the 'admin' page.
	* @param integer $id the ID of the model to be deleted
	*/
	public function actionDelete($id)
	{
		if(Yii::app()->request->isPostRequest)
		{
		// we only allow deletion via POST request		
		$this->loadModel($id)->delete();

		// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
		if(!isset($_GET['ajax']))
		$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
		}
		else
		throw new CHttpException(400,'Invalid request. Please do not repeat this request again.');
	}

	/**
	* Lists all models.
	*/
	public function actionAdmin()
	{
		$dataProvider=new CActiveDataProvider('FixedAsset');
		$this->render('index',array(
		'dataProvider'=>$dataProvider,
		));
	}

	/**
	* Manages all models.
	*/
	public function actionIndex()
	{
		$model=new FixedAsset('search');
		$model->unsetAttributes();  // clear any default values
		if(isset($_GET['FixedAsset']))
		$model->attributes=$_GET['FixedAsset'];
		if (isset($_GET['pageSize'])) {
			Yii::app()->user->setState('pageSize',(int)$_GET['pageSize']); 
			unset($_GET['pageSize']);
		}
		$this->render('admin',array(
		'model'=>$model,
		));
	}
	
	public function actionBatchDelete()
	{
		$request = Yii::app()->getRequest();
		if($request->getIsPostRequest()){
			if(isset($_POST['ids'])){
				$ids = $_POST['ids'];
			}
			if (empty($ids)) {
				echo CJSON::encode(array('status' => 'failure', 'msg' => 'you should at least choice one item'));
				die();
			}
			$successCount = $failureCount = 0;
			$dataError=array();
			foreach ($ids as $id) {
				$model = $this->loadModel($id);
				$temp = $this->loadModel($id);
				if($model->delete()){
					$successCount++ ;
				}else{
					$failureCount++;
					$dataError[]=$model->fa_id;
				}
			}
			echo CJSON::encode(array('status' => 'success',
				'data' => array(
					'successCount' => $successCount,
					'failureCount' => $failureCount,
					'dataError' => $dataError,
				)));
			die();
		}else{
			throw new CHttpException(400,'Invalid request. Please do not repeat this request again.');
		}
	}
	
	public function actionExportExcel()
	{
		ini_set("memory_limit","8056M");
		set_time_limit(100000);
		Yii::import('ext.phpexcel.XPHPExcel');    
		$objPHPExcel= XPHPExcel::createPHPExcel();
		$objPHPExcel->getProperties()->setCreator("File")
		->setLastModifiedBy("File")
		->setTitle("Dokumen")
		->setSubject("Dokumen")
		->setDescription("File")
		->setKeywords("File")
		->setCategory("File");
		$model=new FixedAsset;

		$objPHPExcel->setActiveSheetIndex(0)->setCellValue('A1',"No");
		$objPHPExcel->setActiveSheetIndex(0)->setCellValue('B1',$model->getAttributeLabel('fa_id'));
		$objPHPExcel->setActiveSheetIndex(0)->setCellValue('C1',$model->getAttributeLabel('fa_jenis'));
		$objPHPExcel->setActiveSheetIndex(0)->setCellValue('D1',$model->getAttributeLabel('fa_merk'));
		$objPHPExcel->setActiveSheetIndex(0)->setCellValue('E1',$model->getAttributeLabel('fa_tipe'));
		$objPHPExcel->setActiveSheetIndex(0)->setCellValue('F1',$model->getAttributeLabel('fa_unit'));
		$objPHPExcel->setActiveSheetIndex(0)->setCellValue('G1',$model->getAttributeLabel('fa_lantai'));
		$objPHPExcel->setActiveSheetIndex(0)->setCellValue('H1',$model->getAttributeLabel('fa_ruang'));
		$objPHPExcel->setActiveSheetIndex(0)->setCellValue('I1',$model->getAttributeLabel('fa_harga_dasar'));
		$objPHPExcel->setActiveSheetIndex(0)->setCellValue('J1',$model->getAttributeLabel('fa_harga_perolehan'));
		$objPHPExcel->setActiveSheetIndex(0)->setCellValue('K1',$model->getAttributeLabel('fa_tgl_pembukuan'));
		$objPHPExcel->setActiveSheetIndex(0)->setCellValue('L1',$model->getAttributeLabel('fa_tgl_efektif'));
		
		$criteria=new CDbCriteria;
		$dIn=array();
		$ex=explode(",",$_POST['ids']);
		foreach($ex as $d){
			$dIn[]="'$d'";
		}
		
		$in=implode(",",$dIn);
		$criteria->addCondition("fa_id IN (".$in.")");
		$data= FixedAsset::model()->findAll($criteria);    
		$j=1;
		$no=0;
		foreach($data as $row) {
			$j++;
			$no++;
			$objPHPExcel->setActiveSheetIndex(0)->setCellValue('A'.$j, $no);
			$objPHPExcel->setActiveSheetIndex(0)->setCellValue('B'.$j, $row->fa_id);
			$objPHPExcel->setActiveSheetIndex(0)->setCellValue('C'.$j, $row->fa_jenis);
			$objPHPExcel->setActiveSheetIndex(0)->setCellValue('D'.$j, $row->fa_merk);
			$objPHPExcel->setActiveSheetIndex(0)->setCellValue('E'.$j, $row->fa_tipe);
			$objPHPExcel->setActiveSheetIndex(0)->setCellValue('F'.$j, $row->fa_unit);
			$objPHPExcel->setActiveSheetIndex(0)->setCellValue('G'.$j, $row->fa_lantai);
			$objPHPExcel->setActiveSheetIndex(0)->setCellValue('H'.$j, $row->fa_ruang);
			$objPHPExcel->setActiveSheetIndex(0)->setCellValue('I'.$j, $row->fa_harga_dasar);
			$objPHPExcel->setActiveSheetIndex(0)->setCellValue('J'.$j, $row->fa_harga_perolehan);
			$objPHPExcel->setActiveSheetIndex(0)->setCellValue('K'.$j, $row->fa_tgl_pembukuan);
			$objPHPExcel->setActiveSheetIndex(0)->setCellValue('L'.$j, $row->fa_tgl_efektif);
		}
		$styleArray = array(
			'font' => array(
				'bold' => true,
			),
		);
		$styleArrayData = array(
			'borders' => array(
				'allborders' => array(
					'style' => PHPExcel_Style_Border::BORDER_THIN
				),
			),
		);
		$objPHPExcel->getActiveSheet()->getStyle('A1:L12')->applyFromArray($styleArray);
		$objPHPExcel->getActiveSheet()->getStyle('A1:L'.$j)->applyFromArray($styleArrayData);
		// Rename worksheet
		$objPHPExcel->getActiveSheet()->setTitle('Default');

		// Set active sheet index to the first sheet, so Excel opens this as the first sheet
		$objPHPExcel->setActiveSheetIndex(0);
		 		 
		// Redirect output to a client�s web browser (Excel5)
		header('Content-Type: application/vnd.ms-excel');
		header('Content-Disposition: attachment;filename="Template Fixed Asset '.date('Y-m-d H:i:s').'.xls"');
		header('Cache-Control: max-age=0');
		// If you're serving to IE 9, then the following may be needed
		header('Cache-Control: max-age=1');
		 
		// If you're serving to IE over SSL, then the following may be needed
		header ('Expires: Mon, 26 Jul 1997 05:00:00 GMT'); // Date in the past
		header ('Last-Modified: '.gmdate('D, d M Y H:i:s').' GMT'); // always modified
		header ('Cache-Control: cache, must-revalidate'); // HTTP/1.1
		header ('Pragma: public'); // HTTP/1.0
		 
		$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
		$objWriter->save('php://output');
	    Yii::app()->end();
	}
	
	public function actionExportPdf()
	{
		Yii::import('ext.MPDF57.*');
		include("mpdf.php");
		ini_set("memory_limit","8056M");
		set_time_limit(100000);
		$mpdf=new mPDF('utf-8', 'A4-L', 0, null, 3,3,10,20,15,15 );
		
		$model=new FixedAsset;
		
		$criteria=new CDbCriteria;
		$dIn=array();
		$ex=explode(",",$_POST['ids']);
		foreach($ex as $d){
			$dIn[]="'$d'";
		}
		
		$in=implode(",",$dIn);
		$criteria->addCondition("fa_id IN (".$in.")");
		$data= FixedAsset::model()->findAll($criteria); 
		
		$j = 1;
		$no=0;
		
		$html='
		<style type="text/css" media="print,screen" >
		th {
			font-family:Arial;
			color:black;
			background-color:lightgrey;
			padding:5px;
		}
		td{
			padding:5px;
		}
		thead {
			display:table-header-group;
		}
		tbody {
			display:table-row-group;
		}
		</style>
		<h2 style="text-align: center;">Fixed Asset</h2>
		<br>
		<table border="1" style="border:1px; width:100%; vertical-align: text-top;border-collapse: collapse;">
			<thead>
				<tr>
					<th>No</th>
										<th>'.$model->getAttributeLabel('fa_id').'</th>
					<th>'.$model->getAttributeLabel('fa_jenis').'</th>
					<th>'.$model->getAttributeLabel('fa_merk').'</th>
					<th>'.$model->getAttributeLabel('fa_tipe').'</th>
					<th>'.$model->getAttributeLabel('fa_unit').'</th>
					<th>'.$model->getAttributeLabel('fa_lantai').'</th>
					<th>'.$model->getAttributeLabel('fa_ruang').'</th>
					<th>'.$model->getAttributeLabel('fa_harga_dasar').'</th>
					<th>'.$model->getAttributeLabel('fa_harga_perolehan').'</th>
					<th>'.$model->getAttributeLabel('fa_tgl_pembukuan').'</th>
					<th>'.$model->getAttributeLabel('fa_tgl_efektif').'</th>
				</tr>
			</thead>
			<tbody>';
			
			$no=0;
			foreach($data as $row){
			$no++;
			$html.='
				<tr>
					<td>'.$no.'</td>
										<td>'.$row->fa_id.'</td>
					<td>'.$row->fa_jenis.'</td>
					<td>'.$row->fa_merk.'</td>
					<td>'.$row->fa_tipe.'</td>
					<td>'.$row->fa_unit.'</td>
					<td>'.$row->fa_lantai.'</td>
					<td>'.$row->fa_ruang.'</td>
					<td>'.$row->fa_harga_dasar.'</td>
					<td>'.$row->fa_harga_perolehan.'</td>
					<td>'.$row->fa_tgl_pembukuan.'</td>
					<td>'.$row->fa_tgl_efektif.'</td>
				</tr>';
			}	
			$html.='
			</tbody>
		</table>	
		';
		ini_set("memory_limit","256M");
		$mpdf->Bookmark('Start of the document');
		$mpdf->SetDisplayMode('fullpage','two');
		$mpdf->setFooter('Tanggal {DATE j-m-Y}||Halaman {PAGENO} dari {nbpg}') ;
		$mpdf->simpleTables = true;
		$mpdf->WriteHTML('<sheet-size="Letter" />');
		$mpdf->WriteHTML($html);
		$mpdf->Output();
	}
	
	public function actionPrintLabel()
	{		
		$criteria=new CDbCriteria;
		$dIn=array();
		$ex=explode(",",$_POST['ids']);
		foreach($ex as $d){
			$dIn[]="'$d'";
		}
		
		$in=implode(",",$dIn);
		$criteria->addCondition("fa_id IN (".$in.")");
		$data= FixedAsset::model()->findAll($criteria); 
		
		$this->layout='//layouts/admin/blank';
		$this->render('print_label',array(
		'model'=>$data,
		));
		
	}
	
	public function actionImport()
	{
		ini_set("memory_limit","8056M");
		set_time_limit(100000);
		$model=new FixedAsset;
		if(isset($_POST['FixedAsset']))
		{
		$fileUpload=CUploadedFile::getInstance($model,'import');
		$nama=date('YmdHis').'_'.$fileUpload;
		$path=Yii::app()->basePath . '/../temp_excel/'.$nama.'';
		$fileUpload->saveAs($path);
			
		Yii::import('ext.phpexcelreader.JPhpExcelReader');
		$data=new JPhpExcelReader($path);
		$baris=$data->sheets[0][numRows];
		$kolom=$data->sheets[0][numCols];
		$sukses=0;
		$gagal=0;
		$rError=array();
		$rError=array();
		for($i=2;$i<=$baris; $i++){
			$model=new FixedAsset;
			$model->fa_id= trim($data->sheets[0]['cells'][$i][1]);
			$model->fa_jenis= trim($data->sheets[0]['cells'][$i][2]);
			$model->fa_merk= trim($data->sheets[0]['cells'][$i][3]);
			$model->fa_tipe= trim($data->sheets[0]['cells'][$i][4]);
			$model->fa_unit= trim($data->sheets[0]['cells'][$i][5]);
			$model->fa_lantai= trim($data->sheets[0]['cells'][$i][6]);
			$model->fa_ruang= trim($data->sheets[0]['cells'][$i][7]);
			$model->fa_harga_dasar= trim($data->sheets[0]['cells'][$i][8]);
			$model->fa_harga_perolehan= trim($data->sheets[0]['cells'][$i][9]);
			$model->fa_tgl_pembukuan= trim($data->sheets[0]['cells'][$i][10]);
			$model->fa_tgl_efektif= trim($data->sheets[0]['cells'][$i][11]);
			$cek=FixedAsset::model()->findbyPk(trim($data->sheets[0]['cells'][$i][1]));
			if(count($cek)==0){
				if ($model->save()){
				$sukses++;
				}else{
				$gagal++;
				$rError[]=CActiveForm::validate($model)."[".$data->sheets[0]['cells'][$i][1]."]";
				}
			} else {
				$gagal++;
				$rError[]=$cek->fa_id;
			}
		}
		@unlink($path);
		 Yii::app()->user->setFlash('success', "Proses import data selesai<br/>
         Jumlah data yang sukses diimport : ".$sukses." Jumlah data yang gagal diimport : ".$gagal." <br/>".implode(" <br/> ",$rError)."
         ");
		$this->redirect(array('index'));
		}
	}
	
	public function actionDownloadTemplate(){
		Yii::import('ext.phpexcel.XPHPExcel');    
		$objPHPExcel= XPHPExcel::createPHPExcel();
		$objPHPExcel->getProperties()->setCreator("File")
		->setLastModifiedBy("File")
		->setTitle("Dokumen Rahasia")
		->setSubject("Dokumen Rahasia")
		->setDescription("File")
		->setKeywords("File")
		->setCategory("File");
		$model=new FixedAsset;
		//loop here		
				$objPHPExcel->setActiveSheetIndex(0)->setCellValue('A1',$model->getAttributeLabel('fa_id'));
		$objPHPExcel->setActiveSheetIndex(0)->setCellValue('B1',$model->getAttributeLabel('fa_jenis'));
		$objPHPExcel->setActiveSheetIndex(0)->setCellValue('C1',$model->getAttributeLabel('fa_merk'));
		$objPHPExcel->setActiveSheetIndex(0)->setCellValue('D1',$model->getAttributeLabel('fa_tipe'));
		$objPHPExcel->setActiveSheetIndex(0)->setCellValue('E1',$model->getAttributeLabel('fa_unit'));
		$objPHPExcel->setActiveSheetIndex(0)->setCellValue('F1',$model->getAttributeLabel('fa_lantai'));
		$objPHPExcel->setActiveSheetIndex(0)->setCellValue('G1',$model->getAttributeLabel('fa_ruang'));
		$objPHPExcel->setActiveSheetIndex(0)->setCellValue('H1',$model->getAttributeLabel('fa_harga_dasar'));
		$objPHPExcel->setActiveSheetIndex(0)->setCellValue('I1',$model->getAttributeLabel('fa_harga_perolehan'));
		$objPHPExcel->setActiveSheetIndex(0)->setCellValue('J1',$model->getAttributeLabel('fa_tgl_pembukuan'));
		$objPHPExcel->setActiveSheetIndex(0)->setCellValue('K1',$model->getAttributeLabel('fa_tgl_efektif'));
		
		$data= FixedAsset::model()->findAll(array('limit'=>10));    
		$j = 1;
		$no=0;
		foreach($data as $row) {
			$j++;
			$no++;
						$objPHPExcel->setActiveSheetIndex(0)->setCellValue('A'.$j, $row->fa_id);
			$objPHPExcel->setActiveSheetIndex(0)->setCellValue('B'.$j, $row->fa_jenis);
			$objPHPExcel->setActiveSheetIndex(0)->setCellValue('C'.$j, $row->fa_merk);
			$objPHPExcel->setActiveSheetIndex(0)->setCellValue('D'.$j, $row->fa_tipe);
			$objPHPExcel->setActiveSheetIndex(0)->setCellValue('E'.$j, $row->fa_unit);
			$objPHPExcel->setActiveSheetIndex(0)->setCellValue('F'.$j, $row->fa_lantai);
			$objPHPExcel->setActiveSheetIndex(0)->setCellValue('G'.$j, $row->fa_ruang);
			$objPHPExcel->setActiveSheetIndex(0)->setCellValue('H'.$j, $row->fa_harga_dasar);
			$objPHPExcel->setActiveSheetIndex(0)->setCellValue('I'.$j, $row->fa_harga_perolehan);
			$objPHPExcel->setActiveSheetIndex(0)->setCellValue('J'.$j, $row->fa_tgl_pembukuan);
			$objPHPExcel->setActiveSheetIndex(0)->setCellValue('K'.$j, $row->fa_tgl_efektif);
		}
		$styleArray = array(
			'font' => array(
				'bold' => true,
			),
		);
		$styleArrayData = array(
			'borders' => array(
				'allborders' => array(
					'style' => PHPExcel_Style_Border::BORDER_THIN
				),
			),
		);
		$objPHPExcel->getActiveSheet()->getStyle('A1:K11')->applyFromArray($styleArray);
		$objPHPExcel->getActiveSheet()->getStyle('A1:K'.$j)->applyFromArray($styleArrayData);
		// Rename worksheet
		$objPHPExcel->getActiveSheet()->setTitle('Default');

		// Set active sheet index to the first sheet, so Excel opens this as the first sheet
		$objPHPExcel->setActiveSheetIndex(0);
		 		 
		// Redirect output to a client�s web browser (Excel5)
		header('Content-Type: application/vnd.ms-excel');
		header('Content-Disposition: attachment;filename="Template Fixed Asset '.date('Y-m-d H:i:s').'.xls"');
		header('Cache-Control: max-age=0');
		// If you're serving to IE 9, then the following may be needed
		header('Cache-Control: max-age=1');
		 
		// If you're serving to IE over SSL, then the following may be needed
		header ('Expires: Mon, 26 Jul 1997 05:00:00 GMT'); // Date in the past
		header ('Last-Modified: '.gmdate('D, d M Y H:i:s').' GMT'); // always modified
		header ('Cache-Control: cache, must-revalidate'); // HTTP/1.1
		header ('Pragma: public'); // HTTP/1.0
		 
		$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
		$objWriter->save('php://output');
	    Yii::app()->end();
	}
	

	public function loadModel($id)
	{
		$model=FixedAsset::model()->findByPk($id);
		if($model===null)
		throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}

	/**
	* Performs the AJAX validation.
	* @param CModel the model to be validated
	*/
	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='fixed-asset-form')
		{
		echo CActiveForm::validate($model);
		Yii::app()->end();
		}
	}
}
