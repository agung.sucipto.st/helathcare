<?php
class HelpDeskController extends Controller
{
	/**
	* @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	* using two-column layout. See 'protected/views/layouts/column2.php'.
	*/
	public $layout='//layouts/admin/main';

	/**
	* @return array action filters
	*/
	public function filters()
	{
		return array(
		'accessControl', // perform access control for CRUD operations
		);
	}

	/**
	* Specifies the access control rules.
	* This method is used by the 'accessControl' filter.
	* @return array access control rules
	*/
	public function accessRules()
	{
		return array(
		array('allow',  // allow all users to perform 'index' and 'view' actions
		'actions'=>array('index','view'),
		'expression'=>'$user->getAuth()',
		),
		array('allow', // allow admin user to perform 'admin' and 'delete' actions
		'actions'=>array('create','update','admin','delete','BatchDelete','exportExcel','exportPdf','import','downloadTemplate','print','grafik'),
		'expression'=>'$user->getAuth()',
		),

		array('deny',  // deny all users
		'users'=>array('*'),
		),
		);
	}
	
	public function actionPrint()
	{		
		$criteria=new CDbCriteria;
		$dIn=array();
		$ex=explode(",",$_POST['ids']);
		foreach($ex as $d){
			$dIn[]="'$d'";
		}
		
		$in=implode(",",$dIn);
		$criteria->addCondition("hdsk_id IN (".$in.")");
		$data= HelpDesk::model()->findAll($criteria); 
		
		$this->layout='//layouts/admin/blank';
		$this->render('print_helpdesk',array(
		'model'=>$data,
		));
		
	}
	
	public function actionGrafik(){
		$model=new HelpDesk();
		$helpdesk=HelpDesk::model()->findAll(array("select"=>"hdsk_status","distinct"=>"true","order"=>"hdsk_status ASC"));
		$data=array();
		$detail=array();
		if(!isset($_POST['HelpDesk']['hdsk_time']) AND !isset($_POST['HelpDesk']['hdsk_time_end'])){
			$title="Keseluruhan";
		}else{
			$model->attributes=$_POST['HelpDesk'];
			$model->hdsk_time_end=$_POST['HelpDesk']['hdsk_time_end'];
			$title=Lib::dateInd($_POST[HelpDesk][hdsk_time],false)." s.d ".Lib::dateInd($_POST[HelpDesk][hdsk_time_end],false);
		}
		foreach($helpdesk as $row){
			if(!isset($_POST['HelpDesk']['hdsk_time']) AND !isset($_POST['HelpDesk']['hdsk_time_end'])){
				$nData=HelpDesk::model()->count(array("condition"=>"hdsk_status='$row->hdsk_status'"));
			}else{
				$nData=HelpDesk::model()->count(array("condition"=>"date(hdsk_time) between '$model->hdsk_time' AND '$model->hdsk_time_end' AND hdsk_status='$row->hdsk_status'"));
			}
			$data[]='{label:"'.str_replace(" ","",$row->hdsk_status).'",value:'.$nData.'}';
			$detail[str_replace(" ","",$row->hdsk_status)]=$nData;
		}
		
		
		$this->render('grafik',array(
		'data'=>$data,"detail"=>$detail,"title"=>$title,"model"=>$model
		));
	}

	/**
	* Displays a particular model.
	* @param integer $id the ID of the model to be displayed
	*/
	public function actionView($id)
	{
		$this->render('view',array(
		'model'=>$this->loadModel($id),
		));
	}

	/**
	* Creates a new model.
	* If creation is successful, the browser will be redirected to the 'view' page.
	*/
	public function actionCreate()
	{
		$model=new HelpDesk;

		// Uncomment the following line if AJAX validation is needed
		$this->performAjaxValidation($model);

		if(isset($_POST['HelpDesk']))
		{
			$model->attributes=$_POST['HelpDesk'];
			$model->create_at=date("Y-m-d H:i:s");
			$model->modified_at=date("Y-m-d H:i:s");
			$cek=str_replace('../../../../','http://100.100.100.176/irhis/',$_POST['HelpDesk']['hdsk_content']);
			$model->hdsk_content=$cek;
			if($model->save()){
				$this->redirect(array('view','id'=>$model->hdsk_id));
			}
		}

		$this->render('create',array(
		'model'=>$model,
		));
	}

	/**
	* Updates a particular model.
	* If update is successful, the browser will be redirected to the 'view' page.
	* @param integer $id the ID of the model to be updated
	*/
	public function actionUpdate($id)
	{
		$model=$this->loadModel($id);

		// Uncomment the following line if AJAX validation is needed
		$this->performAjaxValidation($model);
	
		if(isset($_POST['HelpDesk']))
		{
			$model->attributes=$_POST['HelpDesk'];
			$model->modified_at=date("Y-m-d H:i:s");
			
			$cek=str_replace('../../../../','http://100.100.100.176/irhis/',$_POST['HelpDesk']['hdsk_content']);
			$model->hdsk_content=$cek;
			
			if($model->save()){
				$this->redirect(array('view','id'=>$model->hdsk_id));
			}
		}

		$this->render('update',array(
		'model'=>$model,
		));
	}

	/**
	* Deletes a particular model.
	* If deletion is successful, the browser will be redirected to the 'admin' page.
	* @param integer $id the ID of the model to be deleted
	*/
	public function actionDelete($id)
	{
		if(Yii::app()->request->isPostRequest)
		{
		// we only allow deletion via POST request		
		$this->loadModel($id)->delete();

		// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
		if(!isset($_GET['ajax']))
		$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
		}
		else
		throw new CHttpException(400,'Invalid request. Please do not repeat this request again.');
	}

	/**
	* Lists all models.
	*/
	public function actionAdmin()
	{
		$dataProvider=new CActiveDataProvider('HelpDesk');
		$this->render('index',array(
		'dataProvider'=>$dataProvider,
		));
	}

	/**
	* Manages all models.
	*/
	public function actionIndex()
	{
		$model=new HelpDesk('search');
		$model->unsetAttributes();  // clear any default values
		if(isset($_GET['HelpDesk']))
		$model->attributes=$_GET['HelpDesk'];
		if (isset($_GET['pageSize'])) {
			Yii::app()->user->setState('pageSize',(int)$_GET['pageSize']); 
			unset($_GET['pageSize']);
		}
		$this->render('admin',array(
		'model'=>$model,
		));
	}
	
	public function actionBatchDelete()
	{
		$request = Yii::app()->getRequest();
		if($request->getIsPostRequest()){
			if(isset($_POST['ids'])){
				$ids = $_POST['ids'];
			}
			if (empty($ids)) {
				echo CJSON::encode(array('status' => 'failure', 'msg' => 'you should at least choice one item'));
				die();
			}
			$successCount = $failureCount = 0;
			$dataError=array();
			foreach ($ids as $id) {
				$model = $this->loadModel($id);
				$temp = $this->loadModel($id);
				if($model->delete()){
					$successCount++ ;
				}else{
					$failureCount++;
					$dataError[]=$model->hdsk_id;
				}
			}
			echo CJSON::encode(array('status' => 'success',
				'data' => array(
					'successCount' => $successCount,
					'failureCount' => $failureCount,
					'dataError' => $dataError,
				)));
			die();
		}else{
			throw new CHttpException(400,'Invalid request. Please do not repeat this request again.');
		}
	}
	
	public function actionExportExcel()
	{
		ini_set("memory_limit","8056M");
		set_time_limit(100000);
		Yii::import('ext.phpexcel.XPHPExcel');    
		$objPHPExcel= XPHPExcel::createPHPExcel();
		$objPHPExcel->getProperties()->setCreator("File")
		->setLastModifiedBy("File")
		->setTitle("Dokumen")
		->setSubject("Dokumen")
		->setDescription("File")
		->setKeywords("File")
		->setCategory("File");
		$model=new HelpDesk;

		$objPHPExcel->setActiveSheetIndex(0)->setCellValue('A1',"No");
		$objPHPExcel->setActiveSheetIndex(0)->setCellValue('B1',$model->getAttributeLabel('hdsk_id'));
		$objPHPExcel->setActiveSheetIndex(0)->setCellValue('C1',$model->getAttributeLabel('hdsk_time'));
		$objPHPExcel->setActiveSheetIndex(0)->setCellValue('D1',$model->getAttributeLabel('hd_cat_id'));
		$objPHPExcel->setActiveSheetIndex(0)->setCellValue('E1',$model->getAttributeLabel('hdsk_content'));
		$objPHPExcel->setActiveSheetIndex(0)->setCellValue('F1',$model->getAttributeLabel('hdsk_user_report'));
		$objPHPExcel->setActiveSheetIndex(0)->setCellValue('G1',$model->getAttributeLabel('hdsk_user_recieve'));
		$objPHPExcel->setActiveSheetIndex(0)->setCellValue('H1',$model->getAttributeLabel('hdsk_user_execute'));
		$objPHPExcel->setActiveSheetIndex(0)->setCellValue('I1',$model->getAttributeLabel('hdsk_status'));
		$objPHPExcel->setActiveSheetIndex(0)->setCellValue('J1',$model->getAttributeLabel('hdsk_percentage'));
		$objPHPExcel->setActiveSheetIndex(0)->setCellValue('K1',$model->getAttributeLabel('hdsk_priority'));
		$objPHPExcel->setActiveSheetIndex(0)->setCellValue('L1',$model->getAttributeLabel('hdsk_solved_time'));
		$objPHPExcel->setActiveSheetIndex(0)->setCellValue('M1',$model->getAttributeLabel('hdsk_followup_time'));
		$objPHPExcel->setActiveSheetIndex(0)->setCellValue('N1',$model->getAttributeLabel('hdsk_duedate'));
		$objPHPExcel->setActiveSheetIndex(0)->setCellValue('O1',$model->getAttributeLabel('hdsk_note'));
		$objPHPExcel->setActiveSheetIndex(0)->setCellValue('P1',$model->getAttributeLabel('hdsk_approval'));
		$objPHPExcel->setActiveSheetIndex(0)->setCellValue('Q1',$model->getAttributeLabel('create_at'));
		$objPHPExcel->setActiveSheetIndex(0)->setCellValue('R1',$model->getAttributeLabel('modified_at'));
		
		$criteria=new CDbCriteria;
		$criteria->addCondition("hdsk_id IN (".$_POST['ids'].")");
		$data= HelpDesk::model()->findAll($criteria);    
		$j=1;
		$no=0;
		foreach($data as $row) {
			$j++;
			$no++;
			$objPHPExcel->setActiveSheetIndex(0)->setCellValue('A'.$j, $no);
			$objPHPExcel->setActiveSheetIndex(0)->setCellValue('B'.$j, $row->hdsk_id);
			$objPHPExcel->setActiveSheetIndex(0)->setCellValue('C'.$j, $row->hdsk_time);
			$objPHPExcel->setActiveSheetIndex(0)->setCellValue('D'.$j, $row->hd_cat_id);
			$objPHPExcel->setActiveSheetIndex(0)->setCellValue('E'.$j, strip_tags($row->hdsk_content));
			$objPHPExcel->setActiveSheetIndex(0)->setCellValue('F'.$j, $row->hdsk_user_report);
			$objPHPExcel->setActiveSheetIndex(0)->setCellValue('G'.$j, $row->hdsk_user_recieve);
			$objPHPExcel->setActiveSheetIndex(0)->setCellValue('H'.$j, $row->hdsk_user_execute);
			$objPHPExcel->setActiveSheetIndex(0)->setCellValue('I'.$j, $row->hdsk_status);
			$objPHPExcel->setActiveSheetIndex(0)->setCellValue('J'.$j, $row->hdsk_percentage);
			$objPHPExcel->setActiveSheetIndex(0)->setCellValue('K'.$j, $row->hdsk_priority);
			$objPHPExcel->setActiveSheetIndex(0)->setCellValue('L'.$j, $row->hdsk_solved_time);
			$objPHPExcel->setActiveSheetIndex(0)->setCellValue('M'.$j, $row->hdsk_followup_time);
			$objPHPExcel->setActiveSheetIndex(0)->setCellValue('N'.$j, $row->hdsk_duedate);
			$objPHPExcel->setActiveSheetIndex(0)->setCellValue('O'.$j, $row->hdsk_note);
			$objPHPExcel->setActiveSheetIndex(0)->setCellValue('P'.$j, $row->hdsk_approval);
			$objPHPExcel->setActiveSheetIndex(0)->setCellValue('Q'.$j, $row->create_at);
			$objPHPExcel->setActiveSheetIndex(0)->setCellValue('R'.$j, $row->modified_at);
		}
		$styleArray = array(
			'font' => array(
				'bold' => true,
			),
		);
		$styleArrayData = array(
			'borders' => array(
				'allborders' => array(
					'style' => PHPExcel_Style_Border::BORDER_THIN
				),
			),
		);
		$objPHPExcel->getActiveSheet()->getStyle('A1:R18')->applyFromArray($styleArray);
		// Rename worksheet
		$objPHPExcel->getActiveSheet()->setTitle('Default');

		// Set active sheet index to the first sheet, so Excel opens this as the first sheet
		$objPHPExcel->setActiveSheetIndex(0);
		 		 
		// Redirect output to a client�s web browser (Excel5)
		header('Content-Type: application/vnd.ms-excel');
		header('Content-Disposition: attachment;filename="Help Desk '.date('Y-m-d H:i:s').'.xls"');
		header('Cache-Control: max-age=0');
		// If you're serving to IE 9, then the following may be needed
		header('Cache-Control: max-age=1');
		 
		// If you're serving to IE over SSL, then the following may be needed
		header ('Expires: Mon, 26 Jul 1997 05:00:00 GMT'); // Date in the past
		header ('Last-Modified: '.gmdate('D, d M Y H:i:s').' GMT'); // always modified
		header ('Cache-Control: cache, must-revalidate'); // HTTP/1.1
		header ('Pragma: public'); // HTTP/1.0
		 
		$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
		$objWriter->save('php://output');
	    Yii::app()->end();
	}
	
	public function actionExportPdf()
	{
		Yii::import('ext.MPDF57.*');
		include("mpdf.php");
		ini_set("memory_limit","8056M");
		set_time_limit(100000);
		$mpdf=new mPDF('utf-8', 'A4-L', 0, null, 3,3,10,20,15,15 );
		
		$model=new HelpDesk;
		
		$criteria=new CDbCriteria;
		$criteria->addCondition("hdsk_id IN (".$_POST['ids'].")");
		$data= HelpDesk::model()->findAll($criteria); 
		
		$j = 1;
		$no=0;
		
		$html='
		<style type="text/css" media="print,screen" >
		th {
			font-family:Arial;
			color:black;
			background-color:lightgrey;
			padding:5px;
		}
		td{
			padding:5px;
		}
		thead {
			display:table-header-group;
		}
		tbody {
			display:table-row-group;
		}
		</style>
		<h2 style="text-align: center;">Help Desk</h2>
		<br>
		<table border="1" style="border:1px; width:100%; vertical-align: text-top;border-collapse: collapse;">
			<thead>
				<tr>
					<th>No</th>
										<th>'.$model->getAttributeLabel('hdsk_id').'</th>
					<th>'.$model->getAttributeLabel('hdsk_time').'</th>
					<th>'.$model->getAttributeLabel('hd_cat_id').'</th>
					<th>'.$model->getAttributeLabel('hdsk_content').'</th>
					<th>'.$model->getAttributeLabel('hdsk_user_report').'</th>
					<th>'.$model->getAttributeLabel('hdsk_user_recieve').'</th>
					<th>'.$model->getAttributeLabel('hdsk_user_execute').'</th>
					<th>'.$model->getAttributeLabel('hdsk_status').'</th>
					<th>'.$model->getAttributeLabel('hdsk_percentage').'</th>
					<th>'.$model->getAttributeLabel('hdsk_priority').'</th>
					<th>'.$model->getAttributeLabel('hdsk_solved_time').'</th>
					<th>'.$model->getAttributeLabel('hdsk_followup_time').'</th>
					<th>'.$model->getAttributeLabel('hdsk_duedate').'</th>
					<th>'.$model->getAttributeLabel('hdsk_note').'</th>
					<th>'.$model->getAttributeLabel('hdsk_approval').'</th>
					<th>'.$model->getAttributeLabel('create_at').'</th>
					<th>'.$model->getAttributeLabel('modified_at').'</th>
				</tr>
			</thead>
			<tbody>';
			
			$no=0;
			foreach($data as $row){
			$no++;
			$html.='
				<tr>
					<td>'.$no.'</td>
										<td>'.$row->hdsk_id.'</td>
					<td>'.$row->hdsk_time.'</td>
					<td>'.$row->hd_cat_id.'</td>
					<td>'.$row->hdsk_content.'</td>
					<td>'.$row->hdsk_user_report.'</td>
					<td>'.$row->hdsk_user_recieve.'</td>
					<td>'.$row->hdsk_user_execute.'</td>
					<td>'.$row->hdsk_status.'</td>
					<td>'.$row->hdsk_percentage.'</td>
					<td>'.$row->hdsk_priority.'</td>
					<td>'.$row->hdsk_solved_time.'</td>
					<td>'.$row->hdsk_followup_time.'</td>
					<td>'.$row->hdsk_duedate.'</td>
					<td>'.$row->hdsk_note.'</td>
					<td>'.$row->hdsk_approval.'</td>
					<td>'.$row->create_at.'</td>
					<td>'.$row->modified_at.'</td>
				</tr>';
			}	
			$html.='
			</tbody>
		</table>	
		';
		ini_set("memory_limit","256M");
		$mpdf->Bookmark('Start of the document');
		$mpdf->SetDisplayMode('fullpage','two');
		$mpdf->setFooter('Tanggal {DATE j-m-Y}||Halaman {PAGENO} dari {nbpg}') ;
		$mpdf->simpleTables = true;
		$mpdf->WriteHTML('<sheet-size="Letter" />');
		$mpdf->WriteHTML($html);
		$mpdf->Output();
	}
	
	public function actionImport()
	{
		ini_set("memory_limit","8056M");
		set_time_limit(100000);
		$model=new HelpDesk;
		if(isset($_POST['HelpDesk']))
		{
		$fileUpload=CUploadedFile::getInstance($model,'import');
		$nama=date('YmdHis').'_'.$fileUpload;
		$path=Yii::app()->basePath . '/../temp_excel/'.$nama.'';
		$fileUpload->saveAs($path);
			
		Yii::import('ext.phpexcelreader.JPhpExcelReader');
		$data=new JPhpExcelReader($path);
		$baris=$data->sheets[0][numRows];
		$kolom=$data->sheets[0][numCols];
		$sukses=0;
		$gagal=0;
		for($i=2;$i<=$baris; $i++){
			$model=new HelpDesk;
			if(!empty($data->sheets[0]['cells'][$i][1])){
				$model->hdsk_id= $data->sheets[0]['cells'][$i][1];
			}
			$model->hdsk_time= $data->sheets[0]['cells'][$i][2];
			$model->hd_cat_id= $data->sheets[0]['cells'][$i][3];
			$model->hdsk_content= $data->sheets[0]['cells'][$i][4];
			if(!empty($data->sheets[0]['cells'][$i][5])){
				$model->hdsk_user_report= $data->sheets[0]['cells'][$i][5];
			}
			if(!empty($data->sheets[0]['cells'][$i][6])){
				$model->hdsk_user_recieve= $data->sheets[0]['cells'][$i][6];
			}
			if(!empty($data->sheets[0]['cells'][$i][7])){
				$model->hdsk_user_execute= $data->sheets[0]['cells'][$i][7];
			}
			
			
			$model->hdsk_status= trim($data->sheets[0]['cells'][$i][8]);
			$model->hdsk_percentage= $data->sheets[0]['cells'][$i][9];
			$model->hdsk_priority= trim($data->sheets[0]['cells'][$i][10]);
			$model->hdsk_solved_time= $data->sheets[0]['cells'][$i][11];
			$model->hdsk_followup_time= $data->sheets[0]['cells'][$i][12];
			$model->hdsk_duedate= $data->sheets[0]['cells'][$i][13];
			$model->hdsk_note= $data->sheets[0]['cells'][$i][14];
			$model->hdsk_approval= $data->sheets[0]['cells'][$i][15];
			$cek=HelpDesk::model()->findbyPk($data->sheets[0]['cells'][$i][1]);
			if(count($cek)==0){
				if ($model->save()){
				$sukses++;
				}else{
				$gagal++;
				}
			} else {
				$gagal++;
			}
		}
		@unlink($path);
		 Yii::app()->user->setFlash('success', "Proses import data selesai<br/>
         Jumlah data yang sukses diimport : ".$sukses." Jumlah data yang gagal diimport : ".$gagal."
         ");
		$this->redirect(array('index'));
		}
	}
	
	public function actionDownloadTemplate(){
		Yii::import('ext.phpexcel.XPHPExcel');    
		$objPHPExcel= XPHPExcel::createPHPExcel();
		$objPHPExcel->getProperties()->setCreator("File")
		->setLastModifiedBy("File")
		->setTitle("Dokumen Rahasia")
		->setSubject("Dokumen Rahasia")
		->setDescription("File")
		->setKeywords("File")
		->setCategory("File");
		$model=new HelpDesk;
		$cat=new HelpDeskCategory;
		$objPHPExcel->setActiveSheetIndex(0)->setCellValue('A1',$model->getAttributeLabel('hdsk_id')." (Kosongkan Jika tidak ada)");
		$objPHPExcel->setActiveSheetIndex(0)->setCellValue('B1',$model->getAttributeLabel('hdsk_time'));
		$objPHPExcel->setActiveSheetIndex(0)->setCellValue('C1',$model->getAttributeLabel('hd_cat_id'));
		$objPHPExcel->setActiveSheetIndex(0)->setCellValue('D1',$model->getAttributeLabel('hdsk_content'));
		$objPHPExcel->setActiveSheetIndex(0)->setCellValue('E1',$model->getAttributeLabel('hdsk_user_report'));
		$objPHPExcel->setActiveSheetIndex(0)->setCellValue('F1',$model->getAttributeLabel('hdsk_user_recieve'));
		$objPHPExcel->setActiveSheetIndex(0)->setCellValue('G1',$model->getAttributeLabel('hdsk_user_execute'));
		$objPHPExcel->setActiveSheetIndex(0)->setCellValue('H1',$model->getAttributeLabel('hdsk_status'));
		$objPHPExcel->setActiveSheetIndex(0)->setCellValue('I1',$model->getAttributeLabel('hdsk_percentage'));
		$objPHPExcel->setActiveSheetIndex(0)->setCellValue('J1',$model->getAttributeLabel('hdsk_priority'));
		$objPHPExcel->setActiveSheetIndex(0)->setCellValue('K1',$model->getAttributeLabel('hdsk_solved_time'));
		$objPHPExcel->setActiveSheetIndex(0)->setCellValue('L1',$model->getAttributeLabel('hdsk_followup_time'));
		$objPHPExcel->setActiveSheetIndex(0)->setCellValue('M1',$model->getAttributeLabel('hdsk_duedate'));
		$objPHPExcel->setActiveSheetIndex(0)->setCellValue('N1',$model->getAttributeLabel('hdsk_note'));
		$objPHPExcel->setActiveSheetIndex(0)->setCellValue('O1',$model->getAttributeLabel('hdsk_approval'));
		
		
		$data= HelpDesk::model()->findAll(array('limit'=>10));    
		$j = 1;
		$no=0;
		foreach($data as $row) {
			$j++;
			$no++;
			$objPHPExcel->setActiveSheetIndex(0)->setCellValue('A'.$j, $row->hdsk_id);
			$objPHPExcel->setActiveSheetIndex(0)->setCellValue('B'.$j, $row->hdsk_time);
			$objPHPExcel->setActiveSheetIndex(0)->setCellValue('C'.$j, $row->hd_cat_id);
			$objPHPExcel->setActiveSheetIndex(0)->setCellValue('D'.$j, $row->hdsk_content);
			$objPHPExcel->setActiveSheetIndex(0)->setCellValue('E'.$j, $row->hdsk_user_report);
			$objPHPExcel->setActiveSheetIndex(0)->setCellValue('F'.$j, $row->hdsk_user_recieve);
			$objPHPExcel->setActiveSheetIndex(0)->setCellValue('G'.$j, $row->hdsk_user_execute);
			$objPHPExcel->setActiveSheetIndex(0)->setCellValue('H'.$j, $row->hdsk_status);
			$objPHPExcel->setActiveSheetIndex(0)->setCellValue('I'.$j, $row->hdsk_percentage);
			$objPHPExcel->setActiveSheetIndex(0)->setCellValue('J'.$j, $row->hdsk_priority);
			$objPHPExcel->setActiveSheetIndex(0)->setCellValue('K'.$j, $row->hdsk_solved_time);
			$objPHPExcel->setActiveSheetIndex(0)->setCellValue('L'.$j, $row->hdsk_followup_time);
			$objPHPExcel->setActiveSheetIndex(0)->setCellValue('M'.$j, $row->hdsk_duedate);
			$objPHPExcel->setActiveSheetIndex(0)->setCellValue('N'.$j, $row->hdsk_note);
			$objPHPExcel->setActiveSheetIndex(0)->setCellValue('O'.$j, $row->hdsk_approval);
		}
		
		
		
		$styleArray = array(
			'font' => array(
				'bold' => true,
			),
		);
		$styleArrayData = array(
			'borders' => array(
				'allborders' => array(
					'style' => PHPExcel_Style_Border::BORDER_THIN
				),
			),
		);
		$objPHPExcel->getActiveSheet()->getStyle('A1:O15')->applyFromArray($styleArray);
		$objPHPExcel->getActiveSheet()->getStyle('A1:O'.$j)->applyFromArray($styleArrayData);
		// Rename worksheet
		$objPHPExcel->createSheet(1);
        $objPHPExcel->setActiveSheetIndex(1);
		$objPHPExcel->setActiveSheetIndex(1)->setCellValue('A1',$model->getAttributeLabel('hd_cat_id'));
		$objPHPExcel->setActiveSheetIndex(1)->setCellValue('B1',$model->getAttributeLabel('hd_cat_name'));
		$dataCat= HelpDeskCategory::model()->findAll();    
		$m = 1;
		foreach($dataCat as $row) {
			$m++;
			$no++;
			$objPHPExcel->setActiveSheetIndex(1)->setCellValue('A'.$m, $row->hd_cat_id);
			$objPHPExcel->setActiveSheetIndex(1)->setCellValue('B'.$m, $row->hd_cat_name);
		}
		$objPHPExcel->getActiveSheet()->setTitle('Category');

		// Set active sheet index to the first sheet, so Excel opens this as the first sheet
		$objPHPExcel->setActiveSheetIndex(0);
		$objPHPExcel->getActiveSheet()->setTitle('Helpdesk');
		// Redirect output to a client�s web browser (Excel5)
		header('Content-Type: application/vnd.ms-excel');
		header('Content-Disposition: attachment;filename="Template Help Desk '.date('Y-m-d H:i:s').'.xls"');
		header('Cache-Control: max-age=0');
		// If you're serving to IE 9, then the following may be needed
		header('Cache-Control: max-age=1');
		 
		// If you're serving to IE over SSL, then the following may be needed
		header ('Expires: Mon, 26 Jul 1997 05:00:00 GMT'); // Date in the past
		header ('Last-Modified: '.gmdate('D, d M Y H:i:s').' GMT'); // always modified
		header ('Cache-Control: cache, must-revalidate'); // HTTP/1.1
		header ('Pragma: public'); // HTTP/1.0
		 
		$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
		$objWriter->save('php://output');
	    Yii::app()->end();
	}
	

	public function loadModel($id)
	{
		$model=HelpDesk::model()->findByPk($id);
		if($model===null)
		throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}

	/**
	* Performs the AJAX validation.
	* @param CModel the model to be validated
	*/
	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='help-desk-form')
		{
		echo CActiveForm::validate($model);
		Yii::app()->end();
		}
	}
}
