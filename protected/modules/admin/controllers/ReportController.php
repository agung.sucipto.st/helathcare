<?php
class ReportController extends Controller
{
	/**
	* @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	* using two-column layout. See 'protected/views/layouts/column2.php'.
	*/
	public $layout='//layouts/admin/main';

	/**
	* @return array action filters
	*/
	public function filters()
	{
		return array(
		'accessControl', // perform access control for CRUD operations
		);
	}

	/**
	* Specifies the access control rules.
	* This method is used by the 'accessControl' filter.
	* @return array access control rules
	*/
	public function accessRules()
	{
		return array(
		array('allow',  // allow all users to perform 'index' and 'view' actions
		'actions'=>array('responseTime'),
		'expression'=>'$user->getAuth()',
		),

		array('deny',  // deny all users
		'users'=>array('*'),
		),
		);
	}

	/**
	* Displays a particular model.
	* @param integer $id the ID of the model to be displayed
	*/
	public function actionResponseTime()
	{		
		$model=new Registration;
		
		if(isset($_POST['Registration'])){
			$model->reg_start=$_POST['Registration']['reg_start'];
			$model->reg_end=$_POST['Registration']['reg_end'];
			$criteria=new CDbCriteria;
			$criteria->condition="DATE(reg_date_in) between '$model->reg_start' and '$model->reg_end' and reg_type='OUTPATIENT'";
			$criteria->order="reg_date_in asc";
			$data=Registration::model()->findAll($criteria);
		}else{
			$criteria=new CDbCriteria;
			$criteria->condition="DATE(reg_date_in) between '".date("Y-m-d")."' and '".date("Y-m-d")."' and reg_type='OUTPATIENT'";
			$criteria->order="reg_date_in asc";
			$criteria->order="reg_date_in asc";
			$data=Registration::model()->findAll($criteria);
		}
		
		if(isset($_POST['export'])){
			header("Content-Type:   application/vnd.ms-excel; charset=utf-8");
			header("Content-type:   application/x-msexcel; charset=utf-8");
			header("Content-Disposition: attachment; filename=export_responsetime.xls"); 
			header("Expires: 0");
			header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
			header("Cache-Control: private",false);
			$this->layout='//layouts/admin/realy_blank';
			$this->render('export_responsetime',array(
				'data'=>$data
			));
			exit;
		}
		
		$this->render('responseTime',array(
		'data'=>$data,'model'=>$model
		));
	}
}
