<?php

class AttendanceController extends Controller
{
	/**
	* @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	* using two-column layout. See 'protected/views/layouts/column2.php'.
	*/
	public $layout='//layouts/admin/main';

	/**
	* @return array action filters
	*/
	public function filters()
	{
		return array(
		'accessControl', // perform access control for CRUD operations
		);
	}

	/**
	* Specifies the access control rules.
	* This method is used by the 'accessControl' filter.
	* @return array access control rules
	*/
	public function accessRules()
	{
		return array(
		array('allow', // allow admin user to perform 'admin' and 'delete' actions
		'actions'=>array('index','view','create','update','admin','delete','BatchDelete','exportExcel','exportPdf','import','downloadTemplate','backHome','createGroup','printForm','employeeReport','viewReport','viewPersonal','summaryReport','viewSummary','printSummary','temp','getTemp','modified','SummaryReportPersonal','viewSummaryPersonal','createByEmployee','downloadAttendance','updateOne','createOne','setAttendance','downloadSelected','doubleShift','onCall','reattendance','setStatus','notAttend','personalLate','personalEarly','personalEarlyData'),
		//'expression'=>'$user->getAuth()',
		'users'=>array('@'),
		),
		array('deny',  // deny all users
		'users'=>array('*'),
		),
		);
	}

	/**
	* Displays a particular model.
	* @param integer $id the ID of the model to be displayed
	*/
	
	
	
	public function actionGetTemp(){
		$form=new Attendance;
		if(isset($_POST['Attendance'])){
			$start=$_POST['Attendance']['att_date'];
			$end=$_POST['Attendance']['att_date_to'];
			$id=$_POST['Attendance']['att_emp_id'];
			if($id!=''){
				$att=Attendance::model()->findAll(array("condition"=>"att_emp_id='$id' AND att_date between '$start' AND '$end' and att_should_attend='NO'"));
			}else{
				$att=Attendance::model()->findAll(array("condition"=>"att_date between '$start' AND '$end'"));
			}
			
			$updateX=0;
			foreach($att as $row){
					$update=$this->loadModel($row->att_id);
					$shift=Shift::model()->findbyPk($update->shift_id);
					$update->att_should_attend=$shift->shift_attend;
					if($update->att_should_attend=="No" or $update->att_sts_id!=''){
						$update->att_in=null;
						$update->att_out=null;
						$update->fp_id=null;
					}else{
						if($update->att_date_to==""){
							$dataIN=Attendance::checkIn($shift->shift_att_in,$update->att_date,$update->attEmp->att_id);
							$dataOUT=Attendance::checkOut($shift->shift_att_out,$update->att_date,$update->attEmp->att_id);
							$update->att_in=$dataIN;
							$update->att_out=$dataOUT;
							$update->fp_id=$dataIN->fp_id;
							$update->att_update=date("Y-m-d H:i:s");
						}else{
							$dataIN=Attendance::checkIn($shift->shift_att_in,$update->att_date,$update->attEmp->att_id);
							$dataOUT=Attendance::checkOut($shift->shift_att_out,$update->att_date_to,$update->attEmp->att_id);
							$update->att_in=$dataIN;
							$update->att_out=$dataOUT;
							$update->fp_id=$dataIN->fp_id;
							$update->att_update=date("Y-m-d H:i:s");
						}
					}
					
					if($update->save()){
						$updateX++;
					}
				
				
			}
			
			Yii::app()->user->setFlash('result', "Download Success !<br/> $updateX Data Update");
		}
		$this->render('download_attendance',array(
		'model'=>$form
		));
		
	}
	
	public function actionView($periode)
	{
		$model=Attendance::model()->findAll(array("condition"=>"att_date=:date","params"=>array("date"=>$periode),"order"=>"RIGHT(att_emp_id,5) ASC"));
		$this->render('view',array(
		'model'=>$model,
		));
	}
	
	public function actionViewPersonal($periode,$id)
	{
		$periode=explode("-",$periode);
		$model=Attendance::model()->findAll(array("condition"=>"YEAR(att_date)=:year AND MONTH(att_date)=:month AND att_emp_id=:id","params"=>array("year"=>$periode[0],"month"=>$periode[1],"id"=>$id),"order"=>"att_date ASC"));
		if(!isset($_GET['print'])){
		$this->render('view_personal',array(
		'model'=>$model,
		));
		}else{
		$this->layout='//layouts/admin/empty';
		$this->render('print_view_personal',array(
		'model'=>$model,
		));
		}
	}
	
	public function actionEmployeeReport()
	{
		$model=new Employee('search');
		$model->unsetAttributes();  // clear any default values
		if(isset($_GET['Employee']))
		$model->attributes=$_GET['Employee'];
		if (isset($_GET['pageSize'])) {
		//user()->setState('pageSize',(int)$_GET['pageSize']);  //this doesn't work for me
		Yii::app()->user->setState('pageSize',(int)$_GET['pageSize']);   //use Yii::app()->user
		unset($_GET['pageSize']);  // would interfere with pager and repetitive page size change
		}
		$this->render('employee_admin',array(
		'model'=>$model,
		));
	}

	public function actionViewReport($id)
	{
		$model=new Attendance('searchByEmployee');
		$model->unsetAttributes();  // clear any default values
		if(isset($_GET['Attendance']))
		$model->attributes=$_GET['Attendance'];
		if (isset($_GET['pageSize'])) {
		//user()->setState('pageSize',(int)$_GET['pageSize']);  //this doesn't work for me
		Yii::app()->user->setState('pageSize',(int)$_GET['pageSize']);   //use Yii::app()->user
		unset($_GET['pageSize']);  // would interfere with pager and repetitive page size change
		}
		$this->render('view_report_personal',array(
		'model'=>$model
		));
	}
	
	public function actionSummaryReport()
	{
		$form=new Attendance;
		if(isset($_POST['Attendance'])){
			if(!isset($_POST['excel'])){
				$this->redirect(array('viewSummary','from'=>$_POST['Attendance']['att_date'],'to'=>$_POST['Attendance']['att_emp_id']));
			}else{
				$this->redirect(array('viewSummary','from'=>$_POST['Attendance']['att_date'],'to'=>$_POST['Attendance']['att_emp_id'],'excel'=>'true'));
			}
		}
		$this->render('summary_report',array(
		'model'=>$form
		));
	}
	
	public function actionPersonalLate()
	{
		$form=new Attendance;
		if(isset($_POST['Attendance'])){
			$form->attributes=$_POST['Attendance'];
			$model=Attendance::model()->findAll(array("condition"=>"att_date between :from AND :to and att_emp_id=:att_emp_id","params"=>array("from"=>$_POST['Attendance']['att_date'],"to"=>$_POST['Attendance']['att_date_to'],"att_emp_id"=>$_POST['Attendance']['att_emp_id']),"order"=>"att_date ASC"));
			
			$from=$_POST['Attendance']['att_date'];
			$to=$_POST['Attendance']['att_date_to'];
			$form->att_date=$from;
			$form->att_date_to=$to;
			$emp=$_POST['Attendance']['att_emp_id'];
		}else{
			$model=Attendance::model()->findAll(array("condition"=>"att_date between :from AND :to","params"=>array("from"=>date("Y-m-d"),"to"=>date("Y-m-d")),"order"=>"att_date ASC"));
			$from=date("Y-m-d");
			$to=date("Y-m-d");
			$form->att_date=$from;
			$form->att_date_to=$to;
			$emp='';
		}

			$this->render('personal_late',array(
			'model'=>$form,'data'=>$model,'from'=>$from,'to'=>$to,'back'=>'true','emp'=>$emp
			));
		
		
	}
	
	public function actionPersonalEarly()
	{
		$form=new Attendance;
		if(isset($_POST['Attendance'])){
			$form->attributes=$_POST['Attendance'];
			$model=Attendance::model()->findAll(array("condition"=>"att_date between :from AND :to and att_emp_id=:att_emp_id","params"=>array("from"=>$_POST['Attendance']['att_date'],"to"=>$_POST['Attendance']['att_date_to'],"att_emp_id"=>$_POST['Attendance']['att_emp_id']),"order"=>"att_date ASC"));
			
			$from=$_POST['Attendance']['att_date'];
			$to=$_POST['Attendance']['att_date_to'];
			$form->att_date=$from;
			$form->att_date_to=$to;
			$emp=$_POST['Attendance']['att_emp_id'];
		}else{
			$model=Attendance::model()->findAll(array("condition"=>"att_date between :from AND :to","params"=>array("from"=>date("Y-m-d"),"to"=>date("Y-m-d")),"order"=>"att_date ASC"));
			$from=date("Y-m-d");
			$to=date("Y-m-d");
			$form->att_date=$from;
			$form->att_date_to=$to;
			$emp='';
		}

			$this->render('personal_early',array(
			'model'=>$form,'data'=>$model,'from'=>$from,'to'=>$to,'back'=>'true','emp'=>$emp
			));
		
		
	}
	
	
	public function actionPersonalEarlyData()
	{
		$form=new Attendance;
		if(isset($_POST['Attendance'])){
			$form->attributes=$_POST['Attendance'];			
			$model=Yii::app()->db->createCommand("SELECT TIMEDIFF(shift.shift_att_in,attendance.att_in) as selisih,attendance.att_emp_id,attendance.att_in,shift.shift_att_in,DATE_FORMAT(attendance.att_date,'%Y-%m') as att_date, COUNT(IF(TIMEDIFF(shift.shift_att_in,attendance.att_in)>='00:30:00',1,NULL)) as early,employee.emp_full_name FROM `attendance` Inner join shift on attendance.shift_id=shift.shift_id inner join employee on attendance.att_emp_id=employee.emp_id where attendance.att_date between '".$_POST['Attendance']['att_date']."' AND '".$_POST['Attendance']['att_date_to']."' AND attendance.`att_should_attend`='Yes' group by attendance.att_emp_id,DATE_FORMAT(attendance.att_date,'%Y-%m') order by RIGHT(attendance.att_emp_id,5) ASC")->queryAll();
			
			$from=$_POST['Attendance']['att_date'];
			$to=$_POST['Attendance']['att_date_to'];
			$form->att_date=$from;
			$form->att_date_to=$to;
		}else{			
			$model=Yii::app()->db->createCommand("SELECT TIMEDIFF(shift.shift_att_in,attendance.att_in) as selisih,attendance.att_emp_id,attendance.att_in,shift.shift_att_in,DATE_FORMAT(attendance.att_date,'%Y-%m') as att_date, COUNT(IF(TIMEDIFF(shift.shift_att_in,attendance.att_in)>='00:30:00',1,NULL)) as early,employee.emp_full_name FROM `attendance` Inner join shift on attendance.shift_id=shift.shift_id inner join employee on attendance.att_emp_id=employee.emp_id where attendance.att_date between '".date("Y-m-d")."' AND '".date("Y-m-d")."' AND attendance.`att_should_attend`='Yes' group by attendance.att_emp_id,DATE_FORMAT(attendance.att_date,'%Y-%m') order by RIGHT(attendance.att_emp_id,5) ASC")->queryAll();
			$from=date("Y-m-d");
			$to=date("Y-m-d");
			$form->att_date=$from;
			$form->att_date_to=$to;
			
		}

		if(!isset($_POST['print']) AND !isset($_POST['excel'])){
			$this->render('personal_early_data',array(
			'model'=>$form,'data'=>$model,'from'=>$from,'to'=>$to,'back'=>'true',
			));
		}elseif(isset($_POST['excel'])){
			header("Content-Type:   application/vnd.ms-excel; charset=utf-8");
			header("Content-type:   application/x-msexcel; charset=utf-8");
			header("Content-Disposition: attachment; filename=Early_Come_Report.xls"); 
			header("Expires: 0");
			header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
			header("Cache-Control: private",false);
			$this->layout='//layouts/admin/blank';
			$this->render('_personal_early_data',array(
			'model'=>$form,'data'=>$model,'from'=>$from,'to'=>$to,'back'=>'true',
			));
			exit;
		}elseif(isset($_POST['print'])){
			$this->layout='//layouts/admin/blank';
			$this->render('_personal_early_data',array(
			'model'=>$form,'data'=>$model,'from'=>$from,'to'=>$to,'back'=>'true',
			));
		}
		
		
	}
	
	public function actionNotAttend()
	{
		$form=new Attendance;
		if(isset($_POST['Attendance'])){
			$form->attributes=$_POST['Attendance'];
			if($_POST['Attendance']['att_sts_id']!=""){
				$model=Attendance::model()->findAll(array("condition"=>"att_date between :from AND :to and att_sts_id=:att_sts_id","params"=>array("from"=>$_POST['Attendance']['att_date'],"to"=>$_POST['Attendance']['att_date_to'],"att_sts_id"=>$_POST['Attendance']['att_sts_id']),"order"=>"att_date ASC"));
			}else{
				$model=Attendance::model()->findAll(array("condition"=>"att_date between :from AND :to and att_sts_id<>''","params"=>array("from"=>$_POST['Attendance']['att_date'],"to"=>$_POST['Attendance']['att_date_to']),"order"=>"att_date ASC"));
			}
			
			$from=$_POST['Attendance']['att_date'];
			$to=$_POST['Attendance']['att_date_to'];
			$form->att_date=$from;
			$form->att_date_to=$to;
		}else{
			$model=Attendance::model()->findAll(array("condition"=>"att_date between :from AND :to","params"=>array("from"=>date("Y-m-d"),"to"=>date("Y-m-d")),"order"=>"att_date ASC"));
			$from=date("Y-m-d");
			$to=date("Y-m-d");
			$form->att_date=$from;
			$form->att_date_to=$to;
		}
		if(!isset($_POST['print'])){
			$this->render('not_attend',array(
			'model'=>$form,'data'=>$model,'from'=>$from,'to'=>$to
			));
		}else{
			$this->layout='//layouts/admin/blank';
			$this->render('_not_attend',array(
			'data'=>$model,'from'=>$from,'to'=>$to,'back'=>'true'
			));
		}
		
	}
	
	public function actionSummaryReportPersonal()
	{
		$form=new Attendance;
		if(isset($_POST['Attendance'])){
			if(isset($_POST['finger'])){
				$this->redirect(array('viewSummaryPersonal','from'=>$_POST['Attendance']['att_date'],'to'=>$_POST['Attendance']['att_shift_id'],'id'=>$_POST['Attendance']['att_emp_id'],'finger'=>"true"));
			}else{
				$this->redirect(array('viewSummaryPersonal','from'=>$_POST['Attendance']['att_date'],'to'=>$_POST['Attendance']['att_shift_id'],'id'=>$_POST['Attendance']['att_emp_id']));
			}
			
		}
		$this->render('summary_report_personal',array(
		'model'=>$form
		));
	}
	
	public function actionSetStatus()
	{
		$attid=$_POST['attid'];
		$stsid=$_POST['stsid'];
	
		$update=$this->loadModel($attid);
		if($stsid=="null"){
			$update->att_sts_id=null;
		}else{
			$update->att_sts_id=$stsid;
		}
		
		$shift=Shift::model()->findbyPk($update->shift_id);
		if($shift->shift_att_in>$shift->shift_att_out){
			$ex=explode("-",$update->att_date);
			$nextTo=date("Y-m-d",mktime(0,0,0,$ex[1],$ex[2]+1,$ex[0]));
			$update->att_date_to=$nextTo;
		}else{
			$update->att_date_to=null;
		}
		
		$update->att_should_attend=$shift->shift_attend;
		if($update->att_should_attend=="No" or $update->att_sts_id!=''){
			$update->att_in=null;
			$update->att_out=null;
			$update->fp_id=null;
		}else{
			if($update->att_date_to==""){
				$dataIN=Attendance::checkIn($shift->shift_att_in,$update->att_date,$update->attEmp->att_id);
				$dataOUT=Attendance::checkOut($shift->shift_att_out,$update->att_date,$update->attEmp->att_id);
				$update->att_in=$dataIN;
				$update->att_out=$dataOUT;
				$update->fp_id=$dataIN->fp_id;
				$update->att_update=date("Y-m-d H:i:s");
			}else{
				$dataIN=Attendance::checkIn($shift->shift_att_in,$update->att_date,$update->attEmp->att_id);
				$dataOUT=Attendance::checkOut($shift->shift_att_out,$update->att_date_to,$update->attEmp->att_id);
				$update->att_in=$dataIN;
				$update->att_out=$dataOUT;
				$update->fp_id=$dataIN->fp_id;
				$update->att_update=date("Y-m-d H:i:s");
			}
			
		}
		$update->save();
		
		
		$no++;
		$statQuick='';
		$statLate='';
		$reason=array();
		$ot='';
		$quick='';
		$late=Lib::Late($update->shift->shift_att_in,$update->att_in);
		
		if($update->att_sts_id==""){
			if($update->att_should_attend=="Yes"){
				$att=1;
				$nAtt++;
				if($update->att_out!='' AND $update->att_out!='00:00:00'){
					$quick=Lib::QuickHome($update->shift->shift_att_out,$update->att_out,$update->shift->shift_att_in);
					
					if($quick=="00:00:00"){
						$statQuick='';
						$quick='';
					}else{
						if($update->att_is_possible_quick_home==1){
							$reason[]=$update->att_reason_possible_quick_home;
							$quick='';
						}else{
							$statQuick='style="background:red;color:white;"';
						}
						$nFast++;
					}
				}else{
					$quick='';
				}
				
				if($late=="00:00:00"){
					$statLate='';
					$late='';
					$nOn++;
				}else{
					if($late>"00:00:00"){
						if($update->att_is_possible_late==1){
							$statLate='';
							$reason[]=$update->att_reason_possible_late;
							$late='';
						}else{
							$statLate='style="background:red;color:white;"';
							$nLate++;
						}
					}
				}
				$work_hour=Lib::WO($update->att_in,$update->att_out,$update->shift->shift_att_in,$update->shift->shift_att_out);
				$real_hour=Lib::WO($update->att_in,$update->att_out,$update->shift->shift_att_in,$update->att_out);				
			}else{
				$att=0;
				$late='';
				$quick='';
				$ot='';
				$reason='';
				$work_hour='-';
				$real_hour='-';
			}
			
		}else{
			if($update->att_should_attend=="Yes"){
				$nAtt++;
			}
			$late='';
			$quick='';
			$ot='';
			$reason=$update->attSts->att_sts_name;
			$work_hour='-';
			$real_hour='-';
		}
		
		$status=array();
		$dataStatus=AttendanceStatus::model()->findAll();
		foreach($dataStatus as $rowY){
			$status[$rowY->att_sts_id]=$rowY->att_sts_name;
		}
		
	
		$data=array("in"=>'<div '.$statLate.'>'.$update->att_in.'</div>',"out"=>'<div '.$statQuick.'>'.$update->att_out.'</div>',"ket"=>$update->attSts->att_sts_name);
		echo CJSON::encode($data);
			
	}
	public function actionReattendance()
	{
		$attid=$_POST['attid'];
		$shiftid=$_POST['shiftid'];
	
		$update=$this->loadModel($attid);
		$update->shift_id=$shiftid;
		$shift=Shift::model()->findbyPk($shiftid);
		if($shift->shift_att_in>$shift->shift_att_out){
			$ex=explode("-",$update->att_date);
			$nextTo=date("Y-m-d",mktime(0,0,0,$ex[1],$ex[2]+1,$ex[0]));
			$update->att_date_to=$nextTo;
		}else{
			$update->att_date_to=null;
		}
		
		$update->att_should_attend=$shift->shift_attend;
		if($update->att_should_attend=="No" or $update->att_sts_id!=''){
			$update->att_in=null;
			$update->att_out=null;
			$update->fp_id=null;
		}else{
			if($update->att_date_to==""){
				$dataIN=Attendance::checkIn($shift->shift_att_in,$update->att_date,$update->attEmp->att_id);
				$dataOUT=Attendance::checkOut($shift->shift_att_out,$update->att_date,$update->attEmp->att_id);
				$update->att_in=$dataIN;
				$update->att_out=$dataOUT;
				$update->fp_id=$dataIN->fp_id;
				$update->att_update=date("Y-m-d H:i:s");
			}else{
				$dataIN=Attendance::checkIn($shift->shift_att_in,$update->att_date,$update->attEmp->att_id);
				$dataOUT=Attendance::checkOut($shift->shift_att_out,$update->att_date_to,$update->attEmp->att_id);
				$update->att_in=$dataIN;
				$update->att_out=$dataOUT;
				$update->fp_id=$dataIN->fp_id;
				$update->att_update=date("Y-m-d H:i:s");
			}
			
		}
		$update->save();
		
		
		$no++;
		$statQuick='';
		$statLate='';
		$reason=array();
		$ot='';
		$quick='';
		$late=Lib::Late($update->shift->shift_att_in,$update->att_in);
		
		if($update->att_sts_id==""){
			if($update->att_should_attend=="Yes"){
				$att=1;
				$nAtt++;
				if($update->att_out!='' AND $update->att_out!='00:00:00'){
					$quick=Lib::QuickHome($update->shift->shift_att_out,$update->att_out,$update->shift->shift_att_in);
					
					if($quick=="00:00:00"){
						$statQuick='';
						$quick='';
					}else{
						if($update->att_is_possible_quick_home==1){
							$reason[]=$update->att_reason_possible_quick_home;
							$quick='';
						}else{
							$statQuick='style="background:red;color:white;"';
						}
						$nFast++;
					}
				}else{
					$quick='';
				}
				
				if($late=="00:00:00"){
					$statLate='';
					$late='';
					$nOn++;
				}else{
					if($late>"00:00:00"){
						if($update->att_is_possible_late==1){
							$statLate='';
							$reason[]=$update->att_reason_possible_late;
							$late='';
						}else{
							$statLate='style="background:red;color:white;"';
							$nLate++;
						}
					}
				}
				$work_hour=Lib::WO($update->att_in,$update->att_out,$update->shift->shift_att_in,$update->shift->shift_att_out);
				$real_hour=Lib::WO($update->att_in,$update->att_out,$update->shift->shift_att_in,$update->att_out);				
			}else{
				$att=0;
				$late='';
				$quick='';
				$ot='';
				$reason='';
				$work_hour='-';
				$real_hour='-';
			}
			
		}else{
			if($update->att_should_attend=="Yes"){
				$nAtt++;
			}
			$late='';
			$quick='';
			$ot='';
			$reason='';
			$work_hour='-';
			$real_hour='-';
		}
		$data=array("in"=>'<div '.$statLate.'>'.$update->att_in.'</div>',"out"=>'<div '.$statQuick.'>'.$update->att_out.'</div>',"ket"=>implode(", ",$reason));
		echo CJSON::encode($data);
			
	}
	
	public function actionSetAttendance($from,$to,$id)
	{
		$this->layout='//layouts/admin/blank';
		/*
		$update=$this->loadModel(43739);
		$shift=Shift::model()->findbyPk($update->shift_id);
		$dataIN=Attendance::checkIn($shift->shift_att_in,$update->att_date,$update->attEmp->att_id);
		$dataOUT=Attendance::checkOut($shift->shift_att_out,$update->att_date,$update->attEmp->att_id);
		print_R($dataOUT);
		exit;
		*/
		
		if(isset($_POST['Attendance'])){
			$data=$_POST['Attendance']['shift_id'];
			
			foreach($data as $index=>$row){
				if(is_numeric($row)){
					$update=$this->loadModel($index);
					$update->shift_id=$row;
					$shift=Shift::model()->findbyPk($row);
					if($shift->shift_att_in>$shift->shift_att_out){
						$ex=explode("-",$update->att_date);
						$nextTo=date("Y-m-d",mktime(0,0,0,$ex[1],$ex[2]+1,$ex[0]));
						$update->att_date_to=$nextTo;
					}else{
						$update->att_date_to=null;
					}
					
					$update->att_should_attend=$shift->shift_attend;
					if($update->att_should_attend=="No" or $update->att_sts_id!=''){
						$update->att_in=null;
						$update->att_out=null;
						$update->fp_id=null;
					}else{
						if($update->att_date_to==""){
							$dataIN=Attendance::checkIn($shift->shift_att_in,$update->att_date,$update->attEmp->att_id);
							$dataOUT=Attendance::checkOut($shift->shift_att_out,$update->att_date,$update->attEmp->att_id);
							$update->att_in=$dataIN;
							$update->att_out=$dataOUT;
							$update->fp_id=$dataIN->fp_id;
							$update->att_update=date("Y-m-d H:i:s");
						}else{
							$dataIN=Attendance::checkIn($shift->shift_att_in,$update->att_date,$update->attEmp->att_id);
							$dataOUT=Attendance::checkOut($shift->shift_att_out,$update->att_date_to,$update->attEmp->att_id);
							$update->att_in=$dataIN;
							$update->att_out=$dataOUT;
							$update->fp_id=$dataIN->fp_id;
							$update->att_update=date("Y-m-d H:i:s");
						}
						
					}
					$update->save();
				}
			}
		}
		$model=Attendance::model()->findAll(array("condition"=>"att_date between :from AND :to AND att_emp_id=:id","params"=>array("from"=>$from,"to"=>$to,"id"=>$id),"order"=>"att_date ASC"));
		$this->render('set_attendance',array(
		'model'=>$model
		));
	}
	
	public function actionViewSummary()
	{
		$this->layout='//layouts/admin/pure_blank';
		if(!isset($_GET['excel'])){
			$this->render('view_of_summary');
		}else{
			header("Content-Type:   application/vnd.ms-excel; charset=utf-8");
			header("Content-type:   application/x-msexcel; charset=utf-8");
			header("Content-Disposition: attachment; filename=Attendance_all.xls"); 
			header("Expires: 0");
			header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
			header("Cache-Control: private",false);
			$this->layout='//layouts/admin/realy_blank';
			$this->render('view_of_summary');
			exit;
		}
	}

	
	public function actionViewSummaryPersonal($from,$to,$id)
	{
		if(isset($_POST['Attendance'])){
			$data=$_POST['Attendance']['shift_id'];
			foreach($data as $index=>$row){
				$update=$this->loadModel($index);
				$update->shift_id=$row;
				$shift=Shift::model()->findbyPk($row);
				if($shift->shift_att_in>$shift->shift_att_out){
					$ex=explode("-",$update->att_date);
					$nextTo=date("Y-m-d",mktime(0,0,0,$ex[1],$ex[2]+1,$ex[0]));
					$update->att_date_to=$nextTo;
				}else{
					$update->att_date_to=null;
				}
				if($update->att_date_to==""){
						$dataIN=AttendanceTemp::model()->find(array("condition"=>"att_date='$update->att_date' AND emp_id='".$update->attEmp->att_id."'","limit"=>1,"order"=>"att_time ASC"));
						$dataOUT=AttendanceTemp::model()->find(array("condition"=>"att_date='$update->att_date' AND emp_id='".$update->attEmp->att_id."'","limit"=>1,"order"=>"att_time DESC"));
						$update->att_in=$dataIN->att_time;
						$update->att_out=$dataOUT->att_time;
						$update->fp_id=$dataIN->fp_id;
						$update->att_update=date("Y-m-d H:i:s");
					}else{
						$dataIN=AttendanceTemp::model()->find(array("condition"=>"att_date='$update->att_date' AND emp_id='".$update->attEmp->att_id."'","limit"=>1,"order"=>"att_time DESC"));
						$dataOUT=AttendanceTemp::model()->find(array("condition"=>"att_date='$update->att_date_to' AND emp_id='".$update->attEmp->att_id."'","limit"=>1,"order"=>"att_time ASC"));
						$update->att_in=$dataIN->att_time;
						$update->att_out=$dataOUT->att_time;
						$update->fp_id=$dataIN->fp_id;
						$update->att_update=date("Y-m-d H:i:s");
					}
					$update->att_should_attend=$shift->shift_attend;
					if($update->att_should_attend=="No"){
						$update->att_in=null;
						$update->att_out=null;
						$update->fp_id=null;
					}
				$update->save();
				
			}
		}
		
		$model=Attendance::model()->findAll(array("condition"=>"att_date between :from AND :to AND att_emp_id=:id","params"=>array("from"=>$from,"to"=>$to,"id"=>$id),"order"=>"att_date ASC"));
		
		
		if(!isset($_GET['print'])){
			
				$this->render('view_personal_summary',array(
				'model'=>$model,
				));
		}else{
			if($_GET['finger']=="true"){
				$this->layout='//layouts/admin/pure_blank';
				$this->render('print_view_personal_summary_finger',array(
				'model'=>$model,
				));
			}else{
				$this->layout='//layouts/admin/pure_blank';
			$this->render('print_view_personal_summary',array(
			'model'=>$model,
			));
			}
			
		}
	}
	
	public function actionDownloadSelected($from,$to)
	{
		$emp=EmployeeGroupList::model()->findAll(array("condition"=>"egl_id IN(".$_POST['ids'].")","order"=>"RIGHT(emp_id,5) ASC"));
		$this->layout='//layouts/admin/pure_blank';
		foreach($emp as $row){
		$model=Attendance::model()->findAll(array("condition"=>"att_date between :from AND :to AND att_emp_id=:id","params"=>array("from"=>$from,"to"=>$to,"id"=>$row->emp->emp_id),"order"=>"att_date ASC"));
		
		
		$this->render('print_view_personal_summary_a4',array(
		'model'=>$model,"id"=>$row->emp_id
		));
		}
	}
	
	public function actionDownloadAttendance($from,$to)
	{
		$emp=Employee::model()->findAll(array("condition"=>"emp_status='Active'","order"=>"RIGHT(emp_id,5) ASC"));
		$this->layout='//layouts/admin/empty';
		foreach($emp as $row){
		$model=Attendance::model()->findAll(array("condition"=>"att_date between :from AND :to AND att_emp_id=:id","params"=>array("from"=>$from,"to"=>$to,"id"=>$row->emp_id),"order"=>"att_date ASC"));
		
		
		$this->render('print_view_personal_summary_a4',array(
		'model'=>$model,"id"=>$row->emp_id
		));
		}
	}
	
	public function actionPrintSummary()
	{
		$this->layout='//layouts/admin/empty';
		$this->render('print_view_of_summary');
	}
	/**
	* Creates a new model.
	* If creation is successful, the browser will be redirected to the 'view' page.
	*/
	
	public function actionPrintForm()
	{
		$model=new Attendance('insert');

		// Uncomment the following line if AJAX validation is needed
		$this->performAjaxValidation($model);

		if(isset($_POST['Attendance']))
		{
			$model=Employee::model()->findAll(array("condition"=>"emp_status='Active'","order"=>"RIGHT(emp_id,5) ASC"));
			$this->pageTitle="Form Attandance";
			$this->layout='//layouts/admin/empty';
			$this->render('form_print',array(
				'model'=>$model,
				));
			exit;
		}

		$this->render('print_form',array(
		'model'=>$model,
		));
	}
	
	public function actionCreate()
	{
		$model=new Attendance('insert');

		// Uncomment the following line if AJAX validation is needed
		$this->performAjaxValidation($model);

		if(isset($_POST['Attendance']))
		{
		$model->attributes=$_POST['Attendance'];
			if($model->att_status==""){
				$model->att_in=date("H:i:s");
				if($model->att_in!=""){
					$model->att_in=$_POST['Attendance']['att_in'];
				}
				if($model->att_out!=""){
					$model->att_out=$_POST['Attendance']['att_out'];
				}else{
					$model->att_out=null;
				}
			}else{		
				$model->att_in=null;
				$model->att_out=null;
			}
			$model->att_create=date("Y-m-d H:i:s");
			$model->att_update=date("Y-m-d H:i:s");
		
		if($model->save()){
			$this->redirect(array('view','periode'=>$model->att_date));
		}
		}

		$this->render('create',array(
		'model'=>$model,
		));
	}
	
	
	public function actionCreateByEmployee($id,$date)
	{
		$model=new Attendance('insert');

		// Uncomment the following line if AJAX validation is needed
		$this->performAjaxValidation($model);

		if(isset($_POST['Attendance']))
		{
		$model->attributes=$_POST['Attendance'];
		$model->att_emp_id=$id;
		$model->att_date=$date;
			if($model->att_status==""){
				$model->att_in=date("H:i:s");
				if($model->att_in!=""){
					$model->att_in=$_POST['Attendance']['att_in'];
				}
				if($model->att_out!=""){
					$model->att_out=$_POST['Attendance']['att_out'];
				}else{
					$model->att_out=null;
				}
			}else{		
				$model->att_in=null;
				$model->att_out=null;
			}
			$model->att_create=date("Y-m-d H:i:s");
			$model->att_update=date("Y-m-d H:i:s");
		
		if($model->save()){
			$this->redirect(array('view','periode'=>$model->att_date));
		}
		}

		$this->render('create_by_employee',array(
		'model'=>$model,
		));
	}
	

	public function actionCreateGroup()
	{
		$a="2016-09-01";
		$b="2016-09-30";
		$success=0;
		$failed=0;
		$day=Lib::selisih2tanggal($a,$b);
		$date=explode("-",$a);
		for($i=0;$i<=$day;$i++){
			$nmDay=Lib::getDay($next);
			$model=Employee::model()->findAll();
			foreach($model as $row){
				$next=date("Y-m-d",mktime(0,0,0,$date[1],$date[2]+$i,$date[0]));
				$new=Attendance::model()->find(array("condition"=>"att_emp_id='$row->emp_id' AND att_date='$next'"));
				if(empty($new)){
					$new=new Attendance;
				}
				$new->att_emp_id=$row->emp_id;
				$new->att_date=$next;
				if($new->save()){
					$success++;
				}else{
					$failed++;
				}
			}
		}
		
		echo $success;
		echo"<br/>";
		echo $failed;
	}

	/**
	* Updates a particular model.
	* If update is successful, the browser will be redirected to the 'view' page.
	* @param integer $id the ID of the model to be updated
	*/
	public function actionUpdateOne()
	{
		if(isset($_POST['id']) and !empty($_POST['value'])){
			$id=$_POST['id'];
			$val=$_POST['value'];
			$model=$this->loadModel($id);
			
			$model->shift_id=$val;
			$dtShift=Shift::model()->findbyPk($val);
			if($dtShift->shift_att_in>$dtShift->shift_att_out){
				$ex=explode("-",$tgl);
				$nextTo=date("Y-m-d",mktime(0,0,0,$ex[1],$ex[2]+1,$ex[0]));
				$model->att_date_to=$nextTo;
			}
			$model->save();
			echo $model->shift->shift_att_in.' - '.$model->shift->shift_att_out;
		}
	}
	
	public function actionCreateOne()
	{
		if(isset($_POST['id']) and !empty($_POST['value'] and !empty($_POST['tgl']))){
			$id=$_POST['id'];
			$tgl=$_POST['tgl'];
			$val=$_POST['value'];
			$model=Attendance::model()->find(array("condition"=>"att_emp_id='$id' AND att_date='$tgl'"));
			if(empty($model)){
				$model=new Attendance;
			}
			$dtShift=Shift::model()->findbyPk($val);
			if($dtShift->shift_att_in>$dtShift->shift_att_out){
				$ex=explode("-",$tgl);
				$nextTo=date("Y-m-d",mktime(0,0,0,$ex[1],$ex[2]+1,$ex[0]));
				$model->att_date_to=$nextTo;
			}
			$model->shift_id=$val;
			$model->att_emp_id=$id;
			$model->att_date=$tgl;
			$model->save();
			$array=array("data"=>array("att_id"=>$model->att_id,"value"=>$model->shift->shift_att_in.' - '.$model->shift->shift_att_out));
			
			echo json_encode($array);
		}
	}
	public function actionUpdate($id)
	{
		$model=$this->loadModel($id);

		// Uncomment the following line if AJAX validation is needed
		$this->performAjaxValidation($model);
		if(isset($_POST['Attendance']))
		{
		$model->attributes=$_POST['Attendance'];
			if($model->att_sts_id==""){
				$model->att_in=date("H:i:s");
				if($model->att_in!=""){
					$model->att_in=$_POST['Attendance']['att_in'];
				}
				if($model->att_out!=""){
					$model->att_out=$_POST['Attendance']['att_out'];
				}else{
					$model->att_out=null;
				}
				
				if($_POST['Attendance']['att_is_overtime']==1){
					$model->att_is_overtime=1;
					//upload dokumen lembur
					$model->att_overtime_docs=$_POST['Attendance']['att_overtime_docs'];
				}else{
					$model->att_is_overtime=0;
					$model->att_overtime_docs=null;
				}
				
				if($_POST['Attendance']['att_is_possible_late']==1){
					$model->att_is_possible_late=1;
					$model->att_reason_possible_late=$_POST['Attendance']['att_reason_possible_late'];
				}else{
					$model->att_is_possible_late=0;
					$model->att_reason_possible_late=null;
				}
				
				if($_POST['Attendance']['att_is_possible_quick_home']==1){
					$model->att_is_possible_quick_home=1;
					$model->att_reason_possible_quick_home=$_POST['Attendance']['att_reason_possible_quick_home'];
				}else{
					$model->att_is_possible_quick_home=0;
					$model->att_reason_possible_quick_home=null;
				}
			}else{		
				$model->att_note=$_POST['Attendance']['att_note'];
				$model->att_is_overtime=null;
				$model->att_overtime_docs=null;
				$model->att_is_possible_late=null;
				$model->att_reason_possible_late=null;
				$model->att_is_possible_quick_home=null;
				$model->att_reason_possible_quick_home=null;
				$model->att_in=null;
				$model->att_out=null;
			}

			$model->att_update=date("Y-m-d H:i:s");
			
		if($model->save()){
			if(isset($_GET['from']) AND isset($_GET['to']) AND !isset($_GET['back'])){
				$this->redirect(array('viewSummaryPersonal',"from"=>$_GET['from'],"to"=>$_GET['to'],"id"=>$model->att_emp_id));
			}elseif(isset($_GET['from']) AND isset($_GET['to']) AND isset($_GET['back'])){
				$this->redirect(array($_GET['back'],"from"=>$_GET['from'],"to"=>$_GET['to'],"id"=>$model->att_emp_id));
			}else{
				$this->redirect(array('view','periode'=>$model->att_date));
			}
		}
		}
		if(isset($_GET['from']) AND isset($_GET['to']) AND isset($_GET['back'])){
			$this->layout='//layouts/admin/empty';
		}
		$this->render('update',array(
		'model'=>$model,
		));
	}
	
	
	
	public function actionDoubleShift($id)
	{
		$model=$this->loadModel($id);
		if(isset($_POST['Attendance']))
		{
			$model->att_att_id=$_POST['Attendance']['att_att_id'];
			

			$model->att_update=date("Y-m-d H:i:s");
			
			if($model->save()){
				if(isset($_GET['from']) AND isset($_GET['to']) AND !isset($_GET['back'])){
					$this->redirect(array('viewSummaryPersonal',"from"=>$_GET['from'],"to"=>$_GET['to'],"id"=>$model->att_emp_id));
				}elseif(isset($_GET['from']) AND isset($_GET['to']) AND isset($_GET['back'])){
					$this->redirect(array($_GET['back'],"from"=>$_GET['from'],"to"=>$_GET['to'],"id"=>$model->att_emp_id));
				}else{
					$this->redirect(array('view','periode'=>$model->att_date));
				}
			}
		}
		if(isset($_GET['from']) AND isset($_GET['to']) AND isset($_GET['back'])){
			$this->layout='//layouts/admin/empty';
		}
		$this->render('double',array(
		'model'=>$model,
		));
	}
	
	
	public function actionOnCall($id)
	{
		$model=$this->loadModel($id);
		if(isset($_POST['Attendance']))
		{
			$model->oncall_is=1;
			$model->oncall_date=$_POST['Attendance']['oncall_date'];
			$model->oncall_date_to=$_POST['Attendance']['oncall_date_to'];
			$model->oncall_start=$_POST['Attendance']['oncall_start'];
			$model->oncall_end=$_POST['Attendance']['oncall_end'];
			

			$model->att_update=date("Y-m-d H:i:s");
			
			if($model->save()){
				$shift=Shift::model()->findbyPk($model->shift_id);
				$update=$this->loadModel($id);
				if($update->att_should_attend=="No" or $update->att_sts_id!=''){
					$update->att_in=null;
					$update->att_out=null;
					$update->fp_id=null;
				}else{
					if($update->att_date_to==""){
						$dataOUT=Attendance::checkOut($shift->shift_att_out,$update->att_date,$update->attEmp->att_id);
						$update->att_out=$dataOUT;
						$update->att_update=date("Y-m-d H:i:s");
					}else{
						
						$dataOUT=Attendance::checkOut($shift->shift_att_out,$update->att_date_to,$update->attEmp->att_id);
						$update->att_out=$dataOUT;
						$update->att_update=date("Y-m-d H:i:s");
					}
					
				}
				
				
				$update->save();
				
				if(isset($_GET['from']) AND isset($_GET['to']) AND !isset($_GET['back'])){
					$this->redirect(array('viewSummaryPersonal',"from"=>$_GET['from'],"to"=>$_GET['to'],"id"=>$model->att_emp_id));
				}elseif(isset($_GET['from']) AND isset($_GET['to']) AND isset($_GET['back'])){
					$this->redirect(array($_GET['back'],"from"=>$_GET['from'],"to"=>$_GET['to'],"id"=>$model->att_emp_id));
				}else{
					$this->redirect(array('view','periode'=>$model->att_date));
				}
			}
		}
		if(isset($_GET['from']) AND isset($_GET['to']) AND isset($_GET['back'])){
			$this->layout='//layouts/admin/empty';
		}
		$this->render('oncall',array(
		'model'=>$model,
		));
	}
	
	
	public function actionBackHome($id)
	{
		$model=$this->loadModel($id);
		$model->att_out=date("H:i:s");
		
		if($model->save()){
			$this->redirect(array('view','periode'=>$model->att_date));
		}
	}

	/**
	* Deletes a particular model.
	* If deletion is successful, the browser will be redirected to the 'admin' page.
	* @param integer $id the ID of the model to be deleted
	*/
	public function actionDelete($id)
	{
		if(Yii::app()->request->isPostRequest)
		{
		// we only allow deletion via POST request
		$model=$this->loadModel($id);
		$this->loadModel($id)->delete();

		// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
		if(!isset($_GET['ajax']))
		$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
		}
		else
		throw new CHttpException(400,'Invalid request. Please do not repeat this request again.');
	}

	/**
	* Lists all models.
	*/
	public function actionAdmin()
	{
		$dataProvider=new CActiveDataProvider('Attendance');
		$this->render('index',array(
		'dataProvider'=>$dataProvider,
		));
	}

	/**
	* Manages all models.
	*/
	public function actionIndex()
	{
		$model=new Attendance('searchGroup');
		$model->unsetAttributes();  // clear any default values
		if(isset($_GET['Attendance']))
		$model->attributes=$_GET['Attendance'];
		if (isset($_GET['pageSize'])) {
		//user()->setState('pageSize',(int)$_GET['pageSize']);  //this doesn't work for me
		Yii::app()->user->setState('pageSize',(int)$_GET['pageSize']);   //use Yii::app()->user
		unset($_GET['pageSize']);  // would interfere with pager and repetitive page size change
		}
		$this->render('admin',array(
		'model'=>$model,
		));
	}
	
	public function actionBatchDelete()
	{
		$request = Yii::app()->getRequest();
		if($request->getIsPostRequest()){
			if(isset($_POST['ids'])){
				$ids = $_POST['ids'];
			}
			if (empty($ids)) {
				echo CJSON::encode(array('status' => 'failure', 'msg' => 'you should at least choice one item'));
				die();
			}
			$successCount = $failureCount = 0;
			$dataError=array();
			foreach ($ids as $id) {
				$model = $this->loadModel($id);
				$temp = $this->loadModel($id);
				if($model->delete()){
					$successCount++ ;
				}else{
					$failureCount++;
					$dataError[]=$model->att_id;
				}
			}
			echo CJSON::encode(array('status' => 'success',
				'data' => array(
					'successCount' => $successCount,
					'failureCount' => $failureCount,
					'dataError' => $dataError,
				)));
			die();
		}else{
			throw new CHttpException(400,'Invalid request. Please do not repeat this request again.');
		}
	}
	
	public function actionExportExcel()
	{
		ini_set("memory_limit","8056M");
		set_time_limit(100000);
		Yii::import('ext.phpexcel.XPHPExcel');    
		$objPHPExcel= XPHPExcel::createPHPExcel();
		$objPHPExcel->getProperties()->setCreator("Abdurab")
		->setLastModifiedBy("Abdurab")
		->setTitle("Dokumen Rahasia")
		->setSubject("Dokumen Rahasia")
		->setDescription("Abdurab")
		->setKeywords("Abdurab")
		->setCategory("Abdurab");
		$model=new Attendance;

				$objPHPExcel->setActiveSheetIndex(0)->setCellValue('A1',"No");
		$objPHPExcel->setActiveSheetIndex(0)->setCellValue('B1',$model->getAttributeLabel('att_id'));
		$objPHPExcel->setActiveSheetIndex(0)->setCellValue('C1',$model->getAttributeLabel('att_shift_id'));
		$objPHPExcel->setActiveSheetIndex(0)->setCellValue('D1',$model->getAttributeLabel('att_emp_id'));
		$objPHPExcel->setActiveSheetIndex(0)->setCellValue('E1',$model->getAttributeLabel('att_date'));
		$objPHPExcel->setActiveSheetIndex(0)->setCellValue('F1',$model->getAttributeLabel('att_in'));
		$objPHPExcel->setActiveSheetIndex(0)->setCellValue('G1',$model->getAttributeLabel('att_out'));
		$objPHPExcel->setActiveSheetIndex(0)->setCellValue('H1',$model->getAttributeLabel('att_status'));
		$objPHPExcel->setActiveSheetIndex(0)->setCellValue('I1',$model->getAttributeLabel('att_create'));
		$objPHPExcel->setActiveSheetIndex(0)->setCellValue('J1',$model->getAttributeLabel('att_update'));
		
		$criteria=new CDbCriteria;
		$criteria->addCondition("att_id IN (".$_POST['ids'].")");
		$data= Attendance::model()->findAll($criteria);    
		$j=1;
		$no=0;
		foreach($data as $row) {
			$j++;
			$no++;
						$objPHPExcel->setActiveSheetIndex(0)->setCellValue('A'.$j, $no);
			$objPHPExcel->setActiveSheetIndex(0)->setCellValue('B'.$j, $row->att_id);
			$objPHPExcel->setActiveSheetIndex(0)->setCellValue('C'.$j, $row->att_shift_id);
			$objPHPExcel->setActiveSheetIndex(0)->setCellValue('D'.$j, $row->att_emp_id);
			$objPHPExcel->setActiveSheetIndex(0)->setCellValue('E'.$j, $row->att_date);
			$objPHPExcel->setActiveSheetIndex(0)->setCellValue('F'.$j, $row->att_in);
			$objPHPExcel->setActiveSheetIndex(0)->setCellValue('G'.$j, $row->att_out);
			$objPHPExcel->setActiveSheetIndex(0)->setCellValue('H'.$j, $row->att_status);
			$objPHPExcel->setActiveSheetIndex(0)->setCellValue('I'.$j, $row->att_create);
			$objPHPExcel->setActiveSheetIndex(0)->setCellValue('J'.$j, $row->att_update);
		}
		$styleArray = array(
			'font' => array(
				'bold' => true,
			),
		);
		$styleArrayData = array(
			'borders' => array(
				'allborders' => array(
					'style' => PHPExcel_Style_Border::BORDER_THIN
				),
			),
		);
		$objPHPExcel->getActiveSheet()->getStyle('A1:J10')->applyFromArray($styleArray);
		$objPHPExcel->getActiveSheet()->getStyle('A1:J'.$j)->applyFromArray($styleArrayData);
		// Rename worksheet
		$objPHPExcel->getActiveSheet()->setTitle('Default');

		// Set active sheet index to the first sheet, so Excel opens this as the first sheet
		$objPHPExcel->setActiveSheetIndex(0);
		 		 
		// Redirect output to a client�s web browser (Excel5)
		header('Content-Type: application/vnd.ms-excel');
		header('Content-Disposition: attachment;filename="Template Attendance '.date('Y-m-d H:i:s').'.xls"');
		header('Cache-Control: max-age=0');
		// If you're serving to IE 9, then the following may be needed
		header('Cache-Control: max-age=1');
		 
		// If you're serving to IE over SSL, then the following may be needed
		header ('Expires: Mon, 26 Jul 1997 05:00:00 GMT'); // Date in the past
		header ('Last-Modified: '.gmdate('D, d M Y H:i:s').' GMT'); // always modified
		header ('Cache-Control: cache, must-revalidate'); // HTTP/1.1
		header ('Pragma: public'); // HTTP/1.0
		 
		$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
		$objWriter->save('php://output');
	    Yii::app()->end();
	}
	
	public function actionExportPdf()
	{
		Yii::import('ext.MPDF57.*');
		include("mpdf.php");
		ini_set("memory_limit","8056M");
		set_time_limit(100000);
		$mpdf=new mPDF('utf-8', 'A4-L', 0, null, 3,3,10,20,15,15 );
		
		$model=new Attendance;
		
		$criteria=new CDbCriteria;
		$criteria->addCondition("att_id IN (".$_POST['ids'].")");
		$data= Attendance::model()->findAll($criteria); 
		
		$j = 1;
		$no=0;
		
		$html='
		<style type="text/css" media="print,screen" >
		th {
			font-family:Arial;
			color:black;
			background-color:lightgrey;
			padding:5px;
		}
		td{
			padding:5px;
		}
		thead {
			display:table-header-group;
		}
		tbody {
			display:table-row-group;
		}
		</style>
		<h2 style="text-align: center;">Attendance</h2>
		<br>
		<table border="1" style="border:1px; width:100%; vertical-align: text-top;border-collapse: collapse;">
			<thead>
				<tr>
					<th>No</th>
										<th>'.$model->getAttributeLabel('att_id').'</th>
					<th>'.$model->getAttributeLabel('att_shift_id').'</th>
					<th>'.$model->getAttributeLabel('att_emp_id').'</th>
					<th>'.$model->getAttributeLabel('att_date').'</th>
					<th>'.$model->getAttributeLabel('att_in').'</th>
					<th>'.$model->getAttributeLabel('att_out').'</th>
					<th>'.$model->getAttributeLabel('att_status').'</th>
					<th>'.$model->getAttributeLabel('att_create').'</th>
					<th>'.$model->getAttributeLabel('att_update').'</th>
				</tr>
			</thead>
			<tbody>';
			
			$no=0;
			foreach($data as $row){
			$no++;
			$html.='
				<tr>
					<td>'.$no.'</td>
										<td>'.$row->att_id.'</td>
					<td>'.$row->att_shift_id.'</td>
					<td>'.$row->att_emp_id.'</td>
					<td>'.$row->att_date.'</td>
					<td>'.$row->att_in.'</td>
					<td>'.$row->att_out.'</td>
					<td>'.$row->att_status.'</td>
					<td>'.$row->att_create.'</td>
					<td>'.$row->att_update.'</td>
				</tr>';
			}	
			$html.='
			</tbody>
		</table>	
		';
		ini_set("memory_limit","256M");
		$mpdf->Bookmark('Start of the document');
		$mpdf->SetDisplayMode('fullpage','two');
		$mpdf->setFooter('Tanggal {DATE j-m-Y}||Halaman {PAGENO} dari {nbpg}') ;
		$mpdf->simpleTables = true;
		$mpdf->WriteHTML('<sheet-size="Letter" />');
		$mpdf->WriteHTML($html);
		$mpdf->Output();
	}
	
	public function actionImport()
	{
		ini_set("memory_limit","8056M");
		set_time_limit(100000);
		$model=new Attendance;
		if(isset($_POST['Attendance']))
		{
		$fileUpload=CUploadedFile::getInstance($model,'import');
		$nama=date('YmdHis').'_'.$fileUpload;
		$path=Yii::app()->basePath . '/../temp_excel/'.$nama.'';
		$fileUpload->saveAs($path);
			
		Yii::import('ext.phpexcelreader.JPhpExcelReader');
		$data=new JPhpExcelReader($path);
		$baris=$data->sheets[0][numRows];
		$kolom=$data->sheets[0][numCols];
		$sukses=0;
		$gagal=0;
		for($i=2;$i<=$baris; $i++){
			$model=new Attendance;
						$model->att_id= $data->sheets[0]['cells'][$i][1];
			$model->att_shift_id= $data->sheets[0]['cells'][$i][2];
			$model->att_emp_id= $data->sheets[0]['cells'][$i][3];
			$model->att_date= $data->sheets[0]['cells'][$i][4];
			$model->att_in= $data->sheets[0]['cells'][$i][5];
			$model->att_out= $data->sheets[0]['cells'][$i][6];
			$model->att_status= $data->sheets[0]['cells'][$i][7];
			$model->att_create= $data->sheets[0]['cells'][$i][8];
			$model->att_update= $data->sheets[0]['cells'][$i][9];
			$model->create_at=date('Y-m-d H:i:s');
			$model->modified_at=date('Y-m-d H:i:s');
			$cek=Attendance::model()->findbyPk($data->sheets[0]['cells'][$i][1]);
			if(count($cek)==0){
				if ($model->save()){
				$sukses++;
				}else{
				$gagal++;
				}
			} else {
				$gagal++;
			}
		}
		@unlink($path);
		 Yii::app()->user->setFlash('success', "Proses import data selesai<br/>
         Jumlah data yang sukses diimport : ".$sukses." Jumlah data yang gagal diimport : ".$gagal."
         ");
		$this->redirect(array('index'));
		}
	}
	
	public function actionDownloadTemplate(){
		Yii::import('ext.phpexcel.XPHPExcel');    
		$objPHPExcel= XPHPExcel::createPHPExcel();
		$objPHPExcel->getProperties()->setCreator("Abdurab")
		->setLastModifiedBy("Abdurab")
		->setTitle("Dokumen Rahasia")
		->setSubject("Dokumen Rahasia")
		->setDescription("Abdurab")
		->setKeywords("Abdurab")
		->setCategory("Abdurab");
		$model=new Attendance;
		//loop here		
				$objPHPExcel->setActiveSheetIndex(0)->setCellValue('A1',$model->getAttributeLabel('att_id'));
		$objPHPExcel->setActiveSheetIndex(0)->setCellValue('B1',$model->getAttributeLabel('att_shift_id'));
		$objPHPExcel->setActiveSheetIndex(0)->setCellValue('C1',$model->getAttributeLabel('att_emp_id'));
		$objPHPExcel->setActiveSheetIndex(0)->setCellValue('D1',$model->getAttributeLabel('att_date'));
		$objPHPExcel->setActiveSheetIndex(0)->setCellValue('E1',$model->getAttributeLabel('att_in'));
		$objPHPExcel->setActiveSheetIndex(0)->setCellValue('F1',$model->getAttributeLabel('att_out'));
		$objPHPExcel->setActiveSheetIndex(0)->setCellValue('G1',$model->getAttributeLabel('att_status'));
		$objPHPExcel->setActiveSheetIndex(0)->setCellValue('H1',$model->getAttributeLabel('att_create'));
		$objPHPExcel->setActiveSheetIndex(0)->setCellValue('I1',$model->getAttributeLabel('att_update'));
		//loop here	
		
		$data= Attendance::model()->findAll(array('limit'=>10));    
		$j = 1;
		$no=0;
		foreach($data as $row) {
			$j++;
			$no++;
					$objPHPExcel->setActiveSheetIndex(0)->setCellValue('A'.$j, $row->att_id);
		$objPHPExcel->setActiveSheetIndex(0)->setCellValue('B'.$j, $row->att_shift_id);
		$objPHPExcel->setActiveSheetIndex(0)->setCellValue('C'.$j, $row->att_emp_id);
		$objPHPExcel->setActiveSheetIndex(0)->setCellValue('D'.$j, $row->att_date);
		$objPHPExcel->setActiveSheetIndex(0)->setCellValue('E'.$j, $row->att_in);
		$objPHPExcel->setActiveSheetIndex(0)->setCellValue('F'.$j, $row->att_out);
		$objPHPExcel->setActiveSheetIndex(0)->setCellValue('G'.$j, $row->att_status);
		$objPHPExcel->setActiveSheetIndex(0)->setCellValue('H'.$j, $row->att_create);
		$objPHPExcel->setActiveSheetIndex(0)->setCellValue('I'.$j, $row->att_update);
		}
		$styleArray = array(
			'font' => array(
				'bold' => true,
			),
		);
		$styleArrayData = array(
			'borders' => array(
				'allborders' => array(
					'style' => PHPExcel_Style_Border::BORDER_THIN
				),
			),
		);
		$objPHPExcel->getActiveSheet()->getStyle('A1:I9')->applyFromArray($styleArray);
		$objPHPExcel->getActiveSheet()->getStyle('A1:I'.$j)->applyFromArray($styleArrayData);
		// Rename worksheet
		$objPHPExcel->getActiveSheet()->setTitle('Default');

		// Set active sheet index to the first sheet, so Excel opens this as the first sheet
		$objPHPExcel->setActiveSheetIndex(0);
		 		 
		// Redirect output to a client�s web browser (Excel5)
		header('Content-Type: application/vnd.ms-excel');
		header('Content-Disposition: attachment;filename="Template Attendance '.date('Y-m-d H:i:s').'.xls"');
		header('Cache-Control: max-age=0');
		// If you're serving to IE 9, then the following may be needed
		header('Cache-Control: max-age=1');
		 
		// If you're serving to IE over SSL, then the following may be needed
		header ('Expires: Mon, 26 Jul 1997 05:00:00 GMT'); // Date in the past
		header ('Last-Modified: '.gmdate('D, d M Y H:i:s').' GMT'); // always modified
		header ('Cache-Control: cache, must-revalidate'); // HTTP/1.1
		header ('Pragma: public'); // HTTP/1.0
		 
		$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
		$objWriter->save('php://output');
	    Yii::app()->end();
	}
	
	
	
	
	/**
	* Returns the data model based on the primary key given in the GET variable.
	* If the data model is not found, an HTTP exception will be raised.
	* @param integer the ID of the model to be loaded
	*/
	public function loadModel($id)
	{
		$model=Attendance::model()->findByPk($id);
		if($model===null)
		throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}

	/**
	* Performs the AJAX validation.
	* @param CModel the model to be validated
	*/
	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='attendance-form')
		{
		echo CActiveForm::validate($model);
		Yii::app()->end();
		}
	}
}
