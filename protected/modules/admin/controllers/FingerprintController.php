<?php
class FingerprintController extends Controller
{
	/**
	* @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	* using two-column layout. See 'protected/views/layouts/column2.php'.
	*/
	public $layout='//layouts/admin/main';

	/**
	* @return array action filters
	*/
	public function filters()
	{
		return array(
		'accessControl', // perform access control for CRUD operations
		);
	}

	/**
	* Specifies the access control rules.
	* This method is used by the 'accessControl' filter.
	* @return array access control rules
	*/
	public function accessRules()
	{
		return array(
		array('allow',  // allow all users to perform 'index' and 'view' actions
		'actions'=>array('index','view','cekStatus'),
		'expression'=>'$user->getAuth()',
		),
		array('allow', // allow admin user to perform 'admin' and 'delete' actions
		'actions'=>array('create','update','admin','delete','BatchDelete','exportExcel','exportPdf','import','downloadTemplate','downloadData'),
		'expression'=>'$user->getAuth()',
		),

		array('deny',  // deny all users
		'users'=>array('*'),
		),
		);
	}

	/**
	* Displays a particular model.
	* @param integer $id the ID of the model to be displayed
	*/
	public function actionCekStatus(){
		
		$model=$this->loadModel($_POST['id']);
		$IP=$model->fp_ip_address;
		$Key="0";
		$Connect = fsockopen($IP, "80", $errno, $errstr, 1);
		if($Connect){
			echo'Success';
		}else {
			echo "Failed";
		}
	}
	public function actionView($id)
	{
		$model=new AttendanceTemp('search');
		$model->unsetAttributes();  // clear any default values
		$model->fp_id=$id;  // clear any default values
		if(isset($_GET['AttendanceTemp']))
		$model->attributes=$_GET['AttendanceTemp'];
		if (isset($_GET['pageSize'])) {
			Yii::app()->user->setState('pageSize',(int)$_GET['pageSize']); 
			unset($_GET['pageSize']);
		}
		
		$this->render('view',array(
		'model'=>$this->loadModel($id),'data'=>$model
		));
		
	}
	
	public function actionDownloadData(){
		$form=new Fingerprint;
		if(isset($_POST['Fingerprint'])){
			$a=$_POST['Fingerprint']['fp_name'];
			$b=$_POST['Fingerprint']['fp_location'];
			$form->fp_name=$a;
			$form->fp_location=$b;
			$form->fp_id=$_POST['Fingerprint']['fp_id'];
			$day=Lib::selisih2tanggal($a,$b);
			$date=explode("-",$a);
			$onlyThis=array();
			for($i=0;$i<=$day;$i++){
				$onlyThis[]=date("Y-m-d",mktime(0,0,0,$date[1],$date[2]+$i,$date[0]));
			}
			$onlyThis[]="GetAttLogResponse";
			$model=$this->loadModel($_POST['Fingerprint']['fp_id']);
			$IP=$model->fp_ip_address;
			$Key="0";
			$Connect = fsockopen($IP, "80", $errno, $errstr, 1);
			if($Connect){
				$soap_request="<GetAttLog><ArgComKey xsi:type=\"xsd:integer\">".$Key."</ArgComKey><Arg><PIN xsi:type=\"xsd:integer\">All</PIN></Arg></GetAttLog>";
				$newLine="\r\n";
				fputs($Connect, "POST /iWsService HTTP/1.0".$newLine);
				fputs($Connect, "Content-Type: text/xml".$newLine);
				fputs($Connect, "Content-Length: ".strlen($soap_request).$newLine.$newLine);
				fputs($Connect, $soap_request.$newLine);
				$buffer="";
				while($Response=fgets($Connect, 1024)){
						if(!empty($Response)){
							$cek=Lib::strpos_arr($Response, $onlyThis);
							$cek=trim($cek);
							if($cek!=""){
								$buffer.=$Response;
							}
						}
				}
				
				$buffer=Lib::Parse_Data($buffer,"<GetAttLogResponse>","</GetAttLogResponse>");
				$buffer=explode("\r\n",$buffer);
				$fp=Fingerprint::model()->find(array("condition"=>"fp_ip_address='$IP'"));
				
				$update=0;
				$create=0;
				$values=array();
				for($a=0;$a<count($buffer);$a++){
					$data=Lib::Parse_Data($buffer[$a],"<Row>","</Row>");
					$PIN=Lib::Parse_Data($data,"<PIN>","</PIN>");
					$DateTime=Lib::Parse_Data($data,"<DateTime>","</DateTime>");
					$time=explode(" ",$DateTime);
					$Verified=Lib::Parse_Data($data,"<Verified>","</Verified>");
					$Status=Lib::Parse_Data($data,"<Status>","</Status>");
					
					if(!empty($PIN)){
						$cek=AttendanceTemp::model()->find(array("condition"=>"fp_id='$fp->fp_id' AND emp_id='$PIN' AND att_date='$time[0]' AND att_time='$time[1]'"));
						
						if(count($cek)==0){
							$values[]="('".$fp->fp_id."','".$PIN."','".$time[0]."','".$time[1]."')";
							$create++;
						}else{
							$values[]="('".$fp->fp_id."','".$PIN."','".$time[0]."','".$time[1]."')";
							$update++;
						}
					}
				}
				$sql = 'INSERT INTO attendance_temp (fp_id, emp_id, att_date, att_time) VALUES '.implode(",",$values);
				
				if(!empty($values)){
				$command = Yii::app()->db->createCommand($sql);
				$command->execute();
				Yii::app()->user->setFlash('result', "Download Success !<br/>$create Data Create, $update Data Have Been In Database");
				}else{
				Yii::app()->user->setFlash('result', "No Data to Process");	
				}
			}else {
				echo "Koneksi Gagal";
			}
			
		}
		
		$this->render('download_attendance',array(
		'model'=>$form
		));
	}

	/**
	* Creates a new model.
	* If creation is successful, the browser will be redirected to the 'view' page.
	*/
	public function actionCreate()
	{
		$model=new Fingerprint;

		// Uncomment the following line if AJAX validation is needed
		$this->performAjaxValidation($model);

		if(isset($_POST['Fingerprint']))
		{
			$model->attributes=$_POST['Fingerprint'];
			if($model->save()){
				$this->redirect(array('view','id'=>$model->fp_id));
			}
		}

		$this->render('create',array(
		'model'=>$model,
		));
	}

	/**
	* Updates a particular model.
	* If update is successful, the browser will be redirected to the 'view' page.
	* @param integer $id the ID of the model to be updated
	*/
	public function actionUpdate($id)
	{
		$model=$this->loadModel($id);

		// Uncomment the following line if AJAX validation is needed
		$this->performAjaxValidation($model);
	
		if(isset($_POST['Fingerprint']))
		{
			$model->attributes=$_POST['Fingerprint'];
			if($model->save()){
				$this->redirect(array('view','id'=>$model->fp_id));
			}
		}

		$this->render('update',array(
		'model'=>$model,
		));
	}

	/**
	* Deletes a particular model.
	* If deletion is successful, the browser will be redirected to the 'admin' page.
	* @param integer $id the ID of the model to be deleted
	*/
	public function actionDelete($id)
	{
		if(Yii::app()->request->isPostRequest)
		{
		// we only allow deletion via POST request		
		$this->loadModel($id)->delete();

		// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
		if(!isset($_GET['ajax']))
		$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
		}
		else
		throw new CHttpException(400,'Invalid request. Please do not repeat this request again.');
	}

	/**
	* Lists all models.
	*/
	public function actionAdmin()
	{
		$dataProvider=new CActiveDataProvider('Fingerprint');
		$this->render('index',array(
		'dataProvider'=>$dataProvider,
		));
	}

	/**
	* Manages all models.
	*/
	public function actionIndex()
	{
		$model=new Fingerprint('search');
		$model->unsetAttributes();  // clear any default values
		if(isset($_GET['Fingerprint']))
		$model->attributes=$_GET['Fingerprint'];
		if (isset($_GET['pageSize'])) {
			Yii::app()->user->setState('pageSize',(int)$_GET['pageSize']); 
			unset($_GET['pageSize']);
		}
		$this->render('admin',array(
		'model'=>$model,
		));
	}
	
	public function actionBatchDelete()
	{
		$request = Yii::app()->getRequest();
		if($request->getIsPostRequest()){
			if(isset($_POST['ids'])){
				$ids = $_POST['ids'];
			}
			if (empty($ids)) {
				echo CJSON::encode(array('status' => 'failure', 'msg' => 'you should at least choice one item'));
				die();
			}
			$successCount = $failureCount = 0;
			$dataError=array();
			foreach ($ids as $id) {
				$model = $this->loadModel($id);
				$temp = $this->loadModel($id);
				if($model->delete()){
					$successCount++ ;
				}else{
					$failureCount++;
					$dataError[]=$model->fp_id;
				}
			}
			echo CJSON::encode(array('status' => 'success',
				'data' => array(
					'successCount' => $successCount,
					'failureCount' => $failureCount,
					'dataError' => $dataError,
				)));
			die();
		}else{
			throw new CHttpException(400,'Invalid request. Please do not repeat this request again.');
		}
	}
	
	public function actionExportExcel()
	{
		ini_set("memory_limit","8056M");
		set_time_limit(100000);
		Yii::import('ext.phpexcel.XPHPExcel');    
		$objPHPExcel= XPHPExcel::createPHPExcel();
		$objPHPExcel->getProperties()->setCreator("File")
		->setLastModifiedBy("File")
		->setTitle("Dokumen")
		->setSubject("Dokumen")
		->setDescription("File")
		->setKeywords("File")
		->setCategory("File");
		$model=new Fingerprint;

		$objPHPExcel->setActiveSheetIndex(0)->setCellValue('A1',"No");
		$objPHPExcel->setActiveSheetIndex(0)->setCellValue('B1',$model->getAttributeLabel('fp_id'));
		$objPHPExcel->setActiveSheetIndex(0)->setCellValue('C1',$model->getAttributeLabel('fp_name'));
		$objPHPExcel->setActiveSheetIndex(0)->setCellValue('D1',$model->getAttributeLabel('fp_location'));
		$objPHPExcel->setActiveSheetIndex(0)->setCellValue('E1',$model->getAttributeLabel('fp_ip_address'));
		$objPHPExcel->setActiveSheetIndex(0)->setCellValue('F1',$model->getAttributeLabel('fp_create'));
		$objPHPExcel->setActiveSheetIndex(0)->setCellValue('G1',$model->getAttributeLabel('fp_update'));
		
		$criteria=new CDbCriteria;
		$criteria->addCondition("fp_id IN (".$_POST['ids'].")");
		$data= Fingerprint::model()->findAll($criteria);    
		$j=1;
		$no=0;
		foreach($data as $row) {
			$j++;
			$no++;
			$objPHPExcel->setActiveSheetIndex(0)->setCellValue('A'.$j, $no);
			$objPHPExcel->setActiveSheetIndex(0)->setCellValue('B'.$j, $row->fp_id);
			$objPHPExcel->setActiveSheetIndex(0)->setCellValue('C'.$j, $row->fp_name);
			$objPHPExcel->setActiveSheetIndex(0)->setCellValue('D'.$j, $row->fp_location);
			$objPHPExcel->setActiveSheetIndex(0)->setCellValue('E'.$j, $row->fp_ip_address);
			$objPHPExcel->setActiveSheetIndex(0)->setCellValue('F'.$j, $row->fp_create);
			$objPHPExcel->setActiveSheetIndex(0)->setCellValue('G'.$j, $row->fp_update);
		}
		$styleArray = array(
			'font' => array(
				'bold' => true,
			),
		);
		$styleArrayData = array(
			'borders' => array(
				'allborders' => array(
					'style' => PHPExcel_Style_Border::BORDER_THIN
				),
			),
		);
		$objPHPExcel->getActiveSheet()->getStyle('A1:G7')->applyFromArray($styleArray);
		$objPHPExcel->getActiveSheet()->getStyle('A1:G'.$j)->applyFromArray($styleArrayData);
		// Rename worksheet
		$objPHPExcel->getActiveSheet()->setTitle('Default');

		// Set active sheet index to the first sheet, so Excel opens this as the first sheet
		$objPHPExcel->setActiveSheetIndex(0);
		 		 
		// Redirect output to a client�s web browser (Excel5)
		header('Content-Type: application/vnd.ms-excel');
		header('Content-Disposition: attachment;filename="Template Fingerprint '.date('Y-m-d H:i:s').'.xls"');
		header('Cache-Control: max-age=0');
		// If you're serving to IE 9, then the following may be needed
		header('Cache-Control: max-age=1');
		 
		// If you're serving to IE over SSL, then the following may be needed
		header ('Expires: Mon, 26 Jul 1997 05:00:00 GMT'); // Date in the past
		header ('Last-Modified: '.gmdate('D, d M Y H:i:s').' GMT'); // always modified
		header ('Cache-Control: cache, must-revalidate'); // HTTP/1.1
		header ('Pragma: public'); // HTTP/1.0
		 
		$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
		$objWriter->save('php://output');
	    Yii::app()->end();
	}
	
	public function actionExportPdf()
	{
		Yii::import('ext.MPDF57.*');
		include("mpdf.php");
		ini_set("memory_limit","8056M");
		set_time_limit(100000);
		$mpdf=new mPDF('utf-8', 'A4-L', 0, null, 3,3,10,20,15,15 );
		
		$model=new Fingerprint;
		
		$criteria=new CDbCriteria;
		$criteria->addCondition("fp_id IN (".$_POST['ids'].")");
		$data= Fingerprint::model()->findAll($criteria); 
		
		$j = 1;
		$no=0;
		
		$html='
		<style type="text/css" media="print,screen" >
		th {
			font-family:Arial;
			color:black;
			background-color:lightgrey;
			padding:5px;
		}
		td{
			padding:5px;
		}
		thead {
			display:table-header-group;
		}
		tbody {
			display:table-row-group;
		}
		</style>
		<h2 style="text-align: center;">Fingerprint</h2>
		<br>
		<table border="1" style="border:1px; width:100%; vertical-align: text-top;border-collapse: collapse;">
			<thead>
				<tr>
					<th>No</th>
										<th>'.$model->getAttributeLabel('fp_id').'</th>
					<th>'.$model->getAttributeLabel('fp_name').'</th>
					<th>'.$model->getAttributeLabel('fp_location').'</th>
					<th>'.$model->getAttributeLabel('fp_ip_address').'</th>
					<th>'.$model->getAttributeLabel('fp_create').'</th>
					<th>'.$model->getAttributeLabel('fp_update').'</th>
				</tr>
			</thead>
			<tbody>';
			
			$no=0;
			foreach($data as $row){
			$no++;
			$html.='
				<tr>
					<td>'.$no.'</td>
										<td>'.$row->fp_id.'</td>
					<td>'.$row->fp_name.'</td>
					<td>'.$row->fp_location.'</td>
					<td>'.$row->fp_ip_address.'</td>
					<td>'.$row->fp_create.'</td>
					<td>'.$row->fp_update.'</td>
				</tr>';
			}	
			$html.='
			</tbody>
		</table>	
		';
		ini_set("memory_limit","256M");
		$mpdf->Bookmark('Start of the document');
		$mpdf->SetDisplayMode('fullpage','two');
		$mpdf->setFooter('Tanggal {DATE j-m-Y}||Halaman {PAGENO} dari {nbpg}') ;
		$mpdf->simpleTables = true;
		$mpdf->WriteHTML('<sheet-size="Letter" />');
		$mpdf->WriteHTML($html);
		$mpdf->Output();
	}
	
	public function actionImport()
	{
		ini_set("memory_limit","8056M");
		set_time_limit(100000);
		$model=new Fingerprint;
		if(isset($_POST['Fingerprint']))
		{
		$fileUpload=CUploadedFile::getInstance($model,'import');
		$nama=date('YmdHis').'_'.$fileUpload;
		$path=Yii::app()->basePath . '/../temp_excel/'.$nama.'';
		$fileUpload->saveAs($path);
			
		Yii::import('ext.phpexcelreader.JPhpExcelReader');
		$data=new JPhpExcelReader($path);
		$baris=$data->sheets[0][numRows];
		$kolom=$data->sheets[0][numCols];
		$sukses=0;
		$gagal=0;
		for($i=2;$i<=$baris; $i++){
			$model=new Fingerprint;
						$model->fp_id= $data->sheets[0]['cells'][$i][1];
			$model->fp_name= $data->sheets[0]['cells'][$i][2];
			$model->fp_location= $data->sheets[0]['cells'][$i][3];
			$model->fp_ip_address= $data->sheets[0]['cells'][$i][4];
			$model->fp_create= $data->sheets[0]['cells'][$i][5];
			$model->fp_update= $data->sheets[0]['cells'][$i][6];
			$cek=Fingerprint::model()->findbyPk($data->sheets[0]['cells'][$i][1]);
			if(count($cek)==0){
				if ($model->save()){
				$sukses++;
				}else{
				$gagal++;
				}
			} else {
				$gagal++;
			}
		}
		@unlink($path);
		 Yii::app()->user->setFlash('success', "Proses import data selesai<br/>
         Jumlah data yang sukses diimport : ".$sukses." Jumlah data yang gagal diimport : ".$gagal."
         ");
		$this->redirect(array('index'));
		}
	}
	
	public function actionDownloadTemplate(){
		Yii::import('ext.phpexcel.XPHPExcel');    
		$objPHPExcel= XPHPExcel::createPHPExcel();
		$objPHPExcel->getProperties()->setCreator("File")
		->setLastModifiedBy("File")
		->setTitle("Dokumen Rahasia")
		->setSubject("Dokumen Rahasia")
		->setDescription("File")
		->setKeywords("File")
		->setCategory("File");
		$model=new Fingerprint;
		//loop here		
				$objPHPExcel->setActiveSheetIndex(0)->setCellValue('A1',$model->getAttributeLabel('fp_id'));
		$objPHPExcel->setActiveSheetIndex(0)->setCellValue('B1',$model->getAttributeLabel('fp_name'));
		$objPHPExcel->setActiveSheetIndex(0)->setCellValue('C1',$model->getAttributeLabel('fp_location'));
		$objPHPExcel->setActiveSheetIndex(0)->setCellValue('D1',$model->getAttributeLabel('fp_ip_address'));
		$objPHPExcel->setActiveSheetIndex(0)->setCellValue('E1',$model->getAttributeLabel('fp_create'));
		$objPHPExcel->setActiveSheetIndex(0)->setCellValue('F1',$model->getAttributeLabel('fp_update'));
		
		$data= Fingerprint::model()->findAll(array('limit'=>10));    
		$j = 1;
		$no=0;
		foreach($data as $row) {
			$j++;
			$no++;
						$objPHPExcel->setActiveSheetIndex(0)->setCellValue('A'.$j, $row->fp_id);
			$objPHPExcel->setActiveSheetIndex(0)->setCellValue('B'.$j, $row->fp_name);
			$objPHPExcel->setActiveSheetIndex(0)->setCellValue('C'.$j, $row->fp_location);
			$objPHPExcel->setActiveSheetIndex(0)->setCellValue('D'.$j, $row->fp_ip_address);
			$objPHPExcel->setActiveSheetIndex(0)->setCellValue('E'.$j, $row->fp_create);
			$objPHPExcel->setActiveSheetIndex(0)->setCellValue('F'.$j, $row->fp_update);
		}
		$styleArray = array(
			'font' => array(
				'bold' => true,
			),
		);
		$styleArrayData = array(
			'borders' => array(
				'allborders' => array(
					'style' => PHPExcel_Style_Border::BORDER_THIN
				),
			),
		);
		$objPHPExcel->getActiveSheet()->getStyle('A1:F6')->applyFromArray($styleArray);
		$objPHPExcel->getActiveSheet()->getStyle('A1:F'.$j)->applyFromArray($styleArrayData);
		// Rename worksheet
		$objPHPExcel->getActiveSheet()->setTitle('Default');

		// Set active sheet index to the first sheet, so Excel opens this as the first sheet
		$objPHPExcel->setActiveSheetIndex(0);
		 		 
		// Redirect output to a client�s web browser (Excel5)
		header('Content-Type: application/vnd.ms-excel');
		header('Content-Disposition: attachment;filename="Template Fingerprint '.date('Y-m-d H:i:s').'.xls"');
		header('Cache-Control: max-age=0');
		// If you're serving to IE 9, then the following may be needed
		header('Cache-Control: max-age=1');
		 
		// If you're serving to IE over SSL, then the following may be needed
		header ('Expires: Mon, 26 Jul 1997 05:00:00 GMT'); // Date in the past
		header ('Last-Modified: '.gmdate('D, d M Y H:i:s').' GMT'); // always modified
		header ('Cache-Control: cache, must-revalidate'); // HTTP/1.1
		header ('Pragma: public'); // HTTP/1.0
		 
		$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
		$objWriter->save('php://output');
	    Yii::app()->end();
	}
	

	public function loadModel($id)
	{
		$model=Fingerprint::model()->findByPk($id);
		if($model===null)
		throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}

	/**
	* Performs the AJAX validation.
	* @param CModel the model to be validated
	*/
	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='fingerprint-form')
		{
		echo CActiveForm::validate($model);
		Yii::app()->end();
		}
	}
}
