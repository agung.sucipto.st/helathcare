<?php
/**
 * The following variables are available in this template:
 * - $this: the BootCrudCode object
 */
?>
<?php
$co=strlen($this->modelClass)-1;
$contrl=strtolower(substr($this->modelClass,0,1)).substr($this->modelClass,1,$co);
echo "<?php\n";
echo "\$this->breadcrumbs=array(
	'Kelola ".substr($this->pluralize($this->class2name($this->modelClass)),0,-1)."'=>array('".$contrl."/index'),
	'Perbarui ".substr($this->pluralize($this->class2name($this->modelClass)),0,-1)."',
);

\$this->title=array(
	'title'=>'Perbarui ".substr($this->pluralize($this->class2name($this->modelClass)),0,-1)."',
	'deskripsi'=>'Untuk Memperbarui ".substr($this->class2name($this->modelClass),0,-1)."'
);";
?>
?>

<div class="panel panel-default">
	<div class="panel-heading">
		<h3 class="panel-title pull-left">Form Perbarui</h3>
		<a href="<?php echo'<?php echo Yii::app()->createUrl(Yii::app()->controller->id);?>';?>" class="btn btn-primary btn-xs pull-right">
			<i class="fa fa-arrow-circle-left"></i>
			<span>Kembali</span>
		</a>
		<div style="clear:both"></div>
	</div>
	<div class="panel-body">		
		<?php echo "<?php echo \$this->renderPartial('_form', array('model'=>\$model)); ?>"; ?>
	</div>
</div>