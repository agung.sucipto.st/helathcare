<?php
/**
 * The following variables are available in this template:
 * - $this: the BootCrudCode object
 */
?>
<?php
$co=strlen($this->modelClass)-1;
$contrl=strtolower(substr($this->modelClass,0,1)).substr($this->modelClass,1,$co);
echo "<?php\n";
echo "\$this->breadcrumbs=array(
	'Kelola ".substr($this->pluralize($this->class2name($this->modelClass)),0,-1)."'=>array('".$contrl."/index'),
	'Detail ".substr($this->pluralize($this->class2name($this->modelClass)),0,-1)."',
);

\$this->title=array(
	'title'=>'Detail ".substr($this->pluralize($this->class2name($this->modelClass)),0,-1)."',
	'deskripsi'=>'Untuk Melihat Detail ".substr($this->pluralize($this->class2name($this->modelClass)),0,-1)."'
);\n";
?>
?>

<div class="panel panel-default">
	<div class="panel-heading">
		<h3 class="panel-title pull-left">Detail</h3>
		<a href="<?php echo'<?php echo Yii::app()->createUrl(Yii::app()->controller->id);?>';?>" class="btn btn-primary btn-xs pull-right" style="display:<?php echo'<?php echo $visibleEdit;?>';?>">
			<i class="fa fa-arrow-circle-left"></i>
			<span>Kembali</span>
		</a>
		<a href="<?php echo'<?php echo Yii::app()->createUrl(Yii::app()->controller->id.\'/create\');?>';?>" class="btn btn-primary btn-xs pull-right" style="display:<?php echo'<?php echo ((Lib::accessBy(\''.$this->modelClass.'\',\'create\')==true)?"block":"");?>';?>">
			<i class="fa fa-copy"></i>
			<span>Tambah Data</span>
		</a>
		<a href="<?php echo'<?php echo Yii::app()->createUrl(Yii::app()->controller->id.\'/update/id/\'.$_GET[\'id\']);?>';?>" class="btn btn-primary btn-xs pull-right" style="display:<?php echo'<?php echo ((Lib::accessBy(\''.$this->modelClass.'\',\'update\')==true)?"block":"");?>';?>">
			<i class="fa fa-edit"></i>
			<span>Edit Data</span>
		</a>
		<div style="clear:both"></div>
	</div>
	<div class="panel-body">		
		<div id="example-2_wrapper" class="dataTables_wrapper form-inline dt-bootstrap no-footer">
			<?php echo "<?php"; ?> $this->widget('booster.widgets.TbDetailView',array(
			'data'=>$model,
			'attributes'=>array(
			<?php
			foreach ($this->tableSchema->columns as $column) {
echo "\t\t\t\t'" . $column->name . "',\n";
			}
			?>
			),
			)); ?>
		</div>
	</div>
</div>
