<?php

/**
 * This is the model class for table "access_menu_arrange".
 *
 * The followings are the available columns in table 'access_menu_arrange':
 * @property integer $menu_arrange_id
 * @property string $menu_arrange_name
 * @property string $menu_icon
 * @property integer $access_id
 * @property integer $menu_arrange_parent
 * @property integer $menu_arrange_order
 * @property integer $menu_group_id
 *
 * The followings are the available model relations:
 * @property Access $access
 * @property AccessMenuGroup $menuGroup
 */
class AccessMenuArrange extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'access_menu_arrange';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('menu_arrange_parent, menu_arrange_order, menu_group_id', 'required'),
			array('access_id, menu_arrange_parent, menu_arrange_order, menu_group_id', 'numerical', 'integerOnly'=>true),
			array('menu_arrange_name, menu_icon', 'length', 'max'=>50),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('menu_arrange_id, menu_arrange_name, menu_icon, access_id, menu_arrange_parent, menu_arrange_order, menu_group_id', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'access' => array(self::BELONGS_TO, 'Access', 'access_id'),
			'menuGroup' => array(self::BELONGS_TO, 'AccessMenuGroup', 'menu_group_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'menu_arrange_id' => 'Menu Arrange',
			'menu_arrange_name' => 'Menu Arrange Name',
			'menu_icon' => 'Menu Icon',
			'access_id' => 'Access',
			'menu_arrange_parent' => 'Menu Arrange Parent',
			'menu_arrange_order' => 'Menu Arrange Order',
			'menu_group_id' => 'Menu Group',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('menu_arrange_id',$this->menu_arrange_id);
		$criteria->compare('menu_arrange_name',$this->menu_arrange_name,true);
		$criteria->compare('menu_icon',$this->menu_icon,true);
		$criteria->compare('access_id',$this->access_id);
		$criteria->compare('menu_arrange_parent',$this->menu_arrange_parent);
		$criteria->compare('menu_arrange_order',$this->menu_arrange_order);
		$criteria->compare('menu_group_id',$this->menu_group_id);

		return new CActiveDataProvider($this, array(
			'pagination'=>array(
				'pageSize'=> Yii::app()->user->getState('pageSize',Yii::app()->params['defaultPageSize']),
			),
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return AccessMenuArrange the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
