<?php

/**
 * This is the model class for table "resep_pasien_list".
 *
 * The followings are the available columns in table 'resep_pasien_list':
 * @property string $id_resep_pasien_list
 * @property string $id_resep_pasien
 * @property string $id_item
 * @property string $jumlah
 * @property string $jumlah_hari
 * @property string $signa
 * @property string $signa_tambahan
 * @property string $status_jual
 * @property string $alasan
 * @property string $user_approve
 * @property string $time_approve
 *
 * The followings are the available model relations:
 * @property ItemPasien[] $itemPasiens
 * @property Item $idItem
 * @property ResepPasien $idResepPasien
 * @property User $userApprove
 */
class ResepPasienList extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'resep_pasien_list';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('id_resep_pasien, id_item, jumlah', 'required'),
			array('id_resep_pasien, id_item, jumlah_hari, user_approve', 'length', 'max'=>20),
			array('jumlah', 'length', 'max'=>30),
			array('status_jual', 'length', 'max'=>1),
			array('signa, signa_tambahan, alasan, time_approve', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id_resep_pasien_list, id_resep_pasien, id_item, jumlah, jumlah_hari, signa, signa_tambahan, status_jual, alasan, user_approve, time_approve', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'itemPasiens' => array(self::HAS_MANY, 'ItemPasien', 'id_resep_pasien_list'),
			'idItem' => array(self::BELONGS_TO, 'Item', 'id_item'),
			'idResepPasien' => array(self::BELONGS_TO, 'ResepPasien', 'id_resep_pasien'),
			'userApprove' => array(self::BELONGS_TO, 'User', 'user_approve'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id_resep_pasien_list' => 'Id Resep Pasien List',
			'id_resep_pasien' => 'Id Resep Pasien',
			'id_item' => 'Id Item',
			'jumlah' => 'Jumlah',
			'jumlah_hari' => 'Jumlah Hari',
			'signa' => 'Signa',
			'signa_tambahan' => 'Signa Tambahan',
			'status_jual' => 'Status Jual',
			'alasan' => 'Alasan',
			'user_approve' => 'User Approve',
			'time_approve' => 'Time Approve',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id_resep_pasien_list',$this->id_resep_pasien_list,true);
		$criteria->compare('id_resep_pasien',$this->id_resep_pasien,true);
		$criteria->compare('id_item',$this->id_item,true);
		$criteria->compare('jumlah',$this->jumlah,true);
		$criteria->compare('jumlah_hari',$this->jumlah_hari,true);
		$criteria->compare('signa',$this->signa,true);
		$criteria->compare('signa_tambahan',$this->signa_tambahan,true);
		$criteria->compare('status_jual',$this->status_jual,true);
		$criteria->compare('alasan',$this->alasan,true);
		$criteria->compare('user_approve',$this->user_approve,true);
		$criteria->compare('time_approve',$this->time_approve,true);

		return new CActiveDataProvider($this, array(
			'pagination'=>array(
				'pageSize'=> Yii::app()->user->getState('pageSize',Yii::app()->params['defaultPageSize']),
			),
			'criteria'=>$criteria,
		));
	}
	
	public function getAllowView()
	{
		$akses = UserAccess::model()->find(array("with"=>array("access"),'condition'=>"id_user='".Yii::app()->user->id."' and access.access_controller='ResepPasienList' and access.access_action='view'"));
		if(!empty($akses)) return true; else return false;
	}
	
	public function getAllowUpdate()
	{
		$akses = UserAccess::model()->find(array("with"=>array("access"),'condition'=>"id_user='".Yii::app()->user->id."' and access.access_controller='ResepPasienList' and access.access_action='update'"));
		if(!empty($akses)) return true; else return false;
	}
	
	public function getAllowDelete($id)
	{
		$akses = UserAccess::model()->find(array("with"=>array("access"),'condition'=>"id_user='".Yii::app()->user->id."' and access.access_controller='ResepPasienList' and access.access_action='delete'"));
		$rel=$this->getEmptyRelation($id);
		if(!empty($akses) AND $rel==true){
			return true;
		}else{
			return false;
		}
	}
	
	
	public function getEmptyRelation($id)
	{
		$count=0;
								if($count>0){
			return false;
		}else{
			return true;
		}
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return ResepPasienList the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
