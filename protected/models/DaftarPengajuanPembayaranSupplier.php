<?php

/**
 * This is the model class for table "daftar_pengajuan_pembayaran_supplier".
 *
 * The followings are the available columns in table 'daftar_pengajuan_pembayaran_supplier':
 * @property string $id_daftar_pengajuan
 * @property string $id_pengajuan
 * @property string $jenis_rincian
 * @property integer $id_coa
 * @property integer $gl_id_unit
 * @property string $id_daftar_item_transaksi
 * @property string $jenis_pph
 * @property string $nilai_pph
 * @property integer $harga_item
 * @property string $deskripsi
 *
 * The followings are the available model relations:
 * @property PengajuanPembayaranSupplier $idPengajuan
 * @property DaftarItemTransaksi $idDaftarItemTransaksi
 * @property GlCoa $idCoa
 * @property GlUnit $glIdUnit
 */
class DaftarPengajuanPembayaranSupplier extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'daftar_pengajuan_pembayaran_supplier';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('id_pengajuan, jenis_rincian, harga_item', 'required'),
			array('id_coa, gl_id_unit, harga_item', 'numerical', 'integerOnly'=>true),
			array('id_pengajuan, id_daftar_item_transaksi', 'length', 'max'=>20),
			array('jenis_rincian', 'length', 'max'=>19),
			array('jenis_pph', 'length', 'max'=>13),
			array('nilai_pph', 'length', 'max'=>11),
			array('deskripsi', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id_daftar_pengajuan, id_pengajuan, jenis_rincian, id_coa, gl_id_unit, id_daftar_item_transaksi, jenis_pph, nilai_pph, harga_item, deskripsi', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'idPengajuan' => array(self::BELONGS_TO, 'PengajuanPembayaranSupplier', 'id_pengajuan'),
			'idDaftarItemTransaksi' => array(self::BELONGS_TO, 'DaftarItemTransaksi', 'id_daftar_item_transaksi'),
			'idCoa' => array(self::BELONGS_TO, 'GlCoa', 'id_coa'),
			'glIdUnit' => array(self::BELONGS_TO, 'GlUnit', 'gl_id_unit'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id_daftar_pengajuan' => 'Id Daftar Pengajuan',
			'id_pengajuan' => 'Id Pengajuan',
			'jenis_rincian' => 'Jenis Rincian',
			'id_coa' => 'Id Coa',
			'gl_id_unit' => 'Gl Id Unit',
			'id_daftar_item_transaksi' => 'Id Daftar Item Transaksi',
			'jenis_pph' => 'Jenis Pph',
			'nilai_pph' => 'Nilai Pph',
			'harga_item' => 'Harga Item',
			'deskripsi' => 'Deskripsi',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id_daftar_pengajuan',$this->id_daftar_pengajuan,true);
		$criteria->compare('id_pengajuan',$this->id_pengajuan,true);
		$criteria->compare('jenis_rincian',$this->jenis_rincian,true);
		$criteria->compare('id_coa',$this->id_coa);
		$criteria->compare('gl_id_unit',$this->gl_id_unit);
		$criteria->compare('id_daftar_item_transaksi',$this->id_daftar_item_transaksi,true);
		$criteria->compare('jenis_pph',$this->jenis_pph,true);
		$criteria->compare('nilai_pph',$this->nilai_pph,true);
		$criteria->compare('harga_item',$this->harga_item);
		$criteria->compare('deskripsi',$this->deskripsi,true);

		return new CActiveDataProvider($this, array(
			'pagination'=>array(
				'pageSize'=> Yii::app()->user->getState('pageSize',Yii::app()->params['defaultPageSize']),
			),
			'criteria'=>$criteria,
		));
	}
	
	public function getAllowView()
	{
		$akses = UserAccess::model()->find(array("with"=>array("access"),'condition'=>"id_user='".Yii::app()->user->id."' and access.access_controller='DaftarPengajuanPembayaranSupplier' and access.access_action='view'"));
		if(!empty($akses)) return true; else return false;
	}
	
	public function getAllowUpdate()
	{
		$akses = UserAccess::model()->find(array("with"=>array("access"),'condition'=>"id_user='".Yii::app()->user->id."' and access.access_controller='DaftarPengajuanPembayaranSupplier' and access.access_action='update'"));
		if(!empty($akses)) return true; else return false;
	}
	
	public function getAllowDelete($id)
	{
		$akses = UserAccess::model()->find(array("with"=>array("access"),'condition'=>"id_user='".Yii::app()->user->id."' and access.access_controller='DaftarPengajuanPembayaranSupplier' and access.access_action='delete'"));
		$rel=$this->getEmptyRelation($id);
		if(!empty($akses) AND $rel==true){
			return true;
		}else{
			return false;
		}
	}
	
	
	public function getEmptyRelation($id)
	{
		$count=0;
										if($count>0){
			return false;
		}else{
			return true;
		}
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return DaftarPengajuanPembayaranSupplier the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
