<?php

/**
 * This is the model class for table "lembar_konsultasi".
 *
 * The followings are the available columns in table 'lembar_konsultasi':
 * @property string $id_lembar_konsultasi
 * @property string $id_registrasi
 * @property string $nama_dokter
 * @property string $poli
 * @property string $tgl_tanya
 * @property string $pertanyaan
 * @property string $keperluan
 * @property string $to_dokter
 * @property string $spesialis
 * @property string $tgl_jawab
 * @property string $jawaban
 * @property string $user_create
 * @property string $user_update
 * @property string $tgl_create
 * @property string $tgl_update
 */
class LembarKonsultasi extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'lembar_konsultasi';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('id_lembar_konsultasi', 'required'),
			array('id_lembar_konsultasi, id_registrasi, user_create, user_update', 'length', 'max'=>20),
			array('nama_dokter, poli, to_dokter, spesialis', 'length', 'max'=>35),
			array('keperluan', 'length', 'max'=>13),
			array('tgl_tanya, pertanyaan, tgl_jawab, jawaban, tgl_create, tgl_update', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id_lembar_konsultasi, id_registrasi, nama_dokter, poli, tgl_tanya, pertanyaan, keperluan, to_dokter, spesialis, tgl_jawab, jawaban, user_create, user_update, tgl_create, tgl_update', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id_lembar_konsultasi' => 'Id Lembar Konsultasi',
			'id_registrasi' => 'Id Registrasi',
			'nama_dokter' => 'Nama Dokter',
			'poli' => 'Poli',
			'tgl_tanya' => 'Tgl Tanya',
			'pertanyaan' => 'Pertanyaan',
			'keperluan' => 'Keperluan',
			'to_dokter' => 'To Dokter',
			'spesialis' => 'Spesialis',
			'tgl_jawab' => 'Tgl Jawab',
			'jawaban' => 'Jawaban',
			'user_create' => 'User Create',
			'user_update' => 'User Update',
			'tgl_create' => 'Tgl Create',
			'tgl_update' => 'Tgl Update',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id_lembar_konsultasi',$this->id_lembar_konsultasi,true);
		$criteria->compare('id_registrasi',$this->id_registrasi,true);
		$criteria->compare('nama_dokter',$this->nama_dokter,true);
		$criteria->compare('poli',$this->poli,true);
		$criteria->compare('tgl_tanya',$this->tgl_tanya,true);
		$criteria->compare('pertanyaan',$this->pertanyaan,true);
		$criteria->compare('keperluan',$this->keperluan,true);
		$criteria->compare('to_dokter',$this->to_dokter,true);
		$criteria->compare('spesialis',$this->spesialis,true);
		$criteria->compare('tgl_jawab',$this->tgl_jawab,true);
		$criteria->compare('jawaban',$this->jawaban,true);
		$criteria->compare('user_create',$this->user_create,true);
		$criteria->compare('user_update',$this->user_update,true);
		$criteria->compare('tgl_create',$this->tgl_create,true);
		$criteria->compare('tgl_update',$this->tgl_update,true);

		return new CActiveDataProvider($this, array(
			'pagination'=>array(
				'pageSize'=> Yii::app()->user->getState('pageSize',Yii::app()->params['defaultPageSize']),
			),
			'criteria'=>$criteria,
		));
	}
	
	public function getAllowView()
	{
		$akses = UserAccess::model()->find(array("with"=>array("access"),'condition'=>"id_user='".Yii::app()->user->id."' and access.access_controller='LembarKonsultasi' and access.access_action='view'"));
		if(!empty($akses)) return true; else return false;
	}
	
	public function getAllowUpdate()
	{
		$akses = UserAccess::model()->find(array("with"=>array("access"),'condition'=>"id_user='".Yii::app()->user->id."' and access.access_controller='LembarKonsultasi' and access.access_action='update'"));
		if(!empty($akses)) return true; else return false;
	}
	
	public function getAllowDelete($id)
	{
		$akses = UserAccess::model()->find(array("with"=>array("access"),'condition'=>"id_user='".Yii::app()->user->id."' and access.access_controller='LembarKonsultasi' and access.access_action='delete'"));
		$rel=$this->getEmptyRelation($id);
		if(!empty($akses) AND $rel==true){
			return true;
		}else{
			return false;
		}
	}
	
	
	public function getEmptyRelation($id)
	{
		$count=0;
		if($count>0){
			return false;
		}else{
			return true;
		}
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return LembarKonsultasi the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
