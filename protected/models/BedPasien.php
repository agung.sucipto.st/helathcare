<?php

/**
 * This is the model class for table "bed_pasien".
 *
 * The followings are the available columns in table 'bed_pasien':
 * @property string $id_bed_pasien
 * @property string $id_registrasi
 * @property string $id_tarif_ruang_rawatan
 * @property string $id_bed
 * @property string $waktu_masuk
 * @property string $waktu_keluar
 * @property string $jasa_paramedis
 * @property string $biaya_makan
 * @property string $tarif_kamar
 * @property string $titipan
 * @property string $id_kelas_diminta
 * @property string $user_create
 * @property string $user_update
 * @property string $user_approve
 * @property string $time_create
 * @property string $time_update
 * @property string $time_approve
 *
 * The followings are the available model relations:
 * @property Kelas $idKelasDiminta
 * @property Bed $idBed
 * @property Registrasi $idRegistrasi
 * @property TarifRuangRawatan $idTarifRuangRawatan
 * @property User $userCreate
 * @property User $userUpdate
 * @property User $userApprove
 * @property DaftarTagihan[] $daftarTagihans
 */
class BedPasien extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'bed_pasien';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('id_registrasi, id_tarif_ruang_rawatan, id_bed, waktu_masuk, user_create, time_create', 'required'),
			array('id_registrasi, id_tarif_ruang_rawatan, id_bed, id_kelas_diminta, user_create, user_update, user_approve', 'length', 'max'=>20),
			array('jasa_paramedis, biaya_makan, tarif_kamar', 'length', 'max'=>30),
			array('titipan', 'length', 'max'=>1),
			array('waktu_keluar, time_update, time_approve', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id_bed_pasien, id_registrasi, id_tarif_ruang_rawatan, id_bed, waktu_masuk, waktu_keluar, jasa_paramedis, biaya_makan, tarif_kamar, titipan, id_kelas_diminta, user_create, user_update, user_approve, time_create, time_update, time_approve', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'idKelasDiminta' => array(self::BELONGS_TO, 'Kelas', 'id_kelas_diminta'),
			'idBed' => array(self::BELONGS_TO, 'Bed', 'id_bed'),
			'idRegistrasi' => array(self::BELONGS_TO, 'Registrasi', 'id_registrasi'),
			'idTarifRuangRawatan' => array(self::BELONGS_TO, 'TarifRuangRawatan', 'id_tarif_ruang_rawatan'),
			'userCreate' => array(self::BELONGS_TO, 'User', 'user_create'),
			'userUpdate' => array(self::BELONGS_TO, 'User', 'user_update'),
			'userApprove' => array(self::BELONGS_TO, 'User', 'user_approve'),
			'daftarTagihans' => array(self::HAS_MANY, 'DaftarTagihan', 'id_bed_pasien'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id_bed_pasien' => 'Id Bed Pasien',
			'id_registrasi' => 'Id Registrasi',
			'id_tarif_ruang_rawatan' => 'Id Tarif Ruang Rawatan',
			'id_bed' => 'Id Bed',
			'waktu_masuk' => 'Waktu Masuk',
			'waktu_keluar' => 'Waktu Keluar',
			'jasa_paramedis' => 'Jasa Paramedis',
			'biaya_makan' => 'Biaya Makan',
			'tarif_kamar' => 'Tarif Kamar',
			'titipan' => 'Titipan',
			'id_kelas_diminta' => 'Id Kelas Diminta',
			'user_create' => 'User Create',
			'user_update' => 'User Update',
			'user_approve' => 'User Approve',
			'time_create' => 'Time Create',
			'time_update' => 'Time Update',
			'time_approve' => 'Time Approve',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id_bed_pasien',$this->id_bed_pasien,true);
		$criteria->compare('id_registrasi',$this->id_registrasi,true);
		$criteria->compare('id_tarif_ruang_rawatan',$this->id_tarif_ruang_rawatan,true);
		$criteria->compare('id_bed',$this->id_bed,true);
		$criteria->compare('waktu_masuk',$this->waktu_masuk,true);
		$criteria->compare('waktu_keluar',$this->waktu_keluar,true);
		$criteria->compare('jasa_paramedis',$this->jasa_paramedis,true);
		$criteria->compare('biaya_makan',$this->biaya_makan,true);
		$criteria->compare('tarif_kamar',$this->tarif_kamar,true);
		$criteria->compare('titipan',$this->titipan,true);
		$criteria->compare('id_kelas_diminta',$this->id_kelas_diminta,true);
		$criteria->compare('user_create',$this->user_create,true);
		$criteria->compare('user_update',$this->user_update,true);
		$criteria->compare('user_approve',$this->user_approve,true);
		$criteria->compare('time_create',$this->time_create,true);
		$criteria->compare('time_update',$this->time_update,true);
		$criteria->compare('time_approve',$this->time_approve,true);

		return new CActiveDataProvider($this, array(
			'pagination'=>array(
				'pageSize'=> Yii::app()->user->getState('pageSize',Yii::app()->params['defaultPageSize']),
			),
			'criteria'=>$criteria,
		));
	}
	
	public function getAllowView()
	{
		$akses = UserAccess::model()->find(array("with"=>array("access"),'condition'=>"id_user='".Yii::app()->user->id."' and access.access_controller='BedPasien' and access.access_action='view'"));
		if(!empty($akses)) return true; else return false;
	}
	
	public function getAllowUpdate()
	{
		$akses = UserAccess::model()->find(array("with"=>array("access"),'condition'=>"id_user='".Yii::app()->user->id."' and access.access_controller='BedPasien' and access.access_action='update'"));
		if(!empty($akses)) return true; else return false;
	}
	
	public function getAllowDelete($id)
	{
		$akses = UserAccess::model()->find(array("with"=>array("access"),'condition'=>"id_user='".Yii::app()->user->id."' and access.access_controller='BedPasien' and access.access_action='delete'"));
		$rel=$this->getEmptyRelation($id);
		if(!empty($akses) AND $rel==true){
			return true;
		}else{
			return false;
		}
	}
	
	
	public function getEmptyRelation($id)
	{
		$count=0;
																$count+=DaftarTagihan::model()->count(array('condition'=>"id_bed_pasien='$id'"));
		if($count>0){
			return false;
		}else{
			return true;
		}
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return BedPasien the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
