<?php

/**
 * This is the model class for table "queue_area".
 *
 * The followings are the available columns in table 'queue_area':
 * @property string $queue_area_id
 * @property string $queue_area_name
 * @property string $queue_area_status
 * @property string $queue_area_sound
 *
 * The followings are the available model relations:
 * @property QueueCall[] $queueCalls
 * @property QueueCounter[] $queueCounters
 * @property QueueGenerator[] $queueGenerators
 * @property QueueList[] $queueLists
 */
class QueueArea extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'queue_area';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('queue_area_name, queue_area_status', 'length', 'max'=>45),
			array('queue_area_sound', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('queue_area_id, queue_area_name, queue_area_status, queue_area_sound', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'queueCalls' => array(self::HAS_MANY, 'QueueCall', 'queue_area_id'),
			'queueCounters' => array(self::HAS_MANY, 'QueueCounter', 'queue_area_id'),
			'queueGenerators' => array(self::HAS_MANY, 'QueueGenerator', 'queue_area_id'),
			'queueLists' => array(self::HAS_MANY, 'QueueList', 'queue_area_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'queue_area_id' => 'Queue Area',
			'queue_area_name' => 'Queue Area Name',
			'queue_area_status' => 'Queue Area Status',
			'queue_area_sound' => 'Queue Area Sound',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('queue_area_id',$this->queue_area_id,true);
		$criteria->compare('queue_area_name',$this->queue_area_name,true);
		$criteria->compare('queue_area_status',$this->queue_area_status,true);
		$criteria->compare('queue_area_sound',$this->queue_area_sound,true);

		return new CActiveDataProvider($this, array(
			'pagination'=>array(
				'pageSize'=> Yii::app()->user->getState('pageSize',Yii::app()->params['defaultPageSize']),
			),
			'criteria'=>$criteria,
		));
	}
	
	public function getAllowView()
	{
		$akses = UserAccess::model()->find(array("with"=>array("access"),'condition'=>"id_user='".Yii::app()->user->id."' and access.access_controller='QueueArea' and access.access_action='view'"));
		if(!empty($akses)) return true; else return false;
	}
	
	public function getAllowUpdate()
	{
		$akses = UserAccess::model()->find(array("with"=>array("access"),'condition'=>"id_user='".Yii::app()->user->id."' and access.access_controller='QueueArea' and access.access_action='update'"));
		if(!empty($akses)) return true; else return false;
	}
	
	public function getAllowDelete($id)
	{
		$akses = UserAccess::model()->find(array("with"=>array("access"),'condition'=>"id_user='".Yii::app()->user->id."' and access.access_controller='QueueArea' and access.access_action='delete'"));
		$rel=$this->getEmptyRelation($id);
		if(!empty($akses) AND $rel==true){
			return true;
		}else{
			return false;
		}
	}
	
	
	public function getEmptyRelation($id)
	{
		$count=0;
		$count+=QueueCall::model()->count(array('condition'=>"queue_area_id='$id'"));
		$count+=QueueCounter::model()->count(array('condition'=>"queue_area_id='$id'"));
		$count+=QueueGenerator::model()->count(array('condition'=>"queue_area_id='$id'"));
		$count+=QueueList::model()->count(array('condition'=>"queue_area_id='$id'"));
		if($count>0){
			return false;
		}else{
			return true;
		}
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return QueueArea the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
