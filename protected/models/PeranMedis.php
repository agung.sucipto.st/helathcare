<?php

/**
 * This is the model class for table "peran_medis".
 *
 * The followings are the available columns in table 'peran_medis':
 * @property string $id_peran_medis
 * @property string $nama_peran_medis
 * @property string $jenis_peran_medis
 *
 * The followings are the available model relations:
 * @property LaboratoriumMedis[] $laboratoriumMedises
 * @property PeralatanMedis[] $peralatanMedises
 * @property RadiologyMedis[] $radiologyMedises
 * @property TindakanMedis[] $tindakanMedises
 */
class PeranMedis extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'peran_medis';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('nama_peran_medis, jenis_peran_medis', 'required'),
			array('nama_peran_medis', 'length', 'max'=>60),
			array('jenis_peran_medis', 'length', 'max'=>45),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id_peran_medis, nama_peran_medis, jenis_peran_medis', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'laboratoriumMedises' => array(self::HAS_MANY, 'LaboratoriumMedis', 'id_peran_medis'),
			'peralatanMedises' => array(self::HAS_MANY, 'PeralatanMedis', 'id_peran_medis'),
			'radiologyMedises' => array(self::HAS_MANY, 'RadiologyMedis', 'id_peran_medis'),
			'tindakanMedises' => array(self::HAS_MANY, 'TindakanMedis', 'id_peran_medis'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id_peran_medis' => 'Id Peran Medis',
			'nama_peran_medis' => 'Nama Peran Medis',
			'jenis_peran_medis' => 'Jenis Peran Medis',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id_peran_medis',$this->id_peran_medis,true);
		$criteria->compare('nama_peran_medis',$this->nama_peran_medis,true);
		$criteria->compare('jenis_peran_medis',$this->jenis_peran_medis,true);

		return new CActiveDataProvider($this, array(
			'pagination'=>array(
				'pageSize'=> Yii::app()->user->getState('pageSize',Yii::app()->params['defaultPageSize']),
			),
			'criteria'=>$criteria,
		));
	}
	
	public function getAllowView()
	{
		$akses = UserAccess::model()->find(array("with"=>array("access"),'condition'=>"id_user='".Yii::app()->user->id."' and access.access_controller='PeranMedis' and access.access_action='view'"));
		if(!empty($akses)) return true; else return false;
	}
	
	public function getAllowUpdate()
	{
		$akses = UserAccess::model()->find(array("with"=>array("access"),'condition'=>"id_user='".Yii::app()->user->id."' and access.access_controller='PeranMedis' and access.access_action='update'"));
		if(!empty($akses)) return true; else return false;
	}
	
	public function getAllowDelete($id)
	{
		$akses = UserAccess::model()->find(array("with"=>array("access"),'condition'=>"id_user='".Yii::app()->user->id."' and access.access_controller='PeranMedis' and access.access_action='delete'"));
		$rel=$this->getEmptyRelation($id);
		if(!empty($akses) AND $rel==true){
			return true;
		}else{
			return false;
		}
	}
	
	
	public function getEmptyRelation($id)
	{
		$count=0;
		$count+=LaboratoriumMedis::model()->count(array('condition'=>"id_peran_medis='$id'"));
		$count+=PeralatanMedis::model()->count(array('condition'=>"id_peran_medis='$id'"));
		$count+=RadiologyMedis::model()->count(array('condition'=>"id_peran_medis='$id'"));
		$count+=TindakanMedis::model()->count(array('condition'=>"id_peran_medis='$id'"));
		if($count>0){
			return false;
		}else{
			return true;
		}
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return PeranMedis the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
