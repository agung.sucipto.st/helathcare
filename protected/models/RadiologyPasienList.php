<?php

/**
 * This is the model class for table "radiology_pasien_list".
 *
 * The followings are the available columns in table 'radiology_pasien_list':
 * @property string $id_radiology_pasien_list
 * @property string $id_radiology_pasien
 * @property string $id_radiology
 * @property string $id_tarif_radiology
 * @property string $id_petugas_radiology
 * @property string $id_dokter_pemeriksa
 * @property string $jumlah
 * @property string $hasil
 * @property string $waktu_penyelesaian
 * @property string $file
 * @property string $status
 * @property string $id_daftar_tagihan
 *
 * The followings are the available model relations:
 * @property DaftarJasaMedis[] $daftarJasaMedises
 * @property DaftarTagihan[] $daftarTagihans
 * @property DaftarTagihan $idDaftarTagihan
 * @property Pegawai $idDokterPemeriksa
 * @property Pegawai $idPetugasRadiology
 * @property Radiology $idRadiology
 * @property RadiologyPasien $idRadiologyPasien
 * @property TarifRadiology $idTarifRadiology
 * @property RadiologyPasienMedis[] $radiologyPasienMedises
 */
class RadiologyPasienList extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'radiology_pasien_list';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('id_radiology_pasien_list, id_radiology_pasien, id_radiology, id_tarif_radiology, id_petugas_radiology, id_dokter_pemeriksa, id_daftar_tagihan', 'required'),
			array('id_radiology_pasien_list, id_radiology_pasien, id_radiology, id_tarif_radiology, id_petugas_radiology, id_dokter_pemeriksa, jumlah, id_daftar_tagihan', 'length', 'max'=>20),
			array('status', 'length', 'max'=>45),
			array('hasil, waktu_penyelesaian, file', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id_radiology_pasien_list, id_radiology_pasien, id_radiology, id_tarif_radiology, id_petugas_radiology, id_dokter_pemeriksa, jumlah, hasil, waktu_penyelesaian, file, status, id_daftar_tagihan', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'daftarJasaMedises' => array(self::HAS_MANY, 'DaftarJasaMedis', 'id_radiology_pasien_list'),
			'daftarTagihans' => array(self::HAS_MANY, 'DaftarTagihan', 'id_radiology_pasien_list'),
			'idDaftarTagihan' => array(self::BELONGS_TO, 'DaftarTagihan', 'id_daftar_tagihan'),
			'idDokterPemeriksa' => array(self::BELONGS_TO, 'Pegawai', 'id_dokter_pemeriksa'),
			'idPetugasRadiology' => array(self::BELONGS_TO, 'Pegawai', 'id_petugas_radiology'),
			'idRadiology' => array(self::BELONGS_TO, 'Radiology', 'id_radiology'),
			'idRadiologyPasien' => array(self::BELONGS_TO, 'RadiologyPasien', 'id_radiology_pasien'),
			'idTarifRadiology' => array(self::BELONGS_TO, 'TarifRadiology', 'id_tarif_radiology'),
			'radiologyPasienMedises' => array(self::HAS_MANY, 'RadiologyPasienMedis', 'id_radiology_pasien_list'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id_radiology_pasien_list' => 'Id Radiology Pasien List',
			'id_radiology_pasien' => 'Id Radiology Pasien',
			'id_radiology' => 'Id Radiology',
			'id_tarif_radiology' => 'Id Tarif Radiology',
			'id_petugas_radiology' => 'Id Petugas Radiology',
			'id_dokter_pemeriksa' => 'Id Dokter Pemeriksa',
			'jumlah' => 'Jumlah',
			'hasil' => 'Hasil',
			'waktu_penyelesaian' => 'Waktu Penyelesaian',
			'file' => 'File',
			'status' => 'Status',
			'id_daftar_tagihan' => 'Id Daftar Tagihan',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id_radiology_pasien_list',$this->id_radiology_pasien_list,true);
		$criteria->compare('id_radiology_pasien',$this->id_radiology_pasien,true);
		$criteria->compare('id_radiology',$this->id_radiology,true);
		$criteria->compare('id_tarif_radiology',$this->id_tarif_radiology,true);
		$criteria->compare('id_petugas_radiology',$this->id_petugas_radiology,true);
		$criteria->compare('id_dokter_pemeriksa',$this->id_dokter_pemeriksa,true);
		$criteria->compare('jumlah',$this->jumlah,true);
		$criteria->compare('hasil',$this->hasil,true);
		$criteria->compare('waktu_penyelesaian',$this->waktu_penyelesaian,true);
		$criteria->compare('file',$this->file,true);
		$criteria->compare('status',$this->status,true);
		$criteria->compare('id_daftar_tagihan',$this->id_daftar_tagihan,true);

		return new CActiveDataProvider($this, array(
			'pagination'=>array(
				'pageSize'=> Yii::app()->user->getState('pageSize',Yii::app()->params['defaultPageSize']),
			),
			'criteria'=>$criteria,
		));
	}
	
	public function getAllowView()
	{
		$akses = UserAccess::model()->find(array("with"=>array("access"),'condition'=>"id_user='".Yii::app()->user->id."' and access.access_controller='RadiologyPasienList' and access.access_action='view'"));
		if(!empty($akses)) return true; else return false;
	}
	
	public function getAllowUpdate()
	{
		$akses = UserAccess::model()->find(array("with"=>array("access"),'condition'=>"id_user='".Yii::app()->user->id."' and access.access_controller='RadiologyPasienList' and access.access_action='update'"));
		if(!empty($akses)) return true; else return false;
	}
	
	public function getAllowDelete($id)
	{
		$akses = UserAccess::model()->find(array("with"=>array("access"),'condition'=>"id_user='".Yii::app()->user->id."' and access.access_controller='RadiologyPasienList' and access.access_action='delete'"));
		$rel=$this->getEmptyRelation($id);
		if(!empty($akses) AND $rel==true){
			return true;
		}else{
			return false;
		}
	}
	
	
	public function getEmptyRelation($id)
	{
		$count=0;
		$count+=DaftarJasaMedis::model()->count(array('condition'=>"id_radiology_pasien_list='$id'"));
		$count+=DaftarTagihan::model()->count(array('condition'=>"id_radiology_pasien_list='$id'"));
														$count+=RadiologyPasienMedis::model()->count(array('condition'=>"id_radiology_pasien_list='$id'"));
		if($count>0){
			return false;
		}else{
			return true;
		}
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return RadiologyPasienList the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
