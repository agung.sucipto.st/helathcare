<?php

/**
 * This is the model class for table "jaminan_pasien".
 *
 * The followings are the available columns in table 'jaminan_pasien':
 * @property string $id_jaminan_pasien
 * @property string $id_registrasi
 * @property string $id_penjamin
 * @property string $no_kartu
 * @property string $nama_pemegang_kartu
 * @property string $kelas_jaminan
 * @property string $hubungan_pemilik_jaminan
 * @property string $no_eligibilitas
 * @property string $tgl_eligibilitas
 * @property string $is_utama
 * @property string $model_jaminan
 * @property string $user_create
 * @property string $user_update
 * @property string $user_final
 * @property string $time_create
 * @property string $time_update
 * @property string $time_final
 *
 * The followings are the available model relations:
 * @property Penjamin $idPenjamin
 * @property Registrasi $idRegistrasi
 * @property User $userCreate
 * @property User $userUpdate
 * @property User $userFinal
 * @property JenisHubungan $hubunganPemilikJaminan
 */
class JaminanPasien extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'jaminan_pasien';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('id_registrasi, id_penjamin, no_kartu, is_utama, model_jaminan, user_create, time_create', 'required'),
			array('id_registrasi, id_penjamin, hubungan_pemilik_jaminan, user_create, user_update, user_final', 'length', 'max'=>20),
			array('no_kartu', 'length', 'max'=>30),
			array('nama_pemegang_kartu, kelas_jaminan, no_eligibilitas', 'length', 'max'=>45),
			array('is_utama', 'length', 'max'=>1),
			array('model_jaminan', 'length', 'max'=>29),
			array('tgl_eligibilitas, time_update, time_final', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id_jaminan_pasien, id_registrasi, id_penjamin, no_kartu, nama_pemegang_kartu, kelas_jaminan, hubungan_pemilik_jaminan, no_eligibilitas, tgl_eligibilitas, is_utama, model_jaminan, user_create, user_update, user_final, time_create, time_update, time_final', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'idPenjamin' => array(self::BELONGS_TO, 'Penjamin', 'id_penjamin'),
			'idRegistrasi' => array(self::BELONGS_TO, 'Registrasi', 'id_registrasi'),
			'userCreate' => array(self::BELONGS_TO, 'User', 'user_create'),
			'userUpdate' => array(self::BELONGS_TO, 'User', 'user_update'),
			'userFinal' => array(self::BELONGS_TO, 'User', 'user_final'),
			'hubunganPemilikJaminan' => array(self::BELONGS_TO, 'JenisHubungan', 'hubungan_pemilik_jaminan'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id_jaminan_pasien' => 'Id Jaminan Pasien',
			'id_registrasi' => 'Id Registrasi',
			'id_penjamin' => 'Id Penjamin',
			'no_kartu' => 'No Kartu',
			'nama_pemegang_kartu' => 'Nama Pemegang Kartu',
			'kelas_jaminan' => 'Kelas Jaminan',
			'hubungan_pemilik_jaminan' => 'Hubungan Pemilik Jaminan',
			'no_eligibilitas' => 'No Eligibilitas',
			'tgl_eligibilitas' => 'Tgl Eligibilitas',
			'is_utama' => 'Is Utama',
			'model_jaminan' => 'Model Jaminan',
			'user_create' => 'User Create',
			'user_update' => 'User Update',
			'user_final' => 'User Final',
			'time_create' => 'Time Create',
			'time_update' => 'Time Update',
			'time_final' => 'Time Final',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id_jaminan_pasien',$this->id_jaminan_pasien,true);
		$criteria->compare('id_registrasi',$this->id_registrasi,true);
		$criteria->compare('id_penjamin',$this->id_penjamin,true);
		$criteria->compare('no_kartu',$this->no_kartu,true);
		$criteria->compare('nama_pemegang_kartu',$this->nama_pemegang_kartu,true);
		$criteria->compare('kelas_jaminan',$this->kelas_jaminan,true);
		$criteria->compare('hubungan_pemilik_jaminan',$this->hubungan_pemilik_jaminan,true);
		$criteria->compare('no_eligibilitas',$this->no_eligibilitas,true);
		$criteria->compare('tgl_eligibilitas',$this->tgl_eligibilitas,true);
		$criteria->compare('is_utama',$this->is_utama,true);
		$criteria->compare('model_jaminan',$this->model_jaminan,true);
		$criteria->compare('user_create',$this->user_create,true);
		$criteria->compare('user_update',$this->user_update,true);
		$criteria->compare('user_final',$this->user_final,true);
		$criteria->compare('time_create',$this->time_create,true);
		$criteria->compare('time_update',$this->time_update,true);
		$criteria->compare('time_final',$this->time_final,true);

		return new CActiveDataProvider($this, array(
			'pagination'=>array(
				'pageSize'=> Yii::app()->user->getState('pageSize',Yii::app()->params['defaultPageSize']),
			),
			'criteria'=>$criteria,
		));
	}
	
	public function getAllowView()
	{
		$akses = UserAccess::model()->find(array("with"=>array("access"),'condition'=>"id_user='".Yii::app()->user->id."' and access.access_controller='JaminanPasien' and access.access_action='view'"));
		if(!empty($akses)) return true; else return false;
	}
	
	public function getAllowUpdate()
	{
		$akses = UserAccess::model()->find(array("with"=>array("access"),'condition'=>"id_user='".Yii::app()->user->id."' and access.access_controller='JaminanPasien' and access.access_action='update'"));
		if(!empty($akses)) return true; else return false;
	}
	
	public function getAllowDelete($id)
	{
		$akses = UserAccess::model()->find(array("with"=>array("access"),'condition'=>"id_user='".Yii::app()->user->id."' and access.access_controller='JaminanPasien' and access.access_action='delete'"));
		$rel=$this->getEmptyRelation($id);
		if(!empty($akses) AND $rel==true){
			return true;
		}else{
			return false;
		}
	}
	
	
	public function getEmptyRelation($id)
	{
		$count=0;
														if($count>0){
			return false;
		}else{
			return true;
		}
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return JaminanPasien the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
