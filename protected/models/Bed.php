<?php

/**
 * This is the model class for table "bed".
 *
 * The followings are the available columns in table 'bed':
 * @property string $id_bed
 * @property string $id_ruang_rawatan
 * @property string $no_bed
 * @property string $posisi_bed
 * @property string $status_bed
 * @property string $kondisi_bed
 * @property string $keterangan_bed
 *
 * The followings are the available model relations:
 * @property RuanganRawatan $idRuangRawatan
 * @property BedPasien[] $bedPasiens
 */
class Bed extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'bed';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('id_ruang_rawatan, no_bed, status_bed, kondisi_bed', 'required'),
			array('id_ruang_rawatan', 'length', 'max'=>20),
			array('no_bed, posisi_bed', 'length', 'max'=>45),
			array('status_bed', 'length', 'max'=>14),
			array('kondisi_bed', 'length', 'max'=>24),
			array('keterangan_bed', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id_bed, id_ruang_rawatan, no_bed, posisi_bed, status_bed, kondisi_bed, keterangan_bed', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'idRuangRawatan' => array(self::BELONGS_TO, 'RuanganRawatan', 'id_ruang_rawatan'),
			'bedPasiens' => array(self::HAS_MANY, 'BedPasien', 'id_bed'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id_bed' => 'Id Bed',
			'id_ruang_rawatan' => 'Id Ruang Rawatan',
			'no_bed' => 'No Bed',
			'posisi_bed' => 'Posisi Bed',
			'status_bed' => 'Status Bed',
			'kondisi_bed' => 'Kondisi Bed',
			'keterangan_bed' => 'Keterangan Bed',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id_bed',$this->id_bed,true);
		$criteria->compare('id_ruang_rawatan',$this->id_ruang_rawatan,true);
		$criteria->compare('no_bed',$this->no_bed,true);
		$criteria->compare('posisi_bed',$this->posisi_bed,true);
		$criteria->compare('status_bed',$this->status_bed,true);
		$criteria->compare('kondisi_bed',$this->kondisi_bed,true);
		$criteria->compare('keterangan_bed',$this->keterangan_bed,true);

		return new CActiveDataProvider($this, array(
			'pagination'=>array(
				'pageSize'=> Yii::app()->user->getState('pageSize',Yii::app()->params['defaultPageSize']),
			),
			'criteria'=>$criteria,
		));
	}
	
	public function getAllowView()
	{
		$akses = UserAccess::model()->find(array("with"=>array("access"),'condition'=>"id_user='".Yii::app()->user->id."' and access.access_controller='Bed' and access.access_action='view'"));
		if(!empty($akses)) return true; else return false;
	}
	
	public function getAllowUpdate()
	{
		$akses = UserAccess::model()->find(array("with"=>array("access"),'condition'=>"id_user='".Yii::app()->user->id."' and access.access_controller='Bed' and access.access_action='update'"));
		if(!empty($akses)) return true; else return false;
	}
	
	public function getAllowDelete($id)
	{
		$akses = UserAccess::model()->find(array("with"=>array("access"),'condition'=>"id_user='".Yii::app()->user->id."' and access.access_controller='Bed' and access.access_action='delete'"));
		$rel=$this->getEmptyRelation($id);
		if(!empty($akses) AND $rel==true){
			return true;
		}else{
			return false;
		}
	}
	
	
	public function getEmptyRelation($id)
	{
		$count=0;
				$count+=BedPasien::model()->count(array('condition'=>"id_bed='$id'"));
		if($count>0){
			return false;
		}else{
			return true;
		}
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Bed the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
