<?php

/**
 * This is the model class for table "queue".
 *
 * The followings are the available columns in table 'queue':
 * @property string $q_id
 * @property string $q_number
 * @property string $q_time_generate
 * @property string $q_time_available
 * @property string $q_status
 * @property string $is_available
 * @property string $qg_id
 * @property string $qt_id
 * @property string $qcc_id
 *
 * The followings are the available model relations:
 * @property QueueCounter $qcc
 * @property QueueGenerator $qg
 * @property QueueType $qt
 * @property QueueCall[] $queueCalls
 * @property Registrasi[] $registrasis
 */
class Queue extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'queue';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('qg_id, qt_id, qcc_id', 'required'),
			array('q_number, qg_id, qt_id, qcc_id', 'length', 'max'=>20),
			array('q_status, is_available', 'length', 'max'=>45),
			array('q_time_generate, q_time_available', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('q_id, q_number, q_time_generate, q_time_available, q_status, is_available, qg_id, qt_id, qcc_id', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'qcc' => array(self::BELONGS_TO, 'QueueCounter', 'qcc_id'),
			'qg' => array(self::BELONGS_TO, 'QueueGenerator', 'qg_id'),
			'qt' => array(self::BELONGS_TO, 'QueueType', 'qt_id'),
			'queueCalls' => array(self::HAS_MANY, 'QueueCall', 'q_id'),
			'registrasis' => array(self::HAS_MANY, 'Registrasi', 'q_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'q_id' => 'Q',
			'q_number' => 'Q Number',
			'q_time_generate' => 'Q Time Generate',
			'q_time_available' => 'Q Time Available',
			'q_status' => 'Q Status',
			'is_available' => 'Is Available',
			'qg_id' => 'Qg',
			'qt_id' => 'Qt',
			'qcc_id' => 'Qcc',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('q_id',$this->q_id,true);
		$criteria->compare('q_number',$this->q_number,true);
		$criteria->compare('q_time_generate',$this->q_time_generate,true);
		$criteria->compare('q_time_available',$this->q_time_available,true);
		$criteria->compare('q_status',$this->q_status,true);
		$criteria->compare('is_available',$this->is_available,true);
		$criteria->compare('qg_id',$this->qg_id,true);
		$criteria->compare('qt_id',$this->qt_id,true);
		$criteria->compare('qcc_id',$this->qcc_id,true);

		return new CActiveDataProvider($this, array(
			'pagination'=>array(
				'pageSize'=> Yii::app()->user->getState('pageSize',Yii::app()->params['defaultPageSize']),
			),
			'criteria'=>$criteria,
		));
	}
	
	public function getAllowView()
	{
		$akses = UserAccess::model()->find(array("with"=>array("access"),'condition'=>"id_user='".Yii::app()->user->id."' and access.access_controller='Queue' and access.access_action='view'"));
		if(!empty($akses)) return true; else return false;
	}
	
	public function getAllowUpdate()
	{
		$akses = UserAccess::model()->find(array("with"=>array("access"),'condition'=>"id_user='".Yii::app()->user->id."' and access.access_controller='Queue' and access.access_action='update'"));
		if(!empty($akses)) return true; else return false;
	}
	
	public function getAllowDelete($id)
	{
		$akses = UserAccess::model()->find(array("with"=>array("access"),'condition'=>"id_user='".Yii::app()->user->id."' and access.access_controller='Queue' and access.access_action='delete'"));
		$rel=$this->getEmptyRelation($id);
		if(!empty($akses) AND $rel==true){
			return true;
		}else{
			return false;
		}
	}
	
	
	public function getEmptyRelation($id)
	{
		$count=0;
								$count+=QueueCall::model()->count(array('condition'=>"q_id='$id'"));
		$count+=Registrasi::model()->count(array('condition'=>"q_id='$id'"));
		if($count>0){
			return false;
		}else{
			return true;
		}
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Queue the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
