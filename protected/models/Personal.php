<?php

/**
 * This is the model class for table "personal".
 *
 * The followings are the available columns in table 'personal':
 * @property string $id_personal
 * @property string $nama_lengkap
 * @property string $jenis_kelamin
 * @property string $tempat_lahir
 * @property string $tanggal_lahir
 * @property string $status_perkawinan
 * @property string $id_agama
 * @property string $id_pendidikan
 * @property string $id_pekerjaan
 * @property string $alamat_domisili
 * @property string $alamat_sementara
 * @property string $id_negara
 * @property string $id_provinsi
 * @property string $id_kabupaten
 * @property string $id_kecamatan
 *
 * The followings are the available model relations:
 * @property IdentitasPersonal[] $identitasPersonals
 * @property KontakPersonal[] $kontakPersonals
 * @property Pasien[] $pasiens
 * @property Pegawai[] $pegawais
 * @property PembayaranTagihan[] $pembayaranTagihans
 * @property Agama $idAgama
 * @property Kabupaten $idKabupaten
 * @property Kecamatan $idKecamatan
 * @property Negara $idNegara
 * @property Pekerjaan $idPekerjaan
 * @property Pendidikan $idPendidikan
 * @property Provinsi $idProvinsi
 * @property PersonalKeluarga[] $personalKeluargas
 * @property PersonalKeluarga[] $personalKeluargas1
 */
class Personal extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public $id_pasien;
	public function tableName()
	{
		return 'personal';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('nama_lengkap, jenis_kelamin', 'required'),
			array('nama_lengkap', 'length', 'max'=>60),
			array('jenis_kelamin', 'length', 'max'=>9),
			array('tempat_lahir', 'length', 'max'=>100),
			array('status_perkawinan', 'length', 'max'=>45),
			array('id_agama, id_pendidikan, id_pekerjaan', 'length', 'max'=>20),
			array('id_negara, id_provinsi', 'length', 'max'=>2),
			array('id_kabupaten', 'length', 'max'=>4),
			array('id_kecamatan', 'length', 'max'=>7),
			array('tanggal_lahir, alamat_domisili, alamat_sementara', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id_personal, nama_lengkap, jenis_kelamin, tempat_lahir, tanggal_lahir, status_perkawinan, id_agama, id_pendidikan, id_pekerjaan, alamat_domisili, alamat_sementara, id_negara, id_provinsi, id_kabupaten, id_kecamatan', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'identitasPersonals' => array(self::HAS_MANY, 'IdentitasPersonal', 'id_personal'),
			'kontakPersonals' => array(self::HAS_MANY, 'KontakPersonal', 'id_personal'),
			'pasiens' => array(self::HAS_MANY, 'Pasien', 'id_personal'),
			'pegawais' => array(self::HAS_MANY, 'Pegawai', 'id_personal'),
			'pembayaranTagihans' => array(self::HAS_MANY, 'PembayaranTagihan', 'id_personal'),
			'idAgama' => array(self::BELONGS_TO, 'Agama', 'id_agama'),
			'idKabupaten' => array(self::BELONGS_TO, 'Kabupaten', 'id_kabupaten'),
			'idKecamatan' => array(self::BELONGS_TO, 'Kecamatan', 'id_kecamatan'),
			'idNegara' => array(self::BELONGS_TO, 'Negara', 'id_negara'),
			'idPekerjaan' => array(self::BELONGS_TO, 'Pekerjaan', 'id_pekerjaan'),
			'idPendidikan' => array(self::BELONGS_TO, 'Pendidikan', 'id_pendidikan'),
			'idProvinsi' => array(self::BELONGS_TO, 'Provinsi', 'id_provinsi'),
			'personalKeluargas' => array(self::HAS_MANY, 'PersonalKeluarga', 'id_personal_from'),
			'personalKeluargas1' => array(self::HAS_MANY, 'PersonalKeluarga', 'id_personal_to'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id_personal' => 'Id Personal',
			'nama_lengkap' => 'Nama Lengkap',
			'jenis_kelamin' => 'Jenis Kelamin',
			'tempat_lahir' => 'Tempat Lahir',
			'tanggal_lahir' => 'Tanggal Lahir',
			'status_perkawinan' => 'Status Perkawinan',
			'id_agama' => 'Id Agama',
			'id_pendidikan' => 'Id Pendidikan',
			'id_pekerjaan' => 'Id Pekerjaan',
			'alamat_domisili' => 'Alamat Domisili',
			'alamat_sementara' => 'Alamat Sementara',
			'id_negara' => 'Id Negara',
			'id_provinsi' => 'Id Provinsi',
			'id_kabupaten' => 'Id Kabupaten',
			'id_kecamatan' => 'Id Kecamatan',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id_personal',$this->id_personal,true);
		$criteria->compare('nama_lengkap',$this->nama_lengkap,true);
		$criteria->compare('jenis_kelamin',$this->jenis_kelamin,true);
		$criteria->compare('tempat_lahir',$this->tempat_lahir,true);
		$criteria->compare('tanggal_lahir',$this->tanggal_lahir,true);
		$criteria->compare('status_perkawinan',$this->status_perkawinan,true);
		$criteria->compare('id_agama',$this->id_agama,true);
		$criteria->compare('id_pendidikan',$this->id_pendidikan,true);
		$criteria->compare('id_pekerjaan',$this->id_pekerjaan,true);
		$criteria->compare('alamat_domisili',$this->alamat_domisili,true);
		$criteria->compare('alamat_sementara',$this->alamat_sementara,true);
		$criteria->compare('id_negara',$this->id_negara,true);
		$criteria->compare('id_provinsi',$this->id_provinsi,true);
		$criteria->compare('id_kabupaten',$this->id_kabupaten,true);
		$criteria->compare('id_kecamatan',$this->id_kecamatan,true);

		return new CActiveDataProvider($this, array(
			'pagination'=>array(
				'pageSize'=> Yii::app()->user->getState('pageSize',Yii::app()->params['defaultPageSize']),
			),
			'criteria'=>$criteria,
		));
	}
	
	public function getAllowView()
	{
		$akses = UserAccess::model()->find(array("with"=>array("access"),'condition'=>"id_user='".Yii::app()->user->id."' and access.access_controller='Personal' and access.access_action='view'"));
		if(!empty($akses)) return true; else return false;
	}
	
	public function getAllowUpdate()
	{
		$akses = UserAccess::model()->find(array("with"=>array("access"),'condition'=>"id_user='".Yii::app()->user->id."' and access.access_controller='Personal' and access.access_action='update'"));
		if(!empty($akses)) return true; else return false;
	}
	
	public function getAllowDelete($id)
	{
		$akses = UserAccess::model()->find(array("with"=>array("access"),'condition'=>"id_user='".Yii::app()->user->id."' and access.access_controller='Personal' and access.access_action='delete'"));
		$rel=$this->getEmptyRelation($id);
		if(!empty($akses) AND $rel==true){
			return true;
		}else{
			return false;
		}
	}
	
	
	public function getEmptyRelation($id)
	{
		$count=0;
		$count+=IdentitasPersonal::model()->count(array('condition'=>"id_personal='$id'"));
		$count+=KontakPersonal::model()->count(array('condition'=>"id_personal='$id'"));
		$count+=Pasien::model()->count(array('condition'=>"id_personal='$id'"));
		$count+=Pegawai::model()->count(array('condition'=>"id_personal='$id'"));
		$count+=PembayaranTagihan::model()->count(array('condition'=>"id_personal='$id'"));
																$count+=PersonalKeluarga::model()->count(array('condition'=>"id_personal_from='$id'"));
		$count+=PersonalKeluarga::model()->count(array('condition'=>"id_personal_to='$id'"));
		if($count>0){
			return false;
		}else{
			return true;
		}
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Personal the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
