<?php

/**
 * This is the model class for table "tarif_peralatan".
 *
 * The followings are the available columns in table 'tarif_peralatan':
 * @property string $id_tarif_peralatan
 * @property string $id_kelompok_tarif
 * @property string $id_kelas
 * @property string $id_peralatan
 * @property string $jasa_pelayanan
 * @property string $harga_jual
 *
 * The followings are the available model relations:
 * @property PeralatanPasien[] $peralatanPasiens
 * @property PeralatanPasienMedis[] $peralatanPasienMedises
 * @property Kelas $idKelas
 * @property KelompokTarif $idKelompokTarif
 * @property Peralatan $idPeralatan
 * @property TarifPeralatanMedis[] $tarifPeralatanMedises
 */
class TarifPeralatan extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'tarif_peralatan';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('id_kelompok_tarif, id_kelas, id_peralatan, jasa_pelayanan, harga_jual', 'required'),
			array('id_kelompok_tarif, id_kelas, id_peralatan', 'length', 'max'=>20),
			array('jasa_pelayanan, harga_jual', 'length', 'max'=>30),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id_tarif_peralatan, id_kelompok_tarif, id_kelas, id_peralatan, jasa_pelayanan, harga_jual', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'peralatanPasiens' => array(self::HAS_MANY, 'PeralatanPasien', 'id_tarif_peralatan'),
			'peralatanPasienMedises' => array(self::HAS_MANY, 'PeralatanPasienMedis', 'id_tarif_peralatan'),
			'idKelas' => array(self::BELONGS_TO, 'Kelas', 'id_kelas'),
			'idKelompokTarif' => array(self::BELONGS_TO, 'KelompokTarif', 'id_kelompok_tarif'),
			'idPeralatan' => array(self::BELONGS_TO, 'Peralatan', 'id_peralatan'),
			'tarifPeralatanMedises' => array(self::HAS_MANY, 'TarifPeralatanMedis', 'id_tarif_peralatan'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id_tarif_peralatan' => 'Id Tarif Peralatan',
			'id_kelompok_tarif' => 'Id Kelompok Tarif',
			'id_kelas' => 'Id Kelas',
			'id_peralatan' => 'Id Peralatan',
			'jasa_pelayanan' => 'Jasa Pelayanan',
			'harga_jual' => 'Harga Jual',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id_tarif_peralatan',$this->id_tarif_peralatan,true);
		$criteria->compare('id_kelompok_tarif',$this->id_kelompok_tarif,true);
		$criteria->compare('id_kelas',$this->id_kelas,true);
		$criteria->compare('id_peralatan',$this->id_peralatan,true);
		$criteria->compare('jasa_pelayanan',$this->jasa_pelayanan,true);
		$criteria->compare('harga_jual',$this->harga_jual,true);

		return new CActiveDataProvider($this, array(
			'pagination'=>array(
				'pageSize'=> Yii::app()->user->getState('pageSize',Yii::app()->params['defaultPageSize']),
			),
			'criteria'=>$criteria,
		));
	}
	
	public function getAllowView()
	{
		$akses = UserAccess::model()->find(array("with"=>array("access"),'condition'=>"id_user='".Yii::app()->user->id."' and access.access_controller='TarifPeralatan' and access.access_action='view'"));
		if(!empty($akses)) return true; else return false;
	}
	
	public function getAllowUpdate()
	{
		$akses = UserAccess::model()->find(array("with"=>array("access"),'condition'=>"id_user='".Yii::app()->user->id."' and access.access_controller='TarifPeralatan' and access.access_action='update'"));
		if(!empty($akses)) return true; else return false;
	}
	
	public function getAllowDelete($id)
	{
		$akses = UserAccess::model()->find(array("with"=>array("access"),'condition'=>"id_user='".Yii::app()->user->id."' and access.access_controller='TarifPeralatan' and access.access_action='delete'"));
		$rel=$this->getEmptyRelation($id);
		if(!empty($akses) AND $rel==true){
			return true;
		}else{
			return false;
		}
	}
	
	
	public function getEmptyRelation($id)
	{
		$count=0;
		$count+=PeralatanPasien::model()->count(array('condition'=>"id_tarif_peralatan='$id'"));
		$count+=PeralatanPasienMedis::model()->count(array('condition'=>"id_tarif_peralatan='$id'"));
								$count+=TarifPeralatanMedis::model()->count(array('condition'=>"id_tarif_peralatan='$id'"));
		if($count>0){
			return false;
		}else{
			return true;
		}
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return TarifPeralatan the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
