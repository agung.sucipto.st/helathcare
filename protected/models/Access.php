<?php

/**
 * This is the model class for table "access".
 *
 * The followings are the available columns in table 'access':
 * @property integer $access_id
 * @property string $access_name
 * @property string $access_controller
 * @property string $access_action
 * @property integer $access_parent
 * @property integer $access_status
 * @property integer $access_visible
 *
 * The followings are the available model relations:
 * @property AccessMenuArrange[] $accessMenuArranges
 * @property UserAccess[] $userAccesses
 */
class Access extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public $acccess_path;
	public function tableName()
	{
		return 'access';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('access_controller, access_action, access_status, access_visible', 'required'),
			array('access_parent, access_status, access_visible', 'numerical', 'integerOnly'=>true),
			array('access_name, access_controller, access_action', 'length', 'max'=>50),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('access_id, access_name, access_controller, access_action, access_parent, access_status, access_visible', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'accessMenuArranges' => array(self::HAS_MANY, 'AccessMenuArrange', 'access_id'),
			'userAccesses' => array(self::HAS_MANY, 'UserAccess', 'access_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'access_id' => 'ID',
			'access_name' => 'Name',
			'access_controller' => 'Controller',
			'access_action' => 'Action',
			'access_parent' => 'Parent',
			'access_status' => 'Status',
			'access_visible' => 'Visible',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;
		
		$criteria->compare('access_id',$this->access_id);
		$criteria->compare('access_name',$this->access_name,true);
		$criteria->compare('access_controller',$this->access_controller,true);
		$criteria->compare('access_action',$this->access_action,true);
		$criteria->compare('access_parent',$this->access_parent);
		$criteria->compare('access_status',$this->access_status);
		$criteria->compare('access_visible',$this->access_visible);

		return new CActiveDataProvider($this, array(
			'pagination'=>array(
				'pageSize'=> Yii::app()->user->getState('pageSize',Yii::app()->params['defaultPageSize']),
			),
			'criteria'=>$criteria,
		));
	}
	
	public function getEmpty($id){
		$no=0;
		$no+=Access::model()->count(array("condition"=>"access_parent='$id'"));
		$no+=UserAccess::model()->count(array("condition"=>"access_id='$id'"));
		if($no>0){
			return false;
		}else{
			return true;
		}
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Access the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
