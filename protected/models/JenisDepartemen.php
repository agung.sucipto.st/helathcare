<?php

/**
 * This is the model class for table "jenis_departemen".
 *
 * The followings are the available columns in table 'jenis_departemen':
 * @property string $id_jenis_departemen
 * @property string $nama_jenis_departemen
 * @property string $jenis_layanan
 *
 * The followings are the available model relations:
 * @property Departemen[] $departemens
 * @property TarifAdministrasi[] $tarifAdministrasis
 */
class JenisDepartemen extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'jenis_departemen';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('nama_jenis_departemen, jenis_layanan', 'required'),
			array('nama_jenis_departemen', 'length', 'max'=>45),
			array('jenis_layanan', 'length', 'max'=>11),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id_jenis_departemen, nama_jenis_departemen, jenis_layanan', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'departemens' => array(self::HAS_MANY, 'Departemen', 'id_jenis_departemen'),
			'tarifAdministrasis' => array(self::HAS_MANY, 'TarifAdministrasi', 'id_jenis_departemen'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id_jenis_departemen' => 'Id Jenis Departemen',
			'nama_jenis_departemen' => 'Nama Jenis Departemen',
			'jenis_layanan' => 'Jenis Layanan',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id_jenis_departemen',$this->id_jenis_departemen,true);
		$criteria->compare('nama_jenis_departemen',$this->nama_jenis_departemen,true);
		$criteria->compare('jenis_layanan',$this->jenis_layanan,true);

		return new CActiveDataProvider($this, array(
			'pagination'=>array(
				'pageSize'=> Yii::app()->user->getState('pageSize',Yii::app()->params['defaultPageSize']),
			),
			'criteria'=>$criteria,
		));
	}
	
	public function getAllowView()
	{
		$akses = UserAccess::model()->find(array("with"=>array("access"),'condition'=>"id_user='".Yii::app()->user->id."' and access.access_controller='JenisDepartemen' and access.access_action='view'"));
		if(!empty($akses)) return true; else return false;
	}
	
	public function getAllowUpdate()
	{
		$akses = UserAccess::model()->find(array("with"=>array("access"),'condition'=>"id_user='".Yii::app()->user->id."' and access.access_controller='JenisDepartemen' and access.access_action='update'"));
		if(!empty($akses)) return true; else return false;
	}
	
	public function getAllowDelete($id)
	{
		$akses = UserAccess::model()->find(array("with"=>array("access"),'condition'=>"id_user='".Yii::app()->user->id."' and access.access_controller='JenisDepartemen' and access.access_action='delete'"));
		$rel=$this->getEmptyRelation($id);
		if(!empty($akses) AND $rel==true){
			return true;
		}else{
			return false;
		}
	}
	
	
	public function getEmptyRelation($id)
	{
		$count=0;
		$count+=Departemen::model()->count(array('condition'=>"id_jenis_departemen='$id'"));
		$count+=TarifAdministrasi::model()->count(array('condition'=>"id_jenis_departemen='$id'"));
		if($count>0){
			return false;
		}else{
			return true;
		}
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return JenisDepartemen the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
