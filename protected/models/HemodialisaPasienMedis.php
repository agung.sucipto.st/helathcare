<?php

/**
 * This is the model class for table "hemodialisa_pasien_medis".
 *
 * The followings are the available columns in table 'hemodialisa_pasien_medis':
 * @property string $id_hemodialisa_pasien_medis
 * @property string $id_hemodialisa_pasien_list
 * @property string $id_pegawai
 * @property string $id_tarif_tindakan_medis
 * @property string $id_tindakan_medis
 * @property string $jasa_medis
 * @property string $jasa_medis_org
 * @property string $gratis_biaya_jasa
 *
 * The followings are the available model relations:
 * @property DaftarJasaMedis[] $daftarJasaMedises
 * @property HemodialisaPasienList $idHemodialisaPasienList
 * @property Pegawai $idPegawai
 * @property TarifTindakan $idTarifTindakanMedis
 * @property TindakanMedis $idTindakanMedis
 */
class HemodialisaPasienMedis extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'hemodialisa_pasien_medis';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('id_hemodialisa_pasien_list, id_pegawai, id_tarif_tindakan_medis, id_tindakan_medis', 'required'),
			array('id_hemodialisa_pasien_list, id_pegawai, id_tarif_tindakan_medis, id_tindakan_medis', 'length', 'max'=>20),
			array('jasa_medis, jasa_medis_org', 'length', 'max'=>30),
			array('gratis_biaya_jasa', 'length', 'max'=>5),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id_hemodialisa_pasien_medis, id_hemodialisa_pasien_list, id_pegawai, id_tarif_tindakan_medis, id_tindakan_medis, jasa_medis, jasa_medis_org, gratis_biaya_jasa', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'daftarJasaMedises' => array(self::HAS_MANY, 'DaftarJasaMedis', 'id_hemodialisa_pasien_medis'),
			'idHemodialisaPasienList' => array(self::BELONGS_TO, 'HemodialisaPasienList', 'id_hemodialisa_pasien_list'),
			'idPegawai' => array(self::BELONGS_TO, 'Pegawai', 'id_pegawai'),
			'idTarifTindakanMedis' => array(self::BELONGS_TO, 'TarifTindakan', 'id_tarif_tindakan_medis'),
			'idTindakanMedis' => array(self::BELONGS_TO, 'TindakanMedis', 'id_tindakan_medis'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id_hemodialisa_pasien_medis' => 'Id Hemodialisa Pasien Medis',
			'id_hemodialisa_pasien_list' => 'Id Hemodialisa Pasien List',
			'id_pegawai' => 'Id Pegawai',
			'id_tarif_tindakan_medis' => 'Id Tarif Tindakan Medis',
			'id_tindakan_medis' => 'Id Tindakan Medis',
			'jasa_medis' => 'Jasa Medis',
			'jasa_medis_org' => 'Jasa Medis Org',
			'gratis_biaya_jasa' => 'Gratis Biaya Jasa',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id_hemodialisa_pasien_medis',$this->id_hemodialisa_pasien_medis,true);
		$criteria->compare('id_hemodialisa_pasien_list',$this->id_hemodialisa_pasien_list,true);
		$criteria->compare('id_pegawai',$this->id_pegawai,true);
		$criteria->compare('id_tarif_tindakan_medis',$this->id_tarif_tindakan_medis,true);
		$criteria->compare('id_tindakan_medis',$this->id_tindakan_medis,true);
		$criteria->compare('jasa_medis',$this->jasa_medis,true);
		$criteria->compare('jasa_medis_org',$this->jasa_medis_org,true);
		$criteria->compare('gratis_biaya_jasa',$this->gratis_biaya_jasa,true);

		return new CActiveDataProvider($this, array(
			'pagination'=>array(
				'pageSize'=> Yii::app()->user->getState('pageSize',Yii::app()->params['defaultPageSize']),
			),
			'criteria'=>$criteria,
		));
	}
	
	public function getAllowView()
	{
		$akses = UserAccess::model()->find(array("with"=>array("access"),'condition'=>"id_user='".Yii::app()->user->id."' and access.access_controller='HemodialisaPasienMedis' and access.access_action='view'"));
		if(!empty($akses)) return true; else return false;
	}
	
	public function getAllowUpdate()
	{
		$akses = UserAccess::model()->find(array("with"=>array("access"),'condition'=>"id_user='".Yii::app()->user->id."' and access.access_controller='HemodialisaPasienMedis' and access.access_action='update'"));
		if(!empty($akses)) return true; else return false;
	}
	
	public function getAllowDelete($id)
	{
		$akses = UserAccess::model()->find(array("with"=>array("access"),'condition'=>"id_user='".Yii::app()->user->id."' and access.access_controller='HemodialisaPasienMedis' and access.access_action='delete'"));
		$rel=$this->getEmptyRelation($id);
		if(!empty($akses) AND $rel==true){
			return true;
		}else{
			return false;
		}
	}
	
	
	public function getEmptyRelation($id)
	{
		$count=0;
		$count+=DaftarJasaMedis::model()->count(array('condition'=>"id_hemodialisa_pasien_medis='$id'"));
										if($count>0){
			return false;
		}else{
			return true;
		}
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return HemodialisaPasienMedis the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
