<?php

/**
 * This is the model class for table "laboratorium_pasien_list".
 *
 * The followings are the available columns in table 'laboratorium_pasien_list':
 * @property string $id_laboratorium_pasien_list
 * @property string $id_laboratorium_pasien
 * @property string $id_laboratorium
 * @property string $id_tarif_laboratorium
 * @property string $id_petugas_lab
 * @property string $id_dokter_pemeriksa
 * @property string $jumlah
 * @property string $hasil
 * @property string $normal
 * @property string $waktu_penyelesaian
 * @property string $status
 * @property string $status_rujukan
 * @property string $id_rujukan
 * @property string $biaya_rujukan
 * @property string $id_daftar_tagihan
 *
 * The followings are the available model relations:
 * @property DaftarTagihan[] $daftarTagihans
 * @property Laboratorium $idLaboratorium
 * @property LaboratoriumPasien $idLaboratoriumPasien
 * @property Pegawai $idPetugasLab
 * @property Pegawai $idDokterPemeriksa
 * @property Supplier $idRujukan
 * @property TarifLaboratorium $idTarifLaboratorium
 * @property DaftarTagihan $idDaftarTagihan
 * @property LaboratoriumPasienMedis[] $laboratoriumPasienMedises
 */
class LaboratoriumPasienList extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'laboratorium_pasien_list';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('id_laboratorium_pasien, id_laboratorium, id_tarif_laboratorium, id_dokter_pemeriksa, jumlah, status, status_rujukan', 'required'),
			array('id_laboratorium_pasien, id_laboratorium, id_tarif_laboratorium, id_petugas_lab, id_dokter_pemeriksa, jumlah, id_rujukan, id_daftar_tagihan', 'length', 'max'=>20),
			array('status', 'length', 'max'=>45),
			array('status_rujukan', 'length', 'max'=>1),
			array('biaya_rujukan', 'length', 'max'=>30),
			array('hasil, normal, waktu_penyelesaian', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id_laboratorium_pasien_list, id_laboratorium_pasien, id_laboratorium, id_tarif_laboratorium, id_petugas_lab, id_dokter_pemeriksa, jumlah, hasil, normal, waktu_penyelesaian, status, status_rujukan, id_rujukan, biaya_rujukan, id_daftar_tagihan', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'daftarTagihans' => array(self::HAS_MANY, 'DaftarTagihan', 'id_laboratorium_pasien_list'),
			'idLaboratorium' => array(self::BELONGS_TO, 'Laboratorium', 'id_laboratorium'),
			'idLaboratoriumPasien' => array(self::BELONGS_TO, 'LaboratoriumPasien', 'id_laboratorium_pasien'),
			'idPetugasLab' => array(self::BELONGS_TO, 'Pegawai', 'id_petugas_lab'),
			'idDokterPemeriksa' => array(self::BELONGS_TO, 'Pegawai', 'id_dokter_pemeriksa'),
			'idRujukan' => array(self::BELONGS_TO, 'Supplier', 'id_rujukan'),
			'idTarifLaboratorium' => array(self::BELONGS_TO, 'TarifLaboratorium', 'id_tarif_laboratorium'),
			'idDaftarTagihan' => array(self::BELONGS_TO, 'DaftarTagihan', 'id_daftar_tagihan'),
			'laboratoriumPasienMedises' => array(self::HAS_MANY, 'LaboratoriumPasienMedis', 'id_laboratorium_pasien_list'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id_laboratorium_pasien_list' => 'Id Laboratorium Pasien List',
			'id_laboratorium_pasien' => 'Id Laboratorium Pasien',
			'id_laboratorium' => 'Id Laboratorium',
			'id_tarif_laboratorium' => 'Id Tarif Laboratorium',
			'id_petugas_lab' => 'Id Petugas Lab',
			'id_dokter_pemeriksa' => 'Id Dokter Pemeriksa',
			'jumlah' => 'Jumlah',
			'hasil' => 'Hasil',
			'normal' => 'Normal',
			'waktu_penyelesaian' => 'Waktu Penyelesaian',
			'status' => 'Status',
			'status_rujukan' => 'Status Rujukan',
			'id_rujukan' => 'Id Rujukan',
			'biaya_rujukan' => 'Biaya Rujukan',
			'id_daftar_tagihan' => 'Id Daftar Tagihan',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id_laboratorium_pasien_list',$this->id_laboratorium_pasien_list,true);
		$criteria->compare('id_laboratorium_pasien',$this->id_laboratorium_pasien,true);
		$criteria->compare('id_laboratorium',$this->id_laboratorium,true);
		$criteria->compare('id_tarif_laboratorium',$this->id_tarif_laboratorium,true);
		$criteria->compare('id_petugas_lab',$this->id_petugas_lab,true);
		$criteria->compare('id_dokter_pemeriksa',$this->id_dokter_pemeriksa,true);
		$criteria->compare('jumlah',$this->jumlah,true);
		$criteria->compare('hasil',$this->hasil,true);
		$criteria->compare('normal',$this->normal,true);
		$criteria->compare('waktu_penyelesaian',$this->waktu_penyelesaian,true);
		$criteria->compare('status',$this->status,true);
		$criteria->compare('status_rujukan',$this->status_rujukan,true);
		$criteria->compare('id_rujukan',$this->id_rujukan,true);
		$criteria->compare('biaya_rujukan',$this->biaya_rujukan,true);
		$criteria->compare('id_daftar_tagihan',$this->id_daftar_tagihan,true);

		return new CActiveDataProvider($this, array(
			'pagination'=>array(
				'pageSize'=> Yii::app()->user->getState('pageSize',Yii::app()->params['defaultPageSize']),
			),
			'criteria'=>$criteria,
		));
	}
	
	public function getAllowView()
	{
		$akses = UserAccess::model()->find(array("with"=>array("access"),'condition'=>"id_user='".Yii::app()->user->id."' and access.access_controller='LaboratoriumPasienList' and access.access_action='view'"));
		if(!empty($akses)) return true; else return false;
	}
	
	public function getAllowUpdate()
	{
		$akses = UserAccess::model()->find(array("with"=>array("access"),'condition'=>"id_user='".Yii::app()->user->id."' and access.access_controller='LaboratoriumPasienList' and access.access_action='update'"));
		if(!empty($akses)) return true; else return false;
	}
	
	public function getAllowDelete($id)
	{
		$akses = UserAccess::model()->find(array("with"=>array("access"),'condition'=>"id_user='".Yii::app()->user->id."' and access.access_controller='LaboratoriumPasienList' and access.access_action='delete'"));
		$rel=$this->getEmptyRelation($id);
		if(!empty($akses) AND $rel==true){
			return true;
		}else{
			return false;
		}
	}
	
	
	public function getEmptyRelation($id)
	{
		$count=0;
		$count+=DaftarTagihan::model()->count(array('condition'=>"id_laboratorium_pasien_list='$id'"));
																$count+=LaboratoriumPasienMedis::model()->count(array('condition'=>"id_laboratorium_pasien_list='$id'"));
		if($count>0){
			return false;
		}else{
			return true;
		}
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return LaboratoriumPasienList the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
