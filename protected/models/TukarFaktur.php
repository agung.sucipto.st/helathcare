<?php

/**
 * This is the model class for table "tukar_faktur".
 *
 * The followings are the available columns in table 'tukar_faktur':
 * @property string $id_tukar_faktur
 * @property string $jenis_tukar_faktur
 * @property string $id_supplier
 * @property string $id_item_transaksi
 * @property string $no_invoice
 * @property string $nilai_invoice
 * @property string $tanggal_register
 * @property string $tanggal_tukar_faktur
 * @property string $tanggal_jatuh_tempo
 * @property string $faktur_pajak
 * @property string $tanggal_faktur_pajak
 * @property integer $ada_kwitansi
 * @property integer $ada_faktur_pajak
 * @property integer $ada_po_kontrak_spk
 * @property integer $ada_berita_acara
 * @property integer $ada_surat_jalan
 * @property integer $ada_tanda_terima_barang
 * @property string $status
 *
 * The followings are the available model relations:
 * @property PengajuanPembayaranSupplier $pengajuanPembayaranSupplier
 * @property PengajuanPembayaranSupplier[] $pengajuanPembayaranSuppliers
 * @property Supplier $idSupplier
 */
class TukarFaktur extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'tukar_faktur';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('jenis_tukar_faktur, id_supplier, tanggal_register, tanggal_tukar_faktur, faktur_pajak, ada_kwitansi, ada_faktur_pajak, ada_po_kontrak_spk, ada_berita_acara, ada_surat_jalan, ada_tanda_terima_barang, status', 'required'),
			array('ada_kwitansi, ada_faktur_pajak, ada_po_kontrak_spk, ada_berita_acara, ada_surat_jalan, ada_tanda_terima_barang', 'numerical', 'integerOnly'=>true),
			array('jenis_tukar_faktur', 'length', 'max'=>8),
			array('id_supplier, id_item_transaksi', 'length', 'max'=>20),
			array('no_invoice, faktur_pajak', 'length', 'max'=>100),
			array('nilai_invoice', 'length', 'max'=>30),
			array('status', 'length', 'max'=>14),
			array('tanggal_jatuh_tempo, tanggal_faktur_pajak', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id_tukar_faktur, jenis_tukar_faktur, id_supplier, id_item_transaksi, no_invoice, nilai_invoice, tanggal_register, tanggal_tukar_faktur, tanggal_jatuh_tempo, faktur_pajak, tanggal_faktur_pajak, ada_kwitansi, ada_faktur_pajak, ada_po_kontrak_spk, ada_berita_acara, ada_surat_jalan, ada_tanda_terima_barang, status', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'pengajuanPembayaranSupplier' => array(self::HAS_ONE, 'PengajuanPembayaranSupplier', 'id_pengajuan'),
			'pengajuanPembayaranSuppliers' => array(self::HAS_MANY, 'PengajuanPembayaranSupplier', 'id_tukar_faktur'),
			'idSupplier' => array(self::BELONGS_TO, 'Supplier', 'id_supplier'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id_tukar_faktur' => 'Id Tukar Faktur',
			'jenis_tukar_faktur' => 'Jenis Tukar Faktur',
			'id_supplier' => 'Id Supplier',
			'id_item_transaksi' => 'Id Item Transaksi',
			'no_invoice' => 'No Invoice',
			'nilai_invoice' => 'Nilai Invoice',
			'tanggal_register' => 'Tanggal Register',
			'tanggal_tukar_faktur' => 'Tanggal Tukar Faktur',
			'tanggal_jatuh_tempo' => 'Tanggal Jatuh Tempo',
			'faktur_pajak' => 'Faktur Pajak',
			'tanggal_faktur_pajak' => 'Tanggal Faktur Pajak',
			'ada_kwitansi' => 'Ada Kwitansi',
			'ada_faktur_pajak' => 'Ada Faktur Pajak',
			'ada_po_kontrak_spk' => 'Ada Po Kontrak Spk',
			'ada_berita_acara' => 'Ada Berita Acara',
			'ada_surat_jalan' => 'Ada Surat Jalan',
			'ada_tanda_terima_barang' => 'Ada Tanda Terima Barang',
			'status' => 'Status',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id_tukar_faktur',$this->id_tukar_faktur,true);
		$criteria->compare('jenis_tukar_faktur',$this->jenis_tukar_faktur,true);
		$criteria->compare('id_supplier',$this->id_supplier,true);
		$criteria->compare('id_item_transaksi',$this->id_item_transaksi,true);
		$criteria->compare('no_invoice',$this->no_invoice,true);
		$criteria->compare('nilai_invoice',$this->nilai_invoice,true);
		$criteria->compare('tanggal_register',$this->tanggal_register,true);
		$criteria->compare('tanggal_tukar_faktur',$this->tanggal_tukar_faktur,true);
		$criteria->compare('tanggal_jatuh_tempo',$this->tanggal_jatuh_tempo,true);
		$criteria->compare('faktur_pajak',$this->faktur_pajak,true);
		$criteria->compare('tanggal_faktur_pajak',$this->tanggal_faktur_pajak,true);
		$criteria->compare('ada_kwitansi',$this->ada_kwitansi);
		$criteria->compare('ada_faktur_pajak',$this->ada_faktur_pajak);
		$criteria->compare('ada_po_kontrak_spk',$this->ada_po_kontrak_spk);
		$criteria->compare('ada_berita_acara',$this->ada_berita_acara);
		$criteria->compare('ada_surat_jalan',$this->ada_surat_jalan);
		$criteria->compare('ada_tanda_terima_barang',$this->ada_tanda_terima_barang);
		$criteria->compare('status',$this->status,true);

		return new CActiveDataProvider($this, array(
			'pagination'=>array(
				'pageSize'=> Yii::app()->user->getState('pageSize',Yii::app()->params['defaultPageSize']),
			),
			'criteria'=>$criteria,
		));
	}
	
	public function getAllowView()
	{
		$akses = UserAccess::model()->find(array("with"=>array("access"),'condition'=>"id_user='".Yii::app()->user->id."' and access.access_controller='TukarFaktur' and access.access_action='view'"));
		if(!empty($akses)) return true; else return false;
	}
	
	public function getAllowUpdate()
	{
		$akses = UserAccess::model()->find(array("with"=>array("access"),'condition'=>"id_user='".Yii::app()->user->id."' and access.access_controller='TukarFaktur' and access.access_action='update'"));
		if(!empty($akses)) return true; else return false;
	}
	
	public function getAllowDelete($id)
	{
		$akses = UserAccess::model()->find(array("with"=>array("access"),'condition'=>"id_user='".Yii::app()->user->id."' and access.access_controller='TukarFaktur' and access.access_action='delete'"));
		$rel=$this->getEmptyRelation($id);
		if(!empty($akses) AND $rel==true){
			return true;
		}else{
			return false;
		}
	}
	
	
	public function getEmptyRelation($id)
	{
		$count=0;
				$count+=PengajuanPembayaranSupplier::model()->count(array('condition'=>"id_tukar_faktur='$id'"));
				if($count>0){
			return false;
		}else{
			return true;
		}
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return TukarFaktur the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
