<?php

/**
 * This is the model class for table "laboratorium_nilai_rujukan".
 *
 * The followings are the available columns in table 'laboratorium_nilai_rujukan':
 * @property string $id_laboratorium_nilai_rujukan
 * @property string $id_laboratorium
 * @property string $sex
 * @property string $age_from
 * @property string $age_to
 * @property string $nilai_minimum
 * @property string $nilai_maksimum
 * @property string $nilai_normal
 * @property string $keterangan
 *
 * The followings are the available model relations:
 * @property Laboratorium $idLaboratorium
 */
class LaboratoriumNilaiRujukan extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'laboratorium_nilai_rujukan';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('id_laboratorium, sex, age_from, age_to', 'required'),
			array('id_laboratorium, age_from, age_to, nilai_minimum, nilai_maksimum', 'length', 'max'=>20),
			array('sex', 'length', 'max'=>6),
			array('nilai_normal', 'length', 'max'=>45),
			array('keterangan', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id_laboratorium_nilai_rujukan, id_laboratorium, sex, age_from, age_to, nilai_minimum, nilai_maksimum, nilai_normal, keterangan', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'idLaboratorium' => array(self::BELONGS_TO, 'Laboratorium', 'id_laboratorium'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id_laboratorium_nilai_rujukan' => 'Id Laboratorium Nilai Rujukan',
			'id_laboratorium' => 'Id Laboratorium',
			'sex' => 'Sex',
			'age_from' => 'Age From',
			'age_to' => 'Age To',
			'nilai_minimum' => 'Nilai Minimum',
			'nilai_maksimum' => 'Nilai Maksimum',
			'nilai_normal' => 'Nilai Normal',
			'keterangan' => 'Keterangan',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id_laboratorium_nilai_rujukan',$this->id_laboratorium_nilai_rujukan,true);
		$criteria->compare('id_laboratorium',$this->id_laboratorium,true);
		$criteria->compare('sex',$this->sex,true);
		$criteria->compare('age_from',$this->age_from,true);
		$criteria->compare('age_to',$this->age_to,true);
		$criteria->compare('nilai_minimum',$this->nilai_minimum,true);
		$criteria->compare('nilai_maksimum',$this->nilai_maksimum,true);
		$criteria->compare('nilai_normal',$this->nilai_normal,true);
		$criteria->compare('keterangan',$this->keterangan,true);

		return new CActiveDataProvider($this, array(
			'pagination'=>array(
				'pageSize'=> Yii::app()->user->getState('pageSize',Yii::app()->params['defaultPageSize']),
			),
			'criteria'=>$criteria,
		));
	}
	
	public function getAllowView()
	{
		$akses = UserAccess::model()->find(array("with"=>array("access"),'condition'=>"id_user='".Yii::app()->user->id."' and access.access_controller='LaboratoriumNilaiRujukan' and access.access_action='view'"));
		if(!empty($akses)) return true; else return false;
	}
	
	public function getAllowUpdate()
	{
		$akses = UserAccess::model()->find(array("with"=>array("access"),'condition'=>"id_user='".Yii::app()->user->id."' and access.access_controller='LaboratoriumNilaiRujukan' and access.access_action='update'"));
		if(!empty($akses)) return true; else return false;
	}
	
	public function getAllowDelete($id)
	{
		$akses = UserAccess::model()->find(array("with"=>array("access"),'condition'=>"id_user='".Yii::app()->user->id."' and access.access_controller='LaboratoriumNilaiRujukan' and access.access_action='delete'"));
		$rel=$this->getEmptyRelation($id);
		if(!empty($akses) AND $rel==true){
			return true;
		}else{
			return false;
		}
	}
	
	
	public function getEmptyRelation($id)
	{
		$count=0;
				if($count>0){
			return false;
		}else{
			return true;
		}
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return LaboratoriumNilaiRujukan the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
