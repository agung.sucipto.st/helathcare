<?php

/**
 * This is the model class for table "jasa_medis".
 *
 * The followings are the available columns in table 'jasa_medis':
 * @property string $id_jasa_medis
 * @property string $id_pegawai
 * @property string $waktu_jasa_medis
 * @property string $total_jasa_medis
 * @property string $total_pph21
 * @property string $status_pembayaran
 * @property string $user_create
 * @property string $user_update
 * @property string $user_final
 * @property string $time_create
 * @property string $time_update
 * @property string $time_final
 *
 * The followings are the available model relations:
 * @property DaftarJasaMedis[] $daftarJasaMedises
 * @property Pegawai $idPegawai
 * @property User $userCreate
 * @property User $userUpdate
 * @property User $userFinal
 * @property PembayaranJasaMedis[] $pembayaranJasaMedises
 */
class JasaMedis extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'jasa_medis';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('id_pegawai, waktu_jasa_medis, total_jasa_medis, status_pembayaran, user_create, time_create', 'required'),
			array('id_pegawai, user_create, user_update, user_final', 'length', 'max'=>20),
			array('total_jasa_medis, total_pph21', 'length', 'max'=>30),
			array('status_pembayaran', 'length', 'max'=>50),
			array('time_update, time_final', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id_jasa_medis, id_pegawai, waktu_jasa_medis, total_jasa_medis, total_pph21, status_pembayaran, user_create, user_update, user_final, time_create, time_update, time_final', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'daftarJasaMedises' => array(self::HAS_MANY, 'DaftarJasaMedis', 'id_jasa_medis'),
			'idPegawai' => array(self::BELONGS_TO, 'Pegawai', 'id_pegawai'),
			'userCreate' => array(self::BELONGS_TO, 'User', 'user_create'),
			'userUpdate' => array(self::BELONGS_TO, 'User', 'user_update'),
			'userFinal' => array(self::BELONGS_TO, 'User', 'user_final'),
			'pembayaranJasaMedises' => array(self::HAS_MANY, 'PembayaranJasaMedis', 'id_jasa_medis'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id_jasa_medis' => 'Id Jasa Medis',
			'id_pegawai' => 'Id Pegawai',
			'waktu_jasa_medis' => 'Waktu Jasa Medis',
			'total_jasa_medis' => 'Total Jasa Medis',
			'total_pph21' => 'Total Pph21',
			'status_pembayaran' => 'Status Pembayaran',
			'user_create' => 'User Create',
			'user_update' => 'User Update',
			'user_final' => 'User Final',
			'time_create' => 'Time Create',
			'time_update' => 'Time Update',
			'time_final' => 'Time Final',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id_jasa_medis',$this->id_jasa_medis,true);
		$criteria->compare('id_pegawai',$this->id_pegawai,true);
		$criteria->compare('waktu_jasa_medis',$this->waktu_jasa_medis,true);
		$criteria->compare('total_jasa_medis',$this->total_jasa_medis,true);
		$criteria->compare('total_pph21',$this->total_pph21,true);
		$criteria->compare('status_pembayaran',$this->status_pembayaran,true);
		$criteria->compare('user_create',$this->user_create,true);
		$criteria->compare('user_update',$this->user_update,true);
		$criteria->compare('user_final',$this->user_final,true);
		$criteria->compare('time_create',$this->time_create,true);
		$criteria->compare('time_update',$this->time_update,true);
		$criteria->compare('time_final',$this->time_final,true);

		return new CActiveDataProvider($this, array(
			'pagination'=>array(
				'pageSize'=> Yii::app()->user->getState('pageSize',Yii::app()->params['defaultPageSize']),
			),
			'criteria'=>$criteria,
		));
	}
	
	public function getAllowView()
	{
		$akses = UserAccess::model()->find(array("with"=>array("access"),'condition'=>"id_user='".Yii::app()->user->id."' and access.access_controller='JasaMedis' and access.access_action='view'"));
		if(!empty($akses)) return true; else return false;
	}
	
	public function getAllowUpdate()
	{
		$akses = UserAccess::model()->find(array("with"=>array("access"),'condition'=>"id_user='".Yii::app()->user->id."' and access.access_controller='JasaMedis' and access.access_action='update'"));
		if(!empty($akses)) return true; else return false;
	}
	
	public function getAllowDelete($id)
	{
		$akses = UserAccess::model()->find(array("with"=>array("access"),'condition'=>"id_user='".Yii::app()->user->id."' and access.access_controller='JasaMedis' and access.access_action='delete'"));
		$rel=$this->getEmptyRelation($id);
		if(!empty($akses) AND $rel==true){
			return true;
		}else{
			return false;
		}
	}
	
	
	public function getEmptyRelation($id)
	{
		$count=0;
		$count+=DaftarJasaMedis::model()->count(array('condition'=>"id_jasa_medis='$id'"));
										$count+=PembayaranJasaMedis::model()->count(array('condition'=>"id_jasa_medis='$id'"));
		if($count>0){
			return false;
		}else{
			return true;
		}
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return JasaMedis the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
