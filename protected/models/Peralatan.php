<?php

/**
 * This is the model class for table "peralatan".
 *
 * The followings are the available columns in table 'peralatan':
 * @property string $id_peralatan
 * @property string $id_kelompok_peralatan
 * @property string $nama_peralatan
 * @property string $status
 *
 * The followings are the available model relations:
 * @property KelompokPeralatan $idKelompokPeralatan
 * @property PeralatanMedis[] $peralatanMedises
 * @property PeralatanPasien[] $peralatanPasiens
 * @property PeralatanPasienMedis[] $peralatanPasienMedises
 * @property TarifPeralatan[] $tarifPeralatans
 * @property TarifPeralatanMedis[] $tarifPeralatanMedises
 */
class Peralatan extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'peralatan';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('id_kelompok_peralatan, nama_peralatan', 'required'),
			array('id_kelompok_peralatan', 'length', 'max'=>20),
			array('nama_peralatan', 'length', 'max'=>60),
			array('status', 'length', 'max'=>11),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id_peralatan, id_kelompok_peralatan, nama_peralatan, status', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'idKelompokPeralatan' => array(self::BELONGS_TO, 'KelompokPeralatan', 'id_kelompok_peralatan'),
			'peralatanMedises' => array(self::HAS_MANY, 'PeralatanMedis', 'id_peralatan'),
			'peralatanPasiens' => array(self::HAS_MANY, 'PeralatanPasien', 'id_peralatan'),
			'peralatanPasienMedises' => array(self::HAS_MANY, 'PeralatanPasienMedis', 'id_peralatan'),
			'tarifPeralatans' => array(self::HAS_MANY, 'TarifPeralatan', 'id_peralatan'),
			'tarifPeralatanMedises' => array(self::HAS_MANY, 'TarifPeralatanMedis', 'id_peralatan'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id_peralatan' => 'Id Peralatan',
			'id_kelompok_peralatan' => 'Id Kelompok Peralatan',
			'nama_peralatan' => 'Nama Peralatan',
			'status' => 'Status',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id_peralatan',$this->id_peralatan,true);
		$criteria->compare('id_kelompok_peralatan',$this->id_kelompok_peralatan,true);
		$criteria->compare('nama_peralatan',$this->nama_peralatan,true);
		$criteria->compare('status',$this->status,true);

		return new CActiveDataProvider($this, array(
			'pagination'=>array(
				'pageSize'=> Yii::app()->user->getState('pageSize',Yii::app()->params['defaultPageSize']),
			),
			'criteria'=>$criteria,
		));
	}
	
	public function getAllowView()
	{
		$akses = UserAccess::model()->find(array("with"=>array("access"),'condition'=>"id_user='".Yii::app()->user->id."' and access.access_controller='Peralatan' and access.access_action='view'"));
		if(!empty($akses)) return true; else return false;
	}
	
	public function getAllowUpdate()
	{
		$akses = UserAccess::model()->find(array("with"=>array("access"),'condition'=>"id_user='".Yii::app()->user->id."' and access.access_controller='Peralatan' and access.access_action='update'"));
		if(!empty($akses)) return true; else return false;
	}
	
	public function getAllowDelete($id)
	{
		$akses = UserAccess::model()->find(array("with"=>array("access"),'condition'=>"id_user='".Yii::app()->user->id."' and access.access_controller='Peralatan' and access.access_action='delete'"));
		$rel=$this->getEmptyRelation($id);
		if(!empty($akses) AND $rel==true){
			return true;
		}else{
			return false;
		}
	}
	
	
	public function getEmptyRelation($id)
	{
		$count=0;
				$count+=PeralatanMedis::model()->count(array('condition'=>"id_peralatan='$id'"));
		$count+=PeralatanPasien::model()->count(array('condition'=>"id_peralatan='$id'"));
		$count+=PeralatanPasienMedis::model()->count(array('condition'=>"id_peralatan='$id'"));
		$count+=TarifPeralatan::model()->count(array('condition'=>"id_peralatan='$id'"));
		$count+=TarifPeralatanMedis::model()->count(array('condition'=>"id_peralatan='$id'"));
		if($count>0){
			return false;
		}else{
			return true;
		}
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Peralatan the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
