<?php

/**
 * This is the model class for table "tarif_administrasi".
 *
 * The followings are the available columns in table 'tarif_administrasi':
 * @property string $id_tarif_administrasi
 * @property string $id_jenis_departemen
 * @property string $id_kelompok_tarif
 * @property string $id_jenis_administrasi
 * @property string $minimum
 * @property string $maximum
 * @property string $persen
 *
 * The followings are the available model relations:
 * @property AdministrasiPasien[] $administrasiPasiens
 * @property JenisAdministrasi $idJenisAdministrasi
 * @property JenisDepartemen $idJenisDepartemen
 * @property KelompokTarif $idKelompokTarif
 */
class TarifAdministrasi extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'tarif_administrasi';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('id_jenis_departemen, id_kelompok_tarif, id_jenis_administrasi', 'required'),
			array('id_jenis_departemen, id_kelompok_tarif, id_jenis_administrasi', 'length', 'max'=>20),
			array('minimum, maximum, persen', 'length', 'max'=>30),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id_tarif_administrasi, id_jenis_departemen, id_kelompok_tarif, id_jenis_administrasi, minimum, maximum, persen', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'administrasiPasiens' => array(self::HAS_MANY, 'AdministrasiPasien', 'id_tarif_administrasi'),
			'idJenisAdministrasi' => array(self::BELONGS_TO, 'JenisAdministrasi', 'id_jenis_administrasi'),
			'idJenisDepartemen' => array(self::BELONGS_TO, 'JenisDepartemen', 'id_jenis_departemen'),
			'idKelompokTarif' => array(self::BELONGS_TO, 'KelompokTarif', 'id_kelompok_tarif'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id_tarif_administrasi' => 'Id Tarif Administrasi',
			'id_jenis_departemen' => 'Id Jenis Departemen',
			'id_kelompok_tarif' => 'Id Kelompok Tarif',
			'id_jenis_administrasi' => 'Id Jenis Administrasi',
			'minimum' => 'Minimum',
			'maximum' => 'Maximum',
			'persen' => 'Persen',
		);
	}

	public static function getTarif($reg,$type="Rajal",$item="Administrasi"){
		$id_jenis_departemen=$reg->idDepartemen->id_jenis_departemen;
		if($reg->jenis_jaminan=="UMUM"){
			$id_kelompok_tarif=1;
		}else{
			$jaminan=JaminanPasien::model()->find(array("condition"=>"id_registrasi='$reg->id_registrasi' AND is_utama='1'"));
			$id_kelompok_tarif=$jaminan->idPenjamin->id_kelompok_tarif;
		}
		
		if($type=="Rajal" AND $item=="Administrasi"){
			if($registrasi->jenis_kunjungan==1){
				$id_jenis_administrasi=1;
			}else{
				$id_jenis_administrasi=2;
			}
		}elseif($type=="Rajal" AND $item=="Kartu"){
			$id_jenis_administrasi=3;
		}elseif($type=="Ranap" AND $item=="Administrasi"){
			
		}elseif($type=="Ranap" AND $item=="Kartu"){
			
		}
		
		$model=TarifAdministrasi::model()->find(array("condition"=>"id_jenis_administrasi='$id_jenis_administrasi' and id_jenis_administrasi='$id_jenis_administrasi' and id_kelompok_tarif='$id_kelompok_tarif' AND id_jenis_departemen='$id_jenis_departemen'"));
		
		return $model;
	}
	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id_tarif_administrasi',$this->id_tarif_administrasi,true);
		$criteria->compare('id_jenis_departemen',$this->id_jenis_departemen,true);
		$criteria->compare('id_kelompok_tarif',$this->id_kelompok_tarif,true);
		$criteria->compare('id_jenis_administrasi',$this->id_jenis_administrasi,true);
		$criteria->compare('minimum',$this->minimum,true);
		$criteria->compare('maximum',$this->maximum,true);
		$criteria->compare('persen',$this->persen,true);

		return new CActiveDataProvider($this, array(
			'pagination'=>array(
				'pageSize'=> Yii::app()->user->getState('pageSize',Yii::app()->params['defaultPageSize']),
			),
			'criteria'=>$criteria,
		));
	}
	
	public function getAllowView()
	{
		$akses = UserAccess::model()->find(array("with"=>array("access"),'condition'=>"id_user='".Yii::app()->user->id."' and access.access_controller='TarifAdministrasi' and access.access_action='view'"));
		if(!empty($akses)) return true; else return false;
	}
	
	public function getAllowUpdate()
	{
		$akses = UserAccess::model()->find(array("with"=>array("access"),'condition'=>"id_user='".Yii::app()->user->id."' and access.access_controller='TarifAdministrasi' and access.access_action='update'"));
		if(!empty($akses)) return true; else return false;
	}
	
	public function getAllowDelete($id)
	{
		$akses = UserAccess::model()->find(array("with"=>array("access"),'condition'=>"id_user='".Yii::app()->user->id."' and access.access_controller='TarifAdministrasi' and access.access_action='delete'"));
		$rel=$this->getEmptyRelation($id);
		if(!empty($akses) AND $rel==true){
			return true;
		}else{
			return false;
		}
	}
	
	
	public function getEmptyRelation($id)
	{
		$count=0;
		$count+=AdministrasiPasien::model()->count(array('condition'=>"id_tarif_administrasi='$id'"));
								if($count>0){
			return false;
		}else{
			return true;
		}
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return TarifAdministrasi the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
