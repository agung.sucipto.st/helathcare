<?php

/**
 * This is the model class for table "pembayaran_hutang_supplier".
 *
 * The followings are the available columns in table 'pembayaran_hutang_supplier':
 * @property string $id_pembayaran
 * @property string $waktu_pembayaran
 * @property string $kode_pembayaran
 * @property string $id_pengajuan
 * @property string $id_supplier
 * @property integer $id_coa_kas
 * @property string $cara_bayar
 * @property string $no_bayar
 * @property string $tanggal_bayar
 * @property string $potongan
 * @property string $total_bayar
 * @property string $keterangan
 * @property string $status
 * @property string $user_create
 * @property string $user_approve_reject
 * @property string $time_create
 * @property string $time_approve_reject
 *
 * The followings are the available model relations:
 * @property Supplier $idSupplier
 * @property GlCoa $idCoaKas
 * @property PengajuanPembayaranSupplier $idPengajuan
 * @property User $userApproveReject
 * @property User $userCreate
 */
class PembayaranHutangSupplier extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'pembayaran_hutang_supplier';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('waktu_pembayaran, kode_pembayaran, id_pengajuan, id_supplier, id_coa_kas, cara_bayar, no_bayar, tanggal_bayar, potongan, total_bayar, keterangan, status, user_create, time_create', 'required'),
			array('id_coa_kas', 'numerical', 'integerOnly'=>true),
			array('kode_pembayaran, potongan, total_bayar', 'length', 'max'=>30),
			array('id_pengajuan, id_supplier, user_create, user_approve_reject', 'length', 'max'=>20),
			array('cara_bayar', 'length', 'max'=>15),
			array('no_bayar', 'length', 'max'=>100),
			array('status', 'length', 'max'=>10),
			array('time_approve_reject', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id_pembayaran, waktu_pembayaran, kode_pembayaran, id_pengajuan, id_supplier, id_coa_kas, cara_bayar, no_bayar, tanggal_bayar, potongan, total_bayar, keterangan, status, user_create, user_approve_reject, time_create, time_approve_reject', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'idSupplier' => array(self::BELONGS_TO, 'Supplier', 'id_supplier'),
			'idCoaKas' => array(self::BELONGS_TO, 'GlCoa', 'id_coa_kas'),
			'idPengajuan' => array(self::BELONGS_TO, 'PengajuanPembayaranSupplier', 'id_pengajuan'),
			'userApproveReject' => array(self::BELONGS_TO, 'User', 'user_approve_reject'),
			'userCreate' => array(self::BELONGS_TO, 'User', 'user_create'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id_pembayaran' => 'Id Pembayaran',
			'waktu_pembayaran' => 'Waktu Pembayaran',
			'kode_pembayaran' => 'Kode Pembayaran',
			'id_pengajuan' => 'Id Pengajuan',
			'id_supplier' => 'Id Supplier',
			'id_coa_kas' => 'Id Coa Kas',
			'cara_bayar' => 'Cara Bayar',
			'no_bayar' => 'No Bayar',
			'tanggal_bayar' => 'Tanggal Bayar',
			'potongan' => 'Potongan',
			'total_bayar' => 'Total Bayar',
			'keterangan' => 'Keterangan',
			'status' => 'Status',
			'user_create' => 'User Create',
			'user_approve_reject' => 'User Approve Reject',
			'time_create' => 'Time Create',
			'time_approve_reject' => 'Time Approve Reject',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id_pembayaran',$this->id_pembayaran,true);
		$criteria->compare('waktu_pembayaran',$this->waktu_pembayaran,true);
		$criteria->compare('kode_pembayaran',$this->kode_pembayaran,true);
		$criteria->compare('id_pengajuan',$this->id_pengajuan,true);
		$criteria->compare('id_supplier',$this->id_supplier,true);
		$criteria->compare('id_coa_kas',$this->id_coa_kas);
		$criteria->compare('cara_bayar',$this->cara_bayar,true);
		$criteria->compare('no_bayar',$this->no_bayar,true);
		$criteria->compare('tanggal_bayar',$this->tanggal_bayar,true);
		$criteria->compare('potongan',$this->potongan,true);
		$criteria->compare('total_bayar',$this->total_bayar,true);
		$criteria->compare('keterangan',$this->keterangan,true);
		$criteria->compare('status',$this->status,true);
		$criteria->compare('user_create',$this->user_create,true);
		$criteria->compare('user_approve_reject',$this->user_approve_reject,true);
		$criteria->compare('time_create',$this->time_create,true);
		$criteria->compare('time_approve_reject',$this->time_approve_reject,true);

		return new CActiveDataProvider($this, array(
			'pagination'=>array(
				'pageSize'=> Yii::app()->user->getState('pageSize',Yii::app()->params['defaultPageSize']),
			),
			'criteria'=>$criteria,
		));
	}
	
	public function getAllowView()
	{
		$akses = UserAccess::model()->find(array("with"=>array("access"),'condition'=>"id_user='".Yii::app()->user->id."' and access.access_controller='PembayaranHutangSupplier' and access.access_action='view'"));
		if(!empty($akses)) return true; else return false;
	}
	
	public function getAllowUpdate()
	{
		$akses = UserAccess::model()->find(array("with"=>array("access"),'condition'=>"id_user='".Yii::app()->user->id."' and access.access_controller='PembayaranHutangSupplier' and access.access_action='update'"));
		if(!empty($akses)) return true; else return false;
	}
	
	public function getAllowDelete($id)
	{
		$akses = UserAccess::model()->find(array("with"=>array("access"),'condition'=>"id_user='".Yii::app()->user->id."' and access.access_controller='PembayaranHutangSupplier' and access.access_action='delete'"));
		$rel=$this->getEmptyRelation($id);
		if(!empty($akses) AND $rel==true){
			return true;
		}else{
			return false;
		}
	}
	
	
	public function getEmptyRelation($id)
	{
		$count=0;
												if($count>0){
			return false;
		}else{
			return true;
		}
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return PembayaranHutangSupplier the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
