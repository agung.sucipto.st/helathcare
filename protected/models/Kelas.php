<?php

/**
 * This is the model class for table "kelas".
 *
 * The followings are the available columns in table 'kelas':
 * @property string $id_kelas
 * @property string $nama_kelas
 * @property string $jenis_kelas
 *
 * The followings are the available model relations:
 * @property BedPasien[] $bedPasiens
 * @property LaboratoriumPasien[] $laboratoriumPasiens
 * @property PeralatanPasien[] $peralatanPasiens
 * @property RadiologyPasien[] $radiologyPasiens
 * @property Registrasi[] $registrasis
 * @property Registrasi[] $registrasis1
 * @property RuanganRawatan[] $ruanganRawatans
 * @property TarifItem[] $tarifItems
 * @property TarifLaboratorium[] $tarifLaboratoria
 * @property TarifPeralatan[] $tarifPeralatans
 * @property TarifRadiology[] $tarifRadiologies
 * @property TarifRuangan[] $tarifRuangans
 * @property TarifTindakan[] $tarifTindakans
 * @property TarifVisiteDokter[] $tarifVisiteDokters
 * @property TindakanPasien[] $tindakanPasiens
 * @property VisiteDokterPasien[] $visiteDokterPasiens
 */
class Kelas extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'kelas';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('nama_kelas', 'length', 'max'=>100),
			array('jenis_kelas', 'length', 'max'=>45),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id_kelas, nama_kelas, jenis_kelas', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'bedPasiens' => array(self::HAS_MANY, 'BedPasien', 'id_kelas_diminta'),
			'laboratoriumPasiens' => array(self::HAS_MANY, 'LaboratoriumPasien', 'id_kelas'),
			'peralatanPasiens' => array(self::HAS_MANY, 'PeralatanPasien', 'id_kelas'),
			'radiologyPasiens' => array(self::HAS_MANY, 'RadiologyPasien', 'id_kelas'),
			'registrasis' => array(self::HAS_MANY, 'Registrasi', 'id_kelas'),
			'registrasis1' => array(self::HAS_MANY, 'Registrasi', 'id_kelas_titipan'),
			'ruanganRawatans' => array(self::HAS_MANY, 'RuanganRawatan', 'id_kelas'),
			'tarifItems' => array(self::HAS_MANY, 'TarifItem', 'id_kelas'),
			'tarifLaboratoria' => array(self::HAS_MANY, 'TarifLaboratorium', 'id_kelas'),
			'tarifPeralatans' => array(self::HAS_MANY, 'TarifPeralatan', 'id_kelas'),
			'tarifRadiologies' => array(self::HAS_MANY, 'TarifRadiology', 'id_kelas'),
			'tarifRuangans' => array(self::HAS_MANY, 'TarifRuangan', 'id_kelas'),
			'tarifTindakans' => array(self::HAS_MANY, 'TarifTindakan', 'id_kelas'),
			'tarifVisiteDokters' => array(self::HAS_MANY, 'TarifVisiteDokter', 'id_kelas'),
			'tindakanPasiens' => array(self::HAS_MANY, 'TindakanPasien', 'id_kelas'),
			'visiteDokterPasiens' => array(self::HAS_MANY, 'VisiteDokterPasien', 'id_kelas'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id_kelas' => 'Id Kelas',
			'nama_kelas' => 'Nama Kelas',
			'jenis_kelas' => 'Jenis Kelas',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id_kelas',$this->id_kelas,true);
		$criteria->compare('nama_kelas',$this->nama_kelas,true);
		$criteria->compare('jenis_kelas',$this->jenis_kelas,true);

		return new CActiveDataProvider($this, array(
			'pagination'=>array(
				'pageSize'=> Yii::app()->user->getState('pageSize',Yii::app()->params['defaultPageSize']),
			),
			'criteria'=>$criteria,
		));
	}
	
	public function getAllowView()
	{
		$akses = UserAccess::model()->find(array("with"=>array("access"),'condition'=>"id_user='".Yii::app()->user->id."' and access.access_controller='Kelas' and access.access_action='view'"));
		if(!empty($akses)) return true; else return false;
	}
	
	public function getAllowUpdate()
	{
		$akses = UserAccess::model()->find(array("with"=>array("access"),'condition'=>"id_user='".Yii::app()->user->id."' and access.access_controller='Kelas' and access.access_action='update'"));
		if(!empty($akses)) return true; else return false;
	}
	
	public function getAllowDelete($id)
	{
		$akses = UserAccess::model()->find(array("with"=>array("access"),'condition'=>"id_user='".Yii::app()->user->id."' and access.access_controller='Kelas' and access.access_action='delete'"));
		$rel=$this->getEmptyRelation($id);
		if(!empty($akses) AND $rel==true){
			return true;
		}else{
			return false;
		}
	}
	
	
	public function getEmptyRelation($id)
	{
		$count=0;
		$count+=BedPasien::model()->count(array('condition'=>"id_kelas_diminta='$id'"));
		$count+=LaboratoriumPasien::model()->count(array('condition'=>"id_kelas='$id'"));
		$count+=PeralatanPasien::model()->count(array('condition'=>"id_kelas='$id'"));
		$count+=RadiologyPasien::model()->count(array('condition'=>"id_kelas='$id'"));
		$count+=Registrasi::model()->count(array('condition'=>"id_kelas='$id'"));
		$count+=Registrasi::model()->count(array('condition'=>"id_kelas_titipan='$id'"));
		$count+=RuanganRawatan::model()->count(array('condition'=>"id_kelas='$id'"));
		$count+=TarifItem::model()->count(array('condition'=>"id_kelas='$id'"));
		$count+=TarifLaboratorium::model()->count(array('condition'=>"id_kelas='$id'"));
		$count+=TarifPeralatan::model()->count(array('condition'=>"id_kelas='$id'"));
		$count+=TarifRadiology::model()->count(array('condition'=>"id_kelas='$id'"));
		$count+=TarifRuangan::model()->count(array('condition'=>"id_kelas='$id'"));
		$count+=TarifTindakan::model()->count(array('condition'=>"id_kelas='$id'"));
		$count+=TarifVisiteDokter::model()->count(array('condition'=>"id_kelas='$id'"));
		$count+=TindakanPasien::model()->count(array('condition'=>"id_kelas='$id'"));
		$count+=VisiteDokterPasien::model()->count(array('condition'=>"id_kelas='$id'"));
		if($count>0){
			return false;
		}else{
			return true;
		}
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Kelas the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
