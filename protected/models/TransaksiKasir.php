<?php

/**
 * This is the model class for table "transaksi_kasir".
 *
 * The followings are the available columns in table 'transaksi_kasir':
 * @property string $id_transaksi_kasir
 * @property string $id_pegawai_kasir
 * @property string $waktu_transaksi
 * @property string $jenis_transaksi
 * @property string $cara_transaksi
 * @property string $id_pembayaran_tagihan
 * @property integer $jumlah
 * @property string $status_transaksi
 * @property string $keterangan_transaksi
 *
 * The followings are the available model relations:
 * @property Pegawai $idPegawaiKasir
 * @property PembayaranTagihan $idPembayaranTagihan
 */
class TransaksiKasir extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'transaksi_kasir';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('id_pegawai_kasir, waktu_transaksi, jenis_transaksi, cara_transaksi, id_pembayaran_tagihan, jumlah, status_transaksi, keterangan_transaksi', 'required'),
			array('jumlah', 'numerical', 'integerOnly'=>true),
			array('id_pegawai_kasir, id_pembayaran_tagihan', 'length', 'max'=>20),
			array('jenis_transaksi', 'length', 'max'=>11),
			array('cara_transaksi', 'length', 'max'=>8),
			array('status_transaksi', 'length', 'max'=>24),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id_transaksi_kasir, id_pegawai_kasir, waktu_transaksi, jenis_transaksi, cara_transaksi, id_pembayaran_tagihan, jumlah, status_transaksi, keterangan_transaksi', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'idPegawaiKasir' => array(self::BELONGS_TO, 'Pegawai', 'id_pegawai_kasir'),
			'idPembayaranTagihan' => array(self::BELONGS_TO, 'PembayaranTagihan', 'id_pembayaran_tagihan'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id_transaksi_kasir' => 'Id Transaksi Kasir',
			'id_pegawai_kasir' => 'Id Pegawai Kasir',
			'waktu_transaksi' => 'Waktu Transaksi',
			'jenis_transaksi' => 'Jenis Transaksi',
			'cara_transaksi' => 'Cara Transaksi',
			'id_pembayaran_tagihan' => 'Id Pembayaran Tagihan',
			'jumlah' => 'Jumlah',
			'status_transaksi' => 'Status Transaksi',
			'keterangan_transaksi' => 'Keterangan Transaksi',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id_transaksi_kasir',$this->id_transaksi_kasir,true);
		$criteria->compare('id_pegawai_kasir',$this->id_pegawai_kasir,true);
		$criteria->compare('waktu_transaksi',$this->waktu_transaksi,true);
		$criteria->compare('jenis_transaksi',$this->jenis_transaksi,true);
		$criteria->compare('cara_transaksi',$this->cara_transaksi,true);
		$criteria->compare('id_pembayaran_tagihan',$this->id_pembayaran_tagihan,true);
		$criteria->compare('jumlah',$this->jumlah);
		$criteria->compare('status_transaksi',$this->status_transaksi,true);
		$criteria->compare('keterangan_transaksi',$this->keterangan_transaksi,true);

		return new CActiveDataProvider($this, array(
			'pagination'=>array(
				'pageSize'=> Yii::app()->user->getState('pageSize',Yii::app()->params['defaultPageSize']),
			),
			'criteria'=>$criteria,
		));
	}
	
	public function getAllowView()
	{
		$akses = UserAccess::model()->find(array("with"=>array("access"),'condition'=>"id_user='".Yii::app()->user->id."' and access.access_controller='TransaksiKasir' and access.access_action='view'"));
		if(!empty($akses)) return true; else return false;
	}
	
	public function getAllowUpdate()
	{
		$akses = UserAccess::model()->find(array("with"=>array("access"),'condition'=>"id_user='".Yii::app()->user->id."' and access.access_controller='TransaksiKasir' and access.access_action='update'"));
		if(!empty($akses)) return true; else return false;
	}
	
	public function getAllowDelete($id)
	{
		$akses = UserAccess::model()->find(array("with"=>array("access"),'condition'=>"id_user='".Yii::app()->user->id."' and access.access_controller='TransaksiKasir' and access.access_action='delete'"));
		$rel=$this->getEmptyRelation($id);
		if(!empty($akses) AND $rel==true){
			return true;
		}else{
			return false;
		}
	}
	
	
	public function getEmptyRelation($id)
	{
		$count=0;
						if($count>0){
			return false;
		}else{
			return true;
		}
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return TransaksiKasir the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
