<?php

/**
 * This is the model class for table "history_edit_jaminan".
 *
 * The followings are the available columns in table 'history_edit_jaminan':
 * @property string $id_history_edit_jaminan
 * @property string $id_penjamin_asal
 * @property string $id_penjamin_tujuan
 * @property string $waktu_perubahan
 * @property string $alasan_perubahan
 * @property string $user_create
 * @property string $time_create
 *
 * The followings are the available model relations:
 * @property Penjamin $idPenjaminAsal
 * @property Penjamin $idPenjaminTujuan
 * @property User $userCreate
 */
class HistoryEditJaminan extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'history_edit_jaminan';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('id_history_edit_jaminan, id_penjamin_asal, id_penjamin_tujuan, waktu_perubahan, alasan_perubahan, user_create, time_create', 'required'),
			array('id_history_edit_jaminan, id_penjamin_asal, id_penjamin_tujuan, user_create', 'length', 'max'=>20),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id_history_edit_jaminan, id_penjamin_asal, id_penjamin_tujuan, waktu_perubahan, alasan_perubahan, user_create, time_create', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'idPenjaminAsal' => array(self::BELONGS_TO, 'Penjamin', 'id_penjamin_asal'),
			'idPenjaminTujuan' => array(self::BELONGS_TO, 'Penjamin', 'id_penjamin_tujuan'),
			'userCreate' => array(self::BELONGS_TO, 'User', 'user_create'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id_history_edit_jaminan' => 'Id History Edit Jaminan',
			'id_penjamin_asal' => 'Id Penjamin Asal',
			'id_penjamin_tujuan' => 'Id Penjamin Tujuan',
			'waktu_perubahan' => 'Waktu Perubahan',
			'alasan_perubahan' => 'Alasan Perubahan',
			'user_create' => 'User Create',
			'time_create' => 'Time Create',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id_history_edit_jaminan',$this->id_history_edit_jaminan,true);
		$criteria->compare('id_penjamin_asal',$this->id_penjamin_asal,true);
		$criteria->compare('id_penjamin_tujuan',$this->id_penjamin_tujuan,true);
		$criteria->compare('waktu_perubahan',$this->waktu_perubahan,true);
		$criteria->compare('alasan_perubahan',$this->alasan_perubahan,true);
		$criteria->compare('user_create',$this->user_create,true);
		$criteria->compare('time_create',$this->time_create,true);

		return new CActiveDataProvider($this, array(
			'pagination'=>array(
				'pageSize'=> Yii::app()->user->getState('pageSize',Yii::app()->params['defaultPageSize']),
			),
			'criteria'=>$criteria,
		));
	}
	
	public function getAllowView()
	{
		$akses = UserAccess::model()->find(array("with"=>array("access"),'condition'=>"id_user='".Yii::app()->user->id."' and access.access_controller='HistoryEditJaminan' and access.access_action='view'"));
		if(!empty($akses)) return true; else return false;
	}
	
	public function getAllowUpdate()
	{
		$akses = UserAccess::model()->find(array("with"=>array("access"),'condition'=>"id_user='".Yii::app()->user->id."' and access.access_controller='HistoryEditJaminan' and access.access_action='update'"));
		if(!empty($akses)) return true; else return false;
	}
	
	public function getAllowDelete($id)
	{
		$akses = UserAccess::model()->find(array("with"=>array("access"),'condition'=>"id_user='".Yii::app()->user->id."' and access.access_controller='HistoryEditJaminan' and access.access_action='delete'"));
		$rel=$this->getEmptyRelation($id);
		if(!empty($akses) AND $rel==true){
			return true;
		}else{
			return false;
		}
	}
	
	
	public function getEmptyRelation($id)
	{
		$count=0;
								if($count>0){
			return false;
		}else{
			return true;
		}
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return HistoryEditJaminan the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
