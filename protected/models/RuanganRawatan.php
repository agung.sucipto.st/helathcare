<?php

/**
 * This is the model class for table "ruangan_rawatan".
 *
 * The followings are the available columns in table 'ruangan_rawatan':
 * @property string $id_ruangan_rawatan
 * @property string $id_lokasi_ruangan
 * @property string $id_kelas
 * @property string $kode_ruangan
 * @property string $nama_ruangan
 * @property string $kapasitas_ruangan
 * @property string $status
 *
 * The followings are the available model relations:
 * @property Bed[] $beds
 * @property Kelas $idKelas
 * @property LokasiRuangan $idLokasiRuangan
 * @property TarifRuangRawatan[] $tarifRuangRawatans
 */
class RuanganRawatan extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'ruangan_rawatan';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('id_lokasi_ruangan, id_kelas, kode_ruangan, nama_ruangan, kapasitas_ruangan, status', 'required'),
			array('id_lokasi_ruangan, id_kelas, kapasitas_ruangan', 'length', 'max'=>20),
			array('kode_ruangan, status', 'length', 'max'=>45),
			array('nama_ruangan', 'length', 'max'=>100),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id_ruangan_rawatan, id_lokasi_ruangan, id_kelas, kode_ruangan, nama_ruangan, kapasitas_ruangan, status', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'beds' => array(self::HAS_MANY, 'Bed', 'id_ruangan'),
			'idKelas' => array(self::BELONGS_TO, 'Kelas', 'id_kelas'),
			'idLokasiRuangan' => array(self::BELONGS_TO, 'LokasiRuangan', 'id_lokasi_ruangan'),
			'tarifRuangRawatans' => array(self::HAS_MANY, 'TarifRuangRawatan', 'id_ruangan_rawatan'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id_ruangan_rawatan' => 'Id Ruangan Rawatan',
			'id_lokasi_ruangan' => 'Id Lokasi Ruangan',
			'id_kelas' => 'Id Kelas',
			'kode_ruangan' => 'Kode Ruangan',
			'nama_ruangan' => 'Nama Ruangan',
			'kapasitas_ruangan' => 'Kapasitas Ruangan',
			'status' => 'Status',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id_ruangan_rawatan',$this->id_ruangan_rawatan,true);
		$criteria->compare('id_lokasi_ruangan',$this->id_lokasi_ruangan,true);
		$criteria->compare('id_kelas',$this->id_kelas,true);
		$criteria->compare('kode_ruangan',$this->kode_ruangan,true);
		$criteria->compare('nama_ruangan',$this->nama_ruangan,true);
		$criteria->compare('kapasitas_ruangan',$this->kapasitas_ruangan,true);
		$criteria->compare('status',$this->status,true);

		return new CActiveDataProvider($this, array(
			'pagination'=>array(
				'pageSize'=> Yii::app()->user->getState('pageSize',Yii::app()->params['defaultPageSize']),
			),
			'criteria'=>$criteria,
		));
	}
	
	public function getAllowView()
	{
		$akses = UserAccess::model()->find(array("with"=>array("access"),'condition'=>"id_user='".Yii::app()->user->id."' and access.access_controller='RuanganRawatan' and access.access_action='view'"));
		if(!empty($akses)) return true; else return false;
	}
	
	public function getAllowUpdate()
	{
		$akses = UserAccess::model()->find(array("with"=>array("access"),'condition'=>"id_user='".Yii::app()->user->id."' and access.access_controller='RuanganRawatan' and access.access_action='update'"));
		if(!empty($akses)) return true; else return false;
	}
	
	public function getAllowDelete($id)
	{
		$akses = UserAccess::model()->find(array("with"=>array("access"),'condition'=>"id_user='".Yii::app()->user->id."' and access.access_controller='RuanganRawatan' and access.access_action='delete'"));
		$rel=$this->getEmptyRelation($id);
		if(!empty($akses) AND $rel==true){
			return true;
		}else{
			return false;
		}
	}
	
	
	public function getEmptyRelation($id)
	{
		$count=0;
		$count+=Bed::model()->count(array('condition'=>"id_ruangan='$id'"));
						$count+=TarifRuangRawatan::model()->count(array('condition'=>"id_ruangan_rawatan='$id'"));
		if($count>0){
			return false;
		}else{
			return true;
		}
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return RuanganRawatan the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
