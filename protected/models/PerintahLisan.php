<?php

/**
 * This is the model class for table "perintah_lisan".
 *
 * The followings are the available columns in table 'perintah_lisan':
 * @property string $id_perintah_lisan
 * @property string $id_registrasi
 * @property string $isi_perintah
 * @property string $penerima_perintah
 * @property string $pemberi_perintah
 * @property string $pelaksana_perintah
 * @property string $user_create
 * @property string $user_update
 * @property string $tgl_create
 * @property string $tgl_update
 */
class PerintahLisan extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'perintah_lisan';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('id_perintah_lisan', 'required'),
			array('id_perintah_lisan, id_registrasi, user_create, user_update', 'length', 'max'=>20),
			array('penerima_perintah, pemberi_perintah, pelaksana_perintah', 'length', 'max'=>35),
			array('isi_perintah, tgl_create, tgl_update', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id_perintah_lisan, id_registrasi, isi_perintah, penerima_perintah, pemberi_perintah, pelaksana_perintah, user_create, user_update, tgl_create, tgl_update', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id_perintah_lisan' => 'Id Perintah Lisan',
			'id_registrasi' => 'Id Registrasi',
			'isi_perintah' => 'Isi Perintah',
			'penerima_perintah' => 'Penerima Perintah',
			'pemberi_perintah' => 'Pemberi Perintah',
			'pelaksana_perintah' => 'Pelaksana Perintah',
			'user_create' => 'User Create',
			'user_update' => 'User Update',
			'tgl_create' => 'Tgl Create',
			'tgl_update' => 'Tgl Update',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id_perintah_lisan',$this->id_perintah_lisan,true);
		$criteria->compare('id_registrasi',$this->id_registrasi,true);
		$criteria->compare('isi_perintah',$this->isi_perintah,true);
		$criteria->compare('penerima_perintah',$this->penerima_perintah,true);
		$criteria->compare('pemberi_perintah',$this->pemberi_perintah,true);
		$criteria->compare('pelaksana_perintah',$this->pelaksana_perintah,true);
		$criteria->compare('user_create',$this->user_create,true);
		$criteria->compare('user_update',$this->user_update,true);
		$criteria->compare('tgl_create',$this->tgl_create,true);
		$criteria->compare('tgl_update',$this->tgl_update,true);

		return new CActiveDataProvider($this, array(
			'pagination'=>array(
				'pageSize'=> Yii::app()->user->getState('pageSize',Yii::app()->params['defaultPageSize']),
			),
			'criteria'=>$criteria,
		));
	}
	
	public function getAllowView()
	{
		$akses = UserAccess::model()->find(array("with"=>array("access"),'condition'=>"id_user='".Yii::app()->user->id."' and access.access_controller='PerintahLisan' and access.access_action='view'"));
		if(!empty($akses)) return true; else return false;
	}
	
	public function getAllowUpdate()
	{
		$akses = UserAccess::model()->find(array("with"=>array("access"),'condition'=>"id_user='".Yii::app()->user->id."' and access.access_controller='PerintahLisan' and access.access_action='update'"));
		if(!empty($akses)) return true; else return false;
	}
	
	public function getAllowDelete($id)
	{
		$akses = UserAccess::model()->find(array("with"=>array("access"),'condition'=>"id_user='".Yii::app()->user->id."' and access.access_controller='PerintahLisan' and access.access_action='delete'"));
		$rel=$this->getEmptyRelation($id);
		if(!empty($akses) AND $rel==true){
			return true;
		}else{
			return false;
		}
	}
	
	
	public function getEmptyRelation($id)
	{
		$count=0;
		if($count>0){
			return false;
		}else{
			return true;
		}
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return PerintahLisan the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
