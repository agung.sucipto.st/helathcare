<?php

/**
 * This is the model class for table "penolakan_tindakan".
 *
 * The followings are the available columns in table 'penolakan_tindakan':
 * @property string $id_penolakan_tindakan
 * @property string $id_registrasi
 * @property string $nama
 * @property string $umur
 * @property string $jenis_kelamin
 * @property string $alamat
 * @property string $no_ktp
 * @property string $tindakan_medik
 * @property string $hubungan_keluarga
 * @property string $risiko
 * @property string $tgl_ttd
 * @property string $user_create
 * @property string $user_update
 * @property string $tgl_create
 * @property string $tgl_update
 */
class PenolakanTindakan extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'penolakan_tindakan';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('id_penolakan_tindakan', 'required'),
			array('id_penolakan_tindakan, id_registrasi, user_create, user_update', 'length', 'max'=>20),
			array('nama', 'length', 'max'=>35),
			array('umur, alamat, tindakan_medik, risiko', 'length', 'max'=>100),
			array('jenis_kelamin', 'length', 'max'=>11),
			array('no_ktp', 'length', 'max'=>17),
			array('hubungan_keluarga', 'length', 'max'=>7),
			array('tgl_ttd, tgl_create, tgl_update', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id_penolakan_tindakan, id_registrasi, nama, umur, jenis_kelamin, alamat, no_ktp, tindakan_medik, hubungan_keluarga, risiko, tgl_ttd, user_create, user_update, tgl_create, tgl_update', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id_penolakan_tindakan' => 'Id Penolakan Tindakan',
			'id_registrasi' => 'Id Registrasi',
			'nama' => 'Nama',
			'umur' => 'Umur',
			'jenis_kelamin' => 'Jenis Kelamin',
			'alamat' => 'Alamat',
			'no_ktp' => 'No Ktp',
			'tindakan_medik' => 'Tindakan Medik',
			'hubungan_keluarga' => 'Hubungan Keluarga',
			'risiko' => 'Risiko',
			'tgl_ttd' => 'Tgl Ttd',
			'user_create' => 'User Create',
			'user_update' => 'User Update',
			'tgl_create' => 'Tgl Create',
			'tgl_update' => 'Tgl Update',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id_penolakan_tindakan',$this->id_penolakan_tindakan,true);
		$criteria->compare('id_registrasi',$this->id_registrasi,true);
		$criteria->compare('nama',$this->nama,true);
		$criteria->compare('umur',$this->umur,true);
		$criteria->compare('jenis_kelamin',$this->jenis_kelamin,true);
		$criteria->compare('alamat',$this->alamat,true);
		$criteria->compare('no_ktp',$this->no_ktp,true);
		$criteria->compare('tindakan_medik',$this->tindakan_medik,true);
		$criteria->compare('hubungan_keluarga',$this->hubungan_keluarga,true);
		$criteria->compare('risiko',$this->risiko,true);
		$criteria->compare('tgl_ttd',$this->tgl_ttd,true);
		$criteria->compare('user_create',$this->user_create,true);
		$criteria->compare('user_update',$this->user_update,true);
		$criteria->compare('tgl_create',$this->tgl_create,true);
		$criteria->compare('tgl_update',$this->tgl_update,true);

		return new CActiveDataProvider($this, array(
			'pagination'=>array(
				'pageSize'=> Yii::app()->user->getState('pageSize',Yii::app()->params['defaultPageSize']),
			),
			'criteria'=>$criteria,
		));
	}
	
	public function getAllowView()
	{
		$akses = UserAccess::model()->find(array("with"=>array("access"),'condition'=>"id_user='".Yii::app()->user->id."' and access.access_controller='PenolakanTindakan' and access.access_action='view'"));
		if(!empty($akses)) return true; else return false;
	}
	
	public function getAllowUpdate()
	{
		$akses = UserAccess::model()->find(array("with"=>array("access"),'condition'=>"id_user='".Yii::app()->user->id."' and access.access_controller='PenolakanTindakan' and access.access_action='update'"));
		if(!empty($akses)) return true; else return false;
	}
	
	public function getAllowDelete($id)
	{
		$akses = UserAccess::model()->find(array("with"=>array("access"),'condition'=>"id_user='".Yii::app()->user->id."' and access.access_controller='PenolakanTindakan' and access.access_action='delete'"));
		$rel=$this->getEmptyRelation($id);
		if(!empty($akses) AND $rel==true){
			return true;
		}else{
			return false;
		}
	}
	
	
	public function getEmptyRelation($id)
	{
		$count=0;
		if($count>0){
			return false;
		}else{
			return true;
		}
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return PenolakanTindakan the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
