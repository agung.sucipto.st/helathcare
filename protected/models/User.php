<?php

/**
 * This is the model class for table "user".
 *
 * The followings are the available columns in table 'user':
 * @property string $id_user
 * @property string $id_pegawai
 * @property string $username
 * @property string $password
 * @property string $last_login
 * @property string $status
 * @property string $time_create
 * @property string $time_update
 *
 * The followings are the available model relations:
 * @property BedPasien[] $bedPasiens
 * @property BedPasien[] $bedPasiens1
 * @property BedPasien[] $bedPasiens2
 * @property DaftarJasaMedis[] $daftarJasaMedises
 * @property DaftarJasaMedis[] $daftarJasaMedises1
 * @property DaftarJasaMedis[] $daftarJasaMedises2
 * @property DaftarTagihan[] $daftarTagihans
 * @property DaftarTagihan[] $daftarTagihans1
 * @property DaftarTagihan[] $daftarTagihans2
 * @property DaftarTagihan[] $daftarTagihans3
 * @property EkspedisiRekamMedis[] $ekspedisiRekamMedises
 * @property HistoryEditJaminan[] $historyEditJaminans
 * @property Icd10Pasien[] $icd10Pasiens
 * @property Icd10Pasien[] $icd10Pasiens1
 * @property ItemPasien[] $itemPasiens
 * @property ItemPasien[] $itemPasiens1
 * @property ItemPasien[] $itemPasiens2
 * @property ItemPasienGroup[] $itemPasienGroups
 * @property ItemPasienGroup[] $itemPasienGroups1
 * @property ItemPasienGroup[] $itemPasienGroups2
 * @property ItemTransaksi[] $itemTransaksis
 * @property ItemTransaksi[] $itemTransaksis1
 * @property JaminanPasien[] $jaminanPasiens
 * @property JaminanPasien[] $jaminanPasiens1
 * @property JaminanPasien[] $jaminanPasiens2
 * @property JasaMedis[] $jasaMedises
 * @property JasaMedis[] $jasaMedises1
 * @property JasaMedis[] $jasaMedises2
 * @property LaboratoriumPasien[] $laboratoriumPasiens
 * @property LaboratoriumPasien[] $laboratoriumPasiens1
 * @property LaboratoriumPasien[] $laboratoriumPasiens2
 * @property LaboratoriumPasien[] $laboratoriumPasiens3
 * @property Pasien[] $pasiens
 * @property Pasien[] $pasiens1
 * @property PembayaranJasaMedis[] $pembayaranJasaMedises
 * @property PembayaranJasaMedis[] $pembayaranJasaMedises1
 * @property PembayaranJasaMedis[] $pembayaranJasaMedises2
 * @property PembayaranPiutangPerusahaan[] $pembayaranPiutangPerusahaans
 * @property PembayaranPiutangPerusahaan[] $pembayaranPiutangPerusahaans1
 * @property PembayaranPiutangPerusahaan[] $pembayaranPiutangPerusahaans2
 * @property PembayaranTagihan[] $pembayaranTagihans
 * @property PembayaranTagihan[] $pembayaranTagihans1
 * @property PembayaranTagihan[] $pembayaranTagihans2
 * @property PeralatanPasien[] $peralatanPasiens
 * @property PeralatanPasien[] $peralatanPasiens1
 * @property PeralatanPasien[] $peralatanPasiens2
 * @property Personal[] $personals
 * @property Personal[] $personals1
 * @property PiutangPerusahaan[] $piutangPerusahaans
 * @property PiutangPerusahaan[] $piutangPerusahaans1
 * @property PiutangPerusahaan[] $piutangPerusahaans2
 * @property RadiologyPasien[] $radiologyPasiens
 * @property RadiologyPasien[] $radiologyPasiens1
 * @property RadiologyPasien[] $radiologyPasiens2
 * @property RadiologyPasien[] $radiologyPasiens3
 * @property Registrasi[] $registrasis
 * @property Registrasi[] $registrasis1
 * @property Registrasi[] $registrasis2
 * @property Registrasi[] $registrasis3
 * @property ResepPasien[] $resepPasiens
 * @property ResepPasien[] $resepPasiens1
 * @property ResepPasien[] $resepPasiens2
 * @property ResepPasienList[] $resepPasienLists
 * @property ResponsTime[] $responsTimes
 * @property ResponsTime[] $responsTimes1
 * @property RuanganPasien[] $ruanganPasiens
 * @property RuanganPasien[] $ruanganPasiens1
 * @property RuanganPasien[] $ruanganPasiens2
 * @property SoapPasien[] $soapPasiens
 * @property SoapPasien[] $soapPasiens1
 * @property SoapPasien[] $soapPasiens2
 * @property TagihanPasien[] $tagihanPasiens
 * @property TagihanPasien[] $tagihanPasiens1
 * @property TagihanPasien[] $tagihanPasiens2
 * @property TandaVitalPasien[] $tandaVitalPasiens
 * @property TandaVitalPasien[] $tandaVitalPasiens1
 * @property TindakanPasien[] $tindakanPasiens
 * @property TindakanPasien[] $tindakanPasiens1
 * @property TindakanPasien[] $tindakanPasiens2
 * @property Pegawai $idPegawai
 * @property UserAccess[] $userAccesses
 * @property UserGudang[] $userGudangs
 * @property VisiteDokterPasien[] $visiteDokterPasiens
 * @property VisiteDokterPasien[] $visiteDokterPasiens1
 * @property VisiteDokterPasien[] $visiteDokterPasiens2
 */
class User extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'user';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('id_pegawai', 'length', 'max'=>20),
			array('username, status', 'length', 'max'=>45),
			array('password', 'length', 'max'=>32),
			array('last_login, time_create, time_update', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id_user, id_pegawai, username, password, last_login, status, time_create, time_update', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'bedPasiens' => array(self::HAS_MANY, 'BedPasien', 'user_create'),
			'bedPasiens1' => array(self::HAS_MANY, 'BedPasien', 'user_update'),
			'bedPasiens2' => array(self::HAS_MANY, 'BedPasien', 'user_approve'),
			'daftarJasaMedises' => array(self::HAS_MANY, 'DaftarJasaMedis', 'user_create'),
			'daftarJasaMedises1' => array(self::HAS_MANY, 'DaftarJasaMedis', 'user_update'),
			'daftarJasaMedises2' => array(self::HAS_MANY, 'DaftarJasaMedis', 'user_final'),
			'daftarTagihans' => array(self::HAS_MANY, 'DaftarTagihan', 'user_update'),
			'daftarTagihans1' => array(self::HAS_MANY, 'DaftarTagihan', 'user_final'),
			'daftarTagihans2' => array(self::HAS_MANY, 'DaftarTagihan', 'user_create'),
			'daftarTagihans3' => array(self::HAS_MANY, 'DaftarTagihan', 'user_confirm'),
			'ekspedisiRekamMedises' => array(self::HAS_MANY, 'EkspedisiRekamMedis', 'user_create'),
			'historyEditJaminans' => array(self::HAS_MANY, 'HistoryEditJaminan', 'user_create'),
			'icd10Pasiens' => array(self::HAS_MANY, 'Icd10Pasien', 'user_create'),
			'icd10Pasiens1' => array(self::HAS_MANY, 'Icd10Pasien', 'user_update'),
			'itemPasiens' => array(self::HAS_MANY, 'ItemPasien', 'user_create'),
			'itemPasiens1' => array(self::HAS_MANY, 'ItemPasien', 'user_update'),
			'itemPasiens2' => array(self::HAS_MANY, 'ItemPasien', 'user_final'),
			'itemPasienGroups' => array(self::HAS_MANY, 'ItemPasienGroup', 'user_create'),
			'itemPasienGroups1' => array(self::HAS_MANY, 'ItemPasienGroup', 'user_update'),
			'itemPasienGroups2' => array(self::HAS_MANY, 'ItemPasienGroup', 'user_final'),
			'itemTransaksis' => array(self::HAS_MANY, 'ItemTransaksi', 'user_create'),
			'itemTransaksis1' => array(self::HAS_MANY, 'ItemTransaksi', 'user_update'),
			'jaminanPasiens' => array(self::HAS_MANY, 'JaminanPasien', 'user_create'),
			'jaminanPasiens1' => array(self::HAS_MANY, 'JaminanPasien', 'user_update'),
			'jaminanPasiens2' => array(self::HAS_MANY, 'JaminanPasien', 'user_final'),
			'jasaMedises' => array(self::HAS_MANY, 'JasaMedis', 'user_create'),
			'jasaMedises1' => array(self::HAS_MANY, 'JasaMedis', 'user_update'),
			'jasaMedises2' => array(self::HAS_MANY, 'JasaMedis', 'user_final'),
			'laboratoriumPasiens' => array(self::HAS_MANY, 'LaboratoriumPasien', 'user_create'),
			'laboratoriumPasiens1' => array(self::HAS_MANY, 'LaboratoriumPasien', 'user_update'),
			'laboratoriumPasiens2' => array(self::HAS_MANY, 'LaboratoriumPasien', 'user_approve'),
			'laboratoriumPasiens3' => array(self::HAS_MANY, 'LaboratoriumPasien', 'user_final'),
			'pasiens' => array(self::HAS_MANY, 'Pasien', 'user_create'),
			'pasiens1' => array(self::HAS_MANY, 'Pasien', 'user_update'),
			'pembayaranJasaMedises' => array(self::HAS_MANY, 'PembayaranJasaMedis', 'user_create'),
			'pembayaranJasaMedises1' => array(self::HAS_MANY, 'PembayaranJasaMedis', 'user_update'),
			'pembayaranJasaMedises2' => array(self::HAS_MANY, 'PembayaranJasaMedis', 'user_final'),
			'pembayaranPiutangPerusahaans' => array(self::HAS_MANY, 'PembayaranPiutangPerusahaan', 'user_create'),
			'pembayaranPiutangPerusahaans1' => array(self::HAS_MANY, 'PembayaranPiutangPerusahaan', 'user_update'),
			'pembayaranPiutangPerusahaans2' => array(self::HAS_MANY, 'PembayaranPiutangPerusahaan', 'user_final'),
			'pembayaranTagihans' => array(self::HAS_MANY, 'PembayaranTagihan', 'user_create'),
			'pembayaranTagihans1' => array(self::HAS_MANY, 'PembayaranTagihan', 'user_update'),
			'pembayaranTagihans2' => array(self::HAS_MANY, 'PembayaranTagihan', 'user_final'),
			'peralatanPasiens' => array(self::HAS_MANY, 'PeralatanPasien', 'user_create'),
			'peralatanPasiens1' => array(self::HAS_MANY, 'PeralatanPasien', 'user_update'),
			'peralatanPasiens2' => array(self::HAS_MANY, 'PeralatanPasien', 'user_final'),
			'personals' => array(self::HAS_MANY, 'Personal', 'user_update'),
			'personals1' => array(self::HAS_MANY, 'Personal', 'user_create'),
			'piutangPerusahaans' => array(self::HAS_MANY, 'PiutangPerusahaan', 'user_create'),
			'piutangPerusahaans1' => array(self::HAS_MANY, 'PiutangPerusahaan', 'user_update'),
			'piutangPerusahaans2' => array(self::HAS_MANY, 'PiutangPerusahaan', 'user_final'),
			'radiologyPasiens' => array(self::HAS_MANY, 'RadiologyPasien', 'user_create'),
			'radiologyPasiens1' => array(self::HAS_MANY, 'RadiologyPasien', 'user_update'),
			'radiologyPasiens2' => array(self::HAS_MANY, 'RadiologyPasien', 'user_final'),
			'radiologyPasiens3' => array(self::HAS_MANY, 'RadiologyPasien', 'user_approve'),
			'registrasis' => array(self::HAS_MANY, 'Registrasi', 'user_create'),
			'registrasis1' => array(self::HAS_MANY, 'Registrasi', 'user_update'),
			'registrasis2' => array(self::HAS_MANY, 'Registrasi', 'user_final'),
			'registrasis3' => array(self::HAS_MANY, 'Registrasi', 'user_cancel'),
			'resepPasiens' => array(self::HAS_MANY, 'ResepPasien', 'user_create'),
			'resepPasiens1' => array(self::HAS_MANY, 'ResepPasien', 'user_update'),
			'resepPasiens2' => array(self::HAS_MANY, 'ResepPasien', 'user_final'),
			'resepPasienLists' => array(self::HAS_MANY, 'ResepPasienList', 'user_approve'),
			'responsTimes' => array(self::HAS_MANY, 'ResponsTime', 'user_create'),
			'responsTimes1' => array(self::HAS_MANY, 'ResponsTime', 'user_update'),
			'ruanganPasiens' => array(self::HAS_MANY, 'RuanganPasien', 'user_create'),
			'ruanganPasiens1' => array(self::HAS_MANY, 'RuanganPasien', 'user_update'),
			'ruanganPasiens2' => array(self::HAS_MANY, 'RuanganPasien', 'user_approve'),
			'soapPasiens' => array(self::HAS_MANY, 'SoapPasien', 'user_create'),
			'soapPasiens1' => array(self::HAS_MANY, 'SoapPasien', 'user_update'),
			'soapPasiens2' => array(self::HAS_MANY, 'SoapPasien', 'user_final'),
			'tagihanPasiens' => array(self::HAS_MANY, 'TagihanPasien', 'user_create'),
			'tagihanPasiens1' => array(self::HAS_MANY, 'TagihanPasien', 'user_update'),
			'tagihanPasiens2' => array(self::HAS_MANY, 'TagihanPasien', 'user_final'),
			'tandaVitalPasiens' => array(self::HAS_MANY, 'TandaVitalPasien', 'user_create'),
			'tandaVitalPasiens1' => array(self::HAS_MANY, 'TandaVitalPasien', 'user_update'),
			'tindakanPasiens' => array(self::HAS_MANY, 'TindakanPasien', 'user_create'),
			'tindakanPasiens1' => array(self::HAS_MANY, 'TindakanPasien', 'user_update'),
			'tindakanPasiens2' => array(self::HAS_MANY, 'TindakanPasien', 'user_final'),
			'idPegawai' => array(self::BELONGS_TO, 'Pegawai', 'id_pegawai'),
			'userAccesses' => array(self::HAS_MANY, 'UserAccess', 'id_user'),
			'userGudangs' => array(self::HAS_MANY, 'UserGudang', 'id_user'),
			'visiteDokterPasiens' => array(self::HAS_MANY, 'VisiteDokterPasien', 'user_create'),
			'visiteDokterPasiens1' => array(self::HAS_MANY, 'VisiteDokterPasien', 'user_update'),
			'visiteDokterPasiens2' => array(self::HAS_MANY, 'VisiteDokterPasien', 'user_final'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id_user' => 'Id User',
			'id_pegawai' => 'Id Pegawai',
			'username' => 'Username',
			'password' => 'Password',
			'last_login' => 'Last Login',
			'status' => 'Status',
			'time_create' => 'Time Create',
			'time_update' => 'Time Update',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id_user',$this->id_user,true);
		$criteria->compare('id_pegawai',$this->id_pegawai,true);
		$criteria->compare('username',$this->username,true);
		$criteria->compare('password',$this->password,true);
		$criteria->compare('last_login',$this->last_login,true);
		$criteria->compare('status',$this->status,true);
		$criteria->compare('time_create',$this->time_create,true);
		$criteria->compare('time_update',$this->time_update,true);

		return new CActiveDataProvider($this, array(
			'pagination'=>array(
				'pageSize'=> Yii::app()->user->getState('pageSize',Yii::app()->params['defaultPageSize']),
			),
			'criteria'=>$criteria,
		));
	}
	
	public function getAllowView()
	{
		$akses = UserAccess::model()->find(array("with"=>array("access"),'condition'=>"id_user='".Yii::app()->user->id."' and access.access_controller='User' and access.access_action='view'"));
		if(!empty($akses)) return true; else return false;
	}
	
	public function getAllowUpdate()
	{
		$akses = UserAccess::model()->find(array("with"=>array("access"),'condition'=>"id_user='".Yii::app()->user->id."' and access.access_controller='User' and access.access_action='update'"));
		if(!empty($akses)) return true; else return false;
	}
	
	public function getAllowDelete($id)
	{
		$akses = UserAccess::model()->find(array("with"=>array("access"),'condition'=>"id_user='".Yii::app()->user->id."' and access.access_controller='User' and access.access_action='delete'"));
		$rel=$this->getEmptyRelation($id);
		if(!empty($akses) AND $rel==true){
			return true;
		}else{
			return false;
		}
	}
	
	
	public function getEmptyRelation($id)
	{
		$count=0;
		$count+=BedPasien::model()->count(array('condition'=>"user_create='$id'"));
		$count+=BedPasien::model()->count(array('condition'=>"user_update='$id'"));
		$count+=BedPasien::model()->count(array('condition'=>"user_approve='$id'"));
		$count+=DaftarJasaMedis::model()->count(array('condition'=>"user_create='$id'"));
		$count+=DaftarJasaMedis::model()->count(array('condition'=>"user_update='$id'"));
		$count+=DaftarJasaMedis::model()->count(array('condition'=>"user_final='$id'"));
		$count+=DaftarTagihan::model()->count(array('condition'=>"user_update='$id'"));
		$count+=DaftarTagihan::model()->count(array('condition'=>"user_final='$id'"));
		$count+=DaftarTagihan::model()->count(array('condition'=>"user_create='$id'"));
		$count+=DaftarTagihan::model()->count(array('condition'=>"user_confirm='$id'"));
		$count+=EkspedisiRekamMedis::model()->count(array('condition'=>"user_create='$id'"));
		$count+=HistoryEditJaminan::model()->count(array('condition'=>"user_create='$id'"));
		$count+=Icd10Pasien::model()->count(array('condition'=>"user_create='$id'"));
		$count+=Icd10Pasien::model()->count(array('condition'=>"user_update='$id'"));
		$count+=ItemTransaksi::model()->count(array('condition'=>"user_create='$id'"));
		$count+=ItemTransaksi::model()->count(array('condition'=>"user_update='$id'"));
		$count+=JaminanPasien::model()->count(array('condition'=>"user_create='$id'"));
		$count+=JaminanPasien::model()->count(array('condition'=>"user_update='$id'"));
		$count+=JaminanPasien::model()->count(array('condition'=>"user_final='$id'"));
		$count+=JasaMedis::model()->count(array('condition'=>"user_create='$id'"));
		$count+=JasaMedis::model()->count(array('condition'=>"user_update='$id'"));
		$count+=JasaMedis::model()->count(array('condition'=>"user_final='$id'"));
		$count+=LaboratoriumPasien::model()->count(array('condition'=>"user_create='$id'"));
		$count+=LaboratoriumPasien::model()->count(array('condition'=>"user_update='$id'"));
		$count+=LaboratoriumPasien::model()->count(array('condition'=>"user_approve='$id'"));
		$count+=LaboratoriumPasien::model()->count(array('condition'=>"user_final='$id'"));
		$count+=Pasien::model()->count(array('condition'=>"user_create='$id'"));
		$count+=Pasien::model()->count(array('condition'=>"user_update='$id'"));
		$count+=PembayaranJasaMedis::model()->count(array('condition'=>"user_create='$id'"));
		$count+=PembayaranJasaMedis::model()->count(array('condition'=>"user_update='$id'"));
		$count+=PembayaranJasaMedis::model()->count(array('condition'=>"user_final='$id'"));
		$count+=PembayaranPiutangPerusahaan::model()->count(array('condition'=>"user_create='$id'"));
		$count+=PembayaranPiutangPerusahaan::model()->count(array('condition'=>"user_update='$id'"));
		$count+=PembayaranPiutangPerusahaan::model()->count(array('condition'=>"user_final='$id'"));
		$count+=PembayaranTagihan::model()->count(array('condition'=>"user_create='$id'"));
		$count+=PembayaranTagihan::model()->count(array('condition'=>"user_update='$id'"));
		$count+=PembayaranTagihan::model()->count(array('condition'=>"user_final='$id'"));
		$count+=PeralatanPasien::model()->count(array('condition'=>"user_create='$id'"));
		$count+=PeralatanPasien::model()->count(array('condition'=>"user_update='$id'"));
		$count+=PeralatanPasien::model()->count(array('condition'=>"user_final='$id'"));
		$count+=Personal::model()->count(array('condition'=>"user_update='$id'"));
		$count+=Personal::model()->count(array('condition'=>"user_create='$id'"));
		$count+=PiutangPerusahaan::model()->count(array('condition'=>"user_create='$id'"));
		$count+=PiutangPerusahaan::model()->count(array('condition'=>"user_update='$id'"));
		$count+=PiutangPerusahaan::model()->count(array('condition'=>"user_final='$id'"));
		$count+=RadiologyPasien::model()->count(array('condition'=>"user_create='$id'"));
		$count+=RadiologyPasien::model()->count(array('condition'=>"user_update='$id'"));
		$count+=RadiologyPasien::model()->count(array('condition'=>"user_final='$id'"));
		$count+=RadiologyPasien::model()->count(array('condition'=>"user_approve='$id'"));
		$count+=Registrasi::model()->count(array('condition'=>"user_create='$id'"));
		$count+=Registrasi::model()->count(array('condition'=>"user_update='$id'"));
		$count+=Registrasi::model()->count(array('condition'=>"user_final='$id'"));
		$count+=Registrasi::model()->count(array('condition'=>"user_cancel='$id'"));
		$count+=ResepPasien::model()->count(array('condition'=>"user_create='$id'"));
		$count+=ResepPasien::model()->count(array('condition'=>"user_update='$id'"));
		$count+=ResepPasien::model()->count(array('condition'=>"user_final='$id'"));
		$count+=ResepPasienList::model()->count(array('condition'=>"user_approve='$id'"));
		$count+=ResponsTime::model()->count(array('condition'=>"user_create='$id'"));
		$count+=ResponsTime::model()->count(array('condition'=>"user_update='$id'"));
		$count+=RuanganPasien::model()->count(array('condition'=>"user_create='$id'"));
		$count+=RuanganPasien::model()->count(array('condition'=>"user_update='$id'"));
		$count+=RuanganPasien::model()->count(array('condition'=>"user_approve='$id'"));
		$count+=SoapPasien::model()->count(array('condition'=>"user_create='$id'"));
		$count+=SoapPasien::model()->count(array('condition'=>"user_update='$id'"));
		$count+=SoapPasien::model()->count(array('condition'=>"user_final='$id'"));
		$count+=TagihanPasien::model()->count(array('condition'=>"user_create='$id'"));
		$count+=TagihanPasien::model()->count(array('condition'=>"user_update='$id'"));
		$count+=TagihanPasien::model()->count(array('condition'=>"user_final='$id'"));
		$count+=TandaVitalPasien::model()->count(array('condition'=>"user_create='$id'"));
		$count+=TandaVitalPasien::model()->count(array('condition'=>"user_update='$id'"));
		$count+=TindakanPasien::model()->count(array('condition'=>"user_create='$id'"));
		$count+=TindakanPasien::model()->count(array('condition'=>"user_update='$id'"));
		$count+=TindakanPasien::model()->count(array('condition'=>"user_final='$id'"));
				$count+=UserAccess::model()->count(array('condition'=>"id_user='$id'"));
		$count+=UserGudang::model()->count(array('condition'=>"id_user='$id'"));
		$count+=VisiteDokterPasien::model()->count(array('condition'=>"user_create='$id'"));
		$count+=VisiteDokterPasien::model()->count(array('condition'=>"user_update='$id'"));
		$count+=VisiteDokterPasien::model()->count(array('condition'=>"user_final='$id'"));
		if($count>0){
			return false;
		}else{
			return true;
		}
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return User the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
