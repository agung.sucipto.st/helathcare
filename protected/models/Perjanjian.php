<?php

/**
 * This is the model class for table "perjanjian".
 *
 * The followings are the available columns in table 'perjanjian':
 * @property integer $id_perjanjian
 * @property string $id_pasien
 * @property string $nama_lengkap
 * @property string $jenis_kelamin
 * @property string $tanggal_lahir
 * @property string $tempat_lahir
 * @property string $no_telpon
 * @property string $alamat
 * @property string $id_departemen
 * @property string $id_dokter
 * @property string $waktu_perjanjian
 * @property string $jenis_perjanjian
 * @property string $waktu_daftar
 * @property string $jenis_pelayanan
 * @property string $status_perjanjian
 * @property string $user_create
 * @property string $user_update
 * @property string $user_cancel
 * @property string $time_create
 * @property string $time_update
 * @property string $time_cancel
 *
 * The followings are the available model relations:
 * @property Departemen $idDepartemen
 * @property Pasien $idPasien
 * @property Pegawai $idDokter
 * @property User $userCreate
 * @property User $userUpdate
 * @property User $userCancel
 */
class Perjanjian extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'perjanjian';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('id_pasien, id_departemen, id_dokter, user_create, user_update, user_cancel', 'required'),
			array('id_pasien, id_departemen, id_dokter, user_create, user_update, user_cancel', 'length', 'max'=>20),
			array('nama_lengkap, tempat_lahir, alamat', 'length', 'max'=>100),
			array('jenis_kelamin', 'length', 'max'=>9),
			array('no_telpon', 'length', 'max'=>13),
			array('jenis_perjanjian', 'length', 'max'=>4),
			array('jenis_pelayanan', 'length', 'max'=>15),
			array('status_perjanjian', 'length', 'max'=>12),
			array('tanggal_lahir, waktu_perjanjian, waktu_daftar, time_create, time_update, time_cancel', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id_perjanjian, id_pasien, nama_lengkap, jenis_kelamin, tanggal_lahir, tempat_lahir, no_telpon, alamat, id_departemen, id_dokter, waktu_perjanjian, jenis_perjanjian, waktu_daftar, jenis_pelayanan, status_perjanjian, user_create, user_update, user_cancel, time_create, time_update, time_cancel', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'idDepartemen' => array(self::BELONGS_TO, 'Departemen', 'id_departemen'),
			'idPasien' => array(self::BELONGS_TO, 'Pasien', 'id_pasien'),
			'idDokter' => array(self::BELONGS_TO, 'Pegawai', 'id_dokter'),
			'userCreate' => array(self::BELONGS_TO, 'User', 'user_create'),
			'userUpdate' => array(self::BELONGS_TO, 'User', 'user_update'),
			'userCancel' => array(self::BELONGS_TO, 'User', 'user_cancel'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id_perjanjian' => 'Id Perjanjian',
			'id_pasien' => 'Id Pasien',
			'nama_lengkap' => 'Nama Lengkap',
			'jenis_kelamin' => 'Jenis Kelamin',
			'tanggal_lahir' => 'Tanggal Lahir',
			'tempat_lahir' => 'Tempat Lahir',
			'no_telpon' => 'No Telpon',
			'alamat' => 'Alamat',
			'id_departemen' => 'Id Departemen',
			'id_dokter' => 'Id Dokter',
			'waktu_perjanjian' => 'Waktu Perjanjian',
			'jenis_perjanjian' => 'Jenis Perjanjian',
			'waktu_daftar' => 'Waktu Daftar',
			'jenis_pelayanan' => 'Jenis Pelayanan',
			'status_perjanjian' => 'Status Perjanjian',
			'user_create' => 'User Create',
			'user_update' => 'User Update',
			'user_cancel' => 'User Cancel',
			'time_create' => 'Time Create',
			'time_update' => 'Time Update',
			'time_cancel' => 'Time Cancel',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id_perjanjian',$this->id_perjanjian);
		$criteria->compare('id_pasien',$this->id_pasien,true);
		$criteria->compare('nama_lengkap',$this->nama_lengkap,true);
		$criteria->compare('jenis_kelamin',$this->jenis_kelamin,true);
		$criteria->compare('tanggal_lahir',$this->tanggal_lahir,true);
		$criteria->compare('tempat_lahir',$this->tempat_lahir,true);
		$criteria->compare('no_telpon',$this->no_telpon,true);
		$criteria->compare('alamat',$this->alamat,true);
		$criteria->compare('id_departemen',$this->id_departemen,true);
		$criteria->compare('id_dokter',$this->id_dokter,true);
		$criteria->compare('waktu_perjanjian',$this->waktu_perjanjian,true);
		$criteria->compare('jenis_perjanjian',$this->jenis_perjanjian,true);
		$criteria->compare('waktu_daftar',$this->waktu_daftar,true);
		$criteria->compare('jenis_pelayanan',$this->jenis_pelayanan,true);
		$criteria->compare('status_perjanjian',$this->status_perjanjian,true);
		$criteria->compare('user_create',$this->user_create,true);
		$criteria->compare('user_update',$this->user_update,true);
		$criteria->compare('user_cancel',$this->user_cancel,true);
		$criteria->compare('time_create',$this->time_create,true);
		$criteria->compare('time_update',$this->time_update,true);
		$criteria->compare('time_cancel',$this->time_cancel,true);

		return new CActiveDataProvider($this, array(
			'pagination'=>array(
				'pageSize'=> Yii::app()->user->getState('pageSize',Yii::app()->params['defaultPageSize']),
			),
			'criteria'=>$criteria,
		));
	}
	
	public function getAllowView()
	{
		$akses = UserAccess::model()->find(array("with"=>array("access"),'condition'=>"id_user='".Yii::app()->user->id."' and access.access_controller='Perjanjian' and access.access_action='view'"));
		if(!empty($akses)) return true; else return false;
	}
	
	public function getAllowUpdate()
	{
		$akses = UserAccess::model()->find(array("with"=>array("access"),'condition'=>"id_user='".Yii::app()->user->id."' and access.access_controller='Perjanjian' and access.access_action='update'"));
		if(!empty($akses)) return true; else return false;
	}
	
	public function getAllowDelete($id)
	{
		$akses = UserAccess::model()->find(array("with"=>array("access"),'condition'=>"id_user='".Yii::app()->user->id."' and access.access_controller='Perjanjian' and access.access_action='delete'"));
		$rel=$this->getEmptyRelation($id);
		if(!empty($akses) AND $rel==true){
			return true;
		}else{
			return false;
		}
	}
	
	
	public function getEmptyRelation($id)
	{
		$count=0;
														if($count>0){
			return false;
		}else{
			return true;
		}
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Perjanjian the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
