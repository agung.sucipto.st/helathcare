<?php

/**
 * This is the model class for table "daftar_item_transaksi".
 *
 * The followings are the available columns in table 'daftar_item_transaksi':
 * @property string $id_daftar_item_transaksi
 * @property string $id_item_transaksi
 * @property string $id_item_gudang
 * @property string $id_item
 * @property string $is_bonus
 * @property string $is_racikan
 * @property string $nama_racikan
 * @property string $id_parent_racikan
 * @property integer $biaya_racikan
 * @property string $id_item_satuan_besar
 * @property string $id_item_satuan_kecil
 * @property integer $jumlah_satuan_besar
 * @property integer $jumlah_satuan_kecil
 * @property string $jumlah_transaksi
 * @property string $tanggal_expired
 * @property string $no_batch
 * @property string $jumlah_sebelum_transaksi
 * @property string $jumlah_setelah_transaksi
 * @property string $harga_transaksi
 * @property string $harga_wac
 * @property string $discount
 * @property string $ppn
 * @property string $is_retur
 * @property integer $jumlah_retur
 * @property string $id_daftar_tagihan
 * @property string $formularium
 *
 * The followings are the available model relations:
 * @property ItemGudang $idItemGudang
 * @property DaftarItemTransaksi $idParentRacikan
 * @property DaftarItemTransaksi[] $daftarItemTransaksis
 * @property DaftarTagihan $idDaftarTagihan
 * @property Item $idItem
 * @property ItemSatuan $idItemSatuanKecil
 * @property ItemSatuan $idItemSatuanBesar
 * @property ItemTransaksi $idItemTransaksi
 * @property DaftarPengajuanPembayaranSupplier[] $daftarPengajuanPembayaranSuppliers
 * @property ItemPasien[] $itemPasiens
 * @property ResepPasienList[] $resepPasienLists
 */
class DaftarItemTransaksi extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'daftar_item_transaksi';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('id_item_transaksi, jumlah_transaksi', 'required'),
			array('biaya_racikan, jumlah_satuan_besar, jumlah_satuan_kecil, jumlah_retur', 'numerical', 'integerOnly'=>true),
			array('id_item_transaksi, id_item_gudang, id_item, id_parent_racikan, id_item_satuan_besar, id_item_satuan_kecil, id_daftar_tagihan', 'length', 'max'=>20),
			array('is_bonus, is_racikan, is_retur', 'length', 'max'=>1),
			array('nama_racikan', 'length', 'max'=>50),
			array('jumlah_transaksi, jumlah_sebelum_transaksi, jumlah_setelah_transaksi, harga_transaksi, discount, ppn', 'length', 'max'=>30),
			array('no_batch', 'length', 'max'=>45),
			array('harga_wac', 'length', 'max'=>10),
			array('formularium', 'length', 'max'=>100),
			array('tanggal_expired', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id_daftar_item_transaksi, id_item_transaksi, id_item_gudang, id_item, is_bonus, is_racikan, nama_racikan, id_parent_racikan, biaya_racikan, id_item_satuan_besar, id_item_satuan_kecil, jumlah_satuan_besar, jumlah_satuan_kecil, jumlah_transaksi, tanggal_expired, no_batch, jumlah_sebelum_transaksi, jumlah_setelah_transaksi, harga_transaksi, harga_wac, discount, ppn, is_retur, jumlah_retur, id_daftar_tagihan, formularium', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'idItemGudang' => array(self::BELONGS_TO, 'ItemGudang', 'id_item_gudang'),
			'idParentRacikan' => array(self::BELONGS_TO, 'DaftarItemTransaksi', 'id_parent_racikan'),
			'daftarItemTransaksis' => array(self::HAS_MANY, 'DaftarItemTransaksi', 'id_parent_racikan'),
			'idDaftarTagihan' => array(self::BELONGS_TO, 'DaftarTagihan', 'id_daftar_tagihan'),
			'idItem' => array(self::BELONGS_TO, 'Item', 'id_item'),
			'idItemSatuanKecil' => array(self::BELONGS_TO, 'ItemSatuan', 'id_item_satuan_kecil'),
			'idItemSatuanBesar' => array(self::BELONGS_TO, 'ItemSatuan', 'id_item_satuan_besar'),
			'idItemTransaksi' => array(self::BELONGS_TO, 'ItemTransaksi', 'id_item_transaksi'),
			'daftarPengajuanPembayaranSuppliers' => array(self::HAS_MANY, 'DaftarPengajuanPembayaranSupplier', 'id_daftar_item_transaksi'),
			'itemPasiens' => array(self::HAS_MANY, 'ItemPasien', 'id_daftar_item_transaksi'),
			'resepPasienLists' => array(self::HAS_MANY, 'ResepPasienList', 'id_daftar_item_transaksi'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id_daftar_item_transaksi' => 'Id Daftar Item Transaksi',
			'id_item_transaksi' => 'Id Item Transaksi',
			'id_item_gudang' => 'Id Item Gudang',
			'id_item' => 'Id Item',
			'is_bonus' => 'Is Bonus',
			'is_racikan' => 'Is Racikan',
			'nama_racikan' => 'Nama Racikan',
			'id_parent_racikan' => 'Id Parent Racikan',
			'biaya_racikan' => 'Biaya Racikan',
			'id_item_satuan_besar' => 'Id Item Satuan Besar',
			'id_item_satuan_kecil' => 'Id Item Satuan Kecil',
			'jumlah_satuan_besar' => 'Jumlah Satuan Besar',
			'jumlah_satuan_kecil' => 'Jumlah Satuan Kecil',
			'jumlah_transaksi' => 'Jumlah Transaksi',
			'tanggal_expired' => 'Tanggal Expired',
			'no_batch' => 'No Batch',
			'jumlah_sebelum_transaksi' => 'Jumlah Sebelum Transaksi',
			'jumlah_setelah_transaksi' => 'Jumlah Setelah Transaksi',
			'harga_transaksi' => 'Harga Transaksi',
			'harga_wac' => 'Harga Wac',
			'discount' => 'Discount',
			'ppn' => 'Ppn',
			'is_retur' => 'Is Retur',
			'jumlah_retur' => 'Jumlah Retur',
			'id_daftar_tagihan' => 'Id Daftar Tagihan',
			'formularium' => 'Formularium',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id_daftar_item_transaksi',$this->id_daftar_item_transaksi,true);
		$criteria->compare('id_item_transaksi',$this->id_item_transaksi,true);
		$criteria->compare('id_item_gudang',$this->id_item_gudang,true);
		$criteria->compare('id_item',$this->id_item,true);
		$criteria->compare('is_bonus',$this->is_bonus,true);
		$criteria->compare('is_racikan',$this->is_racikan,true);
		$criteria->compare('nama_racikan',$this->nama_racikan,true);
		$criteria->compare('id_parent_racikan',$this->id_parent_racikan,true);
		$criteria->compare('biaya_racikan',$this->biaya_racikan);
		$criteria->compare('id_item_satuan_besar',$this->id_item_satuan_besar,true);
		$criteria->compare('id_item_satuan_kecil',$this->id_item_satuan_kecil,true);
		$criteria->compare('jumlah_satuan_besar',$this->jumlah_satuan_besar);
		$criteria->compare('jumlah_satuan_kecil',$this->jumlah_satuan_kecil);
		$criteria->compare('jumlah_transaksi',$this->jumlah_transaksi,true);
		$criteria->compare('tanggal_expired',$this->tanggal_expired,true);
		$criteria->compare('no_batch',$this->no_batch,true);
		$criteria->compare('jumlah_sebelum_transaksi',$this->jumlah_sebelum_transaksi,true);
		$criteria->compare('jumlah_setelah_transaksi',$this->jumlah_setelah_transaksi,true);
		$criteria->compare('harga_transaksi',$this->harga_transaksi,true);
		$criteria->compare('harga_wac',$this->harga_wac,true);
		$criteria->compare('discount',$this->discount,true);
		$criteria->compare('ppn',$this->ppn,true);
		$criteria->compare('is_retur',$this->is_retur,true);
		$criteria->compare('jumlah_retur',$this->jumlah_retur);
		$criteria->compare('id_daftar_tagihan',$this->id_daftar_tagihan,true);
		$criteria->compare('formularium',$this->formularium,true);

		return new CActiveDataProvider($this, array(
			'pagination'=>array(
				'pageSize'=> Yii::app()->user->getState('pageSize',Yii::app()->params['defaultPageSize']),
			),
			'criteria'=>$criteria,
		));
	}
	
	public function getAllowView()
	{
		$akses = UserAccess::model()->find(array("with"=>array("access"),'condition'=>"id_user='".Yii::app()->user->id."' and access.access_controller='DaftarItemTransaksi' and access.access_action='view'"));
		if(!empty($akses)) return true; else return false;
	}
	
	public function getAllowUpdate()
	{
		$akses = UserAccess::model()->find(array("with"=>array("access"),'condition'=>"id_user='".Yii::app()->user->id."' and access.access_controller='DaftarItemTransaksi' and access.access_action='update'"));
		if(!empty($akses)) return true; else return false;
	}
	
	public function getAllowDelete($id)
	{
		$akses = UserAccess::model()->find(array("with"=>array("access"),'condition'=>"id_user='".Yii::app()->user->id."' and access.access_controller='DaftarItemTransaksi' and access.access_action='delete'"));
		$rel=$this->getEmptyRelation($id);
		if(!empty($akses) AND $rel==true){
			return true;
		}else{
			return false;
		}
	}
	
	
	public function getEmptyRelation($id)
	{
		$count=0;
		$count+=DaftarItemTransaksi::model()->count(array('condition'=>"id_parent_racikan='$id'"));
		$count+=DaftarPengajuanPembayaranSupplier::model()->count(array('condition'=>"id_daftar_item_transaksi='$id'"));
		$count+=ResepPasienList::model()->count(array('condition'=>"id_daftar_item_transaksi='$id'"));
		if($count>0){
			return false;
		}else{
			return true;
		}
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return DaftarItemTransaksi the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
