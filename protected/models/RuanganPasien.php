<?php

/**
 * This is the model class for table "ruangan_pasien".
 *
 * The followings are the available columns in table 'ruangan_pasien':
 * @property string $id_ruangan_pasien
 * @property string $id_registrasi
 * @property string $id_ruangan
 * @property string $id_tarif_ruangan
 * @property string $id_daftar_tagihan
 * @property string $waktu_masuk
 * @property string $waktu_keluar
 * @property string $jasa_sarana
 * @property string $jasa_peralatan
 * @property string $time_create
 * @property string $time_update
 * @property string $time_approve
 * @property string $user_create
 * @property string $user_update
 * @property string $user_approve
 *
 * The followings are the available model relations:
 * @property DaftarTagihan[] $daftarTagihans
 * @property DaftarTagihan $idDaftarTagihan
 * @property Registrasi $idRegistrasi
 * @property Ruangan $idRuangan
 * @property TarifRuangan $idTarifRuangan
 * @property User $userCreate
 * @property User $userUpdate
 * @property User $userApprove
 */
class RuanganPasien extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'ruangan_pasien';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('id_registrasi, id_ruangan, id_tarif_ruangan, id_daftar_tagihan, waktu_masuk, jasa_sarana, jasa_peralatan, time_create, user_create', 'required'),
			array('id_registrasi, id_ruangan, id_tarif_ruangan, id_daftar_tagihan, user_create, user_update, user_approve', 'length', 'max'=>20),
			array('jasa_sarana, jasa_peralatan', 'length', 'max'=>30),
			array('waktu_keluar, time_update, time_approve', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id_ruangan_pasien, id_registrasi, id_ruangan, id_tarif_ruangan, id_daftar_tagihan, waktu_masuk, waktu_keluar, jasa_sarana, jasa_peralatan, time_create, time_update, time_approve, user_create, user_update, user_approve', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'daftarTagihans' => array(self::HAS_MANY, 'DaftarTagihan', 'id_ruangan_pasien'),
			'idDaftarTagihan' => array(self::BELONGS_TO, 'DaftarTagihan', 'id_daftar_tagihan'),
			'idRegistrasi' => array(self::BELONGS_TO, 'Registrasi', 'id_registrasi'),
			'idRuangan' => array(self::BELONGS_TO, 'Ruangan', 'id_ruangan'),
			'idTarifRuangan' => array(self::BELONGS_TO, 'TarifRuangan', 'id_tarif_ruangan'),
			'userCreate' => array(self::BELONGS_TO, 'User', 'user_create'),
			'userUpdate' => array(self::BELONGS_TO, 'User', 'user_update'),
			'userApprove' => array(self::BELONGS_TO, 'User', 'user_approve'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id_ruangan_pasien' => 'Id Ruangan Pasien',
			'id_registrasi' => 'Id Registrasi',
			'id_ruangan' => 'Id Ruangan',
			'id_tarif_ruangan' => 'Id Tarif Ruangan',
			'id_daftar_tagihan' => 'Id Daftar Tagihan',
			'waktu_masuk' => 'Waktu Masuk',
			'waktu_keluar' => 'Waktu Keluar',
			'jasa_sarana' => 'Jasa Sarana',
			'jasa_peralatan' => 'Jasa Peralatan',
			'time_create' => 'Time Create',
			'time_update' => 'Time Update',
			'time_approve' => 'Time Approve',
			'user_create' => 'User Create',
			'user_update' => 'User Update',
			'user_approve' => 'User Approve',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id_ruangan_pasien',$this->id_ruangan_pasien,true);
		$criteria->compare('id_registrasi',$this->id_registrasi,true);
		$criteria->compare('id_ruangan',$this->id_ruangan,true);
		$criteria->compare('id_tarif_ruangan',$this->id_tarif_ruangan,true);
		$criteria->compare('id_daftar_tagihan',$this->id_daftar_tagihan,true);
		$criteria->compare('waktu_masuk',$this->waktu_masuk,true);
		$criteria->compare('waktu_keluar',$this->waktu_keluar,true);
		$criteria->compare('jasa_sarana',$this->jasa_sarana,true);
		$criteria->compare('jasa_peralatan',$this->jasa_peralatan,true);
		$criteria->compare('time_create',$this->time_create,true);
		$criteria->compare('time_update',$this->time_update,true);
		$criteria->compare('time_approve',$this->time_approve,true);
		$criteria->compare('user_create',$this->user_create,true);
		$criteria->compare('user_update',$this->user_update,true);
		$criteria->compare('user_approve',$this->user_approve,true);

		return new CActiveDataProvider($this, array(
			'pagination'=>array(
				'pageSize'=> Yii::app()->user->getState('pageSize',Yii::app()->params['defaultPageSize']),
			),
			'criteria'=>$criteria,
		));
	}
	
	public function getAllowView()
	{
		$akses = UserAccess::model()->find(array("with"=>array("access"),'condition'=>"id_user='".Yii::app()->user->id."' and access.access_controller='RuanganPasien' and access.access_action='view'"));
		if(!empty($akses)) return true; else return false;
	}
	
	public function getAllowUpdate()
	{
		$akses = UserAccess::model()->find(array("with"=>array("access"),'condition'=>"id_user='".Yii::app()->user->id."' and access.access_controller='RuanganPasien' and access.access_action='update'"));
		if(!empty($akses)) return true; else return false;
	}
	
	public function getAllowDelete($id)
	{
		$akses = UserAccess::model()->find(array("with"=>array("access"),'condition'=>"id_user='".Yii::app()->user->id."' and access.access_controller='RuanganPasien' and access.access_action='delete'"));
		$rel=$this->getEmptyRelation($id);
		if(!empty($akses) AND $rel==true){
			return true;
		}else{
			return false;
		}
	}
	
	
	public function getEmptyRelation($id)
	{
		$count=0;
		$count+=DaftarTagihan::model()->count(array('condition'=>"id_ruangan_pasien='$id'"));
																if($count>0){
			return false;
		}else{
			return true;
		}
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return RuanganPasien the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
