<?php

/**
 * This is the model class for table "laboratorium".
 *
 * The followings are the available columns in table 'laboratorium':
 * @property string $id_laboratorium
 * @property string $id_jenis_pemeriksaan
 * @property string $is_header
 * @property string $nama_pemeriksaan
 * @property string $status_pemeriksaan
 * @property string $satuan_hasil
 * @property string $isian_hasil
 * @property string $parent_id
 *
 * The followings are the available model relations:
 * @property JenisPemeriksaan $idJenisPemeriksaan
 * @property Laboratorium $parent
 * @property Laboratorium[] $laboratoria
 * @property LaboratoriumMedis[] $laboratoriumMedises
 * @property LaboratoriumNilaiRujukan[] $laboratoriumNilaiRujukans
 * @property LaboratoriumPasienList[] $laboratoriumPasienLists
 * @property LaboratoriumPasienMedis[] $laboratoriumPasienMedises
 * @property TarifLaboratorium[] $tarifLaboratoria
 */
class Laboratorium extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'laboratorium';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('id_jenis_pemeriksaan, is_header, nama_pemeriksaan, status_pemeriksaan, satuan_hasil, isian_hasil', 'required'),
			array('id_jenis_pemeriksaan, parent_id', 'length', 'max'=>20),
			array('is_header', 'length', 'max'=>1),
			array('nama_pemeriksaan, isian_hasil', 'length', 'max'=>100),
			array('status_pemeriksaan', 'length', 'max'=>11),
			array('satuan_hasil', 'length', 'max'=>40),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id_laboratorium, id_jenis_pemeriksaan, is_header, nama_pemeriksaan, status_pemeriksaan, satuan_hasil, isian_hasil, parent_id', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'idJenisPemeriksaan' => array(self::BELONGS_TO, 'JenisPemeriksaan', 'id_jenis_pemeriksaan'),
			'parent' => array(self::BELONGS_TO, 'Laboratorium', 'parent_id'),
			'laboratoria' => array(self::HAS_MANY, 'Laboratorium', 'parent_id'),
			'laboratoriumMedises' => array(self::HAS_MANY, 'LaboratoriumMedis', 'id_laboratorium'),
			'laboratoriumNilaiRujukans' => array(self::HAS_MANY, 'LaboratoriumNilaiRujukan', 'id_laboratorium'),
			'laboratoriumPasienLists' => array(self::HAS_MANY, 'LaboratoriumPasienList', 'id_laboratorium'),
			'laboratoriumPasienMedises' => array(self::HAS_MANY, 'LaboratoriumPasienMedis', 'id_laboratorium'),
			'tarifLaboratoria' => array(self::HAS_MANY, 'TarifLaboratorium', 'id_laboratorium'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id_laboratorium' => 'Id Laboratorium',
			'id_jenis_pemeriksaan' => 'Id Jenis Pemeriksaan',
			'is_header' => 'Is Header',
			'nama_pemeriksaan' => 'Nama Pemeriksaan',
			'status_pemeriksaan' => 'Status Pemeriksaan',
			'satuan_hasil' => 'Satuan Hasil',
			'isian_hasil' => 'Isian Hasil',
			'parent_id' => 'Parent',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id_laboratorium',$this->id_laboratorium,true);
		$criteria->compare('id_jenis_pemeriksaan',$this->id_jenis_pemeriksaan,true);
		$criteria->compare('is_header',$this->is_header,true);
		$criteria->compare('nama_pemeriksaan',$this->nama_pemeriksaan,true);
		$criteria->compare('status_pemeriksaan',$this->status_pemeriksaan,true);
		$criteria->compare('satuan_hasil',$this->satuan_hasil,true);
		$criteria->compare('isian_hasil',$this->isian_hasil,true);
		$criteria->compare('parent_id',$this->parent_id,true);

		return new CActiveDataProvider($this, array(
			'pagination'=>array(
				'pageSize'=> Yii::app()->user->getState('pageSize',Yii::app()->params['defaultPageSize']),
			),
			'criteria'=>$criteria,
		));
	}
	
	public function getAllowView()
	{
		$akses = UserAccess::model()->find(array("with"=>array("access"),'condition'=>"id_user='".Yii::app()->user->id."' and access.access_controller='Laboratorium' and access.access_action='view'"));
		if(!empty($akses)) return true; else return false;
	}
	
	public function getAllowUpdate()
	{
		$akses = UserAccess::model()->find(array("with"=>array("access"),'condition'=>"id_user='".Yii::app()->user->id."' and access.access_controller='Laboratorium' and access.access_action='update'"));
		if(!empty($akses)) return true; else return false;
	}
	
	public function getAllowDelete($id)
	{
		$akses = UserAccess::model()->find(array("with"=>array("access"),'condition'=>"id_user='".Yii::app()->user->id."' and access.access_controller='Laboratorium' and access.access_action='delete'"));
		$rel=$this->getEmptyRelation($id);
		if(!empty($akses) AND $rel==true){
			return true;
		}else{
			return false;
		}
	}
	
	
	public function getEmptyRelation($id)
	{
		$count=0;
						$count+=Laboratorium::model()->count(array('condition'=>"parent_id='$id'"));
		$count+=LaboratoriumMedis::model()->count(array('condition'=>"id_laboratorium='$id'"));
		$count+=LaboratoriumNilaiRujukan::model()->count(array('condition'=>"id_laboratorium='$id'"));
		$count+=LaboratoriumPasienList::model()->count(array('condition'=>"id_laboratorium='$id'"));
		$count+=LaboratoriumPasienMedis::model()->count(array('condition'=>"id_laboratorium='$id'"));
		$count+=TarifLaboratorium::model()->count(array('condition'=>"id_laboratorium='$id'"));
		if($count>0){
			return false;
		}else{
			return true;
		}
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Laboratorium the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
