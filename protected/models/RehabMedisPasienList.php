<?php

/**
 * This is the model class for table "rehab_medis_pasien_list".
 *
 * The followings are the available columns in table 'rehab_medis_pasien_list':
 * @property string $id_rehab_medis_pasien_list
 * @property string $id_rehab_medis_pasien
 * @property string $id_registrasi
 * @property string $id_kelas
 * @property string $id_tindakan
 * @property string $id_tarif_tindakan
 * @property string $catatan
 * @property string $waktu_tindakan
 * @property string $jumlah_tindakan
 * @property string $id_daftar_tagihan
 * @property string $status_tindakan
 * @property string $user_create
 * @property string $user_update
 * @property string $user_final
 * @property string $time_create
 * @property string $time_update
 * @property string $time_final
 *
 * The followings are the available model relations:
 * @property RehabMedisPasien $idRehabMedisPasien
 * @property Registrasi $idRegistrasi
 * @property Kelas $idKelas
 * @property Tindakan $idTindakan
 * @property TarifTindakan $idTarifTindakan
 * @property DaftarTagihan $idDaftarTagihan
 * @property User $userCreate
 * @property User $userUpdate
 * @property User $userFinal
 * @property RehabMedisPasienMedis[] $rehabMedisPasienMedises
 */
class RehabMedisPasienList extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'rehab_medis_pasien_list';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('id_rehab_medis_pasien, id_registrasi, id_kelas, id_tindakan, id_tarif_tindakan, waktu_tindakan, jumlah_tindakan, status_tindakan, user_create, time_create', 'required'),
			array('id_rehab_medis_pasien, id_registrasi, id_kelas, id_tindakan, id_tarif_tindakan, jumlah_tindakan, id_daftar_tagihan, user_create, user_update, user_final', 'length', 'max'=>20),
			array('status_tindakan', 'length', 'max'=>13),
			array('catatan, time_update, time_final', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id_rehab_medis_pasien_list, id_rehab_medis_pasien, id_registrasi, id_kelas, id_tindakan, id_tarif_tindakan, catatan, waktu_tindakan, jumlah_tindakan, id_daftar_tagihan, status_tindakan, user_create, user_update, user_final, time_create, time_update, time_final', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'idRehabMedisPasien' => array(self::BELONGS_TO, 'RehabMedisPasien', 'id_rehab_medis_pasien'),
			'idRegistrasi' => array(self::BELONGS_TO, 'Registrasi', 'id_registrasi'),
			'idKelas' => array(self::BELONGS_TO, 'Kelas', 'id_kelas'),
			'idTindakan' => array(self::BELONGS_TO, 'Tindakan', 'id_tindakan'),
			'idTarifTindakan' => array(self::BELONGS_TO, 'TarifTindakan', 'id_tarif_tindakan'),
			'idDaftarTagihan' => array(self::BELONGS_TO, 'DaftarTagihan', 'id_daftar_tagihan'),
			'userCreate' => array(self::BELONGS_TO, 'User', 'user_create'),
			'userUpdate' => array(self::BELONGS_TO, 'User', 'user_update'),
			'userFinal' => array(self::BELONGS_TO, 'User', 'user_final'),
			'rehabMedisPasienMedises' => array(self::HAS_MANY, 'RehabMedisPasienMedis', 'id_rehab_medis_pasien_list'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id_rehab_medis_pasien_list' => 'Id Rehab Medis Pasien List',
			'id_rehab_medis_pasien' => 'Id Rehab Medis Pasien',
			'id_registrasi' => 'Id Registrasi',
			'id_kelas' => 'Id Kelas',
			'id_tindakan' => 'Id Tindakan',
			'id_tarif_tindakan' => 'Id Tarif Tindakan',
			'catatan' => 'Catatan',
			'waktu_tindakan' => 'Waktu Tindakan',
			'jumlah_tindakan' => 'Jumlah Tindakan',
			'id_daftar_tagihan' => 'Id Daftar Tagihan',
			'status_tindakan' => 'Status Tindakan',
			'user_create' => 'User Create',
			'user_update' => 'User Update',
			'user_final' => 'User Final',
			'time_create' => 'Time Create',
			'time_update' => 'Time Update',
			'time_final' => 'Time Final',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id_rehab_medis_pasien_list',$this->id_rehab_medis_pasien_list,true);
		$criteria->compare('id_rehab_medis_pasien',$this->id_rehab_medis_pasien,true);
		$criteria->compare('id_registrasi',$this->id_registrasi,true);
		$criteria->compare('id_kelas',$this->id_kelas,true);
		$criteria->compare('id_tindakan',$this->id_tindakan,true);
		$criteria->compare('id_tarif_tindakan',$this->id_tarif_tindakan,true);
		$criteria->compare('catatan',$this->catatan,true);
		$criteria->compare('waktu_tindakan',$this->waktu_tindakan,true);
		$criteria->compare('jumlah_tindakan',$this->jumlah_tindakan,true);
		$criteria->compare('id_daftar_tagihan',$this->id_daftar_tagihan,true);
		$criteria->compare('status_tindakan',$this->status_tindakan,true);
		$criteria->compare('user_create',$this->user_create,true);
		$criteria->compare('user_update',$this->user_update,true);
		$criteria->compare('user_final',$this->user_final,true);
		$criteria->compare('time_create',$this->time_create,true);
		$criteria->compare('time_update',$this->time_update,true);
		$criteria->compare('time_final',$this->time_final,true);

		return new CActiveDataProvider($this, array(
			'pagination'=>array(
				'pageSize'=> Yii::app()->user->getState('pageSize',Yii::app()->params['defaultPageSize']),
			),
			'criteria'=>$criteria,
		));
	}
	
	public function getAllowView()
	{
		$akses = UserAccess::model()->find(array("with"=>array("access"),'condition'=>"id_user='".Yii::app()->user->id."' and access.access_controller='RehabMedisPasienList' and access.access_action='view'"));
		if(!empty($akses)) return true; else return false;
	}
	
	public function getAllowUpdate()
	{
		$akses = UserAccess::model()->find(array("with"=>array("access"),'condition'=>"id_user='".Yii::app()->user->id."' and access.access_controller='RehabMedisPasienList' and access.access_action='update'"));
		if(!empty($akses)) return true; else return false;
	}
	
	public function getAllowDelete($id)
	{
		$akses = UserAccess::model()->find(array("with"=>array("access"),'condition'=>"id_user='".Yii::app()->user->id."' and access.access_controller='RehabMedisPasienList' and access.access_action='delete'"));
		$rel=$this->getEmptyRelation($id);
		if(!empty($akses) AND $rel==true){
			return true;
		}else{
			return false;
		}
	}
	
	
	public function getEmptyRelation($id)
	{
		$count=0;
																				$count+=RehabMedisPasienMedis::model()->count(array('condition'=>"id_rehab_medis_pasien_list='$id'"));
		if($count>0){
			return false;
		}else{
			return true;
		}
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return RehabMedisPasienList the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
