<?php

/**
 * This is the model class for table "tarif_visite_dokter".
 *
 * The followings are the available columns in table 'tarif_visite_dokter':
 * @property string $id_tarif_visite_dokter
 * @property string $id_kelompok_tarif
 * @property string $id_kelas
 * @property string $id_pegawai
 * @property string $share_dokter
 * @property string $share_layanan
 *
 * The followings are the available model relations:
 * @property Kelas $idKelas
 * @property KelompokTarif $idKelompokTarif
 * @property Pegawai $idPegawai
 * @property VisiteDokterPasien[] $visiteDokterPasiens
 */
class TarifVisiteDokter extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'tarif_visite_dokter';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('id_kelompok_tarif, id_kelas, id_pegawai, share_dokter, share_layanan', 'required'),
			array('id_kelompok_tarif, id_kelas, id_pegawai', 'length', 'max'=>20),
			array('share_dokter, share_layanan', 'length', 'max'=>30),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id_tarif_visite_dokter, id_kelompok_tarif, id_kelas, id_pegawai, share_dokter, share_layanan', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'idKelas' => array(self::BELONGS_TO, 'Kelas', 'id_kelas'),
			'idKelompokTarif' => array(self::BELONGS_TO, 'KelompokTarif', 'id_kelompok_tarif'),
			'idPegawai' => array(self::BELONGS_TO, 'Pegawai', 'id_pegawai'),
			'visiteDokterPasiens' => array(self::HAS_MANY, 'VisiteDokterPasien', 'id_tarif_visite_dokter'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id_tarif_visite_dokter' => 'Id Tarif Visite Dokter',
			'id_kelompok_tarif' => 'Id Kelompok Tarif',
			'id_kelas' => 'Id Kelas',
			'id_pegawai' => 'Id Pegawai',
			'share_dokter' => 'Share Dokter',
			'share_layanan' => 'Share Layanan',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id_tarif_visite_dokter',$this->id_tarif_visite_dokter,true);
		$criteria->compare('id_kelompok_tarif',$this->id_kelompok_tarif,true);
		$criteria->compare('id_kelas',$this->id_kelas,true);
		$criteria->compare('id_pegawai',$this->id_pegawai,true);
		$criteria->compare('share_dokter',$this->share_dokter,true);
		$criteria->compare('share_layanan',$this->share_layanan,true);

		return new CActiveDataProvider($this, array(
			'pagination'=>array(
				'pageSize'=> Yii::app()->user->getState('pageSize',Yii::app()->params['defaultPageSize']),
			),
			'criteria'=>$criteria,
		));
	}
	
	public function getAllowView()
	{
		$akses = UserAccess::model()->find(array("with"=>array("access"),'condition'=>"id_user='".Yii::app()->user->id."' and access.access_controller='TarifVisiteDokter' and access.access_action='view'"));
		if(!empty($akses)) return true; else return false;
	}
	
	public function getAllowUpdate()
	{
		$akses = UserAccess::model()->find(array("with"=>array("access"),'condition'=>"id_user='".Yii::app()->user->id."' and access.access_controller='TarifVisiteDokter' and access.access_action='update'"));
		if(!empty($akses)) return true; else return false;
	}
	
	public function getAllowDelete($id)
	{
		$akses = UserAccess::model()->find(array("with"=>array("access"),'condition'=>"id_user='".Yii::app()->user->id."' and access.access_controller='TarifVisiteDokter' and access.access_action='delete'"));
		$rel=$this->getEmptyRelation($id);
		if(!empty($akses) AND $rel==true){
			return true;
		}else{
			return false;
		}
	}
	
	
	public function getEmptyRelation($id)
	{
		$count=0;
		$count+=VisiteDokterPasien::model()->count(array('condition'=>"id_tarif_visite_dokter='$id'"));
		if($count>0){
			return false;
		}else{
			return true;
		}
	}
	
	public static function getTarif($reg,$kelas,$dokter){
		if($reg->jenis_jaminan=="UMUM"){
			$id_kelompok_tarif=1;
		}else{
			$jaminan=JaminanPasien::model()->find(array("condition"=>"id_registrasi='$reg->id_registrasi' AND is_utama='1'"));
			$id_kelompok_tarif=$jaminan->idPenjamin->id_kelompok_tarif;
		}
		
		$model=TarifVisiteDokter::model()->find(array("condition"=>"id_kelompok_tarif='$id_kelompok_tarif' AND id_kelas='$kelas' AND id_pegawai='$dokter'"));
		
		return $model;
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return TarifVisiteDokter the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
