<?php

/**
 * This is the model class for table "piutang_pelanggan".
 *
 * The followings are the available columns in table 'piutang_pelanggan':
 * @property string $id_piutang_pelanggan
 * @property string $id_pelanggan
 * @property string $waktu_pembuatan_piutang
 * @property string $waktu_jatuh_tempo
 * @property string $no_tagihan
 * @property string $total_tagihan
 * @property string $total_discount
 * @property string $status_pembayaran
 * @property string $user_create
 * @property string $user_update
 * @property string $user_final
 * @property string $time_create
 * @property string $time_update
 * @property string $time_final
 *
 * The followings are the available model relations:
 * @property DaftarPiutangPelanggan[] $daftarPiutangPelanggans
 * @property PembayaranPiutangPelanggan[] $pembayaranPiutangPelanggans
 * @property Pelanggan $idPelanggan
 * @property User $userCreate
 * @property User $userUpdate
 * @property User $userFinal
 */
class PiutangPelanggan extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'piutang_pelanggan';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('id_pelanggan, waktu_pembuatan_piutang, waktu_jatuh_tempo, no_tagihan, total_tagihan, user_create, time_create', 'required'),
			array('id_pelanggan, user_create, user_update, user_final', 'length', 'max'=>20),
			array('no_tagihan, total_tagihan, total_discount', 'length', 'max'=>45),
			array('status_pembayaran', 'length', 'max'=>16),
			array('time_update, time_final', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id_piutang_pelanggan, id_pelanggan, waktu_pembuatan_piutang, waktu_jatuh_tempo, no_tagihan, total_tagihan, total_discount, status_pembayaran, user_create, user_update, user_final, time_create, time_update, time_final', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'daftarPiutangPelanggans' => array(self::HAS_MANY, 'DaftarPiutangPelanggan', 'id_piutang_pelanggan'),
			'pembayaranPiutangPelanggans' => array(self::HAS_MANY, 'PembayaranPiutangPelanggan', 'id_piutang_pelanggan'),
			'idPelanggan' => array(self::BELONGS_TO, 'Pelanggan', 'id_pelanggan'),
			'userCreate' => array(self::BELONGS_TO, 'User', 'user_create'),
			'userUpdate' => array(self::BELONGS_TO, 'User', 'user_update'),
			'userFinal' => array(self::BELONGS_TO, 'User', 'user_final'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id_piutang_pelanggan' => 'Id Piutang Pelanggan',
			'id_pelanggan' => 'Id Pelanggan',
			'waktu_pembuatan_piutang' => 'Waktu Pembuatan Piutang',
			'waktu_jatuh_tempo' => 'Waktu Jatuh Tempo',
			'no_tagihan' => 'No Tagihan',
			'total_tagihan' => 'Total Tagihan',
			'total_discount' => 'Total Discount',
			'status_pembayaran' => 'Status Pembayaran',
			'user_create' => 'User Create',
			'user_update' => 'User Update',
			'user_final' => 'User Final',
			'time_create' => 'Time Create',
			'time_update' => 'Time Update',
			'time_final' => 'Time Final',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id_piutang_pelanggan',$this->id_piutang_pelanggan,true);
		$criteria->compare('id_pelanggan',$this->id_pelanggan,true);
		$criteria->compare('waktu_pembuatan_piutang',$this->waktu_pembuatan_piutang,true);
		$criteria->compare('waktu_jatuh_tempo',$this->waktu_jatuh_tempo,true);
		$criteria->compare('no_tagihan',$this->no_tagihan,true);
		$criteria->compare('total_tagihan',$this->total_tagihan,true);
		$criteria->compare('total_discount',$this->total_discount,true);
		$criteria->compare('status_pembayaran',$this->status_pembayaran,true);
		$criteria->compare('user_create',$this->user_create,true);
		$criteria->compare('user_update',$this->user_update,true);
		$criteria->compare('user_final',$this->user_final,true);
		$criteria->compare('time_create',$this->time_create,true);
		$criteria->compare('time_update',$this->time_update,true);
		$criteria->compare('time_final',$this->time_final,true);

		return new CActiveDataProvider($this, array(
			'pagination'=>array(
				'pageSize'=> Yii::app()->user->getState('pageSize',Yii::app()->params['defaultPageSize']),
			),
			'criteria'=>$criteria,
		));
	}
	
	public function getAllowView()
	{
		$akses = UserAccess::model()->find(array("with"=>array("access"),'condition'=>"id_user='".Yii::app()->user->id."' and access.access_controller='PiutangPelanggan' and access.access_action='view'"));
		if(!empty($akses)) return true; else return false;
	}
	
	public function getAllowUpdate()
	{
		$akses = UserAccess::model()->find(array("with"=>array("access"),'condition'=>"id_user='".Yii::app()->user->id."' and access.access_controller='PiutangPelanggan' and access.access_action='update'"));
		if(!empty($akses)) return true; else return false;
	}
	
	public function getAllowDelete($id)
	{
		$akses = UserAccess::model()->find(array("with"=>array("access"),'condition'=>"id_user='".Yii::app()->user->id."' and access.access_controller='PiutangPelanggan' and access.access_action='delete'"));
		$rel=$this->getEmptyRelation($id);
		if(!empty($akses) AND $rel==true){
			return true;
		}else{
			return false;
		}
	}
	
	
	public function getEmptyRelation($id)
	{
		$count=0;
		$count+=DaftarPiutangPelanggan::model()->count(array('condition'=>"id_piutang_pelanggan='$id'"));
		$count+=PembayaranPiutangPelanggan::model()->count(array('condition'=>"id_piutang_pelanggan='$id'"));
										if($count>0){
			return false;
		}else{
			return true;
		}
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return PiutangPelanggan the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
