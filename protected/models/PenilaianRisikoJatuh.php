<?php

/**
 * This is the model class for table "penilaian_risiko_jatuh".
 *
 * The followings are the available columns in table 'penilaian_risiko_jatuh':
 * @property string $id_risiko_jatuh
 * @property string $id_registrasi
 * @property integer $risiko_i
 * @property integer $risiko_ii
 * @property integer $risiko_iii
 * @property integer $risiko_iv
 * @property integer $risiko_v
 * @property integer $risiko_vi
 * @property string $user_create
 * @property string $user_update
 * @property string $tgl_create
 * @property string $tgl_update
 * @property string $nama_penilai
 * @property string $tanggal_perawatan
 */
class PenilaianRisikoJatuh extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'penilaian_risiko_jatuh';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('id_risiko_jatuh', 'required'),
			array('risiko_i, risiko_ii, risiko_iii, risiko_iv, risiko_v, risiko_vi', 'numerical', 'integerOnly'=>true),
			array('id_risiko_jatuh, id_registrasi, user_create, user_update', 'length', 'max'=>20),
			array('nama_penilai', 'length', 'max'=>35),
			array('tgl_create, tgl_update, tanggal_perawatan', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id_risiko_jatuh, id_registrasi, risiko_i, risiko_ii, risiko_iii, risiko_iv, risiko_v, risiko_vi, user_create, user_update, tgl_create, tgl_update, nama_penilai, tanggal_perawatan', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id_risiko_jatuh' => 'Id Risiko Jatuh',
			'id_registrasi' => 'Id Registrasi',
			'risiko_i' => 'Risiko I',
			'risiko_ii' => 'Risiko Ii',
			'risiko_iii' => 'Risiko Iii',
			'risiko_iv' => 'Risiko Iv',
			'risiko_v' => 'Risiko V',
			'risiko_vi' => 'Risiko Vi',
			'user_create' => 'User Create',
			'user_update' => 'User Update',
			'tgl_create' => 'Tgl Create',
			'tgl_update' => 'Tgl Update',
			'nama_penilai' => 'Nama Penilai',
			'tanggal_perawatan' => 'Tanggal Perawatan',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id_risiko_jatuh',$this->id_risiko_jatuh,true);
		$criteria->compare('id_registrasi',$this->id_registrasi,true);
		$criteria->compare('risiko_i',$this->risiko_i);
		$criteria->compare('risiko_ii',$this->risiko_ii);
		$criteria->compare('risiko_iii',$this->risiko_iii);
		$criteria->compare('risiko_iv',$this->risiko_iv);
		$criteria->compare('risiko_v',$this->risiko_v);
		$criteria->compare('risiko_vi',$this->risiko_vi);
		$criteria->compare('user_create',$this->user_create,true);
		$criteria->compare('user_update',$this->user_update,true);
		$criteria->compare('tgl_create',$this->tgl_create,true);
		$criteria->compare('tgl_update',$this->tgl_update,true);
		$criteria->compare('nama_penilai',$this->nama_penilai,true);
		$criteria->compare('tanggal_perawatan',$this->tanggal_perawatan,true);

		return new CActiveDataProvider($this, array(
			'pagination'=>array(
				'pageSize'=> Yii::app()->user->getState('pageSize',Yii::app()->params['defaultPageSize']),
			),
			'criteria'=>$criteria,
		));
	}
	
	public function getAllowView()
	{
		$akses = UserAccess::model()->find(array("with"=>array("access"),'condition'=>"id_user='".Yii::app()->user->id."' and access.access_controller='PenilaianRisikoJatuh' and access.access_action='view'"));
		if(!empty($akses)) return true; else return false;
	}
	
	public function getAllowUpdate()
	{
		$akses = UserAccess::model()->find(array("with"=>array("access"),'condition'=>"id_user='".Yii::app()->user->id."' and access.access_controller='PenilaianRisikoJatuh' and access.access_action='update'"));
		if(!empty($akses)) return true; else return false;
	}
	
	public function getAllowDelete($id)
	{
		$akses = UserAccess::model()->find(array("with"=>array("access"),'condition'=>"id_user='".Yii::app()->user->id."' and access.access_controller='PenilaianRisikoJatuh' and access.access_action='delete'"));
		$rel=$this->getEmptyRelation($id);
		if(!empty($akses) AND $rel==true){
			return true;
		}else{
			return false;
		}
	}
	
	
	public function getEmptyRelation($id)
	{
		$count=0;
		if($count>0){
			return false;
		}else{
			return true;
		}
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return PenilaianRisikoJatuh the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
