<?php

/**
 * This is the model class for table "kontak_personal".
 *
 * The followings are the available columns in table 'kontak_personal':
 * @property string $id_kontak_personal
 * @property string $id_jenis_kontak
 * @property string $id_personal
 * @property string $kontak
 * @property string $status_aktif
 *
 * The followings are the available model relations:
 * @property JenisKontak $idJenisKontak
 * @property Personal $idPersonal
 */
class KontakPersonal extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'kontak_personal';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('id_jenis_kontak, id_personal, kontak', 'required'),
			array('id_jenis_kontak, id_personal', 'length', 'max'=>20),
			array('status_aktif', 'length', 'max'=>1),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id_kontak_personal, id_jenis_kontak, id_personal, kontak, status_aktif', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'idJenisKontak' => array(self::BELONGS_TO, 'JenisKontak', 'id_jenis_kontak'),
			'idPersonal' => array(self::BELONGS_TO, 'Personal', 'id_personal'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id_kontak_personal' => 'Id Kontak Personal',
			'id_jenis_kontak' => 'Id Jenis Kontak',
			'id_personal' => 'Id Personal',
			'kontak' => 'Kontak',
			'status_aktif' => 'Status Aktif',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id_kontak_personal',$this->id_kontak_personal,true);
		$criteria->compare('id_jenis_kontak',$this->id_jenis_kontak,true);
		$criteria->compare('id_personal',$this->id_personal,true);
		$criteria->compare('kontak',$this->kontak,true);
		$criteria->compare('status_aktif',$this->status_aktif,true);

		return new CActiveDataProvider($this, array(
			'pagination'=>array(
				'pageSize'=> Yii::app()->user->getState('pageSize',Yii::app()->params['defaultPageSize']),
			),
			'criteria'=>$criteria,
		));
	}
	
	public function getAllowView()
	{
		$akses = UserAccess::model()->find(array("with"=>array("access"),'condition'=>"id_user='".Yii::app()->user->id."' and access.access_controller='KontakPersonal' and access.access_action='view'"));
		if(!empty($akses)) return true; else return false;
	}
	
	public function getAllowUpdate()
	{
		$akses = UserAccess::model()->find(array("with"=>array("access"),'condition'=>"id_user='".Yii::app()->user->id."' and access.access_controller='KontakPersonal' and access.access_action='update'"));
		if(!empty($akses)) return true; else return false;
	}
	
	public function getAllowDelete($id)
	{
		$akses = UserAccess::model()->find(array("with"=>array("access"),'condition'=>"id_user='".Yii::app()->user->id."' and access.access_controller='KontakPersonal' and access.access_action='delete'"));
		$rel=$this->getEmptyRelation($id);
		if(!empty($akses) AND $rel==true){
			return true;
		}else{
			return false;
		}
	}
	
	
	public function getEmptyRelation($id)
	{
		$count=0;
						if($count>0){
			return false;
		}else{
			return true;
		}
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return KontakPersonal the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
