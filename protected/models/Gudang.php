<?php

/**
 * This is the model class for table "gudang".
 *
 * The followings are the available columns in table 'gudang':
 * @property string $id_gudang
 * @property string $nama_gudang
 * @property string $status
 * @property string $status_penerimaan
 *
 * The followings are the available model relations:
 * @property ItemGudang[] $itemGudangs
 * @property ItemPasien[] $itemPasiens
 * @property ItemPasienGroup[] $itemPasienGroups
 * @property ItemTransaksi[] $itemTransaksis
 * @property ItemTransaksi[] $itemTransaksis1
 * @property UserGudang[] $userGudangs
 */
class Gudang extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'gudang';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('nama_gudang, status, status_penerimaan', 'required'),
			array('nama_gudang', 'length', 'max'=>100),
			array('status', 'length', 'max'=>11),
			array('status_penerimaan', 'length', 'max'=>1),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id_gudang, nama_gudang, status, status_penerimaan', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'itemGudangs' => array(self::HAS_MANY, 'ItemGudang', 'id_gudang'),
			'itemPasiens' => array(self::HAS_MANY, 'ItemPasien', 'id_gudang'),
			'itemPasienGroups' => array(self::HAS_MANY, 'ItemPasienGroup', 'id_gudang'),
			'itemTransaksis' => array(self::HAS_MANY, 'ItemTransaksi', 'gudang_asal'),
			'itemTransaksis1' => array(self::HAS_MANY, 'ItemTransaksi', 'gudang_tujuan'),
			'userGudangs' => array(self::HAS_MANY, 'UserGudang', 'id_gudang'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id_gudang' => 'Id Gudang',
			'nama_gudang' => 'Nama Gudang',
			'status' => 'Status',
			'status_penerimaan' => 'Status Penerimaan',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id_gudang',$this->id_gudang,true);
		$criteria->compare('nama_gudang',$this->nama_gudang,true);
		$criteria->compare('status',$this->status,true);
		$criteria->compare('status_penerimaan',$this->status_penerimaan,true);

		return new CActiveDataProvider($this, array(
			'pagination'=>array(
				'pageSize'=> Yii::app()->user->getState('pageSize',Yii::app()->params['defaultPageSize']),
			),
			'criteria'=>$criteria,
		));
	}
	
	public function getAllowView()
	{
		$akses = UserAccess::model()->find(array("with"=>array("access"),'condition'=>"id_user='".Yii::app()->user->id."' and access.access_controller='Gudang' and access.access_action='view'"));
		if(!empty($akses)) return true; else return false;
	}
	
	public function getAllowUpdate()
	{
		$akses = UserAccess::model()->find(array("with"=>array("access"),'condition'=>"id_user='".Yii::app()->user->id."' and access.access_controller='Gudang' and access.access_action='update'"));
		if(!empty($akses)) return true; else return false;
	}
	
	public function getAllowDelete($id)
	{
		$akses = UserAccess::model()->find(array("with"=>array("access"),'condition'=>"id_user='".Yii::app()->user->id."' and access.access_controller='Gudang' and access.access_action='delete'"));
		$rel=$this->getEmptyRelation($id);
		if(!empty($akses) AND $rel==true){
			return true;
		}else{
			return false;
		}
	}
	
	
	public function getEmptyRelation($id)
	{
		$count=0;
		$count+=ItemGudang::model()->count(array('condition'=>"id_gudang='$id'"));
		$count+=ItemTransaksi::model()->count(array('condition'=>"gudang_asal='$id'"));
		$count+=ItemTransaksi::model()->count(array('condition'=>"gudang_tujuan='$id'"));
		$count+=UserGudang::model()->count(array('condition'=>"id_gudang='$id'"));
		if($count>0){
			return false;
		}else{
			return true;
		}
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Gudang the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
