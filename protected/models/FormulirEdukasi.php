<?php

/**
 * This is the model class for table "formulir_edukasi".
 *
 * The followings are the available columns in table 'formulir_edukasi':
 * @property string $id_formulir_edukasi
 * @property string $id_registrasi
 * @property string $bahasa
 * @property string $kebutuhan_penerjemah
 * @property string $pendidikan
 * @property string $baca_tulis
 * @property string $tipe_pembelajaran
 * @property string $hambatan
 * @property string $kesediaan
 * @property string $kebutuhan_edukasi_dokter
 * @property string $respon_dokter
 * @property string $kebutuhan_edukasi_perawat
 * @property string $respon_perawat
 * @property string $kebutuhan_edukasi_farmasi
 * @property string $respon_farmasi
 * @property string $kebutuhan_edukasi_nutrisi
 * @property string $respon_nutrisi
 * @property string $kebutuhan_edukasi_rehabilitasi
 * @property string $respon_rehabilitasi
 * @property string $user_create
 * @property string $user_update
 * @property string $tgl_create
 * @property string $tgl_update
 */
class FormulirEdukasi extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'formulir_edukasi';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('id_formulir_edukasi', 'required'),
			array('id_formulir_edukasi, id_registrasi, user_create, user_update', 'length', 'max'=>20),
			array('bahasa, kebutuhan_penerjemah, pendidikan, baca_tulis, tipe_pembelajaran, hambatan, kesediaan, respon_dokter, respon_perawat, respon_farmasi, respon_nutrisi, respon_rehabilitasi', 'length', 'max'=>90),
			array('kebutuhan_edukasi_dokter, kebutuhan_edukasi_perawat, kebutuhan_edukasi_farmasi, kebutuhan_edukasi_nutrisi, kebutuhan_edukasi_rehabilitasi, tgl_create, tgl_update', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id_formulir_edukasi, id_registrasi, bahasa, kebutuhan_penerjemah, pendidikan, baca_tulis, tipe_pembelajaran, hambatan, kesediaan, kebutuhan_edukasi_dokter, respon_dokter, kebutuhan_edukasi_perawat, respon_perawat, kebutuhan_edukasi_farmasi, respon_farmasi, kebutuhan_edukasi_nutrisi, respon_nutrisi, kebutuhan_edukasi_rehabilitasi, respon_rehabilitasi, user_create, user_update, tgl_create, tgl_update', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id_formulir_edukasi' => 'Id Formulir Edukasi',
			'id_registrasi' => 'Id Registrasi',
			'bahasa' => 'Bahasa',
			'kebutuhan_penerjemah' => 'Kebutuhan Penerjemah',
			'pendidikan' => 'Pendidikan',
			'baca_tulis' => 'Baca Tulis',
			'tipe_pembelajaran' => 'Tipe Pembelajaran',
			'hambatan' => 'Hambatan',
			'kesediaan' => 'Kesediaan',
			'kebutuhan_edukasi_dokter' => 'Kebutuhan Edukasi Dokter',
			'respon_dokter' => 'Respon Dokter',
			'kebutuhan_edukasi_perawat' => 'Kebutuhan Edukasi Perawat',
			'respon_perawat' => 'Respon Perawat',
			'kebutuhan_edukasi_farmasi' => 'Kebutuhan Edukasi Farmasi',
			'respon_farmasi' => 'Respon Farmasi',
			'kebutuhan_edukasi_nutrisi' => 'Kebutuhan Edukasi Nutrisi',
			'respon_nutrisi' => 'Respon Nutrisi',
			'kebutuhan_edukasi_rehabilitasi' => 'Kebutuhan Edukasi Rehabilitasi',
			'respon_rehabilitasi' => 'Respon Rehabilitasi',
			'user_create' => 'User Create',
			'user_update' => 'User Update',
			'tgl_create' => 'Tgl Create',
			'tgl_update' => 'Tgl Update',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id_formulir_edukasi',$this->id_formulir_edukasi,true);
		$criteria->compare('id_registrasi',$this->id_registrasi,true);
		$criteria->compare('bahasa',$this->bahasa,true);
		$criteria->compare('kebutuhan_penerjemah',$this->kebutuhan_penerjemah,true);
		$criteria->compare('pendidikan',$this->pendidikan,true);
		$criteria->compare('baca_tulis',$this->baca_tulis,true);
		$criteria->compare('tipe_pembelajaran',$this->tipe_pembelajaran,true);
		$criteria->compare('hambatan',$this->hambatan,true);
		$criteria->compare('kesediaan',$this->kesediaan,true);
		$criteria->compare('kebutuhan_edukasi_dokter',$this->kebutuhan_edukasi_dokter,true);
		$criteria->compare('respon_dokter',$this->respon_dokter,true);
		$criteria->compare('kebutuhan_edukasi_perawat',$this->kebutuhan_edukasi_perawat,true);
		$criteria->compare('respon_perawat',$this->respon_perawat,true);
		$criteria->compare('kebutuhan_edukasi_farmasi',$this->kebutuhan_edukasi_farmasi,true);
		$criteria->compare('respon_farmasi',$this->respon_farmasi,true);
		$criteria->compare('kebutuhan_edukasi_nutrisi',$this->kebutuhan_edukasi_nutrisi,true);
		$criteria->compare('respon_nutrisi',$this->respon_nutrisi,true);
		$criteria->compare('kebutuhan_edukasi_rehabilitasi',$this->kebutuhan_edukasi_rehabilitasi,true);
		$criteria->compare('respon_rehabilitasi',$this->respon_rehabilitasi,true);
		$criteria->compare('user_create',$this->user_create,true);
		$criteria->compare('user_update',$this->user_update,true);
		$criteria->compare('tgl_create',$this->tgl_create,true);
		$criteria->compare('tgl_update',$this->tgl_update,true);

		return new CActiveDataProvider($this, array(
			'pagination'=>array(
				'pageSize'=> Yii::app()->user->getState('pageSize',Yii::app()->params['defaultPageSize']),
			),
			'criteria'=>$criteria,
		));
	}
	
	public function getAllowView()
	{
		$akses = UserAccess::model()->find(array("with"=>array("access"),'condition'=>"id_user='".Yii::app()->user->id."' and access.access_controller='FormulirEdukasi' and access.access_action='view'"));
		if(!empty($akses)) return true; else return false;
	}
	
	public function getAllowUpdate()
	{
		$akses = UserAccess::model()->find(array("with"=>array("access"),'condition'=>"id_user='".Yii::app()->user->id."' and access.access_controller='FormulirEdukasi' and access.access_action='update'"));
		if(!empty($akses)) return true; else return false;
	}
	
	public function getAllowDelete($id)
	{
		$akses = UserAccess::model()->find(array("with"=>array("access"),'condition'=>"id_user='".Yii::app()->user->id."' and access.access_controller='FormulirEdukasi' and access.access_action='delete'"));
		$rel=$this->getEmptyRelation($id);
		if(!empty($akses) AND $rel==true){
			return true;
		}else{
			return false;
		}
	}
	
	
	public function getEmptyRelation($id)
	{
		$count=0;
		if($count>0){
			return false;
		}else{
			return true;
		}
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return FormulirEdukasi the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
