<?php

/**
 * This is the model class for table "pasien_penjamin".
 *
 * The followings are the available columns in table 'pasien_penjamin':
 * @property string $id_pasien_penjamin
 * @property string $id_pasien
 * @property string $id_penjamin
 * @property string $no_kartu
 * @property string $fasilitas_jaminan
 * @property string $nama_pemegang_kartu
 * @property string $id_jenis_hubungan
 *
 * The followings are the available model relations:
 * @property JenisHubungan $idJenisHubungan
 * @property Pasien $idPasien
 * @property Penjamin $idPenjamin
 */
class PasienPenjamin extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'pasien_penjamin';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('id_pasien, id_penjamin, id_jenis_hubungan', 'required'),
			array('id_pasien, id_penjamin, id_jenis_hubungan', 'length', 'max'=>20),
			array('no_kartu', 'length', 'max'=>45),
			array('fasilitas_jaminan', 'length', 'max'=>50),
			array('nama_pemegang_kartu', 'length', 'max'=>60),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id_pasien_penjamin, id_pasien, id_penjamin, no_kartu, fasilitas_jaminan, nama_pemegang_kartu, id_jenis_hubungan', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'idJenisHubungan' => array(self::BELONGS_TO, 'JenisHubungan', 'id_jenis_hubungan'),
			'idPasien' => array(self::BELONGS_TO, 'Pasien', 'id_pasien'),
			'idPenjamin' => array(self::BELONGS_TO, 'Penjamin', 'id_penjamin'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id_pasien_penjamin' => 'Id Pasien Penjamin',
			'id_pasien' => 'Id Pasien',
			'id_penjamin' => 'Id Penjamin',
			'no_kartu' => 'No Kartu',
			'fasilitas_jaminan' => 'Fasilitas Jaminan',
			'nama_pemegang_kartu' => 'Nama Pemegang Kartu',
			'id_jenis_hubungan' => 'Id Jenis Hubungan',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id_pasien_penjamin',$this->id_pasien_penjamin,true);
		$criteria->compare('id_pasien',$this->id_pasien,true);
		$criteria->compare('id_penjamin',$this->id_penjamin,true);
		$criteria->compare('no_kartu',$this->no_kartu,true);
		$criteria->compare('fasilitas_jaminan',$this->fasilitas_jaminan,true);
		$criteria->compare('nama_pemegang_kartu',$this->nama_pemegang_kartu,true);
		$criteria->compare('id_jenis_hubungan',$this->id_jenis_hubungan,true);

		return new CActiveDataProvider($this, array(
			'pagination'=>array(
				'pageSize'=> Yii::app()->user->getState('pageSize',Yii::app()->params['defaultPageSize']),
			),
			'criteria'=>$criteria,
		));
	}
	
	public function getAllowView()
	{
		$akses = UserAccess::model()->find(array("with"=>array("access"),'condition'=>"id_user='".Yii::app()->user->id."' and access.access_controller='PasienPenjamin' and access.access_action='view'"));
		if(!empty($akses)) return true; else return false;
	}
	
	public function getAllowUpdate()
	{
		$akses = UserAccess::model()->find(array("with"=>array("access"),'condition'=>"id_user='".Yii::app()->user->id."' and access.access_controller='PasienPenjamin' and access.access_action='update'"));
		if(!empty($akses)) return true; else return false;
	}
	
	public function getAllowDelete($id)
	{
		$akses = UserAccess::model()->find(array("with"=>array("access"),'condition'=>"id_user='".Yii::app()->user->id."' and access.access_controller='PasienPenjamin' and access.access_action='delete'"));
		$rel=$this->getEmptyRelation($id);
		if(!empty($akses) AND $rel==true){
			return true;
		}else{
			return false;
		}
	}
	
	
	public function getEmptyRelation($id)
	{
		$count=0;
								if($count>0){
			return false;
		}else{
			return true;
		}
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return PasienPenjamin the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
