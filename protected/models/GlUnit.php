<?php

/**
 * This is the model class for table "gl_unit".
 *
 * The followings are the available columns in table 'gl_unit':
 * @property integer $gl_id_unit
 * @property string $nama_cost_unit
 * @property string $jenis
 *
 * The followings are the available model relations:
 * @property DaftarPengajuanPembayaranSupplier[] $daftarPengajuanPembayaranSuppliers
 * @property Departemen[] $departemens
 * @property GlTransaksiRinci[] $glTransaksiRincis
 * @property Kelas[] $kelases
 * @property KelompokTindakan[] $kelompokTindakans
 * @property Ruangan[] $ruangans
 */
class GlUnit extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'gl_unit';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('nama_cost_unit, jenis', 'required'),
			array('nama_cost_unit', 'length', 'max'=>100),
			array('jenis', 'length', 'max'=>13),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('gl_id_unit, nama_cost_unit, jenis', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'daftarPengajuanPembayaranSuppliers' => array(self::HAS_MANY, 'DaftarPengajuanPembayaranSupplier', 'gl_id_unit'),
			'departemens' => array(self::HAS_MANY, 'Departemen', 'id_gl_unit'),
			'glTransaksiRincis' => array(self::HAS_MANY, 'GlTransaksiRinci', 'gl_id_unit'),
			'kelases' => array(self::HAS_MANY, 'Kelas', 'id_gl_unit'),
			'kelompokTindakans' => array(self::HAS_MANY, 'KelompokTindakan', 'id_gl_unit'),
			'ruangans' => array(self::HAS_MANY, 'Ruangan', 'id_gl_unit'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'gl_id_unit' => 'Gl Id Unit',
			'nama_cost_unit' => 'Nama Cost Unit',
			'jenis' => 'Jenis',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('gl_id_unit',$this->gl_id_unit);
		$criteria->compare('nama_cost_unit',$this->nama_cost_unit,true);
		$criteria->compare('jenis',$this->jenis,true);

		return new CActiveDataProvider($this, array(
			'pagination'=>array(
				'pageSize'=> Yii::app()->user->getState('pageSize',Yii::app()->params['defaultPageSize']),
			),
			'criteria'=>$criteria,
		));
	}
	
	public function getAllowView()
	{
		$akses = UserAccess::model()->find(array("with"=>array("access"),'condition'=>"id_user='".Yii::app()->user->id."' and access.access_controller='GlUnit' and access.access_action='view'"));
		if(!empty($akses)) return true; else return false;
	}
	
	public function getAllowUpdate()
	{
		$akses = UserAccess::model()->find(array("with"=>array("access"),'condition'=>"id_user='".Yii::app()->user->id."' and access.access_controller='GlUnit' and access.access_action='update'"));
		if(!empty($akses)) return true; else return false;
	}
	
	public function getAllowDelete($id)
	{
		$akses = UserAccess::model()->find(array("with"=>array("access"),'condition'=>"id_user='".Yii::app()->user->id."' and access.access_controller='GlUnit' and access.access_action='delete'"));
		$rel=$this->getEmptyRelation($id);
		if(!empty($akses) AND $rel==true){
			return true;
		}else{
			return false;
		}
	}
	
	
	public function getEmptyRelation($id)
	{
		$count=0;
		$count+=DaftarPengajuanPembayaranSupplier::model()->count(array('condition'=>"gl_id_unit='$id'"));
		$count+=Departemen::model()->count(array('condition'=>"id_gl_unit='$id'"));
		$count+=GlTransaksiRinci::model()->count(array('condition'=>"gl_id_unit='$id'"));
		$count+=Kelas::model()->count(array('condition'=>"id_gl_unit='$id'"));
		$count+=KelompokTindakan::model()->count(array('condition'=>"id_gl_unit='$id'"));
		$count+=Ruangan::model()->count(array('condition'=>"id_gl_unit='$id'"));
		if($count>0){
			return false;
		}else{
			return true;
		}
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return GlUnit the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
