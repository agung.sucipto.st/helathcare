<?php

/**
 * This is the model class for table "purchase_order".
 *
 * The followings are the available columns in table 'purchase_order':
 * @property string $id_purchase_order
 * @property string $id_supplier
 * @property string $jenis_referensi
 * @property string $no_po
 * @property string $tanggal
 * @property string $jenis_po
 * @property integer $discount_final
 * @property string $ppn
 * @property string $status
 * @property string $keterangan
 * @property string $is_revisi
 * @property string $alasan_revisi
 * @property string $parent_id
 * @property string $user_create
 * @property string $user_update
 * @property string $time_create
 * @property string $time_update
 *
 * The followings are the available model relations:
 * @property ApprovalDokumen[] $approvalDokumens
 * @property DaftarPurchaseOrder[] $daftarPurchaseOrders
 * @property ItemTransaksi[] $itemTransaksis
 * @property Supplier $idSupplier
 * @property User $userCreate
 * @property User $userUpdate
 * @property PurchaseOrder $parent
 * @property PurchaseOrder[] $purchaseOrders
 */
class PurchaseOrder extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'purchase_order';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('id_supplier, jenis_referensi, no_po, tanggal, jenis_po, discount_final, ppn, status, keterangan, user_create, time_create', 'required'),
			array('discount_final', 'numerical', 'integerOnly'=>true),
			array('id_supplier, parent_id', 'length', 'max'=>20),
			array('jenis_referensi, jenis_po', 'length', 'max'=>8),
			array('no_po', 'length', 'max'=>100),
			array('ppn', 'length', 'max'=>30),
			array('status', 'length', 'max'=>19),
			array('is_revisi', 'length', 'max'=>1),
			array('user_create, user_update', 'length', 'max'=>11),
			array('alasan_revisi, time_update', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id_purchase_order, id_supplier, jenis_referensi, no_po, tanggal, jenis_po, discount_final, ppn, status, keterangan, is_revisi, alasan_revisi, parent_id, user_create, user_update, time_create, time_update', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'approvalDokumens' => array(self::HAS_MANY, 'ApprovalDokumen', 'id_purchase_order'),
			'daftarPurchaseOrders' => array(self::HAS_MANY, 'DaftarPurchaseOrder', 'id_purchase_order'),
			'itemTransaksis' => array(self::HAS_MANY, 'ItemTransaksi', 'id_purchase_order'),
			'idSupplier' => array(self::BELONGS_TO, 'Supplier', 'id_supplier'),
			'userCreate' => array(self::BELONGS_TO, 'User', 'user_create'),
			'userUpdate' => array(self::BELONGS_TO, 'User', 'user_update'),
			'parent' => array(self::BELONGS_TO, 'PurchaseOrder', 'parent_id'),
			'purchaseOrders' => array(self::HAS_MANY, 'PurchaseOrder', 'parent_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id_purchase_order' => 'Id Purchase Order',
			'id_supplier' => 'Id Supplier',
			'jenis_referensi' => 'Jenis Referensi',
			'no_po' => 'No Po',
			'tanggal' => 'Tanggal',
			'jenis_po' => 'Jenis Po',
			'discount_final' => 'Discount Final',
			'ppn' => 'Ppn',
			'status' => 'Status',
			'keterangan' => 'Keterangan',
			'is_revisi' => 'Is Revisi',
			'alasan_revisi' => 'Alasan Revisi',
			'parent_id' => 'Parent',
			'user_create' => 'User Create',
			'user_update' => 'User Update',
			'time_create' => 'Time Create',
			'time_update' => 'Time Update',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id_purchase_order',$this->id_purchase_order,true);
		$criteria->compare('id_supplier',$this->id_supplier,true);
		$criteria->compare('jenis_referensi',$this->jenis_referensi,true);
		$criteria->compare('no_po',$this->no_po,true);
		$criteria->compare('tanggal',$this->tanggal,true);
		$criteria->compare('jenis_po',$this->jenis_po,true);
		$criteria->compare('discount_final',$this->discount_final);
		$criteria->compare('ppn',$this->ppn,true);
		$criteria->compare('status',$this->status,true);
		$criteria->compare('keterangan',$this->keterangan,true);
		$criteria->compare('is_revisi',$this->is_revisi,true);
		$criteria->compare('alasan_revisi',$this->alasan_revisi,true);
		$criteria->compare('parent_id',$this->parent_id,true);
		$criteria->compare('user_create',$this->user_create,true);
		$criteria->compare('user_update',$this->user_update,true);
		$criteria->compare('time_create',$this->time_create,true);
		$criteria->compare('time_update',$this->time_update,true);

		return new CActiveDataProvider($this, array(
			'pagination'=>array(
				'pageSize'=> Yii::app()->user->getState('pageSize',Yii::app()->params['defaultPageSize']),
			),
			'criteria'=>$criteria,
		));
	}
	
	public function getAllowView()
	{
		$akses = UserAccess::model()->find(array("with"=>array("access"),'condition'=>"id_user='".Yii::app()->user->id."' and access.access_controller='PurchaseOrder' and access.access_action='view'"));
		if(!empty($akses)) return true; else return false;
	}
	
	public function getAllowUpdate()
	{
		$akses = UserAccess::model()->find(array("with"=>array("access"),'condition'=>"id_user='".Yii::app()->user->id."' and access.access_controller='PurchaseOrder' and access.access_action='update'"));
		if(!empty($akses)) return true; else return false;
	}
	
	public function getAllowDelete($id)
	{
		$akses = UserAccess::model()->find(array("with"=>array("access"),'condition'=>"id_user='".Yii::app()->user->id."' and access.access_controller='PurchaseOrder' and access.access_action='delete'"));
		$rel=$this->getEmptyRelation($id);
		if(!empty($akses) AND $rel==true){
			return true;
		}else{
			return false;
		}
	}
	
	
	public function getEmptyRelation($id)
	{
		$count=0;
		$count+=ApprovalDokumen::model()->count(array('condition'=>"id_purchase_order='$id'"));
		$count+=DaftarPurchaseOrder::model()->count(array('condition'=>"id_purchase_order='$id'"));
		$count+=ItemTransaksi::model()->count(array('condition'=>"id_purchase_order='$id'"));
										$count+=PurchaseOrder::model()->count(array('condition'=>"parent_id='$id'"));
		if($count>0){
			return false;
		}else{
			return true;
		}
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return PurchaseOrder the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
