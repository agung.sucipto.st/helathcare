<?php

/**
 * This is the model class for table "visite_dokter_pasien".
 *
 * The followings are the available columns in table 'visite_dokter_pasien':
 * @property string $id_visite_dokter_pasien
 * @property string $id_registrasi
 * @property string $id_kelas
 * @property string $id_tarif_visite_dokter
 * @property string $id_dokter
 * @property string $waktu_visite
 * @property string $jumlah
 * @property string $tarif
 * @property string $id_daftar_tagihan
 * @property string $user_create
 * @property string $user_update
 * @property string $user_final
 * @property string $time_create
 * @property string $time_update
 * @property string $time_final
 *
 * The followings are the available model relations:
 * @property DaftarJasaMedis[] $daftarJasaMedises
 * @property DaftarTagihan[] $daftarTagihans
 * @property DaftarTagihan $idDaftarTagihan
 * @property Kelas $idKelas
 * @property Pegawai $idDokter
 * @property Registrasi $idRegistrasi
 * @property TarifVisiteDokter $idTarifVisiteDokter
 * @property User $userCreate
 * @property User $userUpdate
 * @property User $userFinal
 */
class VisiteDokterPasien extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'visite_dokter_pasien';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('id_registrasi, id_kelas, id_tarif_visite_dokter, id_dokter, waktu_visite, tarif, user_create, time_create', 'required'),
			array('id_registrasi, id_kelas, id_tarif_visite_dokter, id_dokter, jumlah, id_daftar_tagihan, user_create, user_update, user_final', 'length', 'max'=>20),
			array('tarif', 'length', 'max'=>30),
			array('time_update, time_final', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id_visite_dokter_pasien, id_registrasi, id_kelas, id_tarif_visite_dokter, id_dokter, waktu_visite, jumlah, tarif, id_daftar_tagihan, user_create, user_update, user_final, time_create, time_update, time_final', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'daftarJasaMedises' => array(self::HAS_MANY, 'DaftarJasaMedis', 'id_visite_dokter_pasien'),
			'daftarTagihans' => array(self::HAS_MANY, 'DaftarTagihan', 'id_visite_dokter_pasien'),
			'idDaftarTagihan' => array(self::BELONGS_TO, 'DaftarTagihan', 'id_daftar_tagihan'),
			'idKelas' => array(self::BELONGS_TO, 'Kelas', 'id_kelas'),
			'idDokter' => array(self::BELONGS_TO, 'Pegawai', 'id_dokter'),
			'idRegistrasi' => array(self::BELONGS_TO, 'Registrasi', 'id_registrasi'),
			'idTarifVisiteDokter' => array(self::BELONGS_TO, 'TarifVisiteDokter', 'id_tarif_visite_dokter'),
			'userCreate' => array(self::BELONGS_TO, 'User', 'user_create'),
			'userUpdate' => array(self::BELONGS_TO, 'User', 'user_update'),
			'userFinal' => array(self::BELONGS_TO, 'User', 'user_final'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id_visite_dokter_pasien' => 'Id Visite Dokter Pasien',
			'id_registrasi' => 'Id Registrasi',
			'id_kelas' => 'Id Kelas',
			'id_tarif_visite_dokter' => 'Id Tarif Visite Dokter',
			'id_dokter' => 'Id Dokter',
			'waktu_visite' => 'Waktu Visite',
			'jumlah' => 'Jumlah',
			'share_dokter' => 'Share Dokter',
			'tarif' => 'Tarif',
			'id_daftar_tagihan' => 'Id Daftar Tagihan',
			'user_create' => 'User Create',
			'user_update' => 'User Update',
			'user_final' => 'User Final',
			'time_create' => 'Time Create',
			'time_update' => 'Time Update',
			'time_final' => 'Time Final',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id_visite_dokter_pasien',$this->id_visite_dokter_pasien,true);
		$criteria->compare('id_registrasi',$this->id_registrasi,true);
		$criteria->compare('id_kelas',$this->id_kelas,true);
		$criteria->compare('id_tarif_visite_dokter',$this->id_tarif_visite_dokter,true);
		$criteria->compare('id_dokter',$this->id_dokter,true);
		$criteria->compare('waktu_visite',$this->waktu_visite,true);
		$criteria->compare('jumlah',$this->jumlah,true);
		$criteria->compare('tarif',$this->tarif,true);
		$criteria->compare('id_daftar_tagihan',$this->id_daftar_tagihan,true);
		$criteria->compare('user_create',$this->user_create,true);
		$criteria->compare('user_update',$this->user_update,true);
		$criteria->compare('user_final',$this->user_final,true);
		$criteria->compare('time_create',$this->time_create,true);
		$criteria->compare('time_update',$this->time_update,true);
		$criteria->compare('time_final',$this->time_final,true);

		return new CActiveDataProvider($this, array(
			'pagination'=>array(
				'pageSize'=> Yii::app()->user->getState('pageSize',Yii::app()->params['defaultPageSize']),
			),
			'criteria'=>$criteria,
		));
	}
	
	public function getAllowView()
	{
		$akses = UserAccess::model()->find(array("with"=>array("access"),'condition'=>"id_user='".Yii::app()->user->id."' and access.access_controller='VisiteDokterPasien' and access.access_action='view'"));
		if(!empty($akses)) return true; else return false;
	}
	
	public function getAllowUpdate()
	{
		$akses = UserAccess::model()->find(array("with"=>array("access"),'condition'=>"id_user='".Yii::app()->user->id."' and access.access_controller='VisiteDokterPasien' and access.access_action='update'"));
		if(!empty($akses)) return true; else return false;
	}
	
	public function getAllowDelete($id)
	{
		$akses = UserAccess::model()->find(array("with"=>array("access"),'condition'=>"id_user='".Yii::app()->user->id."' and access.access_controller='VisiteDokterPasien' and access.access_action='delete'"));
		$rel=$this->getEmptyRelation($id);
		if(!empty($akses) AND $rel==true){
			return true;
		}else{
			return false;
		}
	}
	
	
	public function getEmptyRelation($id)
	{
		$count=0;
		$count+=DaftarJasaMedis::model()->count(array('condition'=>"id_visite_dokter_pasien='$id'"));
		$count+=DaftarTagihan::model()->count(array('condition'=>"id_visite_dokter_pasien='$id'"));
																		if($count>0){
			return false;
		}else{
			return true;
		}
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return VisiteDokterPasien the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
