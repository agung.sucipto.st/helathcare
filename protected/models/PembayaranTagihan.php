<?php

/**
 * This is the model class for table "pembayaran_tagihan".
 *
 * The followings are the available columns in table 'pembayaran_tagihan':
 * @property string $id_pembayaran_tagihan
 * @property string $no_pembayaran
 * @property string $id_tagihan_pasien
 * @property string $waktu_pembayaran
 * @property string $jenis_pembayaran
 * @property string $total_pembayaran
 * @property string $total_dibayarkan
 * @property string $total_dikembalikan
 * @property string $id_penjamin
 * @property string $id_personal
 * @property string $nomor_kartu_debit_kredit
 * @property string $nomor_batch
 * @property integer $tujuan_transfer
 * @property string $status_pembayaran
 * @property string $user_create
 * @property string $user_update
 * @property string $user_final
 * @property string $time_create
 * @property string $time_update
 * @property string $time_final
 *
 * The followings are the available model relations:
 * @property DaftarPiutangPerusahaan[] $daftarPiutangPerusahaans
 * @property Penjamin $idPenjamin
 * @property Personal $idPersonal
 * @property TagihanPasien $idTagihanPasien
 * @property User $userCreate
 * @property User $userUpdate
 * @property User $userFinal
 * @property BankAkun $tujuanTransfer
 */
class PembayaranTagihan extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	 
	public $inc;
	public function tableName()
	{
		return 'pembayaran_tagihan';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('no_pembayaran, id_tagihan_pasien, waktu_pembayaran, jenis_pembayaran, total_pembayaran, total_dibayarkan, status_pembayaran, user_create, time_create', 'required'),
			array('tujuan_transfer', 'numerical', 'integerOnly'=>true),
			array('no_pembayaran, jenis_pembayaran, total_pembayaran, total_dibayarkan, total_dikembalikan, nomor_kartu_debit_kredit, nomor_batch, status_pembayaran', 'length', 'max'=>45),
			array('id_tagihan_pasien, id_penjamin, id_personal, user_create, user_update, user_final', 'length', 'max'=>20),
			array('time_update, time_final', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id_pembayaran_tagihan, no_pembayaran, id_tagihan_pasien, waktu_pembayaran, jenis_pembayaran, total_pembayaran, total_dibayarkan, total_dikembalikan, id_penjamin, id_personal, nomor_kartu_debit_kredit, nomor_batch, tujuan_transfer, status_pembayaran, user_create, user_update, user_final, time_create, time_update, time_final', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'daftarPiutangPerusahaans' => array(self::HAS_MANY, 'DaftarPiutangPerusahaan', 'id_pembayaran_tagihan'),
			'idPenjamin' => array(self::BELONGS_TO, 'Penjamin', 'id_penjamin'),
			'idPersonal' => array(self::BELONGS_TO, 'Personal', 'id_personal'),
			'idTagihanPasien' => array(self::BELONGS_TO, 'TagihanPasien', 'id_tagihan_pasien'),
			'userCreate' => array(self::BELONGS_TO, 'User', 'user_create'),
			'userUpdate' => array(self::BELONGS_TO, 'User', 'user_update'),
			'userFinal' => array(self::BELONGS_TO, 'User', 'user_final'),
			'tujuanTransfer' => array(self::BELONGS_TO, 'BankAkun', 'tujuan_transfer'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id_pembayaran_tagihan' => 'Id Pembayaran Tagihan',
			'no_pembayaran' => 'No Pembayaran',
			'id_tagihan_pasien' => 'Id Tagihan Pasien',
			'waktu_pembayaran' => 'Waktu Pembayaran',
			'jenis_pembayaran' => 'Jenis Pembayaran',
			'total_pembayaran' => 'Total Pembayaran',
			'total_dibayarkan' => 'Total Dibayarkan',
			'total_dikembalikan' => 'Total Dikembalikan',
			'id_penjamin' => 'Id Penjamin',
			'id_personal' => 'Id Personal',
			'nomor_kartu_debit_kredit' => 'Nomor Kartu Debit Kredit',
			'nomor_batch' => 'Nomor Batch',
			'tujuan_transfer' => 'Tujuan Transfer',
			'status_pembayaran' => 'Status Pembayaran',
			'user_create' => 'User Create',
			'user_update' => 'User Update',
			'user_final' => 'User Final',
			'time_create' => 'Time Create',
			'time_update' => 'Time Update',
			'time_final' => 'Time Final',
		);
	}

	public static function getPayNo($type,$date){
		$criteria=new CDbCriteria;
		$criteria->select="RIGHT(no_pembayaran,4) as inc";
		$criteria->order="RIGHT(no_pembayaran,4) DESC";
		$criteria->limit=1;
		$criteria->condition="DATE(waktu_pembayaran)='$date' AND jenis_pembayaran='$type'";
		
		$last=PembayaranTagihan::model()->find($criteria);
		
		if(empty($last)){
			return $type.'-'.date("ymd").sprintf("%04s",1);
		}else{
			return $type.'-'.date("ymd").sprintf("%04s",$last->inc+1);
		}
	}
	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id_pembayaran_tagihan',$this->id_pembayaran_tagihan,true);
		$criteria->compare('no_pembayaran',$this->no_pembayaran,true);
		$criteria->compare('id_tagihan_pasien',$this->id_tagihan_pasien,true);
		$criteria->compare('waktu_pembayaran',$this->waktu_pembayaran,true);
		$criteria->compare('jenis_pembayaran',$this->jenis_pembayaran,true);
		$criteria->compare('total_pembayaran',$this->total_pembayaran,true);
		$criteria->compare('total_dibayarkan',$this->total_dibayarkan,true);
		$criteria->compare('total_dikembalikan',$this->total_dikembalikan,true);
		$criteria->compare('id_penjamin',$this->id_penjamin,true);
		$criteria->compare('id_personal',$this->id_personal,true);
		$criteria->compare('nomor_kartu_debit_kredit',$this->nomor_kartu_debit_kredit,true);
		$criteria->compare('nomor_batch',$this->nomor_batch,true);
		$criteria->compare('tujuan_transfer',$this->tujuan_transfer);
		$criteria->compare('status_pembayaran',$this->status_pembayaran,true);
		$criteria->compare('user_create',$this->user_create,true);
		$criteria->compare('user_update',$this->user_update,true);
		$criteria->compare('user_final',$this->user_final,true);
		$criteria->compare('time_create',$this->time_create,true);
		$criteria->compare('time_update',$this->time_update,true);
		$criteria->compare('time_final',$this->time_final,true);

		return new CActiveDataProvider($this, array(
			'pagination'=>array(
				'pageSize'=> Yii::app()->user->getState('pageSize',Yii::app()->params['defaultPageSize']),
			),
			'criteria'=>$criteria,
		));
	}
	
	public function getAllowView()
	{
		$akses = UserAccess::model()->find(array("with"=>array("access"),'condition'=>"id_user='".Yii::app()->user->id."' and access.access_controller='PembayaranTagihan' and access.access_action='view'"));
		if(!empty($akses)) return true; else return false;
	}
	
	public function getAllowUpdate()
	{
		$akses = UserAccess::model()->find(array("with"=>array("access"),'condition'=>"id_user='".Yii::app()->user->id."' and access.access_controller='PembayaranTagihan' and access.access_action='update'"));
		if(!empty($akses)) return true; else return false;
	}
	
	public function getAllowDelete($id)
	{
		$akses = UserAccess::model()->find(array("with"=>array("access"),'condition'=>"id_user='".Yii::app()->user->id."' and access.access_controller='PembayaranTagihan' and access.access_action='delete'"));
		$rel=$this->getEmptyRelation($id);
		if(!empty($akses) AND $rel==true){
			return true;
		}else{
			return false;
		}
	}
	
	
	public function getEmptyRelation($id)
	{
		$count=0;
		$count+=DaftarPiutangPerusahaan::model()->count(array('condition'=>"id_pembayaran_tagihan='$id'"));
																if($count>0){
			return false;
		}else{
			return true;
		}
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return PembayaranTagihan the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
