<?php

/**
 * This is the model class for table "purchase_request".
 *
 * The followings are the available columns in table 'purchase_request':
 * @property string $id_purchase_request
 * @property string $no_pr
 * @property string $tanggal
 * @property string $jenis_pr
 * @property string $status
 * @property string $keterangan
 * @property string $is_revisi
 * @property string $alasan_revisi
 * @property string $parent_id
 * @property string $user_create
 * @property string $user_update
 * @property string $time_create
 * @property string $time_update
 *
 * The followings are the available model relations:
 * @property ApprovalDokumen[] $approvalDokumens
 * @property DaftarPurchaseRequest[] $daftarPurchaseRequests
 * @property User $userCreate
 * @property User $userUpdate
 * @property PurchaseRequest $parent
 * @property PurchaseRequest[] $purchaseRequests
 */
class PurchaseRequest extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'purchase_request';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('no_pr, tanggal, jenis_pr, status, keterangan, user_create, time_create', 'required'),
			array('no_pr', 'length', 'max'=>100),
			array('jenis_pr', 'length', 'max'=>8),
			array('status', 'length', 'max'=>10),
			array('is_revisi', 'length', 'max'=>1),
			array('parent_id', 'length', 'max'=>20),
			array('user_create, user_update', 'length', 'max'=>11),
			array('alasan_revisi, time_update', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id_purchase_request, no_pr, tanggal, jenis_pr, status, keterangan, is_revisi, alasan_revisi, parent_id, user_create, user_update, time_create, time_update', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'approvalDokumens' => array(self::HAS_MANY, 'ApprovalDokumen', 'id_purchase_request'),
			'daftarPurchaseRequests' => array(self::HAS_MANY, 'DaftarPurchaseRequest', 'id_purchase_request'),
			'userCreate' => array(self::BELONGS_TO, 'User', 'user_create'),
			'userUpdate' => array(self::BELONGS_TO, 'User', 'user_update'),
			'parent' => array(self::BELONGS_TO, 'PurchaseRequest', 'parent_id'),
			'purchaseRequests' => array(self::HAS_MANY, 'PurchaseRequest', 'parent_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id_purchase_request' => 'Id Purchase Request',
			'no_pr' => 'No Pr',
			'tanggal' => 'Tanggal',
			'jenis_pr' => 'Jenis Pr',
			'status' => 'Status',
			'keterangan' => 'Keterangan',
			'is_revisi' => 'Is Revisi',
			'alasan_revisi' => 'Alasan Revisi',
			'parent_id' => 'Parent',
			'user_create' => 'User Create',
			'user_update' => 'User Update',
			'time_create' => 'Time Create',
			'time_update' => 'Time Update',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id_purchase_request',$this->id_purchase_request,true);
		$criteria->compare('no_pr',$this->no_pr,true);
		$criteria->compare('tanggal',$this->tanggal,true);
		$criteria->compare('jenis_pr',$this->jenis_pr,true);
		$criteria->compare('status',$this->status,true);
		$criteria->compare('keterangan',$this->keterangan,true);
		$criteria->compare('is_revisi',$this->is_revisi,true);
		$criteria->compare('alasan_revisi',$this->alasan_revisi,true);
		$criteria->compare('parent_id',$this->parent_id,true);
		$criteria->compare('user_create',$this->user_create,true);
		$criteria->compare('user_update',$this->user_update,true);
		$criteria->compare('time_create',$this->time_create,true);
		$criteria->compare('time_update',$this->time_update,true);

		return new CActiveDataProvider($this, array(
			'pagination'=>array(
				'pageSize'=> Yii::app()->user->getState('pageSize',Yii::app()->params['defaultPageSize']),
			),
			'criteria'=>$criteria,
		));
	}
	
	public function getAllowView()
	{
		$akses = UserAccess::model()->find(array("with"=>array("access"),'condition'=>"id_user='".Yii::app()->user->id."' and access.access_controller='PurchaseRequest' and access.access_action='view'"));
		if(!empty($akses)) return true; else return false;
	}
	
	public function getAllowUpdate()
	{
		$akses = UserAccess::model()->find(array("with"=>array("access"),'condition'=>"id_user='".Yii::app()->user->id."' and access.access_controller='PurchaseRequest' and access.access_action='update'"));
		if(!empty($akses)) return true; else return false;
	}
	
	public function getAllowDelete($id)
	{
		$akses = UserAccess::model()->find(array("with"=>array("access"),'condition'=>"id_user='".Yii::app()->user->id."' and access.access_controller='PurchaseRequest' and access.access_action='delete'"));
		$rel=$this->getEmptyRelation($id);
		if(!empty($akses) AND $rel==true){
			return true;
		}else{
			return false;
		}
	}
	
	
	public function getEmptyRelation($id)
	{
		$count=0;
		$count+=ApprovalDokumen::model()->count(array('condition'=>"id_purchase_request='$id'"));
		$count+=DaftarPurchaseRequest::model()->count(array('condition'=>"id_purchase_request='$id'"));
								$count+=PurchaseRequest::model()->count(array('condition'=>"parent_id='$id'"));
		if($count>0){
			return false;
		}else{
			return true;
		}
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return PurchaseRequest the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
