<?php

/**
 * This is the model class for table "kelompok_tarif".
 *
 * The followings are the available columns in table 'kelompok_tarif':
 * @property string $id_kelompok_tarif
 * @property string $nama_kelompok_tarif
 * @property string $status
 *
 * The followings are the available model relations:
 * @property TarifAdministrasi[] $tarifAdministrasis
 * @property TarifItem[] $tarifItems
 * @property TarifLaboratorium[] $tarifLaboratoria
 * @property TarifPeralatan[] $tarifPeralatans
 * @property TarifRadiology[] $tarifRadiologies
 * @property TarifRuangRawatan[] $tarifRuangRawatans
 * @property TarifRuangan[] $tarifRuangans
 * @property TarifTindakan[] $tarifTindakans
 * @property TarifVisiteDokter[] $tarifVisiteDokters
 */
class KelompokTarif extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'kelompok_tarif';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('nama_kelompok_tarif, status', 'required'),
			array('nama_kelompok_tarif, status', 'length', 'max'=>45),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id_kelompok_tarif, nama_kelompok_tarif, status', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'tarifAdministrasis' => array(self::HAS_MANY, 'TarifAdministrasi', 'id_kelompok_tarif'),
			'tarifItems' => array(self::HAS_MANY, 'TarifItem', 'id_kelompok_tarif'),
			'tarifLaboratoria' => array(self::HAS_MANY, 'TarifLaboratorium', 'id_kelompok_tarif'),
			'tarifPeralatans' => array(self::HAS_MANY, 'TarifPeralatan', 'id_kelompok_tarif'),
			'tarifRadiologies' => array(self::HAS_MANY, 'TarifRadiology', 'id_kelompok_tarif'),
			'tarifRuangRawatans' => array(self::HAS_MANY, 'TarifRuangRawatan', 'id_kelompok_tarif'),
			'tarifRuangans' => array(self::HAS_MANY, 'TarifRuangan', 'id_kelompok_tarif'),
			'tarifTindakans' => array(self::HAS_MANY, 'TarifTindakan', 'id_kelompok_tarif'),
			'tarifVisiteDokters' => array(self::HAS_MANY, 'TarifVisiteDokter', 'id_kelompok_tarif'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id_kelompok_tarif' => 'Id Kelompok Tarif',
			'nama_kelompok_tarif' => 'Nama Kelompok Tarif',
			'status' => 'Status',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id_kelompok_tarif',$this->id_kelompok_tarif,true);
		$criteria->compare('nama_kelompok_tarif',$this->nama_kelompok_tarif,true);
		$criteria->compare('status',$this->status,true);

		return new CActiveDataProvider($this, array(
			'pagination'=>array(
				'pageSize'=> Yii::app()->user->getState('pageSize',Yii::app()->params['defaultPageSize']),
			),
			'criteria'=>$criteria,
		));
	}
	
	public function getAllowView()
	{
		$akses = UserAccess::model()->find(array("with"=>array("access"),'condition'=>"id_user='".Yii::app()->user->id."' and access.access_controller='KelompokTarif' and access.access_action='view'"));
		if(!empty($akses)) return true; else return false;
	}
	
	public function getAllowUpdate()
	{
		$akses = UserAccess::model()->find(array("with"=>array("access"),'condition'=>"id_user='".Yii::app()->user->id."' and access.access_controller='KelompokTarif' and access.access_action='update'"));
		if(!empty($akses)) return true; else return false;
	}
	
	public function getAllowDelete($id)
	{
		$akses = UserAccess::model()->find(array("with"=>array("access"),'condition'=>"id_user='".Yii::app()->user->id."' and access.access_controller='KelompokTarif' and access.access_action='delete'"));
		$rel=$this->getEmptyRelation($id);
		if(!empty($akses) AND $rel==true){
			return true;
		}else{
			return false;
		}
	}
	
	
	public function getEmptyRelation($id)
	{
		$count=0;
		$count+=TarifAdministrasi::model()->count(array('condition'=>"id_kelompok_tarif='$id'"));
		$count+=TarifItem::model()->count(array('condition'=>"id_kelompok_tarif='$id'"));
		$count+=TarifLaboratorium::model()->count(array('condition'=>"id_kelompok_tarif='$id'"));
		$count+=TarifPeralatan::model()->count(array('condition'=>"id_kelompok_tarif='$id'"));
		$count+=TarifRadiology::model()->count(array('condition'=>"id_kelompok_tarif='$id'"));
		$count+=TarifRuangRawatan::model()->count(array('condition'=>"id_kelompok_tarif='$id'"));
		$count+=TarifRuangan::model()->count(array('condition'=>"id_kelompok_tarif='$id'"));
		$count+=TarifTindakan::model()->count(array('condition'=>"id_kelompok_tarif='$id'"));
		$count+=TarifVisiteDokter::model()->count(array('condition'=>"id_kelompok_tarif='$id'"));
		if($count>0){
			return false;
		}else{
			return true;
		}
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return KelompokTarif the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
