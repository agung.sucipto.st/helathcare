<?php

/**
 * This is the model class for table "laboratorium_pasien".
 *
 * The followings are the available columns in table 'laboratorium_pasien':
 * @property string $id_laboratorium_pasien
 * @property string $id_registrasi
 * @property string $id_kelas
 * @property string $id_dokter_pengirim
 * @property string $id_dokter_penanggungjawab
 * @property string $no_order
 * @property string $waktu_order
 * @property string $catatan
 * @property string $status_order
 * @property string $user_create
 * @property string $user_update
 * @property string $user_approve
 * @property string $user_final
 * @property string $time_create
 * @property string $time_update
 * @property string $time_approve
 * @property string $time_final
 *
 * The followings are the available model relations:
 * @property Kelas $idKelas
 * @property Pegawai $idDokterPengirim
 * @property Pegawai $idDokterPenanggungjawab
 * @property Registrasi $idRegistrasi
 * @property User $userCreate
 * @property User $userUpdate
 * @property User $userApprove
 * @property User $userFinal
 * @property LaboratoriumPasienList[] $laboratoriumPasienLists
 * @property LaboratoriumPasienMedis[] $laboratoriumPasienMedises
 */
class LaboratoriumPasien extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'laboratorium_pasien';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('id_registrasi, id_kelas, id_dokter_pengirim, id_dokter_penanggungjawab, no_order, waktu_order, status_order, user_create, time_create', 'required'),
			array('id_registrasi, id_kelas, id_dokter_pengirim, id_dokter_penanggungjawab, user_create, user_update, user_approve, user_final', 'length', 'max'=>20),
			array('no_order, status_order', 'length', 'max'=>45),
			array('catatan, time_update, time_approve, time_final', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id_laboratorium_pasien, id_registrasi, id_kelas, id_dokter_pengirim, id_dokter_penanggungjawab, no_order, waktu_order, catatan, status_order, user_create, user_update, user_approve, user_final, time_create, time_update, time_approve, time_final', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'idKelas' => array(self::BELONGS_TO, 'Kelas', 'id_kelas'),
			'idDokterPengirim' => array(self::BELONGS_TO, 'Pegawai', 'id_dokter_pengirim'),
			'idDokterPenanggungjawab' => array(self::BELONGS_TO, 'Pegawai', 'id_dokter_penanggungjawab'),
			'idRegistrasi' => array(self::BELONGS_TO, 'Registrasi', 'id_registrasi'),
			'userCreate' => array(self::BELONGS_TO, 'User', 'user_create'),
			'userUpdate' => array(self::BELONGS_TO, 'User', 'user_update'),
			'userApprove' => array(self::BELONGS_TO, 'User', 'user_approve'),
			'userFinal' => array(self::BELONGS_TO, 'User', 'user_final'),
			'laboratoriumPasienLists' => array(self::HAS_MANY, 'LaboratoriumPasienList', 'id_laboratorium_pasien'),
			'laboratoriumPasienMedises' => array(self::HAS_MANY, 'LaboratoriumPasienMedis', 'id_laboratorium_pasien'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id_laboratorium_pasien' => 'Id Laboratorium Pasien',
			'id_registrasi' => 'Id Registrasi',
			'id_kelas' => 'Id Kelas',
			'id_dokter_pengirim' => 'Id Dokter Pengirim',
			'id_dokter_penanggungjawab' => 'Id Dokter Penanggungjawab',
			'no_order' => 'No Order',
			'waktu_order' => 'Waktu Order',
			'catatan' => 'Catatan',
			'status_order' => 'Status Order',
			'user_create' => 'User Create',
			'user_update' => 'User Update',
			'user_approve' => 'User Approve',
			'user_final' => 'User Final',
			'time_create' => 'Time Create',
			'time_update' => 'Time Update',
			'time_approve' => 'Time Approve',
			'time_final' => 'Time Final',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id_laboratorium_pasien',$this->id_laboratorium_pasien,true);
		$criteria->compare('id_registrasi',$this->id_registrasi,true);
		$criteria->compare('id_kelas',$this->id_kelas,true);
		$criteria->compare('id_dokter_pengirim',$this->id_dokter_pengirim,true);
		$criteria->compare('id_dokter_penanggungjawab',$this->id_dokter_penanggungjawab,true);
		$criteria->compare('no_order',$this->no_order,true);
		$criteria->compare('waktu_order',$this->waktu_order,true);
		$criteria->compare('catatan',$this->catatan,true);
		$criteria->compare('status_order',$this->status_order,true);
		$criteria->compare('user_create',$this->user_create,true);
		$criteria->compare('user_update',$this->user_update,true);
		$criteria->compare('user_approve',$this->user_approve,true);
		$criteria->compare('user_final',$this->user_final,true);
		$criteria->compare('time_create',$this->time_create,true);
		$criteria->compare('time_update',$this->time_update,true);
		$criteria->compare('time_approve',$this->time_approve,true);
		$criteria->compare('time_final',$this->time_final,true);

		return new CActiveDataProvider($this, array(
			'pagination'=>array(
				'pageSize'=> Yii::app()->user->getState('pageSize',Yii::app()->params['defaultPageSize']),
			),
			'criteria'=>$criteria,
		));
	}
	
	public function getAllowView()
	{
		$akses = UserAccess::model()->find(array("with"=>array("access"),'condition'=>"id_user='".Yii::app()->user->id."' and access.access_controller='LaboratoriumPasien' and access.access_action='view'"));
		if(!empty($akses)) return true; else return false;
	}
	
	public function getAllowUpdate()
	{
		$akses = UserAccess::model()->find(array("with"=>array("access"),'condition'=>"id_user='".Yii::app()->user->id."' and access.access_controller='LaboratoriumPasien' and access.access_action='update'"));
		if(!empty($akses)) return true; else return false;
	}
	
	public function getAllowDelete($id)
	{
		$akses = UserAccess::model()->find(array("with"=>array("access"),'condition'=>"id_user='".Yii::app()->user->id."' and access.access_controller='LaboratoriumPasien' and access.access_action='delete'"));
		$rel=$this->getEmptyRelation($id);
		if(!empty($akses) AND $rel==true){
			return true;
		}else{
			return false;
		}
	}
	
	
	public function getEmptyRelation($id)
	{
		$count=0;
																		$count+=LaboratoriumPasienList::model()->count(array('condition'=>"id_laboratorium_pasien='$id'"));
		$count+=LaboratoriumPasienMedis::model()->count(array('condition'=>"id_laboratorium_pasien='$id'"));
		if($count>0){
			return false;
		}else{
			return true;
		}
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return LaboratoriumPasien the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
