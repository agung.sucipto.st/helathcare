<?php

/**
 * This is the model class for table "daftar_tagihan".
 *
 * The followings are the available columns in table 'daftar_tagihan':
 * @property string $id_daftar_tagihan
 * @property string $id_tagihan
 * @property string $id_registrasi
 * @property string $id_penjamin
 * @property string $id_jenis_komponen_tagihan
 * @property string $id_tindakan_pasien
 * @property string $id_peralatan_pasien
 * @property string $id_item_pasien
 * @property string $id_visite_dokter_pasien
 * @property string $id_radiology_pasien_list
 * @property string $id_laboratorium_pasien_list
 * @property string $id_bed_pasien
 * @property string $id_ruangan_pasien
 * @property string $id_administrasi_pasien
 * @property string $deskripsi_komponen
 * @property string $waktu_daftar_tagihan
 * @property string $harga
 * @property string $jumlah
 * @property string $jenis_discount
 * @property string $discount
 * @property string $jumlah_bayar
 * @property string $jumlah_jaminan
 * @property string $tarif
 * @property string $tarif_orig
 * @property string $gabungkan
 * @property string $id_registrasi_gabung
 * @property string $konfirmasi
 * @property string $user_create
 * @property string $user_update
 * @property string $user_final
 * @property string $user_confirm
 * @property string $time_create
 * @property string $time_update
 * @property string $time_final
 * @property string $time_confirm
 *
 * The followings are the available model relations:
 * @property AdministrasiPasien[] $administrasiPasiens
 * @property DaftarJasaMedis[] $daftarJasaMedises
 * @property JenisKomponenTagihan $idJenisKomponenTagihan
 * @property User $userUpdate
 * @property User $userFinal
 * @property User $userCreate
 * @property AdministrasiPasien $idAdministrasiPasien
 * @property BedPasien $idBedPasien
 * @property ItemPasien $idItemPasien
 * @property LaboratoriumPasienList $idLaboratoriumPasienList
 * @property Penjamin $idPenjamin
 * @property PeralatanPasien $idPeralatanPasien
 * @property RadiologyPasienList $idRadiologyPasienList
 * @property RuanganPasien $idRuanganPasien
 * @property TagihanPasien $idTagihan
 * @property TindakanPasien $idTindakanPasien
 * @property User $userConfirm
 * @property VisiteDokterPasien $idVisiteDokterPasien
 * @property Registrasi $idRegistrasi
 * @property ItemPasien[] $itemPasiens
 * @property LaboratoriumPasienList[] $laboratoriumPasienLists
 * @property PeralatanPasien[] $peralatanPasiens
 * @property RadiologyPasienList[] $radiologyPasienLists
 * @property RuanganPasien[] $ruanganPasiens
 * @property TindakanPasien[] $tindakanPasiens
 * @property VisiteDokterPasien[] $visiteDokterPasiens
 */
class DaftarTagihan extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'daftar_tagihan';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('id_registrasi, deskripsi_komponen, waktu_daftar_tagihan, harga, jumlah, jumlah_bayar, tarif, tarif_orig, user_create, time_create', 'required'),
			array('id_daftar_tagihan, id_tagihan, id_registrasi, id_penjamin, id_jenis_komponen_tagihan, id_tindakan_pasien, id_peralatan_pasien, id_visite_dokter_pasien, id_radiology_pasien_list, id_laboratorium_pasien_list, id_bed_pasien, id_ruangan_pasien, id_administrasi_pasien, id_registrasi_gabung, user_create, user_update, user_final, user_confirm', 'length', 'max'=>20),
			array('harga, jumlah, jenis_discount, discount, jumlah_bayar, jumlah_jaminan, tarif, tarif_orig', 'length', 'max'=>30),
			array('gabungkan, konfirmasi', 'length', 'max'=>1),
			array('time_update, time_final, time_confirm', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id_daftar_tagihan, id_tagihan, id_registrasi, id_penjamin, id_jenis_komponen_tagihan, id_tindakan_pasien, id_peralatan_pasien, id_visite_dokter_pasien, id_radiology_pasien_list, id_laboratorium_pasien_list, id_bed_pasien, id_ruangan_pasien, id_administrasi_pasien, deskripsi_komponen, waktu_daftar_tagihan, harga, jumlah, jenis_discount, discount, jumlah_bayar, jumlah_jaminan, tarif, tarif_orig, gabungkan, id_registrasi_gabung, konfirmasi, user_create, user_update, user_final, user_confirm, time_create, time_update, time_final, time_confirm, id_daftar_item_transaksi', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'administrasiPasiens' => array(self::HAS_MANY, 'AdministrasiPasien', 'id_daftar_tagihan'),
			'daftarJasaMedises' => array(self::HAS_MANY, 'DaftarJasaMedis', 'id_daftar_tagihan'),
			'idJenisKomponenTagihan' => array(self::BELONGS_TO, 'JenisKomponenTagihan', 'id_jenis_komponen_tagihan'),
			'userUpdate' => array(self::BELONGS_TO, 'User', 'user_update'),
			'userFinal' => array(self::BELONGS_TO, 'User', 'user_final'),
			'userCreate' => array(self::BELONGS_TO, 'User', 'user_create'),
			'idAdministrasiPasien' => array(self::BELONGS_TO, 'AdministrasiPasien', 'id_administrasi_pasien'),
			'idBedPasien' => array(self::BELONGS_TO, 'BedPasien', 'id_bed_pasien'),
			'idLaboratoriumPasienList' => array(self::BELONGS_TO, 'LaboratoriumPasienList', 'id_laboratorium_pasien_list'),
			'idPenjamin' => array(self::BELONGS_TO, 'Penjamin', 'id_penjamin'),
			'idPeralatanPasien' => array(self::BELONGS_TO, 'PeralatanPasien', 'id_peralatan_pasien'),
			'idRadiologyPasienList' => array(self::BELONGS_TO, 'RadiologyPasienList', 'id_radiology_pasien_list'),
			'idRehabMedisPasienList' => array(self::BELONGS_TO, 'RehabMedisPasienList', 'id_rehab_medis_pasien_list'),
			'idHemodialisaPasienList' => array(self::BELONGS_TO, 'HemodialisaPasienList', 'id_hemodialisa_pasien_list'),
			'idRuanganPasien' => array(self::BELONGS_TO, 'RuanganPasien', 'id_ruangan_pasien'),
			'idTagihan' => array(self::BELONGS_TO, 'TagihanPasien', 'id_tagihan'),
			'idTindakanPasien' => array(self::BELONGS_TO, 'TindakanPasien', 'id_tindakan_pasien'),
			'userConfirm' => array(self::BELONGS_TO, 'User', 'user_confirm'),
			'idVisiteDokterPasien' => array(self::BELONGS_TO, 'VisiteDokterPasien', 'id_visite_dokter_pasien'),
			'idRegistrasi' => array(self::BELONGS_TO, 'Registrasi', 'id_registrasi'),
			'itemPasiens' => array(self::HAS_MANY, 'ItemPasien', 'id_daftar_tagihan'),
			'laboratoriumPasienLists' => array(self::HAS_MANY, 'LaboratoriumPasienList', 'id_daftar_tagihan'),
			'peralatanPasiens' => array(self::HAS_MANY, 'PeralatanPasien', 'id_daftar_tagihan'),
			'radiologyPasienLists' => array(self::HAS_MANY, 'RadiologyPasienList', 'id_daftar_tagihan'),
			'ruanganPasiens' => array(self::HAS_MANY, 'RuanganPasien', 'id_daftar_tagihan'),
			'tindakanPasiens' => array(self::HAS_MANY, 'TindakanPasien', 'id_daftar_tagihan'),
			'visiteDokterPasiens' => array(self::HAS_MANY, 'VisiteDokterPasien', 'id_daftar_tagihan'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id_daftar_tagihan' => 'Id Daftar Tagihan',
			'id_tagihan' => 'Id Tagihan',
			'id_registrasi' => 'Id Registrasi',
			'id_penjamin' => 'Id Penjamin',
			'id_jenis_komponen_tagihan' => 'Id Jenis Komponen Tagihan',
			'id_tindakan_pasien' => 'Id Tindakan Pasien',
			'id_peralatan_pasien' => 'Id Peralatan Pasien',
			'id_daftar_item_transaksi' => 'id_daftar_item_transaksi',
			'id_visite_dokter_pasien' => 'Id Visite Dokter Pasien',
			'id_radiology_pasien_list' => 'Id Radiology Pasien List',
			'id_laboratorium_pasien_list' => 'Id Laboratorium Pasien List',
			'id_bed_pasien' => 'Id Bed Pasien',
			'id_ruangan_pasien' => 'Id Ruangan Pasien',
			'id_administrasi_pasien' => 'Id Administrasi Pasien',
			'deskripsi_komponen' => 'Deskripsi Komponen',
			'waktu_daftar_tagihan' => 'Waktu Daftar Tagihan',
			'harga' => 'Harga',
			'jumlah' => 'Jumlah',
			'jenis_discount' => 'Jenis Discount',
			'discount' => 'Discount',
			'jumlah_bayar' => 'Jumlah Bayar',
			'jumlah_jaminan' => 'Jumlah Jaminan',
			'tarif' => 'Tarif',
			'tarif_orig' => 'Tarif Orig',
			'gabungkan' => 'Gabungkan',
			'id_registrasi_gabung' => 'Id Registrasi Gabung',
			'konfirmasi' => 'Konfirmasi',
			'user_create' => 'User Create',
			'user_update' => 'User Update',
			'user_final' => 'User Final',
			'user_confirm' => 'User Confirm',
			'time_create' => 'Time Create',
			'time_update' => 'Time Update',
			'time_final' => 'Time Final',
			'time_confirm' => 'Time Confirm',
		);
	}
	
	public static function getListTagihanUnprocess($regid){
		$criteria=new CDbCriteria;
		$criteria->condition="id_tagihan is NULL AND id_registrasi='$regid'";
		$model=DaftarTagihan::model()->count($criteria);
		if($model>0){
			return true;
		}else{
			return false;
		}
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;
		$criteria->condition="id_tagihan IS NULL";
		$criteria->compare('id_daftar_tagihan',$this->id_daftar_tagihan,true);
		$criteria->compare('id_tagihan',$this->id_tagihan,true);
		$criteria->compare('id_registrasi',$this->id_registrasi,true);
		$criteria->compare('id_penjamin',$this->id_penjamin,true);
		$criteria->compare('id_jenis_komponen_tagihan',$this->id_jenis_komponen_tagihan,true);
		$criteria->compare('id_tindakan_pasien',$this->id_tindakan_pasien,true);
		$criteria->compare('id_peralatan_pasien',$this->id_peralatan_pasien,true);
		$criteria->compare('id_daftar_item_transaksi',$this->id_daftar_item_transaksi);
		$criteria->compare('id_visite_dokter_pasien',$this->id_visite_dokter_pasien,true);
		$criteria->compare('id_radiology_pasien_list',$this->id_radiology_pasien_list,true);
		$criteria->compare('id_laboratorium_pasien_list',$this->id_laboratorium_pasien_list,true);
		$criteria->compare('id_bed_pasien',$this->id_bed_pasien,true);
		$criteria->compare('id_ruangan_pasien',$this->id_ruangan_pasien,true);
		$criteria->compare('id_administrasi_pasien',$this->id_administrasi_pasien,true);
		$criteria->compare('deskripsi_komponen',$this->deskripsi_komponen,true);
		$criteria->compare('waktu_daftar_tagihan',$this->waktu_daftar_tagihan,true);
		$criteria->compare('harga',$this->harga,true);
		$criteria->compare('jumlah',$this->jumlah,true);
		$criteria->compare('jenis_discount',$this->jenis_discount,true);
		$criteria->compare('discount',$this->discount,true);
		$criteria->compare('jumlah_bayar',$this->jumlah_bayar,true);
		$criteria->compare('jumlah_jaminan',$this->jumlah_jaminan,true);
		$criteria->compare('tarif',$this->tarif,true);
		$criteria->compare('tarif_orig',$this->tarif_orig,true);
		$criteria->compare('gabungkan',$this->gabungkan,true);
		$criteria->compare('id_registrasi_gabung',$this->id_registrasi_gabung,true);
		$criteria->compare('konfirmasi',$this->konfirmasi,true);
		$criteria->compare('user_create',$this->user_create,true);
		$criteria->compare('user_update',$this->user_update,true);
		$criteria->compare('user_final',$this->user_final,true);
		$criteria->compare('user_confirm',$this->user_confirm,true);
		$criteria->compare('time_create',$this->time_create,true);
		$criteria->compare('time_update',$this->time_update,true);
		$criteria->compare('time_final',$this->time_final,true);
		$criteria->compare('time_confirm',$this->time_confirm,true);

		return new CActiveDataProvider($this, array(
			'pagination'=>array(
				'pageSize'=> $this::model()->count($criteria),
			),
			'sort'=>array(
			   'defaultOrder'=>'id_jenis_komponen_tagihan ASC',
			),
			'criteria'=>$criteria,
		));
	}
	
	public function getAllowView()
	{
		$akses = UserAccess::model()->find(array("with"=>array("access"),'condition'=>"id_user='".Yii::app()->user->id."' and access.access_controller='DaftarTagihan' and access.access_action='view'"));
		if(!empty($akses)) return true; else return false;
	}
	
	public function getAllowUpdate()
	{
		$akses = UserAccess::model()->find(array("with"=>array("access"),'condition'=>"id_user='".Yii::app()->user->id."' and access.access_controller='DaftarTagihan' and access.access_action='update'"));
		if(!empty($akses)) return true; else return false;
	}
	
	public function getAllowDelete($id)
	{
		$akses = UserAccess::model()->find(array("with"=>array("access"),'condition'=>"id_user='".Yii::app()->user->id."' and access.access_controller='DaftarTagihan' and access.access_action='delete'"));
		$rel=$this->getEmptyRelation($id);
		if(!empty($akses) AND $rel==true){
			return true;
		}else{
			return false;
		}
	}
	
	
	public function getEmptyRelation($id)
	{
		$count=0;
		$count+=AdministrasiPasien::model()->count(array('condition'=>"id_daftar_tagihan='$id'"));
		$count+=DaftarJasaMedis::model()->count(array('condition'=>"id_daftar_tagihan='$id'"));
		$count+=LaboratoriumPasienList::model()->count(array('condition'=>"id_daftar_tagihan='$id'"));
		$count+=PeralatanPasien::model()->count(array('condition'=>"id_daftar_tagihan='$id'"));
		$count+=RadiologyPasienList::model()->count(array('condition'=>"id_daftar_tagihan='$id'"));
		$count+=RuanganPasien::model()->count(array('condition'=>"id_daftar_tagihan='$id'"));
		$count+=TindakanPasien::model()->count(array('condition'=>"id_daftar_tagihan='$id'"));
		$count+=VisiteDokterPasien::model()->count(array('condition'=>"id_daftar_tagihan='$id'"));
		if($count>0){
			return false;
		}else{
			return true;
		}
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return DaftarTagihan the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
