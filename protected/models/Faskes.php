<?php

/**
 * This is the model class for table "faskes".
 *
 * The followings are the available columns in table 'faskes':
 * @property string $id_faskes
 * @property string $id_jenis_faskes
 * @property string $nama_faskes
 * @property string $alamat
 * @property string $status
 *
 * The followings are the available model relations:
 * @property JenisFaskes $idJenisFaskes
 * @property Registrasi[] $registrasis
 * @property Registrasi[] $registrasis1
 */
class Faskes extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'faskes';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('id_jenis_faskes, nama_faskes, alamat', 'required'),
			array('id_jenis_faskes', 'length', 'max'=>20),
			array('nama_faskes, alamat', 'length', 'max'=>100),
			array('status', 'length', 'max'=>1),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id_faskes, id_jenis_faskes, nama_faskes, alamat, status', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'idJenisFaskes' => array(self::BELONGS_TO, 'JenisFaskes', 'id_jenis_faskes'),
			'registrasis' => array(self::HAS_MANY, 'Registrasi', 'id_faskes'),
			'registrasis1' => array(self::HAS_MANY, 'Registrasi', 'id_faskes_tujuan_rujuk'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id_faskes' => 'Id Faskes',
			'id_jenis_faskes' => 'Id Jenis Faskes',
			'nama_faskes' => 'Nama Faskes',
			'alamat' => 'Alamat',
			'status' => 'Status',
		);
	}

	public static function getList(){
		$model=Faskes::model()->findAll(array("condition"=>"status='1'"));
		$data=array();
		foreach($model as $row){
			$data[$row->id_faskes]=$row->idJenisFaskes->nama_jenis_faskes." : ".$row->nama_faskes;
		}
		return $data;
	}
	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id_faskes',$this->id_faskes,true);
		$criteria->compare('id_jenis_faskes',$this->id_jenis_faskes,true);
		$criteria->compare('nama_faskes',$this->nama_faskes,true);
		$criteria->compare('alamat',$this->alamat,true);
		$criteria->compare('status',$this->status,true);

		return new CActiveDataProvider($this, array(
			'pagination'=>array(
				'pageSize'=> Yii::app()->user->getState('pageSize',Yii::app()->params['defaultPageSize']),
			),
			'criteria'=>$criteria,
		));
	}
	
	public function getAllowView()
	{
		$akses = UserAccess::model()->find(array("with"=>array("access"),'condition'=>"id_user='".Yii::app()->user->id."' and access.access_controller='Faskes' and access.access_action='view'"));
		if(!empty($akses)) return true; else return false;
	}
	
	public function getAllowUpdate()
	{
		$akses = UserAccess::model()->find(array("with"=>array("access"),'condition'=>"id_user='".Yii::app()->user->id."' and access.access_controller='Faskes' and access.access_action='update'"));
		if(!empty($akses)) return true; else return false;
	}
	
	public function getAllowDelete($id)
	{
		$akses = UserAccess::model()->find(array("with"=>array("access"),'condition'=>"id_user='".Yii::app()->user->id."' and access.access_controller='Faskes' and access.access_action='delete'"));
		$rel=$this->getEmptyRelation($id);
		if(!empty($akses) AND $rel==true){
			return true;
		}else{
			return false;
		}
	}
	
	
	public function getEmptyRelation($id)
	{
		$count=0;
				$count+=Registrasi::model()->count(array('condition'=>"id_faskes='$id'"));
		$count+=Registrasi::model()->count(array('condition'=>"id_faskes_tujuan_rujuk='$id'"));
		if($count>0){
			return false;
		}else{
			return true;
		}
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Faskes the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
