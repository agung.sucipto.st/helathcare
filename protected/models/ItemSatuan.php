<?php

/**
 * This is the model class for table "item_satuan".
 *
 * The followings are the available columns in table 'item_satuan':
 * @property string $id_item_satuan
 * @property string $id_satuan
 * @property string $id_item
 * @property string $nilai_satuan_konversi
 * @property string $status
 * @property string $parent_id_item_satuan
 *
 * The followings are the available model relations:
 * @property DaftarItemTransaksi[] $daftarItemTransaksis
 * @property DaftarItemTransaksi[] $daftarItemTransaksis1
 * @property Item $idItem
 * @property ItemSatuan $parentIdItemSatuan
 * @property ItemSatuan[] $itemSatuans
 * @property Satuan $idSatuan
 */
class ItemSatuan extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'item_satuan';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('id_satuan, id_item, nilai_satuan_konversi, status', 'required'),
			array('id_satuan, id_item, parent_id_item_satuan', 'length', 'max'=>20),
			array('nilai_satuan_konversi', 'length', 'max'=>30),
			array('status', 'length', 'max'=>45),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id_item_satuan, id_satuan, id_item, nilai_satuan_konversi, status, parent_id_item_satuan', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'daftarItemTransaksis' => array(self::HAS_MANY, 'DaftarItemTransaksi', 'id_item_satuan_kecil'),
			'daftarItemTransaksis1' => array(self::HAS_MANY, 'DaftarItemTransaksi', 'id_item_satuan_besar'),
			'idItem' => array(self::BELONGS_TO, 'Item', 'id_item'),
			'parentIdItemSatuan' => array(self::BELONGS_TO, 'ItemSatuan', 'parent_id_item_satuan'),
			'itemSatuans' => array(self::HAS_MANY, 'ItemSatuan', 'parent_id_item_satuan'),
			'idSatuan' => array(self::BELONGS_TO, 'Satuan', 'id_satuan'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id_item_satuan' => 'Id Item Satuan',
			'id_satuan' => 'Id Satuan',
			'id_item' => 'Id Item',
			'nilai_satuan_konversi' => 'Nilai Satuan Konversi',
			'status' => 'Status',
			'parent_id_item_satuan' => 'Parent Id Item Satuan',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id_item_satuan',$this->id_item_satuan,true);
		$criteria->compare('id_satuan',$this->id_satuan,true);
		$criteria->compare('id_item',$this->id_item,true);
		$criteria->compare('nilai_satuan_konversi',$this->nilai_satuan_konversi,true);
		$criteria->compare('status',$this->status,true);
		$criteria->compare('parent_id_item_satuan',$this->parent_id_item_satuan,true);

		return new CActiveDataProvider($this, array(
			'pagination'=>array(
				'pageSize'=> Yii::app()->user->getState('pageSize',Yii::app()->params['defaultPageSize']),
			),
			'criteria'=>$criteria,
		));
	}
	
	public function getAllowView()
	{
		$akses = UserAccess::model()->find(array("with"=>array("access"),'condition'=>"id_user='".Yii::app()->user->id."' and access.access_controller='ItemSatuan' and access.access_action='view'"));
		if(!empty($akses)) return true; else return false;
	}
	
	public function getAllowUpdate()
	{
		$akses = UserAccess::model()->find(array("with"=>array("access"),'condition'=>"id_user='".Yii::app()->user->id."' and access.access_controller='ItemSatuan' and access.access_action='update'"));
		if(!empty($akses)) return true; else return false;
	}
	
	public function getAllowDelete($id)
	{
		$akses = UserAccess::model()->find(array("with"=>array("access"),'condition'=>"id_user='".Yii::app()->user->id."' and access.access_controller='ItemSatuan' and access.access_action='delete'"));
		$rel=$this->getEmptyRelation($id);
		if(!empty($akses) AND $rel==true){
			return true;
		}else{
			return false;
		}
	}
	
	
	public function getEmptyRelation($id)
	{
		$count=0;
		$count+=DaftarItemTransaksi::model()->count(array('condition'=>"id_item_satuan_kecil='$id'"));
		$count+=DaftarItemTransaksi::model()->count(array('condition'=>"id_item_satuan_besar='$id'"));
						$count+=ItemSatuan::model()->count(array('condition'=>"parent_id_item_satuan='$id'"));
				if($count>0){
			return false;
		}else{
			return true;
		}
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return ItemSatuan the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
