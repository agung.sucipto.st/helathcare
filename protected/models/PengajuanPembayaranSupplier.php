<?php

/**
 * This is the model class for table "pengajuan_pembayaran_supplier".
 *
 * The followings are the available columns in table 'pengajuan_pembayaran_supplier':
 * @property string $id_pengajuan
 * @property string $kode_pengajuan
 * @property string $id_tukar_faktur
 * @property string $tanggal_pengajuan
 * @property string $tanggal_jatuh_tempo
 * @property string $nilai_pengajuan
 * @property string $discount
 * @property string $ppn
 * @property string $pph
 * @property integer $uang_muka
 * @property string $keterangan
 * @property string $status
 * @property string $catatan_penolakan
 * @property string $user_create
 * @property string $user_approve
 * @property string $time_create
 * @property string $time_approve
 *
 * The followings are the available model relations:
 * @property DaftarPembayaranHutangSupplier[] $daftarPembayaranHutangSuppliers
 * @property DaftarPengajuanPembayaranSupplier[] $daftarPengajuanPembayaranSuppliers
 * @property PembayaranHutangSupplier[] $pembayaranHutangSuppliers
 * @property TukarFaktur $idPengajuan
 * @property TukarFaktur $idTukarFaktur
 */
class PengajuanPembayaranSupplier extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'pengajuan_pembayaran_supplier';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('kode_pengajuan, id_tukar_faktur, tanggal_pengajuan, tanggal_jatuh_tempo, nilai_pengajuan, keterangan, status, user_create, time_create', 'required'),
			array('uang_muka', 'numerical', 'integerOnly'=>true),
			array('kode_pengajuan, nilai_pengajuan', 'length', 'max'=>30),
			array('id_tukar_faktur, user_create, user_approve', 'length', 'max'=>20),
			array('discount, ppn, pph, status', 'length', 'max'=>10),
			array('catatan_penolakan, time_approve', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id_pengajuan, kode_pengajuan, id_tukar_faktur, tanggal_pengajuan, tanggal_jatuh_tempo, nilai_pengajuan, discount, ppn, pph, uang_muka, keterangan, status, catatan_penolakan, user_create, user_approve, time_create, time_approve', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'daftarPembayaranHutangSuppliers' => array(self::HAS_MANY, 'DaftarPembayaranHutangSupplier', 'id_pengajuan'),
			'daftarPengajuanPembayaranSuppliers' => array(self::HAS_MANY, 'DaftarPengajuanPembayaranSupplier', 'id_pengajuan'),
			'pembayaranHutangSuppliers' => array(self::HAS_MANY, 'PembayaranHutangSupplier', 'id_pengajuan'),
			'idPengajuan' => array(self::BELONGS_TO, 'TukarFaktur', 'id_pengajuan'),
			'idTukarFaktur' => array(self::BELONGS_TO, 'TukarFaktur', 'id_tukar_faktur'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id_pengajuan' => 'Id Pengajuan',
			'kode_pengajuan' => 'Kode Pengajuan',
			'id_tukar_faktur' => 'Id Tukar Faktur',
			'tanggal_pengajuan' => 'Tanggal Pengajuan',
			'tanggal_jatuh_tempo' => 'Tanggal Jatuh Tempo',
			'nilai_pengajuan' => 'Nilai Pengajuan',
			'discount' => 'Discount',
			'ppn' => 'Ppn',
			'pph' => 'Pph',
			'uang_muka' => 'Uang Muka',
			'keterangan' => 'Keterangan',
			'status' => 'Status',
			'catatan_penolakan' => 'Catatan Penolakan',
			'user_create' => 'User Create',
			'user_approve' => 'User Approve',
			'time_create' => 'Time Create',
			'time_approve' => 'Time Approve',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id_pengajuan',$this->id_pengajuan,true);
		$criteria->compare('kode_pengajuan',$this->kode_pengajuan,true);
		$criteria->compare('id_tukar_faktur',$this->id_tukar_faktur,true);
		$criteria->compare('tanggal_pengajuan',$this->tanggal_pengajuan,true);
		$criteria->compare('tanggal_jatuh_tempo',$this->tanggal_jatuh_tempo,true);
		$criteria->compare('nilai_pengajuan',$this->nilai_pengajuan,true);
		$criteria->compare('discount',$this->discount,true);
		$criteria->compare('ppn',$this->ppn,true);
		$criteria->compare('pph',$this->pph,true);
		$criteria->compare('uang_muka',$this->uang_muka);
		$criteria->compare('keterangan',$this->keterangan,true);
		$criteria->compare('status',$this->status,true);
		$criteria->compare('catatan_penolakan',$this->catatan_penolakan,true);
		$criteria->compare('user_create',$this->user_create,true);
		$criteria->compare('user_approve',$this->user_approve,true);
		$criteria->compare('time_create',$this->time_create,true);
		$criteria->compare('time_approve',$this->time_approve,true);

		return new CActiveDataProvider($this, array(
			'pagination'=>array(
				'pageSize'=> Yii::app()->user->getState('pageSize',Yii::app()->params['defaultPageSize']),
			),
			'criteria'=>$criteria,
		));
	}
	
	public function getAllowView()
	{
		$akses = UserAccess::model()->find(array("with"=>array("access"),'condition'=>"id_user='".Yii::app()->user->id."' and access.access_controller='PengajuanPembayaranSupplier' and access.access_action='view'"));
		if(!empty($akses)) return true; else return false;
	}
	
	public function getAllowUpdate()
	{
		$akses = UserAccess::model()->find(array("with"=>array("access"),'condition'=>"id_user='".Yii::app()->user->id."' and access.access_controller='PengajuanPembayaranSupplier' and access.access_action='update'"));
		if(!empty($akses)) return true; else return false;
	}
	
	public function getAllowDelete($id)
	{
		$akses = UserAccess::model()->find(array("with"=>array("access"),'condition'=>"id_user='".Yii::app()->user->id."' and access.access_controller='PengajuanPembayaranSupplier' and access.access_action='delete'"));
		$rel=$this->getEmptyRelation($id);
		if(!empty($akses) AND $rel==true){
			return true;
		}else{
			return false;
		}
	}
	
	
	public function getEmptyRelation($id)
	{
		$count=0;
		$count+=DaftarPembayaranHutangSupplier::model()->count(array('condition'=>"id_pengajuan='$id'"));
		$count+=DaftarPengajuanPembayaranSupplier::model()->count(array('condition'=>"id_pengajuan='$id'"));
		$count+=PembayaranHutangSupplier::model()->count(array('condition'=>"id_pengajuan='$id'"));
						if($count>0){
			return false;
		}else{
			return true;
		}
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return PengajuanPembayaranSupplier the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
