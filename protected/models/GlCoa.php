<?php

/**
 * This is the model class for table "gl_coa".
 *
 * The followings are the available columns in table 'gl_coa':
 * @property integer $id_coa
 * @property integer $id_coa_tipe
 * @property string $kode_coa
 * @property string $nama_coa
 * @property integer $parent_id
 * @property integer $level
 * @property string $is_kas
 * @property string $is_bank
 * @property string $is_edc_provider
 *
 * The followings are the available model relations:
 * @property BankAkun[] $bankAkuns
 * @property Departemen[] $departemens
 * @property Departemen[] $departemens1
 * @property GlCoaTipe $idCoaTipe
 * @property GlCoa $parent
 * @property GlCoa[] $glCoas
 * @property GlTransaksiRinci[] $glTransaksiRincis
 * @property GlTransaksiRinci[] $glTransaksiRincis1
 * @property GlTransaksiRinci[] $glTransaksiRincis2
 * @property GlTransaksiRinci[] $glTransaksiRincis3
 * @property GlTransaksiRinci[] $glTransaksiRincis4
 * @property GlTransaksiRinci[] $glTransaksiRincis5
 * @property Gudang[] $gudangs
 * @property ItemKategori[] $itemKategoris
 * @property ItemKategori[] $itemKategoris1
 * @property ItemKategori[] $itemKategoris2
 * @property ItemKategori[] $itemKategoris3
 * @property ItemKategori[] $itemKategoris4
 * @property ItemKategori[] $itemKategoris5
 * @property ItemKategori[] $itemKategoris6
 * @property Kelas[] $kelases
 * @property KelompokTindakan[] $kelompokTindakans
 * @property KelompokTindakan[] $kelompokTindakans1
 * @property KelompokTindakan[] $kelompokTindakans2
 * @property KelompokTindakan[] $kelompokTindakans3
 * @property KelompokTindakan[] $kelompokTindakans4
 * @property KelompokTindakan[] $kelompokTindakans5
 * @property KelompokTindakan[] $kelompokTindakans6
 * @property KelompokTindakan[] $kelompokTindakans7
 * @property KelompokTindakan[] $kelompokTindakans8
 * @property PembayaranHutangSupplier[] $pembayaranHutangSuppliers
 * @property PembayaranJasaMedis[] $pembayaranJasaMedises
 * @property PembayaranPiutangKaryawan[] $pembayaranPiutangKaryawans
 * @property PembayaranPiutangPasien[] $pembayaranPiutangPasiens
 * @property PembayaranPiutangPerusahaan[] $pembayaranPiutangPerusahaans
 * @property PembayaranTagihan[] $pembayaranTagihans
 * @property Ruangan[] $ruangans
 * @property Ruangan[] $ruangans1
 * @property Supplier[] $suppliers
 */
class GlCoa extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'gl_coa';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('id_coa_tipe, is_bank, is_edc_provider', 'required'),
			array('id_coa_tipe, parent_id, level', 'numerical', 'integerOnly'=>true),
			array('kode_coa, nama_coa', 'length', 'max'=>45),
			array('is_kas, is_bank, is_edc_provider', 'length', 'max'=>1),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id_coa, id_coa_tipe, kode_coa, nama_coa, parent_id, level, is_kas, is_bank, is_edc_provider', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'bankAkuns' => array(self::HAS_MANY, 'BankAkun', 'id_coa'),
			'departemens' => array(self::HAS_MANY, 'Departemen', 'id_coa_pendapatan'),
			'departemens1' => array(self::HAS_MANY, 'Departemen', 'id_coa_biaya'),
			'idCoaTipe' => array(self::BELONGS_TO, 'GlCoaTipe', 'id_coa_tipe'),
			'parent' => array(self::BELONGS_TO, 'GlCoa', 'parent_id'),
			'glCoas' => array(self::HAS_MANY, 'GlCoa', 'parent_id'),
			'glTransaksiRincis' => array(self::HAS_MANY, 'GlTransaksiRinci', 'gl_coa_id_coa1'),
			'glTransaksiRincis1' => array(self::HAS_MANY, 'GlTransaksiRinci', 'gl_coa_id_coa2'),
			'glTransaksiRincis2' => array(self::HAS_MANY, 'GlTransaksiRinci', 'gl_coa_id_coa3'),
			'glTransaksiRincis3' => array(self::HAS_MANY, 'GlTransaksiRinci', 'gl_coa_id_coa4'),
			'glTransaksiRincis4' => array(self::HAS_MANY, 'GlTransaksiRinci', 'gl_coa_id_coa5'),
			'glTransaksiRincis5' => array(self::HAS_MANY, 'GlTransaksiRinci', 'gl_coa_id'),
			'gudangs' => array(self::HAS_MANY, 'Gudang', 'id_gl_unit'),
			'itemKategoris' => array(self::HAS_MANY, 'ItemKategori', 'id_coa_persediaan'),
			'itemKategoris1' => array(self::HAS_MANY, 'ItemKategori', 'id_coa_pendapatan_rajal'),
			'itemKategoris2' => array(self::HAS_MANY, 'ItemKategori', 'id_coa_pendapatan_ranap'),
			'itemKategoris3' => array(self::HAS_MANY, 'ItemKategori', 'id_coa_biaya_rajal'),
			'itemKategoris4' => array(self::HAS_MANY, 'ItemKategori', 'id_coa_biaya_ranap'),
			'itemKategoris5' => array(self::HAS_MANY, 'ItemKategori', 'id_coa_penyesuaian'),
			'itemKategoris6' => array(self::HAS_MANY, 'ItemKategori', 'id_coa_biaya_operasional'),
			'kelases' => array(self::HAS_MANY, 'Kelas', 'id_coa_pendapatan_kamar_rawatan'),
			'kelompokTindakans' => array(self::HAS_MANY, 'KelompokTindakan', 'id_coa_pendapatan_konsultasi_rajal'),
			'kelompokTindakans1' => array(self::HAS_MANY, 'KelompokTindakan', 'id_coa_pendapatan_konsultasi_ranap'),
			'kelompokTindakans2' => array(self::HAS_MANY, 'KelompokTindakan', 'id_coa_biaya_konsultasi_rajal'),
			'kelompokTindakans3' => array(self::HAS_MANY, 'KelompokTindakan', 'id_coa_biaya_konsultasi_ranap'),
			'kelompokTindakans4' => array(self::HAS_MANY, 'KelompokTindakan', 'id_coa_pendapatan_tindakan_rajal'),
			'kelompokTindakans5' => array(self::HAS_MANY, 'KelompokTindakan', 'id_coa_pendapatan_tindakan_ranap'),
			'kelompokTindakans6' => array(self::HAS_MANY, 'KelompokTindakan', 'id_coa_biaya_tindakan_rajal'),
			'kelompokTindakans7' => array(self::HAS_MANY, 'KelompokTindakan', 'id_coa_biaya_tindakan_ranap'),
			'kelompokTindakans8' => array(self::HAS_MANY, 'KelompokTindakan', 'id_coa_hutang_jasa_medis'),
			'pembayaranHutangSuppliers' => array(self::HAS_MANY, 'PembayaranHutangSupplier', 'id_coa_kas'),
			'pembayaranJasaMedises' => array(self::HAS_MANY, 'PembayaranJasaMedis', 'id_coa_kas'),
			'pembayaranPiutangKaryawans' => array(self::HAS_MANY, 'PembayaranPiutangKaryawan', 'id_coa_kas'),
			'pembayaranPiutangPasiens' => array(self::HAS_MANY, 'PembayaranPiutangPasien', 'id_coa_kas'),
			'pembayaranPiutangPerusahaans' => array(self::HAS_MANY, 'PembayaranPiutangPerusahaan', 'id_coa_kas'),
			'pembayaranTagihans' => array(self::HAS_MANY, 'PembayaranTagihan', 'id_coa_hutang'),
			'ruangans' => array(self::HAS_MANY, 'Ruangan', 'id_coa_pendapatan_ruangan'),
			'ruangans1' => array(self::HAS_MANY, 'Ruangan', 'id_coa_biaya_ruangan'),
			'suppliers' => array(self::HAS_MANY, 'Supplier', 'id_coa_hutang'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id_coa' => 'Id Coa',
			'id_coa_tipe' => 'Id Coa Tipe',
			'kode_coa' => 'Kode Coa',
			'nama_coa' => 'Nama Coa',
			'parent_id' => 'Parent',
			'level' => 'Level',
			'is_kas' => 'Is Kas',
			'is_bank' => 'Is Bank',
			'is_edc_provider' => 'Is Edc Provider',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id_coa',$this->id_coa);
		$criteria->compare('id_coa_tipe',$this->id_coa_tipe);
		$criteria->compare('kode_coa',$this->kode_coa,true);
		$criteria->compare('nama_coa',$this->nama_coa,true);
		$criteria->compare('parent_id',$this->parent_id);
		$criteria->compare('level',$this->level);
		$criteria->compare('is_kas',$this->is_kas,true);
		$criteria->compare('is_bank',$this->is_bank,true);
		$criteria->compare('is_edc_provider',$this->is_edc_provider,true);

		return new CActiveDataProvider($this, array(
			'pagination'=>array(
				'pageSize'=> Yii::app()->user->getState('pageSize',Yii::app()->params['defaultPageSize']),
			),
			'criteria'=>$criteria,
		));
	}
	
	public function getAllowView()
	{
		$akses = UserAccess::model()->find(array("with"=>array("access"),'condition'=>"id_user='".Yii::app()->user->id."' and access.access_controller='GlCoa' and access.access_action='view'"));
		if(!empty($akses)) return true; else return false;
	}
	
	public function getAllowUpdate()
	{
		$akses = UserAccess::model()->find(array("with"=>array("access"),'condition'=>"id_user='".Yii::app()->user->id."' and access.access_controller='GlCoa' and access.access_action='update'"));
		if(!empty($akses)) return true; else return false;
	}
	
	public function getAllowDelete($id)
	{
		$akses = UserAccess::model()->find(array("with"=>array("access"),'condition'=>"id_user='".Yii::app()->user->id."' and access.access_controller='GlCoa' and access.access_action='delete'"));
		$rel=$this->getEmptyRelation($id);
		if(!empty($akses) AND $rel==true){
			return true;
		}else{
			return false;
		}
	}
	
	
	public function getEmptyRelation($id)
	{
		$count=0;
		$count+=BankAkun::model()->count(array('condition'=>"id_coa='$id'"));
		$count+=Departemen::model()->count(array('condition'=>"id_coa_pendapatan='$id'"));
		$count+=Departemen::model()->count(array('condition'=>"id_coa_biaya='$id'"));
						$count+=GlCoa::model()->count(array('condition'=>"parent_id='$id'"));
		$count+=GlTransaksiRinci::model()->count(array('condition'=>"gl_coa_id_coa1='$id'"));
		$count+=GlTransaksiRinci::model()->count(array('condition'=>"gl_coa_id_coa2='$id'"));
		$count+=GlTransaksiRinci::model()->count(array('condition'=>"gl_coa_id_coa3='$id'"));
		$count+=GlTransaksiRinci::model()->count(array('condition'=>"gl_coa_id_coa4='$id'"));
		$count+=GlTransaksiRinci::model()->count(array('condition'=>"gl_coa_id_coa5='$id'"));
		$count+=GlTransaksiRinci::model()->count(array('condition'=>"gl_coa_id='$id'"));
		$count+=Gudang::model()->count(array('condition'=>"id_gl_unit='$id'"));
		$count+=ItemKategori::model()->count(array('condition'=>"id_coa_persediaan='$id'"));
		$count+=ItemKategori::model()->count(array('condition'=>"id_coa_pendapatan_rajal='$id'"));
		$count+=ItemKategori::model()->count(array('condition'=>"id_coa_pendapatan_ranap='$id'"));
		$count+=ItemKategori::model()->count(array('condition'=>"id_coa_biaya_rajal='$id'"));
		$count+=ItemKategori::model()->count(array('condition'=>"id_coa_biaya_ranap='$id'"));
		$count+=ItemKategori::model()->count(array('condition'=>"id_coa_penyesuaian='$id'"));
		$count+=ItemKategori::model()->count(array('condition'=>"id_coa_biaya_operasional='$id'"));
		$count+=Kelas::model()->count(array('condition'=>"id_coa_pendapatan_kamar_rawatan='$id'"));
		$count+=KelompokTindakan::model()->count(array('condition'=>"id_coa_pendapatan_konsultasi_rajal='$id'"));
		$count+=KelompokTindakan::model()->count(array('condition'=>"id_coa_pendapatan_konsultasi_ranap='$id'"));
		$count+=KelompokTindakan::model()->count(array('condition'=>"id_coa_biaya_konsultasi_rajal='$id'"));
		$count+=KelompokTindakan::model()->count(array('condition'=>"id_coa_biaya_konsultasi_ranap='$id'"));
		$count+=KelompokTindakan::model()->count(array('condition'=>"id_coa_pendapatan_tindakan_rajal='$id'"));
		$count+=KelompokTindakan::model()->count(array('condition'=>"id_coa_pendapatan_tindakan_ranap='$id'"));
		$count+=KelompokTindakan::model()->count(array('condition'=>"id_coa_biaya_tindakan_rajal='$id'"));
		$count+=KelompokTindakan::model()->count(array('condition'=>"id_coa_biaya_tindakan_ranap='$id'"));
		$count+=KelompokTindakan::model()->count(array('condition'=>"id_coa_hutang_jasa_medis='$id'"));
		$count+=PembayaranHutangSupplier::model()->count(array('condition'=>"id_coa_kas='$id'"));
		$count+=PembayaranJasaMedis::model()->count(array('condition'=>"id_coa_kas='$id'"));
		$count+=PembayaranPiutangKaryawan::model()->count(array('condition'=>"id_coa_kas='$id'"));
		$count+=PembayaranPiutangPasien::model()->count(array('condition'=>"id_coa_kas='$id'"));
		$count+=PembayaranPiutangPerusahaan::model()->count(array('condition'=>"id_coa_kas='$id'"));
		$count+=PembayaranTagihan::model()->count(array('condition'=>"id_coa_hutang='$id'"));
		$count+=Ruangan::model()->count(array('condition'=>"id_coa_pendapatan_ruangan='$id'"));
		$count+=Ruangan::model()->count(array('condition'=>"id_coa_biaya_ruangan='$id'"));
		$count+=Supplier::model()->count(array('condition'=>"id_coa_hutang='$id'"));
		if($count>0){
			return false;
		}else{
			return true;
		}
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return GlCoa the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
