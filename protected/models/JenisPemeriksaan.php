<?php

/**
 * This is the model class for table "jenis_pemeriksaan".
 *
 * The followings are the available columns in table 'jenis_pemeriksaan':
 * @property string $id_jenis_pemeriksaan
 * @property string $nama_jenis_pemeriksaan
 * @property string $group_pemeriksaan
 *
 * The followings are the available model relations:
 * @property Laboratorium[] $laboratoria
 * @property Radiology[] $radiologies
 */
class JenisPemeriksaan extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'jenis_pemeriksaan';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('nama_jenis_pemeriksaan, group_pemeriksaan', 'required'),
			array('nama_jenis_pemeriksaan', 'length', 'max'=>60),
			array('group_pemeriksaan', 'length', 'max'=>45),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id_jenis_pemeriksaan, nama_jenis_pemeriksaan, group_pemeriksaan', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'laboratoria' => array(self::HAS_MANY, 'Laboratorium', 'id_jenis_pemeriksaan'),
			'radiologies' => array(self::HAS_MANY, 'Radiology', 'id_jenis_pemeriksaan'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id_jenis_pemeriksaan' => 'Id Jenis Pemeriksaan',
			'nama_jenis_pemeriksaan' => 'Nama Jenis Pemeriksaan',
			'group_pemeriksaan' => 'Group Pemeriksaan',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id_jenis_pemeriksaan',$this->id_jenis_pemeriksaan,true);
		$criteria->compare('nama_jenis_pemeriksaan',$this->nama_jenis_pemeriksaan,true);
		$criteria->compare('group_pemeriksaan',$this->group_pemeriksaan,true);

		return new CActiveDataProvider($this, array(
			'pagination'=>array(
				'pageSize'=> Yii::app()->user->getState('pageSize',Yii::app()->params['defaultPageSize']),
			),
			'criteria'=>$criteria,
		));
	}
	
	public function getAllowView()
	{
		$akses = UserAccess::model()->find(array("with"=>array("access"),'condition'=>"id_user='".Yii::app()->user->id."' and access.access_controller='JenisPemeriksaan' and access.access_action='view'"));
		if(!empty($akses)) return true; else return false;
	}
	
	public function getAllowUpdate()
	{
		$akses = UserAccess::model()->find(array("with"=>array("access"),'condition'=>"id_user='".Yii::app()->user->id."' and access.access_controller='JenisPemeriksaan' and access.access_action='update'"));
		if(!empty($akses)) return true; else return false;
	}
	
	public function getAllowDelete($id)
	{
		$akses = UserAccess::model()->find(array("with"=>array("access"),'condition'=>"id_user='".Yii::app()->user->id."' and access.access_controller='JenisPemeriksaan' and access.access_action='delete'"));
		$rel=$this->getEmptyRelation($id);
		if(!empty($akses) AND $rel==true){
			return true;
		}else{
			return false;
		}
	}
	
	
	public function getEmptyRelation($id)
	{
		$count=0;
		$count+=Laboratorium::model()->count(array('condition'=>"id_jenis_pemeriksaan='$id'"));
		$count+=Radiology::model()->count(array('condition'=>"id_jenis_pemeriksaan='$id'"));
		if($count>0){
			return false;
		}else{
			return true;
		}
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return JenisPemeriksaan the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
