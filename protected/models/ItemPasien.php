<?php

/**
 * This is the model class for table "item_pasien".
 *
 * The followings are the available columns in table 'item_pasien':
 * @property string $id_item_pasien
 * @property string $id_resep_pasien_list
 * @property string $id_resep_pasien
 * @property string $id_registrasi
 * @property string $id_item_pasien_group
 * @property string $id_gudang
 * @property string $id_item
 * @property string $id_tarif_item
 * @property string $id_daftar_item_transaksi
 * @property string $is_racikan
 * @property string $nama_racikan
 * @property string $satuan_racikan
 * @property string $parent_racikan
 * @property string $item_dijamin
 * @property string $signa
 * @property string $signa_tambahan
 * @property string $peringatan
 * @property string $jumlah_item
 * @property string $embalase
 * @property string $harga
 * @property string $status_ditagih
 * @property string $id_daftar_tagihan
 * @property string $user_create
 * @property string $user_update
 * @property string $user_final
 * @property string $time_create
 * @property string $time_update
 * @property string $time_final
 *
 * The followings are the available model relations:
 * @property DaftarTagihan[] $daftarTagihans
 * @property DaftarItemTransaksi $idDaftarItemTransaksi
 * @property DaftarTagihan $idDaftarTagihan
 * @property Gudang $idGudang
 * @property Item $idItem
 * @property ItemPasien $parentRacikan
 * @property ItemPasien[] $itemPasiens
 * @property ItemPasienGroup $idItemPasienGroup
 * @property Registrasi $idRegistrasi
 * @property ResepPasien $idResepPasien
 * @property ResepPasienList $idResepPasienList
 * @property TarifItem $idTarifItem
 * @property User $userCreate
 * @property User $userUpdate
 * @property User $userFinal
 */
class ItemPasien extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'item_pasien';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('id_registrasi, id_item_pasien_group, id_gudang, id_item, id_tarif_item, id_daftar_item_transaksi, jumlah_item, harga, status_ditagih, user_create, time_create', 'required'),
			array('id_item_pasien, id_resep_pasien_list, id_resep_pasien, id_registrasi, id_item_pasien_group, id_gudang, id_item, id_tarif_item, id_daftar_item_transaksi, parent_racikan, id_daftar_tagihan, user_create, user_update, user_final', 'length', 'max'=>20),
			array('is_racikan, item_dijamin', 'length', 'max'=>1),
			array('nama_racikan, satuan_racikan, peringatan, status_ditagih', 'length', 'max'=>45),
			array('jumlah_item, embalase, harga', 'length', 'max'=>30),
			array('signa, signa_tambahan, time_update, time_final', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id_item_pasien, id_resep_pasien_list, id_resep_pasien, id_registrasi, id_item_pasien_group, id_gudang, id_item, id_tarif_item, id_daftar_item_transaksi, is_racikan, nama_racikan, satuan_racikan, parent_racikan, item_dijamin, signa, signa_tambahan, peringatan, jumlah_item, embalase, harga, status_ditagih, id_daftar_tagihan, user_create, user_update, user_final, time_create, time_update, time_final', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'daftarTagihans' => array(self::HAS_MANY, 'DaftarTagihan', 'id_item_pasien'),
			'idDaftarItemTransaksi' => array(self::BELONGS_TO, 'DaftarItemTransaksi', 'id_daftar_item_transaksi'),
			'idDaftarTagihan' => array(self::BELONGS_TO, 'DaftarTagihan', 'id_daftar_tagihan'),
			'idGudang' => array(self::BELONGS_TO, 'Gudang', 'id_gudang'),
			'idItem' => array(self::BELONGS_TO, 'Item', 'id_item'),
			'parentRacikan' => array(self::BELONGS_TO, 'ItemPasien', 'parent_racikan'),
			'itemPasiens' => array(self::HAS_MANY, 'ItemPasien', 'parent_racikan'),
			'idItemPasienGroup' => array(self::BELONGS_TO, 'ItemPasienGroup', 'id_item_pasien_group'),
			'idRegistrasi' => array(self::BELONGS_TO, 'Registrasi', 'id_registrasi'),
			'idResepPasien' => array(self::BELONGS_TO, 'ResepPasien', 'id_resep_pasien'),
			'idResepPasienList' => array(self::BELONGS_TO, 'ResepPasienList', 'id_resep_pasien_list'),
			'idTarifItem' => array(self::BELONGS_TO, 'TarifItem', 'id_tarif_item'),
			'userCreate' => array(self::BELONGS_TO, 'User', 'user_create'),
			'userUpdate' => array(self::BELONGS_TO, 'User', 'user_update'),
			'userFinal' => array(self::BELONGS_TO, 'User', 'user_final'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id_item_pasien' => 'Id Item Pasien',
			'id_resep_pasien_list' => 'Id Resep Pasien List',
			'id_resep_pasien' => 'Id Resep Pasien',
			'id_registrasi' => 'Id Registrasi',
			'id_item_pasien_group' => 'Id Item Pasien Group',
			'id_gudang' => 'Id Gudang',
			'id_item' => 'Id Item',
			'id_tarif_item' => 'Id Tarif Item',
			'id_daftar_item_transaksi' => 'Id Daftar Item Transaksi',
			'is_racikan' => 'Is Racikan',
			'nama_racikan' => 'Nama Racikan',
			'satuan_racikan' => 'Satuan Racikan',
			'parent_racikan' => 'Parent Racikan',
			'item_dijamin' => 'Item Dijamin',
			'signa' => 'Signa',
			'signa_tambahan' => 'Signa Tambahan',
			'peringatan' => 'Peringatan',
			'jumlah_item' => 'Jumlah Item',
			'embalase' => 'Embalase',
			'harga' => 'Harga',
			'status_ditagih' => 'Status Ditagih',
			'id_daftar_tagihan' => 'Id Daftar Tagihan',
			'user_create' => 'User Create',
			'user_update' => 'User Update',
			'user_final' => 'User Final',
			'time_create' => 'Time Create',
			'time_update' => 'Time Update',
			'time_final' => 'Time Final',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id_item_pasien',$this->id_item_pasien,true);
		$criteria->compare('id_resep_pasien_list',$this->id_resep_pasien_list,true);
		$criteria->compare('id_resep_pasien',$this->id_resep_pasien,true);
		$criteria->compare('id_registrasi',$this->id_registrasi,true);
		$criteria->compare('id_item_pasien_group',$this->id_item_pasien_group,true);
		$criteria->compare('id_gudang',$this->id_gudang,true);
		$criteria->compare('id_item',$this->id_item,true);
		$criteria->compare('id_tarif_item',$this->id_tarif_item,true);
		$criteria->compare('id_daftar_item_transaksi',$this->id_daftar_item_transaksi,true);
		$criteria->compare('is_racikan',$this->is_racikan,true);
		$criteria->compare('nama_racikan',$this->nama_racikan,true);
		$criteria->compare('satuan_racikan',$this->satuan_racikan,true);
		$criteria->compare('parent_racikan',$this->parent_racikan,true);
		$criteria->compare('item_dijamin',$this->item_dijamin,true);
		$criteria->compare('signa',$this->signa,true);
		$criteria->compare('signa_tambahan',$this->signa_tambahan,true);
		$criteria->compare('peringatan',$this->peringatan,true);
		$criteria->compare('jumlah_item',$this->jumlah_item,true);
		$criteria->compare('embalase',$this->embalase,true);
		$criteria->compare('harga',$this->harga,true);
		$criteria->compare('status_ditagih',$this->status_ditagih,true);
		$criteria->compare('id_daftar_tagihan',$this->id_daftar_tagihan,true);
		$criteria->compare('user_create',$this->user_create,true);
		$criteria->compare('user_update',$this->user_update,true);
		$criteria->compare('user_final',$this->user_final,true);
		$criteria->compare('time_create',$this->time_create,true);
		$criteria->compare('time_update',$this->time_update,true);
		$criteria->compare('time_final',$this->time_final,true);

		return new CActiveDataProvider($this, array(
			'pagination'=>array(
				'pageSize'=> Yii::app()->user->getState('pageSize',Yii::app()->params['defaultPageSize']),
			),
			'criteria'=>$criteria,
		));
	}
	
	public function getAllowView()
	{
		$akses = UserAccess::model()->find(array("with"=>array("access"),'condition'=>"id_user='".Yii::app()->user->id."' and access.access_controller='ItemPasien' and access.access_action='view'"));
		if(!empty($akses)) return true; else return false;
	}
	
	public function getAllowUpdate()
	{
		$akses = UserAccess::model()->find(array("with"=>array("access"),'condition'=>"id_user='".Yii::app()->user->id."' and access.access_controller='ItemPasien' and access.access_action='update'"));
		if(!empty($akses)) return true; else return false;
	}
	
	public function getAllowDelete($id)
	{
		$akses = UserAccess::model()->find(array("with"=>array("access"),'condition'=>"id_user='".Yii::app()->user->id."' and access.access_controller='ItemPasien' and access.access_action='delete'"));
		$rel=$this->getEmptyRelation($id);
		if(!empty($akses) AND $rel==true){
			return true;
		}else{
			return false;
		}
	}
	
	
	public function getEmptyRelation($id)
	{
		$count=0;
		$count+=DaftarTagihan::model()->count(array('condition'=>"id_item_pasien='$id'"));
												$count+=ItemPasien::model()->count(array('condition'=>"parent_racikan='$id'"));
																		if($count>0){
			return false;
		}else{
			return true;
		}
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return ItemPasien the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
