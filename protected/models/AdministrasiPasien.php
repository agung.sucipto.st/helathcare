<?php

/**
 * This is the model class for table "administrasi_pasien".
 *
 * The followings are the available columns in table 'administrasi_pasien':
 * @property string $id_administrasi_pasien
 * @property string $id_registrasi
 * @property string $id_tarif_administrasi
 * @property string $tarif
 * @property string $id_daftar_tagihan
 *
 * The followings are the available model relations:
 * @property DaftarTagihan $idDaftarTagihan
 * @property Registrasi $idRegistrasi
 * @property TarifAdministrasi $idTarifAdministrasi
 * @property DaftarTagihan[] $daftarTagihans
 */
class AdministrasiPasien extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'administrasi_pasien';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('id_registrasi, id_tarif_administrasi, tarif, id_daftar_tagihan', 'required'),
			array('id_registrasi, id_tarif_administrasi, id_daftar_tagihan', 'length', 'max'=>20),
			array('tarif', 'length', 'max'=>30),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id_administrasi_pasien, id_registrasi, id_tarif_administrasi, tarif, id_daftar_tagihan', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'idDaftarTagihan' => array(self::BELONGS_TO, 'DaftarTagihan', 'id_daftar_tagihan'),
			'idRegistrasi' => array(self::BELONGS_TO, 'Registrasi', 'id_registrasi'),
			'idTarifAdministrasi' => array(self::BELONGS_TO, 'TarifAdministrasi', 'id_tarif_administrasi'),
			'daftarTagihans' => array(self::HAS_MANY, 'DaftarTagihan', 'id_administrasi_pasien'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id_administrasi_pasien' => 'Id Administrasi Pasien',
			'id_registrasi' => 'Id Registrasi',
			'id_tarif_administrasi' => 'Id Tarif Administrasi',
			'tarif' => 'Tarif',
			'id_daftar_tagihan' => 'Id Daftar Tagihan',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id_administrasi_pasien',$this->id_administrasi_pasien,true);
		$criteria->compare('id_registrasi',$this->id_registrasi,true);
		$criteria->compare('id_tarif_administrasi',$this->id_tarif_administrasi,true);
		$criteria->compare('tarif',$this->tarif,true);
		$criteria->compare('id_daftar_tagihan',$this->id_daftar_tagihan,true);

		return new CActiveDataProvider($this, array(
			'pagination'=>array(
				'pageSize'=> Yii::app()->user->getState('pageSize',Yii::app()->params['defaultPageSize']),
			),
			'criteria'=>$criteria,
		));
	}
	
	public function getAllowView()
	{
		$akses = UserAccess::model()->find(array("with"=>array("access"),'condition'=>"id_user='".Yii::app()->user->id."' and access.access_controller='AdministrasiPasien' and access.access_action='view'"));
		if(!empty($akses)) return true; else return false;
	}
	
	public function getAllowUpdate()
	{
		$akses = UserAccess::model()->find(array("with"=>array("access"),'condition'=>"id_user='".Yii::app()->user->id."' and access.access_controller='AdministrasiPasien' and access.access_action='update'"));
		if(!empty($akses)) return true; else return false;
	}
	
	public function getAllowDelete($id)
	{
		$akses = UserAccess::model()->find(array("with"=>array("access"),'condition'=>"id_user='".Yii::app()->user->id."' and access.access_controller='AdministrasiPasien' and access.access_action='delete'"));
		$rel=$this->getEmptyRelation($id);
		if(!empty($akses) AND $rel==true){
			return true;
		}else{
			return false;
		}
	}
	
	
	public function getEmptyRelation($id)
	{
		$count=0;
								$count+=DaftarTagihan::model()->count(array('condition'=>"id_administrasi_pasien='$id'"));
		if($count>0){
			return false;
		}else{
			return true;
		}
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return AdministrasiPasien the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
