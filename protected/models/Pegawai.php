<?php

/**
 * This is the model class for table "pegawai".
 *
 * The followings are the available columns in table 'pegawai':
 * @property string $id_pegawai
 * @property string $id_personal
 * @property string $jenis_pegawai
 * @property string $fungsional
 * @property string $id_gelar
 * @property string $status_pegawai
 * @property string $tanggal_bergabung
 * @property string $tanggal_keluar
 * @property string $status_aktif
 * @property string $gaji
 *
 * The followings are the available model relations:
 * @property DaftarJasaMedis[] $daftarJasaMedises
 * @property DepartemenPegawai[] $departemenPegawais
 * @property Dpjp[] $dpjps
 * @property EkspedisiRekamMedis[] $ekspedisiRekamMedises
 * @property JadwalPraktek[] $jadwalPrakteks
 * @property JasaMedis[] $jasaMedises
 * @property LaboratoriumPasien[] $laboratoriumPasiens
 * @property LaboratoriumPasien[] $laboratoriumPasiens1
 * @property LaboratoriumPasienList[] $laboratoriumPasienLists
 * @property LaboratoriumPasienList[] $laboratoriumPasienLists1
 * @property LaboratoriumPasienMedis[] $laboratoriumPasienMedises
 * @property Personal $idPersonal
 * @property Gelar $idGelar
 * @property PembayaranJasaMedis[] $pembayaranJasaMedises
 * @property PeralatanPasienMedis[] $peralatanPasienMedises
 * @property RadiologyPasien[] $radiologyPasiens
 * @property RadiologyPasien[] $radiologyPasiens1
 * @property RadiologyPasienList[] $radiologyPasienLists
 * @property RadiologyPasienList[] $radiologyPasienLists1
 * @property RadiologyPasienMedis[] $radiologyPasienMedises
 * @property ResepPasien[] $resepPasiens
 * @property SoapPasien[] $soapPasiens
 * @property TarifVisiteDokter[] $tarifVisiteDokters
 * @property TindakanPasienMedis[] $tindakanPasienMedises
 * @property User[] $users
 * @property VisiteDokterPasien[] $visiteDokterPasiens
 */
class Pegawai extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'pegawai';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('id_personal, jenis_pegawai, fungsional, status_aktif', 'required'),
			array('id_personal, id_gelar', 'length', 'max'=>20),
			array('jenis_pegawai', 'length', 'max'=>9),
			array('fungsional, status_aktif, gaji', 'length', 'max'=>45),
			array('status_pegawai, tanggal_bergabung, tanggal_keluar', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id_pegawai, id_personal, jenis_pegawai, fungsional, id_gelar, status_pegawai, tanggal_bergabung, tanggal_keluar, status_aktif, gaji', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'daftarJasaMedises' => array(self::HAS_MANY, 'DaftarJasaMedis', 'id_pegawai'),
			'departemenPegawais' => array(self::HAS_MANY, 'DepartemenPegawai', 'id_pegawai'),
			'dpjps' => array(self::HAS_MANY, 'Dpjp', 'id_dokter'),
			'ekspedisiRekamMedises' => array(self::HAS_MANY, 'EkspedisiRekamMedis', 'id_pegawai_penerima'),
			'jadwalPrakteks' => array(self::HAS_MANY, 'JadwalPraktek', 'id_pegawai'),
			'jasaMedises' => array(self::HAS_MANY, 'JasaMedis', 'id_pegawai'),
			'laboratoriumPasiens' => array(self::HAS_MANY, 'LaboratoriumPasien', 'id_dokter_pengirim'),
			'laboratoriumPasiens1' => array(self::HAS_MANY, 'LaboratoriumPasien', 'id_dokter_penanggungjawab'),
			'laboratoriumPasienLists' => array(self::HAS_MANY, 'LaboratoriumPasienList', 'id_petugas_lab'),
			'laboratoriumPasienLists1' => array(self::HAS_MANY, 'LaboratoriumPasienList', 'id_dokter_pemeriksa'),
			'laboratoriumPasienMedises' => array(self::HAS_MANY, 'LaboratoriumPasienMedis', 'id_pegawai'),
			'idPersonal' => array(self::BELONGS_TO, 'Personal', 'id_personal'),
			'idGelar' => array(self::BELONGS_TO, 'Gelar', 'id_gelar'),
			'pembayaranJasaMedises' => array(self::HAS_MANY, 'PembayaranJasaMedis', 'id_pegawai'),
			'peralatanPasienMedises' => array(self::HAS_MANY, 'PeralatanPasienMedis', 'id_pegawai'),
			'radiologyPasiens' => array(self::HAS_MANY, 'RadiologyPasien', 'id_dokter_penanggungjawab'),
			'radiologyPasiens1' => array(self::HAS_MANY, 'RadiologyPasien', 'id_dokter_pengirim'),
			'radiologyPasienLists' => array(self::HAS_MANY, 'RadiologyPasienList', 'id_dokter_pemeriksa'),
			'radiologyPasienLists1' => array(self::HAS_MANY, 'RadiologyPasienList', 'id_petugas_radiology'),
			'radiologyPasienMedises' => array(self::HAS_MANY, 'RadiologyPasienMedis', 'id_pegawai'),
			'resepPasiens' => array(self::HAS_MANY, 'ResepPasien', 'id_dokter'),
			'soapPasiens' => array(self::HAS_MANY, 'SoapPasien', 'id_pegawai'),
			'tarifVisiteDokters' => array(self::HAS_MANY, 'TarifVisiteDokter', 'id_pegawai'),
			'tindakanPasienMedises' => array(self::HAS_MANY, 'TindakanPasienMedis', 'id_pegawai'),
			'users' => array(self::HAS_MANY, 'User', 'id_pegawai'),
			'visiteDokterPasiens' => array(self::HAS_MANY, 'VisiteDokterPasien', 'id_dokter'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id_pegawai' => 'Id Pegawai',
			'id_personal' => 'Id Personal',
			'jenis_pegawai' => 'Jenis Pegawai',
			'fungsional' => 'Fungsional',
			'id_gelar' => 'Id Gelar',
			'status_pegawai' => 'Status Pegawai',
			'tanggal_bergabung' => 'Tanggal Bergabung',
			'tanggal_keluar' => 'Tanggal Keluar',
			'status_aktif' => 'Status Aktif',
			'gaji' => 'Gaji',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id_pegawai',$this->id_pegawai,true);
		$criteria->compare('id_personal',$this->id_personal,true);
		$criteria->compare('jenis_pegawai',$this->jenis_pegawai,true);
		$criteria->compare('fungsional',$this->fungsional,true);
		$criteria->compare('id_gelar',$this->id_gelar,true);
		$criteria->compare('status_pegawai',$this->status_pegawai,true);
		$criteria->compare('tanggal_bergabung',$this->tanggal_bergabung,true);
		$criteria->compare('tanggal_keluar',$this->tanggal_keluar,true);
		$criteria->compare('status_aktif',$this->status_aktif,true);
		$criteria->compare('gaji',$this->gaji,true);

		return new CActiveDataProvider($this, array(
			'pagination'=>array(
				'pageSize'=> Yii::app()->user->getState('pageSize',Yii::app()->params['defaultPageSize']),
			),
			'criteria'=>$criteria,
		));
	}
	
	public function getAllowView()
	{
		$akses = UserAccess::model()->find(array("with"=>array("access"),'condition'=>"id_user='".Yii::app()->user->id."' and access.access_controller='Pegawai' and access.access_action='view'"));
		if(!empty($akses)) return true; else return false;
	}
	
	public function getAllowUpdate()
	{
		$akses = UserAccess::model()->find(array("with"=>array("access"),'condition'=>"id_user='".Yii::app()->user->id."' and access.access_controller='Pegawai' and access.access_action='update'"));
		if(!empty($akses)) return true; else return false;
	}
	
	public function getAllowDelete($id)
	{
		$akses = UserAccess::model()->find(array("with"=>array("access"),'condition'=>"id_user='".Yii::app()->user->id."' and access.access_controller='Pegawai' and access.access_action='delete'"));
		$rel=$this->getEmptyRelation($id);
		if(!empty($akses) AND $rel==true){
			return true;
		}else{
			return false;
		}
	}
	
	
	public function getEmptyRelation($id)
	{
		$count=0;
		$count+=DaftarJasaMedis::model()->count(array('condition'=>"id_pegawai='$id'"));
		$count+=DepartemenPegawai::model()->count(array('condition'=>"id_pegawai='$id'"));
		$count+=Dpjp::model()->count(array('condition'=>"id_dokter='$id'"));
		$count+=EkspedisiRekamMedis::model()->count(array('condition'=>"id_pegawai_penerima='$id'"));
		$count+=JadwalPraktek::model()->count(array('condition'=>"id_pegawai='$id'"));
		$count+=JasaMedis::model()->count(array('condition'=>"id_pegawai='$id'"));
		$count+=LaboratoriumPasien::model()->count(array('condition'=>"id_dokter_pengirim='$id'"));
		$count+=LaboratoriumPasien::model()->count(array('condition'=>"id_dokter_penanggungjawab='$id'"));
		$count+=LaboratoriumPasienList::model()->count(array('condition'=>"id_petugas_lab='$id'"));
		$count+=LaboratoriumPasienList::model()->count(array('condition'=>"id_dokter_pemeriksa='$id'"));
		$count+=LaboratoriumPasienMedis::model()->count(array('condition'=>"id_pegawai='$id'"));
						$count+=PembayaranJasaMedis::model()->count(array('condition'=>"id_pegawai='$id'"));
		$count+=PeralatanPasienMedis::model()->count(array('condition'=>"id_pegawai='$id'"));
		$count+=RadiologyPasien::model()->count(array('condition'=>"id_dokter_penanggungjawab='$id'"));
		$count+=RadiologyPasien::model()->count(array('condition'=>"id_dokter_pengirim='$id'"));
		$count+=RadiologyPasienList::model()->count(array('condition'=>"id_dokter_pemeriksa='$id'"));
		$count+=RadiologyPasienList::model()->count(array('condition'=>"id_petugas_radiology='$id'"));
		$count+=RadiologyPasienMedis::model()->count(array('condition'=>"id_pegawai='$id'"));
		$count+=ResepPasien::model()->count(array('condition'=>"id_dokter='$id'"));
		$count+=SoapPasien::model()->count(array('condition'=>"id_pegawai='$id'"));
		$count+=TarifVisiteDokter::model()->count(array('condition'=>"id_pegawai='$id'"));
		$count+=TindakanPasienMedis::model()->count(array('condition'=>"id_pegawai='$id'"));
		$count+=User::model()->count(array('condition'=>"id_pegawai='$id'"));
		$count+=VisiteDokterPasien::model()->count(array('condition'=>"id_dokter='$id'"));
		if($count>0){
			return false;
		}else{
			return true;
		}
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Pegawai the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
