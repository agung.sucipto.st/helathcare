<?php

/**
 * This is the model class for table "icd10_pasien".
 *
 * The followings are the available columns in table 'icd10_pasien':
 * @property string $id_icd_pasien
 * @property string $id_registrasi
 * @property string $id_icd10
 * @property string $jenis_kasus
 * @property string $order_diagnosa
 * @property string $kasus_bedah
 * @property string $meninggal
 * @property string $user_create
 * @property string $user_update
 * @property string $time_create
 * @property string $time_update
 *
 * The followings are the available model relations:
 * @property Icd10 $idIcd10
 * @property Registrasi $idRegistrasi
 * @property User $userCreate
 * @property User $userUpdate
 */
class Icd10Pasien extends CActiveRecord
{
	public $waktu_registrasi;
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'icd10_pasien';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('id_registrasi, id_icd10, jenis_kasus, order_diagnosa, user_create, time_create', 'required'),
			array('id_registrasi, id_icd10, user_create, user_update', 'length', 'max'=>20),
			array('jenis_kasus', 'length', 'max'=>4),
			array('order_diagnosa', 'length', 'max'=>8),
			array('kasus_bedah, meninggal', 'length', 'max'=>5),
			array('time_update', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id_icd_pasien, id_registrasi, id_icd10, jenis_kasus, order_diagnosa, kasus_bedah, meninggal, user_create, user_update, time_create, time_update', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'idIcd10' => array(self::BELONGS_TO, 'Icd10', 'id_icd10'),
			'idRegistrasi' => array(self::BELONGS_TO, 'Registrasi', 'id_registrasi'),
			'userCreate' => array(self::BELONGS_TO, 'User', 'user_create'),
			'userUpdate' => array(self::BELONGS_TO, 'User', 'user_update'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id_icd_pasien' => 'Id Icd Pasien',
			'id_registrasi' => 'Id Registrasi',
			'id_icd10' => 'Id Icd10',
			'jenis_kasus' => 'Jenis Kasus',
			'order_diagnosa' => 'Order Diagnosa',
			'kasus_bedah' => 'Kasus Bedah',
			'meninggal' => 'Meninggal',
			'user_create' => 'User Create',
			'user_update' => 'User Update',
			'time_create' => 'Time Create',
			'time_update' => 'Time Update',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id_icd_pasien',$this->id_icd_pasien,true);
		$criteria->compare('id_registrasi',$this->id_registrasi,true);
		$criteria->compare('id_icd10',$this->id_icd10,true);
		$criteria->compare('jenis_kasus',$this->jenis_kasus,true);
		$criteria->compare('order_diagnosa',$this->order_diagnosa,true);
		$criteria->compare('kasus_bedah',$this->kasus_bedah,true);
		$criteria->compare('meninggal',$this->meninggal,true);
		$criteria->compare('user_create',$this->user_create,true);
		$criteria->compare('user_update',$this->user_update,true);
		$criteria->compare('time_create',$this->time_create,true);
		$criteria->compare('time_update',$this->time_update,true);

		return new CActiveDataProvider($this, array(
			'pagination'=>array(
				'pageSize'=> Yii::app()->user->getState('pageSize',Yii::app()->params['defaultPageSize']),
			),
			'criteria'=>$criteria,
		));
	}
	
	public function getAllowView()
	{
		$akses = UserAccess::model()->find(array("with"=>array("access"),'condition'=>"id_user='".Yii::app()->user->id."' and access.access_controller='Icd10Pasien' and access.access_action='view'"));
		if(!empty($akses)) return true; else return false;
	}
	
	public function getAllowUpdate()
	{
		$akses = UserAccess::model()->find(array("with"=>array("access"),'condition'=>"id_user='".Yii::app()->user->id."' and access.access_controller='Icd10Pasien' and access.access_action='update'"));
		if(!empty($akses)) return true; else return false;
	}
	
	public function getAllowDelete($id)
	{
		$akses = UserAccess::model()->find(array("with"=>array("access"),'condition'=>"id_user='".Yii::app()->user->id."' and access.access_controller='Icd10Pasien' and access.access_action='delete'"));
		$rel=$this->getEmptyRelation($id);
		if(!empty($akses) AND $rel==true){
			return true;
		}else{
			return false;
		}
	}
	
	
	public function getEmptyRelation($id)
	{
		$count=0;
										if($count>0){
			return false;
		}else{
			return true;
		}
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Icd10Pasien the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
