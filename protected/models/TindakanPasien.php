<?php

/**
 * This is the model class for table "tindakan_pasien".
 *
 * The followings are the available columns in table 'tindakan_pasien':
 * @property string $id_tindakan_pasien
 * @property string $id_registrasi
 * @property string $id_kelas
 * @property string $id_tindakan
 * @property string $id_tarif_tindakan
 * @property string $catatan
 * @property string $waktu_tindakan
 * @property string $jumlah_tindakan
 * @property string $id_daftar_tagihan
 * @property string $user_create
 * @property string $user_update
 * @property string $user_final
 * @property string $time_create
 * @property string $time_update
 * @property string $time_final
 *
 * The followings are the available model relations:
 * @property DaftarJasaMedis[] $daftarJasaMedises
 * @property DaftarTagihan[] $daftarTagihans
 * @property Registrasi $idRegistrasi
 * @property TarifTindakan $idTarifTindakan
 * @property Tindakan $idTindakan
 * @property DaftarTagihan $idDaftarTagihan
 * @property Kelas $idKelas
 * @property User $userCreate
 * @property User $userUpdate
 * @property User $userFinal
 * @property TindakanPasienMedis[] $tindakanPasienMedises
 */
class TindakanPasien extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public $id_kelompok_tindakan;
	public $jenis_input;
	public $id_peran_medis;
	public $tarif_tindakan;
	public $jasa_medis;
	public function tableName()
	{
		return 'tindakan_pasien';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('id_registrasi, id_kelas, id_tindakan, id_tarif_tindakan, waktu_tindakan, jumlah_tindakan, user_create, time_create', 'required'),
			array('id_registrasi, id_kelas, id_tindakan, id_tarif_tindakan, waktu_tindakan, jumlah_tindakan, user_create, time_create, jenis_input, id_peran_medis, tarif_tindakan,id_kelompok_tindakan', 'required', "on" => "manual"),
			array('id_registrasi, id_kelas, id_tindakan, id_tarif_tindakan, jumlah_tindakan, id_daftar_tagihan, user_create, user_update, user_final', 'length', 'max'=>20),
			array('catatan, time_update, time_final', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id_tindakan_pasien, id_registrasi, id_kelas, id_tindakan, id_tarif_tindakan, catatan, waktu_tindakan, jumlah_tindakan, id_daftar_tagihan, user_create, user_update, user_final, time_create, time_update, time_final', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'daftarJasaMedises' => array(self::HAS_MANY, 'DaftarJasaMedis', 'id_tindakan_pasien'),
			'daftarTagihans' => array(self::HAS_MANY, 'DaftarTagihan', 'id_tindakan_pasien'),
			'idRegistrasi' => array(self::BELONGS_TO, 'Registrasi', 'id_registrasi'),
			'idTarifTindakan' => array(self::BELONGS_TO, 'TarifTindakan', 'id_tarif_tindakan'),
			'idTindakan' => array(self::BELONGS_TO, 'Tindakan', 'id_tindakan'),
			'idDaftarTagihan' => array(self::BELONGS_TO, 'DaftarTagihan', 'id_daftar_tagihan'),
			'idKelas' => array(self::BELONGS_TO, 'Kelas', 'id_kelas'),
			'userCreate' => array(self::BELONGS_TO, 'User', 'user_create'),
			'userUpdate' => array(self::BELONGS_TO, 'User', 'user_update'),
			'userFinal' => array(self::BELONGS_TO, 'User', 'user_final'),
			'tindakanPasienMedises' => array(self::HAS_MANY, 'TindakanPasienMedis', 'id_tindakan_pasien'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'jenis_input' => 'Jenis Input',
			'nama_tindakan' => 'Nama Tindakan',
			'id_peran_medis' => 'Peran Medis',
			'id_kelompok_tindakan' => 'Kelompok Tindakan',
			'id_tindakan_pasien' => 'Tindakan Pasien',
			'id_registrasi' => 'Registrasi',
			'id_kelas' => 'Kelas',
			'id_tindakan' => 'Tindakan',
			'id_tarif_tindakan' => 'Tarif Tindakan',
			'catatan' => 'Catatan',
			'waktu_tindakan' => 'Waktu Tindakan',
			'jumlah_tindakan' => 'Jumlah Tindakan',
			'id_daftar_tagihan' => 'Daftar Tagihan',
			'user_create' => 'User Create',
			'user_update' => 'User Update',
			'user_final' => 'User Final',
			'time_create' => 'Time Create',
			'time_update' => 'Time Update',
			'time_final' => 'Time Final',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id_tindakan_pasien',$this->id_tindakan_pasien,true);
		$criteria->compare('id_registrasi',$this->id_registrasi,true);
		$criteria->compare('id_kelas',$this->id_kelas,true);
		$criteria->compare('id_tindakan',$this->id_tindakan,true);
		$criteria->compare('id_tarif_tindakan',$this->id_tarif_tindakan,true);
		$criteria->compare('catatan',$this->catatan,true);
		$criteria->compare('waktu_tindakan',$this->waktu_tindakan,true);
		$criteria->compare('jumlah_tindakan',$this->jumlah_tindakan,true);
		$criteria->compare('id_daftar_tagihan',$this->id_daftar_tagihan,true);
		$criteria->compare('user_create',$this->user_create,true);
		$criteria->compare('user_update',$this->user_update,true);
		$criteria->compare('user_final',$this->user_final,true);
		$criteria->compare('time_create',$this->time_create,true);
		$criteria->compare('time_update',$this->time_update,true);
		$criteria->compare('time_final',$this->time_final,true);

		return new CActiveDataProvider($this, array(
			'pagination'=>array(
				'pageSize'=> Yii::app()->user->getState('pageSize',Yii::app()->params['defaultPageSize']),
			),
			'criteria'=>$criteria,
		));
	}
	
	public function getAllowView()
	{
		$akses = UserAccess::model()->find(array("with"=>array("access"),'condition'=>"id_user='".Yii::app()->user->id."' and access.access_controller='TindakanPasien' and access.access_action='view'"));
		if(!empty($akses)) return true; else return false;
	}
	
	public function getAllowUpdate()
	{
		$akses = UserAccess::model()->find(array("with"=>array("access"),'condition'=>"id_user='".Yii::app()->user->id."' and access.access_controller='TindakanPasien' and access.access_action='update'"));
		if(!empty($akses)) return true; else return false;
	}
	
	public function getAllowDelete($id)
	{
		$akses = UserAccess::model()->find(array("with"=>array("access"),'condition'=>"id_user='".Yii::app()->user->id."' and access.access_controller='TindakanPasien' and access.access_action='delete'"));
		$rel=$this->getEmptyRelation($id);
		if(!empty($akses) AND $rel==true){
			return true;
		}else{
			return false;
		}
	}
	
	
	public function getEmptyRelation($id)
	{
		$count=0;
		$count+=DaftarJasaMedis::model()->count(array('condition'=>"id_tindakan_pasien='$id'"));
		$count+=DaftarTagihan::model()->count(array('condition'=>"id_tindakan_pasien='$id'"));
		$count+=TindakanPasienMedis::model()->count(array('condition'=>"id_tindakan_pasien='$id'"));
		if($count>0){
			return false;
		}else{
			return true;
		}
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return TindakanPasien the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
