<?php

/**
 * This is the model class for table "paket_detail".
 *
 * The followings are the available columns in table 'paket_detail':
 * @property string $id_paket_detail
 * @property string $id_paket
 * @property string $jenis_komponen
 * @property string $id_tindakan
 * @property string $id_laboratorium
 * @property string $id_radiologi
 * @property integer $jumlah
 *
 * The followings are the available model relations:
 * @property Paket $idPaket
 * @property Tindakan $idTindakan
 * @property Laboratorium $idLaboratorium
 * @property Radiology $idRadiologi
 * @property PaketDetailMedis[] $paketDetailMedises
 * @property PaketPasienList[] $paketPasienLists
 */
class PaketDetail extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'paket_detail';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('id_paket, jenis_komponen, jumlah', 'required'),
			array('jumlah', 'numerical', 'integerOnly'=>true),
			array('id_paket, id_tindakan, id_laboratorium, id_radiologi', 'length', 'max'=>20),
			array('jenis_komponen', 'length', 'max'=>12),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id_paket_detail, id_paket, jenis_komponen, id_tindakan, id_laboratorium, id_radiologi, jumlah', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'idPaket' => array(self::BELONGS_TO, 'Paket', 'id_paket'),
			'idTindakan' => array(self::BELONGS_TO, 'Tindakan', 'id_tindakan'),
			'idLaboratorium' => array(self::BELONGS_TO, 'Laboratorium', 'id_laboratorium'),
			'idRadiologi' => array(self::BELONGS_TO, 'Radiology', 'id_radiologi'),
			'paketDetailMedises' => array(self::HAS_MANY, 'PaketDetailMedis', 'id_paket_detail'),
			'paketPasienLists' => array(self::HAS_MANY, 'PaketPasienList', 'id_paket_detail'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id_paket_detail' => 'Id Paket Detail',
			'id_paket' => 'Id Paket',
			'jenis_komponen' => 'Jenis Komponen',
			'id_tindakan' => 'Id Tindakan',
			'id_laboratorium' => 'Id Laboratorium',
			'id_radiologi' => 'Id Radiologi',
			'jumlah' => 'Jumlah',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id_paket_detail',$this->id_paket_detail,true);
		$criteria->compare('id_paket',$this->id_paket,true);
		$criteria->compare('jenis_komponen',$this->jenis_komponen,true);
		$criteria->compare('id_tindakan',$this->id_tindakan,true);
		$criteria->compare('id_laboratorium',$this->id_laboratorium,true);
		$criteria->compare('id_radiologi',$this->id_radiologi,true);
		$criteria->compare('jumlah',$this->jumlah);

		return new CActiveDataProvider($this, array(
			'pagination'=>array(
				'pageSize'=> Yii::app()->user->getState('pageSize',Yii::app()->params['defaultPageSize']),
			),
			'criteria'=>$criteria,
		));
	}
	
	public function getAllowView()
	{
		$akses = UserAccess::model()->find(array("with"=>array("access"),'condition'=>"id_user='".Yii::app()->user->id."' and access.access_controller='PaketDetail' and access.access_action='view'"));
		if(!empty($akses)) return true; else return false;
	}
	
	public function getAllowUpdate()
	{
		$akses = UserAccess::model()->find(array("with"=>array("access"),'condition'=>"id_user='".Yii::app()->user->id."' and access.access_controller='PaketDetail' and access.access_action='update'"));
		if(!empty($akses)) return true; else return false;
	}
	
	public function getAllowDelete($id)
	{
		$akses = UserAccess::model()->find(array("with"=>array("access"),'condition'=>"id_user='".Yii::app()->user->id."' and access.access_controller='PaketDetail' and access.access_action='delete'"));
		$rel=$this->getEmptyRelation($id);
		if(!empty($akses) AND $rel==true){
			return true;
		}else{
			return false;
		}
	}
	
	
	public function getEmptyRelation($id)
	{
		$count=0;
										$count+=PaketDetailMedis::model()->count(array('condition'=>"id_paket_detail='$id'"));
		$count+=PaketPasienList::model()->count(array('condition'=>"id_paket_detail='$id'"));
		if($count>0){
			return false;
		}else{
			return true;
		}
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return PaketDetail the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
