<?php

/**
 * This is the model class for table "tanda_vital_pasien".
 *
 * The followings are the available columns in table 'tanda_vital_pasien':
 * @property string $id_tanda_vital
 * @property string $id_registrasi
 * @property string $height
 * @property string $weight
 * @property string $blood_pressure
 * @property string $pulse_rate
 * @property string $respiration_rate
 * @property string $temperature
 * @property string $head_circumference
 * @property string $user_create
 * @property string $user_update
 * @property string $time_create
 * @property string $time_update
 *
 * The followings are the available model relations:
 * @property Registrasi $idRegistrasi
 * @property User $userCreate
 * @property User $userUpdate
 */
class TandaVitalPasien extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'tanda_vital_pasien';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('id_registrasi, height, weight, blood_pressure, pulse_rate, respiration_rate, temperature, user_create, time_create', 'required'),
			array('id_registrasi, user_create, user_update', 'length', 'max'=>20),
			array('height, weight, blood_pressure, pulse_rate, respiration_rate, temperature, head_circumference', 'length', 'max'=>10),
			array('time_update', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id_tanda_vital, id_registrasi, height, weight, blood_pressure, pulse_rate, respiration_rate, temperature, head_circumference, user_create, user_update, time_create, time_update', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'idRegistrasi' => array(self::BELONGS_TO, 'Registrasi', 'id_registrasi'),
			'userCreate' => array(self::BELONGS_TO, 'User', 'user_create'),
			'userUpdate' => array(self::BELONGS_TO, 'User', 'user_update'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id_tanda_vital' => 'Id Tanda Vital',
			'id_registrasi' => 'Id Registrasi',
			'height' => 'Tinggi',
			'weight' => 'Berat',
			'blood_pressure' => 'Tekanan Darah',
			'pulse_rate' => 'Denyut Nadi',
			'respiration_rate' => 'Pernafasan',
			'temperature' => 'Suhu',
			'head_circumference' => 'Lingkar Kepala',
			'user_create' => 'User Create',
			'user_update' => 'User Update',
			'time_create' => 'Time Create',
			'time_update' => 'Time Update',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id_tanda_vital',$this->id_tanda_vital,true);
		$criteria->compare('id_registrasi',$this->id_registrasi,true);
		$criteria->compare('height',$this->height,true);
		$criteria->compare('weight',$this->weight,true);
		$criteria->compare('blood_pressure',$this->blood_pressure,true);
		$criteria->compare('pulse_rate',$this->pulse_rate,true);
		$criteria->compare('respiration_rate',$this->respiration_rate,true);
		$criteria->compare('temperature',$this->temperature,true);
		$criteria->compare('head_circumference',$this->head_circumference,true);
		$criteria->compare('user_create',$this->user_create,true);
		$criteria->compare('user_update',$this->user_update,true);
		$criteria->compare('time_create',$this->time_create,true);
		$criteria->compare('time_update',$this->time_update,true);

		return new CActiveDataProvider($this, array(
			'pagination'=>array(
				'pageSize'=> Yii::app()->user->getState('pageSize',Yii::app()->params['defaultPageSize']),
			),
			'criteria'=>$criteria,
		));
	}
	
	public function getAllowView()
	{
		$akses = UserAccess::model()->find(array("with"=>array("access"),'condition'=>"id_user='".Yii::app()->user->id."' and access.access_controller='TandaVitalPasien' and access.access_action='view'"));
		if(!empty($akses)) return true; else return false;
	}
	
	public function getAllowUpdate()
	{
		$akses = UserAccess::model()->find(array("with"=>array("access"),'condition'=>"id_user='".Yii::app()->user->id."' and access.access_controller='TandaVitalPasien' and access.access_action='update'"));
		if(!empty($akses)) return true; else return false;
	}
	
	public function getAllowDelete($id)
	{
		$akses = UserAccess::model()->find(array("with"=>array("access"),'condition'=>"id_user='".Yii::app()->user->id."' and access.access_controller='TandaVitalPasien' and access.access_action='delete'"));
		$rel=$this->getEmptyRelation($id);
		if(!empty($akses) AND $rel==true){
			return true;
		}else{
			return false;
		}
	}
	
	
	public function getEmptyRelation($id)
	{
		$count=0;
								if($count>0){
			return false;
		}else{
			return true;
		}
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return TandaVitalPasien the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
