<?php

/**
 * This is the model class for table "daftar_jasa_medis".
 *
 * The followings are the available columns in table 'daftar_jasa_medis':
 * @property string $id_daftar_jasa_medis
 * @property string $id_jasa_medis
 * @property string $id_pembayaran_jasa_medis
 * @property string $id_pegawai
 * @property string $id_daftar_tagihan
 * @property string $id_tindakan_pasien_medis
 * @property string $id_peralatan_pasien_medis
 * @property string $id_visite_dokter_pasien
 * @property string $id_radiology_pasien_medis
 * @property string $id_laboratorium_pasien_medis
 * @property string $id_rehab_medis_pasien_medis
 * @property string $id_hemodialisa_pasien_medis
 * @property string $id_paket_pasien_medis
 * @property string $nilai_jasa_medis
 * @property string $gross
 * @property string $status_pembayaran
 * @property string $keterangan
 * @property string $user_create
 * @property string $user_update
 * @property string $user_final
 * @property string $time_create
 * @property string $time_update
 * @property string $time_final
 *
 * The followings are the available model relations:
 * @property DaftarTagihan $idDaftarTagihan
 * @property RadiologyPasienMedis $idRadiologyPasienMedis
 * @property LaboratoriumPasienMedis $idLaboratoriumPasienMedis
 * @property RehabMedisPasienMedis $idRehabMedisPasienMedis
 * @property HemodialisaPasienMedis $idHemodialisaPasienMedis
 * @property PaketPasienMedis $idPaketPasienMedis
 * @property JasaMedis $idJasaMedis
 * @property PeralatanPasienMedis $idPeralatanPasienMedis
 * @property User $userCreate
 * @property User $userUpdate
 * @property User $userFinal
 * @property VisiteDokterPasien $idVisiteDokterPasien
 * @property Pegawai $idPegawai
 * @property TindakanPasienMedis $idTindakanPasienMedis
 */
class DaftarJasaMedis extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'daftar_jasa_medis';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('id_pegawai, id_daftar_tagihan, nilai_jasa_medis, gross, user_create, time_create', 'required'),
			array('id_jasa_medis, id_pembayaran_jasa_medis, id_pegawai, id_daftar_tagihan, id_tindakan_pasien_medis, id_peralatan_pasien_medis, id_visite_dokter_pasien, id_radiology_pasien_medis, id_laboratorium_pasien_medis, id_rehab_medis_pasien_medis, id_hemodialisa_pasien_medis, id_paket_pasien_medis, user_create, user_update, user_final', 'length', 'max'=>20),
			array('nilai_jasa_medis, gross', 'length', 'max'=>30),
			array('status_pembayaran', 'length', 'max'=>11),
			array('keterangan, time_update, time_final', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id_daftar_jasa_medis, id_jasa_medis, id_pembayaran_jasa_medis, id_pegawai, id_daftar_tagihan, id_tindakan_pasien_medis, id_peralatan_pasien_medis, id_visite_dokter_pasien, id_radiology_pasien_medis, id_laboratorium_pasien_medis, id_rehab_medis_pasien_medis, id_hemodialisa_pasien_medis, id_paket_pasien_medis, nilai_jasa_medis, gross, status_pembayaran, keterangan, user_create, user_update, user_final, time_create, time_update, time_final', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'idDaftarTagihan' => array(self::BELONGS_TO, 'DaftarTagihan', 'id_daftar_tagihan'),
			'idRadiologyPasienMedis' => array(self::BELONGS_TO, 'RadiologyPasienMedis', 'id_radiology_pasien_medis'),
			'idLaboratoriumPasienMedis' => array(self::BELONGS_TO, 'LaboratoriumPasienMedis', 'id_laboratorium_pasien_medis'),
			'idRehabMedisPasienMedis' => array(self::BELONGS_TO, 'RehabMedisPasienMedis', 'id_rehab_medis_pasien_medis'),
			'idHemodialisaPasienMedis' => array(self::BELONGS_TO, 'HemodialisaPasienMedis', 'id_hemodialisa_pasien_medis'),
			'idPaketPasienMedis' => array(self::BELONGS_TO, 'PaketPasienMedis', 'id_paket_pasien_medis'),
			'idJasaMedis' => array(self::BELONGS_TO, 'JasaMedis', 'id_jasa_medis'),
			'idPeralatanPasienMedis' => array(self::BELONGS_TO, 'PeralatanPasienMedis', 'id_peralatan_pasien_medis'),
			'userCreate' => array(self::BELONGS_TO, 'User', 'user_create'),
			'userUpdate' => array(self::BELONGS_TO, 'User', 'user_update'),
			'userFinal' => array(self::BELONGS_TO, 'User', 'user_final'),
			'idVisiteDokterPasien' => array(self::BELONGS_TO, 'VisiteDokterPasien', 'id_visite_dokter_pasien'),
			'idPegawai' => array(self::BELONGS_TO, 'Pegawai', 'id_pegawai'),
			'idTindakanPasienMedis' => array(self::BELONGS_TO, 'TindakanPasienMedis', 'id_tindakan_pasien_medis'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id_daftar_jasa_medis' => 'Id Daftar Jasa Medis',
			'id_jasa_medis' => 'Id Jasa Medis',
			'id_pembayaran_jasa_medis' => 'Id Pembayaran Jasa Medis',
			'id_pegawai' => 'Id Pegawai',
			'id_daftar_tagihan' => 'Id Daftar Tagihan',
			'id_tindakan_pasien_medis' => 'Id Tindakan Pasien Medis',
			'id_peralatan_pasien_medis' => 'Id Peralatan Pasien Medis',
			'id_visite_dokter_pasien' => 'Id Visite Dokter Pasien',
			'id_radiology_pasien_medis' => 'Id Radiology Pasien Medis',
			'id_laboratorium_pasien_medis' => 'Id Laboratorium Pasien Medis',
			'id_rehab_medis_pasien_medis' => 'Id Rehab Medis Pasien Medis',
			'id_hemodialisa_pasien_medis' => 'Id Hemodialisa Pasien Medis',
			'id_paket_pasien_medis' => 'Id Paket Pasien Medis',
			'nilai_jasa_medis' => 'Nilai Jasa Medis',
			'gross' => 'Gross',
			'status_pembayaran' => 'Status Pembayaran',
			'keterangan' => 'Keterangan',
			'user_create' => 'User Create',
			'user_update' => 'User Update',
			'user_final' => 'User Final',
			'time_create' => 'Time Create',
			'time_update' => 'Time Update',
			'time_final' => 'Time Final',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id_daftar_jasa_medis',$this->id_daftar_jasa_medis,true);
		$criteria->compare('id_jasa_medis',$this->id_jasa_medis,true);
		$criteria->compare('id_pembayaran_jasa_medis',$this->id_pembayaran_jasa_medis,true);
		$criteria->compare('id_pegawai',$this->id_pegawai,true);
		$criteria->compare('id_daftar_tagihan',$this->id_daftar_tagihan,true);
		$criteria->compare('id_tindakan_pasien_medis',$this->id_tindakan_pasien_medis,true);
		$criteria->compare('id_peralatan_pasien_medis',$this->id_peralatan_pasien_medis,true);
		$criteria->compare('id_visite_dokter_pasien',$this->id_visite_dokter_pasien,true);
		$criteria->compare('id_radiology_pasien_medis',$this->id_radiology_pasien_medis,true);
		$criteria->compare('id_laboratorium_pasien_medis',$this->id_laboratorium_pasien_medis,true);
		$criteria->compare('id_rehab_medis_pasien_medis',$this->id_rehab_medis_pasien_medis,true);
		$criteria->compare('id_hemodialisa_pasien_medis',$this->id_hemodialisa_pasien_medis,true);
		$criteria->compare('id_paket_pasien_medis',$this->id_paket_pasien_medis,true);
		$criteria->compare('nilai_jasa_medis',$this->nilai_jasa_medis,true);
		$criteria->compare('gross',$this->gross,true);
		$criteria->compare('status_pembayaran',$this->status_pembayaran,true);
		$criteria->compare('keterangan',$this->keterangan,true);
		$criteria->compare('user_create',$this->user_create,true);
		$criteria->compare('user_update',$this->user_update,true);
		$criteria->compare('user_final',$this->user_final,true);
		$criteria->compare('time_create',$this->time_create,true);
		$criteria->compare('time_update',$this->time_update,true);
		$criteria->compare('time_final',$this->time_final,true);

		return new CActiveDataProvider($this, array(
			'pagination'=>array(
				'pageSize'=> Yii::app()->user->getState('pageSize',Yii::app()->params['defaultPageSize']),
			),
			'criteria'=>$criteria,
		));
	}
	
	public function getAllowView()
	{
		$akses = UserAccess::model()->find(array("with"=>array("access"),'condition'=>"id_user='".Yii::app()->user->id."' and access.access_controller='DaftarJasaMedis' and access.access_action='view'"));
		if(!empty($akses)) return true; else return false;
	}
	
	public function getAllowUpdate()
	{
		$akses = UserAccess::model()->find(array("with"=>array("access"),'condition'=>"id_user='".Yii::app()->user->id."' and access.access_controller='DaftarJasaMedis' and access.access_action='update'"));
		if(!empty($akses)) return true; else return false;
	}
	
	public function getAllowDelete($id)
	{
		$akses = UserAccess::model()->find(array("with"=>array("access"),'condition'=>"id_user='".Yii::app()->user->id."' and access.access_controller='DaftarJasaMedis' and access.access_action='delete'"));
		$rel=$this->getEmptyRelation($id);
		if(!empty($akses) AND $rel==true){
			return true;
		}else{
			return false;
		}
	}
	
	
	public function getEmptyRelation($id)
	{
		$count=0;
																														if($count>0){
			return false;
		}else{
			return true;
		}
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return DaftarJasaMedis the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
