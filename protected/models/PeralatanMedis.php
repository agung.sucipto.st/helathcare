<?php

/**
 * This is the model class for table "peralatan_medis".
 *
 * The followings are the available columns in table 'peralatan_medis':
 * @property string $id_peralatan_medis
 * @property string $id_peralatan
 * @property string $id_peran_medis
 * @property string $mandatory
 * @property string $order
 *
 * The followings are the available model relations:
 * @property Peralatan $idPeralatan
 * @property PeranMedis $idPeranMedis
 * @property PeralatanPasienMedis[] $peralatanPasienMedises
 * @property TarifPeralatanMedis[] $tarifPeralatanMedises
 */
class PeralatanMedis extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'peralatan_medis';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('id_peralatan, id_peran_medis, mandatory, order', 'required'),
			array('id_peralatan, id_peran_medis, order', 'length', 'max'=>20),
			array('mandatory', 'length', 'max'=>1),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id_peralatan_medis, id_peralatan, id_peran_medis, mandatory, order', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'idPeralatan' => array(self::BELONGS_TO, 'Peralatan', 'id_peralatan'),
			'idPeranMedis' => array(self::BELONGS_TO, 'PeranMedis', 'id_peran_medis'),
			'peralatanPasienMedises' => array(self::HAS_MANY, 'PeralatanPasienMedis', 'id_peralatan_medis'),
			'tarifPeralatanMedises' => array(self::HAS_MANY, 'TarifPeralatanMedis', 'id_peralatan_medis'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id_peralatan_medis' => 'Id Peralatan Medis',
			'id_peralatan' => 'Id Peralatan',
			'id_peran_medis' => 'Id Peran Medis',
			'mandatory' => 'Mandatory',
			'order' => 'Order',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id_peralatan_medis',$this->id_peralatan_medis,true);
		$criteria->compare('id_peralatan',$this->id_peralatan,true);
		$criteria->compare('id_peran_medis',$this->id_peran_medis,true);
		$criteria->compare('mandatory',$this->mandatory,true);
		$criteria->compare('order',$this->order,true);

		return new CActiveDataProvider($this, array(
			'pagination'=>array(
				'pageSize'=> Yii::app()->user->getState('pageSize',Yii::app()->params['defaultPageSize']),
			),
			'criteria'=>$criteria,
		));
	}
	
	public function getAllowView()
	{
		$akses = UserAccess::model()->find(array("with"=>array("access"),'condition'=>"id_user='".Yii::app()->user->id."' and access.access_controller='PeralatanMedis' and access.access_action='view'"));
		if(!empty($akses)) return true; else return false;
	}
	
	public function getAllowUpdate()
	{
		$akses = UserAccess::model()->find(array("with"=>array("access"),'condition'=>"id_user='".Yii::app()->user->id."' and access.access_controller='PeralatanMedis' and access.access_action='update'"));
		if(!empty($akses)) return true; else return false;
	}
	
	public function getAllowDelete($id)
	{
		$akses = UserAccess::model()->find(array("with"=>array("access"),'condition'=>"id_user='".Yii::app()->user->id."' and access.access_controller='PeralatanMedis' and access.access_action='delete'"));
		$rel=$this->getEmptyRelation($id);
		if(!empty($akses) AND $rel==true){
			return true;
		}else{
			return false;
		}
	}
	
	
	public function getEmptyRelation($id)
	{
		$count=0;
						$count+=PeralatanPasienMedis::model()->count(array('condition'=>"id_peralatan_medis='$id'"));
		$count+=TarifPeralatanMedis::model()->count(array('condition'=>"id_peralatan_medis='$id'"));
		if($count>0){
			return false;
		}else{
			return true;
		}
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return PeralatanMedis the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
