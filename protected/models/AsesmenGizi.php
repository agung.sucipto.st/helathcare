<?php

/**
 * This is the model class for table "asesmen_gizi".
 *
 * The followings are the available columns in table 'asesmen_gizi':
 * @property string $id_asesmen_gizi
 * @property string $id_registrasi
 * @property string $tgl_asesmen
 * @property string $diagnosa_medis
 * @property string $antroprometri
 * @property integer $bb
 * @property integer $tb
 * @property integer $imt
 * @property integer $lingkar_lengan
 * @property integer $tinggi_lutut
 * @property string $biokimia
 * @property string $fisik
 * @property string $alergi_makanan
 * @property string $list_alergi
 * @property string $pola_makan
 * @property string $riwayat_personal
 * @property string $diagnosa_gizi
 * @property string $intervensi_gizi
 * @property string $user_create
 * @property string $user_update
 * @property string $tgl_create
 * @property string $tgl_update
 */
class AsesmenGizi extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'asesmen_gizi';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('id_asesmen_gizi', 'required'),
			array('bb, tb, imt, lingkar_lengan, tinggi_lutut', 'numerical', 'integerOnly'=>true),
			array('id_asesmen_gizi, id_registrasi, user_create, user_update', 'length', 'max'=>20),
			array('antroprometri', 'length', 'max'=>150),
			array('alergi_makanan', 'length', 'max'=>5),
			array('tgl_asesmen, diagnosa_medis, biokimia, fisik, list_alergi, pola_makan, riwayat_personal, diagnosa_gizi, intervensi_gizi, tgl_create, tgl_update', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id_asesmen_gizi, id_registrasi, tgl_asesmen, diagnosa_medis, antroprometri, bb, tb, imt, lingkar_lengan, tinggi_lutut, biokimia, fisik, alergi_makanan, list_alergi, pola_makan, riwayat_personal, diagnosa_gizi, intervensi_gizi, user_create, user_update, tgl_create, tgl_update', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id_asesmen_gizi' => 'Id Asesmen Gizi',
			'id_registrasi' => 'Id Registrasi',
			'tgl_asesmen' => 'Tgl Asesmen',
			'diagnosa_medis' => 'Diagnosa Medis',
			'antroprometri' => 'Antroprometri',
			'bb' => 'Bb',
			'tb' => 'Tb',
			'imt' => 'Imt',
			'lingkar_lengan' => 'Lingkar Lengan',
			'tinggi_lutut' => 'Tinggi Lutut',
			'biokimia' => 'Biokimia',
			'fisik' => 'Fisik',
			'alergi_makanan' => 'Alergi Makanan',
			'list_alergi' => 'List Alergi',
			'pola_makan' => 'Pola Makan',
			'riwayat_personal' => 'Riwayat Personal',
			'diagnosa_gizi' => 'Diagnosa Gizi',
			'intervensi_gizi' => 'Intervensi Gizi',
			'user_create' => 'User Create',
			'user_update' => 'User Update',
			'tgl_create' => 'Tgl Create',
			'tgl_update' => 'Tgl Update',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id_asesmen_gizi',$this->id_asesmen_gizi,true);
		$criteria->compare('id_registrasi',$this->id_registrasi,true);
		$criteria->compare('tgl_asesmen',$this->tgl_asesmen,true);
		$criteria->compare('diagnosa_medis',$this->diagnosa_medis,true);
		$criteria->compare('antroprometri',$this->antroprometri,true);
		$criteria->compare('bb',$this->bb);
		$criteria->compare('tb',$this->tb);
		$criteria->compare('imt',$this->imt);
		$criteria->compare('lingkar_lengan',$this->lingkar_lengan);
		$criteria->compare('tinggi_lutut',$this->tinggi_lutut);
		$criteria->compare('biokimia',$this->biokimia,true);
		$criteria->compare('fisik',$this->fisik,true);
		$criteria->compare('alergi_makanan',$this->alergi_makanan,true);
		$criteria->compare('list_alergi',$this->list_alergi,true);
		$criteria->compare('pola_makan',$this->pola_makan,true);
		$criteria->compare('riwayat_personal',$this->riwayat_personal,true);
		$criteria->compare('diagnosa_gizi',$this->diagnosa_gizi,true);
		$criteria->compare('intervensi_gizi',$this->intervensi_gizi,true);
		$criteria->compare('user_create',$this->user_create,true);
		$criteria->compare('user_update',$this->user_update,true);
		$criteria->compare('tgl_create',$this->tgl_create,true);
		$criteria->compare('tgl_update',$this->tgl_update,true);

		return new CActiveDataProvider($this, array(
			'pagination'=>array(
				'pageSize'=> Yii::app()->user->getState('pageSize',Yii::app()->params['defaultPageSize']),
			),
			'criteria'=>$criteria,
		));
	}
	
	public function getAllowView()
	{
		$akses = UserAccess::model()->find(array("with"=>array("access"),'condition'=>"id_user='".Yii::app()->user->id."' and access.access_controller='AsesmenGizi' and access.access_action='view'"));
		if(!empty($akses)) return true; else return false;
	}
	
	public function getAllowUpdate()
	{
		$akses = UserAccess::model()->find(array("with"=>array("access"),'condition'=>"id_user='".Yii::app()->user->id."' and access.access_controller='AsesmenGizi' and access.access_action='update'"));
		if(!empty($akses)) return true; else return false;
	}
	
	public function getAllowDelete($id)
	{
		$akses = UserAccess::model()->find(array("with"=>array("access"),'condition'=>"id_user='".Yii::app()->user->id."' and access.access_controller='AsesmenGizi' and access.access_action='delete'"));
		$rel=$this->getEmptyRelation($id);
		if(!empty($akses) AND $rel==true){
			return true;
		}else{
			return false;
		}
	}
	
	
	public function getEmptyRelation($id)
	{
		$count=0;
		if($count>0){
			return false;
		}else{
			return true;
		}
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return AsesmenGizi the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
