<?php

/**
 * This is the model class for table "uang_muka".
 *
 * The followings are the available columns in table 'uang_muka':
 * @property string $id_uang_muka
 * @property string $id_registrasi
 * @property string $waktu_bayar
 * @property string $cara_bayar
 * @property integer $jumlah_bayar
 * @property integer $user_create
 * @property string $time_create
 *
 * The followings are the available model relations:
 * @property Registrasi $idRegistrasi
 */
class UangMuka extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'uang_muka';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('id_registrasi, waktu_bayar, cara_bayar, jumlah_bayar, user_create, time_create', 'required'),
			array('jumlah_bayar, user_create', 'numerical', 'integerOnly'=>true),
			array('id_registrasi', 'length', 'max'=>20),
			array('cara_bayar', 'length', 'max'=>8),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id_uang_muka, id_registrasi, waktu_bayar, cara_bayar, jumlah_bayar, user_create, time_create', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'idRegistrasi' => array(self::BELONGS_TO, 'Registrasi', 'id_registrasi'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id_uang_muka' => 'Id Uang Muka',
			'id_registrasi' => 'Id Registrasi',
			'waktu_bayar' => 'Waktu Bayar',
			'cara_bayar' => 'Cara Bayar',
			'jumlah_bayar' => 'Jumlah Bayar',
			'user_create' => 'User Create',
			'time_create' => 'Time Create',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id_uang_muka',$this->id_uang_muka,true);
		$criteria->compare('id_registrasi',$this->id_registrasi,true);
		$criteria->compare('waktu_bayar',$this->waktu_bayar,true);
		$criteria->compare('cara_bayar',$this->cara_bayar,true);
		$criteria->compare('jumlah_bayar',$this->jumlah_bayar);
		$criteria->compare('user_create',$this->user_create);
		$criteria->compare('time_create',$this->time_create,true);

		return new CActiveDataProvider($this, array(
			'pagination'=>array(
				'pageSize'=> Yii::app()->user->getState('pageSize',Yii::app()->params['defaultPageSize']),
			),
			'criteria'=>$criteria,
		));
	}
	
	public function getAllowView()
	{
		$akses = UserAccess::model()->find(array("with"=>array("access"),'condition'=>"id_user='".Yii::app()->user->id."' and access.access_controller='UangMuka' and access.access_action='view'"));
		if(!empty($akses)) return true; else return false;
	}
	
	public function getAllowUpdate()
	{
		$akses = UserAccess::model()->find(array("with"=>array("access"),'condition'=>"id_user='".Yii::app()->user->id."' and access.access_controller='UangMuka' and access.access_action='update'"));
		if(!empty($akses)) return true; else return false;
	}
	
	public function getAllowDelete($id)
	{
		$akses = UserAccess::model()->find(array("with"=>array("access"),'condition'=>"id_user='".Yii::app()->user->id."' and access.access_controller='UangMuka' and access.access_action='delete'"));
		$rel=$this->getEmptyRelation($id);
		if(!empty($akses) AND $rel==true){
			return true;
		}else{
			return false;
		}
	}
	
	
	public function getEmptyRelation($id)
	{
		$count=0;
				if($count>0){
			return false;
		}else{
			return true;
		}
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return UangMuka the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
