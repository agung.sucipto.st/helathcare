<?php

/**
 * This is the model class for table "tarif_item".
 *
 * The followings are the available columns in table 'tarif_item':
 * @property string $id_tarif_item
 * @property string $id_item
 * @property string $id_kelompok_tarif
 * @property string $id_kelas
 * @property string $harga_dasar
 * @property string $margin_persen
 * @property string $margin_nominal
 * @property string $ppn_persen
 * @property string $ppn_nominal
 * @property string $discount_persen
 * @property string $discount_nominal
 * @property string $tarif
 *
 * The followings are the available model relations:
 * @property ItemPasien[] $itemPasiens
 * @property Kelas $idKelas
 * @property KelompokTarif $idKelompokTarif
 * @property Item $idItem
 */
class TarifItem extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'tarif_item';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('id_item, id_kelompok_tarif, id_kelas, harga_dasar, margin_persen, tarif', 'required'),
			array('id_tarif_item, id_item, id_kelompok_tarif, id_kelas', 'length', 'max'=>20),
			array('harga_dasar, margin_persen, margin_nominal, ppn_persen, ppn_nominal, discount_persen, discount_nominal, tarif', 'length', 'max'=>30),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id_tarif_item, id_item, id_kelompok_tarif, id_kelas, harga_dasar, margin_persen, margin_nominal, ppn_persen, ppn_nominal, discount_persen, discount_nominal, tarif', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'itemPasiens' => array(self::HAS_MANY, 'ItemPasien', 'id_tarif_item'),
			'idKelas' => array(self::BELONGS_TO, 'Kelas', 'id_kelas'),
			'idKelompokTarif' => array(self::BELONGS_TO, 'KelompokTarif', 'id_kelompok_tarif'),
			'idItem' => array(self::BELONGS_TO, 'Item', 'id_item'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id_tarif_item' => 'Id Tarif Item',
			'id_item' => 'Id Item',
			'id_kelompok_tarif' => 'Id Kelompok Tarif',
			'id_kelas' => 'Id Kelas',
			'harga_dasar' => 'Harga Dasar',
			'margin_persen' => 'Margin Persen',
			'margin_nominal' => 'Margin Nominal',
			'ppn_persen' => 'Ppn Persen',
			'ppn_nominal' => 'Ppn Nominal',
			'discount_persen' => 'Discount Persen',
			'discount_nominal' => 'Discount Nominal',
			'tarif' => 'Tarif',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id_tarif_item',$this->id_tarif_item,true);
		$criteria->compare('id_item',$this->id_item,true);
		$criteria->compare('id_kelompok_tarif',$this->id_kelompok_tarif,true);
		$criteria->compare('id_kelas',$this->id_kelas,true);
		$criteria->compare('harga_dasar',$this->harga_dasar,true);
		$criteria->compare('margin_persen',$this->margin_persen,true);
		$criteria->compare('margin_nominal',$this->margin_nominal,true);
		$criteria->compare('ppn_persen',$this->ppn_persen,true);
		$criteria->compare('ppn_nominal',$this->ppn_nominal,true);
		$criteria->compare('discount_persen',$this->discount_persen,true);
		$criteria->compare('discount_nominal',$this->discount_nominal,true);
		$criteria->compare('tarif',$this->tarif,true);

		return new CActiveDataProvider($this, array(
			'pagination'=>array(
				'pageSize'=> Yii::app()->user->getState('pageSize',Yii::app()->params['defaultPageSize']),
			),
			'criteria'=>$criteria,
		));
	}
	
	public function getAllowView()
	{
		$akses = UserAccess::model()->find(array("with"=>array("access"),'condition'=>"id_user='".Yii::app()->user->id."' and access.access_controller='TarifItem' and access.access_action='view'"));
		if(!empty($akses)) return true; else return false;
	}
	
	public function getAllowUpdate()
	{
		$akses = UserAccess::model()->find(array("with"=>array("access"),'condition'=>"id_user='".Yii::app()->user->id."' and access.access_controller='TarifItem' and access.access_action='update'"));
		if(!empty($akses)) return true; else return false;
	}
	
	public function getAllowDelete($id)
	{
		$akses = UserAccess::model()->find(array("with"=>array("access"),'condition'=>"id_user='".Yii::app()->user->id."' and access.access_controller='TarifItem' and access.access_action='delete'"));
		$rel=$this->getEmptyRelation($id);
		if(!empty($akses) AND $rel==true){
			return true;
		}else{
			return false;
		}
	}
	
	
	public function getEmptyRelation($id)
	{
		$count=0;
								if($count>0){
			return false;
		}else{
			return true;
		}
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return TarifItem the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
