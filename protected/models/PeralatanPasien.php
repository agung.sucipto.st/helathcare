<?php

/**
 * This is the model class for table "peralatan_pasien".
 *
 * The followings are the available columns in table 'peralatan_pasien':
 * @property string $id_peralatan_pasien
 * @property string $id_kelas
 * @property string $id_registrasi
 * @property string $id_peralatan
 * @property string $id_tarif_peralatan
 * @property string $catatan
 * @property string $waktu_penggunaan_alat
 * @property string $jumlah_penggunaan
 * @property string $id_daftar_tagihan
 * @property string $user_create
 * @property string $user_update
 * @property string $user_final
 * @property string $time_create
 * @property string $time_update
 * @property string $time_final
 *
 * The followings are the available model relations:
 * @property DaftarJasaMedis[] $daftarJasaMedises
 * @property DaftarTagihan[] $daftarTagihans
 * @property Kelas $idKelas
 * @property Peralatan $idPeralatan
 * @property Registrasi $idRegistrasi
 * @property TarifPeralatan $idTarifPeralatan
 * @property User $userCreate
 * @property User $userUpdate
 * @property User $userFinal
 * @property DaftarTagihan $idDaftarTagihan
 * @property PeralatanPasienMedis[] $peralatanPasienMedises
 */
class PeralatanPasien extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	 public $id_kelompok_peralatan;
	public function tableName()
	{
		return 'peralatan_pasien';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('id_kelas, id_registrasi, id_peralatan, id_tarif_peralatan, waktu_penggunaan_alat, jumlah_penggunaan, user_create, time_create', 'required'),
			array('id_kelas, id_registrasi, id_peralatan, id_tarif_peralatan, jumlah_penggunaan, id_daftar_tagihan, user_create, user_update, user_final', 'length', 'max'=>20),
			array('catatan, time_update, time_final', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id_peralatan_pasien, id_kelas, id_registrasi, id_peralatan, id_tarif_peralatan, catatan, waktu_penggunaan_alat, jumlah_penggunaan, id_daftar_tagihan, user_create, user_update, user_final, time_create, time_update, time_final', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'daftarJasaMedises' => array(self::HAS_MANY, 'DaftarJasaMedis', 'id_peralatan_pasien'),
			'daftarTagihans' => array(self::HAS_MANY, 'DaftarTagihan', 'id_peralatan_pasien'),
			'idKelas' => array(self::BELONGS_TO, 'Kelas', 'id_kelas'),
			'idPeralatan' => array(self::BELONGS_TO, 'Peralatan', 'id_peralatan'),
			'idRegistrasi' => array(self::BELONGS_TO, 'Registrasi', 'id_registrasi'),
			'idTarifPeralatan' => array(self::BELONGS_TO, 'TarifPeralatan', 'id_tarif_peralatan'),
			'userCreate' => array(self::BELONGS_TO, 'User', 'user_create'),
			'userUpdate' => array(self::BELONGS_TO, 'User', 'user_update'),
			'userFinal' => array(self::BELONGS_TO, 'User', 'user_final'),
			'idDaftarTagihan' => array(self::BELONGS_TO, 'DaftarTagihan', 'id_daftar_tagihan'),
			'peralatanPasienMedises' => array(self::HAS_MANY, 'PeralatanPasienMedis', 'id_peralatan_pasien'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id_peralatan_pasien' => 'Id Peralatan Pasien',
			'id_kelas' => 'Id Kelas',
			'id_registrasi' => 'Id Registrasi',
			'id_peralatan' => 'Id Peralatan',
			'id_tarif_peralatan' => 'Id Tarif Peralatan',
			'catatan' => 'Catatan',
			'waktu_penggunaan_alat' => 'Waktu Penggunaan Alat',
			'jumlah_penggunaan' => 'Jumlah Penggunaan',
			'id_daftar_tagihan' => 'Id Daftar Tagihan',
			'user_create' => 'User Create',
			'user_update' => 'User Update',
			'user_final' => 'User Final',
			'time_create' => 'Time Create',
			'time_update' => 'Time Update',
			'time_final' => 'Time Final',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id_peralatan_pasien',$this->id_peralatan_pasien,true);
		$criteria->compare('id_kelas',$this->id_kelas,true);
		$criteria->compare('id_registrasi',$this->id_registrasi,true);
		$criteria->compare('id_peralatan',$this->id_peralatan,true);
		$criteria->compare('id_tarif_peralatan',$this->id_tarif_peralatan,true);
		$criteria->compare('catatan',$this->catatan,true);
		$criteria->compare('waktu_penggunaan_alat',$this->waktu_penggunaan_alat,true);
		$criteria->compare('jumlah_penggunaan',$this->jumlah_penggunaan,true);
		$criteria->compare('id_daftar_tagihan',$this->id_daftar_tagihan,true);
		$criteria->compare('user_create',$this->user_create,true);
		$criteria->compare('user_update',$this->user_update,true);
		$criteria->compare('user_final',$this->user_final,true);
		$criteria->compare('time_create',$this->time_create,true);
		$criteria->compare('time_update',$this->time_update,true);
		$criteria->compare('time_final',$this->time_final,true);

		return new CActiveDataProvider($this, array(
			'pagination'=>array(
				'pageSize'=> Yii::app()->user->getState('pageSize',Yii::app()->params['defaultPageSize']),
			),
			'criteria'=>$criteria,
		));
	}
	
	public function getAllowView()
	{
		$akses = UserAccess::model()->find(array("with"=>array("access"),'condition'=>"id_user='".Yii::app()->user->id."' and access.access_controller='PeralatanPasien' and access.access_action='view'"));
		if(!empty($akses)) return true; else return false;
	}
	
	public function getAllowUpdate()
	{
		$akses = UserAccess::model()->find(array("with"=>array("access"),'condition'=>"id_user='".Yii::app()->user->id."' and access.access_controller='PeralatanPasien' and access.access_action='update'"));
		if(!empty($akses)) return true; else return false;
	}
	
	public function getAllowDelete($id)
	{
		$akses = UserAccess::model()->find(array("with"=>array("access"),'condition'=>"id_user='".Yii::app()->user->id."' and access.access_controller='PeralatanPasien' and access.access_action='delete'"));
		$rel=$this->getEmptyRelation($id);
		if(!empty($akses) AND $rel==true){
			return true;
		}else{
			return false;
		}
	}
	
	
	public function getEmptyRelation($id)
	{
		$count=0;
		$count+=DaftarJasaMedis::model()->count(array('condition'=>"id_peralatan_pasien='$id'"));
		$count+=DaftarTagihan::model()->count(array('condition'=>"id_peralatan_pasien='$id'"));
																		$count+=PeralatanPasienMedis::model()->count(array('condition'=>"id_peralatan_pasien='$id'"));
		if($count>0){
			return false;
		}else{
			return true;
		}
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return PeralatanPasien the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
