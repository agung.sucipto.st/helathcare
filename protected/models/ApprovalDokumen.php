<?php

/**
 * This is the model class for table "approval_dokumen".
 *
 * The followings are the available columns in table 'approval_dokumen':
 * @property string $id_approval
 * @property string $id_pegawai
 * @property string $id_purchase_request
 * @property string $id_purchase_order
 * @property string $waktu_approval
 * @property integer $order_approval
 * @property string $status_approval
 * @property string $catatan_approval
 *
 * The followings are the available model relations:
 * @property Pegawai $idPegawai
 * @property PurchaseRequest $idPurchaseRequest
 * @property PurchaseOrder $idPurchaseOrder
 */
class ApprovalDokumen extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'approval_dokumen';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('id_pegawai, waktu_approval, order_approval, catatan_approval', 'required'),
			array('order_approval', 'numerical', 'integerOnly'=>true),
			array('id_pegawai, id_purchase_request, id_purchase_order', 'length', 'max'=>20),
			array('status_approval', 'length', 'max'=>10),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id_approval, id_pegawai, id_purchase_request, id_purchase_order, waktu_approval, order_approval, status_approval, catatan_approval', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'idPegawai' => array(self::BELONGS_TO, 'Pegawai', 'id_pegawai'),
			'idPurchaseRequest' => array(self::BELONGS_TO, 'PurchaseRequest', 'id_purchase_request'),
			'idPurchaseOrder' => array(self::BELONGS_TO, 'PurchaseOrder', 'id_purchase_order'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id_approval' => 'Id Approval',
			'id_pegawai' => 'Id Pegawai',
			'id_purchase_request' => 'Id Purchase Request',
			'id_purchase_order' => 'Id Purchase Order',
			'waktu_approval' => 'Waktu Approval',
			'order_approval' => 'Order Approval',
			'status_approval' => 'Status Approval',
			'catatan_approval' => 'Catatan Approval',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id_approval',$this->id_approval,true);
		$criteria->compare('id_pegawai',$this->id_pegawai,true);
		$criteria->compare('id_purchase_request',$this->id_purchase_request,true);
		$criteria->compare('id_purchase_order',$this->id_purchase_order,true);
		$criteria->compare('waktu_approval',$this->waktu_approval,true);
		$criteria->compare('order_approval',$this->order_approval);
		$criteria->compare('status_approval',$this->status_approval,true);
		$criteria->compare('catatan_approval',$this->catatan_approval,true);

		return new CActiveDataProvider($this, array(
			'pagination'=>array(
				'pageSize'=> Yii::app()->user->getState('pageSize',Yii::app()->params['defaultPageSize']),
			),
			'criteria'=>$criteria,
		));
	}
	
	public function getAllowView()
	{
		$akses = UserAccess::model()->find(array("with"=>array("access"),'condition'=>"id_user='".Yii::app()->user->id."' and access.access_controller='ApprovalDokumen' and access.access_action='view'"));
		if(!empty($akses)) return true; else return false;
	}
	
	public function getAllowUpdate()
	{
		$akses = UserAccess::model()->find(array("with"=>array("access"),'condition'=>"id_user='".Yii::app()->user->id."' and access.access_controller='ApprovalDokumen' and access.access_action='update'"));
		if(!empty($akses)) return true; else return false;
	}
	
	public function getAllowDelete($id)
	{
		$akses = UserAccess::model()->find(array("with"=>array("access"),'condition'=>"id_user='".Yii::app()->user->id."' and access.access_controller='ApprovalDokumen' and access.access_action='delete'"));
		$rel=$this->getEmptyRelation($id);
		if(!empty($akses) AND $rel==true){
			return true;
		}else{
			return false;
		}
	}
	
	
	public function getEmptyRelation($id)
	{
		$count=0;
								if($count>0){
			return false;
		}else{
			return true;
		}
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return ApprovalDokumen the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
