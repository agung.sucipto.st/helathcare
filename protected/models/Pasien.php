<?php

/**
 * This is the model class for table "pasien".
 *
 * The followings are the available columns in table 'pasien':
 * @property string $id_pasien
 * @property string $id_personal
 * @property string $identifikasi
 * @property string $panggilan
 * @property string $waktu_daftar
 * @property string $id_pasien_ibu
 * @property string $status_rekam_medis
 * @property string $user_create
 * @property string $user_update
 * @property string $time_create
 * @property string $time_update
 *
 * The followings are the available model relations:
 * @property EkspedisiRekamMedis[] $ekspedisiRekamMedises
 * @property Pasien $idPasienIbu
 * @property Pasien[] $pasiens
 * @property Personal $idPersonal
 * @property User $userCreate
 * @property User $userUpdate
 * @property PasienPenjamin[] $pasienPenjamins
 * @property Registrasi[] $registrasis
 */
class Pasien extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public $nama_lengkap,$jenis_kelamin,$tempat_lahir,$tanggal_lahir,$status_perkawinan,$id_agama,$id_pendidikan,$id_pekerjaan,$alamat_domisili,$alamat_sementara,$id_negara,$id_provinsi,$id_kabupaten,$id_kecamatan,$id_jenis_identitas,$no_identitas,$kontak1,$kontak2,$kontak3,$nama_keluarga,$hubungan_pasien,$hubungan_ke_pasien,$nama_ibu,$id_penjamin,$no_kartu,$fasilitas_jaminan,$nama_pemegang_kartu,$id_jenis_hubungan,$jenis_kelamin_keluarga,$alamat_keluarga,$golongan_darah,$resus,$alergi;
	
	public $insertNoHP,$insertKeluarga;
	public function tableName()
	{
		return 'pasien';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('id_personal, identifikasi, panggilan, waktu_daftar, user_create, time_create', 'required','on'=>'teridentifikasi'),
			array('identifikasi, waktu_daftar, user_create, time_create', 'required','on'=>'not_teridentifikasi'),
			array('id_personal, id_pasien_ibu, user_create, user_update', 'length', 'max'=>20),
			array('identifikasi', 'length', 'max'=>5),
			array('panggilan', 'length', 'max'=>10),
			array('status_rekam_medis', 'length', 'max'=>60),
			array('time_update', 'safe'),
			array('nama_lengkap,tanggal_lahir,tempat_lahir', 'findUnique', 'on' => 'teridentifikasi'),
			//array('no_identitas', 'findId', 'on' => 'teridentifikasi'),
			
			array('nama_lengkap, jenis_kelamin, tempat_lahir, tanggal_lahir, alamat_domisili, status_perkawinan, id_agama, id_negara, id_provinsi, id_kabupaten,id_pendidikan,id_pekerjaan', 'required','on'=>'teridentifikasi'),
			array('nama_lengkap,nama_keluarga', 'length', 'max'=>60,'on'=>'teridentifikasi'),
			array('jenis_kelamin', 'length', 'max'=>9,'on'=>'teridentifikasi'),
			array('tempat_lahir', 'length', 'max'=>100,'on'=>'teridentifikasi'),
			array('status_perkawinan', 'length', 'max'=>45,'on'=>'teridentifikasi'),
			array('id_agama, id_pendidikan, id_pekerjaan', 'length', 'max'=>20,'on'=>'teridentifikasi'),
			array('id_negara, id_provinsi', 'length', 'max'=>2,'on'=>'teridentifikasi'),
			array('id_kabupaten', 'length', 'max'=>4,'on'=>'teridentifikasi'),
			array('id_kecamatan', 'length', 'max'=>7,'on'=>'teridentifikasi'),
			
			array('id_jenis_identitas,no_identitas', 'required','on'=>'teridentifikasi'),
			array('kontak1', 'required','on'=>'teridentifikasi'),
			
			array('nama_keluarga,kontak3,hubungan_ke_pasien,hubungan_pasien,alamat_keluarga,jenis_kelamin_keluarga', 'required','on'=>'teridentifikasi'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id_pasien, id_personal, identifikasi, panggilan, waktu_daftar, id_pasien_ibu, status_rekam_medis, user_create, user_update, time_create, time_update,nama_lengkap,tempat_lahir,tanggal_lahir,alamat_domisili,$nama_ibu,no_identitas,kontak1,kontak2', 'safe', 'on'=>'search'),
		);
	}
	
	public function findUnique($attribute, $params)
    {
        $find = Personal::model()->find(array("condition"=>"nama_lengkap='$this->nama_lengkap' AND tanggal_lahir='$this->tanggal_lahir' AND tempat_lahir='$this->tempat_lahir'"));
		
        if (count($find)>0){
			$pasien = Pasien::model()->findByAttributes(array('id_personal'=>$find->id_personal));
			if(count($pasien)>0){
				$this->addError($attribute, 'Data Personal Sudah Ada Pada Database, Silahkan Gunakan Fitur Cari Data Pasien dengan No MR : '.Lib::MRN($pasien->id_pasien));
			}
		}
    }
	
	public function findId($attribute, $params)
    {
        $find = IdentitasPersonal::model()->find(array("condition"=>"no_identitas='$this->no_identitas'"));
        if (count($find)>0){
            $this->addError($attribute, 'No Identitas Sudah Gunakan atas nama : '.$find->idPersonal->nama_lengkap." ( ".$find->idJenisIdentitas->jenis_identitas.")");
		}
    }

	
	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'ekspedisiRekamMedises' => array(self::HAS_MANY, 'EkspedisiRekamMedis', 'id_pasien'),
			'idPasienIbu' => array(self::BELONGS_TO, 'Pasien', 'id_pasien_ibu'),
			'pasiens' => array(self::HAS_MANY, 'Pasien', 'id_pasien_ibu'),
			'idPersonal' => array(self::BELONGS_TO, 'Personal', 'id_personal'),
			'userCreate' => array(self::BELONGS_TO, 'User', 'user_create'),
			'userUpdate' => array(self::BELONGS_TO, 'User', 'user_update'),
			'pasienPenjamins' => array(self::HAS_MANY, 'PasienPenjamin', 'id_pasien'),
			'registrasis' => array(self::HAS_MANY, 'Registrasi', 'id_pasien'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id_pasien' => 'MR ID',
			'id_personal' => 'Id Personal',
			'identifikasi' => 'Identifikasi',
			'panggilan' => 'Title',
			'waktu_daftar' => 'Waktu Daftar',
			'id_pasien_ibu' => 'Relasi Ibu',
			'status_rekam_medis' => 'Status Rekam Medis',
			'user_create' => 'Create',
			'user_update' => 'Update',
			'time_create' => 'Time Create',
			'time_update' => 'Time Update',
			
			'nama_lengkap' => 'Nama Lengkap',
			'jenis_kelamin' => 'Jenis Kelamin',
			'tempat_lahir' => 'Tempat Lahir',
			'tanggal_lahir' => 'Tanggal Lahir',
			'status_perkawinan' => 'Status Perkawinan',
			'id_agama' => 'Agama',
			'id_pendidikan' => 'Pendidikan',
			'id_pekerjaan' => 'Pekerjaan',
			'alamat_domisili' => 'Alamat Domisili',
			'alamat_sementara' => 'Alamat Sementara',
			'id_negara' => 'Negara',
			'id_provinsi' => 'Provinsi',
			'id_kabupaten' => 'Kabupaten',
			'id_kecamatan' => 'Kecamatan',
			
			'id_jenis_identitas' => 'Jenis Identitas',
			'no_identitas' => 'No Identitas',
			
			'kontak1' => 'No HP',
			'kontak2' => 'No HP (Optional)',
			'kontak3' => 'No HP Keluarga / Kerabat',
			'nama_keluarga' => 'Keluarga / Kerabat',
			'jenis_kelamin_keluarga' => 'Jenis Kelamin',
			'hubungan_pasien' => 'Hubungan Pasien Ke Keluarga',
			'hubungan_ke_pasien' => 'Hubungan Ke Pasien',
			'alamat_keluarga' => 'Alamat Keluarga/Kerabat',
			
			'id_penjamin'=>'Penjamin',
			'no_kartu'=>'No Kartu',
			'fasilitas_jaminan'=>'Fasilitas',
			'nama_pemegang_kartu'=>'Nama Pemegang Kartu',
			'id_jenis_hubungan'=>'Hubungan Pemilik Kartu'
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;
		$criteria->distinct=true;
		$criteria->select='nama_lengkap,jenis_kelamin,tempat_lahir,tanggal_lahir,alamat_domisili';

		$criteria->with=array(
			'identitasPersonals',
			'kontakPersonals',
			'pasiens',
		);
		$criteria->order="pasiens.id_pasien DESC";
		
		$criteria->together=true;
		$criteria->compare('kontakPersonals.kontak', $this->kontak1, true);
		$criteria->compare('identitasPersonals.no_identitas', $this->no_identitas, true);
		$criteria->compare('pasiens.id_pasien',$this->id_pasien, false);
		$criteria->compare('nama_lengkap',$this->nama_lengkap,true);
		$criteria->compare('alamat_domisili',$this->alamat_domisili,true);
		$criteria->compare('tempat_lahir',$this->tempat_lahir,true);
		$criteria->compare('tanggal_lahir',$this->tanggal_lahir,true);
	
		return new CActiveDataProvider('Personal', array(
			'pagination'=>array(
				'pageSize'=> Yii::app()->user->getState('pageSize',Yii::app()->params['defaultPageSize']),
			),
			'sort'=>array(
			   'defaultOrder'=>'pasiens.id_pasien ASC',
			   'attributes'=>array(
						'no_identitas'=>array(
							'asc'=>'identitasPersonals.no_identitas ASC',
							'desc'=>'identitasPersonals.no_identitas DESC',
						),
						'id_pasien'=>array(
							'asc'=>'pasiens.id_pasien ASC',
							'desc'=>'pasiens.id_pasien DESC',
						),
						'nama_lengkap'=>array(
							'asc'=>'nama_lengkap ASC',
							'desc'=>'nama_lengkap DESC',
						),
						'alamat_domisili'=>array(
							'asc'=>'alamat_domisili ASC',
							'desc'=>'alamat_domisili DESC',
						),
						'tempat_lahir'=>array(
							'asc'=>'tempat_lahir ASC',
							'desc'=>'tempat_lahir DESC',
						),
						'tanggal_lahir'=>array(
							'asc'=>'tanggal_lahir ASC',
							'desc'=>'tanggal_lahir DESC',
						),
						'*',
					),
			),
			'criteria'=>$criteria,
		));
	}
	
	public function getAllowView()
	{
		$akses = UserAccess::model()->find(array("with"=>array("access"),'condition'=>"id_user='".Yii::app()->user->id."' and access.access_controller='Pasien' and access.access_action='view'"));
		if(!empty($akses)) return true; else return false;
	}
	
	public function getAllowUpdate()
	{
		$akses = UserAccess::model()->find(array("with"=>array("access"),'condition'=>"id_user='".Yii::app()->user->id."' and access.access_controller='Pasien' and access.access_action='update'"));
		if(!empty($akses)) return true; else return false;
	}
	
	public function getAllowDelete($id)
	{
		$akses = UserAccess::model()->find(array("with"=>array("access"),'condition'=>"id_user='".Yii::app()->user->id."' and access.access_controller='Pasien' and access.access_action='delete'"));
		$rel=$this->getEmptyRelation($id);
		if(!empty($akses) AND $rel==true){
			return true;
		}else{
			return false;
		}
	}
	
	
	public function getEmptyRelation($id)
	{
		$count=0;
		$count+=EkspedisiRekamMedis::model()->count(array('condition'=>"id_pasien='$id'"));
		$count+=Pasien::model()->count(array('condition'=>"id_pasien_ibu='$id'"));
		$count+=PasienPenjamin::model()->count(array('condition'=>"id_pasien='$id'"));
		$count+=Registrasi::model()->count(array('condition'=>"id_pasien='$id'"));
		if($count>0){
			return false;
		}else{
			return true;
		}
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Pasien the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
