<?php

/**
 * This is the model class for table "gl_transaksi".
 *
 * The followings are the available columns in table 'gl_transaksi':
 * @property string $id_gl_transaksi
 * @property integer $id_jenis_transaksi
 * @property string $table_referensi
 * @property string $id_referensi
 * @property string $no_referensi
 * @property string $table_subledger
 * @property string $id_subledger
 * @property string $tanggal_efektif
 * @property string $status_posting
 * @property string $deskripsi
 * @property string $is_reserve
 * @property string $user_create
 * @property string $time_create
 *
 * The followings are the available model relations:
 * @property GlJenisTransaksi $idJenisTransaksi
 * @property User $userCreate
 * @property GlTransaksiRinci[] $glTransaksiRincis
 */
class GlTransaksi extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'gl_transaksi';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('id_jenis_transaksi, deskripsi, user_create, time_create', 'required'),
			array('id_jenis_transaksi', 'numerical', 'integerOnly'=>true),
			array('table_referensi, table_subledger', 'length', 'max'=>30),
			array('id_referensi, id_subledger, user_create', 'length', 'max'=>20),
			array('no_referensi', 'length', 'max'=>100),
			array('status_posting, is_reserve', 'length', 'max'=>1),
			array('tanggal_efektif', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id_gl_transaksi, id_jenis_transaksi, table_referensi, id_referensi, no_referensi, table_subledger, id_subledger, tanggal_efektif, status_posting, deskripsi, is_reserve, user_create, time_create', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'idJenisTransaksi' => array(self::BELONGS_TO, 'GlJenisTransaksi', 'id_jenis_transaksi'),
			'userCreate' => array(self::BELONGS_TO, 'User', 'user_create'),
			'glTransaksiRincis' => array(self::HAS_MANY, 'GlTransaksiRinci', 'id_gl_transaksi'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id_gl_transaksi' => 'Id Gl Transaksi',
			'id_jenis_transaksi' => 'Id Jenis Transaksi',
			'table_referensi' => 'Table Referensi',
			'id_referensi' => 'Id Referensi',
			'no_referensi' => 'No Referensi',
			'table_subledger' => 'Table Subledger',
			'id_subledger' => 'Id Subledger',
			'tanggal_efektif' => 'Tanggal Efektif',
			'status_posting' => 'Status Posting',
			'deskripsi' => 'Deskripsi',
			'is_reserve' => 'Is Reserve',
			'user_create' => 'User Create',
			'time_create' => 'Time Create',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id_gl_transaksi',$this->id_gl_transaksi,true);
		$criteria->compare('id_jenis_transaksi',$this->id_jenis_transaksi);
		$criteria->compare('table_referensi',$this->table_referensi,true);
		$criteria->compare('id_referensi',$this->id_referensi,true);
		$criteria->compare('no_referensi',$this->no_referensi,true);
		$criteria->compare('table_subledger',$this->table_subledger,true);
		$criteria->compare('id_subledger',$this->id_subledger,true);
		$criteria->compare('tanggal_efektif',$this->tanggal_efektif,true);
		$criteria->compare('status_posting',$this->status_posting,true);
		$criteria->compare('deskripsi',$this->deskripsi,true);
		$criteria->compare('is_reserve',$this->is_reserve,true);
		$criteria->compare('user_create',$this->user_create,true);
		$criteria->compare('time_create',$this->time_create,true);

		return new CActiveDataProvider($this, array(
			'pagination'=>array(
				'pageSize'=> Yii::app()->user->getState('pageSize',Yii::app()->params['defaultPageSize']),
			),
			'criteria'=>$criteria,
		));
	}
	
	public function getAllowView()
	{
		$akses = UserAccess::model()->find(array("with"=>array("access"),'condition'=>"id_user='".Yii::app()->user->id."' and access.access_controller='GlTransaksi' and access.access_action='view'"));
		if(!empty($akses)) return true; else return false;
	}
	
	public function getAllowUpdate()
	{
		$akses = UserAccess::model()->find(array("with"=>array("access"),'condition'=>"id_user='".Yii::app()->user->id."' and access.access_controller='GlTransaksi' and access.access_action='update'"));
		if(!empty($akses)) return true; else return false;
	}
	
	public function getAllowDelete($id)
	{
		$akses = UserAccess::model()->find(array("with"=>array("access"),'condition'=>"id_user='".Yii::app()->user->id."' and access.access_controller='GlTransaksi' and access.access_action='delete'"));
		$rel=$this->getEmptyRelation($id);
		if(!empty($akses) AND $rel==true){
			return true;
		}else{
			return false;
		}
	}
	
	
	public function getEmptyRelation($id)
	{
		$count=0;
						$count+=GlTransaksiRinci::model()->count(array('condition'=>"id_gl_transaksi='$id'"));
		if($count>0){
			return false;
		}else{
			return true;
		}
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return GlTransaksi the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
