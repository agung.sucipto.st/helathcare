<?php

/**
 * This is the model class for table "personal_keluarga".
 *
 * The followings are the available columns in table 'personal_keluarga':
 * @property string $id_personal_keluarga
 * @property string $id_personal_from
 * @property string $id_personal_to
 * @property string $id_jenis_hubungan_to_from
 * @property string $id_jenis_hubungan_from_to
 *
 * The followings are the available model relations:
 * @property JenisHubungan $idJenisHubunganToFrom
 * @property JenisHubungan $idJenisHubunganFromTo
 * @property Personal $idPersonalFrom
 * @property Personal $idPersonalTo
 */
class PersonalKeluarga extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'personal_keluarga';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('id_personal_from, id_personal_to, id_jenis_hubungan_to_from, id_jenis_hubungan_from_to', 'required'),
			array('id_personal_keluarga, id_personal_from, id_personal_to, id_jenis_hubungan_to_from, id_jenis_hubungan_from_to', 'length', 'max'=>20),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id_personal_keluarga, id_personal_from, id_personal_to, id_jenis_hubungan_to_from, id_jenis_hubungan_from_to', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'idJenisHubunganToFrom' => array(self::BELONGS_TO, 'JenisHubungan', 'id_jenis_hubungan_to_from'),
			'idJenisHubunganFromTo' => array(self::BELONGS_TO, 'JenisHubungan', 'id_jenis_hubungan_from_to'),
			'idPersonalFrom' => array(self::BELONGS_TO, 'Personal', 'id_personal_from'),
			'idPersonalTo' => array(self::BELONGS_TO, 'Personal', 'id_personal_to'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id_personal_keluarga' => 'Id Personal Keluarga',
			'id_personal_from' => 'Id Personal From',
			'id_personal_to' => 'Id Personal To',
			'id_jenis_hubungan_to_from' => 'Id Jenis Hubungan To From',
			'id_jenis_hubungan_from_to' => 'Id Jenis Hubungan From To',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id_personal_keluarga',$this->id_personal_keluarga,true);
		$criteria->compare('id_personal_from',$this->id_personal_from,true);
		$criteria->compare('id_personal_to',$this->id_personal_to,true);
		$criteria->compare('id_jenis_hubungan_to_from',$this->id_jenis_hubungan_to_from,true);
		$criteria->compare('id_jenis_hubungan_from_to',$this->id_jenis_hubungan_from_to,true);

		return new CActiveDataProvider($this, array(
			'pagination'=>array(
				'pageSize'=> Yii::app()->user->getState('pageSize',Yii::app()->params['defaultPageSize']),
			),
			'criteria'=>$criteria,
		));
	}
	
	public function getAllowView()
	{
		$akses = UserAccess::model()->find(array("with"=>array("access"),'condition'=>"id_user='".Yii::app()->user->id."' and access.access_controller='PersonalKeluarga' and access.access_action='view'"));
		if(!empty($akses)) return true; else return false;
	}
	
	public function getAllowUpdate()
	{
		$akses = UserAccess::model()->find(array("with"=>array("access"),'condition'=>"id_user='".Yii::app()->user->id."' and access.access_controller='PersonalKeluarga' and access.access_action='update'"));
		if(!empty($akses)) return true; else return false;
	}
	
	public function getAllowDelete($id)
	{
		$akses = UserAccess::model()->find(array("with"=>array("access"),'condition'=>"id_user='".Yii::app()->user->id."' and access.access_controller='PersonalKeluarga' and access.access_action='delete'"));
		$rel=$this->getEmptyRelation($id);
		if(!empty($akses) AND $rel==true){
			return true;
		}else{
			return false;
		}
	}
	
	
	public function getEmptyRelation($id)
	{
		$count=0;
										if($count>0){
			return false;
		}else{
			return true;
		}
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return PersonalKeluarga the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
