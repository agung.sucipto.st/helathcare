<?php

/**
 * This is the model class for table "formulir_dnr".
 *
 * The followings are the available columns in table 'formulir_dnr':
 * @property string $id_formulir_dnr
 * @property string $id_registrasi
 * @property string $dokter_pelaksana
 * @property string $diagnosa
 * @property string $tatacara
 * @property string $dnr
 * @property string $tujuan
 * @property string $risiko
 * @property string $prognosis
 * @property string $lain_lain
 * @property string $nama
 * @property string $umur
 * @property string $jenis_kelamin
 * @property string $alamat
 * @property string $tindakan
 * @property string $hubungan_keluarga
 * @property string $user_create
 * @property string $user_update
 * @property string $tgl_create
 * @property string $tgl_update
 */
class FormulirDnr extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'formulir_dnr';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('id_formulir_dnr, id_registrasi, user_create, user_update', 'length', 'max'=>20),
			array('dokter_pelaksana, nama, hubungan_keluarga', 'length', 'max'=>35),
			array('diagnosa, tatacara, dnr, tujuan, risiko, prognosis, lain_lain', 'length', 'max'=>200),
			array('umur', 'length', 'max'=>10),
			array('jenis_kelamin', 'length', 'max'=>1),
			array('alamat, tindakan', 'length', 'max'=>100),
			array('tgl_create, tgl_update', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id_formulir_dnr, id_registrasi, dokter_pelaksana, diagnosa, tatacara, dnr, tujuan, risiko, prognosis, lain_lain, nama, umur, jenis_kelamin, alamat, tindakan, hubungan_keluarga, user_create, user_update, tgl_create, tgl_update', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id_formulir_dnr' => 'Id Formulir Dnr',
			'id_registrasi' => 'Id Registrasi',
			'dokter_pelaksana' => 'Dokter Pelaksana',
			'diagnosa' => 'Diagnosa',
			'tatacara' => 'Tatacara',
			'dnr' => 'Dnr',
			'tujuan' => 'Tujuan',
			'risiko' => 'Risiko',
			'prognosis' => 'Prognosis',
			'lain_lain' => 'Lain Lain',
			'nama' => 'Nama',
			'umur' => 'Umur',
			'jenis_kelamin' => 'Jenis Kelamin',
			'alamat' => 'Alamat',
			'tindakan' => 'Tindakan',
			'hubungan_keluarga' => 'Hubungan Keluarga',
			'user_create' => 'User Create',
			'user_update' => 'User Update',
			'tgl_create' => 'Tgl Create',
			'tgl_update' => 'Tgl Update',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id_formulir_dnr',$this->id_formulir_dnr,true);
		$criteria->compare('id_registrasi',$this->id_registrasi,true);
		$criteria->compare('dokter_pelaksana',$this->dokter_pelaksana,true);
		$criteria->compare('diagnosa',$this->diagnosa,true);
		$criteria->compare('tatacara',$this->tatacara,true);
		$criteria->compare('dnr',$this->dnr,true);
		$criteria->compare('tujuan',$this->tujuan,true);
		$criteria->compare('risiko',$this->risiko,true);
		$criteria->compare('prognosis',$this->prognosis,true);
		$criteria->compare('lain_lain',$this->lain_lain,true);
		$criteria->compare('nama',$this->nama,true);
		$criteria->compare('umur',$this->umur,true);
		$criteria->compare('jenis_kelamin',$this->jenis_kelamin,true);
		$criteria->compare('alamat',$this->alamat,true);
		$criteria->compare('tindakan',$this->tindakan,true);
		$criteria->compare('hubungan_keluarga',$this->hubungan_keluarga,true);
		$criteria->compare('user_create',$this->user_create,true);
		$criteria->compare('user_update',$this->user_update,true);
		$criteria->compare('tgl_create',$this->tgl_create,true);
		$criteria->compare('tgl_update',$this->tgl_update,true);

		return new CActiveDataProvider($this, array(
			'pagination'=>array(
				'pageSize'=> Yii::app()->user->getState('pageSize',Yii::app()->params['defaultPageSize']),
			),
			'criteria'=>$criteria,
		));
	}
	
	public function getAllowView()
	{
		$akses = UserAccess::model()->find(array("with"=>array("access"),'condition'=>"id_user='".Yii::app()->user->id."' and access.access_controller='FormulirDnr' and access.access_action='view'"));
		if(!empty($akses)) return true; else return false;
	}
	
	public function getAllowUpdate()
	{
		$akses = UserAccess::model()->find(array("with"=>array("access"),'condition'=>"id_user='".Yii::app()->user->id."' and access.access_controller='FormulirDnr' and access.access_action='update'"));
		if(!empty($akses)) return true; else return false;
	}
	
	public function getAllowDelete($id)
	{
		$akses = UserAccess::model()->find(array("with"=>array("access"),'condition'=>"id_user='".Yii::app()->user->id."' and access.access_controller='FormulirDnr' and access.access_action='delete'"));
		$rel=$this->getEmptyRelation($id);
		if(!empty($akses) AND $rel==true){
			return true;
		}else{
			return false;
		}
	}
	
	
	public function getEmptyRelation($id)
	{
		$count=0;
		if($count>0){
			return false;
		}else{
			return true;
		}
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return FormulirDnr the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
