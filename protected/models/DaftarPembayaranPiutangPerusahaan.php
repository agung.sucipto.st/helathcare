<?php

/**
 * This is the model class for table "daftar_pembayaran_piutang_perusahaan".
 *
 * The followings are the available columns in table 'daftar_pembayaran_piutang_perusahaan':
 * @property string $id_daftar_pembayaran_piutang_perusahaan
 * @property string $id_daftar_piutang_perusahaan
 * @property integer $nilai_dibayarkan
 *
 * The followings are the available model relations:
 * @property DaftarPiutangPerusahaan $idDaftarPiutangPerusahaan
 */
class DaftarPembayaranPiutangPerusahaan extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'daftar_pembayaran_piutang_perusahaan';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('id_daftar_piutang_perusahaan, nilai_dibayarkan', 'required'),
			array('nilai_dibayarkan', 'numerical', 'integerOnly'=>true),
			array('id_daftar_piutang_perusahaan', 'length', 'max'=>20),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id_daftar_pembayaran_piutang_perusahaan, id_daftar_piutang_perusahaan, nilai_dibayarkan', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'idDaftarPiutangPerusahaan' => array(self::BELONGS_TO, 'DaftarPiutangPerusahaan', 'id_daftar_piutang_perusahaan'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id_daftar_pembayaran_piutang_perusahaan' => 'Id Daftar Pembayaran Piutang Perusahaan',
			'id_daftar_piutang_perusahaan' => 'Id Daftar Piutang Perusahaan',
			'nilai_dibayarkan' => 'Nilai Dibayarkan',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id_daftar_pembayaran_piutang_perusahaan',$this->id_daftar_pembayaran_piutang_perusahaan,true);
		$criteria->compare('id_daftar_piutang_perusahaan',$this->id_daftar_piutang_perusahaan,true);
		$criteria->compare('nilai_dibayarkan',$this->nilai_dibayarkan);

		return new CActiveDataProvider($this, array(
			'pagination'=>array(
				'pageSize'=> Yii::app()->user->getState('pageSize',Yii::app()->params['defaultPageSize']),
			),
			'criteria'=>$criteria,
		));
	}
	
	public function getAllowView()
	{
		$akses = UserAccess::model()->find(array("with"=>array("access"),'condition'=>"id_user='".Yii::app()->user->id."' and access.access_controller='DaftarPembayaranPiutangPerusahaan' and access.access_action='view'"));
		if(!empty($akses)) return true; else return false;
	}
	
	public function getAllowUpdate()
	{
		$akses = UserAccess::model()->find(array("with"=>array("access"),'condition'=>"id_user='".Yii::app()->user->id."' and access.access_controller='DaftarPembayaranPiutangPerusahaan' and access.access_action='update'"));
		if(!empty($akses)) return true; else return false;
	}
	
	public function getAllowDelete($id)
	{
		$akses = UserAccess::model()->find(array("with"=>array("access"),'condition'=>"id_user='".Yii::app()->user->id."' and access.access_controller='DaftarPembayaranPiutangPerusahaan' and access.access_action='delete'"));
		$rel=$this->getEmptyRelation($id);
		if(!empty($akses) AND $rel==true){
			return true;
		}else{
			return false;
		}
	}
	
	
	public function getEmptyRelation($id)
	{
		$count=0;
				if($count>0){
			return false;
		}else{
			return true;
		}
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return DaftarPembayaranPiutangPerusahaan the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
