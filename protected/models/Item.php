<?php

/**
 * This is the model class for table "item".
 *
 * The followings are the available columns in table 'item':
 * @property string $id_item
 * @property string $id_principal
 * @property string $id_item_grup
 * @property string $id_item_kategori
 * @property string $nama_item
 * @property string $harga_dasar_satuan_kecil
 * @property string $status
 * @property string $zat_aktif
 * @property string $wac
 * @property string $user_create
 * @property string $user_update
 * @property string $time_create
 * @property string $time_update
 *
 * The followings are the available model relations:
 * @property DaftarItemTransaksi[] $daftarItemTransaksis
 * @property ItemGrup $idItemGrup
 * @property ItemKategori $idItemKategori
 * @property Principal $idPrincipal
 * @property ItemGudang[] $itemGudangs
 * @property ItemPasien[] $itemPasiens
 * @property ItemSatuan[] $itemSatuans
 * @property ResepPasienList[] $resepPasienLists
 */
class Item extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public $unit;
	public $stok;
	public function tableName()
	{
		return 'item';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('id_item_kategori, nama_item, harga_dasar_satuan_kecil, status, user_create, time_create, kode_item', 'required'),
			array('id_principal, id_item_grup, id_item_kategori, user_create, user_update', 'length', 'max'=>20),
			array('harga_dasar_satuan_kecil, wac', 'length', 'max'=>30),
			array('status', 'length', 'max'=>45),
			array('nama_item,kode_item', 'findUnique', 'on' => 'insert'),
			array('nama_item,kode_item', 'findUniqueUpdate', 'on' => 'update'),
			array('zat_aktif, time_update', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id_item, id_principal, id_item_grup, id_item_kategori, nama_item, harga_dasar_satuan_kecil, status, zat_aktif, wac, user_create, user_update, time_create, time_update', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'daftarItemTransaksis' => array(self::HAS_MANY, 'DaftarItemTransaksi', 'id_item'),
			'idItemGrup' => array(self::BELONGS_TO, 'ItemGrup', 'id_item_grup'),
			'idItemKategori' => array(self::BELONGS_TO, 'ItemKategori', 'id_item_kategori'),
			'idPrincipal' => array(self::BELONGS_TO, 'Principal', 'id_principal'),
			'itemGudangs' => array(self::HAS_MANY, 'ItemGudang', 'id_item'),
			'itemPasiens' => array(self::HAS_MANY, 'ItemPasien', 'id_item'),
			'itemSatuans' => array(self::HAS_MANY, 'ItemSatuan', 'id_item'),
			'resepPasienLists' => array(self::HAS_MANY, 'ResepPasienList', 'id_item'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id_item' => 'Id Item',
			'id_principal' => 'Id Principal',
			'id_item_grup' => 'Id Item Grup',
			'id_item_kategori' => 'Id Item Kategori',
			'kode_item' => 'Kode Item',
			'nama_item' => 'Nama Item',
			'harga_dasar_satuan_kecil' => 'Harga Dasar Satuan Kecil',
			'status' => 'Status',
			'zat_aktif' => 'Zat Aktif',
			'wac' => 'Wac',
			'unit' => 'Satuan',
			'user_create' => 'User Create',
			'user_update' => 'User Update',
			'time_create' => 'Time Create',
			'time_update' => 'Time Update',
		);
	}
	
	public function findUnique($attribute, $params)
    {
        $find = $this::model()->findByAttributes(array($attribute=>$this->$attribute));
        if (count($find)>0){
            $this->addError($attribute, $this->getAttributeLabel($attribute).' Sudah ada didalam database, '.$this->getAttributeLabel($attribute).' lainnya !');
		}
    }
	
	public function findUniqueUpdate($attribute, $params)
    {
			$old = $this::model()->findbyPk($this->id_item);
			if($old->$attribute!=$this->$attribute){
				$find = $this::model()->findByAttributes(array($attribute=>$this->$attribute));
				if (count($find)>0){
					$this->addError($attribute, $this->getAttributeLabel($attribute).' Sudah ada didalam database, coba '.$this->getAttributeLabel($attribute).' lainnya !');
				}
			}
    }

	public static function getParentUnit($id){
		$satuan=ItemSatuan::model()->find(array("condition"=>"id_item='$id' and parent_id_item_satuan IS NULL"));
		return $satuan;
	}
	
	public static function getTarif($reg,$kelas,$id){
		$id_jenis_departemen=$reg->idDepartemen->id_jenis_departemen;
		if($reg->jenis_jaminan=="UMUM"){
			$id_kelompok_tarif=1;
		}else{
			$jaminan=JaminanPasien::model()->find(array("condition"=>"id_registrasi='$reg->id_registrasi' AND is_utama='1'"));
			$id_kelompok_tarif=$jaminan->idPenjamin->id_kelompok_tarif;
		}
		
		$criteria=new CDbCriteria;
		$criteria->condition="id_item='$id' AND id_kelompok_tarif='$id_kelompok_tarif' AND id_kelas='$kelas'";
		$tarif=TarifItem::model()->find($criteria);
		
		return $tarif;
	}
	
	public static function getPrice($id,$kelas,$kelompok){
		$criteria=new CDbCriteria;
		$criteria->condition="id_item='$id' AND id_kelompok_tarif='$kelompok' AND id_kelas='$kelas'";
		$tarif=TarifItem::model()->find($criteria);
		return ($tarif->idItem->wac+($tarif->idItem->wac*($tarif->margin_persen/100))+($tarif->idItem->wac*($tarif->ppn_persen/100)));
	}
	
	public static function getPriceId($id,$kelas,$kelompok){
		$criteria=new CDbCriteria;
		$criteria->condition="id_item='$id' AND id_kelompok_tarif='$kelompok' AND id_kelas='$kelas'";
		$tarif=TarifItem::model()->find($criteria);
		return $tarif->id_tarif_item;
	}
	
	public static function getStokAll($id){
		$model=ItemGudang::model()->find(array("select"=>"sum(stok) as stok","condition"=>"id_item='$id'"));
		if(!empty($model)){
			return $model->stok;
		}else{
			return 0;
		}		
	}
	
	public static function getStock($id,$warehouse){
		$model=ItemGudang::model()->find(array("select"=>"stok","condition"=>"id_item='$id' and id_gudang='$warehouse'"));
		if(!empty($model)){
			return $model->stok;
		}else{
			return 0;
		}		
	}
	
	public static function getId($idItem,$gudang){
		$model=ItemGudang::model()->find(array("select"=>"id_item_gudang","condition"=>"id_item='$idItem' and id_gudang='$gudang'"));
		if(!empty($model)){
			return $model->id_item_gudang;
		}else{
			$model=new ItemGudang;
			$model->id_item=$idItem;
			$model->id_gudang=$gudang;
			$model->stok=0;
			$model->save();
			return $model->id_item_gudang;
		}		
	}
	
	public static function countWAC($itemId,$price,$amount){
		$model=ItemGudang::model()->find(array("select"=>"SUM(stok) as stock","condition"=>"id_item='$itemId'"));
		$stock=0;
		if(!empty($model->stok)){
			$stock=$model->stok;
		}
		$item=Item::model()->findByPk($itemId);	
		$wac=(($stock*$item->wac)+($price*$amount))/($stock+$amount);
		return $wac;
	}
	
	public static function updateStock($id,$gudang,$stock){
		$model=ItemGudang::model()->find(array("condition"=>"id_item='$id' and id_gudang='$gudang'"));
		if(empty($model)){
			$model=new ItemGudang;
			$model->id_item=$id;
			$model->id_gudang=$gudang;
		}
		$model->stok=$stock;
		$model->save();
	}
	
	public static function updateWAC($id,$wacc){
		$model=Item::model()->findByPk($id);
		$model->wac=$wacc;
		$model->unit=1;
		$model->save();
	}
	
	public static function getWAC($id){
		$model=Item::model()->findByPk($id);
		return $model->wac;
	}
	
	public static function getLastExp($id){
		$model=DaftarItemTransaksi::model()->find(array("with"=>array("idItemTransaksi"),"condition"=>"id_item='$id' AND (idItemTransaksi.id_jenis_item_transaksi='1' OR idItemTransaksi.id_jenis_item_transaksi='8')","order"=>"idItemTransaksi.waktu_transaksi DESC"));
		return $model;
	}
	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id_item',$this->id_item,true);
		$criteria->compare('id_principal',$this->id_principal,true);
		$criteria->compare('id_item_grup',$this->id_item_grup,true);
		$criteria->compare('id_item_kategori',$this->id_item_kategori,true);
		$criteria->compare('nama_item',$this->nama_item,true);
		$criteria->compare('harga_dasar_satuan_kecil',$this->harga_dasar_satuan_kecil,true);
		$criteria->compare('status',$this->status,true);
		$criteria->compare('zat_aktif',$this->zat_aktif,true);
		$criteria->compare('wac',$this->wac,true);
		$criteria->compare('user_create',$this->user_create,true);
		$criteria->compare('user_update',$this->user_update,true);
		$criteria->compare('time_create',$this->time_create,true);
		$criteria->compare('time_update',$this->time_update,true);

		return new CActiveDataProvider($this, array(
			'pagination'=>array(
				'pageSize'=> Yii::app()->user->getState('pageSize',Yii::app()->params['defaultPageSize']),
			),
			'criteria'=>$criteria,
		));
	}
	
	public function getAllowView()
	{
		$akses = UserAccess::model()->find(array("with"=>array("access"),'condition'=>"id_user='".Yii::app()->user->id."' and access.access_controller='Item' and access.access_action='view'"));
		if(!empty($akses)) return true; else return false;
	}
	
	public function getAllowUpdate()
	{
		$akses = UserAccess::model()->find(array("with"=>array("access"),'condition'=>"id_user='".Yii::app()->user->id."' and access.access_controller='Item' and access.access_action='update'"));
		if(!empty($akses)) return true; else return false;
	}
	
	public function getAllowDelete($id)
	{
		$akses = UserAccess::model()->find(array("with"=>array("access"),'condition'=>"id_user='".Yii::app()->user->id."' and access.access_controller='Item' and access.access_action='delete'"));
		$rel=$this->getEmptyRelation($id);
		if(!empty($akses) AND $rel==true){
			return true;
		}else{
			return false;
		}
	}
	
	
	public function getEmptyRelation($id)
	{
		$count=0;
		$count+=DaftarItemTransaksi::model()->count(array('condition'=>"id_item='$id'"));
		$count+=ItemGudang::model()->count(array('condition'=>"id_item='$id'"));
		$count+=ItemSatuan::model()->count(array('condition'=>"id_item='$id'"));
		$count+=ResepPasienList::model()->count(array('condition'=>"id_item='$id'"));
		if($count>0){
			return false;
		}else{
			return true;
		}
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Item the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
