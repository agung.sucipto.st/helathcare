<?php

/**
 * This is the model class for table "queue_type".
 *
 * The followings are the available columns in table 'queue_type':
 * @property string $qt_id
 * @property string $qt_name
 * @property string $qt_start
 * @property string $qt_end
 * @property string $qt_key
 *
 * The followings are the available model relations:
 * @property Queue[] $queues
 * @property QueueList[] $queueLists
 */
class QueueType extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'queue_type';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('qt_start, qt_end, qt_key', 'required'),
			array('qt_name', 'length', 'max'=>45),
			array('qt_start, qt_end, qt_key', 'length', 'max'=>20),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('qt_id, qt_name, qt_start, qt_end, qt_key', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'queues' => array(self::HAS_MANY, 'Queue', 'qt_id'),
			'queueLists' => array(self::HAS_MANY, 'QueueList', 'qt_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'qt_id' => 'Qt',
			'qt_name' => 'Qt Name',
			'qt_start' => 'Qt Start',
			'qt_end' => 'Qt End',
			'qt_key' => 'Qt Key',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('qt_id',$this->qt_id,true);
		$criteria->compare('qt_name',$this->qt_name,true);
		$criteria->compare('qt_start',$this->qt_start,true);
		$criteria->compare('qt_end',$this->qt_end,true);
		$criteria->compare('qt_key',$this->qt_key,true);

		return new CActiveDataProvider($this, array(
			'pagination'=>array(
				'pageSize'=> Yii::app()->user->getState('pageSize',Yii::app()->params['defaultPageSize']),
			),
			'criteria'=>$criteria,
		));
	}
	
	public function getAllowView()
	{
		$akses = UserAccess::model()->find(array("with"=>array("access"),'condition'=>"id_user='".Yii::app()->user->id."' and access.access_controller='QueueType' and access.access_action='view'"));
		if(!empty($akses)) return true; else return false;
	}
	
	public function getAllowUpdate()
	{
		$akses = UserAccess::model()->find(array("with"=>array("access"),'condition'=>"id_user='".Yii::app()->user->id."' and access.access_controller='QueueType' and access.access_action='update'"));
		if(!empty($akses)) return true; else return false;
	}
	
	public function getAllowDelete($id)
	{
		$akses = UserAccess::model()->find(array("with"=>array("access"),'condition'=>"id_user='".Yii::app()->user->id."' and access.access_controller='QueueType' and access.access_action='delete'"));
		$rel=$this->getEmptyRelation($id);
		if(!empty($akses) AND $rel==true){
			return true;
		}else{
			return false;
		}
	}
	
	
	public function getEmptyRelation($id)
	{
		$count=0;
		$count+=Queue::model()->count(array('condition'=>"qt_id='$id'"));
		$count+=QueueList::model()->count(array('condition'=>"qt_id='$id'"));
		if($count>0){
			return false;
		}else{
			return true;
		}
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return QueueType the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
