<?php

/**
 * This is the model class for table "jadwal_operasi".
 *
 * The followings are the available columns in table 'jadwal_operasi':
 * @property string $id_jadwal_operasi
 * @property string $id_pasien
 * @property string $id_registrasi
 * @property string $tanggal
 * @property string $jam
 * @property string $id_ruangan
 * @property string $id_jenis_operasi
 * @property integer $id_klasifikasi_operasi
 * @property string $id_dokter_operator
 * @property string $id_dokter_anestesi
 * @property string $id_icd10
 * @property string $id_icd9
 * @property string $status
 * @property string $waktu_mulai
 * @property string $waktu_selesai
 * @property string $keterangan
 * @property string $user_create
 * @property string $user_confirm
 * @property string $time_create
 * @property string $time_confirm
 *
 * The followings are the available model relations:
 * @property Pasien $idPasien
 * @property User $userCreate
 * @property User $userConfirm
 * @property Registrasi $idRegistrasi
 * @property Ruangan $idRuangan
 * @property JenisOperasi $idJenisOperasi
 * @property KlasifikasiOperasi $idKlasifikasiOperasi
 * @property Pegawai $idDokterOperator
 * @property Pegawai $idDokterAnestesi
 * @property Icd10 $idIcd10
 * @property Icd9 $idIcd9
 * @property TindakanPasien[] $tindakanPasiens
 */
class JadwalOperasi extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'jadwal_operasi';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('id_pasien, tanggal, jam, id_ruangan, id_jenis_operasi, id_klasifikasi_operasi, id_dokter_operator, id_icd10, id_icd9, status, keterangan, user_create, time_create', 'required'),
			array('id_klasifikasi_operasi', 'numerical', 'integerOnly'=>true),
			array('id_pasien, id_registrasi, id_ruangan, id_jenis_operasi, id_dokter_operator, id_dokter_anestesi, id_icd10, id_icd9, user_create, user_confirm', 'length', 'max'=>20),
			array('status', 'length', 'max'=>16),
			array('waktu_mulai, waktu_selesai, time_confirm', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id_jadwal_operasi, id_pasien, id_registrasi, tanggal, jam, id_ruangan, id_jenis_operasi, id_klasifikasi_operasi, id_dokter_operator, id_dokter_anestesi, id_icd10, id_icd9, status, waktu_mulai, waktu_selesai, keterangan, user_create, user_confirm, time_create, time_confirm', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'idPasien' => array(self::BELONGS_TO, 'Pasien', 'id_pasien'),
			'userCreate' => array(self::BELONGS_TO, 'User', 'user_create'),
			'userConfirm' => array(self::BELONGS_TO, 'User', 'user_confirm'),
			'idRegistrasi' => array(self::BELONGS_TO, 'Registrasi', 'id_registrasi'),
			'idRuangan' => array(self::BELONGS_TO, 'Ruangan', 'id_ruangan'),
			'idJenisOperasi' => array(self::BELONGS_TO, 'JenisOperasi', 'id_jenis_operasi'),
			'idKlasifikasiOperasi' => array(self::BELONGS_TO, 'KlasifikasiOperasi', 'id_klasifikasi_operasi'),
			'idDokterOperator' => array(self::BELONGS_TO, 'Pegawai', 'id_dokter_operator'),
			'idDokterAnestesi' => array(self::BELONGS_TO, 'Pegawai', 'id_dokter_anestesi'),
			'idIcd10' => array(self::BELONGS_TO, 'Icd10', 'id_icd10'),
			'idIcd9' => array(self::BELONGS_TO, 'Icd9', 'id_icd9'),
			'tindakanPasiens' => array(self::HAS_MANY, 'TindakanPasien', 'id_jadwal_operasi'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id_jadwal_operasi' => 'Id Jadwal Operasi',
			'id_pasien' => 'Id Pasien',
			'id_registrasi' => 'Id Registrasi',
			'tanggal' => 'Tanggal',
			'jam' => 'Jam',
			'id_ruangan' => 'Id Ruangan',
			'id_jenis_operasi' => 'Id Jenis Operasi',
			'id_klasifikasi_operasi' => 'Id Klasifikasi Operasi',
			'id_dokter_operator' => 'Id Dokter Operator',
			'id_dokter_anestesi' => 'Id Dokter Anestesi',
			'id_icd10' => 'Id Icd10',
			'id_icd9' => 'Id Icd9',
			'status' => 'Status',
			'waktu_mulai' => 'Waktu Mulai',
			'waktu_selesai' => 'Waktu Selesai',
			'keterangan' => 'Keterangan',
			'user_create' => 'User Create',
			'user_confirm' => 'User Confirm',
			'time_create' => 'Time Create',
			'time_confirm' => 'Time Confirm',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id_jadwal_operasi',$this->id_jadwal_operasi,true);
		$criteria->compare('id_pasien',$this->id_pasien,true);
		$criteria->compare('id_registrasi',$this->id_registrasi,true);
		$criteria->compare('tanggal',$this->tanggal,true);
		$criteria->compare('jam',$this->jam,true);
		$criteria->compare('id_ruangan',$this->id_ruangan,true);
		$criteria->compare('id_jenis_operasi',$this->id_jenis_operasi,true);
		$criteria->compare('id_klasifikasi_operasi',$this->id_klasifikasi_operasi);
		$criteria->compare('id_dokter_operator',$this->id_dokter_operator,true);
		$criteria->compare('id_dokter_anestesi',$this->id_dokter_anestesi,true);
		$criteria->compare('id_icd10',$this->id_icd10,true);
		$criteria->compare('id_icd9',$this->id_icd9,true);
		$criteria->compare('status',$this->status,true);
		$criteria->compare('waktu_mulai',$this->waktu_mulai,true);
		$criteria->compare('waktu_selesai',$this->waktu_selesai,true);
		$criteria->compare('keterangan',$this->keterangan,true);
		$criteria->compare('user_create',$this->user_create,true);
		$criteria->compare('user_confirm',$this->user_confirm,true);
		$criteria->compare('time_create',$this->time_create,true);
		$criteria->compare('time_confirm',$this->time_confirm,true);

		return new CActiveDataProvider($this, array(
			'pagination'=>array(
				'pageSize'=> Yii::app()->user->getState('pageSize',Yii::app()->params['defaultPageSize']),
			),
			'criteria'=>$criteria,
		));
	}
	
	public function getAllowView()
	{
		$akses = UserAccess::model()->find(array("with"=>array("access"),'condition'=>"id_user='".Yii::app()->user->id."' and access.access_controller='JadwalOperasi' and access.access_action='view'"));
		if(!empty($akses)) return true; else return false;
	}
	
	public function getAllowUpdate()
	{
		$akses = UserAccess::model()->find(array("with"=>array("access"),'condition'=>"id_user='".Yii::app()->user->id."' and access.access_controller='JadwalOperasi' and access.access_action='update'"));
		if(!empty($akses)) return true; else return false;
	}
	
	public function getAllowDelete($id)
	{
		$akses = UserAccess::model()->find(array("with"=>array("access"),'condition'=>"id_user='".Yii::app()->user->id."' and access.access_controller='JadwalOperasi' and access.access_action='delete'"));
		$rel=$this->getEmptyRelation($id);
		if(!empty($akses) AND $rel==true){
			return true;
		}else{
			return false;
		}
	}
	
	
	public function getEmptyRelation($id)
	{
		$count=0;
																								$count+=TindakanPasien::model()->count(array('condition'=>"id_jadwal_operasi='$id'"));
		if($count>0){
			return false;
		}else{
			return true;
		}
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return JadwalOperasi the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
