<?php

/**
 * This is the model class for table "bank_akun".
 *
 * The followings are the available columns in table 'bank_akun':
 * @property integer $id_bank_akun
 * @property integer $id_bank
 * @property string $no_rekening
 * @property string $cabang
 * @property string $nama_pemilik_rekening
 * @property string $status
 *
 * The followings are the available model relations:
 * @property Bank $idBank
 */
class BankAkun extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'bank_akun';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('id_bank, no_rekening, cabang, nama_pemilik_rekening, status', 'required'),
			array('id_bank', 'numerical', 'integerOnly'=>true),
			array('no_rekening', 'length', 'max'=>50),
			array('cabang, nama_pemilik_rekening', 'length', 'max'=>100),
			array('status', 'length', 'max'=>1),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id_bank_akun, id_bank, no_rekening, cabang, nama_pemilik_rekening, status', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'idBank' => array(self::BELONGS_TO, 'Bank', 'id_bank'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id_bank_akun' => 'Id Bank Akun',
			'id_bank' => 'Id Bank',
			'no_rekening' => 'No Rekening',
			'cabang' => 'Cabang',
			'nama_pemilik_rekening' => 'Nama Pemilik Rekening',
			'status' => 'Status',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id_bank_akun',$this->id_bank_akun);
		$criteria->compare('id_bank',$this->id_bank);
		$criteria->compare('no_rekening',$this->no_rekening,true);
		$criteria->compare('cabang',$this->cabang,true);
		$criteria->compare('nama_pemilik_rekening',$this->nama_pemilik_rekening,true);
		$criteria->compare('status',$this->status,true);

		return new CActiveDataProvider($this, array(
			'pagination'=>array(
				'pageSize'=> Yii::app()->user->getState('pageSize',Yii::app()->params['defaultPageSize']),
			),
			'criteria'=>$criteria,
		));
	}
	
	public function getAllowView()
	{
		$akses = UserAccess::model()->find(array("with"=>array("access"),'condition'=>"id_user='".Yii::app()->user->id."' and access.access_controller='BankAkun' and access.access_action='view'"));
		if(!empty($akses)) return true; else return false;
	}
	
	public function getAllowUpdate()
	{
		$akses = UserAccess::model()->find(array("with"=>array("access"),'condition'=>"id_user='".Yii::app()->user->id."' and access.access_controller='BankAkun' and access.access_action='update'"));
		if(!empty($akses)) return true; else return false;
	}
	
	public function getAllowDelete($id)
	{
		$akses = UserAccess::model()->find(array("with"=>array("access"),'condition'=>"id_user='".Yii::app()->user->id."' and access.access_controller='BankAkun' and access.access_action='delete'"));
		$rel=$this->getEmptyRelation($id);
		if(!empty($akses) AND $rel==true){
			return true;
		}else{
			return false;
		}
	}
	
	
	public function getEmptyRelation($id)
	{
		$count=0;
				if($count>0){
			return false;
		}else{
			return true;
		}
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return BankAkun the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
