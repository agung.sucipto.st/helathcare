<?php

/**
 * This is the model class for table "queue_counter".
 *
 * The followings are the available columns in table 'queue_counter':
 * @property string $qcc_id
 * @property string $queue_area_id
 * @property string $qcc_name
 * @property string $qcc_sound
 *
 * The followings are the available model relations:
 * @property Queue[] $queues
 * @property QueueCall[] $queueCalls
 * @property QueueArea $queueArea
 */
class QueueCounter extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'queue_counter';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('queue_area_id', 'required'),
			array('queue_area_id', 'length', 'max'=>20),
			array('qcc_name', 'length', 'max'=>45),
			array('qcc_sound', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('qcc_id, queue_area_id, qcc_name, qcc_sound', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'queues' => array(self::HAS_MANY, 'Queue', 'qcc_id'),
			'queueCalls' => array(self::HAS_MANY, 'QueueCall', 'qcc_id'),
			'queueArea' => array(self::BELONGS_TO, 'QueueArea', 'queue_area_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'qcc_id' => 'Qcc',
			'queue_area_id' => 'Queue Area',
			'qcc_name' => 'Qcc Name',
			'qcc_sound' => 'Qcc Sound',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('qcc_id',$this->qcc_id,true);
		$criteria->compare('queue_area_id',$this->queue_area_id,true);
		$criteria->compare('qcc_name',$this->qcc_name,true);
		$criteria->compare('qcc_sound',$this->qcc_sound,true);

		return new CActiveDataProvider($this, array(
			'pagination'=>array(
				'pageSize'=> Yii::app()->user->getState('pageSize',Yii::app()->params['defaultPageSize']),
			),
			'criteria'=>$criteria,
		));
	}
	
	public function getAllowView()
	{
		$akses = UserAccess::model()->find(array("with"=>array("access"),'condition'=>"id_user='".Yii::app()->user->id."' and access.access_controller='QueueCounter' and access.access_action='view'"));
		if(!empty($akses)) return true; else return false;
	}
	
	public function getAllowUpdate()
	{
		$akses = UserAccess::model()->find(array("with"=>array("access"),'condition'=>"id_user='".Yii::app()->user->id."' and access.access_controller='QueueCounter' and access.access_action='update'"));
		if(!empty($akses)) return true; else return false;
	}
	
	public function getAllowDelete($id)
	{
		$akses = UserAccess::model()->find(array("with"=>array("access"),'condition'=>"id_user='".Yii::app()->user->id."' and access.access_controller='QueueCounter' and access.access_action='delete'"));
		$rel=$this->getEmptyRelation($id);
		if(!empty($akses) AND $rel==true){
			return true;
		}else{
			return false;
		}
	}
	
	
	public function getEmptyRelation($id)
	{
		$count=0;
		$count+=Queue::model()->count(array('condition'=>"qcc_id='$id'"));
		$count+=QueueCall::model()->count(array('condition'=>"qcc_id='$id'"));
				if($count>0){
			return false;
		}else{
			return true;
		}
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return QueueCounter the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
