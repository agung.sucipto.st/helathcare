<?php
class Lib extends CFormModel
{
	public static function getMac(){
		$_IP_SERVER = $_SERVER['SERVER_ADDR'];
		$_IP_ADDRESS = $_SERVER['REMOTE_ADDR']; 
		if($_IP_ADDRESS == $_IP_SERVER)
		{
			ob_start();
			system('ipconfig /all');
			$_PERINTAH  = ob_get_contents();
			ob_clean();
			$_PECAH = strpos($_PERINTAH, "Physical");
			$_HASIL = substr($_PERINTAH,($_PECAH+36),17);
		} else {
			$_PERINTAH = "arp -a $_IP_ADDRESS";
			ob_start();
			system($_PERINTAH);
			$_HASIL = ob_get_contents();
			ob_clean();
			$_PECAH = strstr($_HASIL, $_IP_ADDRESS);
			$_PECAH_STRING = explode($_IP_ADDRESS, str_replace(" ", "", $_PECAH));
			$_HASIL = substr($_PECAH_STRING[1], 0, 17);
		}
		return $_HASIL;
	}
	public static function dateConvert($ex){
		
		$pecah=explode(" ",$ex);
		$tanggal = $pecah[0];
		$bulan = Lib::getBulanIndReserve($pecah[1]);
		$tahun = $pecah[2];
		return $tahun.'-'.$bulan.'-'.$tanggal;	
	}
	
	public static function dateInd($ex, $day=true){
			$pecah=explode(" ",$ex);
			$nameofDay='';
			if($day==true){
				$nameofDay=Lib::getNamaHari($pecah[0]).', ';
			}else{
				$nameofDay='';
			}
			if(!empty($pecah[1])){
				$tgl=$pecah[0];
				$tanggal = substr($tgl,8,2);
				$bulan = Lib::getBulan(substr($tgl,5,2));
				$tahun = substr($tgl,0,4);
				return $nameofDay.$tanggal.' '.$bulan.' '.$tahun.' '.$pecah[1].' WIB';	
			}else{
				$tgl=$pecah[0];
				$tanggal = substr($tgl,8,2);
				$bulan = Lib::getBulan(substr($tgl,5,2));
				$tahun = substr($tgl,0,4);
				return $nameofDay.$tanggal.' '.$bulan.' '.$tahun;	
			}		
	}
	
	public static function dateIndShortMonth($ex, $day=true){
			$pecah=explode(" ",$ex);
			$nameofDay='';
			if($day==true){
				$nameofDay=Lib::getNamaHari($pecah[0]).', ';
			}else{
				$nameofDay='';
			}
			if(!empty($pecah[1])){
				$tgl=$pecah[0];
				$tanggal = substr($tgl,8,2);
				$bulan = Lib::getBulanInd(substr($tgl,5,2));
				$tahun = substr($tgl,0,4);
				return $tanggal.' '.$bulan.' '.$tahun.' '.$pecah[1];	
			}else{
				$tgl=$pecah[0];
				$tanggal = substr($tgl,8,2);
				$bulan = Lib::getBulan(substr($tgl,5,2));
				$tahun = substr($tgl,0,4);
				return $tanggal.' '.$bulan.' '.$tahun;	
			}		
	}
	
	public static function yearMonth($ex){
			$pecah=explode(" ",$ex);
		
				$tgl=$pecah[0];
				$tanggal = substr($tgl,8,2);
				$bulan = Lib::getBulan(substr($tgl,5,2));
				$tahun = substr($tgl,0,4);
				return $bulan.' '.$tahun;	
			
	}
	public static function dateInd2($ex, $day=true, $type){
			$pecah=explode(" ",$ex);
			$nameofDay='';
			if($day==true){
				$nameofDay=Lib::getNamaHari($pecah[0]).', ';
			}else{
				$nameofDay='';
			}
			if(!empty($pecah[1])){
				$tgl=$pecah[0];
				$tanggal = substr($tgl,8,2);
				$bulan = Lib::getBulan(substr($tgl,5,2));
				$tahun = substr($tgl,0,4);
				if($type==1){
					return $nameofDay.$tanggal.' '.$bulan.' '.$tahun;
				} else {
					return $pecah[1].' WIB';
				}
					
			}else{
				$tgl=$pecah[0];
				$tanggal = substr($tgl,8,2);
				$bulan = Lib::getBulan(substr($tgl,5,2));
				$tahun = substr($tgl,0,4);
				return $nameofDay.$tanggal.' '.$bulan.' '.$tahun;	
			}		
	}
	
	public static function cut_text($x,$n)
	{
		$kata=strtok(strip_tags($x)," "); 
		$new="";
		for ($i=1; $i<=$n; $i++){    //membatasi berapa kata yang akan ditampilkan di halaman utama
			$new.=$kata;		//tulis isi agenda
			$new.=" ";
			$kata=strtok(" ");
		}
		return $new;
	}
	
	
	public static function cek_img_tag($text,$original)
	{
		//membuat auto thumbnails
		@preg_match("/src=\"(.+)\"/",$text,$cocok);
		@$patern= explode("\"",$cocok[1]);
		$img = str_replace("\"/>","",$patern[0]);
		$img = str_replace("../","",$img);
		$img = str_replace("/>","",$img);
		if($img=="")
		{
		$img=$original;
		}
		else
		{
		$img=str_replace("\&quot;","",$img);
		
		}
		
		return $img;
	}
	
	public static function simbolRemoving($title)
	{
		$linkbaru=strtolower($title);
		$tanda=array("|",",","\"","'",".","(",")","-","_",":",";","?","!","@","#","\$","%","^","&","*","+","/","\\",">","<","\r","\t","\n");
		$rep=stripslashes(str_replace($tanda,"",$linkbaru));
		$rep=stripslashes(str_replace('  ',"-",$rep));
		$rep=stripslashes(str_replace(' ',"-",$rep));
		return $rep;
	}
	
	public static function getNamaHari($date){
		$namahari = date('D', strtotime($date));
		//Function date(String1, strtotime(String2)); adalah fungsi untuk mendapatkan nama hari
		return Lib::getHari($namahari);
	}
	public static function getDay($date){
		$namahari = date('D', strtotime($date));
		//Function date(String1, strtotime(String2)); adalah fungsi untuk mendapatkan nama hari
		return $namahari;
	}
	
	public static function getHari($hari){
		switch ($hari){
			case 'Mon': 
				return "Senin";
			break;
			case 'Tue': 
				return "Selasa";
			break;
			case 'Wed': 
				return "Rabu";
			break;
			case 'Thu': 
				return "Kamis";
			break;
			case 'Fri': 
				return "Jumat";
			break;
			case 'Sat': 
				return "Sabtu";
			break;
			case 'Sun': 
				return "Minggu";
			break;
		}
	}
	
	public static function getBulanInd($bln){
		switch ($bln){
			case 1: 
				return "Jan";
				break;
			case 2:
				return "Feb";
				break;
			case 3:
				return "Mar";
				break;
			case 4:
				return "Apr";
				break;
			case 5:
				return "Mei";
				break;
			case 6:
				return "Jun";
				break;
			case 7:
				return "Jul";
				break;
			case 8:
				return "Agu";
				break;
			case 9:
				return "Sep";
				break;
			case 10:
				return "Okt";
				break;
			case 11:
				return "Nov";
				break;
			case 12:
				return "Des";
				break;
		}
	}
	
	public static function getBulanIndReserve($bln){
		switch ($bln){
			case "Jan": 
				return 1;
				break;
			case "Feb":
				return 2;
				break;
			case "Mar":
				return 3;
				break;
			case "Apr":
				return 4;
				break;
			case "Mei":
				return 5;
				break;
			case "Jun":
				return 6;
				break;
			case "June":
				return 6;
				break;
			case "Jul":
				return 7;
				break;
			case "July":
				return 7;
				break;
			case "Agu":
				return 8;
				break;
			case "Agust":
				return 8;
				break;
			case "Sep":
				return 9;
				break;
			case "Sept":
				return 9;
				break;
			case "Okt":
				return 10;
				break;
			case "Nov":
				return 11;
				break;
			case "Nop":
				return 11;
				break;
			case "Des":
				return 12;
				break;
		}
	}
	
	public static function getBulan($bln){
		switch ($bln){
			case 1: 
				return "Januari";
				break;
			case 2:
				return "Februari";
				break;
			case 3:
				return "Maret";
				break;
			case 4:
				return "April";
				break;
			case 5:
				return "Mei";
				break;
			case 6:
				return "Juni";
				break;
			case 7:
				return "Juli";
				break;
			case 8:
				return "Agustus";
				break;
			case 9:
				return "September";
				break;
			case 10:
				return "Oktober";
				break;
			case 11:
				return "November";
				break;
			case 12:
				return "Desember";
				break;
		}
	}
	
	public static function getBrowser() 
	{ 
		$u_agent = $_SERVER['HTTP_USER_AGENT']; 
		$bname = 'Unknown';
		$platform = 'Unknown';
		$version= "";

		//First get the platform?
		if (preg_match('/linux/i', $u_agent)) {
			$platform = 'linux';
		}
		elseif (preg_match('/macintosh|mac os x/i', $u_agent)) {
			$platform = 'mac';
		}
		elseif (preg_match('/windows|win32/i', $u_agent)) {
			$platform = 'windows';
		}

		// Next get the name of the useragent yes seperately and for good reason
		if(preg_match('/MSIE/i',$u_agent) && !preg_match('/Opera/i',$u_agent)) 
		{ 
			$bname = 'Internet Explorer'; 
			$ub = "MSIE"; 
		} 
		elseif(preg_match('/Firefox/i',$u_agent)) 
		{ 
			$bname = 'Mozilla Firefox'; 
			$ub = "Firefox"; 
		} 
		elseif(preg_match('/Chrome/i',$u_agent)) 
		{ 
			$bname = 'Google Chrome'; 
			$ub = "Chrome"; 
		} 
		elseif(preg_match('/Safari/i',$u_agent)) 
		{ 
			$bname = 'Apple Safari'; 
			$ub = "Safari"; 
		} 
		elseif(preg_match('/Opera/i',$u_agent)) 
		{ 
			$bname = 'Opera'; 
			$ub = "Opera"; 
		} 
		elseif(preg_match('/Netscape/i',$u_agent)) 
		{ 
			$bname = 'Netscape'; 
			$ub = "Netscape"; 
		} 

		// finally get the correct version number
		$known = array('Version', $ub, 'other');
		$pattern = '#(?<browser>' . join('|', $known) .
		')[/ ]+(?<version>[0-9.|a-zA-Z.]*)#';
		if (!preg_match_all($pattern, $u_agent, $matches)) {
			// we have no matching number just continue
		}

		// see how many we have
		$i = count($matches['browser']);
		if ($i != 1) {
			//we will have two since we are not using 'other' argument yet
			//see if version is before or after the name
			if (strripos($u_agent,"Version") < strripos($u_agent,$ub)){
				$version= $matches['version'][0];
			}
			else {
				$version= $matches['version'][1];
			}
		}
		else {
			$version= $matches['version'][0];
		}

		// check if we have a number
		if ($version==null || $version=="") {$version="?";}
		$IP = $_SERVER['REMOTE_ADDR'];

		return array(
			'userAgent' => $u_agent,
			'name'      => $bname,
			'version'   => $version,
			'platform'  => $platform,
			'IP' => $IP,
		);
	} 


	public static function selisih2tanggal($tgl1,$tgl2){
		$pecah1 = explode("-", $tgl1);
		$date1 = $pecah1[2];
		$month1 = $pecah1[1];
		$year1 = $pecah1[0];

		// memecah tanggal untuk mendapatkan bagian tanggal, bulan dan tahun
		// dari tanggal kedua

		$pecah2 = explode("-", $tgl2);
		$date2 = $pecah2[2];
		$month2 = $pecah2[1];
		$year2 =  $pecah2[0];

		// menghitung JDN dari masing-masing tanggal

		$jd1 = GregorianToJD($month1, $date1, $year1);
		$jd2 = GregorianToJD($month2, $date2, $year2);

		// hitung selisih hari kedua tanggal

		$selisih = $jd2 - $jd1;

		return $selisih;
	}
	
	public static function Umur($tgl1){
		$pecah1 = explode("-", $tgl1);
		$date1 = $pecah1[2];
		$month1 = $pecah1[1];
		$year1 = $pecah1[0];
		$tgl2=date("Y-m-d");
		// memecah tanggal untuk mendapatkan bagian tanggal, bulan dan tahun
		// dari tanggal kedua

		$pecah2 = explode("-", $tgl2);
		$date2 = $pecah2[2];
		$month2 = $pecah2[1];
		$year2 =  $pecah2[0];

		// menghitung JDN dari masing-masing tanggal

		$jd1 = GregorianToJD($month1, $date1, $year1);
		$jd2 = GregorianToJD($month2, $date2, $year2);

		// hitung selisih hari kedua tanggal

		$selisih = $jd2 - $jd1;
		$umur=$selisih/365;
		return floor($umur);
	}
	
	public static function umurDetail($ex){
		$pecah=explode(" ",$ex);
		$tgl=$pecah[0];
		$tgl_lahir=substr($tgl,8,2);
		$bln_lahir=substr($tgl,5,2);
		$thn_lahir=substr($tgl,0,4);
		$tanggal_today = date('d');
		$bulan_today=date('m');
		$tahun_today = date('Y');
		$harilahir=gregoriantojd($bln_lahir,$tgl_lahir,$thn_lahir);
		//menghitung jumlah hari sejak tahun 0 masehi
		$hariini=gregoriantojd($bulan_today,$tanggal_today,$tahun_today);
		//menghitung jumlah hari sejak tahun 0 masehi
		$umur=$hariini-$harilahir;
		//menghitung selisih hari antara tanggal sekarang dengan tanggal lahir
		$tahun=$umur/365;//menghitung usia tahun
		$sisa=$umur%365;//sisa pembagian dari tahun untuk menghitung bulan
		$bulan=$sisa/30;//menghitung usia bulan
		$hari=$sisa%30;//menghitung sisa hari
		$lahir= "$tgl_lahir-$bln_lahir-$thn_lahir";
		$today= "$tanggal_today-$bulan_today-$tahun_today";
		if(floor($tahun)>0 AND floor($bulan)>0 AND $hari>0){
			return floor($tahun)." Thn ".floor($bulan)." Bln $hari Hr";
		}elseif(floor($tahun)>0 AND floor($bulan)==0 AND $hari>0){
			return floor($tahun)." Thn $hari Hr";
		}elseif(floor($tahun)>0 AND floor($bulan)>0 AND $hari==0){
			return floor($tahun)." Thn ".floor($bulan)." Bln";
		}elseif(floor($tahun)==0 AND floor($bulan)>0 AND $hari>0){
			return floor($bulan)." Bln $hari Hr";
		}elseif(floor($tahun)==0 AND floor($bulan)==0 AND $hari>0){
			return "$hari Hr";
		}elseif(floor($tahun)>0 AND floor($bulan)==0 AND $hari==0){
			return floor($tahun)." Thn";
		}
		
	}
	
	public function tglIndo($ex){
	
		$pecah=explode(" ",$ex);
		$tgl=$pecah[0];
		$tanggal = substr($tgl,8,2);
		$bulan = $this->getBulan(substr($tgl,5,2));
		$tahun = substr($tgl,0,4);
		return $tanggal.' '.$bulan.' '.$tahun;	

	}
	
	
	public function timeDiff($time1,$time2){
		if($time2<=$time1){
			$w1=explode(":",$time1);
			$w2=explode(":",$time2);
			
			$s1=($w1[0]*3600)+($w1[1]*60)+$w1[2];
			$s2=($w2[0]*3600)+($w2[1]*60)+$w2[2];
			
			$dif=$s1-$s2;
			
			$jam=floor($dif/3600);
			$menit=floor(($dif-($jam*3600))/60);
			$detik=$dif-($jam*3600)-($menit*60);
			$jam=str_pad((int) $jam,2,"0",STR_PAD_LEFT);
			$menit=str_pad((int) $menit,2,"0",STR_PAD_LEFT);
			$detik=str_pad((int) $detik,2,"0",STR_PAD_LEFT);
			
			return $jam.':'.$menit.':'.$detik;
		}else{
			$w1=explode(":",$time1);
			$w2=explode(":",$time2);
			
			$s1=($w1[0]*3600)+($w1[1]*60)+$w1[2];
			$s2=($w2[0]*3600)+($w2[1]*60)+$w2[2];
			
			$dif=$s2-$s1;
			
			$jam=floor($dif/3600);
			$menit=floor(($dif-($jam*3600))/60);
			$detik=$dif-($jam*3600)-($menit*60);
			$jam=str_pad((int) $jam,2,"0",STR_PAD_LEFT);
			$menit=str_pad((int) $menit,2,"0",STR_PAD_LEFT);
			$detik=str_pad((int) $detik,2,"0",STR_PAD_LEFT);
			
			return $jam.':'.$menit.':'.$detik;
		}
	}
	
	
	public function Late($time1,$time2){
		if($time2!=""){
			if($time2<=$time1){
				return "00:00:00";
			}else{
				$w1=explode(":",$time1);
				$w1[1]+=5;
				$w2=explode(":",$time2);
				
				$s1=($w1[0]*3600)+($w1[1]*60)+$w1[2];
				$s2=($w2[0]*3600)+($w2[1]*60)+$w2[2];
				
				if($s2<=$s1){
					return "00:00:00";
				}else{
					$dif=$s2-$s1;
					$jam=floor($dif/3600);
					$menit=floor(($dif-($jam*3600))/60);
					$detik=$dif-($jam*3600)-($menit*60);
					$jam=str_pad((int) $jam,2,"0",STR_PAD_LEFT);
					$menit=str_pad((int) $menit,2,"0",STR_PAD_LEFT);
					$detik=str_pad((int) $detik,2,"0",STR_PAD_LEFT);
					
					return $jam.':'.$menit.':'.$detik;
				}
			}
		}else{
			return'';
		}
	}
	
	public function QuickHome($time1,$time2,$time3){
		if($time2<$time3)
		{
			return "00:00:00";
		}else{
			if($time2<=$time1){
				$w1=explode(":",$time1);
				$w2=explode(":",$time2);
				
				$s1=($w1[0]*3600)+($w1[1]*60)+$w1[2];
				$s2=($w2[0]*3600)+($w2[1]*60)+$w2[2];
				
				$dif=$s1-$s2;
				
				$jam=floor($dif/3600);
				$menit=floor(($dif-($jam*3600))/60);
				$detik=$dif-($jam*3600)-($menit*60);
				$jam=str_pad((int) $jam,2,"0",STR_PAD_LEFT);
				$menit=str_pad((int) $menit,2,"0",STR_PAD_LEFT);
				$detik=str_pad((int) $detik,2,"0",STR_PAD_LEFT);
				
				return $jam.':'.$menit.':'.$detik;
				
			}else{
				return "00:00:00";
			}
		}
	}
	
	
	public function OverTime($time1,$time2){
		if($time2>$time1){
			$w1=explode(":",$time1);
			$w2=explode(":",$time2);
			
			$s1=($w1[0]*3600)+($w1[1]*60)+$w1[2];
			$s2=($w2[0]*3600)+($w2[1]*60)+$w2[2];
			
			$dif=$s2-$s1;
			
			$jam=floor($dif/3600);
			$menit=floor(($dif-($jam*3600))/60);
			$detik=$dif-($jam*3600)-($menit*60);
			$jam=str_pad((int) $jam,2,"0",STR_PAD_LEFT);
			$menit=str_pad((int) $menit,2,"0",STR_PAD_LEFT);
			$detik=str_pad((int) $detik,2,"0",STR_PAD_LEFT);
			
			return $jam.':'.$menit.':'.$detik;
			
		}else{
			return "00:00:00";
		}
	}
	
	public static function TotalJam($time){
		$dif=0;
		foreach($time as $row){
			$w1=explode(":",$row);
			$dif+=($w1[0]*3600)+($w1[1]*60)+$w1[2];
		}	
		
		$jam=floor($dif/3600);
		$menit=floor(($dif-($jam*3600))/60);
		$detik=$dif-($jam*3600)-($menit*60);
		$jam=str_pad((int) $jam,2,"0",STR_PAD_LEFT);
		$menit=str_pad((int) $menit,2,"0",STR_PAD_LEFT);
		$detik=str_pad((int) $detik,2,"0",STR_PAD_LEFT);
		
		return $jam.':'.$menit.':'.$detik;
	}

	public function WO($attIn,$attOut,$shiftIn,$shiftOut){
		if(!empty($attIn) AND !empty($attOut)){
			if($shiftIn<$shiftOut){
				
				if($attIn<=$shiftIn){
					$start=$shiftIn;
				}else{
					$start=$attIn;
				}
				
				if($attOut>=$shiftOut){
					$end=$shiftOut;
				}else{
					if($attOut>'00:00:00'){
						$end=$shiftOut;
					}else{
						$end=$attOut;
					}
					
				}
				//tidak lintas hari
				$w1=explode(":",$start);
				$w2=explode(":",$end);
				
				$s1=($w1[0]*3600)+($w1[1]*60)+$w1[2];
				$s2=($w2[0]*3600)+($w2[1]*60)+$w2[2];
				
				$dif=$s2-$s1;
				
				$jam=floor($dif/3600);
				$menit=floor(($dif-($jam*3600))/60);
				$detik=$dif-($jam*3600)-($menit*60);
				$jam=str_pad((int) $jam,2,"0",STR_PAD_LEFT);
				$menit=str_pad((int) $menit,2,"0",STR_PAD_LEFT);
				$detik=str_pad((int) $detik,2,"0",STR_PAD_LEFT);
				
				return $jam.':'.$menit.':'.$detik;
				
			}else{
				
				if($attIn<=$shiftIn){
					$start=$shiftIn;
				}else{
					$start=$attIn;
				}
				
				if($attOut>=$shiftOut){
					$end=$shiftOut;
				}else{
					$end=$attOut;
				}
				//tidak lintas hari
				$w1=explode(":",$start);
				$w2=explode(":",$end);
				
				$change="24:00:00";
				$changeDay="00:00:00";
				$w3=explode(":",$change);
				$s2=($w2[0]*3600)+($w2[1]*60)+$w2[2];
				$s1=($w1[0]*3600)+($w1[1]*60)+$w1[2];
				$s3=($w3[0]*3600)+($w3[1]*60)+$w3[2];
				
				$dif=($s3-$s1)+$s2;
				
				$jam=floor($dif/3600);
				$menit=floor(($dif-($jam*3600))/60);
				$detik=$dif-($jam*3600)-($menit*60);
				$jam=str_pad((int) $jam,2,"0",STR_PAD_LEFT);
				$menit=str_pad((int) $menit,2,"0",STR_PAD_LEFT);
				$detik=str_pad((int) $detik,2,"0",STR_PAD_LEFT);
				
				return $jam.':'.$menit.':'.$detik;
				//lintas hari
			}
		}else{
			return "00:00:00";
		}
	}

	public static function Parse_Data($data,$p1,$p2){
	
		$data=" ".$data;
		$hasil="";
		$awal=strpos($data,$p1);
		if($awal!=""){
			$akhir=strpos(strstr($data,$p1),$p2);
			if($akhir!=""){
				$hasil=substr($data,$awal+strlen($p1),$akhir-strlen($p1));
			}
		}
		return $hasil;
	}
	
	public static function strpos_arr($haystack, $needle) {
		if(!is_array($needle)){
			$needle = array($needle);
		}
		foreach($needle as $what) {
			if(($pos = strpos($haystack, $what))!==false){
				return $pos;
			}
		}
		return false;
	}
	
	public static function UploadCompress($new_name,$file,$dir,$quality,$dest){
	  //direktori gambar
	  $vdir_upload = $dir;

	  //Simpan gambar dalam ukuran sebenarnya
	  $source_url=$dir.$file;
	  $info = getimagesize($source_url);

		if ($info['mime'] == 'image/jpeg'){ 
			$image = imagecreatefromjpeg($source_url); 
			//$ext='.jpg';
		}
		elseif($info['mime'] == 'image/gif'){ 
			$image = imagecreatefromgif($source_url); 
			//$ext='.gif';
		}elseif($info['mime'] == 'image/png'){ 
			$image = imagecreatefrompng($source_url); 
			//$ext='.png';
		}
	  
		if(imagejpeg($image, $dest.$new_name, $quality)){
			//unlink($source_url);
			return true;
		}else{
			//unlink($source_url);
			return false;
		}
	  
	}
	
	public static function MRN($mrn){
		$temp= trim(sprintf("%08d", $mrn));
		$temp= rtrim($temp);
		$temp=substr(chunk_split($temp, 2, '-'),0,-1);
		return $temp;
	}
	public static function getMonthDate($from,$to){
		$a=explode("-",$from);
		$b=explode("-",$to);
		$return=array();
		$day=Lib::selisih2tanggal($from,$to);
		for($i=0;$i<=$day;$i++){
			$next=date("Y-m-d",mktime(0,0,0,$a[1],$a[2]+$i,$a[0]));
			$ex=explode("-",$next);
			$return[$ex[1]]++;
		}
		return $return;
	}
	
	public static function Terbilang($x)
	{
	  $abil = array("", "satu", "dua", "tiga", "empat", "lima", "enam", "tujuh", "delapan", "sembilan", "sepuluh", "sebelas");
	  if ($x < 12)
		return " " . $abil[$x];
	  elseif ($x < 20)
		return Lib::Terbilang($x - 10) . " belas";
	  elseif ($x < 100)
		return Lib::Terbilang($x / 10) . " puluh" . Lib::Terbilang($x % 10);
	  elseif ($x < 200)
		return " seratus" . Lib::Terbilang($x - 100);
	  elseif ($x < 1000)
		return Lib::Terbilang($x / 100) . " ratus" . Lib::Terbilang($x % 100);
	  elseif ($x < 2000)
		return " seribu" . Lib::Terbilang($x - 1000);
	  elseif ($x < 1000000)
		return Lib::Terbilang($x / 1000) . " ribu" . Lib::Terbilang($x % 1000);
	  elseif ($x < 1000000000)
		return Lib::Terbilang($x / 1000000) . " juta" . Lib::Terbilang($x % 1000000);
	}
	
	public static function MenuManager($array,$parent_id = 0)
	{
	  $normal=0;
	  $menu='';
	  foreach($array as $element)
	  {
		if($element->menu_arrange_parent==$parent_id)
		{
		  $normal++;
		  $menu.= '<li class="dd-item dd3-item" data-id="'.$element->menu_arrange_id.'"><div class="dd-handle dd3-handle"></div><div class="dd3-content"><i class=""></i> '.((!empty($element->menu_arrange_name)?$element->menu_arrange_name:$element->access->access_name)).'<span class="pull-right">
		  ';
		  if($element->menu_arrange_name!=""){
		  $menu.='<a href="'.Yii::app()->createUrl(Yii::app()->controller->id.'/update',array("id"=>$element->menu_arrange_id)).'" class="btn btn-xs btn-default"><i class="fa fa-pencil"></i></a>';
		  }
		  if(Lib::MenuChild($element->menu_arrange_id)==0){
			  $menu.='<a href="'.Yii::app()->createUrl(Yii::app()->controller->id.'/delete',array("id"=>$element->menu_arrange_id)).'" class="btn btn-xs btn-default" onClick="return confirm(\'Apakah Yakin Akan Menghapus Data?\');"><i class="fa fa-trash-o"></i></a>';
		  }
		   $menu.='</span><br/></div>';
		  if(Lib::MenuManager($array,$element->menu_arrange_id)!='')
		  {
			  $menu.=Lib::MenuManager($array,$element->menu_arrange_id);
		  }

		  $menu.= '</li>';
		}
	  }
	  if($normal>0){
		  return '<ol class="dd-list">'.$menu.'</ol>';
	  }else{
		  return '';
	  }
	  
	}
	
	public static function MenuChild($id)
	{
		$model = AccessMenuArrange::model()->count(array("condition"=>"menu_arrange_parent='$id'"));
		return $model;
	}
	
	public static function accessBy($controller,$action){
		$akses = UserAccess::model()->find(array("with"=>array("access"),'condition'=>"id_user='".Yii::app()->user->id."' and access.access_controller='$controller' and access.access_action='$action'"));
		if(!empty($akses)) return true; else return false;
	}
	
	public static function getAccess($id){
		$akses = UserAccess::model()->find(array('condition'=>"id_user='".Yii::app()->user->id."' and access_id='$id'"));
		if(!empty($akses)) return true; else return false;
	}
	
	public static function GrantedMenu($array,$parent_id = 0)
	{
	  $normal=array();
	  $temp_array = array();
	  foreach($array as $element)
	  {
		if($element->menu_arrange_parent==$parent_id)
		{
		  $normal['label']=($element->menu_arrange_name=="")?$element->access->access_name:$element->menu_arrange_name;
		  $normal['icon']=$element->menu_icon!="" ? $element->menu_icon : '';
		  $normal['url']=($element->menu_arrange_name=="")?Yii::app()->createUrl($element->access->access_controller.'/'.$element->access->access_action):"#";
		  
		  if($element->menu_arrange_name==""){
			$normal['visible']=Lib::getAccess($element->access_id);
		  }else{
			$nromal['visible']=true;  
		  }
		  $child= Lib::GrantedMenu($array,$element->menu_arrange_id);
		  if(isset($child)){
			$normal['items'] =  $child;
			foreach($normal['items'] as $row){
				if($row['visible']==true){
					$normal['visible']=true;
				}
			}
		  }
		  $temp_array[] = $normal;
		}
	  }
	  return $temp_array;
	}
	
	
	public static function LeftMenu($array,$head='<ul class="sidebar-menu">')
	{
	  $normal=0;
	  if(!empty($array)){
		  $menu=$head;
	  }
	  
	  foreach($array as $element)
	  {
		  $normal++;
		  if($element['visible']==1){
		  if(Lib::LeftMenu($element['items'],'<ul class="treeview-menu">')!='')
		  {
			  $menu.= '<li class="treeview"><a href="'.$element['url'].'"><i class="'.$element['icon'].'"></i>
		<span class="title">'.$element['label'].'</span>';
			  $menu.='<i class="fa pull-right fa-angle-left"></i></a>';
			  $menu.=Lib::LeftMenu($element['items'],'<ul class="treeview-menu">');
		  }else{
			  $menu.= '<li><a href="'.$element['url'].'"><i class="'.$element['icon'].'"></i>
		<span class="title">'.$element['label'].'</span>';
			  $menu.='</a>';
		  }
		  $menu.= '</li>';
		  }
	  }
	   if(!empty($array)){
		  $menu.= '</ul>';
		  return $menu;
		}else{
			return '';
		}
	}
	
	
	public static function TopMenu($array,$head='<ul class="nav navbar-nav">',$level=0, $first = 0)
	{
	  $init=0;
	  $normal=0;
	  $sub='';
	  $caret='';
	  if(!empty($array)){
		  if($level==0){
			  $menu='<ul class="nav navbar-nav">';
			  $sub='';
			  $caret='<span class="caret"></span>';
		  }else if($level==1){
			  $menu='<ul class="dropdown-menu" role="menu">';
			  $sub='-submenu';
			  $caret='';
		  }else{
			  $menu='<ul class="dropdown-menu">';
			  $sub='-submenu';
			  $caret='';
		  }
	  }
	  
	  foreach($array as $element)
	  {
		  if($element['visible']==1){
		  $normal++;
		  
		  if(Lib::TopMenu($element['items'],'<ul class="treeview-menu">',$level+1)!='')
		  {
			  $menu.= '<li class="dropdown'.$sub.'"><a href="'.$element['url'].'" class="dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
		'.$element['label'].$caret;
			  $menu.='</a>';
			  $menu.=Lib::TopMenu($element['items'],'<ul class="treeview-menu">',$level+1, $first+1);
		  }else{
			  if($first > 0){
				  $menu.= '<li><a href="'.$element['url'].'">
				'.$element['label'].'';
					  $menu.='</a>';
			  } else {
				  $menu.= '<li><a href="'.$element['url'].'"><i class="'.(!empty($element['icon']) ? $element['icon'] : 'fa fa-home').'"></i></a>';
			  }
			  
		  }
		  $menu.= '</li>';
		  }
		  $first++;
	  }
	   if(!empty($array)){
		  $menu.= '</ul>';
		  return $menu;
		}else{
			return '';
		}
	}
	
	public static function UserAccessTree($array,$parent_id = 0,$level=0,$user,$checkbox)
	{
	 if(!empty($array)){
		  if($level==0){
			  $styleClass='class="checkboxes"';
		  }else{
			  $styleClass='';
		  }
	  }
	  $normal=0;
	  $menu='';
	  foreach($array as $element)
	  {
		if($element->access_parent==$parent_id)
		{
		  $normal++;
		  $ua=UserAccess::model()->find(array("condition"=>"id_user='".$user."' and access_id='$element->access_id'"));
			if(!empty($ua)){
				$checked="checked";
			}else{
				$checked='';
			}
		  $menu.= '<li>';
		  if($checkbox==true){
		  $menu.='<input type="checkbox" name="access[]" id="access'.$element->access_id.'" value="'.$element->access_id.'" '.$checked.'>';
		  }
		  $menu.='
				 <label for="access'.$element->access_id.'" class="custom-unchecked">'.$element->access_name.'</label>
		  ';
		  if(self::UserAccessTree($array,$element->access_id,$level+1,$user,$checkbox)!=''){
			  $menu.=self::UserAccessTree($array,$element->access_id,$level+1,$user,$checkbox);
		}

		  $menu.= '</li>';
		}
	  }
	  if($normal>0){
		  return '<ul '.$styleClass.'>'.$menu.'</ul>';
	  }else{
		  return '';
	  }
	  
	}
}