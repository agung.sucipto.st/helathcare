<?php

/**
 * This is the model class for table "daftar_purchase_order".
 *
 * The followings are the available columns in table 'daftar_purchase_order':
 * @property string $id_daftar_purchase_order
 * @property string $id_purchase_order
 * @property string $id_daftar_purchase_request
 * @property string $id_item
 * @property string $id_item_satuan_besar
 * @property string $id_item_satuan_kecil
 * @property integer $jumlah_order
 * @property integer $jumlah_diterima
 * @property string $keterangan
 * @property string $status
 * @property integer $harga_beli_item
 * @property integer $discount_item
 *
 * The followings are the available model relations:
 * @property Item $idItem
 * @property ItemSatuan $idItemSatuanBesar
 * @property ItemSatuan $idItemSatuanKecil
 * @property PurchaseOrder $idPurchaseOrder
 * @property DaftarPurchaseRequest $idDaftarPurchaseRequest
 */
class DaftarPurchaseOrder extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'daftar_purchase_order';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('id_purchase_order, id_item, id_item_satuan_besar, id_item_satuan_kecil, jumlah_order, jumlah_diterima, keterangan, status, harga_beli_item, discount_item', 'required'),
			array('jumlah_order, jumlah_diterima, harga_beli_item, discount_item', 'numerical', 'integerOnly'=>true),
			array('id_purchase_order, id_daftar_purchase_request, id_item, id_item_satuan_besar, id_item_satuan_kecil', 'length', 'max'=>20),
			array('status', 'length', 'max'=>17),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id_daftar_purchase_order, id_purchase_order, id_daftar_purchase_request, id_item, id_item_satuan_besar, id_item_satuan_kecil, jumlah_order, jumlah_diterima, keterangan, status, harga_beli_item, discount_item', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'idItem' => array(self::BELONGS_TO, 'Item', 'id_item'),
			'idItemSatuanBesar' => array(self::BELONGS_TO, 'ItemSatuan', 'id_item_satuan_besar'),
			'idItemSatuanKecil' => array(self::BELONGS_TO, 'ItemSatuan', 'id_item_satuan_kecil'),
			'idPurchaseOrder' => array(self::BELONGS_TO, 'PurchaseOrder', 'id_purchase_order'),
			'idDaftarPurchaseRequest' => array(self::BELONGS_TO, 'DaftarPurchaseRequest', 'id_daftar_purchase_request'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id_daftar_purchase_order' => 'Id Daftar Purchase Order',
			'id_purchase_order' => 'Id Purchase Order',
			'id_daftar_purchase_request' => 'Id Daftar Purchase Request',
			'id_item' => 'Id Item',
			'id_item_satuan_besar' => 'Id Item Satuan Besar',
			'id_item_satuan_kecil' => 'Id Item Satuan Kecil',
			'jumlah_order' => 'Jumlah Order',
			'jumlah_diterima' => 'Jumlah Diterima',
			'keterangan' => 'Keterangan',
			'status' => 'Status',
			'harga_beli_item' => 'Harga Beli Item',
			'discount_item' => 'Discount Item',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id_daftar_purchase_order',$this->id_daftar_purchase_order,true);
		$criteria->compare('id_purchase_order',$this->id_purchase_order,true);
		$criteria->compare('id_daftar_purchase_request',$this->id_daftar_purchase_request,true);
		$criteria->compare('id_item',$this->id_item,true);
		$criteria->compare('id_item_satuan_besar',$this->id_item_satuan_besar,true);
		$criteria->compare('id_item_satuan_kecil',$this->id_item_satuan_kecil,true);
		$criteria->compare('jumlah_order',$this->jumlah_order);
		$criteria->compare('jumlah_diterima',$this->jumlah_diterima);
		$criteria->compare('keterangan',$this->keterangan,true);
		$criteria->compare('status',$this->status,true);
		$criteria->compare('harga_beli_item',$this->harga_beli_item);
		$criteria->compare('discount_item',$this->discount_item);

		return new CActiveDataProvider($this, array(
			'pagination'=>array(
				'pageSize'=> Yii::app()->user->getState('pageSize',Yii::app()->params['defaultPageSize']),
			),
			'criteria'=>$criteria,
		));
	}
	
	public function getAllowView()
	{
		$akses = UserAccess::model()->find(array("with"=>array("access"),'condition'=>"id_user='".Yii::app()->user->id."' and access.access_controller='DaftarPurchaseOrder' and access.access_action='view'"));
		if(!empty($akses)) return true; else return false;
	}
	
	public function getAllowUpdate()
	{
		$akses = UserAccess::model()->find(array("with"=>array("access"),'condition'=>"id_user='".Yii::app()->user->id."' and access.access_controller='DaftarPurchaseOrder' and access.access_action='update'"));
		if(!empty($akses)) return true; else return false;
	}
	
	public function getAllowDelete($id)
	{
		$akses = UserAccess::model()->find(array("with"=>array("access"),'condition'=>"id_user='".Yii::app()->user->id."' and access.access_controller='DaftarPurchaseOrder' and access.access_action='delete'"));
		$rel=$this->getEmptyRelation($id);
		if(!empty($akses) AND $rel==true){
			return true;
		}else{
			return false;
		}
	}
	
	
	public function getEmptyRelation($id)
	{
		$count=0;
												if($count>0){
			return false;
		}else{
			return true;
		}
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return DaftarPurchaseOrder the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
