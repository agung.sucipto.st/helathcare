<?php

/**
 * This is the model class for table "daftar_piutang_pasien".
 *
 * The followings are the available columns in table 'daftar_piutang_pasien':
 * @property string $id_daftar_piutang_pasien
 * @property string $id_piutang_pasien
 * @property string $id_pasien
 * @property string $id_pembayaran_tagihan
 * @property string $id_registrasi
 * @property string $nilai_piutang
 * @property string $nilai_dibayarkan
 * @property string $status_piutang
 * @property string $status_penagihan
 * @property string $waktu_pembayaran
 *
 * The followings are the available model relations:
 * @property PiutangPasien $idPiutangPasien
 * @property Pasien $idPasien
 * @property PembayaranTagihan $idPembayaranTagihan
 * @property Registrasi $idRegistrasi
 */
class DaftarPiutangPasien extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'daftar_piutang_pasien';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('id_pasien, id_pembayaran_tagihan, id_registrasi, nilai_piutang, status_piutang', 'required'),
			array('id_piutang_pasien, id_pasien, id_pembayaran_tagihan, id_registrasi', 'length', 'max'=>20),
			array('nilai_piutang, nilai_dibayarkan', 'length', 'max'=>30),
			array('status_piutang', 'length', 'max'=>11),
			array('status_penagihan', 'length', 'max'=>22),
			array('waktu_pembayaran', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id_daftar_piutang_pasien, id_piutang_pasien, id_pasien, id_pembayaran_tagihan, id_registrasi, nilai_piutang, nilai_dibayarkan, status_piutang, status_penagihan, waktu_pembayaran', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'idPiutangPasien' => array(self::BELONGS_TO, 'PiutangPasien', 'id_piutang_pasien'),
			'idPasien' => array(self::BELONGS_TO, 'Pasien', 'id_pasien'),
			'idPembayaranTagihan' => array(self::BELONGS_TO, 'PembayaranTagihan', 'id_pembayaran_tagihan'),
			'idRegistrasi' => array(self::BELONGS_TO, 'Registrasi', 'id_registrasi'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id_daftar_piutang_pasien' => 'Id Daftar Piutang Pasien',
			'id_piutang_pasien' => 'Id Piutang Pasien',
			'id_pasien' => 'Id Pasien',
			'id_pembayaran_tagihan' => 'Id Pembayaran Tagihan',
			'id_registrasi' => 'Id Registrasi',
			'nilai_piutang' => 'Nilai Piutang',
			'nilai_dibayarkan' => 'Nilai Dibayarkan',
			'status_piutang' => 'Status Piutang',
			'status_penagihan' => 'Status Penagihan',
			'waktu_pembayaran' => 'Waktu Pembayaran',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id_daftar_piutang_pasien',$this->id_daftar_piutang_pasien,true);
		$criteria->compare('id_piutang_pasien',$this->id_piutang_pasien,true);
		$criteria->compare('id_pasien',$this->id_pasien,true);
		$criteria->compare('id_pembayaran_tagihan',$this->id_pembayaran_tagihan,true);
		$criteria->compare('id_registrasi',$this->id_registrasi,true);
		$criteria->compare('nilai_piutang',$this->nilai_piutang,true);
		$criteria->compare('nilai_dibayarkan',$this->nilai_dibayarkan,true);
		$criteria->compare('status_piutang',$this->status_piutang,true);
		$criteria->compare('status_penagihan',$this->status_penagihan,true);
		$criteria->compare('waktu_pembayaran',$this->waktu_pembayaran,true);

		return new CActiveDataProvider($this, array(
			'pagination'=>array(
				'pageSize'=> Yii::app()->user->getState('pageSize',Yii::app()->params['defaultPageSize']),
			),
			'criteria'=>$criteria,
		));
	}
	
	public function getAllowView()
	{
		$akses = UserAccess::model()->find(array("with"=>array("access"),'condition'=>"id_user='".Yii::app()->user->id."' and access.access_controller='DaftarPiutangPasien' and access.access_action='view'"));
		if(!empty($akses)) return true; else return false;
	}
	
	public function getAllowUpdate()
	{
		$akses = UserAccess::model()->find(array("with"=>array("access"),'condition'=>"id_user='".Yii::app()->user->id."' and access.access_controller='DaftarPiutangPasien' and access.access_action='update'"));
		if(!empty($akses)) return true; else return false;
	}
	
	public function getAllowDelete($id)
	{
		$akses = UserAccess::model()->find(array("with"=>array("access"),'condition'=>"id_user='".Yii::app()->user->id."' and access.access_controller='DaftarPiutangPasien' and access.access_action='delete'"));
		$rel=$this->getEmptyRelation($id);
		if(!empty($akses) AND $rel==true){
			return true;
		}else{
			return false;
		}
	}
	
	
	public function getEmptyRelation($id)
	{
		$count=0;
										if($count>0){
			return false;
		}else{
			return true;
		}
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return DaftarPiutangPasien the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
