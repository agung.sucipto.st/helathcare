<?php

/**
 * This is the model class for table "registrasi".
 *
 * The followings are the available columns in table 'registrasi':
 * @property string $id_registrasi
 * @property string $q_id
 * @property string $id_kelas
 * @property string $id_departemen
 * @property string $id_pasien
 * @property string $no_registrasi
 * @property string $jenis_registrasi
 * @property string $kode_registrasi
 * @property string $no_urut
 * @property string $waktu_registrasi
 * @property string $waktu_tutup_registrasi
 * @property string $jenis_jaminan
 * @property string $titip_ruangan
 * @property string $id_kelas_titipan
 * @property string $rujukan
 * @property string $id_faskes
 * @property string $status_registrasi
 * @property string $status_pembayaran
 * @property string $status_keluar
 * @property string $id_faskes_tujuan_rujuk
 * @property string $catatan
 * @property string $jenis_kasus
 * @property string $tipe_emergency
 * @property string $line_emergency
 * @property string $user_create
 * @property string $user_update
 * @property string $user_final
 * @property string $time_create
 * @property string $time_update
 * @property string $time_final
 *
 * The followings are the available model relations:
 * @property AdministrasiPasien[] $administrasiPasiens
 * @property BedPasien[] $bedPasiens
 * @property DaftarPiutangPerusahaan[] $daftarPiutangPerusahaans
 * @property DaftarTagihan[] $daftarTagihans
 * @property Dpjp[] $dpjps
 * @property Icd10Pasien[] $icd10Pasiens
 * @property Icd9Pasien[] $icd9Pasiens
 * @property ItemPasien[] $itemPasiens
 * @property ItemPasienGroup[] $itemPasienGroups
 * @property JaminanPasien[] $jaminanPasiens
 * @property LaboratoriumPasien[] $laboratoriumPasiens
 * @property PeralatanPasien[] $peralatanPasiens
 * @property RadiologyPasien[] $radiologyPasiens
 * @property Departemen $idDepartemen
 * @property Faskes $idFaskes
 * @property Faskes $idFaskesTujuanRujuk
 * @property Kelas $idKelas
 * @property Kelas $idKelasTitipan
 * @property Pasien $idPasien
 * @property Queue $q
 * @property User $userCreate
 * @property User $userUpdate
 * @property User $userFinal
 * @property ResepPasien[] $resepPasiens
 * @property ResponsTime[] $responsTimes
 * @property RuanganPasien[] $ruanganPasiens
 * @property SoapPasien[] $soapPasiens
 * @property TagihanPasien[] $tagihanPasiens
 * @property TindakanPasien[] $tindakanPasiens
 * @property VisiteDokterPasien[] $visiteDokterPasiens
 */
class Registrasi extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public $id_penjamin,$no_kartu,$fasilitas_jaminan,$nama_pemegang_kartu,$id_jenis_hubungan,$no_eligibilitas,$tgl_eligibilitas,$id_pegawai,$id_dokter,$inc,$nama_lengkap,$jenis_kelamin;
	
	public $wkt_reg,$lk,$pr,$searchPol,$ip_from,$id_bed;
	public function tableName()
	{
		return 'registrasi';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('status_medis, status_keluar', 'required','on'=>"discharge"),
			array('id_departemen, no_registrasi, jenis_registrasi, waktu_registrasi, jenis_jaminan, user_create, time_create', 'required','on'=>"otc"),
			array('id_departemen, no_registrasi, jenis_registrasi, waktu_registrasi, jenis_jaminan, user_create, time_create,id_dokter,jenis_kunjungan', 'required','on'=>"igd"),
			array('id_departemen, no_registrasi, jenis_registrasi, waktu_registrasi, jenis_jaminan, user_create, time_create,id_dokter,jenis_kunjungan, ip_from, id_bed, id_kelas', 'required','on'=>"ranap"),
			array('q_id, id_kelas, id_departemen, id_pasien, jenis_registrasi, no_urut, id_kelas_titipan, id_faskes, id_faskes_tujuan_rujuk, user_create, user_update, user_final', 'length', 'max'=>20),
			array('no_registrasi', 'length', 'max'=>10),
			array('kode_registrasi, status_registrasi, status_pembayaran, jenis_kasus', 'length', 'max'=>45),
			array('jenis_jaminan', 'length', 'max'=>7),
			array('titip_ruangan,jenis_kunjungan', 'length', 'max'=>1),
			array('rujukan', 'length', 'max'=>17),
			array('tipe_emergency, line_emergency', 'length', 'max'=>60),
			array('waktu_tutup_registrasi, catatan, time_update, time_final', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id_registrasi, q_id, id_kelas, id_departemen, id_pasien, no_registrasi, jenis_registrasi, kode_registrasi, no_urut, waktu_registrasi, waktu_tutup_registrasi, jenis_jaminan, titip_ruangan, id_kelas_titipan, rujukan, id_faskes, status_registrasi, status_pembayaran, status_keluar, id_faskes_tujuan_rujuk, catatan, jenis_kasus, tipe_emergency, line_emergency, user_create, user_update, user_final, time_create, time_update, time_final', 'safe', 'on'=>'search'),
			array('id_registrasi, q_id, id_kelas, id_departemen, id_pasien, no_registrasi, jenis_registrasi, kode_registrasi, no_urut, waktu_registrasi, waktu_tutup_registrasi, jenis_jaminan, titip_ruangan, id_kelas_titipan, rujukan, id_faskes, status_registrasi, status_pembayaran, status_keluar, id_faskes_tujuan_rujuk, catatan, jenis_kasus, tipe_emergency, line_emergency, user_create, user_update, user_final, time_create, time_update, time_final,nama_lengkap', 'safe', 'on'=>'searchPembayaran'),
			array('id_registrasi, q_id, id_kelas, id_departemen, id_pasien, no_registrasi, jenis_registrasi, kode_registrasi, no_urut, waktu_registrasi, waktu_tutup_registrasi, jenis_jaminan, titip_ruangan, id_kelas_titipan, rujukan, id_faskes, status_registrasi, status_pembayaran, status_keluar, id_faskes_tujuan_rujuk, catatan, jenis_kasus, tipe_emergency, line_emergency, user_create, user_update, user_final, time_create, time_update, time_final,nama_lengkap', 'safe', 'on'=>'serachActive'),
			array('id_registrasi, q_id, id_kelas, id_departemen, id_pasien, no_registrasi, jenis_registrasi, kode_registrasi, no_urut, waktu_registrasi, waktu_tutup_registrasi, jenis_jaminan, titip_ruangan, id_kelas_titipan, rujukan, id_faskes, status_registrasi, status_pembayaran, status_keluar, id_faskes_tujuan_rujuk, catatan, jenis_kasus, tipe_emergency, line_emergency, user_create, user_update, user_final, time_create, time_update, time_final,nama_lengkap,searchPol, id_dokter', 'safe', 'on'=>'searchPoli'),
			array('waktu_registrasi', 'safe', 'on'=>'searchLaporan'),
			array('id_departemen', 'findUnique', 'on' => 'ranap'),
			array('id_departemen', 'findUnique', 'on' => 'igd'),
		);
	}
	
	public function findUnique($attribute, $params)
    {
        $find = Registrasi::model()->find(array("condition"=>"id_pasien='$this->id_pasien' AND id_departemen='".$this->id_departemen."' AND status_registrasi='Aktif'"));
		
        if ($find){
			$this->addError($attribute, 'Layanan '.$find->idDepartemen->nama_departemen.' Masih Aktif Dengan No Registrasi : <a href="'.Yii::app()->createUrl('registrasi/info',array("id"=>$find->id_pasien,"idReg"=>$find->id_registrasi)).'">'.$find->no_registrasi.'</a>');
		}
    }

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'administrasiPasiens' => array(self::HAS_MANY, 'AdministrasiPasien', 'id_registrasi'),
			'bedPasiens' => array(self::HAS_MANY, 'BedPasien', 'id_registrasi'),
			'daftarPiutangPerusahaans' => array(self::HAS_MANY, 'DaftarPiutangPerusahaan', 'id_registrasi'),
			'daftarTagihans' => array(self::HAS_MANY, 'DaftarTagihan', 'id_registrasi'),
			'dpjps' => array(self::HAS_MANY, 'Dpjp', 'id_registrasi'),
			'icd10Pasiens' => array(self::HAS_MANY, 'Icd10Pasien', 'id_registrasi'),
			'icd9Pasiens' => array(self::HAS_MANY, 'Icd9Pasien', 'id_registrasi'),
			'itemPasiens' => array(self::HAS_MANY, 'ItemPasien', 'id_registrasi'),
			'itemPasienGroups' => array(self::HAS_MANY, 'ItemPasienGroup', 'id_registrasi'),
			'jaminanPasiens' => array(self::HAS_MANY, 'JaminanPasien', 'id_registrasi'),
			'laboratoriumPasiens' => array(self::HAS_MANY, 'LaboratoriumPasien', 'id_registrasi'),
			'peralatanPasiens' => array(self::HAS_MANY, 'PeralatanPasien', 'id_registrasi'),
			'radiologyPasiens' => array(self::HAS_MANY, 'RadiologyPasien', 'id_registrasi'),
			'tandaVitalPasiens' => array(self::HAS_MANY, 'TandaVitalPasien', 'id_registrasi'),
			'idDepartemen' => array(self::BELONGS_TO, 'Departemen', 'id_departemen'),
			'idFaskes' => array(self::BELONGS_TO, 'Faskes', 'id_faskes'),
			'idPegawaiPerujuk' => array(self::BELONGS_TO, 'Pegawai', 'id_pegawai_perujuk'),
			'idFaskesTujuanRujuk' => array(self::BELONGS_TO, 'Faskes', 'id_faskes_tujuan_rujuk'),
			'idKelas' => array(self::BELONGS_TO, 'Kelas', 'id_kelas'),
			'idKelasTitipan' => array(self::BELONGS_TO, 'Kelas', 'id_kelas_titipan'),
			'idPasien' => array(self::BELONGS_TO, 'Pasien', 'id_pasien'),
			'alasanPembatalan' => array(self::BELONGS_TO, 'AlasanPembatalan', 'alasan_pembatalan'),
			'q' => array(self::BELONGS_TO, 'Queue', 'q_id'),
			'userCreate' => array(self::BELONGS_TO, 'User', 'user_create'),
			'userUpdate' => array(self::BELONGS_TO, 'User', 'user_update'),
			'userFinal' => array(self::BELONGS_TO, 'User', 'user_final'),
			'berkasPasiens' => array(self::HAS_MANY, 'BerkasPasien', 'id_registrasi'),
			'resepPasiens' => array(self::HAS_MANY, 'ResepPasien', 'id_registrasi'),
			'responsTimes' => array(self::HAS_MANY, 'ResponsTime', 'id_registrasi'),
			'ruanganPasiens' => array(self::HAS_MANY, 'RuanganPasien', 'id_registrasi'),
			'soapPasiens' => array(self::HAS_MANY, 'SoapPasien', 'id_registrasi'),
			'tagihanPasiens' => array(self::HAS_MANY, 'TagihanPasien', 'id_registrasi'),
			'tindakanPasiens' => array(self::HAS_MANY, 'TindakanPasien', 'id_registrasi'),
			'visiteDokterPasiens' => array(self::HAS_MANY, 'VisiteDokterPasien', 'id_registrasi'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'ip_from' => 'Masuk Dari',
			'id_bed' => 'Bed Pasien',
			'id_registrasi' => 'Registrasi',
			'q_id' => 'Q',
			'id_kelas' => 'Kelas',
			'id_departemen' => 'Departemen',
			'id_pasien' => 'No MR',
			'no_registrasi' => 'No Registrasi',
			'jenis_kunjungan' => 'Jenis Kunjungan',
			'jenis_registrasi' => 'Jenis Registrasi',
			'kode_registrasi' => 'Kode Registrasi',
			'no_urut' => 'No Urut',
			'waktu_registrasi' => 'Waktu Registrasi',
			'waktu_tutup_registrasi' => 'Waktu Tutup Registrasi',
			'jenis_jaminan' => 'Jenis Jaminan',
			'titip_ruangan' => 'Titip Ruangan',
			'id_kelas_titipan' => 'Kelas Titipan',
			'rujukan' => 'Rujukan',
			'id_faskes' => 'Faskes Asal Rujukan',
			'id_pegawai_perujuk' => 'Perujuk',
			'status_registrasi' => 'Status Registrasi',
			'status_pembayaran' => 'Status Pembayaran',
			'status_keluar' => 'Status Keluar',
			'id_faskes_tujuan_rujuk' => 'Faskes Tujuan Rujuk',
			'catatan' => 'Catatan',
			'jenis_kasus' => 'Jenis Kasus',
			'tipe_emergency' => 'Tipe Emergency',
			'line_emergency' => 'Line Emergency',
			'user_create' => 'User Create',
			'user_update' => 'User Update',
			'user_final' => 'User Final',
			'time_create' => 'Time Create',
			'time_update' => 'Time Update',
			'time_final' => 'Time Final',
			
			'id_dokter'=>'Dokter',
			'id_penjamin'=>'Penjamin',
			'no_kartu'=>'No Kartu',
			'fasilitas_jaminan'=>'Fasilitas',
			'nama_pemegang_kartu'=>'Nama Pemegang Kartu',
			'id_jenis_hubungan'=>'Hubungan Pemilik Kartu',
			'no_eligibilitas'=>'No Eligibilitas',
			'tgl_eligibilitas'=>'Tanggal Eligibilitas'
		);
	}

	public static function getRegNumber($date){
		$criteria=new CDbCriteria;
		$criteria->select="RIGHT(no_registrasi,4) as inc";
		$criteria->order="RIGHT(no_registrasi,4) DESC";
		$criteria->limit=1;
		$criteria->condition="DATE(waktu_registrasi)='$date' and status_registrasi!='Batal'";
		
		$last=Registrasi::model()->find($criteria);
		
		if(empty($last)){
			return date("ymd", strtotime($date)).sprintf("%04s",1);
		}else{
			return date("ymd", strtotime($date)).sprintf("%04s",$last->inc+1);
		}
	}
	
	public static function getQueue($date, $dept, $doc){
		$criteria=new CDbCriteria;
		$criteria->with=array('dpjps');
		$criteria->alias="reg";
		$criteria->select="reg.no_urut as inc";
		$criteria->order="reg.no_urut DESC";
		$criteria->limit=1;
		$criteria->condition="DATE(reg.waktu_registrasi)='$date' and reg.status_registrasi!='Batal' AND reg.id_departemen='$dept' and dpjps.id_dokter='$doc'";
		$criteria->together=true;
		
		$last=Registrasi::model()->find($criteria);
		
		if(empty($last)){
			return 1;
		}else{
			return $last->inc+1;
		}
	}
	
	public static function getVisitType($idPasien){
		$criteria=new CDbCriteria;
		$criteria->condition="id_pasien='$idPasien'";
		
		$last=Registrasi::model()->count($criteria);
		
		if($last==0){
			return 1;
		}else{
			return 0;
		}
	}
	
	public static function getRegCode($idPasien,$regType){
		$criteria=new CDbCriteria;
		$criteria->condition="jenis_registrasi='$regType' and status_registrasi!='Batal'";
		
		$last=Registrasi::model()->count($criteria);
		
		return $regType.($last+1);
	}
	
	public static function getStatus($idReg){
		$criteria=new CDbCriteria;		
		$model=Registrasi::model()->findByPk($idReg);
		
		return $model->status_registrasi;
	}
	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;
		$criteria->condition="status_registrasi !='Batal'";
		$criteria->compare('id_registrasi',$this->id_registrasi,true);
		$criteria->compare('q_id',$this->q_id,true);
		$criteria->compare('id_kelas',$this->id_kelas,true);
		$criteria->compare('id_departemen',$this->id_departemen,true);
		$criteria->compare('id_pasien',$this->id_pasien,true);
		$criteria->compare('no_registrasi',$this->no_registrasi,true);
		$criteria->compare('jenis_registrasi',$this->jenis_registrasi,true);
		$criteria->compare('kode_registrasi',$this->kode_registrasi,true);
		$criteria->compare('no_urut',$this->no_urut,true);
		$criteria->compare('waktu_registrasi',$this->waktu_registrasi,true);
		$criteria->compare('waktu_tutup_registrasi',$this->waktu_tutup_registrasi,true);
		$criteria->compare('jenis_jaminan',$this->jenis_jaminan,true);
		$criteria->compare('titip_ruangan',$this->titip_ruangan,true);
		$criteria->compare('id_kelas_titipan',$this->id_kelas_titipan,true);
		$criteria->compare('rujukan',$this->rujukan,true);
		$criteria->compare('id_faskes',$this->id_faskes,true);
		$criteria->compare('status_registrasi',$this->status_registrasi,true);
		$criteria->compare('status_pembayaran',$this->status_pembayaran,true);
		$criteria->compare('status_keluar',$this->status_keluar,true);
		$criteria->compare('id_faskes_tujuan_rujuk',$this->id_faskes_tujuan_rujuk,true);
		$criteria->compare('catatan',$this->catatan,true);
		$criteria->compare('jenis_kasus',$this->jenis_kasus,true);
		$criteria->compare('tipe_emergency',$this->tipe_emergency,true);
		$criteria->compare('line_emergency',$this->line_emergency,true);
		$criteria->compare('user_create',$this->user_create,true);
		$criteria->compare('user_update',$this->user_update,true);
		$criteria->compare('user_final',$this->user_final,true);
		$criteria->compare('time_create',$this->time_create,true);
		$criteria->compare('time_update',$this->time_update,true);
		$criteria->compare('time_final',$this->time_final,true);

		return new CActiveDataProvider($this, array(
			'pagination'=>array(
				'pageSize'=> Yii::app()->user->getState('pageSize',Yii::app()->params['defaultPageSize']),
			),
			'criteria'=>$criteria,
		));
	}
	
	public function searchActive()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;
		$criteria->with=array("idPasien","idPasien.idPersonal");
		$criteria->condition="jenis_registrasi !='OTC'";
		$criteria->compare('idPersonal.nama_lengkap',$this->nama_lengkap,true);
		$criteria->compare('id_registrasi',$this->id_registrasi,true);
		$criteria->compare('q_id',$this->q_id,true);
		$criteria->compare('id_kelas',$this->id_kelas,true);
		$criteria->compare('id_departemen',$this->id_departemen,true);
		$criteria->compare('idPasien.id_pasien',$this->id_pasien,true);
		$criteria->compare('no_registrasi',$this->no_registrasi,true);
		$criteria->compare('kode_registrasi',$this->kode_registrasi,true);
		$criteria->compare('no_urut',$this->no_urut,true);
		$criteria->compare('waktu_registrasi',$this->waktu_registrasi,true);
		$criteria->compare('waktu_tutup_registrasi',$this->waktu_tutup_registrasi,true);
		$criteria->compare('jenis_jaminan',$this->jenis_jaminan,true);
		$criteria->compare('titip_ruangan',$this->titip_ruangan,true);
		$criteria->compare('id_kelas_titipan',$this->id_kelas_titipan,true);
		$criteria->compare('rujukan',$this->rujukan,true);
		$criteria->compare('id_faskes',$this->id_faskes,true);
		$criteria->compare('status_registrasi',$this->status_registrasi,true);
		$criteria->compare('status_pembayaran',$this->status_pembayaran,true);
		$criteria->compare('status_keluar',$this->status_keluar,true);
		$criteria->compare('id_faskes_tujuan_rujuk',$this->id_faskes_tujuan_rujuk,true);
		$criteria->compare('catatan',$this->catatan,true);
		$criteria->compare('jenis_kasus',$this->jenis_kasus,true);
		$criteria->compare('tipe_emergency',$this->tipe_emergency,true);
		$criteria->compare('line_emergency',$this->line_emergency,true);
		$criteria->compare('user_create',$this->user_create,true);
		$criteria->compare('user_update',$this->user_update,true);
		$criteria->compare('user_final',$this->user_final,true);
		$criteria->compare('time_create',$this->time_create,true);
		$criteria->compare('time_update',$this->time_update,true);
		$criteria->compare('time_final',$this->time_final,true);
		

		return new CActiveDataProvider($this, array(
			'pagination'=>array(
				'pageSize'=> Yii::app()->user->getState('pageSize',Yii::app()->params['defaultPageSize']),
			),
			'sort'=>array(
			   'defaultOrder'=>'waktu_registrasi DESC',
			),

			'criteria'=>$criteria,
		));
	}
	
	public function searchPendaftaran()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;
		$criteria->with=array("idPasien","idPasien.idPersonal");
		$criteria->condition="jenis_registrasi !='OTC'";
		$criteria->compare('idPersonal.nama_lengkap',$this->nama_lengkap,true);
		$criteria->compare('id_registrasi',$this->id_registrasi,true);
		$criteria->compare('q_id',$this->q_id,true);
		$criteria->compare('id_kelas',$this->id_kelas,true);
		$criteria->compare('id_departemen',$this->id_departemen,true);
		$criteria->compare('idPasien.id_pasien',$this->id_pasien,true);
		$criteria->compare('no_registrasi',$this->no_registrasi,true);
		$criteria->compare('jenis_registrasi',$this->jenis_registrasi,true);
		$criteria->compare('kode_registrasi',$this->kode_registrasi,true);
		$criteria->compare('no_urut',$this->no_urut,true);
		$criteria->compare('waktu_registrasi',$this->waktu_registrasi,true);
		$criteria->compare('waktu_tutup_registrasi',$this->waktu_tutup_registrasi,true);
		$criteria->compare('jenis_jaminan',$this->jenis_jaminan,true);
		$criteria->compare('titip_ruangan',$this->titip_ruangan,true);
		$criteria->compare('id_kelas_titipan',$this->id_kelas_titipan,true);
		$criteria->compare('rujukan',$this->rujukan,true);
		$criteria->compare('id_faskes',$this->id_faskes,true);
		$criteria->compare('status_registrasi',$this->status_registrasi,true);
		$criteria->compare('status_pembayaran',$this->status_pembayaran,true);
		$criteria->compare('status_keluar',$this->status_keluar,true);
		$criteria->compare('id_faskes_tujuan_rujuk',$this->id_faskes_tujuan_rujuk,true);
		$criteria->compare('catatan',$this->catatan,true);
		$criteria->compare('jenis_kasus',$this->jenis_kasus,true);
		$criteria->compare('tipe_emergency',$this->tipe_emergency,true);
		$criteria->compare('line_emergency',$this->line_emergency,true);
		$criteria->compare('user_create',$this->user_create,true);
		$criteria->compare('user_update',$this->user_update,true);
		$criteria->compare('user_final',$this->user_final,true);
		$criteria->compare('time_create',$this->time_create,true);
		$criteria->compare('time_update',$this->time_update,true);
		$criteria->compare('time_final',$this->time_final,true);
		

		return new CActiveDataProvider($this, array(
			'pagination'=>array(
				'pageSize'=> Yii::app()->user->getState('pageSize',Yii::app()->params['defaultPageSize']),
			),
			'sort'=>array(
			   'defaultOrder'=>'waktu_registrasi DESC',
			),

			'criteria'=>$criteria,
		));
	}
	
	public function searchPembayaran()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;
		$criteria->with=array("idPasien","idPasien.idPersonal");
		$criteria->condition="status_registrasi !='Batal'";
		$criteria->compare('idPersonal.nama_lengkap',$this->nama_lengkap,true);
		$criteria->compare('id_registrasi',$this->id_registrasi,true);
		$criteria->compare('q_id',$this->q_id,true);
		$criteria->compare('id_kelas',$this->id_kelas,true);
		$criteria->compare('id_departemen',$this->id_departemen,true);
		$criteria->compare('idPasien.id_pasien',$this->id_pasien,true);
		$criteria->compare('no_registrasi',$this->no_registrasi,true);
		$criteria->compare('jenis_registrasi',$this->jenis_registrasi,true);
		$criteria->compare('kode_registrasi',$this->kode_registrasi,true);
		$criteria->compare('no_urut',$this->no_urut,true);
		$criteria->compare('waktu_registrasi',$this->waktu_registrasi,true);
		$criteria->compare('waktu_tutup_registrasi',$this->waktu_tutup_registrasi,true);
		$criteria->compare('jenis_jaminan',$this->jenis_jaminan,true);
		$criteria->compare('titip_ruangan',$this->titip_ruangan,true);
		$criteria->compare('id_kelas_titipan',$this->id_kelas_titipan,true);
		$criteria->compare('rujukan',$this->rujukan,true);
		$criteria->compare('id_faskes',$this->id_faskes,true);
		$criteria->compare('status_registrasi',$this->status_registrasi,true);
		$criteria->compare('status_pembayaran',$this->status_pembayaran,true);
		$criteria->compare('status_keluar',$this->status_keluar,true);
		$criteria->compare('id_faskes_tujuan_rujuk',$this->id_faskes_tujuan_rujuk,true);
		$criteria->compare('catatan',$this->catatan,true);
		$criteria->compare('jenis_kasus',$this->jenis_kasus,true);
		$criteria->compare('tipe_emergency',$this->tipe_emergency,true);
		$criteria->compare('line_emergency',$this->line_emergency,true);
		$criteria->compare('user_create',$this->user_create,true);
		$criteria->compare('user_update',$this->user_update,true);
		$criteria->compare('user_final',$this->user_final,true);
		$criteria->compare('time_create',$this->time_create,true);
		$criteria->compare('time_update',$this->time_update,true);
		$criteria->compare('time_final',$this->time_final,true);
		

		return new CActiveDataProvider($this, array(
			'pagination'=>array(
				'pageSize'=> Yii::app()->user->getState('pageSize',Yii::app()->params['defaultPageSize']),
			),
			'sort'=>array(
			   'defaultOrder'=>'waktu_registrasi DESC',
			),

			'criteria'=>$criteria,
		));
	}
	
	public function searchPoli()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.
		$criteria=new CDbCriteria;
		$criteria->distinct=true;
		$criteria->with=array("idPasien","idPasien.idPersonal","idDepartemen","dpjps");
		$criteria->condition="status_registrasi !='Batal'";
		$criteria->compare('idPersonal.nama_lengkap',$this->nama_lengkap,true);
		$criteria->compare('dpjps.id_dokter',$this->id_dokter, true);
		$criteria->compare('id_registrasi',$this->id_registrasi,true);
		$criteria->compare('q_id',$this->q_id,true);
		$criteria->compare('id_kelas',$this->id_kelas,true);
		$criteria->compare('idDepartemen.id_departemen',$this->id_departemen);
		$criteria->compare('idDepartemen.id_jenis_departemen',$this->searchPol);
		$criteria->compare('idPasien.id_pasien',$this->id_pasien,true);
		$criteria->compare('no_registrasi',$this->no_registrasi,true);
		$criteria->compare('jenis_registrasi',$this->jenis_registrasi,true);
		$criteria->compare('kode_registrasi',$this->kode_registrasi,true);
		$criteria->compare('no_urut',$this->no_urut,true);
		$criteria->compare('waktu_registrasi',$this->waktu_registrasi,true);
		$criteria->compare('waktu_tutup_registrasi',$this->waktu_tutup_registrasi,true);
		$criteria->compare('jenis_jaminan',$this->jenis_jaminan,true);
		$criteria->compare('titip_ruangan',$this->titip_ruangan,true);
		$criteria->compare('id_kelas_titipan',$this->id_kelas_titipan,true);
		$criteria->compare('rujukan',$this->rujukan,true);
		$criteria->compare('id_faskes',$this->id_faskes,true);
		$criteria->compare('status_registrasi',$this->status_registrasi,true);
		$criteria->compare('status_pembayaran',$this->status_pembayaran,true);
		$criteria->compare('status_keluar',$this->status_keluar,true);
		$criteria->compare('id_faskes_tujuan_rujuk',$this->id_faskes_tujuan_rujuk,true);
		$criteria->compare('catatan',$this->catatan,true);
		$criteria->compare('jenis_kasus',$this->jenis_kasus,true);
		$criteria->compare('tipe_emergency',$this->tipe_emergency,true);
		$criteria->compare('line_emergency',$this->line_emergency,true);
		$criteria->compare('user_create',$this->user_create,true);
		$criteria->compare('user_update',$this->user_update,true);
		$criteria->compare('user_final',$this->user_final,true);
		$criteria->compare('time_create',$this->time_create,true);
		$criteria->compare('time_update',$this->time_update,true);
		$criteria->compare('time_final',$this->time_final,true);
		$criteria->together=true;

		return new CActiveDataProvider($this, array(
			'pagination'=>array(
				'pageSize'=> Yii::app()->user->getState('pageSize',Yii::app()->params['defaultPageSize']),
			),
			'sort'=>array(
			   'defaultOrder'=>'idDepartemen.id_departemen,no_urut ASC',
			),

			'criteria'=>$criteria,
		));
	}
	
	public function searchLaporan()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;
		if($this->waktu_registrasi!=""){
			$list=explode(" - ",$this->waktu_registrasi);
			$start=explode("/",$list[0]);
			$end=explode("/",$list[1]);
			$dateStart=$start[2]."-".$start[0]."-".$start[1];
			$dateEnd=$end[2]."-".$end[0]."-".$end[1];
		}else{
			$dateStart=date("Y-m-d");
			$dateEnd=date("Y-m-d");
		}
		$criteria->condition="status_registrasi !='Batal' AND DATE(waktu_registrasi) between '$dateStart' AND '$dateEnd'";
		$criteria->compare('id_departemen',$this->id_departemen,true);
		$criteria->compare('status_keluar',$this->status_keluar,true);
		return new CActiveDataProvider($this, array(
			'pagination'=>array(
				'pageSize'=> 30,
			),
			'sort'=>array(
			   'defaultOrder'=>'waktu_registrasi DESC',
			),

			'criteria'=>$criteria,
		));
	}
	
	public function getAllowView()
	{
		$akses = UserAccess::model()->find(array("with"=>array("access"),'condition'=>"id_user='".Yii::app()->user->id."' and access.access_controller='Registrasi' and access.access_action='view'"));
		if(!empty($akses)) return true; else return false;
	}
	
	public function getAllowUpdate()
	{
		$akses = UserAccess::model()->find(array("with"=>array("access"),'condition'=>"id_user='".Yii::app()->user->id."' and access.access_controller='Registrasi' and access.access_action='update'"));
		if(!empty($akses)) return true; else return false;
	}
	
	public function getAllowDelete($id)
	{
		$akses = UserAccess::model()->find(array("with"=>array("access"),'condition'=>"id_user='".Yii::app()->user->id."' and access.access_controller='Registrasi' and access.access_action='delete'"));
		$rel=$this->getEmptyRelation($id);
		if(!empty($akses) AND $rel==true){
			return true;
		}else{
			return false;
		}
	}
	
	
	public function getEmptyRelation($id)
	{
		$count=0;
		$count+=AdministrasiPasien::model()->count(array('condition'=>"id_registrasi='$id'"));
		$count+=BedPasien::model()->count(array('condition'=>"id_registrasi='$id'"));
		$count+=DaftarPiutangPerusahaan::model()->count(array('condition'=>"id_registrasi='$id'"));
		$count+=DaftarTagihan::model()->count(array('condition'=>"id_registrasi='$id'"));
		$count+=Dpjp::model()->count(array('condition'=>"id_registrasi='$id'"));
		$count+=Icd10Pasien::model()->count(array('condition'=>"id_registrasi='$id'"));
		$count+=Icd9Pasien::model()->count(array('condition'=>"id_registrasi='$id'"));
		$count+=JaminanPasien::model()->count(array('condition'=>"id_registrasi='$id'"));
		$count+=LaboratoriumPasien::model()->count(array('condition'=>"id_registrasi='$id'"));
		$count+=PeralatanPasien::model()->count(array('condition'=>"id_registrasi='$id'"));
		$count+=RadiologyPasien::model()->count(array('condition'=>"id_registrasi='$id'"));
		$count+=ResepPasien::model()->count(array('condition'=>"id_registrasi='$id'"));
		$count+=ResponsTime::model()->count(array('condition'=>"id_registrasi='$id'"));
		$count+=RuanganPasien::model()->count(array('condition'=>"id_registrasi='$id'"));
		$count+=SoapPasien::model()->count(array('condition'=>"id_registrasi='$id'"));
		$count+=TagihanPasien::model()->count(array('condition'=>"id_registrasi='$id'"));
		$count+=TindakanPasien::model()->count(array('condition'=>"id_registrasi='$id'"));
		$count+=VisiteDokterPasien::model()->count(array('condition'=>"id_registrasi='$id'"));
		if($count>0){
			return false;
		}else{
			return true;
		}
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Registrasi the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
