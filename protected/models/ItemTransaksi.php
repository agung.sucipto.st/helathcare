<?php

/**
 * This is the model class for table "item_transaksi".
 *
 * The followings are the available columns in table 'item_transaksi':
 * @property string $id_item_transaksi
 * @property string $id_supplier
 * @property string $id_jenis_item_transaksi
 * @property string $gudang_asal
 * @property string $gudang_tujuan
 * @property string $no_transaksi
 * @property string $waktu_transaksi
 * @property string $catatan_transaksi
 * @property string $nominal_transaksi
 * @property string $ppn
 * @property string $discount
 * @property string $user_create
 * @property string $user_update
 * @property string $time_create
 * @property string $time_update
 *
 * The followings are the available model relations:
 * @property DaftarItemTransaksi[] $daftarItemTransaksis
 * @property Gudang $gudangAsal
 * @property Gudang $gudangTujuan
 * @property JenisItemTransaksi $idJenisItemTransaksi
 * @property Supplier $idSupplier
 * @property User $userCreate
 * @property User $userUpdate
 */
class ItemTransaksi extends CActiveRecord
{
	public $inc;
	public $item;
	public $nama_item;
	public $nama_pasien;
	public $no_registrasi;
	public $id_registrasi;
	public $id_pasien;
	public $jenis_registrasi;
	public $hapus;
	public $time_delete;
	public $user_delete;
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'item_transaksi';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('id_jenis_item_transaksi,no_transaksi,waktu_transaksi, user_create, time_create', 'required'),
			array('id_jenis_item_transaksi,id_supplier,no_transaksi,waktu_transaksi, gudang_tujuan, user_create, time_create', 'required',"on"=>"recieve"),
			array('id_jenis_item_transaksi,gudang_asal,no_transaksi,waktu_transaksi, gudang_tujuan, user_create, time_create', 'required',"on"=>"transfer"),
			array('id_jenis_item_transaksi,no_transaksi,waktu_transaksi, gudang_asal, user_create, time_create', 'required',"on"=>"RU"),
			array('id_jenis_item_transaksi,no_transaksi,waktu_transaksi, gudang_asal, user_create, time_create, nama_pasien', 'required',"on"=>"OTC"),
			array('gudang_tujuan,waktu_transaksi,id_item', 'required',"on"=>"kartuStok"),
			array('id_supplier, id_jenis_item_transaksi, gudang_asal, gudang_tujuan, user_create, user_update', 'length', 'max'=>20),
			array('gudang_asal, gudang_tujuan','uniqueDepo', 'on'=>'transfer'),
			array('no_transaksi', 'length', 'max'=>45),
			array('nominal_transaksi, ppn, discount', 'length', 'max'=>30),
			array('waktu_transaksi, catatan_transaksi, time_update', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id_item_transaksi, id_supplier, id_jenis_item_transaksi, gudang_asal, gudang_tujuan, no_transaksi, waktu_transaksi, catatan_transaksi, nominal_transaksi, ppn, discount, user_create, user_update, time_create, time_update, nama_item, hapus', 'safe', 'on'=>'search'),
			array('id_item_transaksi, id_supplier, id_jenis_item_transaksi, gudang_asal, gudang_tujuan, no_transaksi, waktu_transaksi, catatan_transaksi, nominal_transaksi, ppn, discount, user_create, user_update, time_create, time_update, nama_item, id_registrasi, hapus, status_penerimaan_transfer, no_registrasi,id_pasien,nama_pasien,jenis_registrasi', 'safe', 'on'=>'searchPasien'),
		);
	}
	
	public function uniqueDepo($attribute, $params)
    {
		if($this->gudang_asal == $this->gudang_tujuan)
		{
			$this->addError($attribute, 'Gudang Tidak Boleh Sama !');
		}
    }

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'daftarItemTransaksis' => array(self::HAS_MANY, 'DaftarItemTransaksi', 'id_item_transaksi'),
			'gudangAsal' => array(self::BELONGS_TO, 'Gudang', 'gudang_asal'),
			'gudangTujuan' => array(self::BELONGS_TO, 'Gudang', 'gudang_tujuan'),
			'idJenisItemTransaksi' => array(self::BELONGS_TO, 'JenisItemTransaksi', 'id_jenis_item_transaksi'),
			'idRegistrasi' => array(self::BELONGS_TO, 'Registrasi', 'id_registrasi'),
			'idSupplier' => array(self::BELONGS_TO, 'Supplier', 'id_supplier'),
			'userCreate' => array(self::BELONGS_TO, 'User', 'user_create'),
			'userUpdate' => array(self::BELONGS_TO, 'User', 'user_update'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'nama_pasien' => 'Nama Pasien',
			'id_item_transaksi' => 'Id Item Transaksi',
			'id_supplier' => 'Id Supplier',
			'id_jenis_item_transaksi' => 'Id Jenis Item Transaksi',
			'gudang_asal' => 'Gudang Asal',
			'gudang_tujuan' => 'Gudang Tujuan',
			'no_transaksi' => 'No Transaksi',
			'waktu_transaksi' => 'Waktu Transaksi',
			'catatan_transaksi' => 'Catatan Transaksi',
			'nominal_transaksi' => 'Nominal Transaksi',
			'ppn' => 'Ppn',
			'discount' => 'Discount',
			'id_pasien' => 'No RM',
			'user_create' => 'User Create',
			'user_update' => 'User Update',
			'time_create' => 'Time Create',
			'time_update' => 'Time Update',
		);
	}

	public static function getTransactionNumber($trxType,$codeType,$date){
		$criteria=new CDbCriteria;
		$criteria->select="RIGHT(no_transaksi,4) as inc";
		$criteria->order="RIGHT(no_transaksi,4) DESC";
		$criteria->limit=1;
		$criteria->condition="DATE(waktu_transaksi)='$date' and id_jenis_item_transaksi='$trxType'";
		
		$last=ItemTransaksi::model()->find($criteria);
		
		if(empty($last)){
			return $codeType."-".date("ymd").sprintf("%04s",1);
		}else{
			return $codeType."-".date("ymd").sprintf("%04s",$last->inc+1);
		}
	}
	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	 
	public function convertRM(){
		$string=str_replace("-","",$this->id_pasien);
		return $string ? ($string+0): null;
	}
	 
	public function searchPasien()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.
		$criteria=new CDbCriteria;
		$criteria->alias="ori";
		$criteria->with=array('idRegistrasi','idRegistrasi.idPasien','idRegistrasi.idPasien.idPersonal');
		if($this->waktu_transaksi!=''){
			$list=explode(" - ",$this->waktu_transaksi);
			$start=explode("/",$list[0]);
			$end=explode("/",$list[1]);
			$dateStart=$start[2]."-".$start[0]."-".$start[1];
			$dateEnd=$end[2]."-".$end[0]."-".$end[1];
			$criteria->addCondition("DATE(ori.waktu_transaksi) between '$dateStart' AND '$dateEnd'");
		}
		
		$criteria->compare('ori.id_registrasi',$this->id_registrasi);
		$criteria->compare('idRegistrasi.id_pasien',$this->convertRM(),true);
		$criteria->compare('idRegistrasi.jenis_registrasi',$this->jenis_registrasi,true);
		$criteria->compare('idPasien.id_pasien',$this->convertRM(),true);
		$criteria->compare('idPersonal.nama_lengkap',$this->nama_pasien,true);
		$criteria->compare('ori.id_jenis_item_transaksi',$this->id_jenis_item_transaksi);
		$criteria->compare('ori.gudang_asal',$this->gudang_asal,true);
		$criteria->compare('ori.gudang_tujuan',$this->gudang_tujuan,true);
		$criteria->compare('ori.no_transaksi',$this->no_transaksi,true);
		$criteria->compare('ori.hapus',$this->hapus,true);

		return new CActiveDataProvider($this, array(
			'pagination'=>array(
				'pageSize'=> Yii::app()->user->getState('pageSize',Yii::app()->params['defaultPageSize']),
			),
			'criteria'=>$criteria,
			'sort'=>array(
			   'defaultOrder'=>'ori.waktu_transaksi DESC',
			),
		));
	}
	
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.
		$criteria=new CDbCriteria;
		$criteria->alias="ori";
		if($this->waktu_transaksi!=''){
			$list=explode(" - ",$this->waktu_transaksi);
			$start=explode("/",$list[0]);
			$end=explode("/",$list[1]);
			$dateStart=$start[2]."-".$start[0]."-".$start[1];
			$dateEnd=$end[2]."-".$end[0]."-".$end[1];
			$criteria->addCondition("DATE(ori.waktu_transaksi) between '$dateStart' AND '$dateEnd'");
		}
		$criteria->compare('ori.id_item_transaksi',$this->id_item_transaksi);
		$criteria->compare('ori.id_supplier',$this->id_supplier);
		$criteria->compare('ori.id_jenis_item_transaksi',$this->id_jenis_item_transaksi);
		$criteria->compare('ori.gudang_asal',$this->gudang_asal);
		$criteria->compare('ori.gudang_tujuan',$this->gudang_tujuan);
		$criteria->compare('ori.hapus',$this->hapus);
		$criteria->compare('ori.status_penerimaan_transfer',$this->status_penerimaan_transfer);
		$criteria->compare('ori.no_transaksi',$this->no_transaksi,true);
		$criteria->compare('ori.catatan_transaksi',$this->catatan_transaksi,true);
		$criteria->compare('ori.nominal_transaksi',$this->nominal_transaksi,true);
		$criteria->compare('ori.ppn',$this->ppn,true);
		$criteria->compare('ori.discount',$this->discount,true);
		$criteria->compare('ori.user_create',$this->user_create);
		$criteria->compare('ori.user_update',$this->user_update);
		$criteria->compare('ori.time_create',$this->time_create);
		$criteria->compare('ori.time_update',$this->time_update);

		return new CActiveDataProvider($this, array(
			'pagination'=>array(
				'pageSize'=> Yii::app()->user->getState('pageSize',Yii::app()->params['defaultPageSize']),
			),
			'sort'=>array(
			   'defaultOrder'=>'ori.waktu_transaksi DESC',
			),
			'criteria'=>$criteria,
		));
	}
	
	public function getAllowView()
	{
		$akses = UserAccess::model()->find(array("with"=>array("access"),'condition'=>"id_user='".Yii::app()->user->id."' and access.access_controller='ItemTransaksi' and access.access_action='view'"));
		if(!empty($akses)) return true; else return false;
	}
	
	public function getAllowUpdate()
	{
		$akses = UserAccess::model()->find(array("with"=>array("access"),'condition'=>"id_user='".Yii::app()->user->id."' and access.access_controller='ItemTransaksi' and access.access_action='update'"));
		if(!empty($akses)) return true; else return false;
	}
	
	public function getAllowDelete($id)
	{
		$akses = UserAccess::model()->find(array("with"=>array("access"),'condition'=>"id_user='".Yii::app()->user->id."' and access.access_controller='ItemTransaksi' and access.access_action='delete'"));
		$rel=$this->getEmptyRelation($id);
		if(!empty($akses) AND $rel==true){
			return true;
		}else{
			return false;
		}
	}
	
	
	public function getEmptyRelation($id)
	{
		$count=0;
		$count+=DaftarItemTransaksi::model()->count(array('condition'=>"id_item_transaksi='$id'"));
		if($count>0){
			return false;
		}else{
			return true;
		}
	}
	
	public function xResep()
	{
		$nData = count($this->daftarItemTransaksis);
		$nRacikan = 0;
		$nHaveStocknHaveStock = 0;
		foreach($this->daftarItemTransaksis as $row)
		{
			if($row->id_daftar_tagihan == ''){
				$nRacikan++;
			} else {
				if($row->idDaftarTagihan->id_tagihan =='' AND $row->is_retur == ''){
					$nHaveStock++;
				}
			}
		}
		if($nHaveStock == ($nData-$nRacikan)){
			return true;
		} else {
			return false;
		}
	}
	
	public function xRecieve()
	{
		$gudang = $this->gudang_tujuan;
		$nData = count($this->daftarItemTransaksis);
		$nHaveStock = 0;
		foreach($this->daftarItemTransaksis as $row)
		{
			$cek = ItemGudang::getStock($row->id_item,$gudang);
			if($cek >= $row->jumlah_transaksi){
				$nHaveStock++;
			}
		}
		if($nHaveStock == $nData AND ($this->status_tukar_faktur == 'Belum' OR $this->status_tukar_faktur == '')){
			return true;
		} else {
			return false;
		}
	}
	
	public function xTransfer()
	{
		if($this->hapus == '0' AND $this->status_penerimaan_transfer == 'Belum Diterima'){
			return true;
		} else {
			return false;
		}
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return ItemTransaksi the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
