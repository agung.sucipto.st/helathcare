<?php

/**
 * This is the model class for table "item_pasien_group".
 *
 * The followings are the available columns in table 'item_pasien_group':
 * @property string $id_item_pasien_group
 * @property string $id_registrasi
 * @property string $id_resep_pasien
 * @property string $id_gudang
 * @property string $is_billed
 * @property string $source
 * @property string $catatan
 * @property string $user_create
 * @property string $user_update
 * @property string $user_final
 * @property string $time_create
 * @property string $time_update
 * @property string $time_final
 *
 * The followings are the available model relations:
 * @property ItemPasien[] $itemPasiens
 * @property Gudang $idGudang
 * @property Registrasi $idRegistrasi
 * @property ResepPasien $idResepPasien
 * @property User $userCreate
 * @property User $userUpdate
 * @property User $userFinal
 */
class ItemPasienGroup extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'item_pasien_group';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('id_registrasi, id_resep_pasien, id_gudang, source, user_create, time_create', 'required'),
			array('id_item_pasien_group, id_registrasi, id_resep_pasien, id_gudang, user_create, user_update, user_final', 'length', 'max'=>20),
			array('is_billed', 'length', 'max'=>1),
			array('source', 'length', 'max'=>45),
			array('catatan, time_update, time_final', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id_item_pasien_group, id_registrasi, id_resep_pasien, id_gudang, is_billed, source, catatan, user_create, user_update, user_final, time_create, time_update, time_final', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'itemPasiens' => array(self::HAS_MANY, 'ItemPasien', 'id_item_pasien_group'),
			'idGudang' => array(self::BELONGS_TO, 'Gudang', 'id_gudang'),
			'idRegistrasi' => array(self::BELONGS_TO, 'Registrasi', 'id_registrasi'),
			'idResepPasien' => array(self::BELONGS_TO, 'ResepPasien', 'id_resep_pasien'),
			'userCreate' => array(self::BELONGS_TO, 'User', 'user_create'),
			'userUpdate' => array(self::BELONGS_TO, 'User', 'user_update'),
			'userFinal' => array(self::BELONGS_TO, 'User', 'user_final'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id_item_pasien_group' => 'Id Item Pasien Group',
			'id_registrasi' => 'Id Registrasi',
			'id_resep_pasien' => 'Id Resep Pasien',
			'id_gudang' => 'Id Gudang',
			'is_billed' => 'Is Billed',
			'source' => 'Source',
			'catatan' => 'Catatan',
			'user_create' => 'User Create',
			'user_update' => 'User Update',
			'user_final' => 'User Final',
			'time_create' => 'Time Create',
			'time_update' => 'Time Update',
			'time_final' => 'Time Final',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id_item_pasien_group',$this->id_item_pasien_group,true);
		$criteria->compare('id_registrasi',$this->id_registrasi,true);
		$criteria->compare('id_resep_pasien',$this->id_resep_pasien,true);
		$criteria->compare('id_gudang',$this->id_gudang,true);
		$criteria->compare('is_billed',$this->is_billed,true);
		$criteria->compare('source',$this->source,true);
		$criteria->compare('catatan',$this->catatan,true);
		$criteria->compare('user_create',$this->user_create,true);
		$criteria->compare('user_update',$this->user_update,true);
		$criteria->compare('user_final',$this->user_final,true);
		$criteria->compare('time_create',$this->time_create,true);
		$criteria->compare('time_update',$this->time_update,true);
		$criteria->compare('time_final',$this->time_final,true);

		return new CActiveDataProvider($this, array(
			'pagination'=>array(
				'pageSize'=> Yii::app()->user->getState('pageSize',Yii::app()->params['defaultPageSize']),
			),
			'criteria'=>$criteria,
		));
	}
	
	public function getAllowView()
	{
		$akses = UserAccess::model()->find(array("with"=>array("access"),'condition'=>"id_user='".Yii::app()->user->id."' and access.access_controller='ItemPasienGroup' and access.access_action='view'"));
		if(!empty($akses)) return true; else return false;
	}
	
	public function getAllowUpdate()
	{
		$akses = UserAccess::model()->find(array("with"=>array("access"),'condition'=>"id_user='".Yii::app()->user->id."' and access.access_controller='ItemPasienGroup' and access.access_action='update'"));
		if(!empty($akses)) return true; else return false;
	}
	
	public function getAllowDelete($id)
	{
		$akses = UserAccess::model()->find(array("with"=>array("access"),'condition'=>"id_user='".Yii::app()->user->id."' and access.access_controller='ItemPasienGroup' and access.access_action='delete'"));
		$rel=$this->getEmptyRelation($id);
		if(!empty($akses) AND $rel==true){
			return true;
		}else{
			return false;
		}
	}
	
	
	public function getEmptyRelation($id)
	{
		$count=0;
		$count+=ItemPasien::model()->count(array('condition'=>"id_item_pasien_group='$id'"));
														if($count>0){
			return false;
		}else{
			return true;
		}
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return ItemPasienGroup the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
