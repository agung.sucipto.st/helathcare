<?php

/**
 * This is the model class for table "departemen".
 *
 * The followings are the available columns in table 'departemen':
 * @property string $id_departemen
 * @property string $id_jenis_departemen
 * @property string $nama_departemen
 *
 * The followings are the available model relations:
 * @property JenisDepartemen $idJenisDepartemen
 * @property DepartemenPegawai[] $departemenPegawais
 * @property JadwalPraktek[] $jadwalPrakteks
 * @property Registrasi[] $registrasis
 * @property Ruangan[] $ruangans
 */
class Departemen extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'departemen';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('id_jenis_departemen, nama_departemen', 'required'),
			array('id_jenis_departemen', 'length', 'max'=>20),
			array('nama_departemen', 'length', 'max'=>45),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id_departemen, id_jenis_departemen, nama_departemen', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'idJenisDepartemen' => array(self::BELONGS_TO, 'JenisDepartemen', 'id_jenis_departemen'),
			'departemenPegawais' => array(self::HAS_MANY, 'DepartemenPegawai', 'id_departemen'),
			'jadwalPrakteks' => array(self::HAS_MANY, 'JadwalPraktek', 'id_departemen'),
			'registrasis' => array(self::HAS_MANY, 'Registrasi', 'id_departemen'),
			'ruangans' => array(self::HAS_MANY, 'Ruangan', 'id_departemen'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id_departemen' => 'Id Departemen',
			'id_jenis_departemen' => 'Id Jenis Departemen',
			'nama_departemen' => 'Nama Departemen',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id_departemen',$this->id_departemen,true);
		$criteria->compare('id_jenis_departemen',$this->id_jenis_departemen,true);
		$criteria->compare('nama_departemen',$this->nama_departemen,true);

		return new CActiveDataProvider($this, array(
			'pagination'=>array(
				'pageSize'=> Yii::app()->user->getState('pageSize',Yii::app()->params['defaultPageSize']),
			),
			'criteria'=>$criteria,
		));
	}
	
	public function getAllowView()
	{
		$akses = UserAccess::model()->find(array("with"=>array("access"),'condition'=>"id_user='".Yii::app()->user->id."' and access.access_controller='Departemen' and access.access_action='view'"));
		if(!empty($akses)) return true; else return false;
	}
	
	public function getAllowUpdate()
	{
		$akses = UserAccess::model()->find(array("with"=>array("access"),'condition'=>"id_user='".Yii::app()->user->id."' and access.access_controller='Departemen' and access.access_action='update'"));
		if(!empty($akses)) return true; else return false;
	}
	
	public function getAllowDelete($id)
	{
		$akses = UserAccess::model()->find(array("with"=>array("access"),'condition'=>"id_user='".Yii::app()->user->id."' and access.access_controller='Departemen' and access.access_action='delete'"));
		$rel=$this->getEmptyRelation($id);
		if(!empty($akses) AND $rel==true){
			return true;
		}else{
			return false;
		}
	}
	
	
	public function getEmptyRelation($id)
	{
		$count=0;
				$count+=DepartemenPegawai::model()->count(array('condition'=>"id_departemen='$id'"));
		$count+=JadwalPraktek::model()->count(array('condition'=>"id_departemen='$id'"));
		$count+=Registrasi::model()->count(array('condition'=>"id_departemen='$id'"));
		$count+=Ruangan::model()->count(array('condition'=>"id_departemen='$id'"));
		if($count>0){
			return false;
		}else{
			return true;
		}
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Departemen the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
