<?php

/**
 * This is the model class for table "balance_cairan".
 *
 * The followings are the available columns in table 'balance_cairan':
 * @property string $id_balance_cairan
 * @property string $id_registrasi
 * @property string $tanggal
 * @property string $waktu
 * @property integer $makan
 * @property integer $minum
 * @property integer $iuvd
 * @property integer $obat
 * @property integer $spooling_intake
 * @property integer $jumlah_intake
 * @property integer $balance
 * @property integer $iwl
 * @property integer $bab
 * @property integer $bak
 * @property integer $muntah
 * @property integer $spooling_output
 * @property integer $jumlah_output
 * @property string $user_create
 * @property string $user_update
 * @property string $tgl_create
 * @property string $tgl_update
 */
class BalanceCairan extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'balance_cairan';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('id_balance_cairan', 'required'),
			array('makan, minum, iuvd, obat, spooling_intake, jumlah_intake, balance, iwl, bab, bak, muntah, spooling_output, jumlah_output', 'numerical', 'integerOnly'=>true),
			array('id_balance_cairan, id_registrasi, user_create, user_update', 'length', 'max'=>20),
			array('waktu', 'length', 'max'=>5),
			array('tanggal, tgl_create, tgl_update', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id_balance_cairan, id_registrasi, tanggal, waktu, makan, minum, iuvd, obat, spooling_intake, jumlah_intake, balance, iwl, bab, bak, muntah, spooling_output, jumlah_output, user_create, user_update, tgl_create, tgl_update', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id_balance_cairan' => 'Id Balance Cairan',
			'id_registrasi' => 'Id Registrasi',
			'tanggal' => 'Tanggal',
			'waktu' => 'Waktu',
			'makan' => 'Makan',
			'minum' => 'Minum',
			'iuvd' => 'Iuvd',
			'obat' => 'Obat',
			'spooling_intake' => 'Spooling Intake',
			'jumlah_intake' => 'Jumlah Intake',
			'balance' => 'Balance',
			'iwl' => 'Iwl',
			'bab' => 'Bab',
			'bak' => 'Bak',
			'muntah' => 'Muntah',
			'spooling_output' => 'Spooling Output',
			'jumlah_output' => 'Jumlah Output',
			'user_create' => 'User Create',
			'user_update' => 'User Update',
			'tgl_create' => 'Tgl Create',
			'tgl_update' => 'Tgl Update',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id_balance_cairan',$this->id_balance_cairan,true);
		$criteria->compare('id_registrasi',$this->id_registrasi,true);
		$criteria->compare('tanggal',$this->tanggal,true);
		$criteria->compare('waktu',$this->waktu,true);
		$criteria->compare('makan',$this->makan);
		$criteria->compare('minum',$this->minum);
		$criteria->compare('iuvd',$this->iuvd);
		$criteria->compare('obat',$this->obat);
		$criteria->compare('spooling_intake',$this->spooling_intake);
		$criteria->compare('jumlah_intake',$this->jumlah_intake);
		$criteria->compare('balance',$this->balance);
		$criteria->compare('iwl',$this->iwl);
		$criteria->compare('bab',$this->bab);
		$criteria->compare('bak',$this->bak);
		$criteria->compare('muntah',$this->muntah);
		$criteria->compare('spooling_output',$this->spooling_output);
		$criteria->compare('jumlah_output',$this->jumlah_output);
		$criteria->compare('user_create',$this->user_create,true);
		$criteria->compare('user_update',$this->user_update,true);
		$criteria->compare('tgl_create',$this->tgl_create,true);
		$criteria->compare('tgl_update',$this->tgl_update,true);

		return new CActiveDataProvider($this, array(
			'pagination'=>array(
				'pageSize'=> Yii::app()->user->getState('pageSize',Yii::app()->params['defaultPageSize']),
			),
			'criteria'=>$criteria,
		));
	}
	
	public function getAllowView()
	{
		$akses = UserAccess::model()->find(array("with"=>array("access"),'condition'=>"id_user='".Yii::app()->user->id."' and access.access_controller='BalanceCairan' and access.access_action='view'"));
		if(!empty($akses)) return true; else return false;
	}
	
	public function getAllowUpdate()
	{
		$akses = UserAccess::model()->find(array("with"=>array("access"),'condition'=>"id_user='".Yii::app()->user->id."' and access.access_controller='BalanceCairan' and access.access_action='update'"));
		if(!empty($akses)) return true; else return false;
	}
	
	public function getAllowDelete($id)
	{
		$akses = UserAccess::model()->find(array("with"=>array("access"),'condition'=>"id_user='".Yii::app()->user->id."' and access.access_controller='BalanceCairan' and access.access_action='delete'"));
		$rel=$this->getEmptyRelation($id);
		if(!empty($akses) AND $rel==true){
			return true;
		}else{
			return false;
		}
	}
	
	
	public function getEmptyRelation($id)
	{
		$count=0;
		if($count>0){
			return false;
		}else{
			return true;
		}
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return BalanceCairan the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
