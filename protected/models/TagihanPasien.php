<?php

/**
 * This is the model class for table "tagihan_pasien".
 *
 * The followings are the available columns in table 'tagihan_pasien':
 * @property string $id_tagihan_pasien
 * @property string $jenis_tagihan
 * @property string $no_tagihan
 * @property string $id_registrasi
 * @property string $waktu_tagihan
 * @property string $total_tagihan
 * @property string $total_discount
 * @property string $status_tagihan
 * @property string $user_create
 * @property string $user_update
 * @property string $user_final
 * @property string $time_create
 * @property string $time_update
 * @property string $time_final
 *
 * The followings are the available model relations:
 * @property DaftarTagihan[] $daftarTagihans
 * @property PembayaranTagihan[] $pembayaranTagihans
 * @property Registrasi $idRegistrasi
 * @property User $userCreate
 * @property User $userUpdate
 * @property User $userFinal
 */
class TagihanPasien extends CActiveRecord
{
	public $inc;
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'tagihan_pasien';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('jenis_tagihan, no_tagihan, id_registrasi, waktu_tagihan, total_tagihan, user_create, time_create', 'required'),
			array('jenis_tagihan, no_tagihan, total_tagihan, total_discount, status_tagihan', 'length', 'max'=>45),
			array('id_registrasi, user_create, user_update, user_final', 'length', 'max'=>20),
			array('time_update, time_final', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id_tagihan_pasien, jenis_tagihan, no_tagihan, id_registrasi, waktu_tagihan, total_tagihan, total_discount, status_tagihan, user_create, user_update, user_final, time_create, time_update, time_final', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'daftarTagihans' => array(self::HAS_MANY, 'DaftarTagihan', 'id_tagihan'),
			'pembayaranTagihans' => array(self::HAS_MANY, 'PembayaranTagihan', 'id_tagihan_pasien'),
			'idRegistrasi' => array(self::BELONGS_TO, 'Registrasi', 'id_registrasi'),
			'userCreate' => array(self::BELONGS_TO, 'User', 'user_create'),
			'userUpdate' => array(self::BELONGS_TO, 'User', 'user_update'),
			'userFinal' => array(self::BELONGS_TO, 'User', 'user_final'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id_tagihan_pasien' => 'Id Tagihan Pasien',
			'jenis_tagihan' => 'Jenis Tagihan',
			'no_tagihan' => 'No Tagihan',
			'id_registrasi' => 'Id Registrasi',
			'waktu_tagihan' => 'Waktu Tagihan',
			'total_tagihan' => 'Total Tagihan',
			'total_discount' => 'Total Discount',
			'status_tagihan' => 'Status Tagihan',
			'user_create' => 'User Create',
			'user_update' => 'User Update',
			'user_final' => 'User Final',
			'time_create' => 'Time Create',
			'time_update' => 'Time Update',
			'time_final' => 'Time Final',
		);
	}

	public static function getBillNo($type,$date){
		$criteria=new CDbCriteria;
		$criteria->select="RIGHT(no_tagihan,4) as inc";
		$criteria->order="RIGHT(no_tagihan,4) DESC";
		$criteria->limit=1;
		$criteria->condition="DATE(waktu_tagihan)='$date' AND jenis_tagihan='$type'";
		
		$last=TagihanPasien::model()->find($criteria);
		
		if(empty($last)){
			return $type.'-'.date("ymd").sprintf("%04s",1);
		}else{
			return $type.'-'.date("ymd").sprintf("%04s",$last->inc+1);
		}
	}
	
	
	public static function getAmountGroup($data,$type){
		$criteria=new CDbCriteria;
		$criteria->select="SUM($type) as jumlah_bayar";
		$criteria->addCondition("id_daftar_tagihan IN (".implode(",",$data).")");
		
		$model=DaftarTagihan::model()->find($criteria);
		return $model->jumlah_bayar;
	}
	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id_tagihan_pasien',$this->id_tagihan_pasien,true);
		$criteria->compare('jenis_tagihan',$this->jenis_tagihan,true);
		$criteria->compare('no_tagihan',$this->no_tagihan,true);
		$criteria->compare('id_registrasi',$this->id_registrasi,true);
		$criteria->compare('waktu_tagihan',$this->waktu_tagihan,true);
		$criteria->compare('total_tagihan',$this->total_tagihan,true);
		$criteria->compare('total_discount',$this->total_discount,true);
		$criteria->compare('status_tagihan',$this->status_tagihan,true);
		$criteria->compare('user_create',$this->user_create,true);
		$criteria->compare('user_update',$this->user_update,true);
		$criteria->compare('user_final',$this->user_final,true);
		$criteria->compare('time_create',$this->time_create,true);
		$criteria->compare('time_update',$this->time_update,true);
		$criteria->compare('time_final',$this->time_final,true);

		return new CActiveDataProvider($this, array(
			'pagination'=>array(
				'pageSize'=> Yii::app()->user->getState('pageSize',Yii::app()->params['defaultPageSize']),
			),
			'criteria'=>$criteria,
		));
	}
	
	public function getAllowView()
	{
		$akses = UserAccess::model()->find(array("with"=>array("access"),'condition'=>"id_user='".Yii::app()->user->id."' and access.access_controller='TagihanPasien' and access.access_action='view'"));
		if(!empty($akses)) return true; else return false;
	}
	
	public function getAllowUpdate()
	{
		$akses = UserAccess::model()->find(array("with"=>array("access"),'condition'=>"id_user='".Yii::app()->user->id."' and access.access_controller='TagihanPasien' and access.access_action='update'"));
		if(!empty($akses)) return true; else return false;
	}
	
	public function getAllowDelete($id)
	{
		$akses = UserAccess::model()->find(array("with"=>array("access"),'condition'=>"id_user='".Yii::app()->user->id."' and access.access_controller='TagihanPasien' and access.access_action='delete'"));
		$rel=$this->getEmptyRelation($id);
		if(!empty($akses) AND $rel==true){
			return true;
		}else{
			return false;
		}
	}
	
	
	public function getEmptyRelation($id)
	{
		$count=0;
		$count+=DaftarTagihan::model()->count(array('condition'=>"id_tagihan='$id'"));
		$count+=PembayaranTagihan::model()->count(array('condition'=>"id_tagihan_pasien='$id'"));
										if($count>0){
			return false;
		}else{
			return true;
		}
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return TagihanPasien the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
