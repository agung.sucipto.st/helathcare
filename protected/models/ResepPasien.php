<?php

/**
 * This is the model class for table "resep_pasien".
 *
 * The followings are the available columns in table 'resep_pasien':
 * @property string $id_resep_pasien
 * @property string $id_registrasi
 * @property string $id_dokter
 * @property string $jenis_resep
 * @property string $no_resep
 * @property string $status_resep
 * @property string $waktu_resep
 * @property string $racikan
 * @property string $catatan
 * @property string $user_create
 * @property string $user_update
 * @property string $user_final
 * @property string $time_create
 * @property string $time_update
 * @property string $time_final
 *
 * The followings are the available model relations:
 * @property ItemPasien[] $itemPasiens
 * @property ItemPasienGroup[] $itemPasienGroups
 * @property Pegawai $idDokter
 * @property Registrasi $idRegistrasi
 * @property User $userCreate
 * @property User $userUpdate
 * @property User $userFinal
 * @property ResepPasienList[] $resepPasienLists
 */
class ResepPasien extends CActiveRecord
{
	public $resep,$inc,$no_registrasi,$id_pasien,$nama_lengkap,$jenis_registrasi;
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'resep_pasien';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('id_registrasi, id_dokter, user_create, time_create', 'required'),
			array('id_registrasi, id_dokter, user_create, user_update, user_final', 'length', 'max'=>20),
			array('jenis_resep', 'length', 'max'=>2),
			array('no_resep, status_resep', 'length', 'max'=>45),
			array('waktu_resep, racikan, catatan, time_update, time_final', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id_resep_pasien, id_registrasi, id_dokter, jenis_resep, no_resep, status_resep, waktu_resep, racikan, catatan, user_create, user_update, user_final, time_create, time_update, time_final,no_registrasi,id_pasien,nama_lengkap,jenis_registrasi', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'itemPasiens' => array(self::HAS_MANY, 'ItemPasien', 'id_resep_pasien'),
			'itemPasienGroups' => array(self::HAS_MANY, 'ItemPasienGroup', 'id_resep_pasien'),
			'idDokter' => array(self::BELONGS_TO, 'Pegawai', 'id_dokter'),
			'idRegistrasi' => array(self::BELONGS_TO, 'Registrasi', 'id_registrasi'),
			'userCreate' => array(self::BELONGS_TO, 'User', 'user_create'),
			'userUpdate' => array(self::BELONGS_TO, 'User', 'user_update'),
			'userFinal' => array(self::BELONGS_TO, 'User', 'user_final'),
			'resepPasienLists' => array(self::HAS_MANY, 'ResepPasienList', 'id_resep_pasien'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id_resep_pasien' => 'Id Resep Pasien',
			'id_registrasi' => 'Id Registrasi',
			'id_pasien' => 'No RM',
			'nama_lengkap' => 'Nama Pasien',
			'id_dokter' => 'Dokter',
			'jenis_resep' => 'Jenis Resep',
			'no_resep' => 'No Resep',
			'status_resep' => 'Status Resep',
			'waktu_resep' => 'Waktu Resep',
			'racikan' => 'Racikan',
			'catatan' => 'Catatan',
			'user_create' => 'User Create',
			'user_update' => 'User Update',
			'user_final' => 'User Final',
			'time_create' => 'Time Create',
			'time_update' => 'Time Update',
			'time_final' => 'Time Final',
		);
	}

	public static function getNoResep($date){
		$criteria=new CDbCriteria;
		$criteria->select="RIGHT(no_resep,4) as inc";
		$criteria->order="RIGHT(no_resep,4) DESC";
		$criteria->limit=1;
		$criteria->condition="DATE(waktu_resep)='$date'";
		
		$last=ResepPasien::model()->find($criteria);
		
		if(empty($last)){
			return "DD".date("Ymd").sprintf("%04s",1);
		}else{
			return  "DD".date("Ymd").sprintf("%04s",$last->inc+1);
		}
	}
	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;
		$criteria->alias="res";
		$criteria->with=array("idRegistrasi","idRegistrasi.idPasien","idRegistrasi.idPasien.idPersonal");
		$criteria->compare('idRegistrasi.jenis_registrasi',$this->jenis_registrasi,true);
		$criteria->compare('idRegistrasi.no_registrasi',$this->no_registrasi,true);
		$criteria->compare('idRegistrasi.id_pasien',$this->id_pasien,true);
		$criteria->compare('idPersonal.nama_lengkap',$this->nama_lengkap,true);
		$criteria->compare('DATE(res.waktu_resep)',$this->waktu_resep,true);
		$criteria->compare('res.status_resep',$this->status_resep,true);
		$criteria->compare('res.id_registrasi',$this->id_registrasi,true);

		return new CActiveDataProvider($this, array(
			'pagination'=>array(
				'pageSize'=> Yii::app()->user->getState('pageSize',Yii::app()->params['defaultPageSize']),
			),
			'sort'=>array(
			   'defaultOrder'=>'res.waktu_resep DESC, res.status_resep ASC',
			   'attributes'=>array(
						'id_pasien'=>array(
							'asc'=>'idRegistrasi.id_pasien ASC',
							'desc'=>'idRegistrasi.id_pasien DESC',
						),
						'nama_lengkap'=>array(
							'asc'=>'idPersonal.nama_lengkap ASC',
							'desc'=>'idPersonal.nama_lengkap DESC',
						),
						'no_registrasi'=>array(
							'asc'=>'idRegistrasi.no_registrasi ASC',
							'desc'=>'idRegistrasi.no_registrasi DESC',
						),
						'*',
					),
			),
			'criteria'=>$criteria,
		));
	}
	
	public function getAllowView()
	{
		$akses = UserAccess::model()->find(array("with"=>array("access"),'condition'=>"id_user='".Yii::app()->user->id."' and access.access_controller='ResepPasien' and access.access_action='view'"));
		if(!empty($akses)) return true; else return false;
	}
	
	public function getAllowUpdate()
	{
		$akses = UserAccess::model()->find(array("with"=>array("access"),'condition'=>"id_user='".Yii::app()->user->id."' and access.access_controller='ResepPasien' and access.access_action='update'"));
		if(!empty($akses)) return true; else return false;
	}
	
	public function getAllowDelete($id)
	{
		$akses = UserAccess::model()->find(array("with"=>array("access"),'condition'=>"id_user='".Yii::app()->user->id."' and access.access_controller='ResepPasien' and access.access_action='delete'"));
		$rel=$this->getEmptyRelation($id);
		if(!empty($akses) AND $rel==true){
			return true;
		}else{
			return false;
		}
	}
	
	
	public function getEmptyRelation($id)
	{
		$count=0;
		$count+=ResepPasienList::model()->count(array('condition'=>"id_resep_pasien='$id'"));
		if($count>0){
			return false;
		}else{
			return true;
		}
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return ResepPasien the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
