<?php

/**
 * This is the model class for table "penjamin".
 *
 * The followings are the available columns in table 'penjamin':
 * @property string $id_penjamin
 * @property string $id_jenis_penjamin
 * @property string $nama_penjamin
 * @property string $mulai_kerja_sama
 * @property string $berakhir_kerja_sama
 * @property string $alamat
 * @property string $no_telp
 * @property string $status_kerja_sama
 *
 * The followings are the available model relations:
 * @property DaftarPiutangPerusahaan[] $daftarPiutangPerusahaans
 * @property DaftarTagihan[] $daftarTagihans
 * @property HistoryEditJaminan[] $historyEditJaminans
 * @property HistoryEditJaminan[] $historyEditJaminans1
 * @property JaminanPasien[] $jaminanPasiens
 * @property PasienPenjamin[] $pasienPenjamins
 * @property PembayaranPiutangPerusahaan[] $pembayaranPiutangPerusahaans
 * @property PembayaranTagihan[] $pembayaranTagihans
 * @property JenisPenjamin $idJenisPenjamin
 * @property PiutangPerusahaan[] $piutangPerusahaans
 */
class Penjamin extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'penjamin';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('id_jenis_penjamin', 'required'),
			array('id_jenis_penjamin', 'length', 'max'=>20),
			array('nama_penjamin', 'length', 'max'=>100),
			array('no_telp, status_kerja_sama', 'length', 'max'=>45),
			array('mulai_kerja_sama, berakhir_kerja_sama, alamat', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id_penjamin, id_jenis_penjamin, nama_penjamin, mulai_kerja_sama, berakhir_kerja_sama, alamat, no_telp, status_kerja_sama', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'daftarPiutangPerusahaans' => array(self::HAS_MANY, 'DaftarPiutangPerusahaan', 'id_penjamin'),
			'daftarTagihans' => array(self::HAS_MANY, 'DaftarTagihan', 'id_penjamin'),
			'historyEditJaminans' => array(self::HAS_MANY, 'HistoryEditJaminan', 'id_penjamin_asal'),
			'historyEditJaminans1' => array(self::HAS_MANY, 'HistoryEditJaminan', 'id_penjamin_tujuan'),
			'jaminanPasiens' => array(self::HAS_MANY, 'JaminanPasien', 'id_penjamin'),
			'pasienPenjamins' => array(self::HAS_MANY, 'PasienPenjamin', 'id_penjamin'),
			'pembayaranPiutangPerusahaans' => array(self::HAS_MANY, 'PembayaranPiutangPerusahaan', 'id_penjamin'),
			'pembayaranTagihans' => array(self::HAS_MANY, 'PembayaranTagihan', 'id_penjamin'),
			'idJenisPenjamin' => array(self::BELONGS_TO, 'JenisPenjamin', 'id_jenis_penjamin'),
			'piutangPerusahaans' => array(self::HAS_MANY, 'PiutangPerusahaan', 'id_penjamin'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id_penjamin' => 'Id Penjamin',
			'id_jenis_penjamin' => 'Id Jenis Penjamin',
			'nama_penjamin' => 'Nama Penjamin',
			'mulai_kerja_sama' => 'Mulai Kerja Sama',
			'berakhir_kerja_sama' => 'Berakhir Kerja Sama',
			'alamat' => 'Alamat',
			'no_telp' => 'No Telp',
			'status_kerja_sama' => 'Status Kerja Sama',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id_penjamin',$this->id_penjamin,true);
		$criteria->compare('id_jenis_penjamin',$this->id_jenis_penjamin,true);
		$criteria->compare('nama_penjamin',$this->nama_penjamin,true);
		$criteria->compare('mulai_kerja_sama',$this->mulai_kerja_sama,true);
		$criteria->compare('berakhir_kerja_sama',$this->berakhir_kerja_sama,true);
		$criteria->compare('alamat',$this->alamat,true);
		$criteria->compare('no_telp',$this->no_telp,true);
		$criteria->compare('status_kerja_sama',$this->status_kerja_sama,true);

		return new CActiveDataProvider($this, array(
			'pagination'=>array(
				'pageSize'=> Yii::app()->user->getState('pageSize',Yii::app()->params['defaultPageSize']),
			),
			'criteria'=>$criteria,
		));
	}
	
	public function getAllowView()
	{
		$akses = UserAccess::model()->find(array("with"=>array("access"),'condition'=>"id_user='".Yii::app()->user->id."' and access.access_controller='Penjamin' and access.access_action='view'"));
		if(!empty($akses)) return true; else return false;
	}
	
	public function getAllowUpdate()
	{
		$akses = UserAccess::model()->find(array("with"=>array("access"),'condition'=>"id_user='".Yii::app()->user->id."' and access.access_controller='Penjamin' and access.access_action='update'"));
		if(!empty($akses)) return true; else return false;
	}
	
	public function getAllowDelete($id)
	{
		$akses = UserAccess::model()->find(array("with"=>array("access"),'condition'=>"id_user='".Yii::app()->user->id."' and access.access_controller='Penjamin' and access.access_action='delete'"));
		$rel=$this->getEmptyRelation($id);
		if(!empty($akses) AND $rel==true){
			return true;
		}else{
			return false;
		}
	}
	
	
	public function getEmptyRelation($id)
	{
		$count=0;
		$count+=DaftarPiutangPerusahaan::model()->count(array('condition'=>"id_penjamin='$id'"));
		$count+=DaftarTagihan::model()->count(array('condition'=>"id_penjamin='$id'"));
		$count+=HistoryEditJaminan::model()->count(array('condition'=>"id_penjamin_asal='$id'"));
		$count+=HistoryEditJaminan::model()->count(array('condition'=>"id_penjamin_tujuan='$id'"));
		$count+=JaminanPasien::model()->count(array('condition'=>"id_penjamin='$id'"));
		$count+=PasienPenjamin::model()->count(array('condition'=>"id_penjamin='$id'"));
		$count+=PembayaranPiutangPerusahaan::model()->count(array('condition'=>"id_penjamin='$id'"));
		$count+=PembayaranTagihan::model()->count(array('condition'=>"id_penjamin='$id'"));
				$count+=PiutangPerusahaan::model()->count(array('condition'=>"id_penjamin='$id'"));
		if($count>0){
			return false;
		}else{
			return true;
		}
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Penjamin the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
