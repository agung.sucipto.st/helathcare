<?php

/**
 * This is the model class for table "laboratorium_medis".
 *
 * The followings are the available columns in table 'laboratorium_medis':
 * @property string $id_laboratorium_medis
 * @property string $id_laboratorium
 * @property string $id_peran_medis
 * @property string $mandatory
 * @property string $order
 *
 * The followings are the available model relations:
 * @property Laboratorium $idLaboratorium
 * @property PeranMedis $idPeranMedis
 * @property LaboratoriumPasienMedis[] $laboratoriumPasienMedises
 * @property TarifLaboratoriumMedis[] $tarifLaboratoriumMedises
 */
class LaboratoriumMedis extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'laboratorium_medis';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('id_laboratorium, id_peran_medis, mandatory', 'required'),
			array('id_laboratorium, id_peran_medis, order', 'length', 'max'=>20),
			array('mandatory', 'length', 'max'=>1),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id_laboratorium_medis, id_laboratorium, id_peran_medis, mandatory, order', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'idLaboratorium' => array(self::BELONGS_TO, 'Laboratorium', 'id_laboratorium'),
			'idPeranMedis' => array(self::BELONGS_TO, 'PeranMedis', 'id_peran_medis'),
			'laboratoriumPasienMedises' => array(self::HAS_MANY, 'LaboratoriumPasienMedis', 'id_laboratorium_medis'),
			'tarifLaboratoriumMedises' => array(self::HAS_MANY, 'TarifLaboratoriumMedis', 'id_laboratorium_medis'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id_laboratorium_medis' => 'Id Laboratorium Medis',
			'id_laboratorium' => 'Id Laboratorium',
			'id_peran_medis' => 'Id Peran Medis',
			'mandatory' => 'Mandatory',
			'order' => 'Order',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id_laboratorium_medis',$this->id_laboratorium_medis,true);
		$criteria->compare('id_laboratorium',$this->id_laboratorium,true);
		$criteria->compare('id_peran_medis',$this->id_peran_medis,true);
		$criteria->compare('mandatory',$this->mandatory,true);
		$criteria->compare('order',$this->order,true);

		return new CActiveDataProvider($this, array(
			'pagination'=>array(
				'pageSize'=> Yii::app()->user->getState('pageSize',Yii::app()->params['defaultPageSize']),
			),
			'criteria'=>$criteria,
		));
	}
	
	public function getAllowView()
	{
		$akses = UserAccess::model()->find(array("with"=>array("access"),'condition'=>"id_user='".Yii::app()->user->id."' and access.access_controller='LaboratoriumMedis' and access.access_action='view'"));
		if(!empty($akses)) return true; else return false;
	}
	
	public function getAllowUpdate()
	{
		$akses = UserAccess::model()->find(array("with"=>array("access"),'condition'=>"id_user='".Yii::app()->user->id."' and access.access_controller='LaboratoriumMedis' and access.access_action='update'"));
		if(!empty($akses)) return true; else return false;
	}
	
	public function getAllowDelete($id)
	{
		$akses = UserAccess::model()->find(array("with"=>array("access"),'condition'=>"id_user='".Yii::app()->user->id."' and access.access_controller='LaboratoriumMedis' and access.access_action='delete'"));
		$rel=$this->getEmptyRelation($id);
		if(!empty($akses) AND $rel==true){
			return true;
		}else{
			return false;
		}
	}
	
	
	public function getEmptyRelation($id)
	{
		$count=0;
						$count+=LaboratoriumPasienMedis::model()->count(array('condition'=>"id_laboratorium_medis='$id'"));
		$count+=TarifLaboratoriumMedis::model()->count(array('condition'=>"id_laboratorium_medis='$id'"));
		if($count>0){
			return false;
		}else{
			return true;
		}
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return LaboratoriumMedis the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
