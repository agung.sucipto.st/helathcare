<?php

/**
 * This is the model class for table "tarif_ruang_rawatan".
 *
 * The followings are the available columns in table 'tarif_ruang_rawatan':
 * @property string $id_tarif_ruang_rawatan
 * @property string $id_kelompok_tarif
 * @property string $id_kelas
 * @property string $jasa_paramedis
 * @property string $biaya_makan
 * @property string $tarif_kamar
 *
 * The followings are the available model relations:
 * @property BedPasien[] $bedPasiens
 * @property KelompokTarif $idKelompokTarif
 * @property Kelas $idKelas
 */
class TarifRuangRawatan extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'tarif_ruang_rawatan';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('id_kelompok_tarif, id_kelas, jasa_paramedis, biaya_makan, tarif_kamar', 'required'),
			array('id_kelompok_tarif, id_kelas', 'length', 'max'=>20),
			array('jasa_paramedis, biaya_makan, tarif_kamar', 'length', 'max'=>30),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id_tarif_ruang_rawatan, id_kelompok_tarif, id_kelas, jasa_paramedis, biaya_makan, tarif_kamar', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'bedPasiens' => array(self::HAS_MANY, 'BedPasien', 'id_tarif_ruang_rawatan'),
			'idKelompokTarif' => array(self::BELONGS_TO, 'KelompokTarif', 'id_kelompok_tarif'),
			'idKelas' => array(self::BELONGS_TO, 'Kelas', 'id_kelas'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id_tarif_ruang_rawatan' => 'Id Tarif Ruang Rawatan',
			'id_kelompok_tarif' => 'Id Kelompok Tarif',
			'id_kelas' => 'Id Kelas',
			'jasa_paramedis' => 'Jasa Paramedis',
			'biaya_makan' => 'Biaya Makan',
			'tarif_kamar' => 'Tarif Kamar',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id_tarif_ruang_rawatan',$this->id_tarif_ruang_rawatan,true);
		$criteria->compare('id_kelompok_tarif',$this->id_kelompok_tarif,true);
		$criteria->compare('id_kelas',$this->id_kelas,true);
		$criteria->compare('jasa_paramedis',$this->jasa_paramedis,true);
		$criteria->compare('biaya_makan',$this->biaya_makan,true);
		$criteria->compare('tarif_kamar',$this->tarif_kamar,true);

		return new CActiveDataProvider($this, array(
			'pagination'=>array(
				'pageSize'=> Yii::app()->user->getState('pageSize',Yii::app()->params['defaultPageSize']),
			),
			'criteria'=>$criteria,
		));
	}
	
	public function getAllowView()
	{
		$akses = UserAccess::model()->find(array("with"=>array("access"),'condition'=>"id_user='".Yii::app()->user->id."' and access.access_controller='TarifRuangRawatan' and access.access_action='view'"));
		if(!empty($akses)) return true; else return false;
	}
	
	public function getAllowUpdate()
	{
		$akses = UserAccess::model()->find(array("with"=>array("access"),'condition'=>"id_user='".Yii::app()->user->id."' and access.access_controller='TarifRuangRawatan' and access.access_action='update'"));
		if(!empty($akses)) return true; else return false;
	}
	
	public function getAllowDelete($id)
	{
		$akses = UserAccess::model()->find(array("with"=>array("access"),'condition'=>"id_user='".Yii::app()->user->id."' and access.access_controller='TarifRuangRawatan' and access.access_action='delete'"));
		$rel=$this->getEmptyRelation($id);
		if(!empty($akses) AND $rel==true){
			return true;
		}else{
			return false;
		}
	}
	
	
	public function getEmptyRelation($id)
	{
		$count=0;
		$count+=BedPasien::model()->count(array('condition'=>"id_tarif_ruang_rawatan='$id'"));
						if($count>0){
			return false;
		}else{
			return true;
		}
	}
	
	public static function getTarif($reg,$kelas){
		$id_jenis_departemen=$reg->idDepartemen->id_jenis_departemen;
		if($reg->jenis_jaminan=="UMUM"){
			$id_kelompok_tarif=1;
		}else{
			$jaminan=JaminanPasien::model()->find(array("condition"=>"id_registrasi='$reg->id_registrasi' AND is_utama='1'"));
			$id_kelompok_tarif=$jaminan->idPenjamin->id_kelompok_tarif;
		}
		
		$model=TarifRuangRawatan::model()->find(array("condition"=>"id_kelompok_tarif='$id_kelompok_tarif' AND id_kelas='$kelas'"));
		
		return $model;
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return TarifRuangRawatan the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
