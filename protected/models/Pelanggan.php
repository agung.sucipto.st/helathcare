<?php

/**
 * This is the model class for table "pelanggan".
 *
 * The followings are the available columns in table 'pelanggan':
 * @property string $id_pelanggan
 * @property string $nama_pelanggan
 * @property string $alamat
 * @property string $no_telpon
 * @property string $nama_bank
 * @property string $no_rekening
 * @property string $nama_pemegang_akun
 * @property integer $id_coa_piutang
 * @property string $is_aktif
 *
 * The followings are the available model relations:
 * @property GlCoa $idCoaPiutang
 * @property PembayaranPiutangPelanggan[] $pembayaranPiutangPelanggans
 * @property PiutangPelanggan[] $piutangPelanggans
 */
class Pelanggan extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'pelanggan';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('nama_bank, no_rekening, nama_pemegang_akun, id_coa_piutang', 'required'),
			array('id_coa_piutang', 'numerical', 'integerOnly'=>true),
			array('nama_pelanggan', 'length', 'max'=>100),
			array('no_telpon', 'length', 'max'=>45),
			array('nama_bank, nama_pemegang_akun', 'length', 'max'=>50),
			array('no_rekening', 'length', 'max'=>30),
			array('is_aktif', 'length', 'max'=>1),
			array('alamat', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id_pelanggan, nama_pelanggan, alamat, no_telpon, nama_bank, no_rekening, nama_pemegang_akun, id_coa_piutang, is_aktif', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'idCoaPiutang' => array(self::BELONGS_TO, 'GlCoa', 'id_coa_piutang'),
			'pembayaranPiutangPelanggans' => array(self::HAS_MANY, 'PembayaranPiutangPelanggan', 'id_pelanggan'),
			'piutangPelanggans' => array(self::HAS_MANY, 'PiutangPelanggan', 'id_pelanggan'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id_pelanggan' => 'Id Pelanggan',
			'nama_pelanggan' => 'Nama Pelanggan',
			'alamat' => 'Alamat',
			'no_telpon' => 'No Telpon',
			'nama_bank' => 'Nama Bank',
			'no_rekening' => 'No Rekening',
			'nama_pemegang_akun' => 'Nama Pemegang Akun',
			'id_coa_piutang' => 'Id Coa Piutang',
			'is_aktif' => 'Is Aktif',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id_pelanggan',$this->id_pelanggan,true);
		$criteria->compare('nama_pelanggan',$this->nama_pelanggan,true);
		$criteria->compare('alamat',$this->alamat,true);
		$criteria->compare('no_telpon',$this->no_telpon,true);
		$criteria->compare('nama_bank',$this->nama_bank,true);
		$criteria->compare('no_rekening',$this->no_rekening,true);
		$criteria->compare('nama_pemegang_akun',$this->nama_pemegang_akun,true);
		$criteria->compare('id_coa_piutang',$this->id_coa_piutang);
		$criteria->compare('is_aktif',$this->is_aktif,true);

		return new CActiveDataProvider($this, array(
			'pagination'=>array(
				'pageSize'=> Yii::app()->user->getState('pageSize',Yii::app()->params['defaultPageSize']),
			),
			'criteria'=>$criteria,
		));
	}
	
	public function getAllowView()
	{
		$akses = UserAccess::model()->find(array("with"=>array("access"),'condition'=>"id_user='".Yii::app()->user->id."' and access.access_controller='Pelanggan' and access.access_action='view'"));
		if(!empty($akses)) return true; else return false;
	}
	
	public function getAllowUpdate()
	{
		$akses = UserAccess::model()->find(array("with"=>array("access"),'condition'=>"id_user='".Yii::app()->user->id."' and access.access_controller='Pelanggan' and access.access_action='update'"));
		if(!empty($akses)) return true; else return false;
	}
	
	public function getAllowDelete($id)
	{
		$akses = UserAccess::model()->find(array("with"=>array("access"),'condition'=>"id_user='".Yii::app()->user->id."' and access.access_controller='Pelanggan' and access.access_action='delete'"));
		$rel=$this->getEmptyRelation($id);
		if(!empty($akses) AND $rel==true){
			return true;
		}else{
			return false;
		}
	}
	
	
	public function getEmptyRelation($id)
	{
		$count=0;
				$count+=PembayaranPiutangPelanggan::model()->count(array('condition'=>"id_pelanggan='$id'"));
		$count+=PiutangPelanggan::model()->count(array('condition'=>"id_pelanggan='$id'"));
		if($count>0){
			return false;
		}else{
			return true;
		}
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Pelanggan the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
