<?php

/**
 * This is the model class for table "paket_pasien_medis".
 *
 * The followings are the available columns in table 'paket_pasien_medis':
 * @property string $id_paket_pasien_medis
 * @property string $id_paket_pasien_list
 * @property string $id_paket_detail_medis
 * @property integer $jasa_medis
 * @property integer $jasa_medis_org
 *
 * The followings are the available model relations:
 * @property DaftarJasaMedis[] $daftarJasaMedises
 * @property PaketPasienList $idPaketPasienList
 * @property PaketDetailMedis $idPaketDetailMedis
 */
class PaketPasienMedis extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'paket_pasien_medis';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('id_paket_pasien_medis, id_paket_pasien_list, id_paket_detail_medis, jasa_medis, jasa_medis_org', 'required'),
			array('jasa_medis, jasa_medis_org', 'numerical', 'integerOnly'=>true),
			array('id_paket_pasien_medis, id_paket_pasien_list, id_paket_detail_medis', 'length', 'max'=>20),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id_paket_pasien_medis, id_paket_pasien_list, id_paket_detail_medis, jasa_medis, jasa_medis_org', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'daftarJasaMedises' => array(self::HAS_MANY, 'DaftarJasaMedis', 'id_paket_pasien_medis'),
			'idPaketPasienList' => array(self::BELONGS_TO, 'PaketPasienList', 'id_paket_pasien_list'),
			'idPaketDetailMedis' => array(self::BELONGS_TO, 'PaketDetailMedis', 'id_paket_detail_medis'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id_paket_pasien_medis' => 'Id Paket Pasien Medis',
			'id_paket_pasien_list' => 'Id Paket Pasien List',
			'id_paket_detail_medis' => 'Id Paket Detail Medis',
			'jasa_medis' => 'Jasa Medis',
			'jasa_medis_org' => 'Jasa Medis Org',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id_paket_pasien_medis',$this->id_paket_pasien_medis,true);
		$criteria->compare('id_paket_pasien_list',$this->id_paket_pasien_list,true);
		$criteria->compare('id_paket_detail_medis',$this->id_paket_detail_medis,true);
		$criteria->compare('jasa_medis',$this->jasa_medis);
		$criteria->compare('jasa_medis_org',$this->jasa_medis_org);

		return new CActiveDataProvider($this, array(
			'pagination'=>array(
				'pageSize'=> Yii::app()->user->getState('pageSize',Yii::app()->params['defaultPageSize']),
			),
			'criteria'=>$criteria,
		));
	}
	
	public function getAllowView()
	{
		$akses = UserAccess::model()->find(array("with"=>array("access"),'condition'=>"id_user='".Yii::app()->user->id."' and access.access_controller='PaketPasienMedis' and access.access_action='view'"));
		if(!empty($akses)) return true; else return false;
	}
	
	public function getAllowUpdate()
	{
		$akses = UserAccess::model()->find(array("with"=>array("access"),'condition'=>"id_user='".Yii::app()->user->id."' and access.access_controller='PaketPasienMedis' and access.access_action='update'"));
		if(!empty($akses)) return true; else return false;
	}
	
	public function getAllowDelete($id)
	{
		$akses = UserAccess::model()->find(array("with"=>array("access"),'condition'=>"id_user='".Yii::app()->user->id."' and access.access_controller='PaketPasienMedis' and access.access_action='delete'"));
		$rel=$this->getEmptyRelation($id);
		if(!empty($akses) AND $rel==true){
			return true;
		}else{
			return false;
		}
	}
	
	
	public function getEmptyRelation($id)
	{
		$count=0;
		$count+=DaftarJasaMedis::model()->count(array('condition'=>"id_paket_pasien_medis='$id'"));
						if($count>0){
			return false;
		}else{
			return true;
		}
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return PaketPasienMedis the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
