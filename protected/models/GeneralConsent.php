<?php

/**
 * This is the model class for table "general_consent".
 *
 * The followings are the available columns in table 'general_consent':
 * @property string $id_general_consent
 * @property string $id_registrasi
 * @property string $id_ruangan_rawatan
 * @property string $nama
 * @property string $alamat
 * @property string $telp
 * @property string $pelepasan_informasi
 * @property string $privasi
 * @property string $jenis_obat
 * @property string $tgl_ttd
 * @property string $user_create
 * @property string $user_update
 * @property string $tgl_create
 * @property string $tgl_update
 */
class GeneralConsent extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'general_consent';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('id_general_consent', 'required'),
			array('id_general_consent, id_registrasi, id_ruangan_rawatan, user_create, user_update', 'length', 'max'=>20),
			array('nama', 'length', 'max'=>35),
			array('alamat, pelepasan_informasi, privasi', 'length', 'max'=>100),
			array('telp', 'length', 'max'=>15),
			array('jenis_obat', 'length', 'max'=>7),
			array('tgl_ttd, tgl_create, tgl_update', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id_general_consent, id_registrasi, id_ruangan_rawatan, nama, alamat, telp, pelepasan_informasi, privasi, jenis_obat, tgl_ttd, user_create, user_update, tgl_create, tgl_update', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id_general_consent' => 'Id General Consent',
			'id_registrasi' => 'Id Registrasi',
			'id_ruangan_rawatan' => 'Id Ruangan Rawatan',
			'nama' => 'Nama',
			'alamat' => 'Alamat',
			'telp' => 'Telp',
			'pelepasan_informasi' => 'Pelepasan Informasi',
			'privasi' => 'Privasi',
			'jenis_obat' => 'Jenis Obat',
			'tgl_ttd' => 'Tgl Ttd',
			'user_create' => 'User Create',
			'user_update' => 'User Update',
			'tgl_create' => 'Tgl Create',
			'tgl_update' => 'Tgl Update',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id_general_consent',$this->id_general_consent,true);
		$criteria->compare('id_registrasi',$this->id_registrasi,true);
		$criteria->compare('id_ruangan_rawatan',$this->id_ruangan_rawatan,true);
		$criteria->compare('nama',$this->nama,true);
		$criteria->compare('alamat',$this->alamat,true);
		$criteria->compare('telp',$this->telp,true);
		$criteria->compare('pelepasan_informasi',$this->pelepasan_informasi,true);
		$criteria->compare('privasi',$this->privasi,true);
		$criteria->compare('jenis_obat',$this->jenis_obat,true);
		$criteria->compare('tgl_ttd',$this->tgl_ttd,true);
		$criteria->compare('user_create',$this->user_create,true);
		$criteria->compare('user_update',$this->user_update,true);
		$criteria->compare('tgl_create',$this->tgl_create,true);
		$criteria->compare('tgl_update',$this->tgl_update,true);

		return new CActiveDataProvider($this, array(
			'pagination'=>array(
				'pageSize'=> Yii::app()->user->getState('pageSize',Yii::app()->params['defaultPageSize']),
			),
			'criteria'=>$criteria,
		));
	}
	
	public function getAllowView()
	{
		$akses = UserAccess::model()->find(array("with"=>array("access"),'condition'=>"id_user='".Yii::app()->user->id."' and access.access_controller='GeneralConsent' and access.access_action='view'"));
		if(!empty($akses)) return true; else return false;
	}
	
	public function getAllowUpdate()
	{
		$akses = UserAccess::model()->find(array("with"=>array("access"),'condition'=>"id_user='".Yii::app()->user->id."' and access.access_controller='GeneralConsent' and access.access_action='update'"));
		if(!empty($akses)) return true; else return false;
	}
	
	public function getAllowDelete($id)
	{
		$akses = UserAccess::model()->find(array("with"=>array("access"),'condition'=>"id_user='".Yii::app()->user->id."' and access.access_controller='GeneralConsent' and access.access_action='delete'"));
		$rel=$this->getEmptyRelation($id);
		if(!empty($akses) AND $rel==true){
			return true;
		}else{
			return false;
		}
	}
	
	
	public function getEmptyRelation($id)
	{
		$count=0;
		if($count>0){
			return false;
		}else{
			return true;
		}
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return GeneralConsent the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
