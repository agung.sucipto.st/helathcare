<?php

/**
 * This is the model class for table "item_gudang".
 *
 * The followings are the available columns in table 'item_gudang':
 * @property string $id_item_gudang
 * @property string $id_gudang
 * @property string $id_item
 * @property string $stok
 *
 * The followings are the available model relations:
 * @property DaftarItemTransaksi[] $daftarItemTransaksis
 * @property Gudang $idGudang
 * @property Item $idItem
 */
class ItemGudang extends CActiveRecord
{
	public $stock;
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'item_gudang';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('id_gudang, id_item', 'required'),
			array('id_gudang, id_item', 'length', 'max'=>20),
			array('stok', 'length', 'max'=>45),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id_item_gudang, id_gudang, id_item, stok', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'daftarItemTransaksis' => array(self::HAS_MANY, 'DaftarItemTransaksi', 'id_item_gudang'),
			'idGudang' => array(self::BELONGS_TO, 'Gudang', 'id_gudang'),
			'idItem' => array(self::BELONGS_TO, 'Item', 'id_item'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id_item_gudang' => 'Id Item Gudang',
			'id_gudang' => 'Id Gudang',
			'id_item' => 'Id Item',
			'stok' => 'Stok',
		);
	}
	
	public static function getId($idItem,$gudang){
		$model=ItemGudang::model()->find(array("select"=>"id_item_gudang","condition"=>"id_item='$idItem' and id_gudang='$gudang'"));
		return $model->id_item_gudang;
	}
	
	public static function getStock($id,$gudang){
		$model=ItemGudang::model()->find(array("select"=>"stok","condition"=>"id_item='$id' and id_gudang='$gudang'"));
		return $model->stok;
	}
	
	public static function updateStock($id,$gudang,$stock){
		$model=ItemGudang::model()->find(array("condition"=>"id_item='$id' and id_gudang='$gudang'"));
		$model->stok=$stock;
		$model->save();
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id_item_gudang',$this->id_item_gudang,true);
		$criteria->compare('id_gudang',$this->id_gudang,true);
		$criteria->compare('id_item',$this->id_item,true);
		$criteria->compare('stok',$this->stok,true);

		return new CActiveDataProvider($this, array(
			'pagination'=>array(
				'pageSize'=> Yii::app()->user->getState('pageSize',Yii::app()->params['defaultPageSize']),
			),
			'criteria'=>$criteria,
		));
	}
	
	public function getAllowView()
	{
		$akses = UserAccess::model()->find(array("with"=>array("access"),'condition'=>"id_user='".Yii::app()->user->id."' and access.access_controller='ItemGudang' and access.access_action='view'"));
		if(!empty($akses)) return true; else return false;
	}
	
	public function getAllowUpdate()
	{
		$akses = UserAccess::model()->find(array("with"=>array("access"),'condition'=>"id_user='".Yii::app()->user->id."' and access.access_controller='ItemGudang' and access.access_action='update'"));
		if(!empty($akses)) return true; else return false;
	}
	
	public function getAllowDelete($id)
	{
		$akses = UserAccess::model()->find(array("with"=>array("access"),'condition'=>"id_user='".Yii::app()->user->id."' and access.access_controller='ItemGudang' and access.access_action='delete'"));
		$rel=$this->getEmptyRelation($id);
		if(!empty($akses) AND $rel==true){
			return true;
		}else{
			return false;
		}
	}
	
	
	public function getEmptyRelation($id)
	{
		$count=0;
		$count+=DaftarItemTransaksi::model()->count(array('condition'=>"id_item_gudang='$id'"));
						if($count>0){
			return false;
		}else{
			return true;
		}
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return ItemGudang the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
