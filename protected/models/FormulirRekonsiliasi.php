<?php

/**
 * This is the model class for table "formulir_rekonsiliasi".
 *
 * The followings are the available columns in table 'formulir_rekonsiliasi':
 * @property string $id_formulir_rekonsiliasi
 * @property string $id_registrasi
 * @property string $nama_obat
 * @property string $manifestasi_alergi
 * @property string $dampak
 * @property string $dosis
 * @property string $frekuensi
 * @property string $cara_pemberian
 * @property string $jumlah
 * @property string $dirawat
 * @property string $keluar
 * @property string $nama_perawat
 * @property string $nama_dokter
 * @property string $nama_farmasi
 * @property string $tgl_perawat
 * @property string $tgl_dokter
 * @property string $tgl_farmasi
 * @property string $user_create
 * @property string $user_update
 * @property string $tgl_create
 * @property string $tgl_update
 * @property string $status
 */
class FormulirRekonsiliasi extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'formulir_rekonsiliasi';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('id_formulir_rekonsiliasi', 'required'),
			array('id_formulir_rekonsiliasi, id_registrasi, user_create, user_update', 'length', 'max'=>20),
			array('nama_obat, manifestasi_alergi', 'length', 'max'=>100),
			array('dampak', 'length', 'max'=>6),
			array('dosis, frekuensi, cara_pemberian, jumlah, nama_perawat, nama_dokter, nama_farmasi', 'length', 'max'=>35),
			array('dirawat, keluar', 'length', 'max'=>5),
			array('status', 'length', 'max'=>1),
			array('tgl_perawat, tgl_dokter, tgl_farmasi, tgl_create, tgl_update', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id_formulir_rekonsiliasi, id_registrasi, nama_obat, manifestasi_alergi, dampak, dosis, frekuensi, cara_pemberian, jumlah, dirawat, keluar, nama_perawat, nama_dokter, nama_farmasi, tgl_perawat, tgl_dokter, tgl_farmasi, user_create, user_update, tgl_create, tgl_update, status', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id_formulir_rekonsiliasi' => 'Id Formulir Rekonsiliasi',
			'id_registrasi' => 'Id Registrasi',
			'nama_obat' => 'Nama Obat',
			'manifestasi_alergi' => 'Manifestasi Alergi',
			'dampak' => 'Dampak',
			'dosis' => 'Dosis',
			'frekuensi' => 'Frekuensi',
			'cara_pemberian' => 'Cara Pemberian',
			'jumlah' => 'Jumlah',
			'dirawat' => 'Dirawat',
			'keluar' => 'Keluar',
			'nama_perawat' => 'Nama Perawat',
			'nama_dokter' => 'Nama Dokter',
			'nama_farmasi' => 'Nama Farmasi',
			'tgl_perawat' => 'Tgl Perawat',
			'tgl_dokter' => 'Tgl Dokter',
			'tgl_farmasi' => 'Tgl Farmasi',
			'user_create' => 'User Create',
			'user_update' => 'User Update',
			'tgl_create' => 'Tgl Create',
			'tgl_update' => 'Tgl Update',
			'status' => 'Status',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id_formulir_rekonsiliasi',$this->id_formulir_rekonsiliasi,true);
		$criteria->compare('id_registrasi',$this->id_registrasi,true);
		$criteria->compare('nama_obat',$this->nama_obat,true);
		$criteria->compare('manifestasi_alergi',$this->manifestasi_alergi,true);
		$criteria->compare('dampak',$this->dampak,true);
		$criteria->compare('dosis',$this->dosis,true);
		$criteria->compare('frekuensi',$this->frekuensi,true);
		$criteria->compare('cara_pemberian',$this->cara_pemberian,true);
		$criteria->compare('jumlah',$this->jumlah,true);
		$criteria->compare('dirawat',$this->dirawat,true);
		$criteria->compare('keluar',$this->keluar,true);
		$criteria->compare('nama_perawat',$this->nama_perawat,true);
		$criteria->compare('nama_dokter',$this->nama_dokter,true);
		$criteria->compare('nama_farmasi',$this->nama_farmasi,true);
		$criteria->compare('tgl_perawat',$this->tgl_perawat,true);
		$criteria->compare('tgl_dokter',$this->tgl_dokter,true);
		$criteria->compare('tgl_farmasi',$this->tgl_farmasi,true);
		$criteria->compare('user_create',$this->user_create,true);
		$criteria->compare('user_update',$this->user_update,true);
		$criteria->compare('tgl_create',$this->tgl_create,true);
		$criteria->compare('tgl_update',$this->tgl_update,true);
		$criteria->compare('status',$this->status,true);

		return new CActiveDataProvider($this, array(
			'pagination'=>array(
				'pageSize'=> Yii::app()->user->getState('pageSize',Yii::app()->params['defaultPageSize']),
			),
			'criteria'=>$criteria,
		));
	}
	
	public function getAllowView()
	{
		$akses = UserAccess::model()->find(array("with"=>array("access"),'condition'=>"id_user='".Yii::app()->user->id."' and access.access_controller='FormulirRekonsiliasi' and access.access_action='view'"));
		if(!empty($akses)) return true; else return false;
	}
	
	public function getAllowUpdate()
	{
		$akses = UserAccess::model()->find(array("with"=>array("access"),'condition'=>"id_user='".Yii::app()->user->id."' and access.access_controller='FormulirRekonsiliasi' and access.access_action='update'"));
		if(!empty($akses)) return true; else return false;
	}
	
	public function getAllowDelete($id)
	{
		$akses = UserAccess::model()->find(array("with"=>array("access"),'condition'=>"id_user='".Yii::app()->user->id."' and access.access_controller='FormulirRekonsiliasi' and access.access_action='delete'"));
		$rel=$this->getEmptyRelation($id);
		if(!empty($akses) AND $rel==true){
			return true;
		}else{
			return false;
		}
	}
	
	
	public function getEmptyRelation($id)
	{
		$count=0;
		if($count>0){
			return false;
		}else{
			return true;
		}
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return FormulirRekonsiliasi the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
