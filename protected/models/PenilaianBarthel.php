<?php

/**
 * This is the model class for table "penilaian_barthel".
 *
 * The followings are the available columns in table 'penilaian_barthel':
 * @property string $id_penilaian_barthel
 * @property string $id_registrasi
 * @property integer $parameter_i
 * @property integer $parameter_ii
 * @property integer $parameter_iii
 * @property integer $parameter_iv
 * @property integer $parameter_v
 * @property integer $parameter_vi
 * @property integer $parameter_vii
 * @property integer $parameter_viii
 * @property integer $parameter_ix
 * @property integer $parameter_x
 * @property string $user_create
 * @property string $user_update
 * @property string $tgl_create
 * @property string $tgl_update
 * @property string $nama_penilai
 * @property string $tanggal_perawatan
 */
class PenilaianBarthel extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'penilaian_barthel';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('id_penilaian_barthel', 'required'),
			array('parameter_i, parameter_ii, parameter_iii, parameter_iv, parameter_v, parameter_vi, parameter_vii, parameter_viii, parameter_ix, parameter_x', 'numerical', 'integerOnly'=>true),
			array('id_penilaian_barthel, id_registrasi, user_create, user_update', 'length', 'max'=>20),
			array('nama_penilai', 'length', 'max'=>35),
			array('tgl_create, tgl_update, tanggal_perawatan', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id_penilaian_barthel, id_registrasi, parameter_i, parameter_ii, parameter_iii, parameter_iv, parameter_v, parameter_vi, parameter_vii, parameter_viii, parameter_ix, parameter_x, user_create, user_update, tgl_create, tgl_update, nama_penilai, tanggal_perawatan', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id_penilaian_barthel' => 'Id Penilaian Barthel',
			'id_registrasi' => 'Id Registrasi',
			'parameter_i' => 'Parameter I',
			'parameter_ii' => 'Parameter Ii',
			'parameter_iii' => 'Parameter Iii',
			'parameter_iv' => 'Parameter Iv',
			'parameter_v' => 'Parameter V',
			'parameter_vi' => 'Parameter Vi',
			'parameter_vii' => 'Parameter Vii',
			'parameter_viii' => 'Parameter Viii',
			'parameter_ix' => 'Parameter Ix',
			'parameter_x' => 'Parameter X',
			'user_create' => 'User Create',
			'user_update' => 'User Update',
			'tgl_create' => 'Tgl Create',
			'tgl_update' => 'Tgl Update',
			'nama_penilai' => 'Nama Penilai',
			'tanggal_perawatan' => 'Tanggal Perawatan',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id_penilaian_barthel',$this->id_penilaian_barthel,true);
		$criteria->compare('id_registrasi',$this->id_registrasi,true);
		$criteria->compare('parameter_i',$this->parameter_i);
		$criteria->compare('parameter_ii',$this->parameter_ii);
		$criteria->compare('parameter_iii',$this->parameter_iii);
		$criteria->compare('parameter_iv',$this->parameter_iv);
		$criteria->compare('parameter_v',$this->parameter_v);
		$criteria->compare('parameter_vi',$this->parameter_vi);
		$criteria->compare('parameter_vii',$this->parameter_vii);
		$criteria->compare('parameter_viii',$this->parameter_viii);
		$criteria->compare('parameter_ix',$this->parameter_ix);
		$criteria->compare('parameter_x',$this->parameter_x);
		$criteria->compare('user_create',$this->user_create,true);
		$criteria->compare('user_update',$this->user_update,true);
		$criteria->compare('tgl_create',$this->tgl_create,true);
		$criteria->compare('tgl_update',$this->tgl_update,true);
		$criteria->compare('nama_penilai',$this->nama_penilai,true);
		$criteria->compare('tanggal_perawatan',$this->tanggal_perawatan,true);

		return new CActiveDataProvider($this, array(
			'pagination'=>array(
				'pageSize'=> Yii::app()->user->getState('pageSize',Yii::app()->params['defaultPageSize']),
			),
			'criteria'=>$criteria,
		));
	}
	
	public function getAllowView()
	{
		$akses = UserAccess::model()->find(array("with"=>array("access"),'condition'=>"id_user='".Yii::app()->user->id."' and access.access_controller='PenilaianBarthel' and access.access_action='view'"));
		if(!empty($akses)) return true; else return false;
	}
	
	public function getAllowUpdate()
	{
		$akses = UserAccess::model()->find(array("with"=>array("access"),'condition'=>"id_user='".Yii::app()->user->id."' and access.access_controller='PenilaianBarthel' and access.access_action='update'"));
		if(!empty($akses)) return true; else return false;
	}
	
	public function getAllowDelete($id)
	{
		$akses = UserAccess::model()->find(array("with"=>array("access"),'condition'=>"id_user='".Yii::app()->user->id."' and access.access_controller='PenilaianBarthel' and access.access_action='delete'"));
		$rel=$this->getEmptyRelation($id);
		if(!empty($akses) AND $rel==true){
			return true;
		}else{
			return false;
		}
	}
	
	
	public function getEmptyRelation($id)
	{
		$count=0;
		if($count>0){
			return false;
		}else{
			return true;
		}
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return PenilaianBarthel the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
