<?php

/**
 * This is the model class for table "radiology".
 *
 * The followings are the available columns in table 'radiology':
 * @property string $id_radiology
 * @property string $id_jenis_pemeriksaan
 * @property string $nama_pemeriksaan
 * @property string $status_pemeriksaan
 *
 * The followings are the available model relations:
 * @property JenisPemeriksaan $idJenisPemeriksaan
 * @property RadiologyMedis[] $radiologyMedises
 * @property RadiologyPasienList[] $radiologyPasienLists
 * @property RadiologyPasienMedis[] $radiologyPasienMedises
 * @property TarifRadiology[] $tarifRadiologies
 */
class Radiology extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'radiology';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('id_radiology, id_jenis_pemeriksaan', 'required'),
			array('id_radiology, id_jenis_pemeriksaan', 'length', 'max'=>20),
			array('nama_pemeriksaan', 'length', 'max'=>100),
			array('status_pemeriksaan', 'length', 'max'=>11),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id_radiology, id_jenis_pemeriksaan, nama_pemeriksaan, status_pemeriksaan', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'idJenisPemeriksaan' => array(self::BELONGS_TO, 'JenisPemeriksaan', 'id_jenis_pemeriksaan'),
			'radiologyMedises' => array(self::HAS_MANY, 'RadiologyMedis', 'id_radiology'),
			'radiologyPasienLists' => array(self::HAS_MANY, 'RadiologyPasienList', 'id_radiology'),
			'radiologyPasienMedises' => array(self::HAS_MANY, 'RadiologyPasienMedis', 'id_radiology'),
			'tarifRadiologies' => array(self::HAS_MANY, 'TarifRadiology', 'id_radiology'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id_radiology' => 'Id Radiology',
			'id_jenis_pemeriksaan' => 'Id Jenis Pemeriksaan',
			'nama_pemeriksaan' => 'Nama Pemeriksaan',
			'status_pemeriksaan' => 'Status Pemeriksaan',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id_radiology',$this->id_radiology,true);
		$criteria->compare('id_jenis_pemeriksaan',$this->id_jenis_pemeriksaan,true);
		$criteria->compare('nama_pemeriksaan',$this->nama_pemeriksaan,true);
		$criteria->compare('status_pemeriksaan',$this->status_pemeriksaan,true);

		return new CActiveDataProvider($this, array(
			'pagination'=>array(
				'pageSize'=> Yii::app()->user->getState('pageSize',Yii::app()->params['defaultPageSize']),
			),
			'criteria'=>$criteria,
		));
	}
	
	public function getAllowView()
	{
		$akses = UserAccess::model()->find(array("with"=>array("access"),'condition'=>"id_user='".Yii::app()->user->id."' and access.access_controller='Radiology' and access.access_action='view'"));
		if(!empty($akses)) return true; else return false;
	}
	
	public function getAllowUpdate()
	{
		$akses = UserAccess::model()->find(array("with"=>array("access"),'condition'=>"id_user='".Yii::app()->user->id."' and access.access_controller='Radiology' and access.access_action='update'"));
		if(!empty($akses)) return true; else return false;
	}
	
	public function getAllowDelete($id)
	{
		$akses = UserAccess::model()->find(array("with"=>array("access"),'condition'=>"id_user='".Yii::app()->user->id."' and access.access_controller='Radiology' and access.access_action='delete'"));
		$rel=$this->getEmptyRelation($id);
		if(!empty($akses) AND $rel==true){
			return true;
		}else{
			return false;
		}
	}
	
	
	public function getEmptyRelation($id)
	{
		$count=0;
				$count+=RadiologyMedis::model()->count(array('condition'=>"id_radiology='$id'"));
		$count+=RadiologyPasienList::model()->count(array('condition'=>"id_radiology='$id'"));
		$count+=RadiologyPasienMedis::model()->count(array('condition'=>"id_radiology='$id'"));
		$count+=TarifRadiology::model()->count(array('condition'=>"id_radiology='$id'"));
		if($count>0){
			return false;
		}else{
			return true;
		}
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Radiology the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
