<?php

/**
 * This is the model class for table "paket_detail_medis".
 *
 * The followings are the available columns in table 'paket_detail_medis':
 * @property string $id_paket_detail_medis
 * @property string $id_paket_detail
 * @property string $id_tindakan_medis
 * @property string $id_laboratorium_medis
 * @property string $id_radiology_medis
 * @property string $jasa_medis
 *
 * The followings are the available model relations:
 * @property PaketDetail $idPaketDetail
 * @property TindakanMedis $idTindakanMedis
 * @property LaboratoriumMedis $idLaboratoriumMedis
 * @property RadiologyMedis $idRadiologyMedis
 * @property PaketPasienMedis[] $paketPasienMedises
 */
class PaketDetailMedis extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'paket_detail_medis';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('id_paket_detail, jasa_medis', 'required'),
			array('id_paket_detail, id_tindakan_medis, id_laboratorium_medis, id_radiology_medis', 'length', 'max'=>20),
			array('jasa_medis', 'length', 'max'=>30),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id_paket_detail_medis, id_paket_detail, id_tindakan_medis, id_laboratorium_medis, id_radiology_medis, jasa_medis', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'idPaketDetail' => array(self::BELONGS_TO, 'PaketDetail', 'id_paket_detail'),
			'idTindakanMedis' => array(self::BELONGS_TO, 'TindakanMedis', 'id_tindakan_medis'),
			'idLaboratoriumMedis' => array(self::BELONGS_TO, 'LaboratoriumMedis', 'id_laboratorium_medis'),
			'idRadiologyMedis' => array(self::BELONGS_TO, 'RadiologyMedis', 'id_radiology_medis'),
			'paketPasienMedises' => array(self::HAS_MANY, 'PaketPasienMedis', 'id_paket_detail_medis'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id_paket_detail_medis' => 'Id Paket Detail Medis',
			'id_paket_detail' => 'Id Paket Detail',
			'id_tindakan_medis' => 'Id Tindakan Medis',
			'id_laboratorium_medis' => 'Id Laboratorium Medis',
			'id_radiology_medis' => 'Id Radiology Medis',
			'jasa_medis' => 'Jasa Medis',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id_paket_detail_medis',$this->id_paket_detail_medis,true);
		$criteria->compare('id_paket_detail',$this->id_paket_detail,true);
		$criteria->compare('id_tindakan_medis',$this->id_tindakan_medis,true);
		$criteria->compare('id_laboratorium_medis',$this->id_laboratorium_medis,true);
		$criteria->compare('id_radiology_medis',$this->id_radiology_medis,true);
		$criteria->compare('jasa_medis',$this->jasa_medis,true);

		return new CActiveDataProvider($this, array(
			'pagination'=>array(
				'pageSize'=> Yii::app()->user->getState('pageSize',Yii::app()->params['defaultPageSize']),
			),
			'criteria'=>$criteria,
		));
	}
	
	public function getAllowView()
	{
		$akses = UserAccess::model()->find(array("with"=>array("access"),'condition'=>"id_user='".Yii::app()->user->id."' and access.access_controller='PaketDetailMedis' and access.access_action='view'"));
		if(!empty($akses)) return true; else return false;
	}
	
	public function getAllowUpdate()
	{
		$akses = UserAccess::model()->find(array("with"=>array("access"),'condition'=>"id_user='".Yii::app()->user->id."' and access.access_controller='PaketDetailMedis' and access.access_action='update'"));
		if(!empty($akses)) return true; else return false;
	}
	
	public function getAllowDelete($id)
	{
		$akses = UserAccess::model()->find(array("with"=>array("access"),'condition'=>"id_user='".Yii::app()->user->id."' and access.access_controller='PaketDetailMedis' and access.access_action='delete'"));
		$rel=$this->getEmptyRelation($id);
		if(!empty($akses) AND $rel==true){
			return true;
		}else{
			return false;
		}
	}
	
	
	public function getEmptyRelation($id)
	{
		$count=0;
										$count+=PaketPasienMedis::model()->count(array('condition'=>"id_paket_detail_medis='$id'"));
		if($count>0){
			return false;
		}else{
			return true;
		}
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return PaketDetailMedis the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
