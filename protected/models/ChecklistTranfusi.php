<?php

/**
 * This is the model class for table "checklist_tranfusi".
 *
 * The followings are the available columns in table 'checklist_tranfusi':
 * @property string $id_checklist_tranfusi
 * @property string $id_registrasi
 * @property string $infomed_consent
 * @property string $tgl_pemberian
 * @property string $jumlah_darah
 * @property string $jenis_darah
 * @property string $mulai_jam_pemberian
 * @property string $sampai_jam_pemberian
 * @property string $nomor_kantong
 * @property string $golongan_darah
 * @property string $tgl_kadaluarsa
 * @property string $jenis_darah_i
 * @property string $jenis_darah_ii
 * @property string $jenis_darah_iii
 * @property string $jenis_darah_iv
 * @property string $golongan_i
 * @property string $golongan_ii
 * @property string $golongan_iii
 * @property string $golongan_iv
 * @property string $kondisi_kantong_i
 * @property string $kondisi_kantong_ii
 * @property string $kondisi_kantong_iii
 * @property string $kondisi_kantong_iv
 * @property string $no_kantong_i
 * @property string $no_kantong_ii
 * @property string $no_kantong_iii
 * @property string $no_kantong_iv
 * @property string $tgl_kadaluarsa_i
 * @property string $tgl_kadaluarsa_ii
 * @property string $tgl_kadaluarsa_iii
 * @property string $tgl_kadaluarsa_iv
 * @property string $nama_lengkap_i
 * @property string $nama_lengkap_ii
 * @property string $nama_lengkap_iii
 * @property string $tgl_lahir_i
 * @property string $tgl_lahir_ii
 * @property string $tgl_lahir_iii
 * @property string $no_rm_i
 * @property string $no_rm_ii
 * @property string $no_rm_iii
 * @property string $golongan_darah_i
 * @property string $golongan_darah_ii
 * @property string $golongan_darah_iii
 * @property string $mulai_tranfusi_jam
 * @property string $ttv_mulai
 * @property string $td_mulai
 * @property string $nd_mulai
 * @property string $rr_mulai
 * @property string $suhu_mulai
 * @property string $selesai_tranfusi_jam
 * @property string $ttv_selesasi
 * @property string $td_selesai
 * @property string $nd_selesai
 * @property string $rr_selesai
 * @property string $suhu_selesai
 * @property string $tanda_alergi
 * @property string $tanda_syok
 * @property string $user_create
 * @property string $user_update
 * @property string $tgl_create
 * @property string $tgl_update
 */
class ChecklistTranfusi extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'checklist_tranfusi';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('id_checklist_tranfusi', 'required'),
			array('id_checklist_tranfusi, id_registrasi, user_create, user_update', 'length', 'max'=>20),
			array('infomed_consent, golongan_darah, ttv_mulai, td_mulai, nd_mulai, rr_mulai, suhu_mulai, ttv_selesasi, td_selesai, nd_selesai, rr_selesai, suhu_selesai, tanda_alergi, tanda_syok', 'length', 'max'=>5),
			array('jumlah_darah, jenis_darah, nomor_kantong', 'length', 'max'=>10),
			array('jenis_darah_i, jenis_darah_ii, jenis_darah_iii, jenis_darah_iv, golongan_i, golongan_ii, golongan_iii, golongan_iv, kondisi_kantong_i, kondisi_kantong_ii, kondisi_kantong_iii, kondisi_kantong_iv, no_kantong_i, no_kantong_ii, no_kantong_iii, no_kantong_iv, tgl_kadaluarsa_i, tgl_kadaluarsa_ii, tgl_kadaluarsa_iii, tgl_kadaluarsa_iv, nama_lengkap_i, nama_lengkap_ii, nama_lengkap_iii, tgl_lahir_i, tgl_lahir_ii, tgl_lahir_iii, no_rm_i, no_rm_ii, no_rm_iii, golongan_darah_i, golongan_darah_ii, golongan_darah_iii', 'length', 'max'=>6),
			array('tgl_pemberian, mulai_jam_pemberian, sampai_jam_pemberian, tgl_kadaluarsa, mulai_tranfusi_jam, selesai_tranfusi_jam, tgl_create, tgl_update', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id_checklist_tranfusi, id_registrasi, infomed_consent, tgl_pemberian, jumlah_darah, jenis_darah, mulai_jam_pemberian, sampai_jam_pemberian, nomor_kantong, golongan_darah, tgl_kadaluarsa, jenis_darah_i, jenis_darah_ii, jenis_darah_iii, jenis_darah_iv, golongan_i, golongan_ii, golongan_iii, golongan_iv, kondisi_kantong_i, kondisi_kantong_ii, kondisi_kantong_iii, kondisi_kantong_iv, no_kantong_i, no_kantong_ii, no_kantong_iii, no_kantong_iv, tgl_kadaluarsa_i, tgl_kadaluarsa_ii, tgl_kadaluarsa_iii, tgl_kadaluarsa_iv, nama_lengkap_i, nama_lengkap_ii, nama_lengkap_iii, tgl_lahir_i, tgl_lahir_ii, tgl_lahir_iii, no_rm_i, no_rm_ii, no_rm_iii, golongan_darah_i, golongan_darah_ii, golongan_darah_iii, mulai_tranfusi_jam, ttv_mulai, td_mulai, nd_mulai, rr_mulai, suhu_mulai, selesai_tranfusi_jam, ttv_selesasi, td_selesai, nd_selesai, rr_selesai, suhu_selesai, tanda_alergi, tanda_syok, user_create, user_update, tgl_create, tgl_update', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id_checklist_tranfusi' => 'Id Checklist Tranfusi',
			'id_registrasi' => 'Id Registrasi',
			'infomed_consent' => 'Infomed Consent',
			'tgl_pemberian' => 'Tgl Pemberian',
			'jumlah_darah' => 'Jumlah Darah',
			'jenis_darah' => 'Jenis Darah',
			'mulai_jam_pemberian' => 'Mulai Jam Pemberian',
			'sampai_jam_pemberian' => 'Sampai Jam Pemberian',
			'nomor_kantong' => 'Nomor Kantong',
			'golongan_darah' => 'Golongan Darah',
			'tgl_kadaluarsa' => 'Tgl Kadaluarsa',
			'jenis_darah_i' => 'Jenis Darah I',
			'jenis_darah_ii' => 'Jenis Darah Ii',
			'jenis_darah_iii' => 'Jenis Darah Iii',
			'jenis_darah_iv' => 'Jenis Darah Iv',
			'golongan_i' => 'Golongan I',
			'golongan_ii' => 'Golongan Ii',
			'golongan_iii' => 'Golongan Iii',
			'golongan_iv' => 'Golongan Iv',
			'kondisi_kantong_i' => 'Kondisi Kantong I',
			'kondisi_kantong_ii' => 'Kondisi Kantong Ii',
			'kondisi_kantong_iii' => 'Kondisi Kantong Iii',
			'kondisi_kantong_iv' => 'Kondisi Kantong Iv',
			'no_kantong_i' => 'No Kantong I',
			'no_kantong_ii' => 'No Kantong Ii',
			'no_kantong_iii' => 'No Kantong Iii',
			'no_kantong_iv' => 'No Kantong Iv',
			'tgl_kadaluarsa_i' => 'Tgl Kadaluarsa I',
			'tgl_kadaluarsa_ii' => 'Tgl Kadaluarsa Ii',
			'tgl_kadaluarsa_iii' => 'Tgl Kadaluarsa Iii',
			'tgl_kadaluarsa_iv' => 'Tgl Kadaluarsa Iv',
			'nama_lengkap_i' => 'Nama Lengkap I',
			'nama_lengkap_ii' => 'Nama Lengkap Ii',
			'nama_lengkap_iii' => 'Nama Lengkap Iii',
			'tgl_lahir_i' => 'Tgl Lahir I',
			'tgl_lahir_ii' => 'Tgl Lahir Ii',
			'tgl_lahir_iii' => 'Tgl Lahir Iii',
			'no_rm_i' => 'No Rm I',
			'no_rm_ii' => 'No Rm Ii',
			'no_rm_iii' => 'No Rm Iii',
			'golongan_darah_i' => 'Golongan Darah I',
			'golongan_darah_ii' => 'Golongan Darah Ii',
			'golongan_darah_iii' => 'Golongan Darah Iii',
			'mulai_tranfusi_jam' => 'Mulai Tranfusi Jam',
			'ttv_mulai' => 'Ttv Mulai',
			'td_mulai' => 'Td Mulai',
			'nd_mulai' => 'Nd Mulai',
			'rr_mulai' => 'Rr Mulai',
			'suhu_mulai' => 'Suhu Mulai',
			'selesai_tranfusi_jam' => 'Selesai Tranfusi Jam',
			'ttv_selesasi' => 'Ttv Selesasi',
			'td_selesai' => 'Td Selesai',
			'nd_selesai' => 'Nd Selesai',
			'rr_selesai' => 'Rr Selesai',
			'suhu_selesai' => 'Suhu Selesai',
			'tanda_alergi' => 'Tanda Alergi',
			'tanda_syok' => 'Tanda Syok',
			'user_create' => 'User Create',
			'user_update' => 'User Update',
			'tgl_create' => 'Tgl Create',
			'tgl_update' => 'Tgl Update',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id_checklist_tranfusi',$this->id_checklist_tranfusi,true);
		$criteria->compare('id_registrasi',$this->id_registrasi,true);
		$criteria->compare('infomed_consent',$this->infomed_consent,true);
		$criteria->compare('tgl_pemberian',$this->tgl_pemberian,true);
		$criteria->compare('jumlah_darah',$this->jumlah_darah,true);
		$criteria->compare('jenis_darah',$this->jenis_darah,true);
		$criteria->compare('mulai_jam_pemberian',$this->mulai_jam_pemberian,true);
		$criteria->compare('sampai_jam_pemberian',$this->sampai_jam_pemberian,true);
		$criteria->compare('nomor_kantong',$this->nomor_kantong,true);
		$criteria->compare('golongan_darah',$this->golongan_darah,true);
		$criteria->compare('tgl_kadaluarsa',$this->tgl_kadaluarsa,true);
		$criteria->compare('jenis_darah_i',$this->jenis_darah_i,true);
		$criteria->compare('jenis_darah_ii',$this->jenis_darah_ii,true);
		$criteria->compare('jenis_darah_iii',$this->jenis_darah_iii,true);
		$criteria->compare('jenis_darah_iv',$this->jenis_darah_iv,true);
		$criteria->compare('golongan_i',$this->golongan_i,true);
		$criteria->compare('golongan_ii',$this->golongan_ii,true);
		$criteria->compare('golongan_iii',$this->golongan_iii,true);
		$criteria->compare('golongan_iv',$this->golongan_iv,true);
		$criteria->compare('kondisi_kantong_i',$this->kondisi_kantong_i,true);
		$criteria->compare('kondisi_kantong_ii',$this->kondisi_kantong_ii,true);
		$criteria->compare('kondisi_kantong_iii',$this->kondisi_kantong_iii,true);
		$criteria->compare('kondisi_kantong_iv',$this->kondisi_kantong_iv,true);
		$criteria->compare('no_kantong_i',$this->no_kantong_i,true);
		$criteria->compare('no_kantong_ii',$this->no_kantong_ii,true);
		$criteria->compare('no_kantong_iii',$this->no_kantong_iii,true);
		$criteria->compare('no_kantong_iv',$this->no_kantong_iv,true);
		$criteria->compare('tgl_kadaluarsa_i',$this->tgl_kadaluarsa_i,true);
		$criteria->compare('tgl_kadaluarsa_ii',$this->tgl_kadaluarsa_ii,true);
		$criteria->compare('tgl_kadaluarsa_iii',$this->tgl_kadaluarsa_iii,true);
		$criteria->compare('tgl_kadaluarsa_iv',$this->tgl_kadaluarsa_iv,true);
		$criteria->compare('nama_lengkap_i',$this->nama_lengkap_i,true);
		$criteria->compare('nama_lengkap_ii',$this->nama_lengkap_ii,true);
		$criteria->compare('nama_lengkap_iii',$this->nama_lengkap_iii,true);
		$criteria->compare('tgl_lahir_i',$this->tgl_lahir_i,true);
		$criteria->compare('tgl_lahir_ii',$this->tgl_lahir_ii,true);
		$criteria->compare('tgl_lahir_iii',$this->tgl_lahir_iii,true);
		$criteria->compare('no_rm_i',$this->no_rm_i,true);
		$criteria->compare('no_rm_ii',$this->no_rm_ii,true);
		$criteria->compare('no_rm_iii',$this->no_rm_iii,true);
		$criteria->compare('golongan_darah_i',$this->golongan_darah_i,true);
		$criteria->compare('golongan_darah_ii',$this->golongan_darah_ii,true);
		$criteria->compare('golongan_darah_iii',$this->golongan_darah_iii,true);
		$criteria->compare('mulai_tranfusi_jam',$this->mulai_tranfusi_jam,true);
		$criteria->compare('ttv_mulai',$this->ttv_mulai,true);
		$criteria->compare('td_mulai',$this->td_mulai,true);
		$criteria->compare('nd_mulai',$this->nd_mulai,true);
		$criteria->compare('rr_mulai',$this->rr_mulai,true);
		$criteria->compare('suhu_mulai',$this->suhu_mulai,true);
		$criteria->compare('selesai_tranfusi_jam',$this->selesai_tranfusi_jam,true);
		$criteria->compare('ttv_selesasi',$this->ttv_selesasi,true);
		$criteria->compare('td_selesai',$this->td_selesai,true);
		$criteria->compare('nd_selesai',$this->nd_selesai,true);
		$criteria->compare('rr_selesai',$this->rr_selesai,true);
		$criteria->compare('suhu_selesai',$this->suhu_selesai,true);
		$criteria->compare('tanda_alergi',$this->tanda_alergi,true);
		$criteria->compare('tanda_syok',$this->tanda_syok,true);
		$criteria->compare('user_create',$this->user_create,true);
		$criteria->compare('user_update',$this->user_update,true);
		$criteria->compare('tgl_create',$this->tgl_create,true);
		$criteria->compare('tgl_update',$this->tgl_update,true);

		return new CActiveDataProvider($this, array(
			'pagination'=>array(
				'pageSize'=> Yii::app()->user->getState('pageSize',Yii::app()->params['defaultPageSize']),
			),
			'criteria'=>$criteria,
		));
	}
	
	public function getAllowView()
	{
		$akses = UserAccess::model()->find(array("with"=>array("access"),'condition'=>"id_user='".Yii::app()->user->id."' and access.access_controller='ChecklistTranfusi' and access.access_action='view'"));
		if(!empty($akses)) return true; else return false;
	}
	
	public function getAllowUpdate()
	{
		$akses = UserAccess::model()->find(array("with"=>array("access"),'condition'=>"id_user='".Yii::app()->user->id."' and access.access_controller='ChecklistTranfusi' and access.access_action='update'"));
		if(!empty($akses)) return true; else return false;
	}
	
	public function getAllowDelete($id)
	{
		$akses = UserAccess::model()->find(array("with"=>array("access"),'condition'=>"id_user='".Yii::app()->user->id."' and access.access_controller='ChecklistTranfusi' and access.access_action='delete'"));
		$rel=$this->getEmptyRelation($id);
		if(!empty($akses) AND $rel==true){
			return true;
		}else{
			return false;
		}
	}
	
	
	public function getEmptyRelation($id)
	{
		$count=0;
		if($count>0){
			return false;
		}else{
			return true;
		}
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return ChecklistTranfusi the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
