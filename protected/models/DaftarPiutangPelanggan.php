<?php

/**
 * This is the model class for table "daftar_piutang_pelanggan".
 *
 * The followings are the available columns in table 'daftar_piutang_pelanggan':
 * @property string $id_daftar_piutang_pelanggan
 * @property string $id_piutang_pelanggan
 * @property string $deskripsi
 * @property integer $id_coa_pendapatan
 * @property integer $nilai_piutang_awal
 * @property integer $nilai_telah_dibayarkan
 * @property string $status_piutang
 * @property string $waktu_pembayaran
 *
 * The followings are the available model relations:
 * @property PiutangPelanggan $idPiutangPelanggan
 * @property GlCoa $idCoaPendapatan
 */
class DaftarPiutangPelanggan extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'daftar_piutang_pelanggan';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('deskripsi, id_coa_pendapatan, nilai_piutang_awal', 'required'),
			array('id_coa_pendapatan, nilai_piutang_awal, nilai_telah_dibayarkan', 'numerical', 'integerOnly'=>true),
			array('id_piutang_pelanggan', 'length', 'max'=>20),
			array('status_piutang', 'length', 'max'=>11),
			array('waktu_pembayaran', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id_daftar_piutang_pelanggan, id_piutang_pelanggan, deskripsi, id_coa_pendapatan, nilai_piutang_awal, nilai_telah_dibayarkan, status_piutang, waktu_pembayaran', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'idPiutangPelanggan' => array(self::BELONGS_TO, 'PiutangPelanggan', 'id_piutang_pelanggan'),
			'idCoaPendapatan' => array(self::BELONGS_TO, 'GlCoa', 'id_coa_pendapatan'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id_daftar_piutang_pelanggan' => 'Id Daftar Piutang Pelanggan',
			'id_piutang_pelanggan' => 'Id Piutang Pelanggan',
			'deskripsi' => 'Deskripsi',
			'id_coa_pendapatan' => 'Id Coa Pendapatan',
			'nilai_piutang_awal' => 'Nilai Piutang Awal',
			'nilai_telah_dibayarkan' => 'Nilai Telah Dibayarkan',
			'status_piutang' => 'Status Piutang',
			'waktu_pembayaran' => 'Waktu Pembayaran',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id_daftar_piutang_pelanggan',$this->id_daftar_piutang_pelanggan,true);
		$criteria->compare('id_piutang_pelanggan',$this->id_piutang_pelanggan,true);
		$criteria->compare('deskripsi',$this->deskripsi,true);
		$criteria->compare('id_coa_pendapatan',$this->id_coa_pendapatan);
		$criteria->compare('nilai_piutang_awal',$this->nilai_piutang_awal);
		$criteria->compare('nilai_telah_dibayarkan',$this->nilai_telah_dibayarkan);
		$criteria->compare('status_piutang',$this->status_piutang,true);
		$criteria->compare('waktu_pembayaran',$this->waktu_pembayaran,true);

		return new CActiveDataProvider($this, array(
			'pagination'=>array(
				'pageSize'=> Yii::app()->user->getState('pageSize',Yii::app()->params['defaultPageSize']),
			),
			'criteria'=>$criteria,
		));
	}
	
	public function getAllowView()
	{
		$akses = UserAccess::model()->find(array("with"=>array("access"),'condition'=>"id_user='".Yii::app()->user->id."' and access.access_controller='DaftarPiutangPelanggan' and access.access_action='view'"));
		if(!empty($akses)) return true; else return false;
	}
	
	public function getAllowUpdate()
	{
		$akses = UserAccess::model()->find(array("with"=>array("access"),'condition'=>"id_user='".Yii::app()->user->id."' and access.access_controller='DaftarPiutangPelanggan' and access.access_action='update'"));
		if(!empty($akses)) return true; else return false;
	}
	
	public function getAllowDelete($id)
	{
		$akses = UserAccess::model()->find(array("with"=>array("access"),'condition'=>"id_user='".Yii::app()->user->id."' and access.access_controller='DaftarPiutangPelanggan' and access.access_action='delete'"));
		$rel=$this->getEmptyRelation($id);
		if(!empty($akses) AND $rel==true){
			return true;
		}else{
			return false;
		}
	}
	
	
	public function getEmptyRelation($id)
	{
		$count=0;
						if($count>0){
			return false;
		}else{
			return true;
		}
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return DaftarPiutangPelanggan the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
