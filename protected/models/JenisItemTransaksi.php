<?php

/**
 * This is the model class for table "jenis_item_transaksi".
 *
 * The followings are the available columns in table 'jenis_item_transaksi':
 * @property string $id_jenis_item_transaksi
 * @property string $kode_jenis_transaksi
 * @property string $nama_jenis_transaksi
 * @property string $metode_stok_asal
 * @property string $metode_stok_tujuan
 *
 * The followings are the available model relations:
 * @property ItemTransaksi[] $itemTransaksis
 */
class JenisItemTransaksi extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'jenis_item_transaksi';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('kode_jenis_transaksi, nama_jenis_transaksi, metode_stok_asal, metode_stok_tujuan', 'required'),
			array('kode_jenis_transaksi, metode_stok_asal, metode_stok_tujuan', 'length', 'max'=>45),
			array('nama_jenis_transaksi', 'length', 'max'=>100),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id_jenis_item_transaksi, kode_jenis_transaksi, nama_jenis_transaksi, metode_stok_asal, metode_stok_tujuan', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'itemTransaksis' => array(self::HAS_MANY, 'ItemTransaksi', 'id_jenis_item_transaksi'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id_jenis_item_transaksi' => 'Id Jenis Item Transaksi',
			'kode_jenis_transaksi' => 'Kode Jenis Transaksi',
			'nama_jenis_transaksi' => 'Nama Jenis Transaksi',
			'metode_stok_asal' => 'Metode Stok Asal',
			'metode_stok_tujuan' => 'Metode Stok Tujuan',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id_jenis_item_transaksi',$this->id_jenis_item_transaksi,true);
		$criteria->compare('kode_jenis_transaksi',$this->kode_jenis_transaksi,true);
		$criteria->compare('nama_jenis_transaksi',$this->nama_jenis_transaksi,true);
		$criteria->compare('metode_stok_asal',$this->metode_stok_asal,true);
		$criteria->compare('metode_stok_tujuan',$this->metode_stok_tujuan,true);

		return new CActiveDataProvider($this, array(
			'pagination'=>array(
				'pageSize'=> Yii::app()->user->getState('pageSize',Yii::app()->params['defaultPageSize']),
			),
			'criteria'=>$criteria,
		));
	}
	
	public function getAllowView()
	{
		$akses = UserAccess::model()->find(array("with"=>array("access"),'condition'=>"id_user='".Yii::app()->user->id."' and access.access_controller='JenisItemTransaksi' and access.access_action='view'"));
		if(!empty($akses)) return true; else return false;
	}
	
	public function getAllowUpdate()
	{
		$akses = UserAccess::model()->find(array("with"=>array("access"),'condition'=>"id_user='".Yii::app()->user->id."' and access.access_controller='JenisItemTransaksi' and access.access_action='update'"));
		if(!empty($akses)) return true; else return false;
	}
	
	public function getAllowDelete($id)
	{
		$akses = UserAccess::model()->find(array("with"=>array("access"),'condition'=>"id_user='".Yii::app()->user->id."' and access.access_controller='JenisItemTransaksi' and access.access_action='delete'"));
		$rel=$this->getEmptyRelation($id);
		if(!empty($akses) AND $rel==true){
			return true;
		}else{
			return false;
		}
	}
	
	
	public function getEmptyRelation($id)
	{
		$count=0;
		$count+=ItemTransaksi::model()->count(array('condition'=>"id_jenis_item_transaksi='$id'"));
		if($count>0){
			return false;
		}else{
			return true;
		}
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return JenisItemTransaksi the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
