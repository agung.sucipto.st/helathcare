<?php

/**
 * This is the model class for table "ekspedisi_rekam_medis".
 *
 * The followings are the available columns in table 'ekspedisi_rekam_medis':
 * @property string $id_ekspedisi_rekam_medis
 * @property string $id_pasien
 * @property string $id_layanan
 * @property string $jenis_distribusi
 * @property string $waktu_distribusi
 * @property string $status
 * @property string $id_pegawai_penerima
 * @property string $user_create
 * @property string $time_create
 *
 * The followings are the available model relations:
 * @property Layanan $idLayanan
 * @property Pasien $idPasien
 * @property Pegawai $idPegawaiPenerima
 * @property User $userCreate
 */
class EkspedisiRekamMedis extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'ekspedisi_rekam_medis';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('id_pasien, id_layanan, jenis_distribusi, waktu_distribusi, status, id_pegawai_penerima, user_create, time_create', 'required'),
			array('id_pasien, id_layanan, id_pegawai_penerima, user_create', 'length', 'max'=>20),
			array('jenis_distribusi', 'length', 'max'=>60),
			array('status', 'length', 'max'=>45),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id_ekspedisi_rekam_medis, id_pasien, id_layanan, jenis_distribusi, waktu_distribusi, status, id_pegawai_penerima, user_create, time_create', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'idLayanan' => array(self::BELONGS_TO, 'Layanan', 'id_layanan'),
			'idPasien' => array(self::BELONGS_TO, 'Pasien', 'id_pasien'),
			'idPegawaiPenerima' => array(self::BELONGS_TO, 'Pegawai', 'id_pegawai_penerima'),
			'userCreate' => array(self::BELONGS_TO, 'User', 'user_create'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id_ekspedisi_rekam_medis' => 'Id Ekspedisi Rekam Medis',
			'id_pasien' => 'Id Pasien',
			'id_layanan' => 'Id Layanan',
			'jenis_distribusi' => 'Jenis Distribusi',
			'waktu_distribusi' => 'Waktu Distribusi',
			'status' => 'Status',
			'id_pegawai_penerima' => 'Id Pegawai Penerima',
			'user_create' => 'User Create',
			'time_create' => 'Time Create',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id_ekspedisi_rekam_medis',$this->id_ekspedisi_rekam_medis,true);
		$criteria->compare('id_pasien',$this->id_pasien,true);
		$criteria->compare('id_layanan',$this->id_layanan,true);
		$criteria->compare('jenis_distribusi',$this->jenis_distribusi,true);
		$criteria->compare('waktu_distribusi',$this->waktu_distribusi,true);
		$criteria->compare('status',$this->status,true);
		$criteria->compare('id_pegawai_penerima',$this->id_pegawai_penerima,true);
		$criteria->compare('user_create',$this->user_create,true);
		$criteria->compare('time_create',$this->time_create,true);

		return new CActiveDataProvider($this, array(
			'pagination'=>array(
				'pageSize'=> Yii::app()->user->getState('pageSize',Yii::app()->params['defaultPageSize']),
			),
			'criteria'=>$criteria,
		));
	}
	
	public function getAllowView()
	{
		$akses = UserAccess::model()->find(array("with"=>array("access"),'condition'=>"id_user='".Yii::app()->user->id."' and access.access_controller='EkspedisiRekamMedis' and access.access_action='view'"));
		if(!empty($akses)) return true; else return false;
	}
	
	public function getAllowUpdate()
	{
		$akses = UserAccess::model()->find(array("with"=>array("access"),'condition'=>"id_user='".Yii::app()->user->id."' and access.access_controller='EkspedisiRekamMedis' and access.access_action='update'"));
		if(!empty($akses)) return true; else return false;
	}
	
	public function getAllowDelete($id)
	{
		$akses = UserAccess::model()->find(array("with"=>array("access"),'condition'=>"id_user='".Yii::app()->user->id."' and access.access_controller='EkspedisiRekamMedis' and access.access_action='delete'"));
		$rel=$this->getEmptyRelation($id);
		if(!empty($akses) AND $rel==true){
			return true;
		}else{
			return false;
		}
	}
	
	
	public function getEmptyRelation($id)
	{
		$count=0;
										if($count>0){
			return false;
		}else{
			return true;
		}
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return EkspedisiRekamMedis the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
