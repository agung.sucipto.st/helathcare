<?php

/**
 * This is the model class for table "paket_pasien".
 *
 * The followings are the available columns in table 'paket_pasien':
 * @property string $id_paket_pasien
 * @property string $id_registrasi
 * @property string $id_paket
 * @property integer $jumlah
 * @property integer $user_create
 * @property string $time_create
 * @property string $id_daftar_tagihan
 *
 * The followings are the available model relations:
 * @property DaftarTagihan[] $daftarTagihans
 * @property Registrasi $idRegistrasi
 * @property Paket $idPaket
 * @property DaftarTagihan $idDaftarTagihan
 * @property PaketPasienList[] $paketPasienLists
 */
class PaketPasien extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'paket_pasien';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('id_registrasi, id_paket, jumlah, user_create, time_create', 'required'),
			array('jumlah, user_create', 'numerical', 'integerOnly'=>true),
			array('id_registrasi, id_paket, id_daftar_tagihan', 'length', 'max'=>20),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id_paket_pasien, id_registrasi, id_paket, jumlah, user_create, time_create, id_daftar_tagihan', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'daftarTagihans' => array(self::HAS_MANY, 'DaftarTagihan', 'id_paket_pasien'),
			'idRegistrasi' => array(self::BELONGS_TO, 'Registrasi', 'id_registrasi'),
			'idPaket' => array(self::BELONGS_TO, 'Paket', 'id_paket'),
			'idDaftarTagihan' => array(self::BELONGS_TO, 'DaftarTagihan', 'id_daftar_tagihan'),
			'paketPasienLists' => array(self::HAS_MANY, 'PaketPasienList', 'id_paket_pasien'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id_paket_pasien' => 'Id Paket Pasien',
			'id_registrasi' => 'Id Registrasi',
			'id_paket' => 'Id Paket',
			'jumlah' => 'Jumlah',
			'user_create' => 'User Create',
			'time_create' => 'Time Create',
			'id_daftar_tagihan' => 'Id Daftar Tagihan',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id_paket_pasien',$this->id_paket_pasien,true);
		$criteria->compare('id_registrasi',$this->id_registrasi,true);
		$criteria->compare('id_paket',$this->id_paket,true);
		$criteria->compare('jumlah',$this->jumlah);
		$criteria->compare('user_create',$this->user_create);
		$criteria->compare('time_create',$this->time_create,true);
		$criteria->compare('id_daftar_tagihan',$this->id_daftar_tagihan,true);

		return new CActiveDataProvider($this, array(
			'pagination'=>array(
				'pageSize'=> Yii::app()->user->getState('pageSize',Yii::app()->params['defaultPageSize']),
			),
			'criteria'=>$criteria,
		));
	}
	
	public function getAllowView()
	{
		$akses = UserAccess::model()->find(array("with"=>array("access"),'condition'=>"id_user='".Yii::app()->user->id."' and access.access_controller='PaketPasien' and access.access_action='view'"));
		if(!empty($akses)) return true; else return false;
	}
	
	public function getAllowUpdate()
	{
		$akses = UserAccess::model()->find(array("with"=>array("access"),'condition'=>"id_user='".Yii::app()->user->id."' and access.access_controller='PaketPasien' and access.access_action='update'"));
		if(!empty($akses)) return true; else return false;
	}
	
	public function getAllowDelete($id)
	{
		$akses = UserAccess::model()->find(array("with"=>array("access"),'condition'=>"id_user='".Yii::app()->user->id."' and access.access_controller='PaketPasien' and access.access_action='delete'"));
		$rel=$this->getEmptyRelation($id);
		if(!empty($akses) AND $rel==true){
			return true;
		}else{
			return false;
		}
	}
	
	
	public function getEmptyRelation($id)
	{
		$count=0;
		$count+=DaftarTagihan::model()->count(array('condition'=>"id_paket_pasien='$id'"));
								$count+=PaketPasienList::model()->count(array('condition'=>"id_paket_pasien='$id'"));
		if($count>0){
			return false;
		}else{
			return true;
		}
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return PaketPasien the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
