<?php

/**
 * This is the model class for table "tindakan".
 *
 * The followings are the available columns in table 'tindakan':
 * @property string $id_tindakan
 * @property string $id_kelompok_tindakan
 * @property string $nama_tindakan
 * @property string $status
 *
 * The followings are the available model relations:
 * @property TarifTindakan[] $tarifTindakans
 * @property KelompokTindakan $idKelompokTindakan
 * @property TindakanMedis[] $tindakanMedises
 * @property TindakanPasien[] $tindakanPasiens
 */
class Tindakan extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'tindakan';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('id_kelompok_tindakan, nama_tindakan, status', 'required'),
			array('id_kelompok_tindakan', 'length', 'max'=>20),
			array('nama_tindakan', 'length', 'max'=>100),
			array('status', 'length', 'max'=>11),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id_tindakan, id_kelompok_tindakan, nama_tindakan, status', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'tarifTindakans' => array(self::HAS_MANY, 'TarifTindakan', 'id_tindakan'),
			'idKelompokTindakan' => array(self::BELONGS_TO, 'KelompokTindakan', 'id_kelompok_tindakan'),
			'tindakanMedises' => array(self::HAS_MANY, 'TindakanMedis', 'id_tindakan'),
			'tindakanPasiens' => array(self::HAS_MANY, 'TindakanPasien', 'id_tindakan'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id_tindakan' => 'Id Tindakan',
			'id_kelompok_tindakan' => 'Id Kelompok Tindakan',
			'nama_tindakan' => 'Nama Tindakan',
			'status' => 'Status',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id_tindakan',$this->id_tindakan,true);
		$criteria->compare('id_kelompok_tindakan',$this->id_kelompok_tindakan,true);
		$criteria->compare('nama_tindakan',$this->nama_tindakan,true);
		$criteria->compare('status',$this->status,true);

		return new CActiveDataProvider($this, array(
			'pagination'=>array(
				'pageSize'=> Yii::app()->user->getState('pageSize',Yii::app()->params['defaultPageSize']),
			),
			'criteria'=>$criteria,
		));
	}
	
	public function getAllowView()
	{
		$akses = UserAccess::model()->find(array("with"=>array("access"),'condition'=>"id_user='".Yii::app()->user->id."' and access.access_controller='Tindakan' and access.access_action='view'"));
		if(!empty($akses)) return true; else return false;
	}
	
	public function getAllowUpdate()
	{
		$akses = UserAccess::model()->find(array("with"=>array("access"),'condition'=>"id_user='".Yii::app()->user->id."' and access.access_controller='Tindakan' and access.access_action='update'"));
		if(!empty($akses)) return true; else return false;
	}
	
	public function getAllowDelete($id)
	{
		$akses = UserAccess::model()->find(array("with"=>array("access"),'condition'=>"id_user='".Yii::app()->user->id."' and access.access_controller='Tindakan' and access.access_action='delete'"));
		$rel=$this->getEmptyRelation($id);
		if(!empty($akses) AND $rel==true){
			return true;
		}else{
			return false;
		}
	}
	
	
	public function getEmptyRelation($id)
	{
		$count=0;
		$count+=TarifTindakan::model()->count(array('condition'=>"id_tindakan='$id'"));
				$count+=TindakanMedis::model()->count(array('condition'=>"id_tindakan='$id'"));
		$count+=TindakanPasien::model()->count(array('condition'=>"id_tindakan='$id'"));
		if($count>0){
			return false;
		}else{
			return true;
		}
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Tindakan the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
