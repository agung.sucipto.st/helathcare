<?php

/**
 * This is the model class for table "gl_transaksi_rinci".
 *
 * The followings are the available columns in table 'gl_transaksi_rinci':
 * @property string $id_transaksi_rinci
 * @property string $id_gl_transaksi
 * @property integer $gl_coa_id
 * @property integer $gl_coa_id_coa1
 * @property integer $gl_coa_id_coa2
 * @property integer $gl_coa_id_coa3
 * @property integer $gl_coa_id_coa4
 * @property integer $gl_coa_id_coa5
 * @property integer $gl_id_unit
 * @property string $debet
 * @property string $kredit
 * @property string $deskripsi
 *
 * The followings are the available model relations:
 * @property GlCoa $glCoaIdCoa1
 * @property GlCoa $glCoaIdCoa2
 * @property GlCoa $glCoaIdCoa3
 * @property GlCoa $glCoaIdCoa4
 * @property GlCoa $glCoaIdCoa5
 * @property GlCoa $glCoa
 * @property GlUnit $glIdUnit
 * @property GlTransaksi $idGlTransaksi
 */
class GlTransaksiRinci extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'gl_transaksi_rinci';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('id_gl_transaksi, gl_coa_id', 'required'),
			array('gl_coa_id, gl_coa_id_coa1, gl_coa_id_coa2, gl_coa_id_coa3, gl_coa_id_coa4, gl_coa_id_coa5, gl_id_unit', 'numerical', 'integerOnly'=>true),
			array('id_gl_transaksi', 'length', 'max'=>20),
			array('debet, kredit', 'length', 'max'=>10),
			array('deskripsi', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id_transaksi_rinci, id_gl_transaksi, gl_coa_id, gl_coa_id_coa1, gl_coa_id_coa2, gl_coa_id_coa3, gl_coa_id_coa4, gl_coa_id_coa5, gl_id_unit, debet, kredit, deskripsi', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'glCoaIdCoa1' => array(self::BELONGS_TO, 'GlCoa', 'gl_coa_id_coa1'),
			'glCoaIdCoa2' => array(self::BELONGS_TO, 'GlCoa', 'gl_coa_id_coa2'),
			'glCoaIdCoa3' => array(self::BELONGS_TO, 'GlCoa', 'gl_coa_id_coa3'),
			'glCoaIdCoa4' => array(self::BELONGS_TO, 'GlCoa', 'gl_coa_id_coa4'),
			'glCoaIdCoa5' => array(self::BELONGS_TO, 'GlCoa', 'gl_coa_id_coa5'),
			'glCoa' => array(self::BELONGS_TO, 'GlCoa', 'gl_coa_id'),
			'glIdUnit' => array(self::BELONGS_TO, 'GlUnit', 'gl_id_unit'),
			'idGlTransaksi' => array(self::BELONGS_TO, 'GlTransaksi', 'id_gl_transaksi'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id_transaksi_rinci' => 'Id Transaksi Rinci',
			'id_gl_transaksi' => 'Id Gl Transaksi',
			'gl_coa_id' => 'Gl Coa',
			'gl_coa_id_coa1' => 'Gl Coa Id Coa1',
			'gl_coa_id_coa2' => 'Gl Coa Id Coa2',
			'gl_coa_id_coa3' => 'Gl Coa Id Coa3',
			'gl_coa_id_coa4' => 'Gl Coa Id Coa4',
			'gl_coa_id_coa5' => 'Gl Coa Id Coa5',
			'gl_id_unit' => 'Gl Id Unit',
			'debet' => 'Debet',
			'kredit' => 'Kredit',
			'deskripsi' => 'Deskripsi',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id_transaksi_rinci',$this->id_transaksi_rinci,true);
		$criteria->compare('id_gl_transaksi',$this->id_gl_transaksi,true);
		$criteria->compare('gl_coa_id',$this->gl_coa_id);
		$criteria->compare('gl_coa_id_coa1',$this->gl_coa_id_coa1);
		$criteria->compare('gl_coa_id_coa2',$this->gl_coa_id_coa2);
		$criteria->compare('gl_coa_id_coa3',$this->gl_coa_id_coa3);
		$criteria->compare('gl_coa_id_coa4',$this->gl_coa_id_coa4);
		$criteria->compare('gl_coa_id_coa5',$this->gl_coa_id_coa5);
		$criteria->compare('gl_id_unit',$this->gl_id_unit);
		$criteria->compare('debet',$this->debet,true);
		$criteria->compare('kredit',$this->kredit,true);
		$criteria->compare('deskripsi',$this->deskripsi,true);

		return new CActiveDataProvider($this, array(
			'pagination'=>array(
				'pageSize'=> Yii::app()->user->getState('pageSize',Yii::app()->params['defaultPageSize']),
			),
			'criteria'=>$criteria,
		));
	}
	
	public function getAllowView()
	{
		$akses = UserAccess::model()->find(array("with"=>array("access"),'condition'=>"id_user='".Yii::app()->user->id."' and access.access_controller='GlTransaksiRinci' and access.access_action='view'"));
		if(!empty($akses)) return true; else return false;
	}
	
	public function getAllowUpdate()
	{
		$akses = UserAccess::model()->find(array("with"=>array("access"),'condition'=>"id_user='".Yii::app()->user->id."' and access.access_controller='GlTransaksiRinci' and access.access_action='update'"));
		if(!empty($akses)) return true; else return false;
	}
	
	public function getAllowDelete($id)
	{
		$akses = UserAccess::model()->find(array("with"=>array("access"),'condition'=>"id_user='".Yii::app()->user->id."' and access.access_controller='GlTransaksiRinci' and access.access_action='delete'"));
		$rel=$this->getEmptyRelation($id);
		if(!empty($akses) AND $rel==true){
			return true;
		}else{
			return false;
		}
	}
	
	
	public function getEmptyRelation($id)
	{
		$count=0;
																		if($count>0){
			return false;
		}else{
			return true;
		}
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return GlTransaksiRinci the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
