<?php

/**
 * This is the model class for table "laboratorium_pasien_medis".
 *
 * The followings are the available columns in table 'laboratorium_pasien_medis':
 * @property string $id_laboratorium_pasien_medis
 * @property string $id_laboratorium_pasien
 * @property string $id_pegawai
 * @property string $id_laboratorium
 * @property string $id_laboratorium_medis
 * @property string $id_tarif_laboratorium_medis
 * @property string $jasa_medis
 * @property string $jasa_medis_org
 * @property string $gratis_biaya_jasa
 *
 * The followings are the available model relations:
 * @property Laboratorium $idLaboratorium
 * @property LaboratoriumMedis $idLaboratoriumMedis
 * @property LaboratoriumPasien $idLaboratoriumPasien
 * @property Pegawai $idPegawai
 * @property TarifLaboratoriumMedis $idTarifLaboratoriumMedis
 */
class LaboratoriumPasienMedis extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'laboratorium_pasien_medis';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('id_laboratorium_pasien, id_pegawai, id_laboratorium, id_laboratorium_medis, id_tarif_laboratorium_medis', 'required'),
			array('id_laboratorium_pasien, id_pegawai, id_laboratorium, id_laboratorium_medis, id_tarif_laboratorium_medis', 'length', 'max'=>20),
			array('jasa_medis, jasa_medis_org', 'length', 'max'=>30),
			array('gratis_biaya_jasa', 'length', 'max'=>5),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id_laboratorium_pasien_medis, id_laboratorium_pasien, id_pegawai, id_laboratorium, id_laboratorium_medis, id_tarif_laboratorium_medis, jasa_medis, jasa_medis_org, gratis_biaya_jasa', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'idLaboratorium' => array(self::BELONGS_TO, 'Laboratorium', 'id_laboratorium'),
			'idLaboratoriumMedis' => array(self::BELONGS_TO, 'LaboratoriumMedis', 'id_laboratorium_medis'),
			'idLaboratoriumPasien' => array(self::BELONGS_TO, 'LaboratoriumPasien', 'id_laboratorium_pasien'),
			'idPegawai' => array(self::BELONGS_TO, 'Pegawai', 'id_pegawai'),
			'idTarifLaboratoriumMedis' => array(self::BELONGS_TO, 'TarifLaboratoriumMedis', 'id_tarif_laboratorium_medis'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id_laboratorium_pasien_medis' => 'Id Laboratorium Pasien Medis',
			'id_laboratorium_pasien' => 'Id Laboratorium Pasien',
			'id_pegawai' => 'Id Pegawai',
			'id_laboratorium' => 'Id Laboratorium',
			'id_laboratorium_medis' => 'Id Laboratorium Medis',
			'id_tarif_laboratorium_medis' => 'Id Tarif Laboratorium Medis',
			'jasa_medis' => 'Jasa Medis',
			'jasa_medis_org' => 'Jasa Medis Org',
			'gratis_biaya_jasa' => 'Gratis Biaya Jasa',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id_laboratorium_pasien_medis',$this->id_laboratorium_pasien_medis,true);
		$criteria->compare('id_laboratorium_pasien',$this->id_laboratorium_pasien,true);
		$criteria->compare('id_pegawai',$this->id_pegawai,true);
		$criteria->compare('id_laboratorium',$this->id_laboratorium,true);
		$criteria->compare('id_laboratorium_medis',$this->id_laboratorium_medis,true);
		$criteria->compare('id_tarif_laboratorium_medis',$this->id_tarif_laboratorium_medis,true);
		$criteria->compare('jasa_medis',$this->jasa_medis,true);
		$criteria->compare('jasa_medis_org',$this->jasa_medis_org,true);
		$criteria->compare('gratis_biaya_jasa',$this->gratis_biaya_jasa,true);

		return new CActiveDataProvider($this, array(
			'pagination'=>array(
				'pageSize'=> Yii::app()->user->getState('pageSize',Yii::app()->params['defaultPageSize']),
			),
			'criteria'=>$criteria,
		));
	}
	
	public function getAllowView()
	{
		$akses = UserAccess::model()->find(array("with"=>array("access"),'condition'=>"id_user='".Yii::app()->user->id."' and access.access_controller='LaboratoriumPasienMedis' and access.access_action='view'"));
		if(!empty($akses)) return true; else return false;
	}
	
	public function getAllowUpdate()
	{
		$akses = UserAccess::model()->find(array("with"=>array("access"),'condition'=>"id_user='".Yii::app()->user->id."' and access.access_controller='LaboratoriumPasienMedis' and access.access_action='update'"));
		if(!empty($akses)) return true; else return false;
	}
	
	public function getAllowDelete($id)
	{
		$akses = UserAccess::model()->find(array("with"=>array("access"),'condition'=>"id_user='".Yii::app()->user->id."' and access.access_controller='LaboratoriumPasienMedis' and access.access_action='delete'"));
		$rel=$this->getEmptyRelation($id);
		if(!empty($akses) AND $rel==true){
			return true;
		}else{
			return false;
		}
	}
	
	
	public function getEmptyRelation($id)
	{
		$count=0;
												if($count>0){
			return false;
		}else{
			return true;
		}
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return LaboratoriumPasienMedis the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
