<?php

/**
 * This is the model class for table "pembayaran_piutang_perusahaan".
 *
 * The followings are the available columns in table 'pembayaran_piutang_perusahaan':
 * @property string $id_pembayaran_piutang_perusahaan
 * @property string $id_piutang_perusahaan
 * @property string $id_penjamin
 * @property string $no_pembayaran
 * @property string $waktu_pembayaran
 * @property string $jumlah_pembayaran
 * @property string $cara_pembayaran
 * @property string $tujuan_pembayaran
 * @property string $user_create
 * @property string $user_update
 * @property string $user_final
 * @property string $time_create
 * @property string $time_update
 * @property string $time_final
 *
 * The followings are the available model relations:
 * @property Penjamin $idPenjamin
 * @property PiutangPerusahaan $idPiutangPerusahaan
 * @property User $userCreate
 * @property User $userUpdate
 * @property User $userFinal
 */
class PembayaranPiutangPerusahaan extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'pembayaran_piutang_perusahaan';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('id_piutang_perusahaan, id_penjamin, user_update, user_final, time_create', 'required'),
			array('id_piutang_perusahaan, id_penjamin, user_create, user_update, user_final', 'length', 'max'=>20),
			array('no_pembayaran, jumlah_pembayaran, cara_pembayaran, tujuan_pembayaran', 'length', 'max'=>45),
			array('waktu_pembayaran, time_update, time_final', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id_pembayaran_piutang_perusahaan, id_piutang_perusahaan, id_penjamin, no_pembayaran, waktu_pembayaran, jumlah_pembayaran, cara_pembayaran, tujuan_pembayaran, user_create, user_update, user_final, time_create, time_update, time_final', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'idPenjamin' => array(self::BELONGS_TO, 'Penjamin', 'id_penjamin'),
			'idPiutangPerusahaan' => array(self::BELONGS_TO, 'PiutangPerusahaan', 'id_piutang_perusahaan'),
			'userCreate' => array(self::BELONGS_TO, 'User', 'user_create'),
			'userUpdate' => array(self::BELONGS_TO, 'User', 'user_update'),
			'userFinal' => array(self::BELONGS_TO, 'User', 'user_final'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id_pembayaran_piutang_perusahaan' => 'Id Pembayaran Piutang Perusahaan',
			'id_piutang_perusahaan' => 'Id Piutang Perusahaan',
			'id_penjamin' => 'Id Penjamin',
			'no_pembayaran' => 'No Pembayaran',
			'waktu_pembayaran' => 'Waktu Pembayaran',
			'jumlah_pembayaran' => 'Jumlah Pembayaran',
			'cara_pembayaran' => 'Cara Pembayaran',
			'tujuan_pembayaran' => 'Tujuan Pembayaran',
			'user_create' => 'User Create',
			'user_update' => 'User Update',
			'user_final' => 'User Final',
			'time_create' => 'Time Create',
			'time_update' => 'Time Update',
			'time_final' => 'Time Final',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id_pembayaran_piutang_perusahaan',$this->id_pembayaran_piutang_perusahaan,true);
		$criteria->compare('id_piutang_perusahaan',$this->id_piutang_perusahaan,true);
		$criteria->compare('id_penjamin',$this->id_penjamin,true);
		$criteria->compare('no_pembayaran',$this->no_pembayaran,true);
		$criteria->compare('waktu_pembayaran',$this->waktu_pembayaran,true);
		$criteria->compare('jumlah_pembayaran',$this->jumlah_pembayaran,true);
		$criteria->compare('cara_pembayaran',$this->cara_pembayaran,true);
		$criteria->compare('tujuan_pembayaran',$this->tujuan_pembayaran,true);
		$criteria->compare('user_create',$this->user_create,true);
		$criteria->compare('user_update',$this->user_update,true);
		$criteria->compare('user_final',$this->user_final,true);
		$criteria->compare('time_create',$this->time_create,true);
		$criteria->compare('time_update',$this->time_update,true);
		$criteria->compare('time_final',$this->time_final,true);

		return new CActiveDataProvider($this, array(
			'pagination'=>array(
				'pageSize'=> Yii::app()->user->getState('pageSize',Yii::app()->params['defaultPageSize']),
			),
			'criteria'=>$criteria,
		));
	}
	
	public function getAllowView()
	{
		$akses = UserAccess::model()->find(array("with"=>array("access"),'condition'=>"id_user='".Yii::app()->user->id."' and access.access_controller='PembayaranPiutangPerusahaan' and access.access_action='view'"));
		if(!empty($akses)) return true; else return false;
	}
	
	public function getAllowUpdate()
	{
		$akses = UserAccess::model()->find(array("with"=>array("access"),'condition'=>"id_user='".Yii::app()->user->id."' and access.access_controller='PembayaranPiutangPerusahaan' and access.access_action='update'"));
		if(!empty($akses)) return true; else return false;
	}
	
	public function getAllowDelete($id)
	{
		$akses = UserAccess::model()->find(array("with"=>array("access"),'condition'=>"id_user='".Yii::app()->user->id."' and access.access_controller='PembayaranPiutangPerusahaan' and access.access_action='delete'"));
		$rel=$this->getEmptyRelation($id);
		if(!empty($akses) AND $rel==true){
			return true;
		}else{
			return false;
		}
	}
	
	
	public function getEmptyRelation($id)
	{
		$count=0;
												if($count>0){
			return false;
		}else{
			return true;
		}
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return PembayaranPiutangPerusahaan the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
