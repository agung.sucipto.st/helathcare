<?php
class ItemController extends Controller
{
	/**
	* @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	* using two-column layout. See 'protected/views/layouts/column2.php'.
	*/
	public $layout='//layouts/admin/main';
	public $groupMenu=13;

	/**
	* @return array action filters
	*/
	public function filters()
	{
		return array(
		'accessControl', // perform access control for CRUD operations
		);
	}

	/**
	* Specifies the access control rules.
	* This method is used by the 'accessControl' filter.
	* @return array access control rules
	*/
	public function accessRules()
	{
		return array(
		array('allow',  // allow all users to perform 'index' and 'view' actions
		'actions'=>array('index','view','price'),
		'expression'=>'$user->getAuth()',
		),
		array('allow', // allow admin user to perform 'admin' and 'delete' actions
		'actions'=>array('create','update','admin','delete'),
		'expression'=>'$user->getAuth()',
		),
		array('allow',  // allow all users to perform 'index' and 'view' actions
		'actions'=>array('getGroup','getItem','import','getItemByDepo'),
		'users'=>array('*'),
		),

		array('deny',  // deny all users
		'users'=>array('*'),
		),
		);
	}
	
	public function actionImport()
	{
		ini_set("memory_limit","8056M");
		set_time_limit(100000);
		if(isset($_POST['Item']))
		{
		$model=new Item;
		$fileUpload=CUploadedFile::getInstance($model,'import');
		$nama=date('YmdHis').'_'.$fileUpload;
		$path=Yii::app()->basePath . '/../temp_excel/'.$nama.'';
		$fileUpload->saveAs($path);
			
		Yii::import('ext.phpexcelreader.JPhpExcelReader');
		$data=new JPhpExcelReader($path);
		$baris=$data->sheets[0]['numRows'];
		$kolom=$data->sheets[0]['numCols'];
		$sukses=0;
		$gagal=0;
		
		for($i=2;$i<=$baris; $i++){
			$harga=$data->sheets[0]['cells'][$i][7];
			$ppn=$data->sheets[0]['cells'][$i][8];
			$margin=$data->sheets[0]['cells'][$i][10];
			$model=new Item;
			$model->id_item_kategori=$data->sheets[0]['cells'][$i][1];
			$model->id_item_grup=$data->sheets[0]['cells'][$i][3];
			$model->kode_item=$data->sheets[0]['cells'][$i][2];
			$model->nama_item=strtoupper($data->sheets[0]['cells'][$i][4]);
			$model->harga_dasar_satuan_kecil=$harga;
			$model->status=$data->sheets[0]['cells'][$i][5];
			$model->zat_aktif=$data->sheets[0]['cells'][$i][6];
			$model->wac=$data->sheets[0]['cells'][$i][7];
			$model->user_create=Yii::app()->user->id;
			$model->time_create=date("Y-m-d H:i:s");
			$cek = Item::model()->find(array("condition"=>"kode_item='".$data->sheets[0]['cells'][$i][2]."'"));
			if(count($cek)==0){
				if($model->save()){
					$w=Gudang::model()->findAll(array("condition"=>"status='Aktif'"));
					foreach($w as $x){
						$y=new ItemGudang;
						$y->id_item=$model->id_item;
						$y->id_gudang=$x->id_gudang;
						$y->stok=0;
						$y->min_stok=0;
						$y->max_stok=0;
						$y->save();
					}
					$uid=Satuan::model()->find(array("condition"=>"nama_satuan like '%".trim($data->sheets[0]['cells'][$i][13])."%' OR alias_satuan like '%".trim($data->sheets[0]['cells'][$i][13])."%'"));
					$n1=new ItemSatuan;
					$n1->id_satuan=$uid->id_satuan;
					$n1->id_item=$model->id_item;
					$n1->nilai_satuan_konversi=1;
					$n1->parent_id_item_satuan=null;
					$n1->status='Aktif';
					$n1->save();
					
					$grup=KelompokTarif::model()->findAll(array("condition"=>"status='Aktif'"));
					$kelas=Kelas::model()->findAll();
					foreach($grup as $g){
						foreach($kelas as $k){
							$tarif=new TarifItem;
							$tarif->id_item=$model->id_item;
							$tarif->id_kelompok_tarif=$g->id_kelompok_tarif;
							$tarif->id_kelas=$k->id_kelas;
							$tarif->harga_dasar=$harga;
							$tarif->ppn_persen=$ppn;
							$tarif->ppn_nominal=$tarif->harga_dasar*($tarif->ppn_persen/100);
							$tarif->margin_persen=$margin;
							$tarif->margin_nominal=($tarif->harga_dasar+$tarif->ppn_nominal)*($tarif->margin_persen/100);
							$tarif->tarif=$tarif->harga_dasar+$tarif->ppn_nominal+$tarif->margin_nominal;
							$tarif->save();
						}
					}
					$sukses++;
				}
			} else {
				$gagal++;
			}
		}
		@unlink($path);
		 Yii::app()->user->setFlash('success', "Proses import data selesai<br/>
         Jumlah data yang sukses diimport : ".$sukses." Jumlah data yang gagal diimport : ".$gagal."
         ");
		$this->redirect(array('index'));
		}
	}
	
	public function actionGetGroup()
	{
		$data=ItemGrup::model()->findAll('id_item_kategori=:id_item_kategori', 
					  array(':id_item_kategori'=>(int) $_POST['Item']['id_item_kategori']));
	 
		$data=CHtml::listData($data,'id_item_grup','nama_item_grup');
		echo CHtml::tag('option',array('value'=>''),CHtml::encode('- Choose -'),true);
		foreach($data as $value=>$name)
		{
			echo CHtml::tag('option',array('value'=>$value),CHtml::encode($name),true);
		}
	}
	
	public function actionGetItem()
	{
		$this->layout='//layouts/admin/realy_blank';
		if(!isset($_POST['item'])){
			$request=trim($_GET['term']);
			if($request!=''){
				$criteria=new CDbCriteria;
				$criteria->with=array("idItem");
				$criteria->condition="(idItem.nama_item like '%$request%' or idItem.kode_item like '%$request%')";
				$model=ItemSatuan::model()->findAll($criteria);
				$data=array();
				foreach($model as $get){
					$data[]=$get->idItem->nama_item.' | '.$get->idItem->kode_item.' | '.$get->idSatuan->nama_satuan;
				}
				echo json_encode($data);
			}
		}else{
			$request=explode(" | ",$_POST['item']);
			$criteria=new CDbCriteria;
			$criteria->with=array("idItem","idSatuan");
			$criteria->condition="(idItem.nama_item like '%$request[0]%' or idItem.kode_item like '%$request[1]%') AND idSatuan.nama_satuan='$request[2]'";
			$model=ItemSatuan::model()->find($criteria);
			$warehouse=$_POST['gudang'];//toko
			$data=array();
			$data['id_item_satuan']=$model->id_item_satuan;
			$data['id_item']=$model->id_item;
			$data['kode_item']=$model->idItem->kode_item;
			$data['parent']=($model->parent_id_item_satuan=="")?$model->id_item_satuan:$model->parent_id_item_satuan;
			$data['satuan']=$model->idSatuan->alias_satuan.' ( '.$model->nilai_satuan_konversi.' '.$model->parentIdItemSatuan->idSatuan->alias_satuan.' )';
			$data['nilaiKeSatuanKecil']=$model->nilai_satuan_konversi;
			$data['hargaSatuanKecil']=$model->idItem->wac;
			$data['hargaPerSatuanBesar']=$model->idItem->wac*$model->nilai_satuan_konversi;
			$data['nama_item']=$model->idItem->nama_item;
			$data['stok']=Item::getStock($model->id_item,$warehouse);
			/*
			
			$data['satuan']=Item::getParentUnit($model->id_item)->idSatuan->nama_satuan;
			$data['harga']=Item::getPrice($model->id_item,2,1);
			*/
			
			echo json_encode($data);
		}
	}
	
	public function actionGetItemByDepo()
	{
		$this->layout='//layouts/admin/realy_blank';
		if(!isset($_POST['item'])){
			$request=trim($_GET['term']);
			if($request!=''){
				$criteria=new CDbCriteria;
				$criteria=new CDbCriteria;
				$criteria->condition="nama_item like '%$request%' or zat_aktif like '%$request%'";
				$model=Item::model()->findAll($criteria);
				$data=array();
				foreach($model as $get){
					$data[]=$get->nama_item;
				}
				echo json_encode($data);
			}
		}else{
			$request=trim($_POST['item']);
			$criteria=new CDbCriteria;
			$criteria->condition="nama_item like '%$request%' or zat_aktif like '%$request%'";
			$model=Item::model()->find($criteria);
			$gudang=$_POST['depo'];//farmasi
			$data=array();
			$data['id_item']=$model->id_item;
			$data['nama_item']=$model->nama_item;
			$data['stok']=ItemGudang::getStock($model->id_item,$gudang);
			$data['satuan']=Item::getParentUnit($model->id_item)->idSatuan->nama_satuan;
			$data['harga']=Item::getPrice($model->id_item,2,1);
			
			echo json_encode($data);
		}
	}

	/**
	* Displays a particular model.
	* @param integer $id the ID of the model to be displayed
	*/
	public function actionView($id)
	{
		$this->render('view',array(
		'model'=>$this->loadModel($id),
		));
	}
	
	public function actionPrice($id)
	{
		if(isset($_GET['idGrup'])){
			$grup=KelompokTarif::model()->findByPk($_GET['idGrup']);
		} else {
			$grup=KelompokTarif::model()->find();
		}
		if(isset($_POST['Price'])){
			foreach($_POST['Price'] as $index => $val) {
				$harga_dasar = $val['harga_dasar'];
				$margin = $val['margin_persen'];
				$harga = $harga_dasar*(1+($margin/100));
				$tarif=TarifItem::model()->find(
					array("condition" => "id_item='$id' AND id_kelompok_tarif='$grup->id_kelompok_tarif' AND id_kelas='$index'")
				);
				if(!$tarif) {
					$tarif = new TarifItem;
					$tarif->id_item=$id;
					$tarif->id_kelompok_tarif=$grup->id_kelompok_tarif;
					$tarif->id_kelas=$index;
				}
				$tarif->harga_dasar=$harga_dasar;
				$tarif->ppn_persen=10;
				$tarif->ppn_nominal=$harga_dasar*($tarif->ppn_persen/100);
				$tarif->margin_persen=$margin;
				$tarif->margin_nominal=($harga_dasar+$tarif->ppn_nominal)*($tarif->margin_persen/100);
				$tarif->tarif=$tarif->harga_dasar+$tarif->ppn_nominal+$tarif->margin_nominal;
				$tarif->save();
			}
		}
		
		$this->render('price',array(
			'model'=>$this->loadModel($id),
			'grup'=>$grup
		));
	}

	/**
	* Creates a new model.
	* If creation is successful, the browser will be redirected to the 'view' page.
	*/
	public function actionCreate()
	{
		$model=new Item('insert');

		// Uncomment the following line if AJAX validation is needed
		$this->performAjaxValidation($model);

		if(isset($_POST['Item']))
		{
			$model->attributes=$_POST['Item'];
			$model->user_create=Yii::app()->user->id;
			$model->time_create=date("Y-m-d H:i:s");
			if(!empty($_POST['Item']['SatuanID'])){
				$model->unit=1;
			}
			if($model->save()){
				if(!empty($_POST['Item']['SatuanID'])){
					$w=Gudang::model()->findAll(array("condition"=>"status='Aktif'"));
					foreach($w as $x){
						$y=new ItemGudang;
						$y->id_item=$model->id_item;
						$y->id_gudang=$x->id_gudang;
						$y->stok=0;
						$y->save();
					}
					
					$idx=0;
					$parent=null;
					foreach($_POST['Item']['SatuanID'] as $index=>$val){
						$unit=new ItemSatuan;
						$unit->id_satuan=$val;
						$unit->id_item=$model->id_item;
						$unit->nilai_satuan_konversi=$_POST['Item']['Satuan'][$index];
						$unit->parent_id_item_satuan=$parent;
						$unit->status='Aktif';
						if($unit->save()){
							if($idx==0){
								$parent=$unit->id_item_satuan;
							}
							$idx++;
						}
					}
					
					$grup = KelompokTarif::model()->findAll();
					$kelas = Kelas::model()->findAll();
					foreach($grup as $g){
						foreach($kelas as $k){
							$tarif=TarifItem::model()->find(
								array("condition" => "id_item='$model->id_item' AND id_kelompok_tarif='$g->id_kelompok_tarif' AND id_kelas='$k->id_kelas'")
							);
							if(!$tarif) {
								$tarif = new TarifItem;
								$tarif->id_item=$model->id_item;
								$tarif->id_kelompok_tarif=$g->id_kelompok_tarif;
								$tarif->id_kelas=$k->id_kelas;
							}
							$tarif->harga_dasar=$model->harga_dasar_satuan_kecil;
							$tarif->ppn_persen=10;
							$tarif->ppn_nominal=$tarif->harga_dasar*($tarif->ppn_persen/100);
							$tarif->margin_persen=25;
							$tarif->margin_nominal=($tarif->harga_dasar+$tarif->ppn_nominal)*($tarif->margin_persen/100);
							$tarif->tarif=$tarif->harga_dasar+$tarif->ppn_nominal+$tarif->margin_nominal;
							$tarif->save();
						}
					}
					$this->redirect(array('view','id'=>$model->id_item));
				}else{
					$model->delete();
				}
			}
		}

		$this->render('create',array(
		'model'=>$model,
		));
	}

	/**
	* Updates a particular model.
	* If update is successful, the browser will be redirected to the 'view' page.
	* @param integer $id the ID of the model to be updated
	*/
	public function actionUpdate($id)
	{
		$model=$this->loadModel($id);

		// Uncomment the following line if AJAX validation is needed
		$this->performAjaxValidation($model);
	
		if(isset($_POST['Item']))
		{
			$model->attributes=$_POST['Item'];
			if($model->save()){
				$grup = KelompokTarif::model()->findAll();
				$kelas = Kelas::model()->findAll();
				foreach($grup as $g){
					foreach($kelas as $k){
						$tarif=TarifItem::model()->find(
							array("condition" => "id_item='$model->id_item' AND id_kelompok_tarif='$g->id_kelompok_tarif' AND id_kelas='$k->id_kelas'")
						);
						if(!$tarif) {
							$tarif = new TarifItem;
							$tarif->id_item=$model->id_item;
							$tarif->id_kelompok_tarif=$g->id_kelompok_tarif;
							$tarif->id_kelas=$k->id_kelas;
						}
						$tarif->harga_dasar=$model->harga_dasar_satuan_kecil;
						$tarif->ppn_nominal=$tarif->harga_dasar*($tarif->ppn_persen/100);
						$tarif->margin_nominal=($tarif->harga_dasar+$tarif->ppn_nominal)*($tarif->margin_persen/100);
						$tarif->tarif=$tarif->harga_dasar+$tarif->ppn_nominal+$tarif->margin_nominal;
						$tarif->save();
					}
				}
				$this->redirect(array('view','id'=>$model->id_item));
			}
		}

		$this->render('update',array(
		'model'=>$model,
		));
	}

	/**
	* Deletes a particular model.
	* If deletion is successful, the browser will be redirected to the 'admin' page.
	* @param integer $id the ID of the model to be deleted
	*/
	public function actionDelete($id)
	{
		if(Yii::app()->request->isPostRequest)
		{
		// we only allow deletion via POST request		
		$this->loadModel($id)->delete();

		// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
		if(!isset($_GET['ajax']))
		$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
		}
		else
		throw new CHttpException(400,'Invalid request. Please do not repeat this request again.');
	}

	/**
	* Lists all models.
	*/
	public function actionAdmin()
	{
		$dataProvider=new CActiveDataProvider('Item');
		$this->render('index',array(
		'dataProvider'=>$dataProvider,
		));
	}

	/**
	* Manages all models.
	*/
	public function actionIndex()
	{
		$model=new Item('search');
		$model->unsetAttributes();  // clear any default values
		if(isset($_GET['Item']))
		$model->attributes=$_GET['Item'];
		if (isset($_GET['pageSize'])) {
			Yii::app()->user->setState('pageSize',(int)$_GET['pageSize']); 
			unset($_GET['pageSize']);
		}
		$this->render('admin',array(
		'model'=>$model,
		));
	}
	
	public function loadModel($id)
	{
		$model=Item::model()->findByPk($id);
		if($model===null)
		throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}

	/**
	* Performs the AJAX validation.
	* @param CModel the model to be validated
	*/
	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='item-form')
		{
		echo CActiveForm::validate($model);
		Yii::app()->end();
		}
	}
}
