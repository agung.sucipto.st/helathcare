<?php

class PoliklinikController extends Controller
{
	public $layout='//layouts/admin/main';
	public $groupMenu=5;
	/**
	* @return array actio0n filters
	*/
	public function filters()
	{
		return array(
		'accessControl', // perform access control for CRUD operations
		);
	}

	/**
	* Specifies the access control rules.
	* This method is used by the 'accessControl' filter.
	* @return array access control rules
	*/
	public function accessRules()
	{
		return array(
			array('allow',  // allow all users to perform 'index' and 'view' actions
			'actions'=>array('index'),
			'expression'=>'$user->getAuth()',
			),			
			array('deny',
                'users'=>array('*'),
            ),

		);
	}
	
	public function actionIndex()
	{
		$model=new Registrasi('searchPoli');
		$model->unsetAttributes();  // clear any default values
		$model->searchPol=2;
		if(isset($_GET['Registrasi']))
		$model->attributes=$_GET['Registrasi'];
		if (isset($_GET['pageSize'])) {
			Yii::app()->user->setState('pageSize',(int)$_GET['pageSize']); 
			unset($_GET['pageSize']);
		}
		$this->render('index',array(
		'model'=>$model,
		));
	}
}