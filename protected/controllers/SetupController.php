<?php

class SetupController extends Controller
{
	public $layout='//layouts/admin/main';
	public $groupMenu=3;
	/**
	* @return array action filters
	*/
	public function filters()
	{
		return array(
		'accessControl', // perform access control for CRUD operations
		);
	}

	/**
	* Specifies the access control rules.
	* This method is used by the 'accessControl' filter.
	* @return array access control rules
	*/
	public function accessRules()
	{
		return array(
			array('allow',  // allow all users to perform 'index' and 'view' actions
			'actions'=>array('harga','updateHarga','uploadHarga','hargaPromo','updateHargaPromo','uploadHargaPromo','barcode'),
			'expression'=>'$user->getAuth()',
			),
			array('allow',  // allow all users to perform 'index' and 'view' actions
			'actions'=>array('minMax','updateMinMax','uploadMinMax','stokAwal','updateStokAwal','uploadStokAwal'),
			'expression'=>'$user->getAuth()',
			),
			array('allow',  // allow all users to perform 'index' and 'view' actions
			'actions'=>array('downloadMinMax','downloadStokAwal','downloadTemplateHarga','import'),
			'users'=>array('*'),
			),
			
			array('deny',
                'users'=>array('*'),
            ),

		);
	}
	
	public function actionImport()
	{
		ini_set("memory_limit","8056M");
		set_time_limit(100000);
		if(isset($_POST['ItemUnit']))
		{
		$model=new ItemUnit;
		$fileUpload=CUploadedFile::getInstance($model,'import');
		$nama=date('YmdHis').'_'.$fileUpload;
		$path=Yii::app()->basePath . '/../temp_excel/'.$nama.'';
		$fileUpload->saveAs($path);
			
		Yii::import('ext.phpexcelreader.JPhpExcelReader');
		$data=new JPhpExcelReader($path);
		$baris=$data->sheets[0]['numRows'];
		$kolom=$data->sheets[0]['numCols'];
		$sukses=0;
		$gagal=0;
		for($i=2;$i<=$baris; $i++){
			$unit=$data->sheets[0]['cells'][$i][1];
			$item=$data->sheets[0]['cells'][$i][2];
			$regD=$data->sheets[0]['cells'][$i][5];
			$regP=$data->sheets[0]['cells'][$i][6];
			$regS=$data->sheets[0]['cells'][$i][7];
			$memD=$data->sheets[0]['cells'][$i][8];
			$memP=$data->sheets[0]['cells'][$i][9];
			$memS=$data->sheets[0]['cells'][$i][10];
			$reg=ItemPrice::model()->find(array("condition"=>"item_id='$item' AND item_unit_id='$unit' AND price_type='REGULAR'"));
			if(empty($reg)){
				$reg=new ItemPrice;
				$reg->item_id=$item;
				$reg->item_unit_id=$unit;
				$reg->price_type="REGULAR";
			}
			$reg->item_price=$regD;
			$reg->item_tax=$regP;
			$reg->sale=$regS;
			$reg->save();
			
			$mem=ItemPrice::model()->find(array("condition"=>"item_id='$item' AND item_unit_id='$unit' AND price_type='MEMBER'"));
			if(empty($mem)){
				$mem=new ItemPrice;
				$mem->item_id=$item;
				$mem->item_unit_id=$unit;
				$mem->price_type="MEMBER";
			}
			$mem->item_price=$memD;
			$mem->item_tax=$memP;
			$mem->sale=$memS;
			$mem->save();
			if($reg->save() AND $mem->save()){
				$sukses++;
			}else{
				$gagal++;
			}
		}
		@unlink($path);
		 Yii::app()->user->setFlash('success', "Proses Update Harga Selesai<br/>
         Jumlah data yang sukses diimport : ".$sukses." Jumlah data yang gagal diimport : ".$gagal."
         ");
		$this->redirect(array('harga'));
		}
	}
	
	public function actionDownloadTemplateHarga(){
		Yii::import('ext.phpexcel.XPHPExcel');    
		$objPHPExcel= XPHPExcel::createPHPExcel();
		$objPHPExcel->getProperties()->setCreator("File")
		->setLastModifiedBy("File")
		->setTitle("Dokumen Rahasia")
		->setSubject("Dokumen Rahasia")
		->setDescription("File")
		->setKeywords("File")
		->setCategory("File");
		//loop here		
		$objPHPExcel->setActiveSheetIndex(0)->setCellValue('A1',"ID ITEM UNIT");
		$objPHPExcel->setActiveSheetIndex(0)->setCellValue('B1',"ID ITEM");
		$objPHPExcel->setActiveSheetIndex(0)->setCellValue('C1',"Nama Item");
		$objPHPExcel->setActiveSheetIndex(0)->setCellValue('D1',"Satuan");
		$objPHPExcel->setActiveSheetIndex(0)->setCellValue('E1',"Harga Dasar Regular");
		$objPHPExcel->setActiveSheetIndex(0)->setCellValue('F1',"PPN Regular");
		$objPHPExcel->setActiveSheetIndex(0)->setCellValue('G1',"Harga Jual Regular");
		$objPHPExcel->setActiveSheetIndex(0)->setCellValue('H1',"Harga Dasar Member");
		$objPHPExcel->setActiveSheetIndex(0)->setCellValue('I1',"PPN Member");
		$objPHPExcel->setActiveSheetIndex(0)->setCellValue('J1',"Harga Jual Member");
		
		$data= ItemUnit::model()->findAll();    
		$j = 1;
		$no=0;
		foreach($data as $row) {
			$j++;
			$no++;
			$reg=ItemPrice::model()->find(array("condition"=>"item_id='$row->item_id' AND item_unit_id='$row->item_unit_id' AND price_type='REGULAR'"));
			$mem=ItemPrice::model()->find(array("condition"=>"item_id='$row->item_id' AND item_unit_id='$row->item_unit_id' AND price_type='REGULAR'"));
			$objPHPExcel->setActiveSheetIndex(0)->setCellValue('A'.$j, $row->item_unit_id);
			$objPHPExcel->setActiveSheetIndex(0)->setCellValue('B'.$j, $row->item_id);
			$objPHPExcel->setActiveSheetIndex(0)->setCellValue('C'.$j, $row->item->item_name);
			$objPHPExcel->setActiveSheetIndex(0)->setCellValue('D'.$j, $row->unit->unit_alias);
			$objPHPExcel->setActiveSheetIndex(0)->setCellValue('E'.$j, $reg->item_price);
			$objPHPExcel->setActiveSheetIndex(0)->setCellValue('F'.$j, $reg->item_tax);
			$objPHPExcel->setActiveSheetIndex(0)->setCellValue('G'.$j, $reg->sale);
			$objPHPExcel->setActiveSheetIndex(0)->setCellValue('H'.$j, $mem->item_price);
			$objPHPExcel->setActiveSheetIndex(0)->setCellValue('I'.$j, $mem->item_tax);
			$objPHPExcel->setActiveSheetIndex(0)->setCellValue('J'.$j, $mem->sale);
		}
		$styleArray = array(
			'font' => array(
				'bold' => true,
			),
		);
		$styleArrayData = array(
			'borders' => array(
				'allborders' => array(
					'style' => PHPExcel_Style_Border::BORDER_THIN
				),
			),
		);
		$objPHPExcel->getActiveSheet()->getStyle('A1:J1')->applyFromArray($styleArray);
		$objPHPExcel->getActiveSheet()->getStyle('A1:J'.$j)->applyFromArray($styleArrayData);
		// Rename worksheet
		$objPHPExcel->getActiveSheet()->setTitle('Default');

		// Set active sheet index to the first sheet, so Excel opens this as the first sheet
		$objPHPExcel->setActiveSheetIndex(0);
		 		 
		// Redirect output to a client’s web browser (Excel5)
		header('Content-Type: application/vnd.ms-excel');
		header('Content-Disposition: attachment;filename="Update Harga '.date('Y-m-d H:i:s').'.xls"');
		header('Cache-Control: max-age=0');
		// If you're serving to IE 9, then the following may be needed
		header('Cache-Control: max-age=1');
		 
		// If you're serving to IE over SSL, then the following may be needed
		header ('Expires: Mon, 26 Jul 1997 05:00:00 GMT'); // Date in the past
		header ('Last-Modified: '.gmdate('D, d M Y H:i:s').' GMT'); // always modified
		header ('Cache-Control: cache, must-revalidate'); // HTTP/1.1
		header ('Pragma: public'); // HTTP/1.0
		 
		$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
		$objWriter->save('php://output');
	    Yii::app()->end();
	}
	
	public function actionHarga()
	{
		$model=new ItemUnit('search');
		$model->unsetAttributes();  // clear any default values
		if(isset($_GET['ItemUnit']))
		$model->attributes=$_GET['ItemUnit'];
		if (isset($_GET['pageSize'])) {
			Yii::app()->user->setState('pageSize',(int)$_GET['pageSize']); 
			unset($_GET['pageSize']);
		}
		$this->render('admin',array(
		'model'=>$model,
		));
	}
	
	public function actionUpdateHarga($item,$unit)
	{
		$itemx=Item::model()->findByPk($item);
		$reg=ItemPrice::model()->find(array("condition"=>"item_id='$item' AND item_unit_id='$unit' AND price_type='REGULAR'"));
		if(empty($reg)){
			$reg=new ItemPrice;
			$reg->item_id=$item;
			$reg->item_unit_id=$unit;
			$reg->price_type="REGULAR";
		}
		
		$member=ItemPrice::model()->find(array("condition"=>"item_id='$item' AND item_unit_id='$unit' AND price_type='MEMBER'"));
		if(empty($member)){
			$member=new ItemPrice;
			$member->item_id=$item;
			$member->item_unit_id=$unit;
			$member->price_type="MEMBER";
		}
		$reg->item_price2=$member->item_price;
		$reg->item_tax2=$member->item_tax;
		$reg->sale2=$member->sale;
		if(isset($_POST['ItemPrice']))
		{
			$reg->attributes=$_POST['ItemPrice'];
			$member->item_price=$_POST['ItemPrice']['item_price2'];
			$member->item_tax=$_POST['ItemPrice']['item_tax2'];
			$member->sale=$_POST['ItemPrice']['sale2'];
			if($reg->save() AND $member->save()){
				$this->redirect(array('harga'));
			}
		}

		$this->render('update_harga',array(
		'mem'=>$member,'reg'=>$reg,'item'=>$itemx
		));
	}
	
	public function actionBarcode($id)
	{
		$this->layout='//layouts/admin/blank';
		$this->pageTitle="Setup Barcode";
		$model=ItemUnit::model()->findByPk($id);
		$model->setScenario('barcode');
		$show=true;
		if(isset($_POST['ItemUnit']))
		{
			$model->attributes=$_POST['ItemUnit'];
			if($model->save()){
				$show=false;
			}
		}
		
		$this->render('barcode',array(
		'model'=>$model,'show'=>$show
		));
	}
	
	public function actionMinMax()
	{
		$model=new WarehouseItem('search');
		$model->unsetAttributes();  // clear any default values
		if(isset($_GET['WarehouseItem']))
		$model->attributes=$_GET['WarehouseItem'];
		if (isset($_GET['pageSize'])) {
			Yii::app()->user->setState('pageSize',(int)$_GET['pageSize']); 
			unset($_GET['pageSize']);
		}
		$this->render('min_max',array(
		'model'=>$model,
		));
	}
	
	public function actionUpdateMinMax($id)
	{
		$this->layout='//layouts/admin/blank';
		$this->pageTitle="Setup Min Max";
		$model=WarehouseItem::model()->findByPk($id);
		$model->setScenario('minmax');
		$show=true;
		if(isset($_POST['WarehouseItem']))
		{
			$model->attributes=$_POST['WarehouseItem'];
			if($model->save()){
				$show=false;
			}
		}
		
		$this->render('update_min_max',array(
		'model'=>$model,'show'=>$show
		));
	}
	
	public function actionDownloadMinMax(){
		if(isset($_POST)){
			Yii::import('ext.phpexcel.XPHPExcel');    
			$objPHPExcel= XPHPExcel::createPHPExcel();
			$objPHPExcel->getProperties()->setCreator("File")
			->setLastModifiedBy("File")
			->setTitle("Dokumen Rahasia")
			->setSubject("Dokumen Rahasia")
			->setDescription("File")
			->setKeywords("File")
			->setCategory("File");
			//loop here		
			$objPHPExcel->setActiveSheetIndex(0)->setCellValue('A1',"ID Warehouse");
			$objPHPExcel->setActiveSheetIndex(0)->setCellValue('B1',"Warehouse");
			$objPHPExcel->setActiveSheetIndex(0)->setCellValue('C1',"Kode Item");
			$objPHPExcel->setActiveSheetIndex(0)->setCellValue('D1',"Nama Item");
			$objPHPExcel->setActiveSheetIndex(0)->setCellValue('E1',"Satuan");
			$objPHPExcel->setActiveSheetIndex(0)->setCellValue('F1',"Minimum Stok");
			$objPHPExcel->setActiveSheetIndex(0)->setCellValue('G1',"Maximum Stok");
			
			$warehouse=$_POST['WarehouseItem']['warehouse_destination_id'];
			if($warehouse!=''){
				$data= WarehouseItem::model()->findAll(array("condition"=>"warehouse_id='$warehouse'"));
			}else{
				$data= WarehouseItem::model()->findAll();  
			}
			  
			$j = 1;
			$no=0;
			foreach($data as $row) {
				$j++;
				$no++;
				$satuan=Item::getParentUnit($row->item_id)->unit->unit_name;
				$objPHPExcel->setActiveSheetIndex(0)->setCellValue('A'.$j, $row->warehouse_item_id);
				$objPHPExcel->setActiveSheetIndex(0)->setCellValue('B'.$j, $row->warehouse->warehouse_name);
				$objPHPExcel->setActiveSheetIndex(0)->setCellValue('C'.$j, $row->item->code);
				$objPHPExcel->setActiveSheetIndex(0)->setCellValue('D'.$j, $row->item->item_name);
				$objPHPExcel->setActiveSheetIndex(0)->setCellValue('E'.$j, $satuan);
				$objPHPExcel->setActiveSheetIndex(0)->setCellValue('F'.$j, $row->min);
				$objPHPExcel->setActiveSheetIndex(0)->setCellValue('G'.$j, $row->max);
			}
			$styleArray = array(
				'font' => array(
					'bold' => true,
				),
			);
			$styleArrayData = array(
				'borders' => array(
					'allborders' => array(
						'style' => PHPExcel_Style_Border::BORDER_THIN
					),
				),
			);
			$objPHPExcel->getActiveSheet()->getStyle('A1:G1')->applyFromArray($styleArray);
			$objPHPExcel->getActiveSheet()->getStyle('A1:G'.$j)->applyFromArray($styleArrayData);
			// Rename worksheet
			$objPHPExcel->getActiveSheet()->setTitle('Default');

			// Set active sheet index to the first sheet, so Excel opens this as the first sheet
			$objPHPExcel->setActiveSheetIndex(0);
					 
			// Redirect output to a client’s web browser (Excel5)
			header('Content-Type: application/vnd.ms-excel');
			header('Content-Disposition: attachment;filename="Update Min Max '.date('Y-m-d H:i:s').'.xls"');
			header('Cache-Control: max-age=0');
			// If you're serving to IE 9, then the following may be needed
			header('Cache-Control: max-age=1');
			 
			// If you're serving to IE over SSL, then the following may be needed
			header ('Expires: Mon, 26 Jul 1997 05:00:00 GMT'); // Date in the past
			header ('Last-Modified: '.gmdate('D, d M Y H:i:s').' GMT'); // always modified
			header ('Cache-Control: cache, must-revalidate'); // HTTP/1.1
			header ('Pragma: public'); // HTTP/1.0
			 
			$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
			$objWriter->save('php://output');
			Yii::app()->end();
		}
	}
	
	public function actionUploadMinMax()
	{
		ini_set("memory_limit","8056M");
		set_time_limit(100000);
		if(isset($_POST['WarehouseItem']))
		{
		$model=new WarehouseItem;
		$fileUpload=CUploadedFile::getInstance($model,'import');
		$nama=date('YmdHis').'_'.$fileUpload;
		$path=Yii::app()->basePath . '/../temp_excel/'.$nama.'';
		$fileUpload->saveAs($path);
			
		Yii::import('ext.phpexcelreader.JPhpExcelReader');
		$data=new JPhpExcelReader($path);
		$baris=$data->sheets[0]['numRows'];
		$kolom=$data->sheets[0]['numCols'];
		$sukses=0;
		$gagal=0;
		for($i=2;$i<=$baris; $i++){
			$id=$data->sheets[0]['cells'][$i][1];
			$min=$data->sheets[0]['cells'][$i][6];
			$max=$data->sheets[0]['cells'][$i][7];
			$reg=WarehouseItem::model()->findByPk($id);
			$reg->min=$min;
			$reg->max=$max;
			$reg->save();
			
			if($reg->save()){
				$sukses++;
			}else{
				$gagal++;
			}
		}
		@unlink($path);
		 Yii::app()->user->setFlash('success', "Proses Update Harga Selesai<br/>
         Jumlah data yang sukses diimport : ".$sukses." Jumlah data yang gagal diimport : ".$gagal."
         ");
		$this->redirect(array('minMax'));
		}
	}
	
	public function actionStokAwal()
	{
		$model=new WarehouseItem('search');
		$model->unsetAttributes();  // clear any default values
		if(isset($_GET['WarehouseItem']))
		$model->attributes=$_GET['WarehouseItem'];
		if (isset($_GET['pageSize'])) {
			Yii::app()->user->setState('pageSize',(int)$_GET['pageSize']); 
			unset($_GET['pageSize']);
		}
		$this->render('stok_awal',array(
		'model'=>$model,
		));
	}
	
	public function actionUpdateStokAwal($id)
	{
		$this->layout='//layouts/admin/blank';
		$this->pageTitle="Setup Stok Awal";
		$model=WarehouseItem::model()->findByPk($id);
		$model->setScenario('awal');
		$show=true;
		if(isset($_POST['WarehouseItem']))
		{
			$model->attributes=$_POST['WarehouseItem'];
			if($model->save()){
				$show=false;
			}
		}
		
		$this->render('update_stok_awal',array(
		'model'=>$model,'show'=>$show
		));
	}
	
	public function actionDownloadStokAwal(){
		if(isset($_POST)){
			Yii::import('ext.phpexcel.XPHPExcel');    
			$objPHPExcel= XPHPExcel::createPHPExcel();
			$objPHPExcel->getProperties()->setCreator("File")
			->setLastModifiedBy("File")
			->setTitle("Dokumen Rahasia")
			->setSubject("Dokumen Rahasia")
			->setDescription("File")
			->setKeywords("File")
			->setCategory("File");
			//loop here		
			$objPHPExcel->setActiveSheetIndex(0)->setCellValue('A1',"ID Warehouse");
			$objPHPExcel->setActiveSheetIndex(0)->setCellValue('B1',"Warehouse");
			$objPHPExcel->setActiveSheetIndex(0)->setCellValue('C1',"Kode Item");
			$objPHPExcel->setActiveSheetIndex(0)->setCellValue('D1',"Nama Item");
			$objPHPExcel->setActiveSheetIndex(0)->setCellValue('E1',"Satuan");
			$objPHPExcel->setActiveSheetIndex(0)->setCellValue('F1',"Stok");
			
			$warehouse=$_POST['WarehouseItem']['warehouse_destination_id'];
			if($warehouse!=''){
				$data= WarehouseItem::model()->findAll(array("condition"=>"warehouse_id='$warehouse'"));
			}else{
				$data= WarehouseItem::model()->findAll();  
			}
			  
			$j = 1;
			$no=0;
			foreach($data as $row) {
				$j++;
				$no++;
				$satuan=Item::getParentUnit($row->item_id)->unit->unit_name;
				$objPHPExcel->setActiveSheetIndex(0)->setCellValue('A'.$j, $row->warehouse_item_id);
				$objPHPExcel->setActiveSheetIndex(0)->setCellValue('B'.$j, $row->warehouse->warehouse_name);
				$objPHPExcel->setActiveSheetIndex(0)->setCellValue('C'.$j, $row->item->code);
				$objPHPExcel->setActiveSheetIndex(0)->setCellValue('D'.$j, $row->item->item_name);
				$objPHPExcel->setActiveSheetIndex(0)->setCellValue('E'.$j, $satuan);
				$objPHPExcel->setActiveSheetIndex(0)->setCellValue('F'.$j, $row->stock);
			}
			$styleArray = array(
				'font' => array(
					'bold' => true,
				),
			);
			$styleArrayData = array(
				'borders' => array(
					'allborders' => array(
						'style' => PHPExcel_Style_Border::BORDER_THIN
					),
				),
			);
			$objPHPExcel->getActiveSheet()->getStyle('A1:F1')->applyFromArray($styleArray);
			$objPHPExcel->getActiveSheet()->getStyle('A1:F'.$j)->applyFromArray($styleArrayData);
			// Rename worksheet
			$objPHPExcel->getActiveSheet()->setTitle('Default');

			// Set active sheet index to the first sheet, so Excel opens this as the first sheet
			$objPHPExcel->setActiveSheetIndex(0);
					 
			// Redirect output to a client’s web browser (Excel5)
			header('Content-Type: application/vnd.ms-excel');
			header('Content-Disposition: attachment;filename="Update Stok Awal '.date('Y-m-d H:i:s').'.xls"');
			header('Cache-Control: max-age=0');
			// If you're serving to IE 9, then the following may be needed
			header('Cache-Control: max-age=1');
			 
			// If you're serving to IE over SSL, then the following may be needed
			header ('Expires: Mon, 26 Jul 1997 05:00:00 GMT'); // Date in the past
			header ('Last-Modified: '.gmdate('D, d M Y H:i:s').' GMT'); // always modified
			header ('Cache-Control: cache, must-revalidate'); // HTTP/1.1
			header ('Pragma: public'); // HTTP/1.0
			 
			$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
			$objWriter->save('php://output');
			Yii::app()->end();
		}
	}
	
	public function actionUploadStokAwal()
	{
		ini_set("memory_limit","8056M");
		set_time_limit(100000);
		if(isset($_POST['WarehouseItem']))
		{
		$model=new WarehouseItem;
		$fileUpload=CUploadedFile::getInstance($model,'import');
		$nama=date('YmdHis').'_'.$fileUpload;
		$path=Yii::app()->basePath . '/../temp_excel/'.$nama.'';
		$fileUpload->saveAs($path);
			
		Yii::import('ext.phpexcelreader.JPhpExcelReader');
		$data=new JPhpExcelReader($path);
		$baris=$data->sheets[0]['numRows'];
		$kolom=$data->sheets[0]['numCols'];
		$sukses=0;
		$gagal=0;
		for($i=2;$i<=$baris; $i++){
			$id=$data->sheets[0]['cells'][$i][1];
			$stok=$data->sheets[0]['cells'][$i][6];
			$reg=WarehouseItem::model()->findByPk($id);
			$reg->stock=$stok;
			$reg->save();
			
			if($reg->save()){
				$sukses++;
			}else{
				$gagal++;
			}
		}
		@unlink($path);
		 Yii::app()->user->setFlash('success', "Proses Update Harga Selesai<br/>
         Jumlah data yang sukses diimport : ".$sukses." Jumlah data yang gagal diimport : ".$gagal."
         ");
		$this->redirect(array('stokAwal'));
		}
	}
}