<?php
class TindakanController extends Controller
{
	/**
	* @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	* using two-column layout. See 'protected/views/layouts/column2.php'.
	*/
	public $layout='//layouts/admin/main';

	/**
	* @return array action filters
	*/
	public function filters()
	{
		return array(
		'accessControl', // perform access control for CRUD operations
		);
	}

	/**
	* Specifies the access control rules.
	* This method is used by the 'accessControl' filter.
	* @return array access control rules
	*/
	public function accessRules()
	{
		return array(
		array('allow',  // allow all users to perform 'index' and 'view' actions
		'actions'=>array('index','view'),
		'expression'=>'$user->getAuth()',
		),
		array('allow', // allow admin user to perform 'admin' and 'delete' actions
		'actions'=>array('create','update','admin','delete'),
		'expression'=>'$user->getAuth()',
		),
		
		array('allow', // allow admin user to perform 'admin' and 'delete' actions
		'actions'=>array('getDetailTindakan','getTindakan'),
		'users'=>array('@'),
		),

		array('deny',  // deny all users
		'users'=>array('*'),
		),
		);
	}
	
	public function actionGetTindakan()
	{
		$data=Tindakan::model()->findAll('id_kelompok_tindakan=:id_kelompok_tindakan', 
					  array(':id_kelompok_tindakan'=>(int) $_POST['TindakanPasien']['id_kelompok_tindakan']));
	 
		$data=CHtml::listData($data,'id_tindakan','nama_tindakan');
		echo CHtml::tag('option',array('value'=>''),CHtml::encode('Pilih Tindakan'),true);
		foreach($data as $value=>$name)
		{
			echo CHtml::tag('option',array('value'=>$value),CHtml::encode($name),true);
		}
	}

	public function actionGetDetailTindakan()
	{
		$this->layout='//layouts/admin/realy_blank';
		$id=$_POST['id'];
		$kelas=$_POST['kelas'];
		$reg=Registrasi::model()->findByPk($_POST['idreg']);
		if($reg->jenis_jaminan=="UMUM"){
			$kelompok=1;
		}else{
			$kelompok=$reg->jaminanPasiens[0]->idPenjamin->id_kelompok_tarif;
		}
		
		$tarif=TarifTindakan::model()->find(array("condition"=>"id_tindakan='$id' and id_kelas='$kelas' and id_kelompok_tarif='$kelompok'"));
		$model=TarifTindakanMedis::model()->findAll(array("with"=>array("idTindakanMedis"),"condition"=>"idTindakanMedis.id_tindakan='$id' AND id_tarif_tindakan='$tarif->id_tarif_tindakan'","order"=>"idTindakanMedis.order_no ASC"));
		$data=array();
		$data["tarif"]=array("jasa_layanan"=>$tarif->jasa_pelayanan,"tarif_all"=>$tarif->harga_jual,"id"=>$tarif->id_tarif_tindakan);
		foreach($model as $row){
			$data["medis"][]=array("id"=>$row->id_tindakan_medis,"id_tarif"=>$row->id_tarif_tindakan_medis,"nama_peran"=>$row->idTindakanMedis->idPeranMedis->nama_peran_medis,"mandatory"=>$row->idTindakanMedis->mandatory,"tarif"=>$row->jasa_medis);
		}
		echo json_encode($data);
	}
	
	/**
	* Displays a particular model.
	* @param integer $id the ID of the model to be displayed
	*/
	public function actionView($id)
	{
		$this->render('view',array(
		'model'=>$this->loadModel($id),
		));
	}

	/**
	* Creates a new model.
	* If creation is successful, the browser will be redirected to the 'view' page.
	*/
	public function actionCreate()
	{
		$model=new Tindakan;

		// Uncomment the following line if AJAX validation is needed
		$this->performAjaxValidation($model);

		if(isset($_POST['Tindakan']))
		{
			$model->attributes=$_POST['Tindakan'];
			if($model->save()){
				$this->redirect(array('view','id'=>$model->id_tindakan));
			}
		}

		$this->render('create',array(
		'model'=>$model,
		));
	}

	/**
	* Updates a particular model.
	* If update is successful, the browser will be redirected to the 'view' page.
	* @param integer $id the ID of the model to be updated
	*/
	public function actionUpdate($id)
	{
		$model=$this->loadModel($id);

		// Uncomment the following line if AJAX validation is needed
		$this->performAjaxValidation($model);
	
		if(isset($_POST['Tindakan']))
		{
			$model->attributes=$_POST['Tindakan'];
			if($model->save()){
				$this->redirect(array('view','id'=>$model->id_tindakan));
			}
		}

		$this->render('update',array(
		'model'=>$model,
		));
	}

	/**
	* Deletes a particular model.
	* If deletion is successful, the browser will be redirected to the 'admin' page.
	* @param integer $id the ID of the model to be deleted
	*/
	public function actionDelete($id)
	{
		if(Yii::app()->request->isPostRequest)
		{
		// we only allow deletion via POST request		
		$this->loadModel($id)->delete();

		// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
		if(!isset($_GET['ajax']))
		$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
		}
		else
		throw new CHttpException(400,'Invalid request. Please do not repeat this request again.');
	}

	/**
	* Lists all models.
	*/
	public function actionAdmin()
	{
		$dataProvider=new CActiveDataProvider('Tindakan');
		$this->render('index',array(
		'dataProvider'=>$dataProvider,
		));
	}

	/**
	* Manages all models.
	*/
	public function actionIndex()
	{
		$model=new Tindakan('search');
		$model->unsetAttributes();  // clear any default values
		if(isset($_GET['Tindakan']))
		$model->attributes=$_GET['Tindakan'];
		if (isset($_GET['pageSize'])) {
			Yii::app()->user->setState('pageSize',(int)$_GET['pageSize']); 
			unset($_GET['pageSize']);
		}
		$this->render('admin',array(
		'model'=>$model,
		));
	}
	
	public function loadModel($id)
	{
		$model=Tindakan::model()->findByPk($id);
		if($model===null)
		throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}

	/**
	* Performs the AJAX validation.
	* @param CModel the model to be validated
	*/
	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='tindakan-form')
		{
		echo CActiveForm::validate($model);
		Yii::app()->end();
		}
	}
}
