<?php
class ItemTransaksiController extends Controller
{
	/**
	* @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	* using two-column layout. See 'protected/views/layouts/column2.php'.
	*/
	public $layout='//layouts/admin/main';

	/**
	* @return array action filters
	*/
	public function filters()
	{
		return array(
		'accessControl', // perform access control for CRUD operations
		);
	}

	/**
	* Specifies the access control rules.
	* This method is used by the 'accessControl' filter.
	* @return array access control rules
	*/
	public function accessRules()
	{
		return array(
		array('allow',  // allow all users to perform 'index' and 'view' actions
		'actions'=>array('hapusRU','hapusR'),
		'users'=>array('*'),
		),
		array('deny',  // deny all users
		'users'=>array('*'),
		),
		);
	}

	public function actionHapusRU($id)
	{
		if(Yii::app()->request->isPostRequest)
		{
		// we only allow deletion via POST request		
		$current = $this->loadModel($id);
		$trx=new ItemTransaksi('RU');
		$trx->id_registrasi=$current->id_registrasi;
		$trx->gudang_asal=$current->gudang_asal;
		$trx->gudang_tujuan=$current->gudang_asal;
		$trx->id_jenis_item_transaksi=22;
		$trx->waktu_transaksi=date("Y-m-d H:i:s");
		$trx->catatan_transaksi="Pembatalan Penjualan Ruangan, Pasien $registrasi->no_registrasi - ".$current->idRegistrasi->idPasien->idPersonal->nama_lengkap." [".Lib::MRN($current->idRegistrasi->id_pasien)."]";
		$trx->no_transaksi=ItemTransaksi::getTransactionNumber($trx->id_jenis_item_transaksi,"XRU",date("Y-m-d"));
		$trx->user_create=Yii::app()->user->id;
		$trx->time_create=date("Y-m-d H:i:s");
		if($trx->save()){
			foreach($current->daftarItemTransaksis as $row){
				$list=new DaftarItemTransaksi;
				$list->id_item_transaksi=$trx->id_item_transaksi;
				$list->id_item_gudang=$row->id_item_gudang;
				$list->id_item=$row->id_item;
				$list->id_item_satuan_besar=$row->id_item_satuan_besar;
				$list->id_item_satuan_kecil=$row->id_item_satuan_kecil;
				$list->jumlah_satuan_kecil=$row->jumlah_satuan_kecil;
				$list->jumlah_satuan_besar=$row->jumlah_satuan_besar;
				$list->jumlah_transaksi=$list->jumlah_satuan_kecil*$list->jumlah_satuan_besar;
				$list->harga_transaksi=$row->harga_transaksi;
				$list->harga_wac=$row->harga_wac;
				$list->jumlah_sebelum_transaksi=ItemGudang::getStock($row->id_item,$trx->gudang_tujuan);
				$list->jumlah_setelah_transaksi=$list->jumlah_sebelum_transaksi+$list->jumlah_transaksi;
				$total+=$list->harga_transaksi*$list->jumlah_transaksi;
				if($list->save()){
					//Update Stok di gudang terkait
					ItemGudang::updateStock($row->id_item,$trx->gudang_tujuan,$list->jumlah_setelah_transaksi);
					$row->idDaftarTagihan->id_daftar_item_transaksi=null;
					$row->id_daftar_tagihan=null;
					if ($row->idDaftarTagihan->save() && $row->save()) {
						$row->idDaftarTagihan->delete();
					}
				}
				//*/
			}
			//*
			$trx->nominal_transaksi=$total;
			$trx->save();
			$current->hapus=1;
			$current->user_delete=Yii::app()->user->id;
			$current->time_delete=date("Y-m-d H:i:s");
			$current->save();
			//*/
		}

		// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
		if(!isset($_GET['ajax']))
		$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
		}
		else
		throw new CHttpException(400,'Invalid request. Please do not repeat this request again.');
	}
	
	
	public function actionHapusR($id)
	{
		if(Yii::app()->request->isPostRequest)
		{
		// we only allow deletion via POST request		
		$current = $this->loadModel($id);
		$trx=new ItemTransaksi('RU');
		$trx->id_registrasi=$current->id_registrasi;
		$trx->gudang_asal=$current->gudang_asal;
		$trx->gudang_tujuan=$current->gudang_asal;
		$trx->id_jenis_item_transaksi=15;
		$trx->waktu_transaksi=date("Y-m-d H:i:s");
		$trx->catatan_transaksi="Pembatalan Penjualan Resep, Pasien $registrasi->no_registrasi - ".$current->idRegistrasi->idPasien->idPersonal->nama_lengkap." [".Lib::MRN($current->idRegistrasi->id_pasien)."]";
		$trx->no_transaksi=ItemTransaksi::getTransactionNumber($trx->id_jenis_item_transaksi,"XR",date("Y-m-d"));
		$trx->user_create=Yii::app()->user->id;
		$trx->time_create=date("Y-m-d H:i:s");
		if($trx->save()){
			foreach($current->daftarItemTransaksis as $row){
				if($row->id_item!=''){
					$list=new DaftarItemTransaksi;
					$list->id_item_transaksi=$trx->id_item_transaksi;
					$list->id_item_gudang=$row->id_item_gudang;
					$list->id_item=$row->id_item;
					$list->id_item_satuan_besar=$row->id_item_satuan_besar;
					$list->id_item_satuan_kecil=$row->id_item_satuan_kecil;
					$list->jumlah_satuan_kecil=$row->jumlah_satuan_kecil;
					$list->jumlah_satuan_besar=$row->jumlah_satuan_besar;
					$list->jumlah_transaksi=$list->jumlah_satuan_kecil*$list->jumlah_satuan_besar;
					$list->harga_transaksi=$row->harga_transaksi;
					$list->harga_wac=$row->harga_wac;
					$list->jumlah_sebelum_transaksi=ItemGudang::getStock($row->id_item,$trx->gudang_tujuan);
					$list->jumlah_setelah_transaksi=$list->jumlah_sebelum_transaksi+$list->jumlah_transaksi;
					$total+=$list->harga_transaksi*$list->jumlah_transaksi;
					if($list->save()){
						//Update Stok di gudang terkait
						ItemGudang::updateStock($row->id_item,$trx->gudang_tujuan,$list->jumlah_setelah_transaksi);
						if($row->id_daftar_tagihan !=''){
							$row->idDaftarTagihan->id_daftar_item_transaksi=null;
							$row->id_daftar_tagihan=null;
							if ($row->idDaftarTagihan->save() && $row->save()) {
								$row->idDaftarTagihan->delete();
							}
						}
					}
				} else {
					$row->idDaftarTagihan->id_daftar_item_transaksi=null;
					$row->id_daftar_tagihan=null;
					if ($row->idDaftarTagihan->save() && $row->save()) {
						$row->idDaftarTagihan->delete();
					}
				}
				//*/
			}
			//*
			$trx->nominal_transaksi=$total;
			$trx->save();
			$current->hapus=1;
			$current->user_delete=Yii::app()->user->id;
			$current->time_delete=date("Y-m-d H:i:s");
			$current->save();
			if($current->idRegistrasi->jenis_registrasi == 'OTC') {
				$current->idRegistrasi->status_registrasi='Batal';
				$current->idRegistrasi->save();;
			}
			//*/
		}

		// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
		if(!isset($_GET['ajax']))
		$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
		}
		else
		throw new CHttpException(400,'Invalid request. Please do not repeat this request again.');
	}
	
	public function loadModel($id)
	{
		$model=ItemTransaksi::model()->findByPk($id);
		if($model===null)
		throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}
}
