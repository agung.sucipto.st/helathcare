<?php

class LaporanController extends Controller
{
	public $layout='//layouts/admin/main';
	public $groupMenu=16;
	/**
	* @return array action filters
	*/
	public function filters()
	{
		return array(
		'accessControl', // perform access control for CRUD operations
		);
	}

	/**
	* Specifies the access control rules.
	* This method is used by the 'accessControl' filter.
	* @return array access control rules
	*/
	public function accessRules()
	{
		return array(
			array('allow',  // allow all users to perform 'index' and 'view' actions
			'actions'=>array('index','harianPoliIgd','penyakitTerbesar','rujuk'),
			'expression'=>'$user->getAuth()',
			),	
			array('allow',  // allow all users to perform 'index' and 'view' actions
			'actions'=>array('grafikWilayah','grafikJenisKunjungan','grafikJenisPembayaran','grafikJenisLayanan'),
			'expression'=>'$user->getAuth()',
			),				
			array('deny',
                'users'=>array('*'),
            ),

		);
	}
	
	public function actionIndex()
	{
		$title="Grafik Kunjungan<br/>".Lib::dateIndShortMonth($dateStart).' - '.Lib::dateIndShortMonth($dateEnd);
		$query="select 
		MONTH(waktu_registrasi) as tanggal, 
		SUM(IF(jenis_jaminan='UMUM',1,0)) AS umum,
		SUM(IF(jenis_jaminan='JAMINAN',1,0)) AS asuransi,
		SUM(IF(jenis_kunjungan=1,1,0)) AS baru,
		SUM(IF(jenis_kunjungan=0,1,0)) AS lama,
		SUM(IF(personal.jenis_kelamin='Laki-laki',1,0)) AS laki,
		SUM(IF(personal.jenis_kelamin='Perempuan',1,0)) AS perempuan 
		from registrasi inner JOIN pasien ON pasien.id_pasien=registrasi.id_pasien inner JOIN personal ON personal.id_personal=pasien.id_personal where MONTH(registrasi.waktu_registrasi)='".date('m')."' AND YEAR(registrasi.waktu_registrasi)='".date('Y')."' AND (jenis_registrasi='Rawat Jalan' OR jenis_registrasi='IGD') AND status_registrasi!='Batal' group by MONTH(waktu_registrasi) order by tanggal ASC";
		
		$queryLayanan="SELECT count(registrasi.id_departemen) as jumlah,departemen.nama_departemen from registrasi inner join departemen ON registrasi.id_departemen=departemen.id_departemen where MONTH(registrasi.waktu_registrasi)='".date('m')."' AND YEAR(registrasi.waktu_registrasi)='".date('Y')."' AND (jenis_registrasi='Rawat Jalan' OR jenis_registrasi='IGD') AND status_registrasi!='Batal' group by registrasi.id_departemen";
		
		
		$queryLine="select DATE(waktu_registrasi) as tanggal, SUM(IF(jenis_kunjungan=1,1,0)) AS baru,
		SUM(IF(jenis_kunjungan=0,1,0)) AS lama from registrasi where MONTH(registrasi.waktu_registrasi)='".date("m")."' AND YEAR(registrasi.waktu_registrasi)='".date('Y')."' AND (jenis_registrasi='Rawat Jalan' OR jenis_registrasi='IGD') group by DATE(waktu_registrasi) order by tanggal ASC";
		
		$queryPendapatan="select DATE(tagihan_pasien.waktu_tagihan) as tanggal, SUM(IF(registrasi.jenis_jaminan='UMUM',tagihan_pasien.total_tagihan,0)) AS umum,
		SUM(IF(registrasi.jenis_jaminan='JAMINAN',tagihan_pasien.total_tagihan,0)) AS asuransi from tagihan_pasien inner join registrasi ON registrasi.id_registrasi=tagihan_pasien.id_registrasi where MONTH(tagihan_pasien.waktu_tagihan)='".date('m')."' AND YEAR(tagihan_pasien.waktu_tagihan)='".date('Y')."' AND (registrasi.jenis_registrasi='Rawat Jalan' OR registrasi.jenis_registrasi='IGD') group by DATE(tagihan_pasien.waktu_tagihan) order by tanggal ASC";

		$get=Yii::app()->db->createCommand($query)->queryAll();
		$getLine=Yii::app()->db->createCommand($queryLine)->queryAll();
		$getPendapatan=Yii::app()->db->createCommand($queryPendapatan)->queryAll();
		$getLayanan=Yii::app()->db->createCommand($queryLayanan)->queryAll();

		$pieJk=array();
		$pieJn=array();
		$pieJp=array();
		$pieLayanan=array();
		foreach($get as $row){
			$pieJk[]='{name: "Laki-laki",y: '.$row['laki'].'}';
			$pieJk[]='{name: "Perempuan",y: '.$row['perempuan'].'}';
			$pieJn[]='{name: "Baru",y: '.$row['baru'].'}';
			$pieJn[]='{name: "Lama",y: '.$row['lama'].'}';
			$pieJp[]='{name: "Umum",y: '.$row['umum'].'}';
			$pieJp[]='{name: "Asuranasi",y: '.$row['asuransi'].'}';
			
		}
		
		if(!empty($getLine)){
			$dataX=array();
			foreach($getLine as $row){
				$dataX[$row['tanggal']]['Baru']=$row['baru'];	
				$dataX[$row['tanggal']]['Lama']=$row['lama'];	
			}
			
			$highcartLabel=array();
			$highcartSeries=array();
			$highcartSeriesValues=array();
			foreach($dataX as $index=>$val){
				$highcartLabel[]='"'.$index.'"';
				$data[$index].="{ y: '".$alias[$index]."', ";
				
				$temp=array();
				foreach($val as $r=>$i){
					$temp[].=str_replace(" ","",$r).":'$i'";
					$detail[str_replace(" ","",$r)]=$r;
					$highcartSeries[$index][$r]=$i;
				}
				$data[$index].=implode(", ",$temp);
				$data[$index].='}';
			}
			
			foreach($detail as $dRow){
				$highcartSeriesValues[$dRow]="{name: \"$dRow\",data: [";
				$tempData=array();
				foreach($highcartLabel as $subRow){
					$tempData[]=($highcartSeries[str_replace('"',"",$subRow)][$dRow]=="")?0:$highcartSeries[str_replace('"',"",$subRow)][$dRow];
				}
				$highcartSeriesValues[$dRow].=implode(",",$tempData);
				$highcartSeriesValues[$dRow].="]}";
			}
		}else{
			$highcartLabel=array();
			$highcartSeries=array();
			$highcartSeriesValues=array();
		}
		
		
		if(!empty($getPendapatan)){
			$dataXP=array();
			foreach($getPendapatan as $row){
				$dataXP[$row['tanggal']]['Umum']=$row['umum'];	
				$dataXP[$row['tanggal']]['Asuransi']=$row['asuransi'];	
			}
			
			$highcartLabelP=array();
			$highcartSeriesP=array();
			$highcartSeriesValuesP=array();
			foreach($dataXP as $index=>$val){
				$highcartLabelP[]='"'.$index.'"';
				
				$tempP=array();
				foreach($val as $r=>$i){
					$tempP[].=str_replace(" ","",$r).":'$i'";
					$detailP[str_replace(" ","",$r)]=$r;
					$highcartSeriesP[$index][$r]=$i;
				}
			}
			
			foreach($detailP as $dRow){
				$highcartSeriesValuesP[$dRow]="{name: \"$dRow\",data: [";
				$tempDataP=array();
				foreach($highcartLabelP as $subRow){
					$tempDataP[]=($highcartSeriesP[str_replace('"',"",$subRow)][$dRow]=="")?0:$highcartSeriesP[str_replace('"',"",$subRow)][$dRow];
				}
				$highcartSeriesValuesP[$dRow].=implode(",",$tempDataP);
				$highcartSeriesValuesP[$dRow].="]}";
			}
			
		}else{
			$highcartLabelP=array();
			$highcartSeriesP=array();
			$highcartSeriesValuesP=array();
		}
		
		foreach($getLayanan as $row){
			$pieLayanan[]='{name: "'.$row['nama_departemen'].'",y: '.$row['jumlah'].'}';
		}
		$this->render('index',array(
			'model'=>$model,
			'title'=>$title,
			'pieJk'=>$pieJk,
			'pieJn'=>$pieJn,
			'pieJp'=>$pieJp,
			'pieLayanan'=>$pieLayanan,
			'hLabel'=>$highcartLabel,'hValues'=>$highcartSeriesValues,
			'hLabelP'=>$highcartLabelP,'hValuesP'=>$highcartSeriesValuesP,
		));
	}
	
	public function actionHarianPoliIgd()
	{
		$this->pageTitle="Laporan Harian Poli / IGD";
		$model=new Registrasi('searchLaporan');
		$model->unsetAttributes();  // clear any default values
		if(isset($_GET['Registrasi'])){
			$model->attributes=$_GET['Registrasi'];
		}
		
		if (isset($_GET['pageSize'])) {
			Yii::app()->user->setState('pageSize',(int)$_GET['pageSize']); 
			unset($_GET['pageSize']);
		}
		$this->render('view_harian_igd',array(
			'model'=>$model
		));
	}	
	
	public function actionPenyakitTerbesar()
	{
		$this->pageTitle="Laporan Penyakit Terbesar Rawat Jalan";
		$model=new Icd10Pasien;
		if(isset($_POST['Icd10Pasien']['waktu_registrasi'])){
			$model->waktu_registrasi=$_POST['Icd10Pasien']['waktu_registrasi'];
			$list=explode(" - ",$_POST['Icd10Pasien']['waktu_registrasi']);
			$start=explode("/",$list[0]);
			$end=explode("/",$list[1]);
			$dateStart=$start[2]."-".$start[0]."-".$start[1];
			$dateEnd=$end[2]."-".$end[0]."-".$end[1];
		}else{
			$dateStart=date("Y-m-d");
			$dateEnd=date("Y-m-d");
		}
		$title=Lib::dateIndShortMonth($dateStart).' - '.Lib::dateIndShortMonth($dateEnd);
		$query="select count(icd10_pasien.id_icd10) as total_diagnosa, icd10.kode_icd, icd10.diagnosa,SUM(IF(personal.jenis_kelamin='Laki-laki',1,0)) AS laki,
		SUM(IF(personal.jenis_kelamin='Perempuan',1,0)) AS perempuan,
		SUM(IF(registrasi.jenis_kunjungan=1,1,0)) AS baru,
		SUM(IF(registrasi.jenis_kunjungan=0,1,0)) AS lama
		from icd10_pasien inner join registrasi ON registrasi.id_registrasi=icd10_pasien.id_registrasi inner JOIN pasien ON pasien.id_pasien=registrasi.id_pasien inner JOIN personal ON personal.id_personal=pasien.id_personal inner join icd10 ON icd10.id_icd10=icd10_pasien.id_icd10 where DATE(registrasi.waktu_registrasi) between '$dateStart' AND '$dateEnd' AND (registrasi.jenis_registrasi='Rawat Jalan' OR registrasi.jenis_registrasi='IGD') group by icd10.kode_icd order by total_diagnosa DESC";
		
		$data=Yii::app()->db->createCommand($query)->queryAll();
		
		$this->render('view_penyakit_terbesar',array(
			'data'=>$data,
			'model'=>$model,
			'title'=>$title
		));
	}
	
	public function actionRujuk()
	{
		$this->pageTitle="Laporan Rujukan";
		$model=new Registrasi('searchPembayaran');
		$model->unsetAttributes();  // clear any default values
		$model->status_keluar="Dirujuk";  // clear any default values
		if(isset($_GET['Registrasi'])){
			$model->attributes=$_GET['Registrasi'];
		}
		
		if (isset($_GET['pageSize'])) {
			Yii::app()->user->setState('pageSize',(int)$_GET['pageSize']); 
			unset($_GET['pageSize']);
		}
		$this->render('view_rujukan',array(
			'model'=>$model
		));
	}
	
	
	public function actionGrafikWilayah()
	{
		$this->pageTitle="Kunjungan Per Wilayah";
		$model=new Icd10Pasien;
		if(isset($_POST['Icd10Pasien']['waktu_registrasi'])){
			$model->waktu_registrasi=$_POST['Icd10Pasien']['waktu_registrasi'];
			$list=explode(" - ",$_POST['Icd10Pasien']['waktu_registrasi']);
			$start=explode("/",$list[0]);
			$end=explode("/",$list[1]);
			$dateStart=$start[2]."-".$start[0]."-".$start[1];
			$dateEnd=$end[2]."-".$end[0]."-".$end[1];
		}else{
			$dateStart=date("Y-m-d");
			$dateEnd=date("Y-m-d");
		}
		
		$title="Grafik Kunjungan Per Wilayah<br/>".Lib::dateIndShortMonth($dateStart).' - '.Lib::dateIndShortMonth($dateEnd);
		$query="select kabupaten.nama , count(id_registrasi) as jumlah from registrasi inner JOIN pasien ON pasien.id_pasien=registrasi.id_pasien inner JOIN personal ON personal.id_personal=pasien.id_personal inner JOIN kabupaten ON personal.id_kabupaten=kabupaten.id_kabupaten where DATE(registrasi.waktu_registrasi) between '$dateStart' AND '$dateEnd' AND (jenis_registrasi='Rawat Jalan' OR jenis_registrasi='IGD') group by personal.id_kabupaten order by personal.id_kabupaten ASC";
		$get=Yii::app()->db->createCommand($query)->queryAll();
		
		if(!empty($get)){
			$dataX=array();
			foreach($get as $row){
				$dataX[$row['nama']]['Jumlah Kunjungan']=$row['jumlah'];	
			}
			
			$highcartLabel=array();
			$highcartSeries=array();
			$highcartSeriesValues=array();
			foreach($dataX as $index=>$val){
				$highcartLabel[]='"'.$index.'"';
				$data[$index].="{ y: '".$alias[$index]."', ";
				
				$temp=array();
				foreach($val as $r=>$i){
					$temp[].=str_replace(" ","",$r).":'$i'";
					$detail[str_replace(" ","",$r)]=$r;
					$highcartSeries[$index][$r]=$i;
				}
				$data[$index].=implode(", ",$temp);
				$data[$index].='}';
			}
			
			foreach($detail as $dRow){
				$highcartSeriesValues[$dRow]="{name: \"$dRow\",data: [";
				$tempData=array();
				foreach($highcartLabel as $subRow){
					$tempData[]=($highcartSeries[str_replace('"',"",$subRow)][$dRow]=="")?0:$highcartSeries[str_replace('"',"",$subRow)][$dRow];
				}
				$highcartSeriesValues[$dRow].=implode(",",$tempData);
				$highcartSeriesValues[$dRow].="]}";
			}
		}else{
			$highcartLabel=array();
			$highcartSeries=array();
			$highcartSeriesValues=array();
		}
		$this->render('grafik_wilayah',array(
			'hLabel'=>$highcartLabel,'hSeries'=>$highcartSeries,'hValues'=>$highcartSeriesValues,
			'model'=>$model,
			'title'=>$title
		));
	}
	
	public function actionGrafikJenisPembayaran()
	{
		$this->pageTitle="Grafik Kunjungan Jenis Pembayaran";
		$model=new Icd10Pasien;
		if(isset($_POST['Icd10Pasien']['waktu_registrasi'])){
			$model->waktu_registrasi=$_POST['Icd10Pasien']['waktu_registrasi'];
			$list=explode(" - ",$_POST['Icd10Pasien']['waktu_registrasi']);
			$start=explode("/",$list[0]);
			$end=explode("/",$list[1]);
			$dateStart=$start[2]."-".$start[0]."-".$start[1];
			$dateEnd=$end[2]."-".$end[0]."-".$end[1];
		}else{
			$dateStart=date("Y-m-d");
			$dateEnd=date("Y-m-d");
		}
		
		$title="Grafik Kunjungan Per Jenis Pembayaran<br/>".Lib::dateIndShortMonth($dateStart).' - '.Lib::dateIndShortMonth($dateEnd);
		$query="select count(id_registrasi) as jumlah,jenis_jaminan,DATE(waktu_registrasi) as tanggal from registrasi where DATE(registrasi.waktu_registrasi) between '$dateStart' AND '$dateEnd' AND (jenis_registrasi='Rawat Jalan' OR jenis_registrasi='IGD') group by jenis_jaminan,DATE(waktu_registrasi) order by waktu_registrasi ASC";
		$get=Yii::app()->db->createCommand($query)->queryAll();
		
		if(!empty($get)){
			$dataX=array();
			foreach($get as $row){
				$dataX[$row['tanggal']][$row['jenis_jaminan']]=$row['jumlah'];	
			}
			
			$highcartLabel=array();
			$highcartSeries=array();
			$highcartSeriesValues=array();
			foreach($dataX as $index=>$val){
				$highcartLabel[]='"'.$index.'"';
				$data[$index].="{ y: '".$alias[$index]."', ";
				
				$temp=array();
				foreach($val as $r=>$i){
					$temp[].=str_replace(" ","",$r).":'$i'";
					$detail[str_replace(" ","",$r)]=$r;
					$highcartSeries[$index][$r]=$i;
				}
				$data[$index].=implode(", ",$temp);
				$data[$index].='}';
			}
			
			foreach($detail as $dRow){
				$highcartSeriesValues[$dRow]="{name: \"$dRow\",data: [";
				$tempData=array();
				foreach($highcartLabel as $subRow){
					$tempData[]=($highcartSeries[str_replace('"',"",$subRow)][$dRow]=="")?0:$highcartSeries[str_replace('"',"",$subRow)][$dRow];
				}
				$highcartSeriesValues[$dRow].=implode(",",$tempData);
				$highcartSeriesValues[$dRow].="]}";
			}
		}else{
			$highcartLabel=array();
		$highcartSeries=array();
		$highcartSeriesValues=array();
		}
		
		$this->render('grafik_jenis_pembayaran',array(
			'hLabel'=>$highcartLabel,'hSeries'=>$highcartSeries,'hValues'=>$highcartSeriesValues,
			'model'=>$model,
			'title'=>$title
		));
	}
	
	
	public function actionGrafikJenisKunjungan()
	{
		$this->pageTitle="Grafik Kunjungan Jenis Kunjungan";
		$model=new Registrasi;
		if(isset($_POST['Registrasi']['waktu_registrasi'])){
			$model->waktu_registrasi=$_POST['Registrasi']['waktu_registrasi'];
			$list=explode(" - ",$_POST['Registrasi']['waktu_registrasi']);
			$start=explode("/",$list[0]);
			$end=explode("/",$list[1]);
			$dateStart=$start[2]."-".$start[0]."-".$start[1];
			$dateEnd=$end[2]."-".$end[0]."-".$end[1];
		}else{
			$dateStart=date("Y-m-d");
			$dateEnd=date("Y-m-d");
		}
		
		$title="Grafik Jenis Kunjungan<br/>".Lib::dateIndShortMonth($dateStart).' - '.Lib::dateIndShortMonth($dateEnd);
		$query="select DATE(waktu_registrasi) as tanggal, SUM(IF(jenis_kunjungan=1,1,0)) AS baru,
		SUM(IF(jenis_kunjungan=0,1,0)) AS lama from registrasi where DATE(registrasi.waktu_registrasi) between '$dateStart' AND '$dateEnd' AND (jenis_registrasi='Rawat Jalan' OR jenis_registrasi='IGD') group by DATE(waktu_registrasi) order by tanggal ASC";
		$get=Yii::app()->db->createCommand($query)->queryAll();
		
		if(!empty($get)){
			$dataX=array();
			foreach($get as $row){
				$dataX[$row['tanggal']]['Baru']=$row['baru'];	
				$dataX[$row['tanggal']]['Lama']=$row['lama'];	
			}
			
			$highcartLabel=array();
			$highcartSeries=array();
			$highcartSeriesValues=array();
			foreach($dataX as $index=>$val){
				$highcartLabel[]='"'.$index.'"';
				$data[$index].="{ y: '".$alias[$index]."', ";
				
				$temp=array();
				foreach($val as $r=>$i){
					$temp[].=str_replace(" ","",$r).":'$i'";
					$detail[str_replace(" ","",$r)]=$r;
					$highcartSeries[$index][$r]=$i;
				}
				$data[$index].=implode(", ",$temp);
				$data[$index].='}';
			}
			
			foreach($detail as $dRow){
				$highcartSeriesValues[$dRow]="{name: \"$dRow\",data: [";
				$tempData=array();
				foreach($highcartLabel as $subRow){
					$tempData[]=($highcartSeries[str_replace('"',"",$subRow)][$dRow]=="")?0:$highcartSeries[str_replace('"',"",$subRow)][$dRow];
				}
				$highcartSeriesValues[$dRow].=implode(",",$tempData);
				$highcartSeriesValues[$dRow].="]}";
			}
		}else{
			$highcartLabel=array();
			$highcartSeries=array();
			$highcartSeriesValues=array();
		}
		
		$this->render('grafik_jenis_kunjungan',array(
			'hLabel'=>$highcartLabel,'hSeries'=>$highcartSeries,'hValues'=>$highcartSeriesValues,
			'model'=>$model,
			'title'=>$title
		));
	}
	
	
	public function actionGrafikJenisLayanan()
	{
		$this->pageTitle="Grafik Kunjungan Jenis Layanan";
		$model=new Registrasi;
		if(isset($_POST['Registrasi']['waktu_registrasi'])){
			$model->waktu_registrasi=$_POST['Registrasi']['waktu_registrasi'];
			$list=explode(" - ",$_POST['Registrasi']['waktu_registrasi']);
			$start=explode("/",$list[0]);
			$end=explode("/",$list[1]);
			$dateStart=$start[2]."-".$start[0]."-".$start[1];
			$dateEnd=$end[2]."-".$end[0]."-".$end[1];
		}else{
			$dateStart=date("Y-m-d");
			$dateEnd=date("Y-m-d");
		}
		
		$title="Grafik Jenis Layanan<br/>".Lib::dateIndShortMonth($dateStart).' - '.Lib::dateIndShortMonth($dateEnd);
		$query="select DATE(waktu_registrasi) as tanggal, COUNT(registrasi.id_departemen) AS layanan, nama_departemen from registrasi inner join departemen ON departemen.id_departemen=registrasi.id_departemen where DATE(registrasi.waktu_registrasi) between '$dateStart' AND '$dateEnd' AND (jenis_registrasi='Rawat Jalan' OR jenis_registrasi='IGD') group by DATE(waktu_registrasi),nama_departemen order by tanggal ASC";
		$get=Yii::app()->db->createCommand($query)->queryAll();
		
		if(!empty($get)){
			$dataX=array();
			foreach($get as $row){
				$dataX[$row['tanggal']][$row['nama_departemen']]=$row['layanan'];	
			}
			
			$highcartLabel=array();
			$highcartSeries=array();
			$highcartSeriesValues=array();
			foreach($dataX as $index=>$val){
				$highcartLabel[]='"'.$index.'"';
				$data[$index].="{ y: '".$alias[$index]."', ";
				
				$temp=array();
				foreach($val as $r=>$i){
					$temp[].=str_replace(" ","",$r).":'$i'";
					$detail[str_replace(" ","",$r)]=$r;
					$highcartSeries[$index][$r]=$i;
				}
				$data[$index].=implode(", ",$temp);
				$data[$index].='}';
			}
			
			foreach($detail as $dRow){
				$highcartSeriesValues[$dRow]="{name: \"$dRow\",data: [";
				$tempData=array();
				foreach($highcartLabel as $subRow){
					$tempData[]=($highcartSeries[str_replace('"',"",$subRow)][$dRow]=="")?0:$highcartSeries[str_replace('"',"",$subRow)][$dRow];
				}
				$highcartSeriesValues[$dRow].=implode(",",$tempData);
				$highcartSeriesValues[$dRow].="]}";
			}
		}else{
			$highcartLabel=array();
			$highcartSeries=array();
			$highcartSeriesValues=array();
		}
		
		$this->render('grafik_jenis_layanan',array(
			'hLabel'=>$highcartLabel,'hSeries'=>$highcartSeries,'hValues'=>$highcartSeriesValues,
			'model'=>$model,
			'title'=>$title
		));
	}
}