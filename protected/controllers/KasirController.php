<?php

class KasirController extends Controller
{
	public $layout='//layouts/admin/main';
	public $groupMenu=11;
	/**
	* @return array action filters
	*/
	public function filters()
	{
		return array(
		'accessControl', // perform access control for CRUD operations
		);
	}

	/**
	* Specifies the access control rules.
	* This method is used by the 'accessControl' filter.
	* @return array access control rules
	*/
	public function accessRules()
	{
		return array(
			array('allow',  // allow all users to perform 'index' and 'view' actions
			'actions'=>array('index','pembayaranPasien','daftarTagihan','daftarPembayaran','bayar','tagihan','kwitansi'),
			'expression'=>'$user->getAuth()',
			),			
			array('deny',
                'users'=>array('*'),
            ),

		);
	}
	
	public function actionIndex()
	{
		$model=new Registrasi('searchPembayaran');
		$model->unsetAttributes();  // clear any default values
		if(isset($_GET['Registrasi']))
		$model->attributes=$_GET['Registrasi'];
		if (isset($_GET['pageSize'])) {
			Yii::app()->user->setState('pageSize',(int)$_GET['pageSize']); 
			unset($_GET['pageSize']);
		}
		$this->render('daftar_kunjungan',array(
		'model'=>$model,
		));
	}
	
	public function actionPembayaranPasien()
	{
		$model=new Registrasi('searchPembayaran');
		$model->unsetAttributes();  // clear any default values
		if(isset($_GET['Registrasi']))
		$model->attributes=$_GET['Registrasi'];
		if (isset($_GET['pageSize'])) {
			Yii::app()->user->setState('pageSize',(int)$_GET['pageSize']); 
			unset($_GET['pageSize']);
		}
		$this->render('daftar_kunjungan',array(
		'model'=>$model,
		));
	}
	
	
	public function actionDaftarTagihan($id)
	{
		$model=Registrasi::model()->findByPk($id);
		$tagihan=new DaftarTagihan("search");
		$tagihan->id_tagihan=null;
		$tagihan->id_registrasi=$id;
		
		$request = Yii::app()->getRequest();
		if($request->getIsPostRequest()){
			if(isset($_POST['ids'])){
				$trx=new TagihanPasien;
				$trx->jenis_tagihan=$model->jenis_registrasi;
				$trx->waktu_tagihan=date("Y-m-d H:i:s");
				$trx->no_tagihan=TagihanPasien::getBillNo($trx->jenis_tagihan,date("Y-m-d"));
				$trx->id_registrasi=$id;
				$trx->total_tagihan=TagihanPasien::getAmountGroup($_POST['ids'],"jumlah_bayar");
				$trx->total_discount=TagihanPasien::getAmountGroup($_POST['ids'],"discount");
				$trx->status_tagihan="Belum Lunas";
				$trx->user_create=Yii::app()->user->id;
				$trx->time_create=date("Y-m-d H:i:s");
				
				if($trx->save()){
					DaftarTagihan::model()->updateAll(array('id_tagihan' => $trx->id_tagihan_pasien), "id_daftar_tagihan IN(".implode(",",$_POST['ids']).")");
					foreach($trx->daftarTagihans as $row){
						if($row->id_tindakan_pasien != ''){
							//ok
							foreach($row->idTindakanPasien->tindakanPasienMedises as $x){
								$jm = new DaftarJasaMedis;
								$jm->id_pegawai=$x->id_pegawai;
								$jm->id_daftar_tagihan=$row->id_daftar_tagihan;
								$jm->id_tindakan_pasien_medis=$x->id_tindakan_pasien_medis;
								$jm->nilai_jasa_medis=$x->jasa_medis;
								$jm->gross=$row->tarif;
								$jm->status_pembayaran='Belum Lunas';
								$jm->keterangan=$row->deskripsi_komponen;
								$jm->user_create=Yii::app()->user->id;
								$jm->time_create=date("Y-m-d H:i:s");
								$jm->save();
							}
						}
						if($row->id_peralatan_pasien != ''){
							// ok
							foreach($row->idPeralatanPasien->peralatanPasienMedises as $x){
								$jm = new DaftarJasaMedis;
								$jm->id_pegawai=$x->id_pegawai;
								$jm->id_daftar_tagihan=$row->id_daftar_tagihan;
								$jm->id_peralatan_pasien_medis=$x->id_peralatan_pasien_medis;
								$jm->nilai_jasa_medis=$x->jasa_medis;
								$jm->gross=$row->tarif;
								$jm->status_pembayaran='Belum Lunas';
								$jm->keterangan=$row->deskripsi_komponen;
								$jm->user_create=Yii::app()->user->id;
								$jm->time_create=date("Y-m-d H:i:s");
								$jm->save();
							}
						}
						if($row->id_visite_dokter_pasien != ''){
							//ok
							$jm = new DaftarJasaMedis;
							$jm->id_pegawai=$row->idVisiteDokterPasien->id_dokter;
							$jm->id_daftar_tagihan=$row->id_daftar_tagihan;
							$jm->id_visite_dokter_pasien=$row->id_visite_dokter_pasien;
							$jm->nilai_jasa_medis=$row->idVisiteDokterPasien->share_dokter;
							$jm->gross=$row->tarif;
							$jm->status_pembayaran='Belum Lunas';
							$jm->keterangan=$row->deskripsi_komponen;
							$jm->user_create=Yii::app()->user->id;
							$jm->time_create=date("Y-m-d H:i:s");
							$jm->save();
						}
						if($row->id_radiology_pasien_list != ''){
							foreach($row->idRadiologyPasienList->radiologyPasienMedises as $x){
								$jm = new DaftarJasaMedis;
								$jm->id_pegawai=$x->id_pegawai;
								$jm->id_daftar_tagihan=$row->id_daftar_tagihan;
								$jm->id_radiology_pasien_medis=$x->id_radiology_pasien_medis;
								$jm->nilai_jasa_medis=$x->jasa_medis;
								$jm->gross=$row->tarif;
								$jm->status_pembayaran='Belum Lunas';
								$jm->keterangan=$row->deskripsi_komponen;
								$jm->user_create=Yii::app()->user->id;
								$jm->time_create=date("Y-m-d H:i:s");
								$jm->save();
							}
						}
						if($row->id_laboratorium_pasien_list != ''){
							foreach($row->idLaboratoriumPasienList->laboratoriumPasienMedises as $x){
								$jm = new DaftarJasaMedis;
								$jm->id_pegawai=$x->id_pegawai;
								$jm->id_daftar_tagihan=$row->id_daftar_tagihan;
								$jm->id_laboratorium_pasien_medis=$x->id_laboratorium_pasien_medis;
								$jm->nilai_jasa_medis=$x->jasa_medis;
								$jm->gross=$row->tarif;
								$jm->status_pembayaran='Belum Lunas';
								$jm->keterangan=$row->deskripsi_komponen;
								$jm->user_create=Yii::app()->user->id;
								$jm->time_create=date("Y-m-d H:i:s");
								$jm->save();
							}
						}
						if($row->id_rehab_medis_pasien_list != ''){
							foreach($row->idRehabMedisPasienList->rehabMedisPasienMedises as $x){
								$jm = new DaftarJasaMedis;
								$jm->id_pegawai=$x->id_pegawai;
								$jm->id_daftar_tagihan=$row->id_daftar_tagihan;
								$jm->id_rehab_medis_pasien_medis=$x->id_rehab_medis_pasien_medis;
								$jm->nilai_jasa_medis=$x->jasa_medis;
								$jm->gross=$row->tarif;
								$jm->status_pembayaran='Belum Lunas';
								$jm->keterangan=$row->deskripsi_komponen;
								$jm->user_create=Yii::app()->user->id;
								$jm->time_create=date("Y-m-d H:i:s");
								$jm->save();
							}
						}
						if($row->id_hemodialisa_pasien_list != ''){
							foreach($row->idHemodialisaPasienList->hemodialisaPasienMedises as $x){
								$jm = new DaftarJasaMedis;
								$jm->id_pegawai=$x->id_pegawai;
								$jm->id_daftar_tagihan=$row->id_daftar_tagihan;
								$jm->id_hemodialisa_pasien_medis=$x->id_hemodialisa_pasien_medis;
								$jm->nilai_jasa_medis=$x->jasa_medis;
								$jm->gross=$row->tarif;
								$jm->status_pembayaran='Belum Lunas';
								$jm->keterangan=$row->deskripsi_komponen;
								$jm->user_create=Yii::app()->user->id;
								$jm->time_create=date("Y-m-d H:i:s");
								$jm->save();
							}
						}
					}
					$data['status']=200;
				}else{
					$data['status']=500;
				}
				
				echo CJSON::encode($data);
			}
			exit;
		}
			
			
		$this->render('view',array(
		'model'=>$model,"tagihan"=>$tagihan
		));
	}
	
	
	public function actionDaftarPembayaran($id)
	{
		$model=Registrasi::model()->findByPk($id);
		$tagihan=new TagihanPasien("search");
		$tagihan->id_registrasi=$id;
			
		$this->render('view',array(
		'model'=>$model,"tagihan"=>$tagihan
		));
	}
	
	
	public function actionTagihan($id,$idReg,$idBill)
	{
		$this->layout='//layouts/admin/realy_blank';
		$model=Registrasi::model()->findByPk($idReg);
		$tagihan=TagihanPasien::model()->findByPk($idBill);
		$list=DaftarTagihan::model()->findAll(array("condition"=>"id_tagihan='$idBill'"));
		$this->render('rincian_tagihan',array(
			'model'=>$model,'tagihan'=>$tagihan,'list'=>$list
		));
	}
	
	public function actionKwitansi($idBill)
	{
		$this->layout='//layouts/admin/realy_blank';
		$tagihan=TagihanPasien::model()->findByPk($idBill);
		$this->render('kwitansi',array(
			'tagihan'=>$tagihan
		));
	}
	
	
	public function actionBayar($id,$idReg)
	{
		$model=Registrasi::model()->findByPk($idReg);
		$tagihan=TagihanPasien::model()->findByPk($id);	
		
		if(isset($_POST['PembayaranTagihan'])){
			$success=0;
			foreach($_POST['PembayaranTagihan'] as $index=>$row){
				if($index=="Cash"){
					$pay=new PembayaranTagihan;
					$pay->no_pembayaran=PembayaranTagihan::getPayNo(strtoupper($index),date("Y-m-d"));
					$pay->id_tagihan_pasien=$id;
					$pay->waktu_pembayaran=date("Y-m-d H:i:s");
					$pay->jenis_pembayaran=strtoupper($index);
					$pay->total_pembayaran=$tagihan->total_tagihan;
					$pay->total_dibayarkan=$row;
					if($row>$tagihan->total_tagihan){
						$pay->total_dikembalikan=$row-$tagihan->total_tagihan;
					}else{
						$pay->total_dikembalikan=null;
					}
					$pay->status_pembayaran="Lunas";
					$pay->user_create=Yii::app()->user->id;
					$pay->time_create=date("Y-m-d H:i:s");
				}elseif($index=="Card"){
					$pay=new PembayaranTagihan;
					$pay->no_pembayaran=PembayaranTagihan::getPayNo(strtoupper($row['type']),date("Y-m-d"));
					$pay->id_tagihan_pasien=$id;
					$pay->waktu_pembayaran=date("Y-m-d H:i:s");
					$pay->jenis_pembayaran=strtoupper($row['type']);
					$pay->total_pembayaran=$tagihan->total_tagihan;
					$pay->total_dibayarkan=$row['bayar'];
					$pay->nomor_kartu_debit_kredit=$row['no_card'];
					$pay->nomor_batch=$row['batch'];
					$pay->tujuan_transfer=$row['no_rek'];
					$pay->status_pembayaran="Lunas";
					$pay->user_create=Yii::app()->user->id;
					$pay->time_create=date("Y-m-d H:i:s");
				}
				if($pay->save()){
					$success++;
				}
			}
			
			if(count($_POST['PembayaranTagihan'])==$success){
				$tagihan->status_tagihan="Lunas";
				$tagihan->user_update=Yii::app()->user->id;
				$tagihan->time_update=date("Y-m-d H:i:s");
				$tagihan->save();
				$this->redirect(array("daftarPembayaran","id"=>$idReg));
			}
		}
		$this->render('view',array(
		'model'=>$model,"tagihan"=>$tagihan,		));
	}
}