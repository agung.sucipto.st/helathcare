<?php

class WarehouseDataController extends Controller
{
	public $layout='//layouts/admin/main';
	public $groupMenu=13;
	/**
	* @return array action filters
	*/
	public function filters()
	{
		return array(
		'accessControl', // perform access control for CRUD operations
		);
	}

	/**
	* Specifies the access control rules.
	* This method is used by the 'accessControl' filter.
	* @return array access control rules
	*/
	public function accessRules()
	{
		return array(
			array('allow',  // allow all users to perform 'index' and 'view' actions
			'actions'=>array('index'),
			'expression'=>'$user->getAuth()',
			),
			
			array('deny',
                'users'=>array('*'),
            ),

		);
	}
	
	public function actionIndex()
	{
		$this->render('index');
	}
}