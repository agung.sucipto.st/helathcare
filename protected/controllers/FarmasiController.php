<?php

class FarmasiController extends Controller
{
	public $layout='//layouts/admin/main';
	public $groupMenu=8;
	/**
	* @return array action filters
	*/
	public function filters()
	{
		return array(
		'accessControl', // perform access control for CRUD operations
		);
	}

	/**
	* Specifies the access control rules.
	* This method is used by the 'accessControl' filter.
	* @return array access control rules
	*/
	public function accessRules()
	{
		return array(
			array('allow',  // allow all users to perform 'index' and 'view' actions
			'actions'=>array('index'),
			'expression'=>'$user->getAuth()',
			),			
			array('allow', // allow admin user to perform 'admin' and 'delete' actions
			'actions'=>array('printEtiket'),
			'users'=>array('*'),
			),
			array('deny',
                'users'=>array('*'),
            ),

		);
	}
	
	public function actionPrintEtiket($id)
	{
		$model=$this->loadModel($id);
		$paper = 'label_etiket';
		$data = array();
		foreach($model->daftarItemTransaksis as $row){
			if ($row->id_daftar_tagihan != ''){
				if($row->is_racikan == 1){
					$data[] = array(
						'no_rm'=>Lib::MRN($model->idRegistrasi->id_pasien),
						'no_reg'=>$model->idRegistrasi->no_registrasi,
						'gudang'=>$model->gudangAsal->nama_gudang,
						'nama_pasien'=>$model->idRegistrasi->idPasien->idPersonal->nama_lengkap,
						'no_transaksi'=>$model->no_transaksi,
						'waktu_transaksi'=>Lib::dateInd2($model->waktu_transaksi, false,1),
						'nama_item'=>$row->nama_racikan,
						'qty'=>$row->jumlah_transaksi,
						'satuan'=>'Racikan',
						'signa'=>$row->signa,
					);
				} else {
					$data[] = array(
						'no_rm'=>Lib::MRN($model->idRegistrasi->id_pasien),
						'no_reg'=>$model->idRegistrasi->no_registrasi,
						'gudang'=>$model->gudangAsal->nama_gudang,
						'nama_pasien'=>$model->idRegistrasi->idPasien->idPersonal->nama_lengkap,
						'no_transaksi'=>$model->no_transaksi,
						'waktu_transaksi'=>Lib::dateInd2($model->waktu_transaksi, false,1),
						'nama_item'=>$row->idItem->nama_item,
						'qty'=>$row->jumlah_transaksi,
						'satuan'=>Item::getParentUnit($row->id_item)->idSatuan->nama_satuan,
						'signa'=>$row->signa,
					);
				}
			} else {
				$data[] = array(
					'no_rm'=>Lib::MRN($model->idRegistrasi->id_pasien),
					'no_reg'=>$model->idRegistrasi->no_registrasi,
					'gudang'=>$model->gudangAsal->nama_gudang,
					'nama_pasien'=>$model->idRegistrasi->idPasien->idPersonal->nama_lengkap,
					'no_transaksi'=>$model->no_transaksi,
					'waktu_transaksi'=>Lib::dateInd2($model->waktu_transaksi, false,1),
					'nama_item'=>$row->idItem->nama_item,
					'qty'=>$row->jumlah_transaksi,
					'satuan'=>Item::getParentUnit($row->id_item)->idSatuan->nama_satuan,
					'signa'=>$row->signa,
				);
			}
		}
		
		$this->renderPartial($paper,array(
		'data'=>CJSON::encode($data),
		));
	}
	
	public function actionIndex()
	{

		$trx=new ItemTransaksi('searchPasien');
		$trx->id_jenis_item_transaksi=2;
		$trx->hapus=0;
		if(isset($_GET['ItemTransaksi']))
		$trx->attributes=$_GET['ItemTransaksi'];
		if (isset($_GET['pageSize'])) {
			Yii::app()->user->setState('pageSize',(int)$_GET['pageSize']); 
			unset($_GET['pageSize']);
		}
		
		$this->render('admin',array(
			'trx'=>$trx
		));
	}
	
	public function loadModel($id)
	{
		$model=ItemTransaksi::model()->findByPk($id);
		if($model===null)
		throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}
}