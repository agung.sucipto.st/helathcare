<?php
class AccessController extends Controller
{
	/**
	* @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	* using two-column layout. See 'protected/views/layouts/column2.php'.
	*/
	public $layout='//layouts/admin/main';
	public $groupMenu=2;

	/**
	* @return array action filters
	*/
	public function filters()
	{
		return array(
		'accessControl', // perform access control for CRUD operations
		);
	}

	/**
	* Specifies the access control rules.
	* This method is used by the 'accessControl' filter.
	* @return array access control rules
	*/
	public function accessRules()
	{
		return array(
		array('allow',  // allow all users to perform 'index' and 'view' actions
		'actions'=>array('index','view'),
		'expression'=>'$user->getAuth()',
		),
		array('allow', // allow admin user to perform 'admin' and 'delete' actions
		'actions'=>array('create','update','admin','delete','BatchDelete','exportExcel','exportPdf','import','downloadTemplate'),
		'expression'=>'$user->getAuth()',
		),

		array('deny',  // deny all users
		'users'=>array('*'),
		),
		);
	}

	/**
	* Displays a particular model.
	* @param integer $id the ID of the model to be displayed
	*/
	
	public function getControllers() {
		$path = Yii::app()->controllerPath;
		$data = array();
		$files = CFileHelper::findFiles($path, array("fileTypes" => array("php")));
		foreach ($files as $file) {
			include_once $file;
			$filename = basename($file, '.php');
			if (($pos = strpos($filename, 'Controller')) > 0) {
				$class_name = $controllers[] = substr($filename, 0, $pos);
				$f = new ReflectionClass($filename);
				$methods = array();
				foreach ($f->getMethods() as $m) {
					if ($m->class == $class_name . "Controller" 
							&& preg_match('/^action+\w{2,}/', $m->name)) {
						$methods[] = str_replace("action", "", $m->name);
					}
				}
				$data[$filename] = $methods;
			}
		}
		return $data;
	}
	
	public function actionView($id)
	{
		$data=new Access('search');
		$data->unsetAttributes();  // clear any default values
		if(isset($_GET['Access']))
		$data->attributes=$_GET['Access'];
		$data->access_parent=$id;
		if (isset($_GET['pageSize'])) {
			Yii::app()->user->setState('pageSize',(int)$_GET['pageSize']); 
			unset($_GET['pageSize']);
		}
		
		
		$this->render('view',array(
		'model'=>$this->loadModel($id),'data'=>$data
		));
	}

	/**
	* Creates a new model.
	* If creation is successful, the browser will be redirected to the 'view' page.
	*/
	public function actionCreate()
	{
		$model=new Access;

		// Uncomment the following line if AJAX validation is needed
		$this->performAjaxValidation($model);

		if(isset($_POST['Access']))
		{
			$model->attributes=$_POST['Access'];
			if($model->save()){
				if($model->access_parent==0){
					$action=array("view","update","delete","create");
					$parent=0;
					foreach($action as $c){
						$new =new Access;
						
						$new->access_status=1;
						$new->access_visible=0;
						$new->access_name=ucwords($model->access_name.' '.$c);
						
						$new->access_controller=$model->access_controller;
						$new->access_action=$c;
						
						$new->access_parent=$model->access_id;
						$new->save();
					}
				}
				$this->redirect(array('view','id'=>$model->access_id));
			}
		}

		$this->render('create',array(
		'model'=>$model,
		));
	}

	/**
	* Updates a particular model.
	* If update is successful, the browser will be redirected to the 'view' page.
	* @param integer $id the ID of the model to be updated
	*/
	public function actionUpdate($id)
	{
		$model=$this->loadModel($id);

		// Uncomment the following line if AJAX validation is needed
		$this->performAjaxValidation($model);
	
		if(isset($_POST['Access']))
		{
			$model->attributes=$_POST['Access'];
			if($model->save()){
				$this->redirect(array('view','id'=>$model->access_id));
			}
		}

		$this->render('update',array(
		'model'=>$model,
		));
	}

	/**
	* Deletes a particular model.
	* If deletion is successful, the browser will be redirected to the 'admin' page.
	* @param integer $id the ID of the model to be deleted
	*/
	public function actionDelete($id)
	{
		if(Yii::app()->request->isPostRequest)
		{
		// we only allow deletion via POST request		
		$this->loadModel($id)->delete();

		// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
		if(!isset($_GET['ajax']))
		$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
		}
		else
		throw new CHttpException(400,'Invalid request. Please do not repeat this request again.');
	}

	/**
	* Lists all models.
	*/
	public function actionAdmin()
	{
		$dataProvider=new CActiveDataProvider('Access');
		$this->render('index',array(
		'dataProvider'=>$dataProvider,
		));
	}

	/**
	* Manages all models.
	*/
	public function actionIndex()
	{
		$model=new Access('search');
		$model->unsetAttributes();  // clear any default values
		if(isset($_GET['Access']))
		$model->attributes=$_GET['Access'];
		if (isset($_GET['pageSize'])) {
			Yii::app()->user->setState('pageSize',(int)$_GET['pageSize']); 
			unset($_GET['pageSize']);
		}
		$this->render('admin',array(
		'model'=>$model,
		));
	}
	
	public function loadModel($id)
	{
		$model=Access::model()->findByPk($id);
		if($model===null)
		throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}

	/**
	* Performs the AJAX validation.
	* @param CModel the model to be validated
	*/
	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='access-form')
		{
		echo CActiveForm::validate($model);
		Yii::app()->end();
		}
	}
}
