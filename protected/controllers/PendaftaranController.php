<?php

class PendaftaranController extends Controller
{
	public $layout='//layouts/admin/main';
	public $groupMenu=3;
	/**
	* @return array action filters
	*/
	public function filters()
	{
		return array(
		'accessControl', // perform access control for CRUD operations
		);
	}

	/**
	* Specifies the access control rules.
	* This method is used by the 'accessControl' filter.
	* @return array access control rules
	*/
	public function accessRules()
	{
		return array(
			array('allow',  // allow all users to perform 'index' and 'view' actions
			'actions'=>array('index'),
			'expression'=>'$user->getAuth()',
			),			
			array('deny',
                'users'=>array('*'),
            ),

		);
	}
	
	public function actionIndex()
	{
		$this->pageTitle="Daftar Kunjungan";
		$model=new Registrasi('searchPembayaran');
		$model->unsetAttributes();  // clear any default values
		if(isset($_GET['Registrasi']))
		$model->attributes=$_GET['Registrasi'];
		if (isset($_GET['pageSize'])) {
			Yii::app()->user->setState('pageSize',(int)$_GET['pageSize']); 
			unset($_GET['pageSize']);
		}
		$this->render('index',array(
		'model'=>$model,
		));
	}
}