<?php
class ReturSupplierController extends Controller
{
	/**
	* @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	* using two-column layout. See 'protected/views/layouts/column2.php'.
	*/
	public $layout='//layouts/admin/main';
	public $groupMenu=3;

	/**
	* @return array action filters
	*/
	public function filters()
	{
		return array(
		'accessControl', // perform access control for CRUD operations
		);
	}

	/**
	* Specifies the access control rules.
	* This method is used by the 'accessControl' filter.
	* @return array access control rules
	*/
	public function accessRules()
	{
		return array(
		array('allow',  // allow all users to perform 'index' and 'view' actions
		'actions'=>array('list'),
		'users'=>array('*'),
		),
		array('allow',  // allow all users to perform 'index' and 'view' actions
		'actions'=>array('index','view','print'),
		'expression'=>'$user->getAuth()',
		),
		array('allow', // allow admin user to perform 'admin' and 'delete' actions
		'actions'=>array('create','update','admin','delete'),
		'expression'=>'$user->getAuth()',
		),

		array('deny',  // deny all users
		'users'=>array('*'),
		),
		);
	}

	/**
	* Displays a particular model.
	* @param integer $id the ID of the model to be displayed
	*/
	public function actionView($id)
	{
		$this->render('view',array(
		'model'=>$this->loadModel($id),
		));
	}
	
	public function actionPrint($id)
	{
		$this->layout='//layouts/admin/realy_blank';
		$this->pageTitle="Cetak Retur Supplier";
		$this->render('print',array(
		'model'=>$this->loadModel($id),
		));
	}

	/**
	* Creates a new model.
	* If creation is successful, the browser will be redirected to the 'view' page.
	*/
	public function actionCreate($id)
	{
		$info=$this->loadModel($id);
		$model=new ItemTransaction('retur');
		$model->supplier_id=$info->supplier_id;
		$model->item_transaction_time=date("Y-m-d H:i:s");
		$model->link_transaction=$id;
		$model->warehouse_origin_id=$info->warehouse_destination_id;
		// Uncomment the following line if AJAX validation is needed
		$this->performAjaxValidation($model);

		if(isset($_POST['ItemTransaction']))
		{
			/*
			echo'<pre>';
			print_R($_POST);
			echo'</pre>';
			exit;
			//*/
			$model->attributes=$_POST['ItemTransaction'];
			$model->transaction_type_id=3;//penerimaan
			$model->user_create=Yii::app()->user->id;//penerimaan
			$model->time_create=date("Y-m-d H:i:s");//penerimaan
			$model->item_transaction_code=ItemTransaction::getCode($model->transaction_type_id,date("Y"));
			if(!empty($_POST['item'])){
				$model->item=1;
			}
			if($model->save()){
				if(!empty($_POST['item'])){
					foreach($_POST['item'] as $row){
						$list=explode("|",$row);
						$item= new ItemTransactionList;
						$item->item_transaction_id=$model->item_transaction_id;
						$item->warehouse_item_id=Item::getId($list[0],$model->	warehouse_origin_id);
						$item->item_id=$list[0];
						$item->item_big_unit_id=$list[1];
						$item->item_small_unit_id=$list[1];
						$item->big_amount=$_POST['qty'.$list[0].$list[3]];
						$item->smal_amount=1;
						$item->transaction_amount=$item->big_amount*$item->smal_amount;
						$item->transaction_amount_before=Item::getStock($list[0],$model->warehouse_origin_id);
						$item->transaction_amount_after=$item->transaction_amount_before-$item->transaction_amount;
						$item->transaction_price_per_unit=Item::getWAC($list[0]);
						$item->transaction_wacc=$item->transaction_price_per_unit;
						$item->list_id=$list[2];
						
						if($item->save()){
							Item::updateStock($list[0],$model->warehouse_origin_id,$item->transaction_amount_after);
							$up=ItemTransactionList::model()->findByPk($list[2]);
							$up->retur_amount+=$item->transaction_amount;
							$up->save();
						}
					}
					$this->redirect(array('view','id'=>$model->item_transaction_id));
				}
			}
		}

		$this->render('create',array(
		'model'=>$model,'info'=>$info
		));
	}


	/**
	* Deletes a particular model.
	* If deletion is successful, the browser will be redirected to the 'admin' page.
	* @param integer $id the ID of the model to be deleted
	*/
	public function actionDelete($id)
	{
		if(Yii::app()->request->isPostRequest)
		{
		// we only allow deletion via POST request		
		$model=$this->loadModel($id);
		$list=ItemTransactionList::model()->findAll(array("condition"=>"item_transaction_id='$id'"));
		foreach($list as $row){			
			$updateStock=Item::getStock($row->item_id,$row->itemTransaction->warehouse_origin_id)+$row->transaction_amount;
			Item::updateStock($row->item_id,$row->itemTransaction->warehouse_origin_id,$updateStock);
			$up=ItemTransactionList::model()->findByPk($row->list_id);
			$up->retur_amount-=$row->transaction_amount;
			$up->save();
		}
		$this->loadModel($id)->delete();

		// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
		if(!isset($_GET['ajax'])){
			$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
		}
		}
		else
		throw new CHttpException(400,'Invalid request. Please do not repeat this request again.');
	}

	/**
	* Manages all models.
	*/
	public function actionIndex()
	{
		$model=new ItemTransaction('search');
		$model->unsetAttributes();  // clear any default values
		$model->transaction_type_id=3;
		$model->item_transaction_time=date("m/d/Y").' - '.date("m/d/Y");
		if(isset($_GET['ItemTransaction']))
		$model->attributes=$_GET['ItemTransaction'];
		if (isset($_GET['pageSize'])) {
			Yii::app()->user->setState('pageSize',(int)$_GET['pageSize']); 
			unset($_GET['pageSize']);
		}
		$this->render('index',array(
		'model'=>$model,
		));
	}
	
	public function actionList()
	{
		$model=new ItemTransaction('search');
		$model->unsetAttributes();  // clear any default values
		$model->transaction_type_id=1;
		$model->item_transaction_time=date("m/d/Y").' - '.date("m/d/Y");
		if(isset($_GET['ItemTransaction']))
		$model->attributes=$_GET['ItemTransaction'];
		if (isset($_GET['pageSize'])) {
			Yii::app()->user->setState('pageSize',(int)$_GET['pageSize']); 
			unset($_GET['pageSize']);
		}
		$this->render('index_penerimaan',array(
		'model'=>$model,
		));
	}
	
	public function loadModel($id)
	{
		$model=ItemTransaction::model()->findByPk($id);
		if($model===null)
		throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}

	/**
	* Performs the AJAX validation.
	* @param CModel the model to be validated
	*/
	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='item-transaction-form')
		{
		echo CActiveForm::validate($model);
		Yii::app()->end();
		}
	}
}
