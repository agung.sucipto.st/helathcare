<?php
class RegistrasiController extends Controller
{
	/**
	* @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	* using two-column layout. See 'protected/views/layouts/column2.php'.
	*/
	public $layout='//layouts/admin/main';

	/**
	* @return array action filters
	*/
	public function filters()
	{
		return array(
		'accessControl', // perform access control for CRUD operations
		);
	}

	/**
	* Specifies the access control rules.
	* This method is used by the 'accessControl' filter.
	* @return array access control rules
	*/
	public function accessRules()
	{
		return array(
		array('allow',  // Display Registrasi
		'actions'=>array('index','info','soap','dataDasar','icd','hapusIcd','berkas','hapusBerkas'),
		'expression'=>'$user->getAuth()',
		),
		array('allow',  // Display Registrasi
		'actions'=>array('resep','buatResep'),
		'expression'=>'$user->getAuth()',
		),
		array('allow',  // Display Registrasi
		'actions'=>array('tindakan','tambahTindakan'),
		'expression'=>'$user->getAuth()',
		),
		array('allow',  // Display Registrasi
		'actions'=>array('alkes','tambahAlkes'),
		'expression'=>'$user->getAuth()',
		),
		
		array('allow',  // Display Registrasi
		'actions'=>array('obatAlkes','tambahObatAlkes'),
		'expression'=>'$user->getAuth()',
		),
		array('allow',  // Display Registrasi
		'actions'=>array('visite','tambahVisite'),
		'expression'=>'$user->getAuth()',
		),
		array('allow', // Create Pendaftaran
		'actions'=>array('igd','poliklinik','rawatInap','keluar','batalRegistrasi','batalKeluar'),
		'expression'=>'$user->getAuth()',
		),
		array('allow', // allow admin user to perform 'admin' and 'delete' actions
		'actions'=>array('getPenjaminPasien','getObat','getSigna','getIcd10','getDokter','cetakLabelRM','kartuPasien','slipDokter', 'editWaktu','getBed','tambahTindakanManual'),
		'users'=>array('@'),
		),

		array('deny',  // deny all users
		'users'=>array('*'),
		),
		);
	}
	
	public function actionEditWaktu($id)
	{
		$this->groupMenu=null;
		$this->layout='//layouts/admin/empty';
		$model= $this->loadModel($id);
		if(isset($_POST['Registrasi']))
		{			
			$model->waktu_registrasi=$_POST['Registrasi']['waktu_registrasi'];
			if($model->save()){
				$event="close";
			} else {
				$event = 'open';
			}
		} else {
			$event = 'open';
		}
		$this->render('edit_waktu', array('event'=>$event, 'model'=>$model));		
	}

	public function actionGetDokter()
	{
		$data=CHtml::listData(DepartemenPegawai::model()->findAll(array("with"=>array("idPegawai","idPegawai.idPersonal"),"condition"=>"idPegawai.status_aktif='Aktif' and id_departemen='". $_POST['Registrasi']['id_departemen']."'")),'id_pegawai','idPegawai.idPersonal.nama_lengkap');
		
		echo CHtml::tag('option',array('value'=>''),CHtml::encode('Pilih Dokter'),true);
		foreach($data as $value=>$name)
		{
			echo CHtml::tag('option',array('value'=>$value),CHtml::encode($name),true);
		}
	}
	
	public function actionGetBed()
	{
		$data=array();
		$bed=Bed::model()->findAll(array("with"=>array("idRuangRawatan"),"condition"=>"status_bed='Tersedia' AND kondisi_bed='Siap Huni' and idRuangRawatan.id_kelas='". $_POST['Registrasi']['id_kelas']."'"));
		foreach($bed as $row){
			$data[$row->id_bed]=$row->idRuangRawatan->nama_ruangan." ".$row->idRuangRawatan->kode_ruangan." - Bed ".$row->no_bed." (".$row->posisi_bed.")";
		}
		
		echo CHtml::tag('option',array('value'=>''),CHtml::encode('Pilih Bed'),true);
		foreach($data as $value=>$name)
		{
			echo CHtml::tag('option',array('value'=>$value),CHtml::encode($name),true);
		}
	}
	
	/**
	* Displays a particular model.
	* @param integer $id the ID of the model to be displayed
	*/
	public function actionGetPenjaminPasien($id)
	{
		$jaminan=PasienPenjamin::model()->find(array("condition"=>"id_pasien='$id' and id_penjamin='$_POST[idPenjamin]'"));
		$data=array();
		$data['id_penjamin']=$jaminan->id_penjamin;
		$data['no_kartu']=$jaminan->no_kartu;
		$data['nama_pemegang_kartu']=$jaminan->nama_pemegang_kartu;
		$data['fasilitas_jaminan']=$jaminan->fasilitas_jaminan;
		$data['id_jenis_hubungan']=$jaminan->id_jenis_hubungan;
		echo json_encode($data);
	}
	
	public function actionGetObat()
	{
		$this->layout='//layouts/admin/realy_blank';
		if(!isset($_POST['item'])){
			$request=trim($_GET['term']);
			if($request!=''){
				$criteria=new CDbCriteria;
				$criteria=new CDbCriteria;
				$criteria->condition="nama_item like '%$request%' or zat_aktif like '%$request%'";
				$model=Item::model()->findAll($criteria);
				$data=array();
				foreach($model as $get){
					$data[]=$get->nama_item;
				}
				echo json_encode($data);
			}
		}else{
			$request=trim($_POST['item']);
			$criteria=new CDbCriteria;
			$criteria->condition="nama_item like '%$request%' or zat_aktif like '%$request%'";
			$model=Item::model()->find($criteria);
			$gudang=1;//farmasi
			$data=array();
			$data['id_item']=$model->id_item;
			$data['nama_item']=$model->nama_item;
			$data['stok']=ItemGudang::getStock($model->id_item,$gudang);
			$data['satuan']=Item::getParentUnit($model->id_item)->idSatuan->nama_satuan;
			$data['harga']=Item::getPrice($model->id_item,2,1);
			
			echo json_encode($data);
		}
	}
	
	
	public function actionGetIcd10()
	{
		$this->layout='//layouts/admin/realy_blank';

			$request=trim($_GET['term']);
			if($request!=''){
				$criteria=new CDbCriteria;
				$criteria=new CDbCriteria;
				$criteria->condition="kode_icd like '%$request%' or diagnosa like '%$request%'";
				$model=Icd10::model()->findAll($criteria);
				$data=array();
				foreach($model as $get){
					$data[]=$get->kode_icd.'|'.$get->diagnosa;
				}
				echo json_encode($data);
			}
	}
	
	
	public function actionGetSigna()
	{
		$this->layout='//layouts/admin/realy_blank';
		$request=trim($_GET['term']);
		if($request!=''){
			$criteria=new CDbCriteria;
			$criteria->compare('jenis_signa',"Utama",true);
			$criteria->compare('signa',$request,true);
			$model=Signa::model()->findAll($criteria);
			$data=array();
			foreach($model as $get){
				$data[]=$get->signa;
			}
			echo json_encode($data);
		}
	}
	
	public function actionInfo($id,$idReg)
	{
		$this->groupMenu=null;
		$model=$this->loadModelPasien($id);
		$registrasi=$this->loadModel($idReg);
		
		$this->render('view',array(
			'model'=>$model,
			'registrasi'=>$registrasi,
		));
	}

	public function actionKeluar($id,$idReg,$type="OP")
	{
		$this->groupMenu=null;
		$this->layout='//layouts/admin/empty';
		$this->pageTitle='Pasien Pulang';
		$model=User::model()->findByPk(Yii::app()->user->id);
		
		$model->password=null;
		$registrasi=$this->loadModel($idReg);
		$registrasi->setScenario('discharge');
		
		if(isset($_POST['User'])){
			$model=User::model()->findByPk(Yii::app()->user->id);
			if($model->password==md5($_POST['User']['password'])){
				if(isset($_POST['Registrasi'])){
					if($_POST['Registrasi']['status_keluar']=="Rujuk External"){
						$registrasi->status_medis=$_POST['Registrasi']['status_medis'];
						$registrasi->status_keluar=$_POST['Registrasi']['status_keluar'];
						$registrasi->id_faskes_tujuan_rujuk=$_POST['Registrasi']['id_faskes_tujuan_rujuk'];
					}elseif($_POST['Registrasi']['status_keluar']=="Meninggal"){
						$registrasi->status_medis=$_POST['Registrasi']['status_medis'];
						$registrasi->status_keluar=$_POST['Registrasi']['status_keluar'];
						$registrasi->waktu_meninggal=$_POST['Registrasi']['waktu_meninggal'];
						$registrasi->lama_meninggal=$_POST['Registrasi']['lama_meninggal'];
						$registrasi->cara_meninggal=$_POST['Registrasi']['cara_meninggal'];
						$registrasi->keterangan_meninggal=$_POST['Registrasi']['keterangan_meninggal'];
					}else{
						$registrasi->status_medis=$_POST['Registrasi']['status_medis'];
						$registrasi->status_keluar=$_POST['Registrasi']['status_keluar'];
					}
					$registrasi->status_registrasi="Tutup Kunjungan";
					$registrasi->user_final=Yii::app()->user->id;
					$registrasi->waktu_tutup_registrasi=date("Y-m-d H:i:s");
					$registrasi->time_final=date("Y-m-d H:i:s");
						
					if($registrasi->save()) {
						if($registrasi->jenis_registrasi=='Rawat Inap') {
							$closeBed = BedPasien::model()->find(array("condition"=>"id_registrasi='$registrasi->id_registrasi'","order"=>"id_bed_pasien DESC","limit"=>1));
							$closeBed->waktu_keluar = date('Y-m-d H:i:s');
							$closeBed->time_update = date('Y-m-d H:i:s');
							$closeBed->user_update = Yii::app()->user->id;
							if($closeBed->save()){
								$bed = BedPasien::model()->findAll(array("condition"=>"id_registrasi='$registrasi->id_registrasi'","order"=>"waktu_masuk ASC"));
								foreach($bed as $row){
									$tStart=explode(" ",$row->waktu_masuk);
									$tEnd=explode(" ",$row->waktu_keluar);
									$a=$tStart[0];
									$b=$tEnd[0];
									$day=Lib::selisih2tanggal($a,$b);
									$date=explode("-",$a);
									$time=explode(":",$tStart[1]);
									for($i=0;$i<=$day;$i++){
										$next=date("Y-m-d",mktime(0,0,0,$date[1],$date[2]+$i,$date[0]));
										$nextTime=date("Y-m-d",mktime($time[0],$time[1],$time[2],$date[1],$date[2]+$i,$date[0]));
										$cekTagihan = DaftarTagihan::model()->find(
											array("condition"=>"id_registrasi='$registrasi->id_registrasi' AND DATE(waktu_daftar_tagihan)='$next' AND id_jenis_komponen_tagihan='5' AND id_tagihan IS NULL")
										);
										if(!$cekTagihan) {
											$cekTagihan = new DaftarTagihan;
											$cekTagihan->id_registrasi=$registrasi->id_registrasi;
											$cekTagihan->id_jenis_komponen_tagihan=5;
											$cekTagihan->id_bed_pasien=$row->id_bed_pasien;
											$cekTagihan->waktu_daftar_tagihan=$nextTime;
											$cekTagihan->deskripsi_komponen='Ruang Rawat Inap '.$row->idBed->idRuangRawatan->nama_ruangan.' '.$row->idBed->idRuangRawatan->kode_ruangan.' ( Bed '.$row->idBed->no_bed.' )';
											$cekTagihan->time_create = date('Y-m-d H:i:s');
											$cekTagihan->user_create = Yii::app()->user->id;
										}
										$cekTagihan->harga=$row->tarif_kamar;
										$cekTagihan->jumlah=1;
										$cekTagihan->jumlah_bayar=$cekTagihan->harga*$cekTagihan->jumlah;
										$cekTagihan->deskripsi_komponen='Ruang Rawat Inap '.$row->idBed->idRuangRawatan->nama_ruangan.' '.$row->idBed->idRuangRawatan->kode_ruangan.' ( Bed '.$row->idBed->no_bed.' )';
										$cekTagihan->tarif=$row->tarif_kamar;
										$cekTagihan->tarif_orig=$row->tarif_kamar;
										$cekTagihan->time_update = date('Y-m-d H:i:s');
										$cekTagihan->user_update = Yii::app()->user->id;
										$cekTagihan->save();
									}
								}
							}
						}
						$event="close";
					} else {
						$event="open";
					}
				}else{
					$event="open";
				}
			}else{
				Yii::app()->user->setFlash('error', "Otorisasi Gagal, Silahkan Masukkan Password Yang Valid !");
				$event="Open";
			}
		}else{
			$event="Open";
		}		
		
		$this->render('keluar_registrasi',array(
			'model'=>$model,
			'registrasi'=>$registrasi,
			'event'=>$event
		));
	}
	
	public function actionBatalKeluar($id)
	{
		$this->groupMenu=null;
		$this->layout='//layouts/admin/empty';
		$this->pageTitle='Batal Pulang / Keluar';
		$model=User::model()->findByPk(Yii::app()->user->id);
		$model->password=null;
		$registrasi=$this->loadModel($id);
		
		if(isset($_POST['User'])){
			$model=User::model()->findByPk(Yii::app()->user->id);
			if($model->password==md5($_POST['User']['password'])){
				$registrasi->status_registrasi="Aktif";
				$registrasi->status_medis=null;
				$registrasi->status_keluar=null;
				$registrasi->id_faskes_tujuan_rujuk=null;
				$registrasi->waktu_meninggal=null;
				$registrasi->cara_meninggal=null;
				$registrasi->lama_meninggal=null;
				$registrasi->keterangan_meninggal=null;
				$registrasi->user_final=null;
				$registrasi->waktu_tutup_registrasi=null;
				$registrasi->time_final=null;
				$registrasi->user_update=Yii::app()->user->id;
				$registrasi->time_update=date("Y-m-d H:i:s");
				if($registrasi->save()){
					if($registrasi->jenis_registrasi == 'Rawat Inap') {
						$closeBed = BedPasien::model()->find(array("condition"=>"id_registrasi='$registrasi->id_registrasi'","order"=>"id_bed_pasien DESC","limit"=>1));
						$tempKeluar =$closeBed->waktu_keluar;
						$closeBed->waktu_keluar = null;
						$closeBed->time_update = date('Y-m-d H:i:s');
						$closeBed->user_update = Yii::app()->user->id;
						if($closeBed->save()){
							$tStart=explode(" ",$closeBed->waktu_masuk);
							$tEnd=explode(" ",$tempKeluar);
							$a=$tStart[0];
							$b=$tEnd[0];
							$day=Lib::selisih2tanggal($a,$b);
							$date=explode("-",$a);
							$time=explode(":",$tStart[1]);
							for($i=0;$i<=$day;$i++){
								$next=date("Y-m-d",mktime(0,0,0,$date[1],$date[2]+$i,$date[0]));
								$cekTagihan = DaftarTagihan::model()->find(
									array("condition"=>"id_registrasi='$registrasi->id_registrasi' AND DATE(waktu_daftar_tagihan)='$next' AND id_jenis_komponen_tagihan='5' AND id_tagihan IS NULL")
								);
								if($cekTagihan) {
									$cekTagihan->delete();
								}
							}
						}
					}
					$event="close";
				}else{
					$event="Open";
				}
				
			}else{
				Yii::app()->user->setFlash('error', "Otorisasi Gagal, Silahkan Masukkan Password Yang Valid !");
				$event="Open";
			}
		}else{
			$event="Open";
		}		
		
		$this->render('batal_keluar_registrasi',array(
			'model'=>$model,
			'event'=>$event
		));
	}
	
	public function actionBatalRegistrasi($id)
	{
		$this->groupMenu=null;
		$this->layout='//layouts/admin/empty';
		$this->pageTitle='Pasien Pulang';
		$registrasi=$this->loadModel($id);
		$model=User::model()->findByPk(Yii::app()->user->id);
		$model->password=null;
		
		//Cek Data Terkatit		
		$block=0;
		$block+=ItemTransaksi::model()->count(array('condition'=>"id_registrasi='$id' AND id_jenis_item_transaksi IN (2,7,11,13) AND hapus='0'"));
		$block+=TagihanPasien::model()->count(array('condition'=>"id_registrasi='$id'"));
		
		//Cek Data Terkatit
		
		if(isset($_POST['User'])){
			$model=User::model()->findByPk(Yii::app()->user->id);
			if($model->password==md5($_POST['User']['password'])){
				$registrasi->alasan_pembatalan=$_POST['Registrasi']['alasan_pembatalan'];
				$registrasi->status_registrasi="Batal";
				$registrasi->user_cancel=Yii::app()->user->id;
				$registrasi->time_cancel=date("Y-m-d H:i:s");
				
				if($registrasi->save()){
					$daftarTagihan=DaftarTagihan::model()->findAll(array('condition'=>"id_registrasi='$id'"));
					foreach($daftarTagihan as $r) {
						$r->id_tindakan_pasien=null;
						$r->id_peralatan_pasien=null;
						$r->id_daftar_item_transaksi=null;
						$r->id_visite_dokter_pasien=null;
						$r->id_radiology_pasien_list=null;
						$r->id_laboratorium_pasien_list=null;
						$r->id_bed_pasien=null;
						$r->id_ruangan_pasien=null;
						$r->id_administrasi_pasien=null;
						$r->id_paket_pasien=null;
						$r->save();
					}
					
					$administrasi=AdministrasiPasien::model()->findAll(array('condition'=>"id_registrasi='$id'"));
					foreach($administrasi as $r){
						$r->id_daftar_tagihan=null;
						$r->save();
					}
					
					$tindakan=TindakanPasien::model()->findAll(array('condition'=>"id_registrasi='$id'"));
					foreach($tindakan as $r){
						$r->id_daftar_tagihan=null;
						$r->save();
					}
					$visit=VisiteDokterPasien::model()->findAll(array('condition'=>"id_registrasi='$id'"));
					foreach($visit as $r){
						$r->id_daftar_tagihan=null;
						$r->save();
					}
					$alat=PeralatanPasien::model()->findAll(array('condition'=>"id_registrasi='$id'"));
					foreach($alat as $r){
						$r->id_daftar_tagihan=null;
						$r->save();
					}
					$ruangan=RuanganPasien::model()->findAll(array('condition'=>"id_registrasi='$id'"));
					foreach($ruangan as $r){
						$r->id_daftar_tagihan=null;
						$r->save();
						$r->delete();
					}
					$rad=RadiologyPasienList::model()->findAll(array('with'=>array('idRadiologyPasien'),'condition'=>"idRadiologyPasien.id_registrasi='$id'"));
					foreach($rad as $r){
						$r->id_daftar_tagihan=null;
						$r->save();
						$r->delete();
					}
					
					$lab=LaboratoriumPasienList::model()->findAll(array('with'=>array('idLaboratoriumPasien'),'condition'=>"idLaboratoriumPasien.id_registrasi='$id'"));
					foreach($lab as $r){
						$r->id_daftar_tagihan=null;
						$r->save();
					}
					if($registrasi->jenis_registrasi=='Rawat Inap'){
						$bed=BedPasien::model()->find(array("condition"=>"id_registrasi='$registrasi->id_registrasi'","order"=>"id_bed_pasien DESC","limit"=>1));
						$updateStatus=Bed::model()->findByPk($bed->id_bed);
						$updateStatus->kondisi_bed='Siap Huni';
						$updateStatus->save();
						$delBed=BedPasien::model()->deleteAll(array('condition'=>"id_registrasi='$id'"));
					}
					$daftarTagihan=DaftarTagihan::model()->deleteAll(array('condition'=>"id_registrasi='$id'"));
					$administrasi=AdministrasiPasien::model()->deleteAll(array('condition'=>"id_registrasi='$id'"));
					$tindakan=TindakanPasien::model()->deleteAll(array('condition'=>"id_registrasi='$id'"));
					$visit=VisiteDokterPasien::model()->deleteAll(array('condition'=>"id_registrasi='$id'"));
					$alat=PeralatanPasien::model()->deleteAll(array('condition'=>"id_registrasi='$id'"));
					$ruangan=RuanganPasien::model()->deleteAll(array('condition'=>"id_registrasi='$id'"));
					$icd10=Icd10Pasien::model()->deleteAll(array('condition'=>"id_registrasi='$id'"));
					$resep=ResepPasien::model()->deleteAll(array('condition'=>"id_registrasi='$id'"));
					$soap=SoapPasien::model()->deleteAll(array('condition'=>"id_registrasi='$id'"));
					$tindakan=TandaVitalPasien::model()->deleteAll(array('condition'=>"id_registrasi='$id'"));
					
					$event="close";
				}else{
					Yii::app()->user->setFlash('error', "Batal Gagal !");
					$event="Open";
				}
			}else{
				Yii::app()->user->setFlash('error', "Otorisasi Gagal, Silahkan Masukkan Password Yang Valid !");
				$event="Open";
			}
		}else{
			$event="open";
		}
		
		if($block==0){
			$this->render('batal_registrasi',array(
				'model'=>$model,
				'registrasi'=>$registrasi,
				'event'=>$event,
			));
		}else{
			$this->render('block_batal_registrasi');
		}
		
	}
	
	public function actionDataDasar($id,$idReg,$idVital=null)
	{
		$this->groupMenu=null;
		$model=$this->loadModelPasien($id);
		$registrasi=$this->loadModel($idReg);
		if($idVital == null){
			$vitalSign=new TandaVitalPasien;
			$vitalSign=new TandaVitalPasien;
			$vitalSign->id_registrasi=$idReg;
			$vitalSign->user_create=Yii::app()->user->id;
			$vitalSign->time_create=date("Y-m-d H:i:s");
		} else {
			$vitalSign=TandaVitalPasien::model()->findByPk($idVital);
			if(isset($_GET['hapus'])){
				$vitalSign->delete();
				$this->redirect(array('dataDasar','id'=>$model->id_pasien,'idReg'=>$registrasi->id_registrasi));
			}
		}
		
		if(isset($_POST['TandaVitalPasien'])){
			$vitalSign->attributes=$_POST['TandaVitalPasien'];
			$vitalSign->user_update=Yii::app()->user->id;
			$vitalSign->time_update=date("Y-m-d H:i:s");
			if($vitalSign->save()){
				Yii::app()->user->setFlash('success', "Data Dasar Berhasil disimpan !");
				$vitalSign->unsetAttributes();
				$this->redirect(array('dataDasar','id'=>$model->id_pasien,'idReg'=>$registrasi->id_registrasi));
			}
		}
		
		$this->render('view',array(
			'model'=>$model,
			'registrasi'=>$registrasi,
			'vitalSign'=>$vitalSign
		));
	}
	
	public function actionResep($id,$idReg)
	{
		$this->groupMenu=null;
		$model=$this->loadModelPasien($id);
		$registrasi=$this->loadModel($idReg);

		$resep=new ResepPasien('search');
		$resep->id_registrasi=$idReg;
		
		$this->render('view',array(
			'model'=>$model,
			'registrasi'=>$registrasi,
			'resep'=>$resep
		));
	}
	
	public function actionBerkas($id,$idReg)
	{
		$this->groupMenu=null;
		$model=$this->loadModelPasien($id);
		$registrasi=$this->loadModel($idReg);

		$berkas=new BerkasPasien('search');
		$berkas->id_registrasi=$idReg;
		
		if(isset($_POST['BerkasPasien']))
		{
			$berkas->attributes=$_POST['BerkasPasien'];
			$simpanSementara=CUploadedFile::getInstance($berkas,'nama_berkas');
			if(!empty($simpanSementara)){
				$berkas->nama_berkas=Lib::simbolRemoving($simpanSementara->name).'-'.date('YmdHis').'.'.$simpanSementara->getExtensionName();
			}
			$berkas->user_create=Yii::app()->user->id;
			$berkas->time_create=date("Y-m-d H:i:s");
			if($berkas->save()){
				$dir=Yii::app()->basePath.'/../../Upload/'.Lib::MRN($registrasi->id_pasien);
				if(!empty($simpanSementara)){
					if (!is_dir($dir)){
						mkdir($dir);         
					}
					if($simpanSementara->saveAs(Yii::app()->basePath.'/../../Upload/'.Lib::MRN($registrasi->id_pasien).'/'.$berkas->nama_berkas.'')){
						Yii::app()->user->setFlash('success','Berkas Berhasil diunggah.');
					} else {	
						$berkas->delete();
						Yii::app()->user->setFlash('success','Berkas Gagal diunggah.');
					}
				}
			} else {
				Yii::app()->user->setFlash('success','Berkas Gagal diunggah.');
			}
			$this->redirect(array('berkas','id'=>$model->id_pasien,'idReg'=>$registrasi->id_registrasi));
		}
		
		$this->render('view',array(
			'model'=>$model,
			'registrasi'=>$registrasi,
			'berkas'=>$berkas
		));
	}
	
	public function actionHapusBerkas($id){
		$berkas = BerkasPasien::model()->findByPk($id);
		$temp=$berkas;
		$dir=Yii::app()->basePath.'/../../Upload/'.Lib::MRN($berkas->idRegistrasi->id_pasien);
		@unlink($dir.'/'.$berkas->nama_berkas);
		// we only allow deletion via POST request
		$berkas->delete();
		$this->redirect(array('berkas','id'=>$temp->idRegistrasi->id_pasien,'idReg'=>$temp->id_registrasi));
	}
	
	public function actionIcd($id,$idReg)
	{
		$this->groupMenu=null;
		$model=$this->loadModelPasien($id);
		$registrasi=$this->loadModel($idReg);

		if(isset($_POST['Icd10Pasien'])){
			
			$getId=explode("|",$_POST['Icd10Pasien']['id_icd10']);
			$idIcd=Icd10::model()->find(array("condition"=>"kode_icd='$getId[0]'"));
			$icdPasien=new Icd10Pasien;
			$icdPasien->id_registrasi=$registrasi->id_registrasi;
			$icdPasien->id_icd10=$idIcd->id_icd10;
			$icdPasien->jenis_kasus=$_POST['Icd10Pasien']['jenis_kasus'];
			$icdPasien->order_diagnosa=$_POST['Icd10Pasien']['order_diagnosa'];
			$icdPasien->user_create=Yii::app()->user->id;
			$icdPasien->time_create=date("Y-m-d H:i:s");
			
			$cek=Icd10Pasien::model()->count(array("condition"=>"id_registrasi='$registrasi->id_registrasi' AND id_icd10='$idIcd->id_icd10'"));
			if($cek==0){
				if($icdPasien->save()){
					echo"Sukses";
				}else{
					echo "Gagal, Periksa Kelengkapan Isian !";
				}
			}else{
				echo "ICD Telah ada pada data pasien !";
			}
			
			exit;
		}
		
		$icd=new Icd10Pasien('search');
		$icd->id_registrasi=$idReg;
		
		$this->render('view',array(
			'model'=>$model,
			'registrasi'=>$registrasi,
			'icd'=>$icd
		));
	}
	
	public function actionHapusIcd($id)
	{
		if(Yii::app()->request->isPostRequest)
		{
		// we only allow deletion via POST request		
		Icd10Pasien::model()->findByPk($id)->delete();

		// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
		if(!isset($_GET['ajax']))
		$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
		}
		else
		throw new CHttpException(400,'Invalid request. Please do not repeat this request again.');
	}
	
	public function actionBuatResep($id,$idReg,$type="OP")
	{
		$this->groupMenu=null;
		$this->layout='//layouts/admin/empty';
		$this->pageTitle='Resep Elektronik';
		$model=$this->loadModelPasien($id);
		$registrasi=$this->loadModel($idReg);
		$resep=new ResepPasien;
		$resep->id_registrasi=$idReg;
		$resep->jenis_resep=$type;
		$resep->id_dokter=$registrasi->dpjps[0]->id_dokter;
		
		if(isset($_POST['ResepPasien'])){
			$resep->no_resep=ResepPasien::getNoResep(date("Y-m-d"));
			$resep->status_resep='Belum Diproses';
			$resep->waktu_resep=date("Y-m-d H:i:s");
			$resep->user_create=Yii::app()->user->id;
			$resep->time_create=date("Y-m-d H:i:s");
			if(!empty($_POST['id_item'])){
				if($resep->save()){
					foreach($_POST['id_item'] as $row){
						$list=new ResepPasienList;
						$list->id_resep_pasien=$resep->id_resep_pasien;
						$list->id_item=$row;
						$list->jumlah=$_POST['qty'.$row];
						$list->signa=$_POST['signa'.$row];
						$list->save();
						$event="close";
					}
				}
			}
		}else{
			$event="open";
		}
		
		$this->render('resep/buat_resep',array(
			'model'=>$model,
			'registrasi'=>$registrasi,
			'resep'=>$resep,
			'event'=>$event
		));
	}
	
	public function actionObatAlkes($id,$idReg)
	{
		$this->groupMenu=null;
		$model=$this->loadModelPasien($id);
		$registrasi=$this->loadModel($idReg);

		$trx=new ItemTransaksi('searchPasien');
		$trx->id_registrasi=$idReg;
		$trx->id_jenis_item_transaksi=11;
		$trx->hapus=0;
		
		$this->render('view',array(
			'model'=>$model,
			'registrasi'=>$registrasi,
			'trx'=>$trx
		));
	}
	
	public function actionTambahObatAlkes($id,$idReg)
	{
		$this->groupMenu=null;
		$this->layout='//layouts/admin/empty';
		$this->pageTitle='Penggunaan Obat & Alkes Ruangan';
		$model=$this->loadModelPasien($id);
		$registrasi=$this->loadModel($idReg);
		$trx=new ItemTransaksi('RU');
		$trx->id_registrasi=$idReg;
		$trx->id_jenis_item_transaksi=11;
		$trx->hapus=0;
		if(isset($_POST['ItemTransaksi'])){
			$total = 0;
			$trx->attributes=$_POST['ItemTransaksi'];
			$trx->waktu_transaksi=date("Y-m-d H:i:s");
			$trx->catatan_transaksi="Penjualan Ruangan, Pasien $registrasi->no_registrasi - ".$model->idPersonal->nama_lengkap." [".Lib::MRN($model->id_pasien)."]";
			$trx->no_transaksi=ItemTransaksi::getTransactionNumber($trx->id_jenis_item_transaksi,"RU",date("Y-m-d"));
			$trx->user_create=Yii::app()->user->id;
			$trx->time_create=date("Y-m-d H:i:s");
		
			
			if(!empty($_POST['id_item'])){
				if($trx->save()){
					foreach($_POST['id_item'] as $row){
						//Menyimpan Item Transaksi
						$list=new DaftarItemTransaksi;
						$list->id_item_transaksi=$trx->id_item_transaksi;
						$list->id_item_gudang=ItemGudang::getId($row,$trx->gudang_asal);
						$list->id_item=$row;
						$list->id_item_satuan_besar=Item::getParentUnit($row)->id_item_satuan;
						$list->id_item_satuan_kecil=Item::getParentUnit($row)->id_item_satuan;
						$list->jumlah_satuan_kecil=Item::getParentUnit($row)->nilai_satuan_konversi;
						$list->jumlah_satuan_besar=$_POST['qty'.$row];
						$list->jumlah_transaksi=$list->jumlah_satuan_kecil*$list->jumlah_satuan_besar;
						$list->harga_transaksi=Item::getTarif($registrasi, $registrasi->id_kelas, $row)->tarif;
						$list->harga_wac=Item::getWAC($row);
						$list->jumlah_sebelum_transaksi=ItemGudang::getStock($row,$trx->gudang_asal);
						$list->jumlah_setelah_transaksi=$list->jumlah_sebelum_transaksi-$list->jumlah_transaksi;
						$total+=$list->harga_transaksi*$list->jumlah_transaksi;
						if($list->save()){
							//Update Stok di gudang terkait
							ItemGudang::updateStock($row,$trx->gudang_asal,$list->jumlah_setelah_transaksi);
							
							$tagihan=new DaftarTagihan;
							$tagihan->id_registrasi=$registrasi->id_registrasi;
							$tagihan->id_jenis_komponen_tagihan=4; // Room Usage
							$tagihan->id_daftar_item_transaksi=$list->id_daftar_item_transaksi;
							$tagihan->deskripsi_komponen=$list->idItem->nama_item;
							$tagihan->waktu_daftar_tagihan=date("Y-m-d H:i:s");
							$tagihan->harga=$list->harga_transaksi;
							$tagihan->tarif=$list->harga_transaksi;
							$tagihan->tarif_orig=$list->harga_transaksi;
							$tagihan->jumlah=$list->jumlah_transaksi;
							$tagihan->jumlah_bayar=$tagihan->harga*$tagihan->jumlah;
							$tagihan->user_create=Yii::app()->user->id;
							$tagihan->time_create=date("Y-m-d H:i:s");
							if($tagihan->save()) {
								$list->id_daftar_tagihan=$tagihan->id_daftar_tagihan;
								$list->save();
							}
						}
					}
					$trx->nominal_transaksi=$total;
					if($trx->save()) {
						$event="close";
					} else {
						$event="open";
					}
				}
			}
		}else{
			$event="open";
		}
		
		$this->render('obatAlkes/tambah_obat_alkes',array(
			'model'=>$model,
			'registrasi'=>$registrasi,
			'trx'=>$trx,
			'event'=>$event
		));
	}
	
	public function actionVisite($id,$idReg)
	{
		$this->groupMenu=null;
		$model=$this->loadModelPasien($id);
		$registrasi=$this->loadModel($idReg);

		$visite=new VisiteDokterPasien('search');
		$visite->id_registrasi=$idReg;
		
		$this->render('view',array(
			'model'=>$model,
			'registrasi'=>$registrasi,
			'visite'=>$visite
		));
	}
	
	
	public function actionTambahVisite($id,$idReg)
	{
		$this->groupMenu=null;
		$this->layout='//layouts/admin/empty';
		$this->pageTitle='Visite Dokter';
		$model=$this->loadModelPasien($id);
		$registrasi=$this->loadModel($idReg);
		$visite=new VisiteDokterPasien;
		$visite->id_registrasi=$idReg;
		$visite->id_kelas=$registrasi->id_kelas;
		if(isset($_POST['VisiteDokterPasien'])){
			$visite->attributes=$_POST['VisiteDokterPasien'];
			$tarifVisite=TarifVisiteDokter::getTarif($registrasi,$visite->id_kelas,$visite->id_dokter);
			if($tarifVisite) {
				$visite->id_tarif_visite_dokter=$tarifVisite->id_tarif_visite_dokter;
				$visite->share_dokter=$tarifVisite->share_dokter;
				$visite->tarif=$tarifVisite->share_dokter+$tarifVisite->share_layanan;
				$visite->user_create=Yii::app()->user->id;
				$visite->time_create=date("Y-m-d H:i:s");
				if($visite->save()){
					$tagihan=new DaftarTagihan;
					$tagihan->id_registrasi=$idReg;
					$tagihan->id_jenis_komponen_tagihan=10; // Visite
					$tagihan->id_visite_dokter_pasien=$visite->id_visite_dokter_pasien;
					$tagihan->deskripsi_komponen="Visite - ".$visite->idDokter->idPersonal->nama_lengkap;
					$tagihan->waktu_daftar_tagihan=date("Y-m-d H:i:s");
					$tagihan->harga=$visite->idTarifVisiteDokter->share_dokter+$visite->idTarifVisiteDokter->share_layanan;
					$tagihan->tarif=$tagihan->harga;
					$tagihan->tarif_orig=$tagihan->harga;
					$tagihan->jumlah=$visite->jumlah;
					$tagihan->jumlah_bayar=$tagihan->harga*$tagihan->jumlah;
					$tagihan->user_create=Yii::app()->user->id;
					$tagihan->time_create=date("Y-m-d H:i:s");
					if($tagihan->save()) {
						$visite->id_daftar_tagihan=$tagihan->id_daftar_tagihan;
						$visite->save();
						$event="close";
					}else {
						$event="open";
					}
				} else {
					Yii::app()->user->setFlash('failed','Visite Dokter Gagal Disimpan!');
					$event="open";
				}
			}else{
				Yii::app()->user->setFlash('failed','Tarif Visite Dokter Belum Di Atur !');
				$event="open";
			}
		}else{
			$event="open";
		}
		
		$this->render('visite/tambah_visite',array(
			'model'=>$model,
			'registrasi'=>$registrasi,
			'visite'=>$visite,
			'event'=>$event
		));
	}
	
	public function actionAlkes($id,$idReg)
	{
		$this->groupMenu=null;
		$model=$this->loadModelPasien($id);
		$registrasi=$this->loadModel($idReg);

		$alkes=new PeralatanPasien('search');
		$alkes->id_registrasi=$idReg;
		
		$this->render('view',array(
			'model'=>$model,
			'registrasi'=>$registrasi,
			'alkes'=>$alkes
		));
	}
	
	public function actionTambahAlkes($id,$idReg)
	{
		$this->groupMenu=null;
		$this->layout='//layouts/admin/empty';
		$this->pageTitle='Alat Kesehatan';
		$model=$this->loadModelPasien($id);
		$registrasi=$this->loadModel($idReg);
		$alkes=new PeralatanPasien;
		$alkes->id_registrasi=$idReg;
		$alkes->id_kelas=$registrasi->id_kelas;
		if(isset($_POST['PeralatanPasien'])){
			$alkes->attributes=$_POST['PeralatanPasien'];
			$alkes->user_create=Yii::app()->user->id;
			$alkes->time_create=date("Y-m-d H:i:s");
			if(!empty($_POST['PeranMedis'])){
				if($alkes->save()){
					$dokter=array();
					foreach($_POST['PeranMedis'] as $index=>$val){
						$cek=TarifPeralatanMedis::model()->findByPk($index);
						$alkMedis=new PeralatanPasienMedis;
						$alkMedis->id_peralatan_pasien=$alkes->id_peralatan_pasien;
						$alkMedis->id_pegawai=$val['medis'];
						$alkMedis->id_tarif_peralatan_medis=$index;
						$alkMedis->id_peralatan_medis=$cek->id_peralatan_medis;
						$alkMedis->jasa_medis=$cek->jasa_medis;
						$alkMedis->jasa_medis_org=$cek->jasa_medis;
						$alkMedis->gratis_biaya_jasa=$val['foc'];
						$alkMedis->save();
						$dokter[]=$alkMedis->idPegawai->idPersonal->nama_lengkap;
					}
					$tagihan=new DaftarTagihan;
					$tagihan->id_registrasi=$idReg;
					$tagihan->id_jenis_komponen_tagihan=11; // Peralatan Medis
					$tagihan->id_peralatan_pasien=$alkes->id_peralatan_pasien;
					$tagihan->deskripsi_komponen=$alkes->idPeralatan->nama_peralatan.' [ '.implode(",",$dokter).' ]';
					$tagihan->waktu_daftar_tagihan=date("Y-m-d H:i:s");
					$tagihan->harga=$alkes->idTarifPeralatan->harga_jual;
					$tagihan->tarif=$alkes->idTarifPeralatan->harga_jual;
					$tagihan->tarif_orig=$alkes->idTarifPeralatan->harga_jual;
					$tagihan->jumlah=$alkes->jumlah_penggunaan;
					$tagihan->jumlah_bayar=$tagihan->harga*$tagihan->jumlah;
					$tagihan->user_create=Yii::app()->user->id;
					$tagihan->time_create=date("Y-m-d H:i:s");
					if($tagihan->save()){
						$alkes->id_daftar_tagihan=$tagihan->id_daftar_tagihan;
						$alkes->save();
						$event="close";
					}else{
						$event="open";
					}
				} else {
					$event="open";
				}
			} else {
				$event="open";
			}
		}else{
			$event="open";
		}
		
		$this->render('alkes/tambah_alkes',array(
			'model'=>$model,
			'registrasi'=>$registrasi,
			'alkes'=>$alkes,
			'event'=>$event
		));
	}
	
	public function actionTindakan($id,$idReg)
	{
		$this->groupMenu=null;
		$model=$this->loadModelPasien($id);
		$registrasi=$this->loadModel($idReg);

		$tindakan=new TindakanPasien('search');
		$tindakan->id_registrasi=$idReg;
		
		$this->render('view',array(
			'model'=>$model,
			'registrasi'=>$registrasi,
			'tindakan'=>$tindakan
		));
	}
	
	public function actionTambahTindakan($id,$idReg)
	{
		$this->groupMenu=null;
		$this->layout='//layouts/admin/empty';
		$this->pageTitle='Tindakan';
		$model=$this->loadModelPasien($id);
		$registrasi=$this->loadModel($idReg);
		$tindakan=new TindakanPasien;
		$tindakan->id_registrasi=$idReg;
		$tindakan->id_kelas=$registrasi->id_kelas;
		if(isset($_POST['TindakanPasien'])){
			$tindakan->attributes=$_POST['TindakanPasien'];
			$tindakan->user_create=Yii::app()->user->id;
			$tindakan->time_create=date("Y-m-d H:i:s");
			if(!empty($_POST['PeranMedis'])){
				if($tindakan->save()){
					$dokter=array();
					foreach($_POST['PeranMedis'] as $index=>$val){
						$cek=TarifTindakanMedis::model()->findByPk($index);
						$tindakanMedis=new TindakanPasienMedis;
						$tindakanMedis->id_tindakan_pasien=$tindakan->id_tindakan_pasien;
						$tindakanMedis->id_pegawai=$val['medis'];
						$tindakanMedis->id_tarif_tindakan_medis=$index;
						$tindakanMedis->id_tindakan_medis=$cek->id_tindakan_medis;
						$tindakanMedis->jasa_medis=$cek->jasa_medis;
						$tindakanMedis->jasa_medis_org=$cek->jasa_medis;
						$tindakanMedis->gratis_biaya_jasa=$val['foc'];
						$tindakanMedis->save();
						$dokter[]=$tindakanMedis->idPegawai->idPersonal->nama_lengkap;
					}
					$tagihan=new DaftarTagihan;
					$tagihan->id_registrasi=$idReg;
					$tagihan->id_jenis_komponen_tagihan=2; // Tindakan
					$tagihan->id_tindakan_pasien=$tindakan->id_tindakan_pasien;
					$tagihan->deskripsi_komponen=$tindakan->idTindakan->nama_tindakan.' [ '.implode(",",$dokter).' ]';
					$tagihan->waktu_daftar_tagihan=date("Y-m-d H:i:s");
					$tagihan->harga=$tindakan->idTarifTindakan->harga_jual;
					$tagihan->tarif=$tindakan->idTarifTindakan->harga_jual;
					$tagihan->tarif_orig=$tindakan->idTarifTindakan->harga_jual;
					$tagihan->jumlah=$tindakan->jumlah_tindakan;
					$tagihan->jumlah_bayar=$tagihan->harga*$tagihan->jumlah;
					$tagihan->user_create=Yii::app()->user->id;
					$tagihan->time_create=date("Y-m-d H:i:s");
					if($tagihan->save()){
						$tindakan->id_daftar_tagihan=$tagihan->id_daftar_tagihan;
						$tindakan->save();
						$event="close";
					}else{
						$event="open";
					}
				} else {
					$event="open";
				}
			} else {
				$event="open";
			}
		}else{
			$event="open";
		}
		
		$this->render('tindakan/tambah_tindakan',array(
			'model'=>$model,
			'registrasi'=>$registrasi,
			'tindakan'=>$tindakan,
			'event'=>$event
		));
	}
	
	
	public function actionTambahTindakanManual($id,$idReg,$input = 'tarif')
	{
		$this->groupMenu=null;
		$this->layout='//layouts/admin/empty';
		$this->pageTitle='Tindakan Manual';
		$model=$this->loadModelPasien($id);
		$registrasi=$this->loadModel($idReg);
		$tindakan=new TindakanPasien('manual');
		$tindakan->id_registrasi=$idReg;
		$tindakan->id_kelas=$registrasi->id_kelas;
		$tindakan->jenis_input=$input;
		$this->ajaxTindakan($tindakan);
		if(isset($_POST['TindakanPasien'])){
			$tindakan->user_create=Yii::app()->user->id;
			$tindakan->time_create=date("Y-m-d H:i:s");
			if($input == 'tarif') {
				// create tindakan pasien dengan custom tarif
				$tindakan->attributes=$_POST['TindakanPasien'];
				if(!empty($_POST['PeranMedis'])){
					$tindakan->id_peran_medis='1';
					if($tindakan->save()){
						$dokter=array();
						foreach($_POST['PeranMedis'] as $index=>$val){
							$cek=TarifTindakanMedis::model()->findByPk($index);
							$tindakanMedis=new TindakanPasienMedis;
							$tindakanMedis->id_tindakan_pasien=$tindakan->id_tindakan_pasien;
							$tindakanMedis->id_pegawai=$val['medis'];
							$tindakanMedis->id_tarif_tindakan_medis=$index;
							$tindakanMedis->id_tindakan_medis=$cek->id_tindakan_medis;
							$tindakanMedis->jasa_medis=$val['jasa_medis'];
							$tindakanMedis->jasa_medis_org=$cek->jasa_medis;
							$tindakanMedis->gratis_biaya_jasa='Tidak';
							$tindakanMedis->save();
							$dokter[]=$tindakanMedis->idPegawai->idPersonal->nama_lengkap;
						}
						$tagihan=new DaftarTagihan;
						$tagihan->id_registrasi=$idReg;
						$tagihan->id_jenis_komponen_tagihan=2; // Tindakan
						$tagihan->id_tindakan_pasien=$tindakan->id_tindakan_pasien;
						$tagihan->deskripsi_komponen=$tindakan->idTindakan->nama_tindakan.' [ '.implode(",",$dokter).' ]';
						$tagihan->waktu_daftar_tagihan=date("Y-m-d H:i:s");
						$tagihan->harga=$_POST['TindakanPasien']['tarif_tindakan'];
						$tagihan->tarif=$_POST['TindakanPasien']['tarif_tindakan'];
						$tagihan->tarif_orig=$tindakan->idTarifTindakan->harga_jual;
						$tagihan->jumlah=$tindakan->jumlah_tindakan;
						$tagihan->jumlah_bayar=$tagihan->harga*$tagihan->jumlah;
						$tagihan->user_create=Yii::app()->user->id;
						$tagihan->time_create=date("Y-m-d H:i:s");
						if($tagihan->save()){
							$tindakan->id_daftar_tagihan=$tagihan->id_daftar_tagihan;
							$tindakan->save();
							$event="close";
						}else{
							Yii::app()->user->setFlash('error', 'Tagihan Gagal Ditambahkan !');
							$event="open";
						}
					} else {
						Yii::app()->user->setFlash('error', 'Tindakan Pasien Gagal Ditambahkan !');
						$event="open";
					}
				} else {
					Yii::app()->user->setFlash('error', 'Peran Medis Harus Diisi !');
					$event="open";
				}
				// create tindkan pasien medis dengan custom jasmed
			} else {
				$tindakan->attributes=$_POST['TindakanPasien'];
				// create tindakan
				$cekTindakan = Tindakan::model()->find(array("condition"=>"id_kelompok_tindakan='$tindakan->id_kelompok_tindakan' AND nama_tindakan='$tindakan->id_tindakan'"));
				if(!$cekTindakan) {
					$newTindakan = new Tindakan;
					$newTindakan->id_kelompok_tindakan=$tindakan->id_kelompok_tindakan;
					$newTindakan->nama_tindakan=$tindakan->id_tindakan;
					$newTindakan->status='Aktif';
					if($newTindakan->save()){
						$tindakan->id_tindakan=$newTindakan->id_tindakan;
						$newTMedis = new TindakanMedis;
						$newTMedis->id_tindakan=$newTindakan->id_tindakan;
						$newTMedis->id_peran_medis=$_POST['PeranMedis']['id'];
						$newTMedis->mandatory=1;
						$newTMedis->order_no=1;
						$newTMedis->save();
						
						$x=Kelas::model()->findAll();
						$y=KelompokTarif::model()->findAll();
						foreach($x as $kelas){
							foreach($y as $kelompok){
								$nTarifTindakan= new TarifTindakan;
								$nTarifTindakan->id_kelompok_tarif=$kelompok->id_kelompok_tarif;
								$nTarifTindakan->id_kelas=$kelas->id_kelas;
								$nTarifTindakan->id_tindakan=$newTindakan->id_tindakan;
								$nTarifTindakan->jasa_pelayanan=$_POST['TindakanPasien']['tarif_tindakan']-$_POST['PeranMedis']['jasa_medis'];
								$nTarifTindakan->harga_jual=$_POST['TindakanPasien']['tarif_tindakan'];
								if($nTarifTindakan->save()){
									$nTTMedis = new TarifTindakanMedis;
									$nTTMedis->id_tarif_tindakan=$nTarifTindakan->id_tarif_tindakan;
									$nTTMedis->id_tindakan_medis=$newTMedis->id_tindakan_medis;
									$nTTMedis->jasa_medis=$_POST['PeranMedis']['jasa_medis'];
									$nTTMedis->save();
								}
							}
						}
						
						$dokter=array();
						if($registrasi->jenis_jaminan=="UMUM"){
							$klmpk=1;
						} else {
							$klmpk=$registrasi->jaminanPasiens[0]->idPenjamin->id_kelompok_tarif;
						}
						
						$cek=TarifTindakanMedis::model()->find(array("with"=>array("idTarifTindakan","idTindakanMedis"),"condition"=>"idTarifTindakan.id_tindakan='$newTindakan->id_tindakan' and idTarifTindakan.id_kelas='$tindakan->id_kelas' and idTarifTindakan.id_kelompok_tarif='$klmpk' and idTindakanMedis.id_peran_medis='$newTMedis->id_peran_medis'"));
						
						$tp = new TindakanPasien('manual');
						$tp->attributes=array('id_tarif_tindakan'=>$cek->id_tarif_tindakan);
						$tp->jenis_input='input';
						$tp->id_kelompok_tindakan=1;
						$tp->id_kelas=$tindakan->id_kelas;
						$tp->id_tindakan=$tindakan->id_tindakan;
						$tp->id_registrasi=$idReg;
						$tp->catatan=$tindakan->catatan;
						$tp->id_peran_medis=$newTMedis->id_peran_medis;
						$tp->waktu_tindakan=$tindakan->waktu_tindakan;
						$tp->jumlah_tindakan=1;
						$tp->tarif_tindakan=$tindakan->tarif_tindakan;
						$tp->id_daftar_tagihan=null;
						$tp->user_create=Yii::app()->user->id;
						$tp->time_create=date('Y-m-d H:i:s');
						if($tp->save()){
							
							$tindakanMedis=new TindakanPasienMedis;
							$tindakanMedis->id_tindakan_pasien=$tp->id_tindakan_pasien;
							$tindakanMedis->id_pegawai=$_POST['PeranMedis']['medis'];
							$tindakanMedis->id_tarif_tindakan_medis=$cek->id_tarif_tindakan_medis;
							$tindakanMedis->id_tindakan_medis=$cek->id_tindakan_medis;
							$tindakanMedis->jasa_medis=$_POST['PeranMedis']['jasa_medis'];
							$tindakanMedis->jasa_medis_org=$cek->jasa_medis;
							$tindakanMedis->gratis_biaya_jasa='Tidak';
							$tindakanMedis->save();
							$dokter[]=$tindakanMedis->idPegawai->idPersonal->nama_lengkap;
							
							$tagihan=new DaftarTagihan;
							$tagihan->id_registrasi=$idReg;
							$tagihan->id_jenis_komponen_tagihan=2; // Tindakan
							$tagihan->id_tindakan_pasien=$tp->id_tindakan_pasien;
							$tagihan->deskripsi_komponen=$tp->idTindakan->nama_tindakan.' [ '.implode(",",$dokter).' ]';
							$tagihan->waktu_daftar_tagihan=date("Y-m-d H:i:s");
							$tagihan->harga=$_POST['TindakanPasien']['tarif_tindakan'];
							$tagihan->tarif=$_POST['TindakanPasien']['tarif_tindakan'];
							$tagihan->tarif_orig=$tp->idTarifTindakan->harga_jual;
							$tagihan->jumlah=$tp->jumlah_tindakan;
							$tagihan->jumlah_bayar=$tagihan->harga*$tagihan->jumlah;
							$tagihan->user_create=Yii::app()->user->id;
							$tagihan->time_create=date("Y-m-d H:i:s");
							if($tagihan->save()){
								$tp->id_daftar_tagihan=$tagihan->id_daftar_tagihan;
								$tp->save();
								$event="close";
							}else{
								Yii::app()->user->setFlash('error', 'Tagihan Gagal Ditambahkan ! '.CActiveForm::validate($tagihan));
								$event="open";
							}
						} else {
							;
							Yii::app()->user->setFlash('error', 'Tindakan Pasien Gagal Ditambahkan ! '.CActiveForm::validate($tp));
							$newTMedis->delete();
							$newTindakan->delete();
							$event="open";
						}
					} else {
						Yii::app()->user->setFlash('error', 'Tindakan Gagal Ditambahkan !');
						$event="open";
					}
					
				} else {
					Yii::app()->user->setFlash('error', 'Tindakan Sudah Ada, mohon gunakan fitur Input Tarif !');
					$event="open";
				}
			}
			/*
			echo'<pre>';
			print_R($_POST);
			echo'</pre>';
			exit;
			//*/
		}else{
			$event="open";
		}
		
		$this->render('tindakan/tambah_tindakan_manual',array(
			'model'=>$model,
			'registrasi'=>$registrasi,
			'tindakan'=>$tindakan,
			'event'=>$event
		));
	}
	
	
	public function actionSoap($id,$idReg)
	{
		$this->groupMenu=null;
		$model=$this->loadModelPasien($id);
		$registrasi=$this->loadModel($idReg);
		$soap=new SoapPasien;
		$soap->id_registrasi=$idReg;
		if(isset($_POST['EditSoap'])){
			$edit = SoapPasien::model()->findByPk($_POST['EditSoap']['id_soap_pasien']);
			$edit->attributes=$_POST['EditSoap'];
			$edit->user_update=Yii::app()->user->id;
			$edit->time_update=date("Y-m-d H:i:s");
			$edit->save();
		}
		if(isset($_POST['SoapPasien'])){
			$soap->attributes=$_POST['SoapPasien'];
			$soap->soap_status="Belum Dilaksanakan";
			$soap->user_create=Yii::app()->user->id;
			$soap->time_create=date("Y-m-d H:i:s");
			$soap->save();
			$soap->unsetAttributes();
		}
		
		$this->render('view',array(
			'model'=>$model,
			'registrasi'=>$registrasi,
			'soap'=>$soap
		));
	}

	/**
	* Creates a new model.
	* If creation is successful, the browser will be redirected to the 'view' page.
	*/
	public function actionIgd($id)
	{
		$registrasi=new Registrasi("igd");
		$registrasi->id_kelas=2;
		$registrasi->id_departemen=1;
		$registrasi->jenis_registrasi="IGD";
		$registrasi->status_registrasi="Aktif";
		$registrasi->user_create=Yii::app()->user->id;
		$model=$this->loadModelPasien($id);
		$this->checkScenario($model->id_pasien,$registrasi->id_departemen,'Aktif');
		
		$identitas=IdentitasPersonal::model()->find(array("condition"=>"id_personal='$model->id_personal'"));
		$jaminan=PasienPenjamin::model()->find(array("condition"=>"id_pasien='$model->id_pasien'"));
		if(!empty($jaminan)){
			$registrasi->jenis_jaminan="JAMINAN";
			$registrasi->id_penjamin=$jaminan->id_penjamin;
			$registrasi->no_kartu=$jaminan->no_kartu;
			$registrasi->nama_pemegang_kartu=$jaminan->nama_pemegang_kartu;
			$registrasi->fasilitas_jaminan=$jaminan->fasilitas_jaminan;
			$registrasi->id_jenis_hubungan=$jaminan->id_jenis_hubungan;
		}else{
			$registrasi->jenis_jaminan="UMUM";
		}
		$registrasi->id_pasien=$model->id_pasien;
		// Uncomment the following line if AJAX validation is needed
		$this->performAjaxValidation($registrasi);

		if(isset($_POST['Registrasi']))
		{
			$registrasi->attributes=$_POST['Registrasi'];
			$date=explode(" ",$registrasi->waktu_registrasi);
			$waktu_registrasi=isset($_GET['manual']) ? $date[0] : date("Y-m-d");
			$registrasi->time_create=date("Y-m-d H:i:s");
			$registrasi->no_registrasi=Registrasi::getRegNumber($waktu_registrasi);
			$registrasi->jenis_kunjungan=Registrasi::getVisitType($model->id_pasien);
			$registrasi->kode_registrasi=Registrasi::getRegCode($model->id_pasien,$registrasi->jenis_registrasi);
			if($registrasi->rujukan=="Luar"){
				$registrasi->id_faskes=$_POST['Registrasi']['id_faskes'];
				$registrasi->id_pegawai_perujuk=null;
			}elseif($registrasi->rujukan=="Dalam"){
				$registrasi->id_faskes=null;
				$registrasi->id_pegawai_perujuk=$_POST['Registrasi']['id_pegawai_perujuk'];
			}else{
				$registrasi->id_faskes=null;
				$registrasi->id_pegawai_perujuk=null;
			}
			$registrasi->jenis_kasus=($_POST['Registrasi']['jenis_kasus']=="")?null:$_POST['Registrasi']['jenis_kasus'];
			$registrasi->tipe_emergency=($_POST['Registrasi']['tipe_emergency']=="")?null:$_POST['Registrasi']['tipe_emergency'];
			$registrasi->line_emergency=($_POST['Registrasi']['line_emergency']=="")?null:$_POST['Registrasi']['line_emergency'];
			if($registrasi->save()){
				//Meregister Dokter Pasien
				if($_POST['Registrasi']['id_dokter']!=''){
					$dokter=new Dpjp;
					$dokter->id_registrasi=$registrasi->id_registrasi;
					$dokter->id_dokter=$_POST['Registrasi']['id_dokter'];
					$dokter->jenis_dpjp="UTAMA";
					$dokter->save();
				}
				
				//Meregister Penjamin Pasien Jika DIpilih Jenis Registrasi sebagai Jaminan
				if($_POST['Registrasi']['jenis_jaminan']=='JAMINAN' AND $_POST['Registrasi']['id_penjamin']!=''){
					
					//Periksa apakah pasien sudah memiliki jaminan atau belum
					$pasienPenjamin=PasienPenjamin::model()->find(array("condition"=>"id_pasien='$model->id_pasien' and id_penjamin=:id_penjamin","params"=>array("id_penjamin"=>$_POST['Registrasi']['id_penjamin'])));
					
					//jika belum tambahkan data penjamin ke pasien agar menjadi history kemudian hari
					if(empty($pasienPenjamin)){
						$pasienPenjamin=new PasienPenjamin;
						$pasienPenjamin->id_pasien=$model->id_pasien;
						$pasienPenjamin->id_penjamin=$_POST['Registrasi']['id_penjamin'];
						$pasienPenjamin->no_kartu=$_POST['Registrasi']['no_kartu'];
						$pasienPenjamin->nama_pemegang_kartu=$_POST['Registrasi']['nama_pemegang_kartu'];
						$pasienPenjamin->id_jenis_hubungan=$_POST['Registrasi']['id_jenis_hubungan'];
						$pasienPenjamin->fasilitas_jaminan=$_POST['Registrasi']['fasilitas_jaminan'];
						$pasienPenjamin->save();
					}
					
					//Menambahkan Penjamin Utama Pasien
					$jaminan=new JaminanPasien;
					$jaminan->id_registrasi=$registrasi->id_registrasi;
					$jaminan->id_penjamin=$_POST['Registrasi']['id_penjamin'];
					$jaminan->no_kartu=$_POST['Registrasi']['no_kartu'];
					$jaminan->nama_pemegang_kartu=$_POST['Registrasi']['nama_pemegang_kartu'];
					$jaminan->kelas_jaminan=$_POST['Registrasi']['fasilitas_jaminan'];
					$jaminan->hubungan_pemilik_jaminan=$_POST['Registrasi']['id_jenis_hubungan'];
					$jaminan->no_eligibilitas=$_POST['Registrasi']['no_eligibilitas'];
					$jaminan->tgl_eligibilitas=$_POST['Registrasi']['tgl_eligibilitas'];
					$jaminan->is_utama=1;
					$jaminan->model_jaminan="Seluruh Komponen Yang Dijamin";
					$jaminan->user_create=Yii::app()->user->id;
					$jaminan->time_create=date("Y-m-d H:i:s");
					
					$jaminan->save();
				}
					
				if(!isset($_GET['manual'])){
					//list administrasi
					$tarifAdm=TarifAdministrasi::getTarif($registrasi,"Rajal","Administrasi");
					
					$adm = new AdministrasiPasien;
					$adm->id_registrasi=$registrasi->id_registrasi;
					$adm->id_tarif_administrasi=$tarifAdm->id_tarif_administrasi;
					$adm->tarif=$tarifAdm->minimum;
					$adm->save();
					
					//tambah tagihan
					$tagihan=new DaftarTagihan;
					$tagihan->id_registrasi=$registrasi->id_registrasi;
					$tagihan->id_jenis_komponen_tagihan=1; // Administrasi
					$tagihan->id_administrasi_pasien=$adm->id_administrasi_pasien;
					$tagihan->deskripsi_komponen=$adm->idTarifAdministrasi->idJenisAdministrasi->nama_administrasi;
					$tagihan->waktu_daftar_tagihan=date("Y-m-d H:i:s");
					$tagihan->harga=$adm->tarif;
					$tagihan->tarif=$adm->tarif;
					$tagihan->tarif_orig=$adm->tarif;
					$tagihan->jumlah=1;
					$tagihan->jumlah_bayar=$tagihan->harga*$tagihan->jumlah;
					$tagihan->user_create=Yii::app()->user->id;
					$tagihan->time_create=date("Y-m-d H:i:s");
					$tagihan->save();
				}
				$this->redirect(array('info','id'=>$model->id_pasien,'idReg'=>$registrasi->id_registrasi));
			}
		}

		$this->render('registrasi_igd',array(
		'registrasi'=>$registrasi,'model'=>$model,'identitas'=>$identitas
		));
	}
	
	public function actionPoliklinik($id)
	{
		$registrasi=new Registrasi("igd");
		$registrasi->id_kelas=2;
		$registrasi->id_departemen=$_POST['Registrasi']['id_departemen'];
		$registrasi->jenis_registrasi="Rawat Jalan";
		$registrasi->status_registrasi="Aktif";
		$registrasi->user_create=Yii::app()->user->id;
		$model=$this->loadModelPasien($id);
		
		$identitas=IdentitasPersonal::model()->find(array("condition"=>"id_personal='$model->id_personal'"));
		$jaminan=PasienPenjamin::model()->find(array("condition"=>"id_pasien='$model->id_pasien'"));
		if(!empty($jaminan)){
			$registrasi->jenis_jaminan="JAMINAN";
			$registrasi->id_penjamin=$jaminan->id_penjamin;
			$registrasi->no_kartu=$jaminan->no_kartu;
			$registrasi->nama_pemegang_kartu=$jaminan->nama_pemegang_kartu;
			$registrasi->fasilitas_jaminan=$jaminan->fasilitas_jaminan;
			$registrasi->id_jenis_hubungan=$jaminan->id_jenis_hubungan;
		}else{
			$registrasi->jenis_jaminan="UMUM";
		}
		$registrasi->id_pasien=$model->id_pasien;
		// Uncomment the following line if AJAX validation is needed
		$this->performAjaxValidation($registrasi);

		if(isset($_POST['Registrasi']))
		{			
			$registrasi->attributes=$_POST['Registrasi'];
			
			$date=explode(" ",$registrasi->waktu_registrasi);
			$waktu_registrasi=isset($_GET['manual']) ? $date[0] : date("Y-m-d");
			$registrasi->time_create=date("Y-m-d H:i:s");
			$registrasi->no_registrasi=Registrasi::getRegNumber($waktu_registrasi);
			$registrasi->no_urut=Registrasi::getQueue($waktu_registrasi, $registrasi->id_departemen,$_POST['Registrasi']['id_dokter']);
			$registrasi->jenis_kunjungan=Registrasi::getVisitType($model->id_pasien);
			$registrasi->kode_registrasi=Registrasi::getRegCode($model->id_pasien,$registrasi->jenis_registrasi);
			if($registrasi->rujukan=="Luar"){
				$registrasi->id_faskes=$_POST['Registrasi']['id_faskes'];
				$registrasi->id_pegawai_perujuk=null;
			}elseif($registrasi->rujukan=="Dalam"){
				$registrasi->id_faskes=null;
				$registrasi->id_pegawai_perujuk=$_POST['Registrasi']['id_pegawai_perujuk'];
			}else{
				$registrasi->id_faskes=null;
				$registrasi->id_pegawai_perujuk=null;
			}
			
			if($registrasi->save()){
				//Meregister Dokter Pasien
				if($_POST['Registrasi']['id_dokter']!=''){
					$dokter=new Dpjp;
					$dokter->id_registrasi=$registrasi->id_registrasi;
					$dokter->id_dokter=$_POST['Registrasi']['id_dokter'];
					$dokter->jenis_dpjp="UTAMA";
					$dokter->save();
				}
				
				//Meregister Penjamin Pasien Jika DIpilih Jenis Registrasi sebagai Jaminan
				if($_POST['Registrasi']['jenis_jaminan']=='JAMINAN' AND $_POST['Registrasi']['id_penjamin']!=''){
					
					//Periksa apakah pasien sudah memiliki jaminan atau belum
					$pasienPenjamin=PasienPenjamin::model()->find(array("condition"=>"id_pasien='$model->id_pasien' and id_penjamin=:id_penjamin","params"=>array("id_penjamin"=>$_POST['Registrasi']['id_penjamin'])));
					
					//jika belum tambahkan data penjamin ke pasien agar menjadi history kemudian hari
					if(empty($pasienPenjamin)){
						$pasienPenjamin=new PasienPenjamin;
						$pasienPenjamin->id_pasien=$model->id_pasien;
						$pasienPenjamin->id_penjamin=$_POST['Registrasi']['id_penjamin'];
						$pasienPenjamin->no_kartu=$_POST['Registrasi']['no_kartu'];
						$pasienPenjamin->nama_pemegang_kartu=$_POST['Registrasi']['nama_pemegang_kartu'];
						$pasienPenjamin->id_jenis_hubungan=$_POST['Registrasi']['id_jenis_hubungan'];
						$pasienPenjamin->fasilitas_jaminan=$_POST['Registrasi']['fasilitas_jaminan'];
						$pasienPenjamin->save();
					}
					
					//Menambahkan Penjamin Utama Pasien
					$jaminan=new JaminanPasien;
					$jaminan->id_registrasi=$registrasi->id_registrasi;
					$jaminan->id_penjamin=$_POST['Registrasi']['id_penjamin'];
					$jaminan->no_kartu=$_POST['Registrasi']['no_kartu'];
					$jaminan->nama_pemegang_kartu=$_POST['Registrasi']['nama_pemegang_kartu'];
					$jaminan->kelas_jaminan=$_POST['Registrasi']['fasilitas_jaminan'];
					$jaminan->hubungan_pemilik_jaminan=$_POST['Registrasi']['id_jenis_hubungan'];
					$jaminan->no_eligibilitas=$_POST['Registrasi']['no_eligibilitas'];
					$jaminan->tgl_eligibilitas=$_POST['Registrasi']['tgl_eligibilitas'];
					$jaminan->is_utama=1;
					$jaminan->model_jaminan="Seluruh Komponen Yang Dijamin";
					$jaminan->user_create=Yii::app()->user->id;
					$jaminan->time_create=date("Y-m-d H:i:s");
					$jaminan->save();
				}
					
				if(!isset($_GET['manual'])){
					//list administrasi
					$tarifAdm=TarifAdministrasi::getTarif($registrasi,"Rajal","Administrasi");
					
					$adm = new AdministrasiPasien;
					$adm->id_registrasi=$registrasi->id_registrasi;
					$adm->id_tarif_administrasi=$tarifAdm->id_tarif_administrasi;
					$adm->tarif=$tarifAdm->minimum;
					$adm->save();
					
					//tambah tagihan
					$tagihan=new DaftarTagihan;
					$tagihan->id_registrasi=$registrasi->id_registrasi;
					$tagihan->id_jenis_komponen_tagihan=1; // Administrasi
					$tagihan->id_administrasi_pasien=$adm->id_administrasi_pasien;
					$tagihan->deskripsi_komponen=$adm->idTarifAdministrasi->idJenisAdministrasi->nama_administrasi;
					$tagihan->waktu_daftar_tagihan=date("Y-m-d H:i:s");
					$tagihan->harga=$adm->tarif;
					$tagihan->tarif=$adm->tarif;
					$tagihan->tarif_orig=$adm->tarif;
					$tagihan->jumlah=1;
					$tagihan->jumlah_bayar=$tagihan->harga*$tagihan->jumlah;
					$tagihan->user_create=Yii::app()->user->id;
					$tagihan->time_create=date("Y-m-d H:i:s");
					$tagihan->save();
				}
				$this->redirect(array('info','id'=>$model->id_pasien,'idReg'=>$registrasi->id_registrasi));
			}
		}

		$this->render('registrasi_poliklinik',array(
		'registrasi'=>$registrasi,'model'=>$model,'identitas'=>$identitas
		));
	}
	
	public function actionRawatInap($id)
	{
		$registrasi=new Registrasi("ranap");
		$registrasi->id_departemen=6;
		$registrasi->jenis_registrasi="Rawat Inap";
		$registrasi->status_registrasi="Aktif";
		$registrasi->user_create=Yii::app()->user->id;
		$model=$this->loadModelPasien($id);
		
		$identitas=IdentitasPersonal::model()->find(array("condition"=>"id_personal='$model->id_personal'"));
		$jaminan=PasienPenjamin::model()->find(array("condition"=>"id_pasien='$model->id_pasien'"));
		if(!empty($jaminan)){
			$registrasi->jenis_jaminan="JAMINAN";
			$registrasi->id_penjamin=$jaminan->id_penjamin;
			$registrasi->no_kartu=$jaminan->no_kartu;
			$registrasi->nama_pemegang_kartu=$jaminan->nama_pemegang_kartu;
			$registrasi->fasilitas_jaminan=$jaminan->fasilitas_jaminan;
			$registrasi->id_jenis_hubungan=$jaminan->id_jenis_hubungan;
		}else{
			$registrasi->jenis_jaminan="UMUM";
		}
		$registrasi->id_pasien=$model->id_pasien;
		// Uncomment the following line if AJAX validation is needed
		$this->performAjaxValidation($registrasi);

		if(isset($_POST['Registrasi']))
		{			
			$registrasi->attributes=$_POST['Registrasi'];
			
			$date=explode(" ",$registrasi->waktu_registrasi);
			$waktu_registrasi=isset($_GET['manual']) ? $date[0] : date("Y-m-d");
			$registrasi->time_create=date("Y-m-d H:i:s");
			$registrasi->no_registrasi=Registrasi::getRegNumber($waktu_registrasi);
			$registrasi->no_urut=Registrasi::getQueue($waktu_registrasi, $registrasi->id_departemen,$_POST['Registrasi']['id_dokter']);
			$registrasi->jenis_kunjungan=Registrasi::getVisitType($model->id_pasien);
			$registrasi->kode_registrasi=Registrasi::getRegCode($model->id_pasien,$registrasi->jenis_registrasi);
			if($registrasi->rujukan=="Luar"){
				$registrasi->id_faskes=$_POST['Registrasi']['id_faskes'];
				$registrasi->id_pegawai_perujuk=null;
			}elseif($registrasi->rujukan=="Dalam"){
				$registrasi->id_faskes=null;
				$registrasi->id_pegawai_perujuk=$_POST['Registrasi']['id_pegawai_perujuk'];
			}else{
				$registrasi->id_faskes=null;
				$registrasi->id_pegawai_perujuk=null;
			}
			
			if($registrasi->save()){
				//Meregister Dokter Pasien
				if($_POST['Registrasi']['id_dokter']!=''){
					$dokter=new Dpjp;
					$dokter->id_registrasi=$registrasi->id_registrasi;
					$dokter->id_dokter=$_POST['Registrasi']['id_dokter'];
					$dokter->jenis_dpjp="UTAMA";
					$dokter->save();
				}
				
				//Meregister Bed Pasien
				if($_POST['Registrasi']['id_bed']!=''){
					$tarifBed=TarifRuangRawatan::getTarif($registrasi,$registrasi->id_kelas);
					$updateBed=Bed::model()->findByPk($registrasi->id_bed);
					$bed=new BedPasien;
					$bed->id_registrasi=$registrasi->id_registrasi;
					$bed->id_bed=$_POST['Registrasi']['id_bed'];
					$bed->id_tarif_ruang_rawatan=$tarifBed->id_tarif_ruang_rawatan;
					$bed->waktu_masuk=date('Y-m-d H:i:s');
					$bed->jasa_paramedis=$tarifBed->jasa_paramedis;
					$bed->biaya_makan=$tarifBed->biaya_makan;
					$bed->tarif_kamar=$tarifBed->tarif_kamar;
					$bed->titipan=0;
					$bed->user_create=Yii::app()->user->id;
					$bed->time_create=date('Y-m-d H:i:s');
					if($bed->save()){
						$updateBed->kondisi_bed='Dihuni';
						$updateBed->save();
					}
				}
				
				//Meregister Penjamin Pasien Jika DIpilih Jenis Registrasi sebagai Jaminan
				if($_POST['Registrasi']['jenis_jaminan']=='JAMINAN' AND $_POST['Registrasi']['id_penjamin']!=''){
					
					//Periksa apakah pasien sudah memiliki jaminan atau belum
					$pasienPenjamin=PasienPenjamin::model()->find(array("condition"=>"id_pasien='$model->id_pasien' and id_penjamin=:id_penjamin","params"=>array("id_penjamin"=>$_POST['Registrasi']['id_penjamin'])));
					
					//jika belum tambahkan data penjamin ke pasien agar menjadi history kemudian hari
					if(empty($pasienPenjamin)){
						$pasienPenjamin=new PasienPenjamin;
						$pasienPenjamin->id_pasien=$model->id_pasien;
						$pasienPenjamin->id_penjamin=$_POST['Registrasi']['id_penjamin'];
						$pasienPenjamin->no_kartu=$_POST['Registrasi']['no_kartu'];
						$pasienPenjamin->nama_pemegang_kartu=$_POST['Registrasi']['nama_pemegang_kartu'];
						$pasienPenjamin->id_jenis_hubungan=$_POST['Registrasi']['id_jenis_hubungan'];
						$pasienPenjamin->fasilitas_jaminan=$_POST['Registrasi']['fasilitas_jaminan'];
						$pasienPenjamin->save();
					}
					
					//Menambahkan Penjamin Utama Pasien
					$jaminan=new JaminanPasien;
					$jaminan->id_registrasi=$registrasi->id_registrasi;
					$jaminan->id_penjamin=$_POST['Registrasi']['id_penjamin'];
					$jaminan->no_kartu=$_POST['Registrasi']['no_kartu'];
					$jaminan->nama_pemegang_kartu=$_POST['Registrasi']['nama_pemegang_kartu'];
					$jaminan->kelas_jaminan=$_POST['Registrasi']['fasilitas_jaminan'];
					$jaminan->hubungan_pemilik_jaminan=$_POST['Registrasi']['id_jenis_hubungan'];
					$jaminan->no_eligibilitas=$_POST['Registrasi']['no_eligibilitas'];
					$jaminan->tgl_eligibilitas=$_POST['Registrasi']['tgl_eligibilitas'];
					$jaminan->is_utama=1;
					$jaminan->model_jaminan="Seluruh Komponen Yang Dijamin";
					$jaminan->user_create=Yii::app()->user->id;
					$jaminan->time_create=date("Y-m-d H:i:s");
					$jaminan->save();
				}
				$this->redirect(array('info','id'=>$model->id_pasien,'idReg'=>$registrasi->id_registrasi));
			}
		}

		$this->render('registrasi_ranap',array(
		'registrasi'=>$registrasi,'model'=>$model,'identitas'=>$identitas
		));
	}

	/**
	* Manages all models.
	*/
	public function actionIndex()
	{
		$model=new Registrasi('search');
		$model->unsetAttributes();  // clear any default values
		if(isset($_GET['Registrasi']))
		$model->attributes=$_GET['Registrasi'];
		if (isset($_GET['pageSize'])) {
			Yii::app()->user->setState('pageSize',(int)$_GET['pageSize']); 
			unset($_GET['pageSize']);
		}
		$this->render('admin',array(
		'model'=>$model,
		));
	}
	
	public function actionCetakLabelRM($id)
	{
		$model=$this->loadModel($id);
		$paper = 'label_rm';
		if(isset($_GET['size'])){
			if($_GET['size'] == 1) {
				$paper = 'label_gelang';
			}
		}
		
		$this->renderPartial('print/'.$paper,array(
		'model'=>$model,
		));
	}
	
	public function actionKartuPasien($id)
	{
		$model=$this->loadModel($id);
		$this->renderPartial('print/kartu',array(
		'model'=>$model,
		));
	}
	
	public function actionSlipDokter($id)
	{
		$model=$this->loadModel($id);
		$this->renderPartial('print/slip_dokter',array(
		'model'=>$model,
		));
	}
	
	public function loadModel($id)
	{
		$model=Registrasi::model()->findByPk($id);
		if($model===null)
		throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}
	
	public function loadModelPasien($id)
	{
		$model=Pasien::model()->findByPk($id);
		if($model===null)
		throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}
	
	public function checkScenario($idPasien,$idDepartemen,$status)
	{
		$model=Registrasi::model()->count(array("condition"=>"id_pasien='$idPasien' and id_departemen='$idDepartemen' and status_registrasi='$status'"));
		if($model>0)
		throw new CHttpException(404,'Pasien Masih Aktif Registrasi Pada Layanan Yang Dipilih. Silahkan Coba Layanan Lain.');
		return $model;
	}
	
	
	/**
	* Performs the AJAX validation.
	* @param CModel the model to be validated
	*/
	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='registrasi-form')
		{
		echo CActiveForm::validate($model);
		Yii::app()->end();
		}
	}
	
	protected function ajaxTindakan($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='tindakan-pasien-form')
		{
		echo CActiveForm::validate($model);
		Yii::app()->end();
		}
	}
}
