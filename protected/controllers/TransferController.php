<?php
class TransferController extends Controller
{
	/**
	* @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	* using two-column layout. See 'protected/views/layouts/column2.php'.
	*/
	public $layout='//layouts/admin/main';
	public $groupMenu=13;

	/**
	* @return array action filters
	*/
	public function filters()
	{
		return array(
		'accessControl', // perform access control for CRUD operations
		);
	}

	/**
	* Specifies the access control rules.
	* This method is used by the 'accessControl' filter.
	* @return array access control rules
	*/
	public function accessRules()
	{
		return array(
		array('allow',  // allow all users to perform 'index' and 'view' actions
		'actions'=>array('index','view'),
		'expression'=>'$user->getAuth()',
		),
		array('allow', // allow admin user to perform 'admin' and 'delete' actions
		'actions'=>array('create','update','admin','delete'),
		'expression'=>'$user->getAuth()',
		),
		array('allow', // allow admin user to perform 'admin' and 'delete' actions
		'actions'=>array('print'),
		'users'=>array('*'),
		),

		array('deny',  // deny all users
		'users'=>array('*'),
		),
		);
	}

	/**
	* Displays a particular model.
	* @param integer $id the ID of the model to be displayed
	*/
	public function actionView($id)
	{
		$this->render('view',array(
		'model'=>$this->loadModel($id),
		));
	}
	
	public function actionPrint($id)
	{
		$this->layout='//layouts/admin/blank';
		$this->pageTitle="Cetak Transfer Barang";
		$this->render('print',array(
		'model'=>$this->loadModel($id),
		));
	}

	/**
	* Creates a new model.
	* If creation is successful, the browser will be redirected to the 'view' page.
	*/
	public function actionCreate()
	{
		$model=new ItemTransaksi('transfer');
		$model->waktu_transaksi=date("Y-m-d H:i:s");
		// Uncomment the following line if AJAX validation is needed
		$this->performAjaxValidation($model);

		if(isset($_POST['ItemTransaksi']))
		{
			
			$model->attributes=$_POST['ItemTransaksi'];
			$model->id_jenis_item_transaksi=3;//penerimaan
			$model->user_create=Yii::app()->user->id;//penerimaan
			$model->time_create=date("Y-m-d H:i:s");//penerimaan
			$model->status_penerimaan_transfer="Belum Diterima";//penerimaan
			$model->hapus=0;//penerimaan
			$model->no_transaksi=ItemTransaksi::getTransactionNumber(3,'MOV',date("Y-m-d"));
			if(!empty($_POST['item'])){
				$model->item=1;
			}
			if($model->save()){
				$nTotal=0;
				if(!empty($_POST['item'])){
					foreach($_POST['item'] as $row){
						$list=explode("|",$row);
						//id_item|id_item_satuan|parent|nilaiKeSatuanKecil
						$item= new DaftarItemTransaksi;
						$item->id_item_transaksi=$model->id_item_transaksi;
						$item->id_item_gudang=Item::getId($list[0],$model->gudang_asal);
						$item->id_item=$list[0];
						$item->id_item_satuan_besar=$list[1];
						$item->id_item_satuan_kecil=$list[2];
						$item->jumlah_satuan_besar=$_POST['qty'.$list[0]];
						$item->jumlah_satuan_kecil=$list[3];
						$item->jumlah_transaksi=$item->jumlah_satuan_besar*$item->jumlah_satuan_kecil;
						$item->jumlah_sebelum_transaksi=Item::getStock($list[0],$model->gudang_asal);
						$item->harga_wac=number_format(Item::getWAC($item->id_item),2);
						$item->jumlah_setelah_transaksi=$item->jumlah_sebelum_transaksi-$item->jumlah_transaksi;
						$nTotal+=$item->harga_transaksi*$item->jumlah_satuan_besar;
						if($item->save()){
							Item::updateStock($item->id_item,$model->gudang_asal,$item->jumlah_setelah_transaksi);
						}
					}
					$model->nominal_transaksi=$nTotal;
					$model->save();
					$this->redirect(array('view','id'=>$model->id_item_transaksi));
				}
			}
		}

		$this->render('create',array(
		'model'=>$model,
		));
	}

	public function actionDelete($id)
	{
		if(Yii::app()->request->isPostRequest)
		{
		// we only allow deletion via POST request		
		$current = $this->loadModel($id);
		
		if($current->hapus == '0' AND $current->status_penerimaan_transfer == 'Belum Diterima') {
			$trx=new ItemTransaksi;
			$trx->gudang_asal=$current->gudang_asal;
			$trx->gudang_tujuan=$current->gudang_tujuan;
			$trx->id_jenis_item_transaksi=16;
			$trx->waktu_transaksi=date("Y-m-d H:i:s");
			$trx->catatan_transaksi="Pembatalan Transfer Barang $current->no_transaksi";
			$trx->no_transaksi=ItemTransaksi::getTransactionNumber($trx->id_jenis_item_transaksi,"XMOV",date("Y-m-d"));
			$trx->user_create=Yii::app()->user->id;
			$trx->time_create=date("Y-m-d H:i:s");
			$trx->hapus=0;
			if($trx->save()){
				foreach($current->daftarItemTransaksis as $row){
					$list=new DaftarItemTransaksi;
					$list->id_item_transaksi=$trx->id_item_transaksi;
					$list->id_item_gudang=$row->id_item_gudang;
					$list->id_item=$row->id_item;
					$list->id_item_satuan_besar=$row->id_item_satuan_besar;
					$list->id_item_satuan_kecil=$row->id_item_satuan_kecil;
					$list->jumlah_satuan_kecil=$row->jumlah_satuan_kecil;
					$list->jumlah_satuan_besar=$row->jumlah_satuan_besar;
					$list->jumlah_transaksi=$list->jumlah_satuan_kecil*$list->jumlah_satuan_besar;
					$list->harga_transaksi=$row->harga_transaksi;
					$list->harga_wac=$row->harga_wac;
					$list->jumlah_sebelum_transaksi=ItemGudang::getStock($row->id_item,$trx->gudang_asal);
					$list->jumlah_setelah_transaksi=$list->jumlah_sebelum_transaksi+$list->jumlah_transaksi;
					$total+=$list->harga_transaksi*$list->jumlah_transaksi;
					if($list->save()){
						$updateStock=Item::getStock($list->id_item,$list->idItemTransaksi->gudang_asal)-$list->jumlah_transaksi;
						
						ItemGudang::updateStock($row->id_item,$trx->gudang_asal,$list->jumlah_setelah_transaksi);
					}
					//*/
				}
				//*
				$trx->nominal_transaksi=$total;
				$trx->save();
				$current->hapus=1;
				$current->user_delete=Yii::app()->user->id;
				$current->time_delete=date("Y-m-d H:i:s");
				$current->save();
				//*/
			}
		}

		// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
		if(!isset($_GET['ajax']))
		$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
		}
		else
		throw new CHttpException(400,'Invalid request. Please do not repeat this request again.');
	}
	/**
	* Deletes a particular model.
	* If deletion is successful, the browser will be redirected to the 'admin' page.
	* @param integer $id the ID of the model to be deleted
	*/
	

	/**
	* Manages all models.
	*/
	public function actionIndex()
	{
		$model=new ItemTransaksi('search');
		$model->unsetAttributes();  // clear any default values
		$model->id_jenis_item_transaksi=3;
		$model->hapus=0;
		//$model->waktu_transaksi=date("m/d/Y").' - '.date("m/d/Y");
		if(isset($_GET['ItemTransaksi']))
		$model->attributes=$_GET['ItemTransaksi'];
		if (isset($_GET['pageSize'])) {
			Yii::app()->user->setState('pageSize',(int)$_GET['pageSize']); 
			unset($_GET['pageSize']);
		}
		$this->render('index',array(
		'model'=>$model,
		));
	}
	
	public function loadModel($id)
	{
		$model=ItemTransaksi::model()->findByPk($id);
		if($model===null)
		throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}

	/**
	* Performs the AJAX validation.
	* @param CModel the model to be validated
	*/
	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='item-transaction-form')
		{
		echo CActiveForm::validate($model);
		Yii::app()->end();
		}
	}
}
