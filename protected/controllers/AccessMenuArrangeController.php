<?php
class AccessMenuArrangeController extends Controller
{
	/**
	* @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	* using two-column layout. See 'protected/views/layouts/column2.php'.
	*/
	public $layout='//layouts/admin/main';
	public $groupMenu=2;
	/**
	* @return array action filters
	*/
	public function filters()
	{
		return array(
		'accessControl', // perform access control for CRUD operations
		);
	}

	/**
	* Specifies the access control rules.
	* This method is used by the 'accessControl' filter.
	* @return array access control rules
	*/
	public function accessRules()
	{
		return array(
		array('allow',  // allow all users to perform 'index' and 'view' actions
		'actions'=>array('index','view'),
		'expression'=>'$user->getAuth()',
		),
		array('allow', // allow admin user to perform 'admin' and 'delete' actions
		'actions'=>array('create','update','admin','delete'),
		'expression'=>'$user->getAuth()',
		),

		array('deny',  // deny all users
		'users'=>array('*'),
		),
		);
	}


	/**
	* Creates a new model.
	* If creation is successful, the browser will be redirected to the 'view' page.
	*/
	public function actionCreate()
	{
		$model=new AccessMenuArrange;

		// Uncomment the following line if AJAX validation is needed
		$this->performAjaxValidation($model);

		if(isset($_POST['AccessMenuArrange']))
		{
			$model->attributes=$_POST['AccessMenuArrange'];
			$cek=AccessMenuArrange::model()->find(array("condition"=>"menu_arrange_parent='0'","order"=>"menu_arrange_order desc"));
			$model->menu_arrange_parent=0;
			$model->menu_arrange_order=$cek->menu_arrange_order+1;
			if($model->save()){
				$this->redirect(array('index'));
			}
		}

		$this->render('create',array(
		'model'=>$model,
		));
	}

	/**
	* Updates a particular model.
	* If update is successful, the browser will be redirected to the 'view' page.
	* @param integer $id the ID of the model to be updated
	*/
	public function actionUpdate($id)
	{
		$model=$this->loadModel($id);

		// Uncomment the following line if AJAX validation is needed
		$this->performAjaxValidation($model);
	
		if(isset($_POST['AccessMenuArrange']))
		{
			$model->attributes=$_POST['AccessMenuArrange'];
			if($model->save()){
				$this->redirect(array('index'));
			}
		}

		$this->render('update',array(
		'model'=>$model,
		));
	}

	/**
	* Deletes a particular model.
	* If deletion is successful, the browser will be redirected to the 'admin' page.
	* @param integer $id the ID of the model to be deleted
	*/
	public function actionDelete($id)
	{
		// we only allow deletion via POST request		
		$this->loadModel($id)->delete();

		$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('index'));
	}


	/**
	* Manages all models.
	*/
	public function actionIndex()
	{
		if(!isset($_GET['id'])){
			$dataGroup=AccessMenuGroup::model()->find(array('order'=>"menu_group_id ASC",'limit'=>'1'));
			$id=$dataGroup->menu_group_id;
		}else{
			$id=$_GET['id'];
			$dataGroup=AccessMenuGroup::model()->findbyPk($id);
		}
		
		if(isset($_POST['menu'])){
			if(!empty($_POST['menu'])){
				foreach($_POST['menu'] as $row){
					$model= new AccessMenuArrange;
					$cek=AccessMenuArrange::model()->find(array("condition"=>"menu_arrange_parent='0' AND menu_group_id='$id'","order"=>"menu_arrange_order desc"));
					$model->access_id=$row;
					$model->menu_group_id=$id;
					$model->menu_arrange_parent=0;
					$model->menu_arrange_order=$cek->menu_arrange_order+1;
					$model->save();
				}
			}
			
		}
		
		
		
		if(isset($_POST['AccessMenuArrange']['save'])){
			$data=json_decode($_POST['AccessMenuArrange']['save']);
			$this::MenuJsonSave($data,$parent=0);
			Yii::app()->user->setFlash('berhasil', "Proses Sukses, Data Berhasil Di Perbarui !!!");
		}

		$this->render('admin',array('dataGroup'=>$dataGroup,'id'=>$id));
	}
	
	public static function MenuJsonSave($array,$parent=0)
	{
	  $order=0;
	  foreach($array as $element)
	  {
		$order++;
		if(isset($element->children))
		{
		  $model = AccessMenuArrange::model()->findbyPk($element->id);
		  $model->menu_arrange_order=$order;
		  $model->menu_arrange_parent=$parent;
		  $model->save();
		  self::MenuJsonSave($element->children,$element->id);
		}else{
			$model = AccessMenuArrange::model()->findbyPk($element->id);
			$model->menu_arrange_order=$order;
			$model->menu_arrange_parent=$parent;
			$model->save();
		}
	  }
	  
	}

	public function loadModel($id)
	{
		$model=AccessMenuArrange::model()->findByPk($id);
		if($model===null)
		throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}

	/**
	* Performs the AJAX validation.
	* @param CModel the model to be validated
	*/
	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='access-menu-arrange-form')
		{
		echo CActiveForm::validate($model);
		Yii::app()->end();
		}
	}
}
