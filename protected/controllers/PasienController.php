<?php
class PasienController extends Controller
{
	/**
	* @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	* using two-column layout. See 'protected/views/layouts/column2.php'.
	*/
	public $layout='//layouts/admin/main';
	public $groupMenu=3;
	/**
	* @return array action filters
	*/
	public function filters()
	{
		return array(
		'accessControl', // perform access control for CRUD operations
		);
	}

	/**
	* Specifies the access control rules.
	* This method is used by the 'accessControl' filter.
	* @return array access control rules
	*/
	public function accessRules()
	{
		return array(
		array('allow',  // allow all users to perform 'index' and 'view' actions
		'actions'=>array('import'),
		'users'=>array('*'),
		),
		array('allow',  // allow all users to perform 'index' and 'view' actions
		'actions'=>array('index','view','pastVisit','medicalHistory'),
		'expression'=>'$user->getAuth()',
		),
		array('allow', // allow admin user to perform 'admin' and 'delete' actions
		'actions'=>array('create','createNotIdentify','update','admin','delete'),
		'expression'=>'$user->getAuth()',
		),

		array('deny',  // deny all users
		'users'=>array('*'),
		),
		);
	}

	/**
	* Displays a particular model.
	* @param integer $id the ID of the model to be displayed
	*/
	
	public function actionPastVisit($id)
	{
		$this->pageTitle='Past Visit';
		$this->layout='//layouts/admin/empty';
		$model=new Registrasi('searchPembayaran');
		$model->unsetAttributes();  // clear any default values
		$model->id_pasien=$id;  // clear any default values
		if(isset($_GET['Registrasi']))
		$model->attributes=$_GET['Registrasi'];
		if (isset($_GET['pageSize'])) {
			Yii::app()->user->setState('pageSize',(int)$_GET['pageSize']); 
			unset($_GET['pageSize']);
		}
		$this->render('past_visit',array(
		'model'=>$model,
		));
	}
	
	public function actionMedicalHistory($id)
	{
		
		$this->pageTitle='Riwayat Medis Pasien';
		$this->layout='//layouts/admin/empty';
		$model=Registrasi::model()->findAll(array("condition"=>"status_registrasi!='Batal' and id_pasien='$id'","order"=>"waktu_registrasi DESC"));
		$pasien=Pasien::model()->findByPk($id);
		$this->render('medical_history',array(
		'model'=>$model,'pasien'=>$pasien
		));
	}
	
	public function actionView($id)
	{
		$this->groupMenu=null;
		$model=$this->loadModel(str_replace("-","",$id)+0);
		$identitas=IdentitasPersonal::model()->find(array("condition"=>"id_personal='$model->id_personal'"));
		$kontak=KontakPersonal::model()->findAll(array("condition"=>"id_personal='$model->id_personal' and id_jenis_kontak='2'","order"=>"id_kontak_personal ASC"));
		$relation=PersonalKeluarga::model()->find(array("condition"=>"id_personal_to='$model->id_personal' OR id_personal_from='$model->id_personal'"));
		if($relation->id_personal_from==$model->id_personal){
			$rel=$relation->id_personal_to;
		}elseif($relation->id_personal_to==$model->id_personal){
			$rel=$relation->id_personal_from;
		}
		$keluarga=Personal::model()->findByPk($rel);
		$noHpKeluarga=KontakPersonal::model()->find(array("condition"=>"id_personal='$keluarga->id_personal'"));
		$jaminan=PasienPenjamin::model()->find(array("condition"=>"id_pasien='$model->id_pasien'"));
		$this->render('view',array(
		'model'=>$model,'identitas'=>$identitas,'kontak'=>$kontak,'keluarga'=>$keluarga,'noHpKeluarga'=>$noHpKeluarga,'relation'=>$relation,'jaminan'=>$jaminan
		));
	}

	/**
	* Creates a new model.
	* If creation is successful, the browser will be redirected to the 'view' page.
	*/
	public function actionCreate()
	{
		$model=new Pasien('teridentifikasi');
		$model->identifikasi='Ya';
		$model->id_negara='ID';
		if(isset($_GET['idPerson'])){
			$personal=Personal::model()->findbyPk($_GET['idPerson']);
			if(count($personal)==0){
				throw new CHttpException(404,'Data Personal Tidak DiTemukan !');
			}
			$model->nama_lengkap=$personal->nama_lengkap;
			$model->tempat_lahir=$personal->tempat_lahir;
			$model->tanggal_lahir=$personal->tanggal_lahir;
			$model->jenis_kelamin=$personal->jenis_kelamin;
			$model->status_perkawinan=$personal->status_perkawinan;
			$model->id_agama=$personal->id_agama;
			$model->id_pendidikan=$personal->id_pendidikan;
			$model->id_pekerjaan=$personal->id_pekerjaan;
			$model->alamat_domisili=$personal->alamat_domisili;
			$model->alamat_sementara=$personal->alamat_sementara;
			$model->id_negara="ID";
			$model->id_provinsi=$personal->id_provinsi;
			$model->id_kabupaten=$personal->id_provinsi;
			$model->golongan_darah=$personal->golongan_darah;
			$model->resus=$personal->resus;
			$model->alergi=$personal->alergi;
			
			$kontak=KontakPersonal::model()->findAll(array("condition"=>"id_personal='$personal->id_personal' and id_jenis_kontak='2'","order"=>"id_kontak_personal ASC"));
			$relation=PersonalKeluarga::model()->find(array("condition"=>"id_personal_from='$personal->id_personal'"));
			$keluarga=Personal::model()->findByPk($relation->id_personal_to);
			$noHpKeluarga=KontakPersonal::model()->find(array("condition"=>"id_personal='$keluarga->id_personal'"));
			$i=0;
			foreach($kontak as $row){
				$i++;
				$var="kontak$i";
				$model->$var=$row->kontak;
			}
			$model->nama_keluarga=$keluarga->nama_lengkap;
			$model->alamat_keluarga=$keluarga->alamat_domisili;
			$model->jenis_kelamin_keluarga=$keluarga->jenis_kelamin;
			$model->kontak3=$noHpKeluarga->kontak;
			$model->hubungan_pasien=$relation->id_jenis_hubungan_from_to;
			$model->hubungan_ke_pasien=$relation->id_jenis_hubungan_to_from;
			if(!empty($kontak)){
				$model->insertNoHP=0;
			}else{
				$model->insertNoHP=1;
			}
			
			if(!empty($keluarga)){
				$model->insertKeluarga=0;
			}else{
				$model->insertKeluarga=1;
			}
		}else{
			$model->insertNoHP=1;
			$model->insertKeluarga=1;
		}
		// Uncomment the following line if AJAX validation is needed
		$this->performAjaxValidation($model);
	
		if(isset($_POST['Pasien']))
		{			
			$model->attributes=$_POST['Pasien'];
			$model->user_create=Yii::app()->user->id;
			$model->time_create=date("Y-m-d H:i:s");
			
			CActiveForm::validate($model);
			if(isset($_GET['idPerson'])){
				$personal=Personal::model()->findbyPk($_GET['idPerson']);
				$personal->user_update=Yii::app()->user->id;
				$personal->time_update=date("Y-m-d H:i:s");
			}else{
				$personal=Personal::model()->find(array("condition"=>"nama_lengkap='".$_POST['Pasien']['nama_lengkap']."' and tempat_lahir='".$_POST['Pasien']['tempat_lahir']."' and tanggal_lahir='".$_POST['Pasien']['tanggal_lahir']."'"));
				
				if(empty($personal)){
					$personal=new Personal;
					$personal->user_create=Yii::app()->user->id;
					$personal->time_create=date("Y-m-d H:i:s");
				}else{
					$personal->user_update=Yii::app()->user->id;
					$personal->time_update=date("Y-m-d H:i:s");
				}
			}
			
			$personal->nama_lengkap=$_POST['Pasien']['nama_lengkap'];
			$personal->tempat_lahir=$_POST['Pasien']['tempat_lahir'];
			$personal->tanggal_lahir=$_POST['Pasien']['tanggal_lahir'];
			$personal->jenis_kelamin=$_POST['Pasien']['jenis_kelamin'];
			$personal->status_perkawinan=$_POST['Pasien']['status_perkawinan'];
			$personal->alamat_domisili=$_POST['Pasien']['alamat_domisili'];
			$personal->alamat_sementara=$_POST['Pasien']['alamat_sementara'];
			$personal->id_agama=($_POST['Pasien']['id_agama']=='')?null:$_POST['Pasien']['id_agama'];
			$personal->id_pendidikan=($_POST['Pasien']['id_pendidikan']=='')?null:$_POST['Pasien']['id_pendidikan'];
			$personal->id_pekerjaan=($_POST['Pasien']['id_pekerjaan']=='')?null:$_POST['Pasien']['id_pekerjaan'];
			$personal->id_negara=($_POST['Pasien']['id_negara']=='')?null:$_POST['Pasien']['id_negara'];
			$personal->id_provinsi=($_POST['Pasien']['id_provinsi']=='')?null:$_POST['Pasien']['id_provinsi'];
			$personal->id_kabupaten=($_POST['Pasien']['id_kabupaten']=='')?null:$_POST['Pasien']['id_kabupaten'];
			$personal->id_kecamatan=($_POST['Pasien']['id_kecamatan']=='')?null:$_POST['Pasien']['id_kecamatan'];
			$personal->golongan_darah=($_POST['Pasien']['id_kecamatan']=='')?null:$_POST['Pasien']['golongan_darah'];
			$personal->resus=($_POST['Pasien']['resus']=='')?null:$_POST['Pasien']['resus'];
			$personal->alergi=($_POST['Pasien']['alergi']=='')?null:$_POST['Pasien']['alergi'];
			if($personal->save()){
				$identitas=new IdentitasPersonal;
				$identitas->id_personal=$personal->id_personal;
				$identitas->id_jenis_identitas=$_POST['Pasien']['id_jenis_identitas'];
				$identitas->no_identitas=$_POST['Pasien']['no_identitas'];
				$identitas->save();
				
				if($model->insertNoHP==1){
				$noHpPasien1=new KontakPersonal;
				$noHpPasien1->id_jenis_kontak=2;
				$noHpPasien1->id_personal=$personal->id_personal;
				$noHpPasien1->kontak=$_POST['Pasien']['kontak1'];
				$noHpPasien1->save();
				}
				
				if(!empty($_POST['Pasien']['kontak2'])){
					$noHpPasien2=new KontakPersonal;
					$noHpPasien2->id_jenis_kontak=2;
					$noHpPasien2->id_personal=$personal->id_personal;
					$noHpPasien2->kontak=$_POST['Pasien']['kontak2'];
					$noHpPasien2->save();
				}
				
				if($model->insertKeluarga==1){
					if($_POST['Pasien']['nama_keluarga']!=''){
						
						$keluarga=new Personal;
						$keluarga->nama_lengkap=$_POST['Pasien']['nama_keluarga'];
						$keluarga->jenis_kelamin=$_POST['Pasien']['jenis_kelamin_keluarga'];
						$keluarga->alamat_domisili=$_POST['Pasien']['alamat_keluarga'];
						$keluarga->user_create=Yii::app()->user->id;
						$keluarga->time_create=date("Y-m-d H:i:s");
						if($keluarga->save()){
							$noHpKeluarga=new KontakPersonal;
							$noHpKeluarga->id_jenis_kontak=2;
							$noHpKeluarga->id_personal=$keluarga->id_personal;
							$noHpKeluarga->kontak=$_POST['Pasien']['kontak3'];
							$noHpKeluarga->save();
							
							$relation=new PersonalKeluarga;
							$relation->id_personal_from=$keluarga->id_personal;
							$relation->id_personal_to=$personal->id_personal;
							$relation->id_jenis_hubungan_to_from=$_POST['Pasien']['hubungan_pasien'];
							$relation->id_jenis_hubungan_from_to=$_POST['Pasien']['hubungan_ke_pasien'];
							$relation->save();	
						}
					}
				}
				
				$model->id_personal=$personal->id_personal;
				if($model->save()){
					if(!empty($_POST['Pasien']['id_penjamin'])){
						$jaminan=new PasienPenjamin;
						$jaminan->id_pasien=$model->id_pasien;
						$jaminan->id_penjamin=$_POST['Pasien']['id_penjamin'];
						$jaminan->no_kartu=$_POST['Pasien']['no_kartu'];
						$jaminan->nama_pemegang_kartu=$_POST['Pasien']['nama_pemegang_kartu'];
						$jaminan->id_jenis_hubungan=$_POST['Pasien']['id_jenis_hubungan'];
						$jaminan->fasilitas_jaminan=$_POST['Pasien']['fasilitas_jaminan'];
						$jaminan->save();
					}
					$this->redirect(array('view','id'=>$model->id_pasien));
				}else{
					if($model->insertKeluarga!=0){
						if(!$noHpKeluarga->isNewRecord){
							$noHpKeluarga->delete();
						}
						if(!$relation->isNewRecord){
							$relation->delete();
						}
						if(!$keluarga->isNewRecord){
							$keluarga->delete();
						}
					}
					if($model->insertNoHP!=0){
						if(!$noHpPasien1->isNewRecord){
							$noHpPasien1->delete();
						}
					}
					if(!$identitas->isNewRecord){
						$identitas->delete();
					}
					if(!empty($_POST['Pasien']['kontak2'])){
						if(!$noHpPasien2->isNewRecord){
							$noHpPasien2->delete();
						}
					}
					if(!$identitas->isNewRecord){
						$identitas->delete();
					}
					//$personal->delete();
				}
			}
		}
		
		$this->render('create',array(
		'model'=>$model,
		));
	}
	
	public function actionCreateNotIdentify()
	{
		$model=new Pasien;

		// Uncomment the following line if AJAX validation is needed
		$this->performAjaxValidation($model);

		if(isset($_POST['Pasien']))
		{
			$model->attributes=$_POST['Pasien'];
			if($model->save()){
				$this->redirect(array('view','id'=>$model->id_pasien));
			}
		}

		$this->render('create',array(
		'model'=>$model,
		));
	}

	/**
	* Updates a particular model.
	* If update is successful, the browser will be redirected to the 'view' page.
	* @param integer $id the ID of the model to be updated
	*/
	public function actionUpdate($id)
	{
		$this->groupMenu=null;
		$model=$this->loadModel($id);
		$personal=Personal::model()->findbyPk($model->id_personal);
		$identitas=IdentitasPersonal::model()->find(array("condition"=>"id_personal='$model->id_personal'"));
		$kontak=KontakPersonal::model()->findAll(array("condition"=>"id_personal='$model->id_personal' and id_jenis_kontak='2'","order"=>"id_kontak_personal ASC"));
		$jaminan=PasienPenjamin::model()->find(array("condition"=>"id_pasien='$model->id_pasien'"));
		
		if($kontak){
			$i=0;
			foreach($kontak as $row){
				$i++;
				$var="kontak$i";
				$model->$var=$row->kontak;
			}
		} else {
			$model->kontak1='';
			$model->kontak2='';
		}
		$model->nama_lengkap=$personal->nama_lengkap;
		$model->tempat_lahir=$personal->tempat_lahir;
		$model->tanggal_lahir=$personal->tanggal_lahir;
		$model->jenis_kelamin=$personal->jenis_kelamin;
		$model->status_perkawinan=$personal->status_perkawinan;
		$model->id_agama=$personal->id_agama;
		$model->id_pendidikan=$personal->id_pendidikan;
		$model->id_pekerjaan=$personal->id_pekerjaan;
		$model->alamat_domisili=$personal->alamat_domisili;
		$model->alamat_sementara=$personal->alamat_sementara;
		$model->id_negara=$personal->id_negara;
		$model->id_provinsi=$personal->id_provinsi;
		$model->id_kabupaten=$personal->id_kabupaten;
		$model->id_kecamatan=$personal->id_kecamatan;
		$model->golongan_darah=$personal->golongan_darah;
		$model->resus=$personal->resus;
		$model->alergi=$personal->alergi;
		$model->id_jenis_identitas=$identitas->id_jenis_identitas;
		$model->no_identitas=$identitas->no_identitas;
		$model->id_penjamin=$jaminan->id_penjamin;
		$model->no_kartu=$jaminan->no_kartu;
		$model->nama_pemegang_kartu=$jaminan->nama_pemegang_kartu;
		$model->id_jenis_hubungan=$jaminan->id_jenis_hubungan;
		$model->fasilitas_jaminan=$jaminan->fasilitas_jaminan;
		
		// Uncomment the following line if AJAX validation is needed
		$this->performAjaxValidation($model);
	
		if(isset($_POST['Pasien']))
		{
			$model->attributes=$_POST['Pasien'];
			$model->user_update=Yii::app()->user->id;
			$model->time_update=date("Y-m-d H:i:s");
			
			CActiveForm::validate($model);
			
			$personal->nama_lengkap=$_POST['Pasien']['nama_lengkap'];
			$personal->tempat_lahir=$_POST['Pasien']['tempat_lahir'];
			$personal->tanggal_lahir=$_POST['Pasien']['tanggal_lahir'];
			$personal->jenis_kelamin=$_POST['Pasien']['jenis_kelamin'];
			$personal->status_perkawinan=$_POST['Pasien']['status_perkawinan'];
			$personal->alamat_domisili=$_POST['Pasien']['alamat_domisili'];
			$personal->alamat_sementara=$_POST['Pasien']['alamat_sementara'];
			$personal->id_agama=($_POST['Pasien']['id_agama']=='')?null:$_POST['Pasien']['id_agama'];
			$personal->id_pendidikan=($_POST['Pasien']['id_pendidikan']=='')?null:$_POST['Pasien']['id_pendidikan'];
			$personal->id_pekerjaan=($_POST['Pasien']['id_pekerjaan']=='')?null:$_POST['Pasien']['id_pekerjaan'];
			$personal->id_negara=($_POST['Pasien']['id_negara']=='')?null:$_POST['Pasien']['id_negara'];
			$personal->id_provinsi=($_POST['Pasien']['id_provinsi']=='')?null:$_POST['Pasien']['id_provinsi'];
			$personal->id_kabupaten=($_POST['Pasien']['id_kabupaten']=='')?null:$_POST['Pasien']['id_kabupaten'];
			$personal->id_kecamatan=($_POST['Pasien']['id_kecamatan']=='')?null:$_POST['Pasien']['id_kecamatan'];
			$personal->golongan_darah=($_POST['Pasien']['id_kecamatan']=='')?null:$_POST['Pasien']['golongan_darah'];
			$personal->resus=($_POST['Pasien']['resus']=='')?null:$_POST['Pasien']['resus'];
			$personal->alergi=($_POST['Pasien']['alergi']=='')?null:$_POST['Pasien']['alergi'];
			$personal->save();
			
			if(!$identitas){
				$identitas=new IdentitasPersonal;
				$identitas->id_personal=$personal->id_personal;
			}
			$identitas->id_jenis_identitas=$_POST['Pasien']['id_jenis_identitas'];
			$identitas->no_identitas=$_POST['Pasien']['no_identitas'];
			$identitas->save();
		
			if(count($jaminan)==1){
				if(!empty($_POST['Pasien']['id_penjamin'])){
					$jaminan->id_penjamin=$_POST['Pasien']['id_penjamin'];
					$jaminan->no_kartu=$_POST['Pasien']['no_kartu'];
					$jaminan->nama_pemegang_kartu=$_POST['Pasien']['nama_pemegang_kartu'];
					$jaminan->id_jenis_hubungan=$_POST['Pasien']['id_jenis_hubungan'];
					$jaminan->fasilitas_jaminan=$_POST['Pasien']['fasilitas_jaminan'];
					$jaminan->save();
				}else{
					$jaminan->delete();
				}
			}else{
				$jaminan=new PasienPenjamin;
				$jaminan->id_pasien=$model->id_pasien;
				$jaminan->id_penjamin=$_POST['Pasien']['id_penjamin'];
				$jaminan->no_kartu=$_POST['Pasien']['no_kartu'];
				$jaminan->nama_pemegang_kartu=$_POST['Pasien']['nama_pemegang_kartu'];
				$jaminan->id_jenis_hubungan=$_POST['Pasien']['id_jenis_hubungan'];
				$jaminan->fasilitas_jaminan=$_POST['Pasien']['fasilitas_jaminan'];
				$jaminan->save();
			}
			
			if(count($kontak)>0){
				$i=0;
				foreach($kontak as $row){
					$i++;
					if($_POST['Pasien']['kontak'.$i]!=""){
						$row->kontak=$_POST['Pasien']['kontak'.$i];
						$row->save();
					}else{
						$row->delete();
					}
				}
			} else {
				if($_POST['Pasien']['kontak1']!=""){
					$noHpPasien1=new KontakPersonal;
					$noHpPasien1->id_jenis_kontak=2;
					$noHpPasien1->id_personal=$personal->id_personal;
					$noHpPasien1->kontak=$_POST['Pasien']['kontak1'];
					$noHpPasien1->save();
				}
				if($_POST['Pasien']['kontak2']!=""){
					$noHpPasien1=new KontakPersonal;
					$noHpPasien1->id_jenis_kontak=2;
					$noHpPasien1->id_personal=$personal->id_personal;
					$noHpPasien1->kontak=$_POST['Pasien']['kontak2'];
					$noHpPasien1->save();
				}
			}
			
			if(count($kontak)==1){
				if(!empty($_POST['Pasien']['kontak2'])){
					$noHpPasien2=new KontakPersonal;
					$noHpPasien2->id_jenis_kontak=2;
					$noHpPasien2->id_personal=$personal->id_personal;
					$noHpPasien2->kontak=$_POST['Pasien']['kontak2'];
					$noHpPasien2->save();
				}
			}
			if($model->save()){
				$this->redirect(array('view','id'=>$model->id_pasien));
			}
		}

		$this->render('update',array(
		'model'=>$model,
		));
	}

	/**
	* Deletes a particular model.
	* If deletion is successful, the browser will be redirected to the 'admin' page.
	* @param integer $id the ID of the model to be deleted
	*/
	public function actionDelete($id)
	{
		if(Yii::app()->request->isPostRequest)
		{
		// we only allow deletion via POST request		
		$model=$this->loadModel($id);
		
		//$identitas=IdentitasPersonal::model()->deleteAll(array("condition"=>"id_personal='$model->id_personal'"));
		//$kontak=KontakPersonal::model()->deleteAll(array("condition"=>"id_personal='$model->id_personal' and id_jenis_kontak='2'"));
		$id=$model->id_personal;
		$model->delete();
		$personal=Personal::model()->findbyPk($id)->delete();
		
		
		// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
		if(!isset($_GET['ajax']))
		$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
		}
		else
		throw new CHttpException(400,'Invalid request. Please do not repeat this request again.');
	}

	/**
	* Manages all models.
	*/
	public function actionIndex()
	{
		$model=new Pasien('search');
		$model->unsetAttributes();  // clear any default values
		if(isset($_GET['Pasien']))
		$model->attributes=$_GET['Pasien'];
		if (isset($_GET['pageSize'])) {
			Yii::app()->user->setState('pageSize',(int)$_GET['pageSize']); 
			unset($_GET['pageSize']);
		}
		$this->render('admin',array(
		'model'=>$model,
		));
	}
	
	public function actionImport()
	{
		ini_set("memory_limit","8056M");
		set_time_limit(100000);
		if(isset($_POST['Pasien']))
		{
		$model=new Pasien;
		$fileUpload=CUploadedFile::getInstance($model,'import');
		$nama=date('YmdHis').'_'.$fileUpload;
		$path=Yii::app()->basePath . '/../temp_excel/'.$nama.'';
		$fileUpload->saveAs($path);
			
		Yii::import('ext.phpexcelreader.JPhpExcelReader');
		$data=new JPhpExcelReader($path);
		$baris=$data->sheets[0]['numRows'];
		$kolom=$data->sheets[0]['numCols'];
		$sukses=0;
		$gagal=0;
		$dataGagal=[];
		$kontak=array();
		$pasien=array();
		for($i=2;$i<=$baris; $i++){
			
			$model=new Personal;
			$model->nama_lengkap=isset($data->sheets[0]['cells'][$i][3]) ? strtoupper($data->sheets[0]['cells'][$i][3]) : '';
			$model->jenis_kelamin=isset($data->sheets[0]['cells'][$i][4]) ? $data->sheets[0]['cells'][$i][4] : '';
			$model->tanggal_lahir=isset($data->sheets[0]['cells'][$i][5]) ? $data->sheets[0]['cells'][$i][5] : '';
			$model->alamat_domisili=isset($data->sheets[0]['cells'][$i][6]) ? $data->sheets[0]['cells'][$i][6] : '';
			$model->user_create=Yii::app()->user->id;
			$model->time_create=date("Y-m-d H:i:s");
			$cek = Pasien::model()->findByPk($data->sheets[0]['cells'][$i][1]);
			if(count($cek)==0){
				if($model->save()){
					$kontak[]="('2','".$model->id_personal."','".(isset($data->sheets[0]['cells'][$i][7]) ? trim($data->sheets[0]['cells'][$i][7]) : '')."','1')";
					$pasien[]="('".$data->sheets[0]['cells'][$i][1]."','".$model->id_personal."','Ya','".(isset($data->sheets[0]['cells'][$i][2]) ? trim($data->sheets[0]['cells'][$i][2]) : '')."','".date("Y-m-d H:i:s")."','Tidak Aktif','".Yii::app()->user->id."','".date("Y-m-d H:i:s")."')";
					/*
					$modelKontak=new KontakPersonal;
					$modelKontak->id_jenis_kontak=2;
					$modelKontak->id_personal=$model->id_personal;
					$modelKontak->kontak=isset($data->sheets[0]['cells'][$i][7]) ? $data->sheets[0]['cells'][$i][7] : '';
					$modelKontak->status_aktif=1;
					
					$modelPasien=new Pasien;
					$modelPasien->id_pasien=$data->sheets[0]['cells'][$i][1];
					$modelPasien->id_personal=$model->id_personal;
					$modelPasien->identifikasi='Ya';
					$modelPasien->panggilan=isset($data->sheets[0]['cells'][$i][2]) ? $data->sheets[0]['cells'][$i][2] : '';
					$modelPasien->waktu_daftar=date("Y-m-d H:i:s");
					$modelPasien->status_rekam_medis="Tidak Aktif";
					$modelPasien->user_create=Yii::app()->user->id;
					$modelPasien->time_create=date("Y-m-d H:i:s");
					if($modelKontak->save() && $modelPasien->save()){
						$sukses++;
					} else {
						$model->delete();
						if(!$modelKontak->isNewRecord){
							$modelKontak->delete();
						}
						if(!$modelPasien->isNewRecord){
							$modelPasien->delete();
						}
						$dataGagal[]=$data->sheets[0]['cells'][$i][1];
						$gagal++;
					}
					*/
				}
			} else {
				$gagal++;
				$dataGagal[]=$data->sheets[0]['cells'][$i][1];
			}
		}
		
		
		$sqlKontak = 'INSERT INTO kontak_personal (id_jenis_kontak, id_personal, kontak, status_aktif) VALUES '.implode(",",$kontak);
		$sqlPasien = 'INSERT INTO pasien (id_pasien, id_personal, identifikasi, panggilan, waktu_daftar, status_rekam_medis, user_create, time_create) VALUES '.implode(",",$pasien);

		$commandKontak = Yii::app()->db->createCommand($sqlKontak);
		$commandKontak->execute();
		$commandPersonal = Yii::app()->db->createCommand($sqlPasien);
		$commandPersonal->execute();
		@unlink($path);
		 Yii::app()->user->setFlash('success', "Proses import data selesai<br/>
         Jumlah data yang sukses diimport : ".$sukses." Jumlah data yang gagal diimport : ".$gagal." 
         ".implode(",",$dataGagal));
		$this->redirect(array('index'));
		}
	}
	
	public function loadModel($id)
	{
		$model=Pasien::model()->findByPk($id);
		if($model===null)
		throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}

	/**
	* Performs the AJAX validation.
	* @param CModel the model to be validated
	*/
	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='pasien-form')
		{
		echo CActiveForm::validate($model);
		Yii::app()->end();
		}
	}
}
