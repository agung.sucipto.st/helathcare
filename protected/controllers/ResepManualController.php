<?php
class ResepManualController extends Controller
{
	/**
	* @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	* using two-column layout. See 'protected/views/layouts/column2.php'.
	*/
	public $layout='//layouts/admin/main';
	public $groupMenu=8;
	/**
	* @return array action filters
	*/
	public function filters()
	{
		return array(
		'accessControl', // perform access control for CRUD operations
		);
	}

	/**
	* Specifies the access control rules.
	* This method is used by the 'accessControl' filter.
	* @return array access control rules
	*/
	public function accessRules()
	{
		return array(
		array('allow',  // allow all users to perform 'index' and 'view' actions
		'actions'=>array('index'),
		'expression'=>'$user->getAuth()',
		),
		array('allow', // allow admin user to perform 'admin' and 'delete' actions
		'actions'=>array('proses', 'otc'),
		'expression'=>'$user->getAuth()',
		),

		array('deny',  // deny all users
		'users'=>array('*'),
		),
		);
	}



	/**
	* Creates a new model.
	* If creation is successful, the browser will be redirected to the 'view' page.
	*/
	public function actionOtc()
	{
		$registrasi=new Registrasi('otc');
		$transaksi=new ItemTransaksi('OTC');
		if(isset($_POST['ItemTransaksi'])){
			$gudang = $_POST['ItemTransaksi']['gudang_asal'];
			$error = 0;
			foreach($_POST['id_item'] as $index=>$row){
				if(is_array($row)){
					foreach($row as $key=>$item){
						$stok = ItemGudang::getStock($key,$gudang);
						if(($item['qty'] * $_POST['qty'.$index]) > $stok) {
							$error++;
						}
					}
				} else {
					$stok = ItemGudang::getStock($row,$gudang);
					if($_POST['qty'.$row] > $stok) {
						$error++;
					}
				}
			}
			if($error == 0) {
				$registrasi->id_kelas=2;
				$registrasi->catatan=$_POST['ItemTransaksi']['nama_pasien'];
				$registrasi->id_departemen=14;
				$registrasi->jenis_registrasi="OTC";
				$registrasi->status_registrasi="Tutup Kunjungan";
				$registrasi->user_create=Yii::app()->user->id;
				$registrasi->jenis_jaminan="UMUM";
				$registrasi->no_registrasi=Registrasi::getRegNumber(date("Y-m-d"));
				$registrasi->waktu_registrasi=date("Y-m-d H:i:s");
				$registrasi->time_create=date("Y-m-d H:i:s");
			
				$transaksi->nama_pasien=$_POST['ItemTransaksi']['nama_pasien'];
				$transaksi->id_registrasi=$registrasi->id_registrasi;
				$transaksi->gudang_asal=$gudang;//farmasi
				$transaksi->waktu_transaksi=date("Y-m-d H:i:s");
				$transaksi->id_jenis_item_transaksi=2;
				$transaksi->catatan_transaksi="Penjualan Resep Pasien $registrasi->no_registrasi - ".$model->idPersonal->nama_lengkap." [".Lib::MRN($model->id_pasien)."]";
				$transaksi->no_transaksi=ItemTransaksi::getTransactionNumber($transaksi->id_jenis_item_transaksi,"R",date("Y-m-d"));
				$transaksi->hapus=0;
				$transaksi->user_create=Yii::app()->user->id;
				$transaksi->time_create=date("Y-m-d H:i:s");
				$total = 0;
				if(!empty($_POST['id_item'])){
					if($registrasi->save()){
						$transaksi->id_registrasi=$registrasi->id_registrasi;
						if($transaksi->save()){
							foreach($_POST['id_item'] as $index=>$row){
								if(is_array($row)){
									// racikan header
									$parent=new DaftarItemTransaksi;
									$parent->id_item_transaksi=$transaksi->id_item_transaksi;
									$parent->is_racikan=1;
									$parent->nama_racikan=$_POST['name'.$index];
									$parent->biaya_racikan=$_POST['biaya'.$index];
									$parent->jumlah_transaksi=$_POST['qty'.$index];
									$parent->signa=$_POST['signa'.$index];
									$parent->save();
									$sTotal = 0;
									foreach($row as $key=>$item){
										// racikan component
										$list=new DaftarItemTransaksi;
										$list->id_parent_racikan=$parent->id_daftar_item_transaksi;
										$list->id_item_transaksi=$transaksi->id_item_transaksi;
										$list->id_item_gudang=ItemGudang::getId($key,$transaksi->gudang_asal);
										$list->id_item=$key;
										$list->id_item_satuan_besar=Item::getParentUnit($key)->id_item_satuan;
										$list->id_item_satuan_kecil=Item::getParentUnit($key)->id_item_satuan;
										$list->jumlah_satuan_kecil=Item::getParentUnit($key)->nilai_satuan_konversi;
										$list->dosis=$item['qty'];
										$list->jumlah_satuan_besar=ceil($item['qty'] * $_POST['qty'.$index]);
										$list->jumlah_transaksi=$list->jumlah_satuan_kecil*$list->jumlah_satuan_besar;
										$list->harga_transaksi=Item::getTarif($registrasi, $registrasi->id_kelas, $key)->tarif;
										$list->harga_wac=Item::getWAC($key);
										$list->jumlah_sebelum_transaksi=ItemGudang::getStock($key,$transaksi->gudang_asal);
										$list->jumlah_setelah_transaksi=$list->jumlah_sebelum_transaksi-$list->jumlah_transaksi;
										$sTotal+=$list->harga_transaksi*$list->jumlah_transaksi;
										if($list->save()){
											ItemGudang::updateStock($key,$transaksi->gudang_asal,$list->jumlah_setelah_transaksi);
										}
									}
									$total+=$sTotal;
									$parent->harga_transaksi=$sTotal/$parent->jumlah_transaksi;
									if($parent->save()){									
										$tagihan=new DaftarTagihan;
										$tagihan->id_registrasi=$registrasi->id_registrasi;
										$tagihan->id_jenis_komponen_tagihan=3; // Room Usage
										$tagihan->id_daftar_item_transaksi=$parent->id_daftar_item_transaksi;
										$tagihan->deskripsi_komponen=$parent->nama_racikan;
										$tagihan->waktu_daftar_tagihan=date("Y-m-d H:i:s");
										$tagihan->harga=$parent->harga_transaksi+($parent->biaya_racikan/$parent->jumlah_transaksi);
										$tagihan->tarif=$parent->harga_transaksi+($parent->biaya_racikan/$parent->jumlah_transaksi);
										$tagihan->tarif_orig=$parent->harga_transaksi+($parent->biaya_racikan/$parent->jumlah_transaksi);
										$tagihan->jumlah=$parent->jumlah_transaksi;
										$tagihan->jumlah_bayar=$tagihan->harga*$tagihan->jumlah;
										$tagihan->user_create=Yii::app()->user->id;
										$tagihan->time_create=date("Y-m-d H:i:s");
										if($tagihan->save()) {
											$parent->id_daftar_tagihan=$tagihan->id_daftar_tagihan;
											$parent->save();
										}
									}
								} else {
									// non racikan
									//Menyimpan Item Transaksi
									$list=new DaftarItemTransaksi;
									$list->id_item_transaksi=$transaksi->id_item_transaksi;
									$list->id_item_gudang=ItemGudang::getId($row,$transaksi->gudang_asal);
									$list->id_item=$row;
									$list->id_item_satuan_besar=Item::getParentUnit($row)->id_item_satuan;
									$list->id_item_satuan_kecil=Item::getParentUnit($row)->id_item_satuan;
									$list->jumlah_satuan_kecil=Item::getParentUnit($row)->nilai_satuan_konversi;
									$list->jumlah_satuan_besar=$_POST['qty'.$row];
									$list->jumlah_transaksi=$list->jumlah_satuan_kecil*$list->jumlah_satuan_besar;
									$list->harga_transaksi=Item::getTarif($registrasi, $registrasi->id_kelas, $row)->tarif;
									$list->harga_wac=Item::getWAC($row);
									$list->signa=$_POST['signa'.$row];
									$list->jumlah_sebelum_transaksi=ItemGudang::getStock($row,$transaksi->gudang_asal);
									$list->jumlah_setelah_transaksi=$list->jumlah_sebelum_transaksi-$list->jumlah_transaksi;
									$total+=$list->harga_transaksi*$list->jumlah_transaksi;
									if($list->save()){
										//Update Stok di gudang terkait
										ItemGudang::updateStock($row,$transaksi->gudang_asal,$list->jumlah_setelah_transaksi);
																			
										$tagihan=new DaftarTagihan;
										$tagihan->id_registrasi=$registrasi->id_registrasi;
										$tagihan->id_jenis_komponen_tagihan=3; // Room Usage
										$tagihan->id_daftar_item_transaksi=$list->id_daftar_item_transaksi;
										$tagihan->deskripsi_komponen=$list->idItem->nama_item;
										$tagihan->waktu_daftar_tagihan=date("Y-m-d H:i:s");
										$tagihan->harga=$list->harga_transaksi;
										$tagihan->tarif=$list->harga_transaksi;
										$tagihan->tarif_orig=$list->harga_transaksi;
										$tagihan->jumlah=$list->jumlah_transaksi;
										$tagihan->jumlah_bayar=$tagihan->harga*$tagihan->jumlah;
										$tagihan->user_create=Yii::app()->user->id;
										$tagihan->time_create=date("Y-m-d H:i:s");
										if($tagihan->save()) {
											$list->id_daftar_tagihan=$tagihan->id_daftar_tagihan;
											$list->save();
										}
									}
								}
							}
							$transaksi->nominal_transaksi=$total;
							$transaksi->save();
							
							$this->redirect(array("farmasi/index"));
						} else {
							Yii::app()->user->setFlash('failed', 'Gagal Menyimpan Transaksi !');
						}
					} else {
						Yii::app()->user->setFlash('failed', 'Gagal Menyimmpan Registrasi !'.CActiveForm::validate($registrasi));
					}
				} else {
					Yii::app()->user->setFlash('failed', 'Item Harus Diisi !');
				}
			} else {
				Yii::app()->user->setFlash('error', 'Salah Satu Item Melebihi Stok Persediaan !');
			}
		}
		
		$this->render('proses_resep_otc',array(
		"registrasi"=>$registrasi,"transaksi"=>$transaksi
		));
	}
	
	public function actionProses($id)
	{
		$registrasi=Registrasi::model()->findByPk($id);
		$model=Pasien::model()->findByPk($registrasi->id_pasien);
		$transaksi=new ItemTransaksi;
		$gudang=1;//farmasi
		if(isset($_POST['ItemTransaksi'])){
			$error = 0;
			foreach($_POST['id_item'] as $index=>$row){
				if(is_array($row)){
					foreach($row as $key=>$item){
						$stok = ItemGudang::getStock($key,$gudang);
						if(($item['qty'] * $_POST['qty'.$index]) > $stok) {
							$error++;
						}
					}
				} else {
					$stok = ItemGudang::getStock($row,$gudang);
					if($_POST['qty'.$row] > $stok) {
						$error++;
					}
				}
			}
			if($error == 0){
				//Definisi Group Item Transaksi
				$transaksi=new ItemTransaksi;
				$transaksi->id_registrasi=$registrasi->id_registrasi;
				$transaksi->gudang_asal=$gudang;//farmasi
				$transaksi->waktu_transaksi=date("Y-m-d H:i:s");
				$transaksi->id_jenis_item_transaksi=2;
				$transaksi->catatan_transaksi="Penjualan Resep Pasien $registrasi->no_registrasi - ".$model->idPersonal->nama_lengkap." [".Lib::MRN($model->id_pasien)."]";
				$transaksi->no_transaksi=ItemTransaksi::getTransactionNumber($transaksi->id_jenis_item_transaksi,"R",date("Y-m-d"));
				$transaksi->hapus=0;
				$transaksi->user_create=Yii::app()->user->id;
				$transaksi->time_create=date("Y-m-d H:i:s");
				$total = 0;
				if(!empty($_POST['id_item'])){
					if($transaksi->save()){
						foreach($_POST['id_item'] as $index=>$row){
							if(is_array($row)){
								// racikan header
								$parent=new DaftarItemTransaksi;
								$parent->id_item_transaksi=$transaksi->id_item_transaksi;
								$parent->is_racikan=1;
								$parent->nama_racikan=$_POST['name'.$index];
								$parent->biaya_racikan=$_POST['biaya'.$index];
								$parent->jumlah_transaksi=$_POST['qty'.$index];
								
								$parent->signa=$_POST['signa'.$index];
								$parent->save();
								$sTotal = 0;
								foreach($row as $key=>$item){
									// racikan component
									$list=new DaftarItemTransaksi;
									$list->id_parent_racikan=$parent->id_daftar_item_transaksi;
									$list->id_item_transaksi=$transaksi->id_item_transaksi;
									$list->id_item_gudang=ItemGudang::getId($key,$transaksi->gudang_asal);
									$list->id_item=$key;
									$list->id_item_satuan_besar=Item::getParentUnit($key)->id_item_satuan;
									$list->id_item_satuan_kecil=Item::getParentUnit($key)->id_item_satuan;
									$list->jumlah_satuan_kecil=Item::getParentUnit($key)->nilai_satuan_konversi;
									$list->jumlah_satuan_besar=ceil($item['qty'] * $_POST['qty'.$index]);
									$list->jumlah_transaksi=$list->jumlah_satuan_kecil*$list->jumlah_satuan_besar;
									$list->harga_transaksi=Item::getTarif($registrasi, $registrasi->id_kelas, $key)->tarif;
									$list->harga_wac=Item::getWAC($key);
									$list->dosis=$item['qty'];
									$list->jumlah_sebelum_transaksi=ItemGudang::getStock($key,$transaksi->gudang_asal);
									$list->jumlah_setelah_transaksi=$list->jumlah_sebelum_transaksi-$list->jumlah_transaksi;
									$sTotal+=$list->harga_transaksi*$list->jumlah_transaksi;
									if($list->save()){
										ItemGudang::updateStock($key,$transaksi->gudang_asal,$list->jumlah_setelah_transaksi);
									}
								}
								$total+=$sTotal;
								$parent->harga_transaksi=$sTotal/$parent->jumlah_transaksi;
								if($parent->save()){									
									$tagihan=new DaftarTagihan;
									$tagihan->id_registrasi=$registrasi->id_registrasi;
									$tagihan->id_jenis_komponen_tagihan=3; // Room Usage
									$tagihan->id_daftar_item_transaksi=$parent->id_daftar_item_transaksi;
									$tagihan->deskripsi_komponen=$parent->nama_racikan;
									$tagihan->waktu_daftar_tagihan=date("Y-m-d H:i:s");
									$tagihan->harga=$parent->harga_transaksi+($parent->biaya_racikan/$parent->jumlah_transaksi);
									$tagihan->tarif=$parent->harga_transaksi+($parent->biaya_racikan/$parent->jumlah_transaksi);
									$tagihan->tarif_orig=$parent->harga_transaksi+($parent->biaya_racikan/$parent->jumlah_transaksi);
									$tagihan->jumlah=$parent->jumlah_transaksi;
									$tagihan->jumlah_bayar=$tagihan->harga*$tagihan->jumlah;
									$tagihan->user_create=Yii::app()->user->id;
									$tagihan->time_create=date("Y-m-d H:i:s");
									if($tagihan->save()) {
										$parent->id_daftar_tagihan=$tagihan->id_daftar_tagihan;
										$parent->save();
									}
								}
							} else {
								// non racikan
								//Menyimpan Item Transaksi
								$list=new DaftarItemTransaksi;
								$list->id_item_transaksi=$transaksi->id_item_transaksi;
								$list->id_item_gudang=ItemGudang::getId($row,$transaksi->gudang_asal);
								$list->id_item=$row;
								$list->id_item_satuan_besar=Item::getParentUnit($row)->id_item_satuan;
								$list->id_item_satuan_kecil=Item::getParentUnit($row)->id_item_satuan;
								$list->jumlah_satuan_kecil=Item::getParentUnit($row)->nilai_satuan_konversi;
								$list->jumlah_satuan_besar=$_POST['qty'.$row];
								$list->jumlah_transaksi=$list->jumlah_satuan_kecil*$list->jumlah_satuan_besar;
								$list->harga_transaksi=Item::getTarif($registrasi, $registrasi->id_kelas, $row)->tarif;
								$list->harga_wac=Item::getWAC($row);
								$list->signa=$_POST['signa'.$row];
								$list->jumlah_sebelum_transaksi=ItemGudang::getStock($row,$transaksi->gudang_asal);
								$list->jumlah_setelah_transaksi=$list->jumlah_sebelum_transaksi-$list->jumlah_transaksi;
								$total+=$list->harga_transaksi*$list->jumlah_transaksi;
								if($list->save()){
									//Update Stok di gudang terkait
									ItemGudang::updateStock($row,$transaksi->gudang_asal,$list->jumlah_setelah_transaksi);
																		
									$tagihan=new DaftarTagihan;
									$tagihan->id_registrasi=$registrasi->id_registrasi;
									$tagihan->id_jenis_komponen_tagihan=3; // Room Usage
									$tagihan->id_daftar_item_transaksi=$list->id_daftar_item_transaksi;
									$tagihan->deskripsi_komponen=$list->idItem->nama_item;
									$tagihan->waktu_daftar_tagihan=date("Y-m-d H:i:s");
									$tagihan->harga=$list->harga_transaksi;
									$tagihan->tarif=$list->harga_transaksi;
									$tagihan->tarif_orig=$list->harga_transaksi;
									$tagihan->jumlah=$list->jumlah_transaksi;
									$tagihan->jumlah_bayar=$tagihan->harga*$tagihan->jumlah;
									$tagihan->user_create=Yii::app()->user->id;
									$tagihan->time_create=date("Y-m-d H:i:s");
									if($tagihan->save()) {
										$list->id_daftar_tagihan=$tagihan->id_daftar_tagihan;
										$list->save();
									}
								}
							}
						}
						$transaksi->nominal_transaksi=$total;
						$transaksi->save();
						
						$this->redirect(array("farmasi/index"));
					}
				}
			} else {
				Yii::app()->user->setFlash('error', 'Salah Satu Item Melebihi Stok Persediaan !');
			}
		}
		
		$this->render('proses_resep',array(
		'model'=>$model,"registrasi"=>$registrasi,"transaksi"=>$transaksi
		));
	}

	/**
	* Manages all models.
	*/
	public function actionIndex()
	{
		$model=new Registrasi('searchActive');
		$model->unsetAttributes();  // clear any default values
		if(isset($_GET['Registrasi']))
		$model->attributes=$_GET['Registrasi'];
		$model->status_registrasi='Aktif';
		if (isset($_GET['pageSize'])) {
			Yii::app()->user->setState('pageSize',(int)$_GET['pageSize']); 
			unset($_GET['pageSize']);
		}
		$this->render('admin',array(
		'model'=>$model,
		));
	}
	
	public function loadModel($id)
	{
		$model=ResepPasien::model()->findByPk($id);
		if($model===null)
		throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}

	/**
	* Performs the AJAX validation.
	* @param CModel the model to be validated
	*/
	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='resep-pasien-form')
		{
		echo CActiveForm::validate($model);
		Yii::app()->end();
		}
	}
}
