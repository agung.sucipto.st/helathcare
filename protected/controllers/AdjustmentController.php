<?php
class AdjustmentController extends Controller
{
	/**
	* @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	* using two-column layout. See 'protected/views/layouts/column2.php'.
	*/
	public $layout='//layouts/admin/main';
	public $groupMenu=3;

	/**
	* @return array action filters
	*/
	public function filters()
	{
		return array(
		'accessControl', // perform access control for CRUD operations
		);
	}

	/**
	* Specifies the access control rules.
	* This method is used by the 'accessControl' filter.
	* @return array access control rules
	*/
	public function accessRules()
	{
		return array(
		array('allow',  // allow all users to perform 'index' and 'view' actions
		'actions'=>array('index','listAdjust','delete'),
		'expression'=>'$user->getAuth()',
		),

		array('deny',  // deny all users
		'users'=>array('*'),
		),
		);
	}

	public function actionListAdjust()
	{
		$model=new ItemTransaction('search');
		$model->unsetAttributes();  // clear any default values
		$model->transaction_type_id=8;
		$model->item_transaction_time=date("m/d/Y").' - '.date("m/d/Y");
		if(isset($_GET['ItemTransaction']))
		$model->attributes=$_GET['ItemTransaction'];
		if (isset($_GET['pageSize'])) {
			Yii::app()->user->setState('pageSize',(int)$_GET['pageSize']); 
			unset($_GET['pageSize']);
		}
		$this->render('list_adjust',array(
		'model'=>$model,
		));
	}

	public function actionIndex()
	{
		$model=new ItemTransaction();
		if(isset($_POST['WarehouseItem'])){
			$adj=new ItemTransaction('adjustment');
			$adj->warehouse_origin_id=$_POST['warehouse'];
			$adj->item_transaction_time=date("Y-m-d H:i:s");
			$adj->transaction_type_id=8;//penerimaan
			$adj->user_create=Yii::app()->user->id;//penerimaan
			$adj->time_create=date("Y-m-d H:i:s");//penerimaan
			$adj->item_transaction_code=ItemTransaction::getCode($adj->transaction_type_id,date("Y"));
			$adj->item=1;
			
			if($adj->save()){

			$item= new ItemTransactionList;
			$item->item_transaction_id=$adj->item_transaction_id;
			$item->warehouse_item_id=$data->warehouse_item_id;
			$item->item_id=$_POST['item'];
			$item->item_big_unit_id=Item::getParentUnit($_POST['item'])->item_unit_id;
			$item->item_small_unit_id=Item::getParentUnit($_POST['item'])->item_unit_id;
			
			
			
			if($_POST['WarehouseItem']['stock']>$_POST['WarehouseItem']['stok_perbaikan']){
				$item->big_amount=$_POST['WarehouseItem']['stock']-$_POST['WarehouseItem']['stok_perbaikan'];
				$item->smal_amount=1;
				$item->transaction_amount=$item->big_amount*$item->smal_amount;
				$item->transaction_amount_before=Item::getStock($_POST['item'],$_POST['warehouse']);
				$item->transaction_amount_after=$item->transaction_amount_before-$item->transaction_amount;
			}else{
				$item->big_amount=$_POST['WarehouseItem']['stok_perbaikan']-$_POST['WarehouseItem']['stock'];
				$item->smal_amount=1;
				$item->transaction_amount=$item->big_amount*$item->smal_amount;
				$item->transaction_amount_before=Item::getStock($_POST['item'],$_POST['warehouse']);
				$item->transaction_amount_after=$item->transaction_amount_before+$item->transaction_amount;
			}
			$item->discount=0;
			$item->transaction_price_per_unit=Item::getWAC($_POST['item']);
			$item->transaction_wacc=Item::getWAC($item-item_id);
			
			$item->expire_date=$_POST['WarehouseItem']['expired'];
			$nTotal+=$item->transaction_price_per_unit*$item->transaction_amount;
			
			if($item->save()){
				Item::updateStock($_POST['item'],$adj->warehouse_origin_id,$item->transaction_amount_after);
			}
			 Yii::app()->user->setFlash('success', "Adjustment Berhasil !");
				$this->redirect(array('listAdjust'));
			}
		}
		
		if(isset($_POST['ItemTransaction'])){
			$brg=explode(" | ",$_POST['item']);
			$item=Item::model()->find(array("condition"=>"code='$brg[1]' AND item_name='$brg[0]'"));
			$warehouse=$_POST['ItemTransaction']['warehouse_destination_id'];
			$data=WarehouseItem::model()->find(array("condition"=>"warehouse_id='$warehouse' AND item_id='$item->item_id'"));
			if(empty($data)){
				$data=new WarehouseItem;
				$data->item_id=$item->item_id;
				$data->warehouse_id=$warehouse;
				$data->stock=0;
				$data->save();
			}	
			$data->item_name=$brg[0];
			$model->warehouse_destination_id=$warehouse;
		}else{
			$data=null;
			$item=null;
			$warehouse=null;
		}
		$this->render('index',array(
		'data'=>$data,'model'=>$model,'item'=>$item,'warehouse'=>$warehouse
		));
	}
	
	public function actionDelete($id)
	{
		if(Yii::app()->request->isPostRequest)
		{
		// we only allow deletion via POST request		
		$model=$this->loadModel($id);
		$list=ItemTransactionList::model()->findAll(array("condition"=>"item_transaction_id='$id'"));
		foreach($list as $row){
			Item::updateStock($row->item_id,$row->itemTransaction->warehouse_origin_id,$row->transaction_amount_before);
		}
		$this->loadModel($id)->delete();

		// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
		if(!isset($_GET['ajax'])){
			$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
		}
		}
		else
		throw new CHttpException(400,'Invalid request. Please do not repeat this request again.');
	}
	
	public function loadModel($id)
	{
		$model=ItemTransaction::model()->findByPk($id);
		if($model===null)
		throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}

	/**
	* Performs the AJAX validation.
	* @param CModel the model to be validated
	*/
	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='item-transaction-form')
		{
		echo CActiveForm::validate($model);
		Yii::app()->end();
		}
	}
}
