<?php
class WarehouseReportController extends Controller
{
	/**
	* @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	* using two-column layout. See 'protected/views/layouts/column2.php'.
	*/
	public $layout='//layouts/admin/main';
	public $groupMenu=13;

	/**
	* @return array action filters
	*/
	public function filters()
	{
		return array(
		'accessControl', // perform access control for CRUD operations
		);
	}

	/**
	* Specifies the access control rules.
	* This method is used by the 'accessControl' filter.
	* @return array access control rules
	*/
	public function accessRules()
	{
		return array(
		array('allow',  // allow all users to perform 'index' and 'view' actions
		'actions'=>array('kartuStok','stokStatus'),
		'expression'=>'$user->getAuth()',
		),
		array('deny',  // deny all users
		'users'=>array('*'),
		),
		);
	}

	/**
	* Displays a particular model.
	* @param integer $id the ID of the model to be displayed
	*/
	public function actionKartuStok()
	{
		$model=new ItemTransaksi("kartuStok");
		if(isset($_POST['ItemTransaksi']) AND $_POST['ItemTransaksi']['waktu_transaksi']!='' AND $_POST['item']!='' AND $_POST['ItemTransaksi']['gudang_tujuan']!=''){
			$list=explode(" - ",$_POST['ItemTransaksi']['waktu_transaksi']);
			$start=explode("/",$list[0]);
			$end=explode("/",$list[1]);
			$dateStart=$start[2]."-".$start[0]."-".$start[1];
			$dateEnd=$end[2]."-".$end[0]."-".$end[1];
			$model->waktu_transaksi=$_POST['ItemTransaksi']['waktu_transaksi'];
			$model->gudang_tujuan=$_POST['ItemTransaksi']['gudang_tujuan'];
			$brg=explode(" | ",$_POST['item']);
			$item=Item::model()->find(array("condition"=>"kode_item='$brg[1]' AND nama_item='$brg[0]'"));
			$postItem=$_POST['item'];
			$warehouse=$_POST['ItemTransaksi']['gudang_tujuan'];
			$criteria=new CDbCriteria;
			$criteria->with=array("idItemTransaksi");
			$IG=ItemGudang::model()->find(array("condition"=>"id_item='$item->id_item' AND id_gudang='$model->gudang_tujuan'"));
			//$criteria->condition="idItemTransaksi.gudang_tujuan='$warehouse' OR idItemTransaksi.gudang_asal='$warehouse'";
			$criteria->addCondition("id_item_gudang='$IG->id_item_gudang'");
			$criteria->addCondition("DATE(waktu_transaksi) between '$dateStart' AND '$dateEnd'");
			$criteria->order="idItemTransaksi.waktu_transaksi ASC";
			$data=DaftarItemTransaksi::model()->findAll($criteria);
			
		}else{
			$data=null;
			$item=null;
			$warehouse=null;
			$postItem=null;
		}
		$this->render('kartu_stok',array(
		'data'=>$data,'model'=>$model,'item'=>$item,'warehouse'=>$warehouse,'postItem'=>$postItem
		));
	}
	
	
	public function actionStokStatus()
	{
		$model=new ItemTransaksi;
		if(isset($_POST['ItemTransaksi'])){
			$model->gudang_tujuan=$_POST['ItemTransaksi']['gudang_tujuan'];
			$warehouse=$_POST['ItemTransaksi']['gudang_tujuan'];
			if(!empty($warehouse)){
				$criteria=new CDbCriteria;
				$criteria->with=array("idItem");
				$criteria->condition="id_gudang='$warehouse'";
				$criteria->order="idItem.nama_item,stok ASC";
				$data=ItemGudang::model()->findAll($criteria);
			}else{
				$criteria=new CDbCriteria;
				$criteria->with=array("idItem");
				$criteria->order="idItem.nama_item,stok ASC";
				$criteria->group="idItem.id_item";
				$data=ItemGudang::model()->findAll($criteria);
			}
		}else{
			$data=null;
			$warehouse=null;
		}
		$this->render('stok_status',array(
		'data'=>$data,'model'=>$model,'warehouse'=>$warehouse
		));
	}
}
