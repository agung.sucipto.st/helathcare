<?php
class SatuanController extends Controller
{
	/**
	* @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	* using two-column layout. See 'protected/views/layouts/column2.php'.
	*/
	public $layout='//layouts/admin/main';
	public $groupMenu=13;

	/**
	* @return array action filters
	*/
	public function filters()
	{
		return array(
		'accessControl', // perform access control for CRUD operations
		);
	}

	/**
	* Specifies the access control rules.
	* This method is used by the 'accessControl' filter.
	* @return array access control rules
	*/
	public function accessRules()
	{
		return array(
		array('allow',  // allow all users to perform 'index' and 'view' actions
		'actions'=>array('index','view'),
		'expression'=>'$user->getAuth()',
		),
		array('allow', // allow admin user to perform 'admin' and 'delete' actions
		'actions'=>array('create','update','admin','delete'),
		'expression'=>'$user->getAuth()',
		),

		array('allow',  // allow all users to perform 'index' and 'view' actions
		'actions'=>array('getUnit','addUnit','changeUnit','deleteUnit'),
		'users'=>array('*'),
		),
		array('deny',  // deny all users
		'users'=>array('*'),
		),
		);
	}
	
	public function actionGetUnit()
	{
		$this->layout='//layouts/admin/blank';
		$this->pageTitle="Satuan";
		$model=new Satuan('search');
		$model->unsetAttributes();  // clear any default values
		if(isset($_GET['Satuan']))
		$model->attributes=$_GET['Satuan'];
		if (isset($_GET['pageSize'])) {
			Yii::app()->user->setState('pageSize',(int)$_GET['pageSize']); 
			unset($_GET['pageSize']);
		}
		$this->render('get_unit',array(
		'model'=>$model,
		));
	}
	
	public function actionChangeUnit($id)
	{
		$this->layout='//layouts/admin/blank';
		$this->pageTitle="Ubah Satuan";
		$model=ItemSatuan::model()->findByPk($id);
		$parent=ItemSatuan::model()->find(array("condition"=>"id_item='$model->id_item' AND parent_id_item_satuan IS NULL"));
		$show=true;
		if(isset($_POST['ItemSatuan']))
		{
			$model->attributes=$_POST['ItemSatuan'];
			if($model->save()){
				$show=false;
			}
		}
		
		$this->render('change_unit',array(
		'model'=>$model,'show'=>$show,'parent'=>$parent
		));
	}
	
	public function actionAddUnit($id)
	{
		$this->layout='//layouts/admin/blank';
		$this->pageTitle="Tambah Satuan";
		$item=Item::model()->findByPk($id);
		$parent=ItemSatuan::model()->find(array("condition"=>"id_item='$id' AND parent_id_item_satuan IS NULL"));
		$model=new ItemSatuan;
		$show=true;
		if(isset($_POST['ItemSatuan']))
		{
			$model->attributes=$_POST['ItemSatuan'];
			$model->id_item=$id;
			$model->parent_id_item_satuan=$parent->id_item_satuan;
			if($model->save()){
				$show=false;
			}
		}
		
		$this->render('add_unit',array(
		'model'=>$model,'show'=>$show,'parent'=>$parent
		));
	}
	
	public function actionDeleteUnit($id)
	{
		$model=ItemSatuan::model()->findByPk($id);
		$idItem=$model->id_item;
		$model->delete();
		$this->redirect(array('item/view','id'=>$idItem));
	}
	/**
	* Displays a particular model.
	* @param integer $id the ID of the model to be displayed
	*/
	public function actionView($id)
	{
		$this->render('view',array(
		'model'=>$this->loadModel($id),
		));
	}

	/**
	* Creates a new model.
	* If creation is successful, the browser will be redirected to the 'view' page.
	*/
	public function actionCreate()
	{
		$model=new Satuan;

		// Uncomment the following line if AJAX validation is needed
		$this->performAjaxValidation($model);

		if(isset($_POST['Satuan']))
		{
			$model->attributes=$_POST['Satuan'];
			if($model->save()){
				$this->redirect(array('view','id'=>$model->id_satuan));
			}
		}

		$this->render('create',array(
		'model'=>$model,
		));
	}

	/**
	* Updates a particular model.
	* If update is successful, the browser will be redirected to the 'view' page.
	* @param integer $id the ID of the model to be updated
	*/
	public function actionUpdate($id)
	{
		$model=$this->loadModel($id);

		// Uncomment the following line if AJAX validation is needed
		$this->performAjaxValidation($model);
	
		if(isset($_POST['Satuan']))
		{
			$model->attributes=$_POST['Satuan'];
			if($model->save()){
				$this->redirect(array('view','id'=>$model->id_satuan));
			}
		}

		$this->render('update',array(
		'model'=>$model,
		));
	}

	/**
	* Deletes a particular model.
	* If deletion is successful, the browser will be redirected to the 'admin' page.
	* @param integer $id the ID of the model to be deleted
	*/
	public function actionDelete($id)
	{
		if(Yii::app()->request->isPostRequest)
		{
		// we only allow deletion via POST request		
		$this->loadModel($id)->delete();

		// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
		if(!isset($_GET['ajax']))
		$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
		}
		else
		throw new CHttpException(400,'Invalid request. Please do not repeat this request again.');
	}

	/**
	* Lists all models.
	*/
	public function actionAdmin()
	{
		$dataProvider=new CActiveDataProvider('Satuan');
		$this->render('index',array(
		'dataProvider'=>$dataProvider,
		));
	}

	/**
	* Manages all models.
	*/
	public function actionIndex()
	{
		$model=new Satuan('search');
		$model->unsetAttributes();  // clear any default values
		if(isset($_GET['Satuan']))
		$model->attributes=$_GET['Satuan'];
		if (isset($_GET['pageSize'])) {
			Yii::app()->user->setState('pageSize',(int)$_GET['pageSize']); 
			unset($_GET['pageSize']);
		}
		$this->render('admin',array(
		'model'=>$model,
		));
	}
	
	public function loadModel($id)
	{
		$model=Satuan::model()->findByPk($id);
		if($model===null)
		throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}

	/**
	* Performs the AJAX validation.
	* @param CModel the model to be validated
	*/
	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='satuan-form')
		{
		echo CActiveForm::validate($model);
		Yii::app()->end();
		}
	}
}
