<?php

class DefaultController extends Controller
{
	public $layout='//layouts/admin/main';

	/**
	* @return array action filters
	*/
	public function filters()
	{
		return array(
		'accessControl', // perform access control for CRUD operations
		);
	}

	/**
	* Specifies the access control rules.
	* This method is used by the 'accessControl' filter.
	* @return array access control rules
	*/
	public function accessRules()
	{
		return array(
			array('allow',  // allow all users to perform 'index' and 'view' actions
			'actions'=>array('index','logout'),
			'expression'=>'$user->getAuth()',
			),
			
			array('allow',  // allow all users to perform 'index' and 'view' actions
			'actions'=>array('login','error'),
			'users'=>array('*'),
			),
			
			array('deny',
                'users'=>array('*'),
            ),

		);
	}
	
	public function actionIndex()
	{
		/*
		$model=Access::model()->findAll();
		foreach($model as $row){
			$new= new UserAccess;
			$new->id_user=Yii::app()->user->id;
			$new->access_id=$row->access_id;
			$new->save();
		}
		//*/
		/*
		$model =new Access;
			
		$model->access_status=1;
		$model->access_visible=1;
		$model->access_name='User';
		
		$model->access_controller='user';
		$model->access_action='index';
		
		$model->access_parent=67;
		if($model->save()){
			$action=array("view","update","delete","create");
			$parent=0;
			foreach($action as $c){
				$new =new Access;
				
				$new->access_visible=0;
				$new->access_status=1;;
				$new->access_name=ucwords($model->access_name.' '.$c);
				
				$new->access_controller=$model->access_controller;
				$new->access_action=$c;
				
				$new->access_parent=$model->access_id;
				$new->save();
			}
		}
		//*/
		
		$title="Grafik Kunjungan<br/>".Lib::dateIndShortMonth($dateStart).' - '.Lib::dateIndShortMonth($dateEnd);
		$query="select 
		MONTH(waktu_registrasi) as tanggal, 
		SUM(IF(jenis_jaminan='UMUM',1,0)) AS umum,
		SUM(IF(jenis_jaminan='JAMINAN',1,0)) AS asuransi,
		SUM(IF(jenis_kunjungan=1,1,0)) AS baru,
		SUM(IF(jenis_kunjungan=0,1,0)) AS lama,
		SUM(IF(personal.jenis_kelamin='Laki-laki',1,0)) AS laki,
		SUM(IF(personal.jenis_kelamin='Perempuan',1,0)) AS perempuan 
		from registrasi inner JOIN pasien ON pasien.id_pasien=registrasi.id_pasien inner JOIN personal ON personal.id_personal=pasien.id_personal where MONTH(registrasi.waktu_registrasi)='".date('m')."' AND YEAR(registrasi.waktu_registrasi)='".date('Y')."' AND (jenis_registrasi='Rawat Jalan' OR jenis_registrasi='IGD') AND status_registrasi!='Batal' group by MONTH(waktu_registrasi) order by tanggal ASC";
		
		$queryLayanan="SELECT count(registrasi.id_departemen) as jumlah,departemen.nama_departemen from registrasi inner join departemen ON registrasi.id_departemen=departemen.id_departemen where MONTH(registrasi.waktu_registrasi)='".date('m')."' AND YEAR(registrasi.waktu_registrasi)='".date('Y')."' AND (jenis_registrasi='Rawat Jalan' OR jenis_registrasi='IGD') AND status_registrasi!='Batal' group by registrasi.id_departemen";
		
		
		$queryLine="select DATE(waktu_registrasi) as tanggal, SUM(IF(jenis_kunjungan=1,1,0)) AS baru,
		SUM(IF(jenis_kunjungan=0,1,0)) AS lama from registrasi where MONTH(registrasi.waktu_registrasi)='".date("m")."' AND YEAR(registrasi.waktu_registrasi)='".date('Y')."' AND (jenis_registrasi='Rawat Jalan' OR jenis_registrasi='IGD') group by DATE(waktu_registrasi) order by tanggal ASC";
		
		$queryPendapatan="select DATE(tagihan_pasien.waktu_tagihan) as tanggal, SUM(IF(registrasi.jenis_jaminan='UMUM',tagihan_pasien.total_tagihan,0)) AS umum,
		SUM(IF(registrasi.jenis_jaminan='JAMINAN',tagihan_pasien.total_tagihan,0)) AS asuransi from tagihan_pasien inner join registrasi ON registrasi.id_registrasi=tagihan_pasien.id_registrasi where MONTH(tagihan_pasien.waktu_tagihan)='".date('m')."' AND YEAR(tagihan_pasien.waktu_tagihan)='".date('Y')."' AND (registrasi.jenis_registrasi='Rawat Jalan' OR registrasi.jenis_registrasi='IGD') group by DATE(tagihan_pasien.waktu_tagihan) order by tanggal ASC";

		$get=Yii::app()->db->createCommand($query)->queryAll();
		$getLine=Yii::app()->db->createCommand($queryLine)->queryAll();
		$getPendapatan=Yii::app()->db->createCommand($queryPendapatan)->queryAll();
		$getLayanan=Yii::app()->db->createCommand($queryLayanan)->queryAll();

		$pieJk=array();
		$pieJn=array();
		$pieJp=array();
		$pieLayanan=array();
		foreach($get as $row){
			$pieJk[]='{name: "Laki-laki",y: '.$row['laki'].'}';
			$pieJk[]='{name: "Perempuan",y: '.$row['perempuan'].'}';
			$pieJn[]='{name: "Baru",y: '.$row['baru'].'}';
			$pieJn[]='{name: "Lama",y: '.$row['lama'].'}';
			$pieJp[]='{name: "Umum",y: '.$row['umum'].'}';
			$pieJp[]='{name: "Asuranasi",y: '.$row['asuransi'].'}';
			
		}
		
		if(!empty($getLine)){
			$dataX=array();
			foreach($getLine as $row){
				$dataX[$row['tanggal']]['Baru']=$row['baru'];	
				$dataX[$row['tanggal']]['Lama']=$row['lama'];	
			}
			
			$highcartLabel=array();
			$highcartSeries=array();
			$highcartSeriesValues=array();
			foreach($dataX as $index=>$val){
				$highcartLabel[]='"'.$index.'"';
				$data[$index].="{ y: '".$alias[$index]."', ";
				
				$temp=array();
				foreach($val as $r=>$i){
					$temp[].=str_replace(" ","",$r).":'$i'";
					$detail[str_replace(" ","",$r)]=$r;
					$highcartSeries[$index][$r]=$i;
				}
				$data[$index].=implode(", ",$temp);
				$data[$index].='}';
			}
			
			foreach($detail as $dRow){
				$highcartSeriesValues[$dRow]="{name: \"$dRow\",data: [";
				$tempData=array();
				foreach($highcartLabel as $subRow){
					$tempData[]=($highcartSeries[str_replace('"',"",$subRow)][$dRow]=="")?0:$highcartSeries[str_replace('"',"",$subRow)][$dRow];
				}
				$highcartSeriesValues[$dRow].=implode(",",$tempData);
				$highcartSeriesValues[$dRow].="]}";
			}
		}else{
			$highcartLabel=array();
			$highcartSeries=array();
			$highcartSeriesValues=array();
		}
		
		
		if(!empty($getPendapatan)){
			$dataXP=array();
			foreach($getPendapatan as $row){
				$dataXP[$row['tanggal']]['Umum']=$row['umum'];	
				$dataXP[$row['tanggal']]['Asuransi']=$row['asuransi'];	
			}
			
			$highcartLabelP=array();
			$highcartSeriesP=array();
			$highcartSeriesValuesP=array();
			foreach($dataXP as $index=>$val){
				$highcartLabelP[]='"'.$index.'"';
				
				$tempP=array();
				foreach($val as $r=>$i){
					$tempP[].=str_replace(" ","",$r).":'$i'";
					$detailP[str_replace(" ","",$r)]=$r;
					$highcartSeriesP[$index][$r]=$i;
				}
			}
			
			foreach($detailP as $dRow){
				$highcartSeriesValuesP[$dRow]="{name: \"$dRow\",data: [";
				$tempDataP=array();
				foreach($highcartLabelP as $subRow){
					$tempDataP[]=($highcartSeriesP[str_replace('"',"",$subRow)][$dRow]=="")?0:$highcartSeriesP[str_replace('"',"",$subRow)][$dRow];
				}
				$highcartSeriesValuesP[$dRow].=implode(",",$tempDataP);
				$highcartSeriesValuesP[$dRow].="]}";
			}
			
		}else{
			$highcartLabelP=array();
			$highcartSeriesP=array();
			$highcartSeriesValuesP=array();
		}
		
		foreach($getLayanan as $row){
			$pieLayanan[]='{name: "'.$row['nama_departemen'].'",y: '.$row['jumlah'].'}';
		}
		$this->render('index',array(
			'model'=>$model,
			'title'=>$title,
			'pieJk'=>$pieJk,
			'pieJn'=>$pieJn,
			'pieJp'=>$pieJp,
			'pieLayanan'=>$pieLayanan,
			'hLabel'=>$highcartLabel,'hValues'=>$highcartSeriesValues,
			'hLabelP'=>$highcartLabelP,'hValuesP'=>$highcartSeriesValuesP,
		));
	}
	
	public function actionLogin()
	{
		$model=new LoginForm('login');
		$this->layout='//layouts/admin/login';
		// if it is ajax validation request
		if(isset($_POST['ajax']) && $_POST['ajax']==='login-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}

		// collect user input data
		if(isset($_POST['LoginForm']))
		{
			$model->attributes=$_POST['LoginForm'];
			// validate user input and redirect to the previous page if valid
			if($model->validate() && $model->login()){
				$cookie_name = "LOGID";
				$cookie_value = Yii::app()->user->id;
				setcookie($cookie_name, $cookie_value, time() + (86400 * 30), "/");
				$update=User::model()->findbyPk(Yii::app()->user->id);
				$now=date('Y-m-d H:i:s');
				$update->last_login=$now;
				$update->save();
				$this->redirect(array('default/index'));
			}
		}
		// display the login form
		$this->render('login',array('model'=>$model));
	}
	
	public function actionLogout() {
        Yii::app()->user->logout();
		setcookie('LOGID', '', -1, '/');
		unset($_COOKIE['LOGID']);
       $this->redirect(array('default/login'));
    }
	
	public function actionError()
	{
		$this->layout='//layouts/admin/login';
		$erroe='';
		if($error=Yii::app()->errorHandler->error)
		{
			if(Yii::app()->request->isAjaxRequest)
				echo $error['message'];
			else
				$this->render('error', array('error'=>$error));
		}
	}
}