<?php
class CustomUrlRule extends CBaseUrlRule {

	public function createUrl($manager,$route,$params,$ampersand) {
		$param='';
		foreach($params as $index=>$row){
			$param.=$index.'/'.$row;
		}
		return $this->base64_url_encode($route.'/'.$param);
	}

	public function parseUrl($manager,$request,$pathInfo,$rawPathInfo) {
		return $this->base64_url_decode($pathInfo);
	}
  
	public function base64_url_encode($input) {
		return base64_encode($input);
	}

	public function base64_url_decode($input) {
		return base64_decode($input);
	} 
}