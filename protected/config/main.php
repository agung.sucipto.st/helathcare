<?php

// uncomment the following to define a path alias
// Yii::setPathOfAlias('local','path/to/local-folder');

// This is the main Web application configuration. Any writable
// CWebApplication properties can be configured here.
return array(
	'basePath'=>dirname(__FILE__).DIRECTORY_SEPARATOR.'..',
	'name'=>'Healthcare +',
	'theme'=>'adminLTE',
	'timezone'=>'Asia/Jakarta',
	'language'=>'EN',
	// preloading 'log' component
	'preload'=>array('log','booster'),
	// autoloading model and component classes
	'import'=>array(
		'application.models.*',
		'application.components.*',
		'booster.components.*',
		'booster.behaviors.*',
		'booster.helpers.*',
		'booster.widgets.*',
		
	),
	'defaultController' => 'default',
	'modules'=>array(
		'admin',
		'gii'=>array(
			'class'=>'system.gii.GiiModule',
			'password'=>'12345',
			// If removed, Gii defaults to localhost only. Edit carefully to taste.
			'ipFilters'=>array('127.0.0.1','::1','192.168.1.3'),
			'generatorPaths' => array(
                'booster.gii'
            ),
		),
		
	),
	///*
	'aliases' => array(
        'booster' => 'ext.booster',
    ),
	//*/
	
	// application components
	'components'=>array(
		'user'=>array(
			// enable cookie-based authentication
			'class'=>'WebUser',
			'allowAutoLogin'=>true,
			'loginUrl'=>array('default/login'),
		),
		// uncomment the following to enable URLs in path-format
		
		'booster' => array(
			'class' => 'booster.components.Booster',
			'responsiveCss'=>true
		),
		
		'urlManager'=>array(
		'showScriptName'=>false,
		//'caseSensitive'=>true,		
			'urlFormat'=>'path',
			'rules'=>array(
				'<controller:\w+>/<id:\d+>'=>'<controller>/view',
				'<controller:\w+>/<action:\w+>/<id:\d+>'=>'<controller>/<action>',
				'<controller:\w+>/<action:\w+>'=>'<controller>/<action>',
				/*
				array( // your custom URL handler
					'class' => 'application.components.CustomUrlRule',
				),
				*/
				
			),
		),
		
		/*
		'db'=>array(
			'connectionString' => 'sqlite:'.dirname(__FILE__).'/../data/testdrive.db',
		),
		// uncomment the following to use a MySQL database
		*/
		'db'=>array(
			'connectionString' => 'mysql:host=localhost;dbname=hospital',
			'emulatePrepare' => true,
			'username' => 'root',
			'password' => '',
			'charset' => 'utf8',
		),
		
		'errorHandler'=>array(
			// use 'site/error' action to display errors
			'errorAction'=>'default/error',
		),
		'log'=>array(
			'class'=>'CLogRouter',
			'routes'=>array(
				array(
					'class'=>'CFileLogRoute',
					'levels'=>'error, warning',
				),
				/*
				 array(
					'class'=>'ext.yii-debug-toolbar.YiiDebugToolbarRoute',
					'ipFilters'=>array('127.0.0.1','100.100.100.176'),
				),
				// uncomment the following to show log messages on web pages
				/*
				array(
					'class'=>'CWebLogRoute',
				),
				*/
			),
		),
	),

	// application-level parameters that can be accessed
	// using Yii::app()->params['paramName']
	'params'=>array(
		// this is used in contact page
		'adminEmail'=>'webmaster@example.com',
	),
);